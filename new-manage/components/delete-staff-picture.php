<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("[Error] In order to perform this action, must be authenticated to the management panel.");
  
$staff_identity = $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]);
$images = $database->querySingle("SELECT image_file, icon_file FROM staff WHERE identity='" . $staff_identity. "'", true);
if ($images === false)
  exit ("[Error] Staff is not in the database.");

if ($images["image_file"] != "" && $images["image_file"] != "no-staff-picture.jpg")
{
  if (!unlink(DOCUMENT_ROOT . "/images/staff/" . $images["image_file"]))
    exit ("[Error] Could not delete the image file from the image directory.");
}
if ($images["icon_file"] != "" && $images["icon_file"] != "no-staff-icon.jpg")
{
  if (!unlink(DOCUMENT_ROOT . "/images/staff/" . $images["icon_file"]))
    exit ("[Error] Could not delete the icon file from the image directory.");
}

$database->exec("UPDATE staff SET image_file='no-staff-picture.jpg', icon_file='no-staff-icon.jpg', icon_x='', icon_y='', icon_width='', icon_height='' WHERE identity='" .
	$staff_identity . "'");
$_SESSION[MANAGE_SESSION]["ICON"] = "no-staff-icon.jpg";
exit("success");
?>