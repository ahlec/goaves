<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to load this frame, you must be authenticated to the management panel.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (!isset($_GET["type"]) || !isset($_GET["identity"]))
  exit ("Parameters were not fully passed.");

switch (strtolower($_GET["type"]))
{
  case "podcast":
  {
    $type_database = "podcasts";
    $type_database_identity_column = "podcast_identity";
    $staff_identity_column = "staff_identity";
	$required_perm = PERM_PODCASTS;
    break;
  }
  case "beat":
  {
    $type_database = "beats";
    $type_database_identity_column = "beat_identity";
    $staff_identity_column = "staff_identity";
    $required_perm = PERM_BEATS;
    break;
  }
  case "gallery":
  {
    $type_database = "photo_galleries";
    $type_database_identity_column = "gallery_identity";
    $staff_identity_column = "staff_identity";
    $required_perm = PERM_GALLERIES;
    break;
  }
  case "video":
  {
    $type_database = "videos";
    $type_database_identity_column = "video_identity";
    $staff_identity_column = null;
    break;
  }
  case "comic":
  {
    $type_database = "cartoons";
    $type_database_identity_column = "identity";
    $staff_identity_column = "artist";
    $required_perm = PERM_COMICS;
    break;
  }
  case "soundslide":
  {
    $type_database = "sound_slides";
    $type_database_identity_column = "slideshow_identity";
    $staff_identity_column = "staff_identity";
	$required_perm = PERM_SOUNDSLIDES;
    break;
  }
  default: exit("Type '" . $content_type . "' is not a valid content type yet.");
}

if (substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], $required_perm, 1) < 1)
  exit ("You do not have the proper permissions to manage this type of content.");

if ($database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" .
	$database->escapeString($_GET["identity"]) . "'") == 0)
  exit ("Could not find the specified content when looking by identity.");
$identity = $database->escapeString($_GET["identity"]);

if ($staff_identity_column !== null && $database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE " .
	$type_database_identity_column . "='" . $identity . "' AND " . $staff_identity_column . "='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
  exit ("You are not the owner of the targeted piece of content.");
  
/* Prework section */
if ($_GET["type"] == "gallery")
{
  if ($database->querySingle("SELECT is_complete FROM photo_galleries WHERE gallery_identity='" . $identity . "' LIMIT 1") != 'TRUE')
    exit ("The gallery is not yet ready to be published (must have at least one picture).");
}
/* End prework section */

$old_value = $database->querySingle("SELECT published FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" . $identity . "' LIMIT 1");
$new_value = ($old_value == "TRUE" ? "FALSE" : "TRUE");

if (!$database->exec("UPDATE " . $type_database . " SET published='" . $new_value . "' WHERE " . $type_database_identity_column . "='" . $identity . "'"))
  exit ("Unable to toggle the value within the database (programming error).");
  
$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "toggle-publish.php", "NEW_VALUE" => $new_value, "IDENTITY" => $identity);
exit ("success");
?>