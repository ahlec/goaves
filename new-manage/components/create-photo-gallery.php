<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 1, 1) != "1")
  exit ("[Error] In order to create a photo gallery, must be authenticated to the management panel and have the permissions to manage photo galleries.");
  
if (!isset($_GET["title"]))
  exit("[Error] Form values were not passed to the creation script.");
  
$title = $database->escapeString(preg_replace("/(<\?php|<\?)([^\?>]+)(\?>)/","", html_entity_decode($_GET["title"])));
if ($database->querySingle("SELECT count(*) FROM photo_galleries WHERE title LIKE '" . $title . "'") > 0)
  exit("[Error] A photo gallery already exists in the database with this title.");
$handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $title))));
if ($database->querySingle("SELECT count(*) FROM photo_galleries WHERE handle LIKE '" . $handle . "'") > 0)
  exit("[Error] A gallery already exists in the database with this handle.");
  
$related_group = (isset($_GET["related_group"]) ? $database->escapeString($_GET["related_group"]) : null);
$topic = (isset($_GET["topic"]) ? $database->escapeString($_GET["topic"]) : null);
if ($related_group !== null && $database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $related_group . "'") == 0)
  exit("[Error] Related group was provided, but the selected group does not exist within the database.");
if ($topic !== null && $database->querySingle("SELECT count(*) FROM topics WHERE topic_identity='" . $topic . "'") == 0)
  exit("[Error] Topic was provided, but the selected topic does not exist within the database.");

if (!$database->exec("INSERT INTO photo_galleries(staff_identity, handle, title, date_last_updated, published, related_group, date_created, is_complete, topic) VALUES('" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "','" . $handle . "','" . $title .
	"','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "','FALSE','" . $related_group . "','" . date("Y-m-d H:i:s", strtotime("+1 hours")) .
	"','FALSE','" . $topic . "')"))
	exit("[Error] Could not create the photo gallery in the database.");

$gallery_identity = $database->querySingle("SELECT gallery_identity FROM photo_galleries WHERE handle='" . $handle . "' LIMIT 1");
$database->exec("DELETE FROM multimedia_feed WHERE media_type='photo_gallery' AND media_identity='" . $gallery_identity . "'"); // clean up if any required
if (!$database->exec("INSERT INTO multimedia_feed(media_type, media_identity, post_time, post_type, posting_staff_member) VALUES('photo_gallery','" .
	$gallery_identity . "','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "','posted', '" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "')"))
	exit("[Error] Could not add a reference to the new photo gallery in the multimedia feed.");

exit($gallery_identity);
?>