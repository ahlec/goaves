<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);

function composeError($error_message)
{
  echo "<body onload=\"alert('" . $error_message . "');\">";
  exit();
  /*echo "<body onload=\"parent.document.getElementById('gallery-upload-error').innerHTML = '" . $error_message . "';if(parent.document.getElementById(" .
	"'gallery-upload-error').isFading != true) { parent.document.getElementById('gallery-upload-error').style.display = 'block'; parent.fadeOutAndHide(" .
	"'gallery-upload-error', 2400); } parent.document.getElementById('photo-gallery-image-input-" .
  	$upload_identity . "').className = 'inputFile formComponentInvalid';\">\n";
  composeError();*/
}

if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_BEATS, 1) == "0")
  composeError ("[Error] In order to compose a beat, must be authenticated to the management panel and have the permissions to manage beats.");
  
if (!isset($_POST["title"]))
  composeError ("[Error] Did not pass through a title.");
$title = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_POST["title"])), true));
if ($database->querySingle("SELECT count(*) FROM beats WHERE title LIKE '" . $title . "'") > 0)
  composeError ("[Error] Requested title is not a unique title.");

$handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $title))));
if ($database->querySingle("SELECT count(*) FROM beats WHERE handle LIKE '" . $handle . "'") > 0)
  composeError ("[Error] Requested title does not generate a unique handle.");
  
if (!isset($_POST["article"]))
  composeError ("[Error] Did not pass through beat contents.");
$article = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_POST["article"]))));

if (!isset($_POST["image-file"]))
  composeError ("[Error] Did not pass through an image file.");
$image_file = $database->escapeString($_POST["image-file"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/limbo/" . $image_file))
  composeError("[Error] The image file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/limbo/" . $image_file);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/limbo")
  composeError("[Error] The image file led to a file or directory that is outside of the temporary limbo directory.");
$image_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/limbo/" . $image_file);
switch ($image_type)
{
  case IMAGETYPE_GIF: $imagetype = "gif"; break;
  case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
  case IMAGETYPE_PNG: $imagetype = "png"; break;
}
if (!rename(MANAGE_DOCUMENT_ROOT . "/limbo/" . $image_file, DOCUMENT_ROOT . "/images/articles/" . $handle . "." . $imagetype))
  composeError("[Error] Could not move the image to the proper location.");
$image_file = $handle . "." . $imagetype;

if (!isset($_POST["icon-file"]))
  composeError ("[Error] Did not pass through an icon file.");
$icon_file = $database->escapeString($_POST["icon-file"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/limbo/" . $icon_file))
  composeError("[Error] The icon file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/limbo/" . $icon_file);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/limbo")
  composeError("[Error] The icon file led to a file or directory that is outside of the temporary limbo directory.");
$icon_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/limbo/" . $icon_file);
switch ($icon_type)
{
  case IMAGETYPE_GIF: $imagetype = "gif"; break;
  case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
  case IMAGETYPE_PNG: $imagetype = "png"; break;
}
if (!rename(MANAGE_DOCUMENT_ROOT . "/limbo/" . $icon_file, DOCUMENT_ROOT . "/images/articles/icon-" . $handle . "." . $imagetype))
  composeError("[Error] Could not move the icon to the proper location.");
$icon_file = "icon-" . $handle . "." . $imagetype;

if (!isset($_POST["alignment"]))
  composeError ("[Error] Alignment of image was not passed through.");
switch (strtolower($_POST["alignment"]))
{
  case "left": $alignment = "Left"; break;
  case "right": $alignment = "Right"; break;
  default: composeError ("[Error] Invalid image alignment parameter.");
}

if (!isset($_POST["caption"]))
  composeError ("[Error] Image caption was not passed through.");
$image_caption = prepare_content_for_insert(unicode_decode(rawurldecode($_POST["caption"])), true);
if (!in_array(mb_substr($image_caption, -1), array(".", "!", "?")))
  $image_caption .= ".";
$image_caption = $database->escapeString($image_caption);

if (!isset($_POST["type"]))
  composeError ("[Error] Beat type was not passed through.");
$valid_post_categories = explode("|", $database->querySingle("SELECT valid_article_types FROM staff " .
	  	"WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' LIMIT 1"));
if (!in_array($_POST["type"], $valid_post_categories))
  composeError ("[Error] Selected beat type is not a valid type, or you do not have permissions to use the selected beat type.");
$type = $database->escapeString($_POST["type"]);

if (isset($_POST["related-group"]) && $_POST["related-group"] != null && $_POST["related-group"] != "" && $_POST["related-group"] != "null")
{
  if ($database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $database->escapeString($_POST["related-group"])
  	. "'") == 0)
  	composeError ("[Error] Selected group does not exist.");
  $related_group = $database->escapeString($_POST["related-group"]);
}

if (!isset($_POST["photo-credit"]))
  composeError ("[Error] Photo credit was not passed through.");
$photo_credit = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_POST["photo-credit"])), true));

if (isset($_POST["topic"]) && $_POST["topic"] != "null" && $_POST["topic"] != "" && $_POST["topic"] != null)
{
  if ($database->querySingle("SELECT count(*) FROM topics WHERE topic_identity='" . $database->escapeString($_POST["topic"]) . 
  	"' AND ((date_start <= '" .
		date("Y-m-d", strtotime("+1 hours")) . "' AND date_end >= '" . date("Y-m-d", strtotime("+1 hours")) . "') OR (" .
		"date_start IS NULL AND date_end IS NULL))") == 0)
		composeError ("[Error] Selected topic does not exist, or is no longer active for more content.");
  $topic = $database->escapeString($_POST["topic"]);
}

if ($database->exec("INSERT INTO beats(staff_identity, handle, title, post_time, contents, image_file, icon_file, image_alignment, ".
	"image_caption, type, published, last_updated, related_group, photo_credit, topic, parameters) VALUES('" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "','" . $handle . "','" . $title . "','" . 
	date("Y-m-d H:i:s", strtotime("+1 hours")) . "','" . $article . "','" . $image_file . "','" . $icon_file . "','" . $alignment .
	"','" . $image_caption . "','" . $type . "','TRUE','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "'," .
	(isset ($related_group) ? "'" . $related_group . "'" : "NULL") . ",'" . $photo_credit . "'," .
	(isset ($topic) ? "'" . $topic . "'" : "NULL") . ",NULL)"))
	{
	  $article_identity = $database->querySingle("SELECT beat_identity FROM beats WHERE handle LIKE '" .
	  	$handle . "' LIMIT 1");
		UpdateContentStatistic();
     echo "<body onload=\"parent.window.location = '" . MANAGE_WEB_PATH . "/beat/compose/success-" . $article_identity . "/';\">\n";
	 exit();
	} else
	composeError ("[Error] Could not post to database (" . $database->lastErrorMsg() . ")");
composeError ("[Error] Unreachable code has been reached... what?");
?>