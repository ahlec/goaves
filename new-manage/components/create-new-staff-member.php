<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_ADMIN, 1) < "1")
  exit ("[Error] In order to add a staff member, must be authenticated to the management panel and have admin permissions.");
  
if (!isset($_GET["first_name"]) || !isset($_GET["last_name"]) || !isset($_GET["position"]) || !isset($_GET["year_graduating"]))
  exit ("[Error] Not all parameters were passed through.");

$first_name = $_GET["first_name"];
if (mb_strlen($first_name) < 1)
  exit("[Error] First name must be at least one or more characters in length.");
if (strpos($first_name, " ") !== FALSE)
  exit("[Error] First name may not contain spaces.");
$first_name = $database->escapeString($first_name);

$last_name = $_GET["last_name"];
if (mb_strlen($last_name) < 1)
  exit("[Error] Last name must be at least one or more characters in length.");
if (strpos($last_name, " ") !== FALSE)
  exit("[Error] Last name may not contain spaces.");
$last_name = $database->escapeString($last_name);

if ($database->querySingle("SELECT count(*) FROM staff WHERE first_name LIKE '" . $first_name . "' AND last_name LIKE '" . $last_name . "'") > 0)
  exit ("[Error] A staff member with the name '" . $first_name . " " . $last_name . "' already exists in the database.");
  
$positions = $database->escapeString($_GET["position"]);

$year_graduating = $_GET["year_graduating"];
if (strlen($year_graduating) < 4 || !is_numeric($year_graduating))
  exit("[Error] Invalid year for graduation.");

$database->exec("INSERT INTO staff(first_name, last_name, positions, password, display_on_staff_listing, year_graduating) VALUES('" . $first_name . "','" . $last_name . "','" . $positions .
	"','default','TRUE','" . $year_graduating . "')");
exit ("success-" . $database->querySingle("SELECT identity FROM staff WHERE first_name='" . $first_name . "' AND last_name='" . $last_name . "' LIMIT 1"));
?>