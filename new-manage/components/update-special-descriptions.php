<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_SPECIALS, 1) < "1")
  exit ("[Error] In order to update a special, must be authenticated to the management panel and have the permissions to manage specials.");
  
if (!isset($_GET["identity"]))
  exit ("[Error] Not all parameters were passed through.");
$special_identity = $database->escapeString($_GET["identity"]);
if ($database->querySingle("SELECT count(*) FROM specials WHERE special_identity='" . $special_identity . "'") == 0)
  exit ("[Error] The special in question does not exist.");
  
$update_components = array();

if (isset($_GET["short"]))
{
  $short = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_GET["short"]))));
  $update_components[] = "short_description='" . $short . "'";
}
if (isset($_GET["full"]))
{
  $full = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_GET["full"]))));
  $update_components[] = "description='" . $full . "'";
}

if (sizeof($update_components) == 0)
  exit ("[Error] There were no changes from the original special.");
  
$update_query = "UPDATE specials SET ";
foreach ($update_components as $index => $component)
  $update_query .= $component . ($index < sizeof($update_components) - 1 ? "," : "") . " ";
$update_query .= "WHERE special_identity='" . $special_identity . "'";

if (!$database->exec($update_query))
  exit ("[Error] Could not update the special in the database.");
UpdateContentStatistic();

$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "update-special-descriptions", "UPDATED" => true);
exit("success");
?>