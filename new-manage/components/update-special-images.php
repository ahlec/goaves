<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_SPECIALS, 1) < "1")
  exit ("[Error] In order to update a special, must be authenticated to the management panel and have the permissions to manage specials.");
  
if (!isset($_GET["identity"]))
  exit ("[Error] Not all parameters were passed through.");
$special_identity = $database->escapeString($_GET["identity"]);
if ($database->querySingle("SELECT count(*) FROM specials WHERE special_identity='" . $special_identity . "'") == 0)
  exit ("[Error] The special in question does not exist.");
  
$update_components = array();
$handle = $database->querySingle("SELECT handle FROM specials WHERE special_identity='" . $special_identity . "' LIMIT 1");

if (isset($_GET["listing_style"]))
{
  if (strtolower($_GET["listing_style"]) != "vertical" && strtolower($_GET["listing_style"]) != "horizontal")
    exit ("[Error] Invalid listing style type.");
  $update_components[] = "listing_style='" . $database->escapeString($_GET["listing_style"]) . "'";
}

if (isset($_GET["listing_image"]))
{
  $listing_banner = $database->escapeString($_GET["listing_image"]);
  if (!file_exists(MANAGE_DOCUMENT_ROOT . "/limbo/" . $listing_banner))
    exit ("[Error] The listing banner file does not exist within the temporary directory.");
  $filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/limbo/" . $listing_banner);
  if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/limbo")
    exit ("[Error] The listing banner file led to a file or directory that is outside of the temporary limbo directory.");
  $icon_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/limbo/" . $listing_banner);
  switch ($icon_type)
  {
    case IMAGETYPE_GIF: $imagetype = "gif"; break;
    case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
    case IMAGETYPE_PNG: $imagetype = "png"; break;
  }
  $update_components[] = "listing_image='listing-" . $handle . "." . $imagetype . "'";
  $old_listing_image_filename = $database->querySingle("SELECT listing_image FROM specials WHERE special_identity='" . $special_identity . "' LIMIT 1");
  $listing_image_filename = "listing-" . $handle . "." . $imagetype;
}

if (isset($_GET["banner"]))
{
  $page_banner = $database->escapeString($_GET["banner"]);
  if (!file_exists(MANAGE_DOCUMENT_ROOT . "/limbo/" . $page_banner))
    exit ("[Error] The page banner file does not exist within the temporary directory.");
  $filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/limbo/" . $page_banner);
  if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/limbo")
    exit ("[Error] The page banner file led to a file or directory that is outside of the temporary limbo directory.");
  $icon_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/limbo/" . $page_banner);
  switch ($icon_type)
  {
    case IMAGETYPE_GIF: $imagetype = "gif"; break;
    case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
    case IMAGETYPE_PNG: $imagetype = "png"; break;
  }
  $update_components[] = "banner_image='banner-" . $handle . "." . $imagetype . "'";
  $old_banner_filename = $database->querySingle("SELECT banner_image FROM specials WHERE special_identity='" . $special_identity . "' LIMIT 1");
  $banner_filename = "banner-" . $handle . "." . $imagetype;
}

if (sizeof($update_components) == 0)
  exit ("[Error] There were no changes from the original special.");
  
$update_query = "UPDATE specials SET ";
foreach ($update_components as $index => $component)
  $update_query .= $component . ($index < sizeof($update_components) - 1 ? "," : "") . " ";
$update_query .= "WHERE special_identity='" . $special_identity . "'";

if (!$database->exec($update_query))
  exit ("[Error] Could not update the special in the database.");
UpdateContentStatistic();

if (isset($banner_filename))
{
  if (file_exists(DOCUMENT_ROOT . "/images/specials/" . $old_banner_filename) && exif_imagetype(DOCUMENT_ROOT . "/images/specials/" . $old_banner_filename) !== false)
    if (!unlink(DOCUMENT_ROOT . "/images/specials/" . $old_banner_filename))
	  exit ("[Error] Could not remove the old banner image from the specials image directory.");
  if (!rename(MANAGE_DOCUMENT_ROOT . "/limbo/" . $page_banner, DOCUMENT_ROOT . "/images/specials/" . $banner_filename))
    exit("[Error] Could not move the page banner to the proper location.");
}
if (isset($listing_image_filename))
{
  if (file_exists(DOCUMENT_ROOT . "/images/specials/" . $old_listing_image_filename) && exif_imagetype(DOCUMENT_ROOT . "/images/specials/" . $old_listing_image_filename) !== false)
    if (!unlink(DOCUMENT_ROOT . "/images/specials/" . $old_listing_image_filename))
	  exit ("[Error] Could not remove the old list image from the specials image directory.");
  if (!rename(MANAGE_DOCUMENT_ROOT . "/limbo/" . $listing_banner, DOCUMENT_ROOT . "/images/specials/" . $listing_image_filename))
    exit("[Error] Could not move the listing banner to the proper location.");
}

$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "update-special-images", "UPDATED" => true);
exit("success");
?>