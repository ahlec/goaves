<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GROUPS, 1) < "1")
  exit ("[Error] In order to create a subgroup, must be authenticated to the management panel and have the abiltiy to manage groups.");
  
if (!isset($_GET["group"]) || !isset($_GET["title"]) || !isset($_GET["image"]))
  exit ("[Error] Not all parameters were passed through.");
  
if ($database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $database->escapeString($_GET["group"]) . "'") == 0)
  exit ("[Error] Specified parent group does not exist within the database.");
$group_identity = $database->escapeString($_GET["group"]);

$title = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_GET["title"])), true));
if ($database->querySingle("SELECT count(*) FROM group_subgroups WHERE name LIKE '" . $title . "'") > 0)
	exit("[Error] Another subgroup already exists in the database with this title.");
$handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $title))));
if ($database->querySingle("SELECT count(*) FROM group_subgroups WHERE handle LIKE '" . $handle . "'") > 0)
	exit("[Error] Another subgroup already exists in the database with this handle.");

$image = $database->escapeString($_GET["image"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/limbo/" . $image))
  exit ("[Error] The image file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/limbo/" . $image);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/limbo")
  exit ("[Error] The image file led to a file or directory that is outside of the temporary limbo directory.");
$icon_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/limbo/" . $image);
switch ($icon_type)
{
  case IMAGETYPE_GIF: $imagetype = "gif"; break;
  case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
  case IMAGETYPE_PNG: $imagetype = "png"; break;
}
$image_filename = "subgroup-" . $handle . "." . $imagetype;
	
$database->exec("INSERT INTO group_subgroups(group_identity, name, handle, image_file) VALUES('" . $group_identity . "','" . $title . "','" . $handle .
	"','" . $image_filename . "')");
$database->exec("UPDATE groups SET group_last_updated='" . date("Y-m-d H:i:s") . "' WHERE group_identity='" . $group_identity . "'");
if (!rename(MANAGE_DOCUMENT_ROOT . "/limbo/" . $image, DOCUMENT_ROOT . "/images/groups/" . $image_filename))
  exit("[Error] Could not move the subgroup image to the proper location.");
UpdateContentStatistic();

$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "create-subgroup", "IDENTITY" => $database->querySingle("SELECT subgroup_identity FROM group_subgroups WHERE handle='" .
	$handle . "' LIMIT 1"));
exit ("success");
?>