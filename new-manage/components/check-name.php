<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("[Error] You must be authenticated into the management panel in order to access this component.");
if (!isset($_GET["value"]))
  exit ("[Error] Parameters not passed to validation script.");

$value = $_GET["value"];
  
if (mb_strlen($value) < 1)
  exit("[Error] Names must be at least one or more characters in length.");

if (strpos($value, " ") !== FALSE)
  exit("[Error] Names may not contain spaces.");
  
exit($database->escapeString($value)); 
?>