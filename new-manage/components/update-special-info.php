<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_SPECIALS, 1) < "1")
  exit ("[Error] In order to update a special, must be authenticated to the management panel and have the permissions to manage specials.");
  
if (!isset($_GET["identity"]) || !isset($_GET["title"]) || !isset($_GET["active"]))
  exit ("[Error] Not all parameters were passed through.");
$special_identity = $database->escapeString($_GET["identity"]);
if ($database->querySingle("SELECT count(*) FROM specials WHERE special_identity='" . $special_identity . "'") == 0)
  exit ("[Error] The special in question does not exist.");
  
$update_components = array();

$title = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_GET["title"])), true));
if ($database->querySingle("SELECT count(*) FROM specials WHERE title LIKE '" . $title . "' AND special_identity<>'" .
	$special_identity . "'") > 0)
  exit("[Error] Another special already exists in the database with this title.");
$update_components[] = "title='" . $title . "'";
$handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $title))));
if ($database->querySingle("SELECT count(*) FROM specials WHERE handle LIKE '" . $handle . "' AND special_identity<>'" .
	$special_identity . "'") > 0)
  exit("[Error] Another special already exists in the database with this handle.");
$update_components[] = "handle='" . $handle . "'";
  
if (strtolower($_GET["active"]) != "true" && strtolower($_GET["active"]) != "false")
  exit ("[Error] Invalid values for 'active' parameter.");
$active_state = strtoupper($_GET["active"]);
$update_components[] = "active='" . $active_state . "'";

if ($active_state == "FALSE" && !isset($_GET["ended"]))
  exit ("[Error] Not all parameters were passed through.");
if ($active_state == "FALSE")
{
  $ended = $database->escapeString($_GET["ended"]);
  if (strlen($ended) < 10)
    exit ("[Error] End timestamp is not in the proper format.");
  if (!checkdate(substr($ended, 5, 2), substr($ended, 8, 2), substr($ended, 0, 4)))
    exit ("[Error] End timestamp does not represent a valid date.");
  $update_components[] = "ended='" . $ended . "'";
}

if (sizeof($update_components) == 0)
  exit ("[Error] There were no changes from the original special.");
  
$update_query = "UPDATE specials SET ";
foreach ($update_components as $index => $component)
  $update_query .= $component . ($index < sizeof($update_components) - 1 ? "," : "") . " ";
$update_query .= "WHERE special_identity='" . $special_identity . "'";

if (!$database->exec($update_query))
  exit ("[Error] Could not update the special in the database.");
UpdateContentStatistic();

$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "update-special-info", "UPDATED" => true);
exit("success");
?>