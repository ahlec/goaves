<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 0, 1) == "0")
  exit ("In order to upload an image, must be authenticated to the management panel and have the permissions to manage beats.");
  
$upload_status = UploadFile("beat-image");
if (!$upload_status["UPLOAD"])
  exit ($upload_status["MESSAGE"]);

if (!FileIsImage(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]))
  exit ("Uploaded file is not a valid image file.");

$beat_image = new ProcessedImage();
$beat_image->loadOriginal(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]);
$beat_image->scale(300, 400, false);
$new_filename = $beat_image->save(MANAGE_DOCUMENT_ROOT . "/limbo/", substr($upload_status["FILEPATH"], 0, strpos($upload_status["FILEPATH"], ".")));
$beat_image->close();
//$beat_image->deleteOriginalImage();

$image_size = getimagesize(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]);

echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/stylesheet-iframe.css\" />\n";
echo "<body onload=\"parent.document.getElementById('image-input').className = 'inputFile formComponentValid';parent.document.getElementById('" .
	"image-input-textbox').innerHTML = parent.document.getElementById('beat-image').value;parent.document.getElementById('image-filepath').value = '" .
	$new_filename . "';parent.processStartIconCreation('" . $upload_status["FILEPATH"] . "','" . $image_size[0] . "','" . $image_size[1] . "');" .
	"parent.processStepFourConfirmation();\">\n";
//echo "<input type=\"hidden\" id=\"comic-filepath\" value=\"" . $new_filename . "\" />\n";
echo "<div class=\"header\">Uploaded Image</div>\n";
echo "<img src=\"" . MANAGE_WEB_PATH . "/limbo/" . $new_filename . "\" class=\"uploadedComic\" />\n";
?>