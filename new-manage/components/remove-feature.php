<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);

if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to work with featuring content, must be authenticated to the management panel.");

if (!isset($_GET["type"]) || !isset($_GET["identity"]))
  exit ("Not all parameters were passed through.");

switch ($_GET["type"])
{
  case "beat":
  {
    $type_database = "beats";
    $type_database_identity_column = "beat_identity";
	$singular_name = "beat";
	$plural_name = "beats";
    $staff_identity_column = "staff_identity";
    $required_perm = "0";
	$feature_type = "article";
	$handle_column = "handle";
    break;
  }
  case "gallery":
  {
    $type_database = "photo_galleries";
    $type_database_identity_column = "gallery_identity";
    $singular_name = "photo gallery";
	$plural_name = "photo galleries";
    $staff_identity_column = "staff_identity";
    $required_perm = "1";
	$feature_type = "photo_gallery";
	$handle_column = "handle";
    break;
  }
  case "soundslides":
  {
    $type_database = "sound_slides";
    $type_database_identity_column = "slideshow_identity";
    $singular_name = "slideshow presentation";
	$plural_name = "soundslide presentations";
    $staff_identity_column = "staff_identity";
    $required_perm = "5";
	$feature_type = "soundslides";
	$handle_column = "handle";
    break;
  }
  case "special":
  {
    $type_database = "specials";
	$type_database_identity_column = "special_identity";
	$singular_name = "special";
	$plural_name = "specials";
	$staff_identity_column = null;
	$required_perm = PERM_SPECIALS;
	$feature_type = "special";
	$handle_column = "handle";
	break;
  }
  default: exit("Type '" . $content_type . "' is not a valid content type, or featuring is not yet supported for it.");
}
if (substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], $required_perm, 1) != "1")
  exit ("You do not have the proper permissions to manage " . $plural_name . ".");
  
if ($database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE" . ($staff_identity_column != null ? " " . $staff_identity_column . "='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' AND" : "") . " " . $type_database_identity_column . "='" .
	$database->escapeString($_GET["identity"]) . "'") == 0)
	exit ("This " . $singular_name . " does not exist, or is not authored by you.");
$identity = $database->escapeString($_GET["identity"]);

if ($database->querySingle("SELECT count(*) FROM features WHERE feature_type='" . $feature_type . "' AND item_identity='" . $identity . "'") == 0)
  exit ("This " . $singular_name . " is not currently featured.");
  
/*if ($database->querySingle("SELECT count(*) FROM manage_requests LEFT JOIN manage_requests_data ON (manage_requests.request_identity = " .
	"manage_requests_data.request_identity AND manage_requests_data.data_handle='item_identity') WHERE manage_requests.request_type='feature_content' " .
	"AND data='" . $identity . "' AND manage_requests.request_type_supplement='" . $feature_type . "'") > 0)
  exit ("The intended " . $plural_name . " is already awaiting approval to become a featured item.");*/

$header_image = $database->querySingle("SELECT header_image FROM features WHERE feature_type='" . $feature_type . "' AND item_identity='" . $identity . "' LIMIT 1");
if (!unlink(DOCUMENT_ROOT . "/images/features/" . $header_image))
  exit ("Could not remove the header image for the feature on this piece of content.");
if (!$database->exec("DELETE FROM features WHERE feature_type='" . $feature_type . "' AND item_identity='" . $identity . "'"))
 exit ("Could not remove the featured status of this content from the database.");
  
$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "remove-feature.php", "SUCCESS" => true);
exit ("success");
?>