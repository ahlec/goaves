<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_SPECIALS, 1) < "1")
  exit ("[Error] In order to create a special, must be authenticated to the management panel and have the permissions to manage specials.");
  
if (!isset($_GET["title"]) || !isset($_GET["page_banner"]) || !isset($_GET["listing_style"]) || !isset($_GET["listing_image"]))
  exit("[Error] Form values were not passed to the creation script.");
  
$title = $database->escapeString(preg_replace("/(<\?php|<\?)([^\?>]+)(\?>)/","", html_entity_decode($_GET["title"])));
if ($database->querySingle("SELECT count(*) FROM specials WHERE title LIKE '" . $title . "'") > 0)
  exit("[Error] A special already exists in the database with this title.");
$handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $title))));
if ($database->querySingle("SELECT count(*) FROM specials WHERE handle LIKE '" . $handle . "'") > 0)
  exit("[Error] A special already exists in the database with this handle.");
  
$page_banner = $database->escapeString($_GET["page_banner"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/limbo/" . $page_banner))
  exit ("[Error] The page banner file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/limbo/" . $page_banner);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/limbo")
  exit ("[Error] The page banner file led to a file or directory that is outside of the temporary limbo directory.");
$icon_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/limbo/" . $page_banner);
switch ($icon_type)
{
  case IMAGETYPE_GIF: $imagetype = "gif"; break;
  case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
  case IMAGETYPE_PNG: $imagetype = "png"; break;
}
$new_page_banner_filename = "banner-" . $handle . "." . $imagetype;

$listing_style = mb_strtoupper($database->escapeString($_GET["listing_style"]));
if ($listing_style !== "HORIZONTAL" && $listing_style !== "VERTICAL")
  exit ("[Error] Invalid listing style.");
  
$listing_banner = $database->escapeString($_GET["listing_image"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/limbo/" . $listing_banner))
  exit ("[Error] The listing image file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/limbo/" . $listing_banner);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/limbo")
  exit ("[Error] The listing image file led to a file or directory that is outside of the temporary limbo directory.");
$icon_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/limbo/" . $listing_banner);
switch ($icon_type)
{
  case IMAGETYPE_GIF: $imagetype = "gif"; break;
  case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
  case IMAGETYPE_PNG: $imagetype = "png"; break;
}
$new_listing_banner_filename = "listing-" . $handle . "." . $imagetype;
  
$creation = $database->exec("INSERT INTO specials(handle, title, active, listing_image, banner_image, listing_style) VALUES('" .
	$handle . "','" . $title . "','TRUE','" . $new_listing_banner_filename . "','" . $new_page_banner_filename . "','" . $listing_style . "')");
if ($creation !== true && $database->lastErrorCode() > 0)
	exit("[Error] " . $database->lastErrorCode());

if (!rename(MANAGE_DOCUMENT_ROOT . "/limbo/" . $page_banner, DOCUMENT_ROOT . "/images/specials/" . $new_page_banner_filename))
  exit("[Error] Could not move the page banner to the proper location.");
if (!rename(MANAGE_DOCUMENT_ROOT . "/limbo/" . $listing_banner, DOCUMENT_ROOT . "/images/specials/" . $new_listing_banner_filename))
  exit("[Error] Could not move the listing banner to the proper location.");
	
$special_identity = $database->querySingle("SELECT special_identity FROM specials WHERE handle='" . $handle . "' LIMIT 1");
exit ($special_identity);

/*
$database->exec("DELETE FROM multimedia_feed WHERE media_type='podcast' AND media_identity='" . $podcast_identity . "'"); // clean up if any required
if (!$database->exec("INSERT INTO multimedia_feed(media_type, media_identity, post_time, post_type, posting_staff_member) VALUES('podcast','" .
	$podcast_identity . "','" . date("Y-m-d H:i:s") . "','posted', '" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "')"))
	exit("[Error] Could not add a reference to the new podcast in the multimedia feed.");

exit($podcast_identity);*/
?>