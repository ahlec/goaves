<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 0, 1) < "1")
  exit ("[Error] In order to update a beat, must be authenticated to the management panel and have the permissions to manage beats.");
  
if (!isset($_GET["identity"]))
  exit ("[Error] Beat identity was not passed to the update component.");
$beat_identity = $database->escapeString($_GET["identity"]);
if ($database->querySingle("SELECT count(*) FROM beats WHERE beat_identity='" . $beat_identity . "' AND staff_identity='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
	exit ("[Error] The beat in question does not exist, or you are not the owner of it.");
  
$update_components = array();
if (isset($_GET["title"]))
{
  $title = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_GET["title"])), true));
  if ($database->querySingle("SELECT count(*) FROM beats WHERE title LIKE '" . $title . "' AND beat_identity<>'" .
	$beat_identity . "'") > 0)
    exit("[Error] Another beat already exists in the database with this title.");
  $update_components[] = "title='" . $title . "'";
  $handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $title))));
  if ($database->querySingle("SELECT count(*) FROM beats WHERE handle LIKE '" . $handle . "' AND beat_identity<>'" .
	$beat_identity . "'") > 0)
    exit("[Error] Another beat already exists in the database with this handle.");
  $update_components[] = "handle='" . $handle . "'";
}
  
if (!isset($_GET["related_group"]))
  exit ("[Error] No related group information has been passed.");
$related_group = ($_GET["related_group"] != "none" ? $database->escapeString($_GET["related_group"]) : null);
if ($related_group !== null && $database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $related_group . "'") == 0)
  exit("[Error] Related group was provided, but the selected group does not exist within the database.");
$update_components[] = "related_group='" . $related_group . "'";
  
if (!isset($_GET["special"]))
  exit ("[Error] No special information has been passed.");
$special = ($_GET["special"] != "none" ? $database->escapeString($_GET["special"]) : null);
if ($special !== null && $database->querySingle("SELECT count(*) FROM specials WHERE special_identity='" . $special . "'") == 0)
  exit("[Error] Special was provided, but the selected special does not exist within the database.");
$update_components[] = "special_identity='" . $special . "'";
  
if (!isset($_GET["type"]))
  exit ("[Error] Beat category was not passed.");
$category = $database->escapeString($_GET["type"]);
if ($database->querySingle("SELECT count(*) FROM beat_types WHERE type_identity='" . $category . "'") == 0)
  exit ("[Error] The passed beat category does not exist within the database.");
$valid_post_categories = explode("|", $database->querySingle("SELECT valid_article_types FROM staff " .
	  	"WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' LIMIT 1"));
if (!in_array($category, $valid_post_categories))
  composeError ("[Error] You do not have the permissions required to post a beat in this category.");
$update_components[] = "type='" . $category . "'";

if (sizeof($update_components) == 0)
  exit ("[Error] There were no changes from the original beat.");
$update_components[] = "last_updated='" . date("Y-m-d H:i:s") . "'";
  
$update_query = "UPDATE beats SET ";
foreach ($update_components as $index => $component)
  $update_query .= $component . ($index < sizeof($update_components) - 1 ? "," : "") . " ";
$update_query .= "WHERE beat_identity='" . $beat_identity . "'";

if (!$database->exec($update_query))
  exit ("[Error] Could not update the beat in the database.");
UpdateContentStatistic();

$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "update-beat-info", "UPDATED" => true);
exit("success");
?>