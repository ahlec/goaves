<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 4, 1) != "1")
  exit ("[Error] In order to upload a comic, must be authenticated to the management panel and have the permissions to manage comics.");
  
if (!isset($_GET["title"]) || !isset($_GET["filepath"]))
  exit("[Error] Form values were not passed to the upload script.");
  
$title = $database->escapeString(preg_replace("/(<\?php|<\?)([^\?>]+)(\?>)/","", html_entity_decode($_GET["title"])));
if ($database->querySingle("SELECT count(*) FROM cartoons WHERE title LIKE '" . $title . "'") > 0)
  exit("[Error] A comic already exists in the database with this title.");
$handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $title))));
if ($database->querySingle("SELECT count(*) FROM cartoons WHERE handle LIKE '" . $handle . "'") > 0)
  exit("[Error] A comic already exists in the database with this handle.");

$filepath = $_GET["filepath"];
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/limbo/" . $filepath))
  exit("[Error] The uploaded file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/limbo/" . $filepath);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/limbo")
  exit("[Error] The uploaded path led to a file or director that is outside of the temporary limbo directory.");

$image_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/limbo/" . $filepath);
switch ($image_type)
{
  case IMAGETYPE_GIF: $imagetype = "gif"; break;
  case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
  case IMAGETYPE_PNG: $imagetype = "png"; break;
}
if (!rename(MANAGE_DOCUMENT_ROOT . "/limbo/" . $filepath, DOCUMENT_ROOT . "/images/cartoons/" . $handle . "." . $imagetype))
  exit("[Error] Could not move the uploaded image to the proper location.");

if (!$database->exec("INSERT INTO cartoons(artist, image_file, handle, title, date_posted, published) VALUES('" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "','" . $handle . "." . $imagetype . "','" . $handle . "','" . $title .
	"','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "','TRUE')"))
	exit("[Error] Could not add the comic to the database.");

$comic_identity = $database->querySingle("SELECT identity FROM cartoons WHERE handle='" . $handle . "' LIMIT 1");
if (!$database->exec("INSERT INTO multimedia_feed(media_type, media_identity, post_time, post_type, posting_staff_member) VALUES('cartoon','" .
	$comic_identity . "','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "','posted', '" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "')"))
	exit("[Error] Could not add a reference to the new comic in the multimedia feed.");

exit($comic_identity);
?>