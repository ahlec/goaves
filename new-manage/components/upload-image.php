<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);

if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("Must be authenticated to the management panel.");
if (!isset($_GET["aspect"]) || !isset($_GET["cropped_variable"]) || !isset($_GET["original_variable"]) || !isset($_GET["handle"]) ||
	!isset($_GET["crop_width"]) || !isset($_GET["crop_height"]))
  exit ("Not all variables have been passed through.");

$upload_handle = $_GET["handle"];
$upload_status = UploadFile($upload_handle . "-image");
if (!$upload_status["UPLOAD"])
  exit ($upload_status["MESSAGE"]);

if (!FileIsImage(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]))
  exit ("Uploaded file is not a valid image file.");

$image_size = getimagesize(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]);

echo "  <script src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.min.js\"></script>\n";
echo "  <script src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.Jcrop.js\"></script>\n";
echo "  <script src=\"" . WEB_PATH . "/layout/horizons-management/javascript-main.js\"></script>\n";
echo "  <script>
function updateCoords(cropbox)
{
  var image_ratio = (" . $image_size[0] . " > 300 ? " . $image_size[0] . " / 300 : 300 / " . $image_size[0] . ");
  parent.document.getElementById('" . $upload_handle . "-crop-x').value = cropbox.x * image_ratio;
  parent.document.getElementById('" . $upload_handle . "-crop-y').value = cropbox.y * image_ratio;
  parent.document.getElementById('" . $upload_handle . "-crop-width').value = cropbox.w * image_ratio;
  parent.document.getElementById('" . $upload_handle . "-crop-height').value = cropbox.h * image_ratio;
}
function cropImage()
{
  var crop_x = parent.document.getElementById('" . $upload_handle . "-crop-x').value;
  var crop_y = parent.document.getElementById('" . $upload_handle . "-crop-y').value;
  var crop_width = parent.document.getElementById('" . $upload_handle . "-crop-width').value;
  var crop_height = parent.document.getElementById('" . $upload_handle . "-crop-height').value;
  if (crop_x == '')
    crop_x = null;
  if (crop_y == '')
    crop_y = null;
  if (crop_width == '')
    crop_width = null;
  if (crop_height == '')
    crop_height = null;
  if (crop_x == null || crop_y == null || crop_width == null || crop_height == null)
  {
	alert ('Must make a selection on the uploaded image before continuing.');
	return;
  }
  executeAJAX('" . MANAGE_WEB_PATH . "/components/crop-image.php?image=" . $upload_status["FILEPATH"] . "&x=' + crop_x + '&y=' + crop_y +
	'&width=' + crop_width + '&height=' + crop_height + '&crop_width=" . $_GET["crop_width"] . "&crop_height=" . $_GET["crop_height"] . "',
	function process(results)
	{
	  if (results.indexOf('~') === 0)
	  {
	    parent.document.getElementById('" . $upload_handle . "-image-final').src = '" . MANAGE_WEB_PATH . "/limbo/' + results.substring(1);
		parent.document.getElementById('" . $upload_handle . "-image-final').style.display = 'block';
	    parent.document.getElementById('" . $_GET["cropped_variable"] . "').value = results.substring(1);
	    parent.document.getElementById('" . $upload_handle . "-image-crop').style.display = 'none';
		parent.document.getElementById('" . $upload_handle . "-image-input').className = 'inputFile formComponentValid';";
	    if (isset($_GET["callback"]))
		  echo "		parent." . $_GET["callback"] . ";\n";
echo "	  } else
	    alert(results.replace('[Error] ', ''));
	});
}
</script>\n";
echo "<body onload=\"
	parent.document.getElementById('" . $upload_handle . "-image-ratio').value = (" . $image_size[0] . " > 300 ? " . $image_size[0] . " / 300 : 300 / " . $image_size[0] . ");
	parent.document.getElementById('" . $_GET["original_variable"] . "').value = '" . $upload_status["FILEPATH"] . "';
	parent.document.getElementById('" . $upload_handle . "-image-input').className = 'inputFile formComponentInProgress';
	parent.document.getElementById('" . $upload_handle . "-crop-image-filepath').value = '" . $upload_status["FILEPATH"] . "';
	parent.document.getElementById('" . $upload_handle . "-input-textbox').innerHTML = '" . $upload_status["FILEPATH"] . "';
	parent.document.getElementById('" . $upload_handle . "-image-crop').style.display = 'block';
	parent.document.getElementById('crop" . $upload_handle . "Button').onclick = cropImage;
	parent.document.getElementById('" . $upload_handle . "-crop-img').onload = 	function()
	{
	  parent.jQuery(function(){
		parent.jQuery('#" . $upload_handle . "-crop-img').Jcrop({
			" . ($_GET["aspect"] != "false" ? "aspectRatio: " . $_GET["aspect"] . ",\n" : "") . "bgOpacity: 0.4,
			onSelect: updateCoords,
			onChange: updateCoords
		});
	  });
	};
	parent.document.getElementById('" . $upload_handle . "-crop-img').src = '" . MANAGE_WEB_PATH . "/purgatory/" . $upload_status["FILEPATH"] . "';
	\" />\n";

?>