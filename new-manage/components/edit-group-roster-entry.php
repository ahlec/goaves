<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GROUPS, 1) < "1")
  exit ("[Error] In order to update a group, must be authenticated to the management panel and have the permissions to manage groups.");
  
if (!isset($_GET["group_identity"]) || !isset($_GET["entry_identity"]) || !isset($_GET["grade"]) || !isset($_GET["position"]))
  exit ("[Error] Not all parameters were passed to the update component.");
if ($database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $database->escapeString($_GET["group_identity"]) . "'") == 0)
	exit ("[Error] The group in question does not exist.");
$group_identity = $database->escapeString($_GET["group_identity"]);
if (isset($_GET["subgroup_identity"]))
{
  if ($database->querySingle("SELECT count(*) FROM group_subgroups WHERE group_identity='" . $group_identity . "' AND subgroup_identity='" .
	$database->escapeString($_GET["subgroup_identity"]) . "'") == 0)
	  exit ("[Error] Specified subgroup does not exist, or is not a subgroup of the specified group.");
  $subgroup_identity = $database->escapeString($_GET["subgroup_identity"]);
}
if ($database->querySingle("SELECT count(*) FROM group_roster WHERE group_identity='" . $group_identity . "' AND subgroup_identity" .
	(isset($subgroup_identity) ? "='" . $subgroup_identity . "'" : " is null") . " AND roster_entry_identity='" .
	$database->escapeString($_GET["entry_identity"]) . "'") == 0)
	exit ("[Error] The specified roster entry does not exist within the database, or is not attached to the specified group/subgroup.");
$roster_entry_identity = $database->escapeString($_GET["entry_identity"]);
	
$update_components = array();

if (!is_numeric($_GET["grade"]) || $_GET["grade"] < 9 || $_GET["grade"] > 12)
  exit ("[Error] Provided grade was not a valid value.");
$grade = $database->escapeString($_GET["grade"]);
$update_components[] = "grade='" . $grade . "'";

$position = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_GET["position"]))));
$update_components[] = "position='" . $position . "'";
  
$update_query = "UPDATE group_roster SET ";
foreach ($update_components as $index => $component)
  $update_query .= $component . ($index < sizeof($update_components) - 1 ? "," : "") . " ";
$update_query .= "WHERE group_identity='" . $group_identity . "' AND subgroup_identity" . (isset($subgroup_identity) ? "='" .
	$subgroup_identity . "'" : " is null") . " AND roster_entry_identity='" . $roster_entry_identity . "'";

if (!$database->exec($update_query))
  exit ("[Error] Could not update the roster entry in the database.");
$database->exec("UPDATE groups SET group_last_updated='" . date("Y-m-d H:i:s") . "' WHERE group_identity='" . $group_identity . "'");
UpdateContentStatistic();

exit("success");
?>