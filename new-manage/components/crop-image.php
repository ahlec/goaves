<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);

if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("[Error] Must be authenticated to the management panel.");

if (!isset($_GET["image"]) || !isset($_GET["x"]) || !isset($_GET["y"]) || !isset($_GET["width"]) || !isset($_GET["height"]) ||
	!isset($_GET["crop_width"]) || !isset($_GET["crop_height"]))
  exit ("[Error] Not all parameters were passed through.");

$image_file = $database->escapeString($_GET["image"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $image_file))
  composeError("[Error] The image file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $image_file);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/purgatory")
  composeError("[Error] The image file led to a file or directory that is outside of the temporary purgatory directory.");
$image_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $image_file);

if (!is_numeric($_GET["x"]))
  exit ("[Error] '" . $_GET["x"] . "' may not be used as an x-coordinate.");
$x = $_GET["x"];
if (!is_numeric($_GET["y"]))
  exit ("[Error] '" . $_GET["y"] . "' may not be used as a y-coordinate.");
$y = $_GET["y"];
if (!is_numeric($_GET["width"]))
  exit ("[Error] '" . $_GET["width"] . "' may not be used as a width.");
$width = $_GET["width"];
if (!is_numeric($_GET["height"]))
  exit ("[Error] '" . $_GET["height"] . "' may not be used as a height.");
$height = $_GET["height"];
if (!is_numeric($_GET["crop_width"]) && $_GET["crop_width"] !== "")
  exit ("[Error] '" . $_GET["crop_width"] . "' may not be used as a destination width.");
if (!is_numeric($_GET["crop_height"]) && $_GET["crop_height"] !== "")
  exit ("[Error] '" . $_GET["crop_height"] . "' may not be used as a destination height.");
$crop_width = ($_GET["crop_width"] === "" ? ($_GET["crop_height"] / $height) * $width : $_GET["crop_width"]);
$crop_height = ($_GET["crop_height"] === "" ? ($_GET["crop_width"] / $width) * $height : $_GET["crop_height"]);

$cropped_image = new ProcessedImage();
$cropped_image->loadOriginal(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $image_file);
$cropped_image->crop($x, $y, $width, $height, $crop_width, $crop_height, false);
$new_filename = $cropped_image->save(MANAGE_DOCUMENT_ROOT . "/limbo/", (isset($_GET["prefix"]) ? $_GET["prefix"] : "") . "crop-" . $image_file);
$cropped_image->close();
exit ("~" . $new_filename);
?>