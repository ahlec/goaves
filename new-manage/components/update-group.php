<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GROUPS, 1) < "1")
  exit ("[Error] In order to update a group, must be authenticated to the management panel and have the permissions to manage groups.");
  
if (!isset($_GET["identity"]) || !isset($_GET["title"]) || !isset($_GET["type"]) || !isset($_GET["active"]) ||
	!isset($_GET["description"]) || !isset($_GET["mission_statement"]))
  exit ("[Error] Not all parameters were passed to the update component.");
if ($database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $database->escapeString($_GET["identity"]) . "'") == 0)
	exit ("[Error] The group in question does not exist.");
$group_identity = $database->escapeString($_GET["identity"]);
	
$update_components = array();
$title = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_GET["title"])), true));
if ($database->querySingle("SELECT count(*) FROM groups WHERE title LIKE '" . $title . "' AND group_identity<>'" . $group_identity . "'") > 0)
	exit("[Error] Another group already exists in the database with this title.");
$update_components[] = "title='" . $title . "'";
$handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $title))));
if ($database->querySingle("SELECT count(*) FROM groups WHERE handle LIKE '" . $handle . "' AND group_identity<>'" . $group_identity . "'") > 0)
	exit("[Error] Another group already exists in the database with this handle.");
$update_components[] = "handle='" . $handle . "'";

if (strtoupper($_GET["active"]) != "TRUE" && strtoupper($_GET["active"]) != "FALSE")
  exit ("[Error] Invalid value for group active status.");
$active = (strtoupper($_GET["active"]) == "TRUE");
$update_components[] = "active='" . ($active ? "TRUE" : "FALSE") . "'";

if ($database->querySingle("SELECT count(*) FROM group_types WHERE type_identity='" . $database->escapeString($_GET["type"]) . "'") == 0)
  exit ("[Error] Invalid group type.");
$group_type = $database->escapeString($_GET["type"]);
$update_components[] = "type='" . $group_type . "'";

if ($active)
{
  if (!isset($_GET["years_active"]))
    exit ("[Error] Not all parameters were passed to the update component.");
  if (strlen($_GET["years_active"]) == 0)
    exit ("[Error] Years active may not be empty.");
  if (!is_numeric($_GET["years_active"]))
    exit ("[Error] Non-numeric values are not allowed for number of years active.");
  $years_active = $database->escapeString($_GET["years_active"]);
  $update_components[] = "years_active='" . $years_active . "'";
}

$description = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_GET["description"]))));
$update_components[] = "description='" . $description . "'";

$mission_statement = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_GET["mission_statement"]))));
$update_components[] = "mission_statement='" . $mission_statement . "'";

$update_components[] = "group_last_updated='" . date("Y-m-d H:i:s") . "'";
  
$update_query = "UPDATE groups SET ";
foreach ($update_components as $index => $component)
  $update_query .= $component . ($index < sizeof($update_components) - 1 ? "," : "") . " ";
$update_query .= "WHERE group_identity='" . $group_identity . "'";

if (!$database->exec($update_query))
  exit ("[Error] Could not update the group in the database.");
UpdateContentStatistic();

$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "update-group", "UPDATED" => true);
exit("success");
?>