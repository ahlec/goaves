<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GROUPS, 1) < "1")
  exit ("[Error] In order to update a group, must be authenticated to the management panel and have the permissions to manage groups.");
  
if (!isset($_GET["group_identity"]))
  exit ("[Error] Not all parameters were passed to the update component.");
if ($database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $database->escapeString($_GET["group_identity"]) . "'") == 0)
	exit ("[Error] The group in question does not exist.");
$group_identity = $database->escapeString($_GET["group_identity"]);
if (isset($_GET["subgroup_identity"]))
{
  if ($database->querySingle("SELECT count(*) FROM group_subgroups WHERE group_identity='" . $group_identity . "' AND subgroup_identity='" .
	$database->escapeString($_GET["subgroup_identity"]) . "'") == 0)
	  exit ("[Error] Specified subgroup does not exist, or is not a subgroup of the specified group.");
  $subgroup_identity = $database->escapeString($_GET["subgroup_identity"]);
}

if (!$database->exec("DELETE FROM group_roster WHERE group_identity='" . $group_identity . "' AND subgroup_identity" .
	(isset($subgroup_identity) ? "='" . $subgroup_identity . "'" : " is null")))
  exit ("[Error] Could not clear the roster in the database.");
$database->exec("UPDATE groups SET group_last_updated='" . date("Y-m-d H:i:s") . "' WHERE group_identity='" . $group_identity . "'");
UpdateContentStatistic();

exit("success");
?>