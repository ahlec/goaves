<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("[Error] You must be authenticated into the management panel in order to access this component.");
$type = $_GET["type"];
$minimum = $_GET["minimum"];
$title = reformat_url_string($_GET["title"]);

if (isset($_GET["identity"]))
  $identity = $database->escapeString($_GET["identity"]);

switch ($type)
{
  case "podcast": $type_database = "podcasts"; $type_database_identity_column = "podcast_identity"; $type_name = "podcast"; break;
  case "beat": $type_database = "beats"; $type_database_identity_column = "beat_identity"; $type_name = "article"; break;
  case "gallery": $type_database = "photo_galleries"; $type_database_identity_column = "gallery_identity"; $type_name = "photo gallery"; break;
  case "video": $type_database = "videos"; $type_database_identity_column = "video_identity"; $type_name = "video"; break;
  case "comic": $type_database = "cartoons"; $type_database_identity_column = "identity"; $type_name = "comic"; break;
  case "soundslide": $type_database = "sound_slides"; $type_database_identity_column = "slideshow_identity"; $type_name = "sound slideshow"; break;
  case "special": $type_database = "specials"; $type_database_identity_column = "special_identity"; $type_name = "special"; break;
  case "group": $type_database = "groups"; $type_database_identity_column = "group_identity"; $type_name = "group"; break;
  case "subgroup": $type_database = "group_subgroups"; $type_database_identity_column = "subgroup_identity"; $type_name = "subgroup"; break;
  case "poll": $type_database = "polls"; $type_database_identity_column = "identity"; $type_name = "poll"; $type_database_title_column = "poll_question"; break;
  case "poll_response": $type_database ="poll_responses"; $type_database_identity_column = "response_identity"; $type_name = "poll option"; $type_database_title_column = "response"; break;
  default: exit("[Error] Type '" . $type . "' is not a valid content type yet.");
}

if (!isset($type_database_title_column))
  $type_database_title_column = "title";

$desired_title = $database->escapeString(utf8_encode($title));

if (mb_strlen($desired_title) < $minimum)
  exit("[Error] Title must be " . $minimum . " character" . ($minimum != 1 ? "s" : "") . " or more.");

if ($database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE " . $type_database_title_column . " LIKE '" . $desired_title .
	"'" . (isset($identity) ? " AND " . $type_database_identity_column . "<>'" . $identity . "'" : "")) > 0)
  exit("[Error] Another " . $type_name . " already exists with this title.");

exit($desired_title); 
?>