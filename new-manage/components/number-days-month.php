<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);

if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("[Error] Must be authenticated to the management panel.");
  
if (!isset($_GET["month"]) || !isset($_GET["year"]))
  exit ("[Error] Proper variables have not been passed through to validate-date.php");

if (!is_numeric($_GET["year"]))
  exit ("[Error] Year must be four number characters only.");
if (strlen($_GET["year"]) != 4)
  exit ("[Error] [Soft] Year must be four characters");
$year = $database->escapeString($_GET["year"]);

if (!is_numeric($_GET["month"]))
  exit ("[Error] Month must be only number characters.");
if ($_GET["month"] < 1 || $_GET["month"] > 12)
  exit ("[Error] Months must be in numbers within the inclusive range of [1, 12].");
$month = $database->escapeString($_GET["month"]);

exit (date("t", strtotime($year . "-" . $month . "-1")));
?>