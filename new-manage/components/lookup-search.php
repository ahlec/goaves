<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to lookup content, you must be authenticated to the management panel.");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_GET["query"]))
  exit ("Parameters were not passed.");
$search_phrase = utf8_encode(reformat_url_string($_GET["query"]));
if (mb_strlen($_GET["query"]) == 0)
  exit ("Search queries must be at least 1 character in length.");
$raw_search_pieces = explode(" ", $search_phrase);
$search_pieces = array();
foreach ($raw_search_pieces as $piece)
  $search_pieces[] = $database->escapeString($piece);
$results = array();

$staff_permissions = preg_split('//', $_SESSION[MANAGE_SESSION]["PERMISSIONS"]);

/* Staff */
$staff_results = $database->query(createLookupQuery("SELECT identity, first_name, last_name, icon_file, active FROM staff",
	array("=identity", "first_name", "last_name"), $search_pieces));
$resultant_staff_identities = array();
while ($staff_result = $staff_results->fetchArray())
{
  $results[] = array("TYPE" => "staff", "IDENTITY" => $staff_result["identity"], "FIRST_NAME" => $staff_result["first_name"],
	"LAST_NAME" => $staff_result["last_name"], "ICON" => $staff_result["icon_file"], "ACTIVE" => ($staff_result["active"] == "TRUE"),
	"PERMISSION_INDEX" => "11");
  $resultant_staff_identities[] = $staff_result["identity"];
}
$search_pieces_with_staff = $search_pieces;
$search_pieces_with_staff["=staff_identity"] = $resultant_staff_identities;
	
/* Beats */
$beat_results = $database->query(createLookupQuery("SELECT beat_identity, staff_identity, staff.first_name, staff.last_name, handle, title, post_time, beats.icon_file, " .
	"published, last_updated FROM beats JOIN staff ON staff.identity = beats.staff_identity", array("=beat_identity", "=staff_identity", "handle", "title"),
	$search_pieces_with_staff));
while ($beat_result = $beat_results->fetchArray())
  $results[] = array("TYPE" => "beat", "IDENTITY" => $beat_result["beat_identity"], "STAFF_IDENTITY" => $beat_result["staff_identity"], "HANDLE" => $beat_result["handle"],
	"TITLE" => format_content($beat_result["title"]) , "POST_TIME" => strtotime($beat_result["post_time"]), "ICON" => $beat_result["icon_file"],
	"PUBLISHED" => ($beat_result["published"] == "TRUE"), "LAST_UPDATED" => strtotime($beat_result["last_updated"]), "STAFF_FIRST_NAME" => $beat_result["first_name"],
	"STAFF_LAST_NAME" => $beat_result["last_name"], "PERMISSION_INDEX" => "1");
	
/* Photo Galleries */
$gallery_results = $database->query(createLookupQuery("SELECT gallery_identity, staff_identity, handle, title, date_last_updated, published, gallery_image, " .
	"date_created, number_photos, first_name, last_name FROM photo_galleries JOIN staff ON staff.identity = photo_galleries.staff_identity", array("=gallery_identity", "=staff_identity",
	"handle", "title"), $search_pieces_with_staff));
while ($gallery_result = $gallery_results->fetchArray())
  $results[] = array("TYPE" => "gallery", "IDENTITY" => $gallery_result["gallery_identity"], "STAFF_IDENTITY" => $gallery_result["staff_identity"],
	"HANDLE" => $gallery_result["handle"], "TITLE" => format_content($gallery_result["title"]), "LAST_UPDATED" => strtotime($gallery_result["date_last_updated"]),
	"PUBLISHED" => ($gallery_result["published"] == "TRUE"), "ICON" => $gallery_result["gallery_image"], "DATE_CREATED" => strtotime($gallery_result["date_created"]),
	"NUMBER_PHOTOS" => $gallery_result["number_photos"], "STAFF_FIRST_NAME" => $gallery_result["first_name"], "STAFF_LAST_NAME" => $gallery_result["last_name"],
	"PERMISSION_INDEX" => "2");
	
/* Podcasts */
$podcast_results = $database->query(createLookupQuery("SELECT podcast_identity, staff_identity, handle, title, date_posted, published, podcasts.icon_file, " .
	"date_last_updated, related_group, topic, first_name, last_name FROM podcasts JOIN staff ON staff.identity = podcasts.staff_identity", array("=podcast_identity",
	"=staff_identity", "handle", "title"), $search_pieces_with_staff));
while ($podcast_result = $podcast_results->fetchArray())
  $results[] = array("TYPE" => "podcast", "IDENTITY" => $podcast_result["podcast_identity"], "STAFF_IDENTITY" => $podcast_result["staff_identity"],
	"HANDLE" => $podcast_result["handle"], "TITLE" => format_content($podcast_result["title"]), "DATE_POSTED" => strtotime($podcast_result["date_posted"]),
	"PUBLISHED" => ($podcast_result["published"] == "TRUE"), "ICON" => $podcast_result["icon_file"], "LAST_UPDATED" => strtotime($podcast_result["date_last_updated"]),
	"STAFF_FIRST_NAME" => format_content($podcast_result["first_name"]), "STAFF_LAST_NAME" => format_content($podcast_result["last_name"]), "PERMISSION_INDEX" => "3");

/* Comics */
$comics_search_pieces = $search_pieces_with_staff;
$comics_search_pieces["=artist"] = $comics_search_pieces["=staff_identity"];
unset ($comics_search_pieces["=staff_identity"]);
$comic_results = $database->query(createLookupQuery("SELECT cartoons.identity, artist, cartoons.icon_file, handle, title, date_posted, published, " .
	"first_name, last_name FROM cartoons JOIN staff ON staff.identity = cartoons.artist", array("=cartoons.identity",
	"=artist", "handle", "title"), $comics_search_pieces));
while ($comic_result = $comic_results->fetchArray())
  $results[] = array("TYPE" => "comic", "IDENTITY" => $comic_result["identity"], "STAFF_IDENTITY" => $comic_result["artist"],
	"HANDLE" => $comic_result["handle"], "TITLE" => format_content($comic_result["title"]), "DATE_POSTED" => strtotime($comic_result["date_posted"]),
	"PUBLISHED" => ($comic_result["published"] == "TRUE"), "ICON" => $comic_result["icon_file"],
	"STAFF_FIRST_NAME" => format_content($comic_result["first_name"]), "STAFF_LAST_NAME" => format_content($comic_result["last_name"]), "PERMISSION_INDEX" => "5");
	
/* Soundslides */
$soundslides_results = $database->query(createLookupQuery("SELECT slideshow_identity, staff_identity, handle, title, post_date, published, thumbnail_image, " .
	"date_last_updated, first_name, last_name FROM sound_slides JOIN staff ON staff.identity = sound_slides.staff_identity", array("=slideshow_identity",
	"=staff_identity", "handle", "title"), $search_pieces_with_staff));
while ($soundslide_result = $soundslides_results->fetchArray())
  $results[] = array("TYPE" => "soundslide", "IDENTITY" => $soundslide_result["slideshow_identity"], "STAFF_IDENTITY" => $soundslide_result["staff_identity"],
	"HANDLE" => $soundslide_result["handle"], "TITLE" => format_content($soundslide_result["title"]), "DATE_POSTED" => strtotime($soundslide_result["post_date"]),
	"PUBLISHED" => ($soundslide_result["published"] == "TRUE"), "ICON" => $soundslide_result["thumbnail_image"],
	"LAST_UPDATED" => strtotime($soundslide_result["date_last_updated"]),
	"STAFF_FIRST_NAME" => format_content($soundslide_result["first_name"]), "STAFF_LAST_NAME" => format_content($soundslide_result["last_name"]), "PERMISSION_INDEX" => "6");
	
/*---- Output ----*/
if (sizeof($results) > 0)
{
  echo "Your search for '<b>" . $search_phrase . "</b>' has returned <b>" . sizeof($results) . " result" . (sizeof($results) != 1 ? "s" : "") . "</b>.<br /><br />\n";
  foreach ($results as $result)
  {
    $can_edit_this = false;
	if ($staff_permissions[$result["PERMISSION_INDEX"]] == "2")
	  $can_edit_this = true;
	else if ($result["TYPE"] == "staff" && $result["IDENTITY"] == $_SESSION[MANAGE_SESSION]["IDENTITY"])
      $can_edit_this = true;
	else if ($result["TYPE"] != "staff" && $staff_permissions[$result["PERMISSION_INDEX"]] == "1" && $result["STAFF_IDENTITY"] == $_SESSION[MANAGE_SESSION]["IDENTITY"])
	  $can_edit_this = true;
    echo "  <div class=\"resultContainer" . ($can_edit_this ? " canEdit" : "") . "\">\n";
	echo "    <div class=\"identifier\">\n";
	echo "      ";
	switch ($result["TYPE"])
	{
	  case "staff": echo "Staff Member"; break;
	  case "beat": echo "Beat"; break;
	  case "gallery": echo "Photo Gallery"; break;
	  case "podcast": echo "Podcast"; break;
	  case "comic": echo "Comic"; break;
	  case "soundslide": echo "Soundslides"; break;
	}
	echo "<br />\n";
	echo "      <img src=\"" . WEB_PATH . "/images/";
	switch ($result["TYPE"])
	{
	  case "staff": echo "staff"; break;
	  case "beat": echo "articles"; break;
	  case "gallery": echo "photos"; break;
	  case "podcast": echo "podcasts"; break;
	  case "comic": echo "cartoons"; break;
	  case "soundslide": echo "sound_slides"; break;
	}
	echo "/";
	switch ($result["TYPE"])
	{
	  case "staff":
	  case "beat":
	  case "podcast":
	  case "comic":
	  case "soundslide":
	  case "gallery": echo $result["ICON"]; break;
	}
	echo "\" />\n";
	echo "    </div>\n";
	switch ($result["TYPE"])
	{
	  case "staff":
	  {
	    echo "    <div class=\"line\"><b>Name:</b> " . $result["FIRST_NAME"] . " " . $result["LAST_NAME"] . "</div>\n";
		echo "    <div class=\"line\"><b>Identity:</b> " . $result["IDENTITY"] . "</div>\n";
		echo "    <div class=\"line\"><b>Active:</b> " . ($result["ACTIVE"] ? "Yes" : "No") . "</div>\n";
		break;
	  }
	  case "beat":
	  {
	    echo "    <div class=\"line\"><b>Title:</b> " . $result["TITLE"] . "</div>\n";
		echo "    <div class=\"line\"><b>Identity:</b> " . $result["IDENTITY"] . "</div>\n";
		echo "    <div class=\"line\"><b>Author:</b> " . $result["STAFF_FIRST_NAME"] . " " . $result["STAFF_LAST_NAME"] . " (ID #" . $result["STAFF_IDENTITY"] . ")</div>\n";
	    echo "    <div class=\"line\"><b>Posted:</b> " . date(LONG_DATETIME_FORMAT, $result["POST_TIME"]) . "</div>\n";
		echo "    <div class=\"line\"><b>Updated:</b> " . date(LONG_DATETIME_FORMAT, $result["LAST_UPDATED"]) . "</div>\n";
		echo "    <div class=\"line\"><b>Published:</b> " . ($result["PUBLISHED"] ? "Yes" : "No") . "</div>\n";
		break;
	  }
	  case "gallery":
	  {
	    echo "    <div class=\"line\"><b>Title:</b> " . $result["TITLE"] . "</div>\n";
		echo "    <div class=\"line\"><b>Identity:</b> " . $result["IDENTITY"] . "</div>\n";
		echo "    <div class=\"line\"><b>Creator:</b> " . $result["STAFF_FIRST_NAME"] . " " . $result["STAFF_LAST_NAME"] . " (ID #" . $result["STAFF_IDENTITY"] . ")</div>\n";
		echo "    <div class=\"line\"><b>Created:</b> " . date(LONG_DATETIME_FORMAT, $result["DATE_CREATED"]) . "</div>\n";
		echo "    <div class=\"line\"><b>Last Updated:</b> " . date(LONG_DATETIME_FORMAT, $result["LAST_UPDATED"]) . "</div>\n";
		echo "    <div class=\"line\"><b>Photos:</b> " . $result["NUMBER_PHOTOS"] . "</div>\n";
		echo "    <div class=\"line\"><b>Published:</b> " . ($result["PUBLISHED"] ? "Yes" : "No") . "</div>\n";
		break;
	  }
	  case "comic":
	  {
	    echo "    <div class=\"line\"><b>Title:</b> " . $result["TITLE"] . "</div>\n";
		echo "    <div class=\"line\"><b>Identity:</b> " . $result["IDENTITY"] . "</div>\n";
		echo "    <div class=\"line\"><b>Author:</b> " . $result["STAFF_FIRST_NAME"] . " " . $result["STAFF_LAST_NAME"] . " (ID #" . $result["STAFF_IDENTITY"] . ")</div>\n";
	    echo "    <div class=\"line\"><b>Posted:</b> " . date(LONG_DATETIME_FORMAT, $result["DATE_POSTED"]) . "</div>\n";
		echo "    <div class=\"line\"><b>Published:</b> " . ($result["PUBLISHED"] ? "Yes" : "No") . "</div>\n";
		break;
	  }
	  case "soundslide":
	  {
	    echo "    <div class=\"line\"><b>Title:</b> " . $result["TITLE"] . "</div>\n";
		echo "    <div class=\"line\"><b>Identity:</b> " . $result["IDENTITY"] . "</div>\n";
		echo "    <div class=\"line\"><b>Author:</b> " . $result["STAFF_FIRST_NAME"] . " " . $result["STAFF_LAST_NAME"] . " (ID #" . $result["STAFF_IDENTITY"] . ")</div>\n";
	    echo "    <div class=\"line\"><b>Posted:</b> " . date(LONG_DATETIME_FORMAT, $result["DATE_POSTED"]) . "</div>\n";
		echo "    <div class=\"line\"><b>Last Updated:</b> " . date(LONG_DATETIME_FORMAT, $result["LAST_UPDATED"]) . "</div>\n";
		echo "    <div class=\"line\"><b>Published:</b> " . ($result["PUBLISHED"] ? "Yes" : "No") . "</div>\n";
		break;
	  }
	}
	if ($can_edit_this)
	{
	  echo "    <a href=\"" . MANAGE_WEB_PATH . "/";
	  switch ($result["TYPE"])
	  {
	    case "staff": echo "staff"; break;
		case "beat": echo "beat"; break;
		case "gallery": echo "photo-gallery"; break;
		case "podcast": echo "podcast"; break;
		case "comic": echo "comic"; break;
		case "soundslide": echo "soundslide"; break;
	  }
	  echo "/manage/" . $result["IDENTITY"] . "/\" class=\"edit\" target=\"_blank\">Manage</a>\n";
	}
	echo "    <div style=\"clear:both;\"></div>\n";
	echo "  </div>\n";
  }
} else
  echo "<div class=\"noResults\">No results were returned for the search query '<b>" . $search_phrase . "</b>'.\n";
			
/* Functions */
function createLookupQuery($base_query, $search_columns, $search_pieces)
	{
	  $query = $base_query . " WHERE";
	  foreach ($search_pieces as $column_name => $piece)
	  {
	    if (is_int($column_name))
		{
	      foreach ($search_columns as $column)
		  {
		    $exact_match = (substr($column, 0, 1) == "=");
			if ($exact_match)
			  $column = substr($column, 1);
		    $query .= " " . $column . ($exact_match ? "='" . $piece . "'" : " LIKE '%" . $piece . "%'") . " OR";
		  }
		} else
		{
		  $exact_match = (substr($column_name, 0, 1) == "=");
		  if ($exact_match)
		    $column_name = substr($column_name, 1);
		  if (is_array($piece))
		  {
		    foreach ($piece as $piece_piece)
			  $query .= " " . $column_name . ($exact_match ? "='" . $piece_piece . "'" : " LIKE '%" . $piece_piece . "%'") .
				" OR";
		  } else
		    $query .= " " . $column_name . ($exact_match ? "='" . $piece . "'" : " LIKE '%" . $piece . "%'") . " OR";
		}
      }
	  $query = rtrim($query, " OR");
	  return $query;
    }
?>