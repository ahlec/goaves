<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GALLERIES, 1) < "1")
  exit ("[Error] In order to update a photo gallery, must be authenticated to the management panel and have the permissions to manage photo galleries.");
  
if (!isset($_GET["identity"]))
  exit ("[Error] Gallery identity was not passed to the update component.");
$gallery_identity = $database->escapeString($_GET["identity"]);
if ($database->querySingle("SELECT count(*) FROM photo_galleries WHERE gallery_identity='" . $gallery_identity . "'" . (substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"],
	PERM_GALLERIES, 1) == "1" ? " AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'" : "")) == 0)
	exit ("[Error] The gallery in question does not exist, or you are not the owner of it.");
	
$update_components = array();
if (isset($_GET["title"]))
{
  $title = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_GET["title"])), true));
  if ($database->querySingle("SELECT count(*) FROM photo_galleries WHERE title LIKE '" . $title . "' AND gallery_identity<>'" .
	$gallery_identity . "'") > 0)
    exit("[Error] Another photo gallery already exists in the database with this title.");
  $update_components[] = "title='" . $title . "'";
  $handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $title))));
  if ($database->querySingle("SELECT count(*) FROM photo_galleries WHERE handle LIKE '" . $handle . "' AND gallery_identity<>'" .
	$gallery_identity . "'") > 0)
    exit("[Error] Another gallery already exists in the database with this handle.");
  $update_components[] = "handle='" . $handle . "'";
}
  
if (!isset($_GET["related_group"]))
  exit ("[Error] No related group information has been passed.");
$related_group = ($_GET["related_group"] != "none" ? $database->escapeString($_GET["related_group"]) : null);
if ($related_group !== null && $database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $related_group . "'") == 0)
  exit("[Error] Related group was provided, but the selected group does not exist within the database.");
$update_components[] = "related_group='" . $related_group . "'";
  
if (!isset($_GET["special"]))
  exit ("[Error] No special information has been passed.");
$special = ($_GET["special"] != "none" ? $database->escapeString($_GET["special"]) : null);
if ($special !== null && $database->querySingle("SELECT count(*) FROM specials WHERE special_identity='" . $special . "'") == 0)
  exit("[Error] Special was provided, but the selected special does not exist within the database.");
$update_components[] = "special_identity='" . $special . "'";
  
if (isset($_GET["icon"]))
{
  $icon_filename = $database->escapeString($_GET["icon"]);
  if ($database->querySingle("SELECT gallery_image FROM photo_galleries WHERE gallery_identity='" . $gallery_identity . "' LIMIT 1") != $icon_filename)
  {
    if ($database->querySingle("SELECT count(*) FROM photos WHERE gallery_identity='" . $gallery_identity . "' AND icon_file='" . $icon_filename . "'") == 0)
	  exit ("[Error] The icon filename that was specified to become the new gallery image does not exist within this photo gallery.");
	$update_components[] = "gallery_image='" . $icon_filename . "'";
	$update_components[] = "gallery_image_set='TRUE'";
  }
}

if (sizeof($update_components) == 0)
  exit ("[Error] There were no changes from the original photo gallery.");
$update_components[] = "date_last_updated='" . date("Y-m-d H:i:s") . "'";
  
$update_query = "UPDATE photo_galleries SET ";
foreach ($update_components as $index => $component)
  $update_query .= $component . ($index < sizeof($update_components) - 1 ? "," : "") . " ";
$update_query .= "WHERE gallery_identity='" . $gallery_identity . "'";

if (!$database->exec($update_query))
  exit ("[Error] Could not update the photo gallery in the database.");
if ($database->querySingle("SELECT count(*) FROM multimedia_feed WHERE media_type='photo_gallery' AND media_identity='" . $gallery_identity . "' AND post_time LIKE " .
	"'" . date("Y-m-d") . "%' AND post_type='updated'") == 0)
	$database->exec("INSERT INTO multimedia_feed(media_type, media_identity, post_time, post_type, posting_staff_member) VALUES('photo_gallery','" .
		$gallery_identity . "','" . date("Y-m-d H:i:s") . "','updated','" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "')");
$author_identity = $database->querySingle("SELECT staff_identity FROM photo_galleries WHERE gallery_identity='" . $gallery_identity . "' LIMIT 1");
if ($author_identity != $_SESSION[MANAGE_SESSION]["IDENTITY"] || $author_identity == 2)
{
  $message_text = "Changes have been made to your photo gallery entitled \"<b>" .
	utf8_decode($database->escapeString($database->querySingle("SELECT title FROM photo_galleries WHERE gallery_identity='" .
	$gallery_identity . "' LIMIT 1"))) . "</b>\" (Gallery Identity #" . $gallery_identity . ")";
  $message_text = $database->escapeString(encodeMailMessage($message_text));
  $database->exec("INSERT INTO staff_messages(sender_identity, recipient_identity, time_sent, message) VALUES('" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "','" . $author_identity . "','" . date("Y-m-d H:i:s") .
	"','" . $message_text . "')");
}
UpdateContentStatistic();

$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "update-photo-gallery", "UPDATED" => true);
exit("success");
?>