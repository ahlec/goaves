<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);

if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 0, 1) == "0")
  exit ("[Error] In order to create a beat icon, must be authenticated to the management panel and have the " .
  	"permissions to manage beats.");

if (!isset($_GET["image"]) || !isset($_GET["x"]) || !isset($_GET["y"]) || !isset($_GET["width"]) || !isset($_GET["height"]))
  exit ("[Error] Not all parameters were passed through.");

$image_file = $database->escapeString($_GET["image"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $image_file))
  composeError("[Error] The image file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $image_file);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/purgatory")
  composeError("[Error] The image file led to a file or directory that is outside of the temporary limbo directory.");
$image_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $image_file);
switch ($image_type)
{
  case IMAGETYPE_GIF: $imagetype = "gif"; break;
  case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
  case IMAGETYPE_PNG: $imagetype = "png"; break;
}
$image_handle = str_replace("." . $imagetype, "", $image_file);

if (!is_numeric($_GET["x"]))
  exit ("[Error] '" . $_GET["x"] . "' may not be used as an x-coordinate.");
$x = $_GET["x"];
if (!is_numeric($_GET["y"]))
  exit ("[Error] '" . $_GET["y"] . "' may not be used as a y-coordinate.");
$y = $_GET["y"];
if (!is_numeric($_GET["width"]))
  exit ("[Error] '" . $_GET["width"] . "' may not be used as a width.");
$width = $_GET["width"];
if (!is_numeric($_GET["height"]))
  exit ("[Error] '" . $_GET["height"] . "' may not be used as a height.");
$height = $_GET["height"];

//exit ("[Error] {" . $x . "," . $y . "," .$width . "," . $height . "}");

$beat_icon = new ProcessedImage();
$beat_icon->loadOriginal(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $image_file);
$beat_icon->crop($x, $y, $width, $height, 74, 58, false);
$new_filename = $beat_icon->save(MANAGE_DOCUMENT_ROOT . "/limbo/", "icon-" . $image_handle);
$beat_icon->close();
$beat_icon->deleteOriginalImage();
exit ($new_filename);
?>