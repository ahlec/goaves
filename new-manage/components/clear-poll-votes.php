<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_INTERACTIONS, 1) < "1")
  exit ("[Error] You must be authenticated into the management panel in order to access this component and have permissions to manage interactions.");

if (!isset($_GET["scope"]) || !isset($_GET["identity"]))
  exit ("[Error] Parameters were not passed correctly to the component.");

if (strtolower($_GET["scope"]) != "poll" && strtolower($_GET["scope"]) != "response")
  exit ("[Error] Invalid scope for vote clearing.");
  
switch (strtolower($_GET["scope"]))
{
  case "poll":
  {
    if ($database->querySingle("SELECT count(*) FROM polls WHERE identity='" . $database->escapeString($_GET["identity"]) . "'") == 0)
	  exit ("[Error] The specified poll does not exist within the database.");
	if (!$database->exec("DELETE FROM poll_votes WHERE poll_identity='" . $database->escapeString($_GET["identity"]) . "'"))
	  exit ("[Error] Could not delete the poll votes from the database.");
    break;
  }
  case "response":
  {
    if ($database->querySingle("SELECT count(*) FROM poll_responses WHERE response_identity='" . $database->escapeString($_GET["identity"]) . "'") == 0)
	  exit ("[Error] The specified poll response does not exist within the database.");
	if (!$database->exec("DELETE FROM poll_votes WHERE response_identity='" . $database->escapeString($_GET["identity"]) . "'"))
	  exit ("[Error] Could not delete the poll votes from the database.");
    break;
  }
  default: exit ("[Error] Invalid scope for vote clearing.");
}

$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "clear-poll-votes", "SCOPE" => strtolower($_GET["scope"]), "IDENTITY" => $database->escapeString($_GET["identity"]));
exit("success"); 
?>