<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
function abortError($message)
{
  exit("<body onload=\"parent.displayError('" . addslashes($message) . "');\">\n");
}

if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 0, 1) < "1")
  abortError ("In order to update a beat, must be authenticated to the management panel and have the permissions to manage beats.");
  
if (!isset($_GET["identity"]))
  abortError ("Beat identity was not passed to the update component.");
$beat_identity = $database->escapeString($_GET["identity"]);
if ($database->querySingle("SELECT count(*) FROM beats WHERE beat_identity='" . $beat_identity . "'" . (isModeratorOver("1") ? "" :
	" AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'")) == 0)
	abortError ("The beat in question does not exist, or you are not the owner of it.");

$update_components = array();
if (!isset($_POST["article"]))
  abortError ("There was no text passed through.");
$text = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_POST["article"]))));
$length_test = strip_tags($text);
$length_test = preg_replace("/(| |\\n)/", "", $length_test);
//abortError(mb_strlen($length_test));
if (strlen($text) < 40)
  abortError ("Articles must be at least 40 characters in length.");
$update_components[] = "contents='" . $text. "'";

if (sizeof($update_components) == 0)
  abortError ("There were no changes from the original beat.");
$update_components[] = "last_updated='" . date("Y-m-d H:i:s") . "'";
  
$update_query = "UPDATE beats SET ";
foreach ($update_components as $index => $component)
  $update_query .= $component . ($index < sizeof($update_components) - 1 ? "," : "") . " ";
$update_query .= "WHERE beat_identity='" . $beat_identity . "'";

if (!$database->exec($update_query))
  abortError ("Could not update the beat in the database.");
$author_identity = $database->querySingle("SELECT staff_identity FROM beats WHERE beat_identity='" . $beat_identity . "' LIMIT 1");
if ($author_identity != $_SESSION[MANAGE_SESSION]["IDENTITY"] || $author_identity == 2)
{
  $message_text = "Corrections have been made to your beat \"" . utf8_decode($database->escapeString($database->querySingle("SELECT title FROM beats WHERE beat_identity='" .
	$beat_identity . "' LIMIT 1"))) . "\"";
  $message_text = $database->escapeString(encodeMailMessage($message_text));
  $database->exec("INSERT INTO staff_messages(sender_identity, recipient_identity, time_sent, message) VALUES('" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "','" . $author_identity . "','" . date("Y-m-d H:i:s") .
	"','" . $message_text . "')");
}
UpdateContentStatistic();

$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "update-beat-text", "UPDATED" => true);
exit("<body onload=\"parent.window.location='" . MANAGE_WEB_PATH . "/beat/manage/" . $beat_identity . "/';\" />\n");
?>