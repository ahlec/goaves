<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
function abortProcess($error_message)
{
  echo "<body onload=\"alert('" . $error_message . "');parent.document.getElementById('upload-soundslide-form').disabled = false;" .
	"parent.document.getElementById('audio-input').className = 'inputFile formComponentInvalid';\">\n";
  exit();
}
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 2, 1) != "1")
  abortProcess("[Error] In order to upload podcast audio, must be authenticated to the management panel and have the " .
  	"permissions to manage podcasts.");
  
$upload_status = UploadFile("podcast-audio");
if (!$upload_status["UPLOAD"])
  abortProcess ($upload_status["MESSAGE"]);

if (!FileIsAudio(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]))
  abortProcess("[Error] The uploaded file is not a valid audio file (MP3 files only).");
	
echo "<body onload=\"parent.document.getElementById('audio-file-input').className = 'inputFile formComponentValid';parent.document.getElementById('audio-input-textbox" .
	"').innerHTML = parent.document.getElementById('podcast-audio').value;parent.document.getElementById('audio-filepath').value='" .
	$upload_status["FILEPATH"] . "';parent.checkFormValid();\">\n";
?>