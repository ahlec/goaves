<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GALLERIES, 1) < "1")
  exit ("[Error] In order to create a photo gallery, must be authenticated to the management panel and have the " .
  	"permissions to manage photo galleries.");
if (!isset($_GET["identity"]))
  exit("[Error] Must provide a picture identity in order to toggle manage value.");
$target_image = $database->escapeString($_GET["identity"]);
$gallery_identity = $database->querySingle("SELECT gallery_identity FROM photos WHERE picture_identity='" . $target_image . "' LIMIT 1");
if ($gallery_identity === false)
  exit("[Error] The specified picture does not exist.");
if ($database->querySingle("SELECT count(*) FROM photo_galleries WHERE gallery_identity='" . $gallery_identity . "' AND staff_identity='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
  exit("[Error] The specified picture does not exist within a photo gallery, or you are not the owner of the selected photo gallery.");

if (!isset($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$gallery_identity]))
  $_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$gallery_identity] = array();
if (in_array($target_image, $_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$gallery_identity]))
{
  $index = array_search($target_image, $_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$gallery_identity]);
  unset ($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$gallery_identity][$index]);
  exit ("unchecked-" . sizeof($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$gallery_identity]));
} else
{
  $_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$gallery_identity][] = $target_image;
  exit ("checked-" . sizeof($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$gallery_identity]));
}
exit ("[Error] Resulted beyond a point of no return.");
?>