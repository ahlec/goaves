<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
function abortProcess($error_message)
{
  echo "<body onload=\"alert('" . addslashes($error_message) . "');document.getElementById('upload-feature-form').disabled = false;\">\n";
  exit();
}

if (!isset($_SESSION[MANAGE_SESSION]))
  abortProcess("[Error] In order to upload a feature header image, you must be authenticated to the management panel.");
  
$upload_status = UploadFile("feature-image");
if (!$upload_status["UPLOAD"])
  abortProcess ($upload_status["MESSAGE"]);

if (!FileIsImage(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]))
  abortProcess ("[Error] Uploaded file is not a valid image file.");

$image_size = getimagesize(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]);
if ($image_size[0] < 200 || $image_size[1] < 200)
  abortProcess("[Error] The uploaded image must be at least 200x200 pixels.");

echo "  <script src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.min.js\"></script>\n";
echo "  <script src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.Jcrop.js\"></script>\n";
echo "  <script>
function updateFeatureIconCoords(cropbox)
{
  var image_ratio = (" . $image_size[0] . " > 300 ? " . $image_size[0] . " / 300 : 300 / " . $image_size[0] . ");
  parent.document.getElementById('feature-header-crop-x').value = cropbox.x" . ($image_size[0] > 300 ? " * image_ratio" : "") . ";
  parent.document.getElementById('feature-header-crop-y').value = cropbox.y" . ($image_size[0] > 300 ? " * image_ratio" : "") . ";
  parent.document.getElementById('feature-header-crop-width').value = cropbox.w" . ($image_size[0] > 300 ? " * image_ratio" : "") . ";
  parent.document.getElementById('feature-header-crop-height').value = cropbox.h" . ($image_size[0] > 300 ? " * image_ratio" : "") . ";
}
</script>\n";
echo "<body onload=\"
	parent.document.getElementById('feature-image-filepath').value ='" . $upload_status["FILEPATH"] . "';
	parent.document.getElementById('feature-image-input-form').style.display = 'none';
	parent.document.getElementById('feature-image-crop').src = '" . MANAGE_WEB_PATH . "/purgatory/" . $upload_status["FILEPATH"] . "';
	parent.document.getElementById('feature-image-crop').style.display = 'block';
	parent.jQuery(function(){
		parent.jQuery('#feature-image-crop').Jcrop({
			aspectRatio: (7/3),
			bgOpacity: 0.4,
			onSelect: updateFeatureIconCoords,
			onChange: updateFeatureIconCoords
		});
	});
	parent.document.getElementById('feature-submit').disabled = false;\" />\n";
	
/*echo "<body onload=\"parent.featureHeaderImageUploaded('" . $upload_status["FILEPATH"] . "','" . $image_size[0] . "','" .
	$image_size[1] . "');\" />\n";*/
	/*parent.document.getElementById('photo-gallery-image-input-" . $upload_identity . "').className = 'inputFile " .
	"formComponentInProgress';parent.document.getElementById('photo-gallery-image-input-textbox-" . $upload_identity . "').innerHTML = " .
	"parent.document.getElementById('photo-gallery-image-upload-" . $upload_identity . "').value;parent.setupIconCreation('" .
	$new_filename . "','" . $added_image_identity . "','" . $image_size[0] . "','" . $image_size[1] . "');\">\n";*/
?>