<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 5, 1) != "1")
  exit ("[Error] In order to perform this action, must be authenticated to the management panel and have the " .
  	"permissions to manage soundslides.");
if (!isset($_GET["identity"]))
  exit("[Error] Must provide a slide identity in order to toggle manage value.");
  
$target_image = $database->escapeString($_GET["identity"]);
$slideshow_identity = $database->querySingle("SELECT slideshow_identity FROM sound_slides_slides WHERE slide_identity='" . $target_image . "' LIMIT 1");
if ($slideshow_identity === false)
  exit("[Error] The specified slide does not exist.");
if ($database->querySingle("SELECT count(*) FROM sound_slides WHERE slideshow_identity='" . $slideshow_identity . "' AND staff_identity='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
  exit("[Error] The specified slide does not exist within a soundslide presentation, or you are not the author of the selected presentation.");

if (!isset($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$slideshow_identity]))
  $_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$slideshow_identity] = array();
if (in_array($target_image, $_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$slideshow_identity]))
{
  $index = array_search($target_image, $_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$slideshow_identity]);
  unset ($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$slideshow_identity][$index]);
  exit ("unchecked-" . sizeof($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$slideshow_identity]));
} else
{
  $_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$slideshow_identity][] = $target_image;
  exit ("checked-" . sizeof($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$slideshow_identity]));
}
exit ("[Error] Resulted beyond a point of no return.");
?>