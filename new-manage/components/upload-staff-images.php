<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("[Error] Must be authenticated to the management panel.");
  
if (!isset($_GET["original"]) || !isset($_GET["staff_image"]) || !isset($_GET["staff_icon"]) || !isset($_GET["icon_x"]) ||
	!isset($_GET["icon_y"]) || !isset($_GET["icon_width"]) || !isset($_GET["icon_height"]))
  exit("[Error] Form values were not passed to the finalization script.");
  
$staff_identity = $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]);
if ($database->querySingle("SELECT count(*) FROM staff WHERE identity='" . $staff_identity . "'") == 0)
  exit ("[Error] Staff member does not exist within the database.");
$staff_name = $database->querySingle("SELECT first_name, last_name FROM staff WHERE identity='" . $staff_identity . "' LIMIT 1", true);

$existing_images = $database->querySingle("SELECT image_file, icon_file FROM staff WHERE identity='" . $staff_identity . "' LIMIT 1", truE);
if ($existing_images["image_file"] != "" && $existing_images["image_file"] != "no-staff-picture.jpg")
  unlink(DOCUMENT_ROOT . "/images/staff/" . $existing_images["image_file"]);
if ($existing_images["icon_file"] != "" && $existing_images["icon_file"] != "no-staff-icon.jpg")
  unlink(DOCUMENT_ROOT . "/images/staff/" . $existing_images["icon_file"]);
  
$staff_image = $database->escapeString($_GET["staff_image"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/limbo/" . $staff_image))
  exit ("[Error] The staff image file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/limbo/" . $staff_image);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/limbo")
  exit ("[Error] The staff image file led to a file or directory that is outside of the temporary limbo directory.");
$icon_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/limbo/" . $staff_image);
switch ($icon_type)
{
  case IMAGETYPE_GIF: $imagetype = "gif"; break;
  case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
  case IMAGETYPE_PNG: $imagetype = "png"; break;
}
$new_staff_image_name = mb_strtolower($staff_name["first_name"] . "-" . $staff_name["last_name"]) . "." . $imagetype;
  
$staff_icon = $database->escapeString($_GET["staff_icon"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/limbo/" . $staff_icon))
  exit ("[Error] The staff icon file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/limbo/" . $staff_icon);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/limbo")
  exit ("[Error] The staff icon file led to a file or directory that is outside of the temporary limbo directory.");
$icon_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/limbo/" . $staff_icon);
switch ($icon_type)
{
  case IMAGETYPE_GIF: $imagetype = "gif"; break;
  case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
  case IMAGETYPE_PNG: $imagetype = "png"; break;
}
$new_staff_icon_name = mb_strtolower($staff_name["first_name"] . "-" . $staff_name["last_name"]) . "-icon." . $imagetype;

if (!is_numeric($_GET["icon_x"]))
  exit ("[Error] '" . $_GET["icon_x"] . "' may not be used as an x-coordinate.");
$icon_x = $_GET["icon_x"];
if (!is_numeric($_GET["icon_y"]))
  exit ("[Error] '" . $_GET["icon_y"] . "' may not be used as a y-coordinate.");
$icon_y = $_GET["icon_y"];
if (!is_numeric($_GET["icon_width"]))
  exit ("[Error] '" . $_GET["icon_width"] . "' may not be used as a width.");
$icon_width = $_GET["icon_width"];
if (!is_numeric($_GET["icon_height"]))
  exit ("[Error] '" . $_GET["icon_height"] . "' may not be used as a height.");
$icon_height = $_GET["icon_height"];

$updated = $database->exec("UPDATE staff SET image_file='" . $new_staff_image_name . "', icon_file='" . $new_staff_icon_name . "', icon_x='" .
	$icon_x . "', icon_y='" . $icon_y . "', icon_width='" . $icon_width . "', icon_height='" . $icon_height . "' WHERE identity='" . $staff_identity . "'");
if ($updated !== true && $database->lastErrorCode() > 0)
	exit("[Error] " . $database->lastErrorCode());

if (!rename(MANAGE_DOCUMENT_ROOT . "/limbo/" . $staff_image, DOCUMENT_ROOT . "/images/staff/" . $new_staff_image_name))
  exit("[Error] Could not move the staff image to the proper location.");
if (!rename(MANAGE_DOCUMENT_ROOT . "/limbo/" . $staff_icon, DOCUMENT_ROOT . "/images/staff/" . $new_staff_icon_name))
  exit("[Error] Could not move the staff icon to the proper location.");
	
$_SESSION[MANAGE_SESSION]["ICON"] = $new_staff_icon_name;
	
exit ("success");

/*
$database->exec("DELETE FROM multimedia_feed WHERE media_type='podcast' AND media_identity='" . $podcast_identity . "'"); // clean up if any required
if (!$database->exec("INSERT INTO multimedia_feed(media_type, media_identity, post_time, post_type, posting_staff_member) VALUES('podcast','" .
	$podcast_identity . "','" . date("Y-m-d H:i:s") . "','posted', '" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "')"))
	exit("[Error] Could not add a reference to the new podcast in the multimedia feed.");

exit($podcast_identity);*/
?>