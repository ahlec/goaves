<?php
ini_set("session.cookie_domain", "goaves.com");
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);

if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("[Error] In order to work with featuring content, must be authenticated to the management panel.");

if (!isset($_GET["type"]) || !isset($_GET["identity"]) || !isset($_GET["image"]) || !isset($_GET["x"]) || !isset($_GET["y"]) ||
	!isset($_GET["width"]) || !isset($_GET["height"]))
  exit ("[Error] Not all parameters were passed through.");

switch ($_GET["type"])
{
  case "beat":
  {
    $type_database = "beats";
    $type_database_identity_column = "beat_identity";
	$plural_name = "beats";
    $staff_identity_column = "staff_identity";
    $required_perm = PERM_BEATS;
	$feature_type = "article";
	$landing_page = "beat/manage/";
	$handle_column = "handle";
    break;
  }
  case "gallery":
  {
    $type_database = "photo_galleries";
    $type_database_identity_column = "gallery_identity";
    $plural_name = "photo galleries";
    $staff_identity_column = "staff_identity";
    $required_perm = PERM_GALLERIES;
	$feature_type = "photo_gallery";
    $landing_page = "photo-gallery/manage/";
	$handle_column = "handle";
    break;
  }
  case "soundslides":
  {
    $type_database = "sound_slides";
    $type_database_identity_column = "slideshow_identity";
    $plural_name = "soundslides";
    $staff_identity_column = "staff_identity";
    $required_perm = PERM_SOUNDSLIDES;
	$feature_type = "soundslides";
    $landing_page = "soundslide/manage/";
	$handle_column = "handle";
    break;
  }
  case "special":
  {
    $type_database = "specials";
	$type_database_identity_column = "special_identity";
	$plural_name = "specials";
	$staff_identity_column = null;
	$required_perm = PERM_SPECIALS;
	$feature_type = "special";
	$landing_page = "special/manage/";
	$handle_column = "handle";
	break;
  }
  default: exit("[Error] Type '" . $content_type . "' is not a valid content type, or featuring is not yet supported for it.");
}
if (substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], $required_perm, 1) < 1)
  exit ("[Error] You do not have the proper permissions to manage " . $plural_name . ".");
  
if ($database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE" . ($staff_identity_column != null ? " " . $staff_identity_column . "='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' AND" : "") . " " . $type_database_identity_column . "='" .
	$database->escapeString($_GET["identity"]) . "'") == 0)
	exit ("[Error] The intended " . $plural_name . " does not exist, or is not authored by you.");
$identity = $database->escapeString($_GET["identity"]);
$handle = $database->querySingle("SELECT " . $handle_column . " FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" .
	$identity . "' LIMIT 1");
  
if ($database->querySingle("SELECT count(*) FROM features WHERE feature_type='" . $feature_type . "' AND item_identity='" . $identity . "'") > 0)
  exit ("[Error] The intended " . $plural_name . " is already a featured item.");
if ($database->querySingle("SELECT count(*) FROM manage_requests LEFT JOIN manage_requests_data ON (manage_requests.request_identity = " .
	"manage_requests_data.request_identity AND manage_requests_data.data_handle='item_identity') WHERE manage_requests.request_type='feature_content' " .
	"AND data='" . $identity . "' AND manage_requests.request_type_supplement='" . $feature_type . "'") > 0)
  exit ("[Error] The intended " . $plural_name . " is already awaiting approval to become a featured item.");
  
$image_file = $database->escapeString($_GET["image"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $image_file))
  composeError("[Error] The image file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $image_file);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/purgatory")
  composeError("[Error] The image file led to a file or directory that is outside of the temporary limbo directory.");

if (!is_numeric($_GET["x"]))
  exit ("[Error] '" . $_GET["x"] . "' may not be used as an x-coordinate.");
$x = $_GET["x"];
if (!is_numeric($_GET["y"]))
  exit ("[Error] '" . $_GET["y"] . "' may not be used as a y-coordinate.");
$y = $_GET["y"];
if (!is_numeric($_GET["width"]))
  exit ("[Error] '" . $_GET["width"] . "' may not be used as a width.");
$width = $_GET["width"];
if (!is_numeric($_GET["height"]))
  exit ("[Error] '" . $_GET["height"] . "' may not be used as a height.");
$height = $_GET["height"];

$date = (isset($_GET["date"]) ? $_GET["date"] : date("Y-m-d H:i:s"));


$header_image = new ProcessedImage();
$header_image->loadOriginal(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $image_file);
$header_image->crop($x, $y, $width, $height, 700, 300, false);
$new_filename = $header_image->save(DOCUMENT_ROOT . "/images/features/", "header-" . $feature_type . "-" . $handle);
$header_image->close();
$header_image->deleteOriginalImage();

$request_timestamp = date("Y-m-d H:i:s");
if (!$database->exec("INSERT INTO features(feature_type, item_identity, header_image, date_featured) VALUES('" . $feature_type . "','" .
	$identity . "','" . $new_filename . "','" . $request_timestamp . "')"))
	exit ("[Error] Could not create feature content reference within the database.");
$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "set-feature-component.php", "FEATURED" => true);
exit ($landing_page . $identity);
/*

if (!$database->exec("INSERT INTO manage_requests(staff_identity, request_datestamp, request_type, request_type_supplement) VALUES('" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) .
	"','" . $request_timestamp . "','feature_content','" . $feature_type . "')"))
	exit ("[Error] Could not send feature request to the database (Programming error, Err#01");
$manage_request_identity = $database->querySingle("SELECT request_identity FROM manage_requests WHERE staff_identity='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' AND request_datestamp='" . $request_timestamp . "' AND request_type='feature_content' LIMIT 1");
if ($manage_request_identity === false)
  exit ("[Error] Could not retrieve request identity from database (Programming error, Err#02");
if (!$database->exec("INSERT INTO manage_requests_data(request_identity, data_handle, data) VALUES('" . $manage_request_identity . "','feature_type','" .
	$feature_type . "')"))
	exit ("[Error] Could not send feature information to the database (Programming error, Err#03");
if (!$database->exec("INSERT INTO manage_requests_data(request_identity, data_handle, data) VALUES('" . $manage_request_identity . "','item_identity','" .
	$identity . "')"))
	exit ("[Error] Could not send feature information to the database (Programming error, Err#04");
if (!$database->exec("INSERT INTO manage_requests_data(request_identity, data_handle, data) VALUES('" . $manage_request_identity . "','header_image','" .
	$new_filename . "')"))
	exit ("[Error] Could not send feature information to the database (Programming error, Err#05");
if (!$database->exec("INSERT INTO manage_requests_data(request_identity, data_handle, data) VALUES('" . $manage_request_identity . "','feature_date','" .
	$date . "')"))
	exit ("[Error] Could not send feature information to the database (Programming error, Err#06");
$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "set-feature-component.php", "AWAITING" => true);
exit ($landing_page . $identity);*/
?>