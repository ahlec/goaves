<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GROUPS, 1) < "1")
  exit ("[Error] In order to update a group, must be authenticated to the management panel and have the permissions to manage groups.");
  
if (!isset($_GET["group_identity"]) || !isset($_GET["first_name"]) || !isset($_GET["last_name"]) || !isset($_GET["grade"]) || !isset($_GET["position"]) ||
	!isset($_GET["member_number"]))
  exit ("[Error] Not all parameters were passed to the update component.");
if ($database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $database->escapeString($_GET["group_identity"]) . "'") == 0)
	exit ("[Error] The group in question does not exist.");
$group_identity = $database->escapeString($_GET["group_identity"]);
if (isset($_GET["subgroup_identity"]))
{
  if ($database->querySingle("SELECT count(*) FROM group_subgroups WHERE group_identity='" . $group_identity . "' AND subgroup_identity='" .
	$database->escapeString($_GET["subgroup_identity"]) . "'") == 0)
	  exit ("[Error] Specified subgroup does not exist, or is not a subgroup of the specified group.");
  $subgroup_identity = $database->escapeString($_GET["subgroup_identity"]);
}

$first_name = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_GET["first_name"]))));
$last_name = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_GET["last_name"]))));

if ($database->querySingle("SELECT count(*) FROM group_roster WHERE group_identity='" . $group_identity . "' AND subgroup_identity" .
	(isset($subgroup_identity) ? "='" . $subgroup_identity . "'" : " is null") . " AND first_name LIKE '" . $first_name . "' AND last_name LIKE '" .
	$last_name . "'") > 0)
	exit ("[Error] A roster entry already exists with this first and last name in the specified group/subgroup.");

if (!is_numeric($_GET["grade"]) || $_GET["grade"] < 9 || $_GET["grade"] > 12)
  exit ("[Error] Provided grade was not a valid value.");
$grade = $database->escapeString($_GET["grade"]);

if ($_GET["member_number"] != "")
{
  if (!is_numeric($_GET["member_number"]))
    exit ("[Error] Member number was provided, and value was not numeric.");
  $member_number = $database->escapeString($_GET["member_number"]);
} else
  $member_number = "";

$position = ($_GET["position"] != "" ? $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_GET["position"])))) : "");
  
if (!$database->exec("INSERT INTO group_roster(group_identity, subgroup_identity, first_name, last_name, position, member_number, grade) VALUES('" .
	$group_identity . "'," . (isset($subgroup_identity) ? "'" . $subgroup_identity . "'" : "NULL") . ",'" . $first_name . "','" . $last_name . "','" .
	$position . "','" . $member_number . "','" . $grade . "')"))
	exit ("[Error] Could not add the information to the database.");
$database->exec("UPDATE groups SET group_last_updated='" . date("Y-m-d H:i:s") . "' WHERE group_identity='" . $group_identity . "'");
UpdateContentStatistic();

exit($database->querySingle("SELECT roster_entry_identity FROM group_roster WHERE group_identity='" . $group_identity . "' AND subgroup_identity" .
	(isset($subgroup_identity) ? "='" . $subgroup_identity . "'" : " is null") . " AND first_name='" . $first_name . "' AND last_name='" . $last_name .
	"' AND grade='" . $grade . "' LIMIT 1"));
?>