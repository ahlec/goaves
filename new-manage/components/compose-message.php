<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("[Error] In order to send a message, must be authenticated to the management panel.");
  
if (!isset($_GET["to"]) || !isset($_GET["message"]))
  exit ("[Error] Not all parameters were passed through.");
  
$staff_identity = $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]);

if ($database->querySingle("SELECT count(*) FROM staff WHERE identity='" . $database->escapeString($_GET["to"]) . "' AND active='TRUE'") == 0)
  exit ("[Error] Recipient does not exist within the database, or is no longer active (and therefore cannot receive messages).");
$to_identity = $database->escapeString($_GET["to"]);

$message = $database->escapeString(encodeMailMessage(prepare_content_for_insert(unicode_decode(rawurldecode($_GET["message"])))));

if (!$database->exec("INSERT INTO staff_messages(sender_identity, recipient_identity, time_sent, message, read_yet) VALUES('" .
	$staff_identity . "','" . $to_identity . "','" . date("Y-m-d H:i:s") . "','" . $message . "','FALSE')"))
	exit ("[Error] Could not send message through database.");
exit ("success");
?>