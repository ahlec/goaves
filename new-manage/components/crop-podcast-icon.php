<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);

if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 2, 1) != "1")
  exit ("[Error] In order to create a podcast icon, must be authenticated to the management panel and have the " .
  	"permissions to manage podcasts.");

if (!isset($_GET["image"]) || !isset($_GET["x"]) || !isset($_GET["y"]) || !isset($_GET["width"]) || !isset($_GET["height"]))
  exit ("[Error] Not all parameters were passed through.");

$image_file = $database->escapeString($_GET["image"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $image_file))
  composeError("[Error] The image file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $image_file);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/purgatory")
  composeError("[Error] The image file led to a file or directory that is outside of the temporary purgatory directory.");
$image_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $image_file);

if (!is_numeric($_GET["x"]))
  exit ("[Error] '" . $_GET["x"] . "' may not be used as an x-coordinate.");
$x = $_GET["x"];
if (!is_numeric($_GET["y"]))
  exit ("[Error] '" . $_GET["y"] . "' may not be used as a y-coordinate.");
$y = $_GET["y"];
if (!is_numeric($_GET["width"]))
  exit ("[Error] '" . $_GET["width"] . "' may not be used as a width.");
$width = $_GET["width"];
if (!is_numeric($_GET["height"]))
  exit ("[Error] '" . $_GET["height"] . "' may not be used as a height.");
$height = $_GET["height"];

$podcast_icon = new ProcessedImage();
$podcast_icon->loadOriginal(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $image_file);
$podcast_icon->crop($x, $y, $width, $height, 100, 100, false);
$new_filename = $podcast_icon->save(MANAGE_DOCUMENT_ROOT . "/limbo/", "icon-" . $image_file);
$podcast_icon->close();
exit ($new_filename);
?>