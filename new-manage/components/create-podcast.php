<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 2, 1) != "1")
  exit ("[Error] In order to upload a podcast, must be authenticated to the management panel and have the permissions to manage podcasts.");
  
if (!isset($_GET["title"]) || !isset($_GET["audio"]) || !isset($_GET["icon"]) || !isset($_GET["icon_x"]) || !isset($_GET["icon_y"]) || !isset($_GET["icon_width"]) || 
	!isset($_GET["icon_height"]) || !isset($_GET["orig_image"]))
  exit("[Error] Form values were not passed to the creation script.");
  
$title = $database->escapeString(preg_replace("/(<\?php|<\?)([^\?>]+)(\?>)/","", html_entity_decode($_GET["title"])));
if ($database->querySingle("SELECT count(*) FROM podcasts WHERE title LIKE '" . $title . "'") > 0)
  exit("[Error] A podcast already exists in the database with this title.");
$handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $title))));
if ($database->querySingle("SELECT count(*) FROM podcasts WHERE handle LIKE '" . $handle . "'") > 0)
  exit("[Error] A podcast already exists in the database with this handle.");
  
$audio = $database->escapeString($_GET["audio"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $audio))
  exit ("[Error] The uploaded audio file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $audio);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/purgatory")
  exit ("[Error] The audio file led to a file or directory that is outside of the temporary limbo directory.");
if (!FileIsAudio(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $audio))
  exit ("[Error] The uploaded file is not a valid audio file.");
  
$icon_file = $database->escapeString($_GET["icon"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/limbo/" . $icon_file))
  exit ("[Error] The icon file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/limbo/" . $icon_file);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/limbo")
  exit ("[Error] The icon file led to a file or directory that is outside of the temporary limbo directory.");
$icon_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/limbo/" . $icon_file);
switch ($icon_type)
{
  case IMAGETYPE_GIF: $imagetype = "gif"; break;
  case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
  case IMAGETYPE_PNG: $imagetype = "png"; break;
}
$new_icon_filename = "icon-" . $handle . "." . $imagetype;

if (!is_numeric($_GET["icon_x"]))
  exit ("[Error] '" . $_GET["icon_x"] . "' may not be used as an x-coordinate.");
$x = $_GET["icon_x"];
if (!is_numeric($_GET["icon_y"]))
  exit ("[Error] '" . $_GET["icon_y"] . "' may not be used as a y-coordinate.");
$y = $_GET["icon_y"];
if (!is_numeric($_GET["icon_width"]))
  exit ("[Error] '" . $_GET["icon_width"] . "' may not be used as a width.");
$width = $_GET["icon_width"];
if (!is_numeric($_GET["icon_height"]))
  exit ("[Error] '" . $_GET["icon_height"] . "' may not be used as a height.");
$height = $_GET["icon_height"];
  
$related_group = (isset($_GET["related_group"]) ? $database->escapeString($_GET["related_group"]) : null);
$topic = (isset($_GET["topic"]) ? $database->escapeString($_GET["topic"]) : null);
if ($related_group !== null && $database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $related_group . "'") == 0)
  exit("[Error] Related group was provided, but the selected group does not exist within the database.");
if ($topic !== null && $database->querySingle("SELECT count(*) FROM topics WHERE topic_identity='" . $topic . "'") == 0)
  exit("[Error] Topic was provided, but the selected topic does not exist within the database.");
	
$creation = $database->exec("INSERT INTO podcasts(staff_identity, handle, title, date_posted, published, audio_file, icon_file, icon_x, icon_y, icon_width, icon_height, " .
	"date_last_updated, related_group, topic) VALUES('" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "','" .
	$handle . "','" . $title . "','" . date("Y-m-d H:i:s") . "','TRUE','" . ($handle . ".mp3") . "','" . $new_icon_filename . "','" .
	$x . "','" . $y . "','" . $width . "','" . $height . "','" . date("Y-m-d H:i:s") . "','" . $related_group . "','" . $topic . "')");
if ($creation !== true && $database->lastErrorCode() > 0)
	exit("[Error] " . $database->lastErrorCode());

if (!rename(MANAGE_DOCUMENT_ROOT . "/limbo/" . $icon_file, DOCUMENT_ROOT . "/images/podcasts/icon-" . $handle . "." . $imagetype))
  exit("[Error] Could not move the icon to the proper location.");
if (!rename(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $audio, DOCUMENT_ROOT . "/components/podcasts_audio/" . $handle . ".mp3"))
  exit ("[Error] The uploaded file could not be moved to the audio directory.");
	
$podcast_identity = $database->querySingle("SELECT podcast_identity FROM podcasts WHERE handle='" . $handle . "' LIMIT 1");
$orig_image_file = $database->escapeString($_GET["orig_image"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $orig_image_file))
  exit ("[Error] The original image file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $orig_image_file);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/purgatory")
  exit ("[Error] The image file led to a file or directory that is outside of the temporary purgatory directory.");
$orig_icon_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $orig_image_file);
switch ($orig_icon_type)
{
  case IMAGETYPE_GIF: $orig_imagetype = "gif"; break;
  case IMAGETYPE_JPEG: $orig_imagetype = "jpg"; break;
  case IMAGETYPE_PNG: $orig_imagetype = "png"; break;
}
@rename(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $orig_image_file, STORAGE_DOCUMENT_ROOT . "/original_images/podcasts/" . $podcast_identity . "." . $orig_imagetype);
$database->exec("UPDATE podcasts SET original_image_file='" . $podcast_identity . "." . $orig_imagetype . "' WHERE podcast_identity='" .
	$podcast_identity . "'");

$database->exec("DELETE FROM multimedia_feed WHERE media_type='podcast' AND media_identity='" . $podcast_identity . "'"); // clean up if any required
if (!$database->exec("INSERT INTO multimedia_feed(media_type, media_identity, post_time, post_type, posting_staff_member) VALUES('podcast','" .
	$podcast_identity . "','" . date("Y-m-d H:i:s") . "','posted', '" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "')"))
	exit("[Error] Could not add a reference to the new podcast in the multimedia feed.");

exit($podcast_identity);
?>