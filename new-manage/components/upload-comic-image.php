<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 4, 1) != "1")
  exit ("In order to create a comic, must be authenticated to the management panel and have the permissions to manage comics.");
  
$upload_status = UploadFile("comic-image");
if (!$upload_status["UPLOAD"])
  exit ($upload_status["MESSAGE"]);

if (!FileIsImage(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]))
  exit ("Uploaded file is not a valid image file.");

$comic_image = new ProcessedImage();
$comic_image->loadOriginal(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]);
$comic_image->scale(650, 500, false);
$new_filename = $comic_image->save(MANAGE_DOCUMENT_ROOT . "/limbo/", substr($upload_status["FILEPATH"], 0, strpos($upload_status["FILEPATH"], ".")));
$comic_image->close();
$comic_image->deleteOriginalImage();

echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/stylesheet-iframe.css\" />\n";
echo "<body onload=\"parent.document.getElementById('comic-image-input').className = 'inputFile formComponentValid';parent.document.getElementById('" .
	"comic-image-input-textbox').innerHTML = parent.document.getElementById('comic-image').value;parent.document.getElementById('comic-filepath').value = '" .
	$new_filename . "';parent.checkFormValid();\">\n";
//echo "<input type=\"hidden\" id=\"comic-filepath\" value=\"" . $new_filename . "\" />\n";
echo "<div class=\"header\">Uploaded Image</div>\n";
echo "<img src=\"" . MANAGE_WEB_PATH . "/limbo/" . $new_filename . "\" class=\"uploadedComic\" />\n";
?>