<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GROUPS, 1) < 1)
  exit ("[Error] Must be authenticated to the management panel and have permissions to manage groups.");
  
if (!isset($_GET["identity"]) || !isset($_GET["original"]) || !isset($_GET["group_image"]) || !isset($_GET["group_icon"]))
  exit("[Error] Form values were not passed to the finalization script.");
  
if ($database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $database->escapeString($_GET["identity"]) . "'") == 0)
  exit ("[Error] Specified group does not exist within the database.");
$group_identity = $database->escapeString($_GET["identity"]);
$group_info = $database->querySingle("SELECT handle, image_file, icon_file FROM groups WHERE group_identity='" . $group_identity . "' LIMIT 1", true);

if ($group_info["image_file"] != "" && $group_info["image_file"] != "no-group-image.jpg")
  unlink(DOCUMENT_ROOT . "/images/groups/" . $group_info["image_file"]);
if ($group_info["icon_file"] != "" && $group_info["icon_file"] != "no-group-icon.jpg")
  unlink(DOCUMENT_ROOT . "/images/groups/" . $group_info["icon_file"]);
  
$group_image = $database->escapeString($_GET["group_image"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/limbo/" . $group_image))
  exit ("[Error] The group image file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/limbo/" . $group_image);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/limbo")
  exit ("[Error] The group image file led to a file or directory that is outside of the temporary limbo directory.");
$icon_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/limbo/" . $group_image);
switch ($icon_type)
{
  case IMAGETYPE_GIF: $imagetype = "gif"; break;
  case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
  case IMAGETYPE_PNG: $imagetype = "png"; break;
}
$new_group_image_name = $group_info["handle"] . "." . $imagetype;
  
$group_icon = $database->escapeString($_GET["group_icon"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/limbo/" . $group_icon))
  exit ("[Error] The group icon file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/limbo/" . $group_icon);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/limbo")
  exit ("[Error] The group icon file led to a file or directory that is outside of the temporary limbo directory.");
$icon_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/limbo/" . $group_icon);
switch ($icon_type)
{
  case IMAGETYPE_GIF: $imagetype = "gif"; break;
  case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
  case IMAGETYPE_PNG: $imagetype = "png"; break;
}
$new_group_icon_name = $group_info["handle"] . "-icon." . $imagetype;

$updated = $database->exec("UPDATE groups SET image_file='" . $new_group_image_name . "', icon_file='" . $new_group_icon_name . "', " .
	"group_last_updated='" . date("Y-m-d", strtotime("+1 hours")) . "' WHERE group_identity='" .
	$group_identity . "'");
if ($updated !== true && $database->lastErrorCode() > 0)
	exit("[Error] " . $database->lastErrorCode());

if (!rename(MANAGE_DOCUMENT_ROOT . "/limbo/" . $group_image, DOCUMENT_ROOT . "/images/groups/" . $new_group_image_name))
  exit("[Error] Could not move the group image to the proper location.");
if (!rename(MANAGE_DOCUMENT_ROOT . "/limbo/" . $group_icon, DOCUMENT_ROOT . "/images/groups/" . $new_group_icon_name))
  exit("[Error] Could not move the group icon to the proper location.");
		
exit ("success");

/*
$database->exec("DELETE FROM multimedia_feed WHERE media_type='podcast' AND media_identity='" . $podcast_identity . "'"); // clean up if any required
if (!$database->exec("INSERT INTO multimedia_feed(media_type, media_identity, post_time, post_type, posting_staff_member) VALUES('podcast','" .
	$podcast_identity . "','" . date("Y-m-d H:i:s") . "','posted', '" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "')"))
	exit("[Error] Could not add a reference to the new podcast in the multimedia feed.");

exit($podcast_identity);*/
?>