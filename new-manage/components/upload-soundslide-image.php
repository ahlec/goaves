<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
function abortProcess($error_message)
{
  echo "<body onload=\"alert('" . $error_message . "');\">\n";
  /*parent.document.getElementById('gallery-upload-error').innerHTML = '" . $error_message . "';if(parent.document.getElementById(" .
	"'gallery-upload-error').isFading != true) { parent.document.getElementById('gallery-upload-error').style.display = 'block'; parent.fadeOutAndHide(" .
	"'gallery-upload-error', 2400); } parent.document.getElementById('photo-gallery-image-input-" .
  	$upload_identity . "').className = 'inputFile formComponentInvalid';\">\n";*/
  exit();
}

if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 5, 1) != "1")
  abortProcess("[Error] In order to upload a slide image, must be authenticated to the management panel and have the " .
  	"permissions to manage soundslides.");
  
$upload_status = UploadFile("soundslide-image");
if (!$upload_status["UPLOAD"])
  abortProcess ($upload_status["MESSAGE"]);

if (!FileIsImage(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]))
  abortProcess ("[Error] Uploaded file is not a valid image file.");
  
$filename_pieces = explode(".", $upload_status["FILEPATH"]);
$save_filename = $filename_pieces[0] . "." . $filename_pieces[1];
  
$slide_image = new ProcessedImage();
$slide_image->loadOriginal(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]);
$slide_image->scale(500, 500, false);
$new_filename = $slide_image->save(MANAGE_DOCUMENT_ROOT . "/limbo/", $save_filename);
$slide_image->close();
//$slide_image->deleteOriginalImage();

$image_size = getimagesize(MANAGE_DOCUMENT_ROOT . "/limbo/" . $new_filename);
	
echo "<body onload=\"parent.document.getElementById('image-input').className = 'inputFile " .
	"formComponentInProgress';parent.document.getElementById('image-input-textbox').innerHTML = " .
	"parent.document.getElementById('soundslide-image').value;parent.document.getElementById('image-filepath').value='" . $new_filename . "';parent.setupSlideIconCreation('" .
	$new_filename . "','" . $image_size[0] . "','" . $image_size[1] . "');\">\n";
?>