<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 2, 1) != "1")
  exit ("In order to upload a podcast icon, must be authenticated to the management panel and have the permissions to manage podcasts.");
  
$upload_status = UploadFile("podcast-icon");
if (!$upload_status["UPLOAD"])
  exit ($upload_status["MESSAGE"]);

if (!FileIsImage(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]))
  exit ("Uploaded file is not a valid image file.");

$image_size = getimagesize(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]);
echo "  <script src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.min.js\"></script>\n";
echo "  <script src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.Jcrop.js\"></script>\n";
echo "  <script>
function updatePodcastIconCoords(cropbox)
{
  var image_ratio = (" . $image_size[0] . " > 300 ? " . $image_size[0] . " / 300 : 300 / " . $image_size[0] . ");
  parent.document.getElementById('podcast-crop-x').value = cropbox.x" . ($image_size[0] > 300 ? " * image_ratio" : "") . ";
  parent.document.getElementById('podcast-crop-y').value = cropbox.y" . ($image_size[0] > 300 ? " * image_ratio" : "") . ";
  parent.document.getElementById('podcast-crop-width').value = cropbox.w" . ($image_size[0] > 300 ? " * image_ratio" : "") . ";
  parent.document.getElementById('podcast-crop-height').value = cropbox.h" . ($image_size[0] > 300 ? " * image_ratio" : "") . ";
}
</script>\n";
echo "<body onload=\"
	parent.document.getElementById('icon-image-input').className = 'inputFile formComponentInProgress';
	parent.document.getElementById('podcast-crop-image-filepath').value = '" . $upload_status["FILEPATH"] . "';
	parent.document.getElementById('icon-crop-img').src = '" . MANAGE_WEB_PATH . "/purgatory/" . $upload_status["FILEPATH"] . "';
	parent.document.getElementById('icon-crop').style.display = 'block';
	parent.jQuery(function(){
		parent.jQuery('#icon-crop-img').Jcrop({
			aspectRatio: 1,
			bgOpacity: 0.4,
			onSelect: updatePodcastIconCoords,
			onChange: updatePodcastIconCoords
		});
	});\" />\n";

echo "<body onload=\"parent.document.getElementById('comic-image-input').className = 'inputFile formComponentValid';parent.document.getElementById('" .
	"comic-image-input-textbox').innerHTML = parent.document.getElementById('comic-image').value;parent.document.getElementById('comic-filepath').value = '" .
	$new_filename . "';parent.checkFormValid();\">\n";
//echo "<input type=\"hidden\" id=\"comic-filepath\" value=\"" . $new_filename . "\" />\n";
echo "<div class=\"header\">Uploaded Image</div>\n";
echo "<img src=\"" . MANAGE_WEB_PATH . "/limbo/" . $new_filename . "\" class=\"uploadedComic\" />\n";
?>