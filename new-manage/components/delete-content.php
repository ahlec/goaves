<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to load this frame, you must be authenticated to the management panel.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (!isset($_GET["type"]) || !isset($_GET["identity"]))
  exit ("Parameters were not fully passed.");

switch (strtolower($_GET["type"]))
{
  case "podcast":
  {
    $type_database = "podcasts";
    $type_database_identity_column = "podcast_identity";
    $staff_identity_column = "staff_identity";
	$feed_type = "podcast";
	$required_perm = "2";
	$feature_type = "podcast";
    break;
  }
  case "beat":
  {
    $type_database = "beats";
    $type_database_identity_column = "beat_identity";
    $staff_identity_column = "staff_identity";
    $required_perm = "0";
	$feed_type = "beat";
	$feature_type = "article";
    break;
  }
  case "gallery":
  case "photos":
  {
    $type_database = "photo_galleries";
    $type_database_identity_column = "gallery_identity";
    $staff_identity_column = "staff_identity";
    $required_perm = "1";
	$feed_type = "photo_gallery";
    break;
  }
  case "video":
  {
    $type_database = "videos";
    $type_database_identity_column = "video_identity";
    $staff_identity_column = null;
    break;
  }
  case "comic":
  {
    $type_database = "cartoons";
    $type_database_identity_column = "identity";
    $staff_identity_column = "artist";
    $required_perm = "4";
	$feed_type = "cartoon";
	$feature_type = "comic";
    break;
  }
  case "soundslide":
  case "soundslide_slides":
  {
    $type_database = "sound_slides";
    $type_database_identity_column = "slideshow_identity";
    $staff_identity_column = "staff_identity";
	$required_perm = "5";
	$feed_type = "sound_slides";
    break;
  }
  case "special":
  {
    $type_database = "specials";
	$type_database_identity_column = "special_identity";
	$staff_identity_column = null;
	$required_perm = PERM_SPECIALS;
	$feature_type = "special";
	$feed_type = null;
    break;
  }
  case "staff":
  {
    $type_database = "staff";
    $type_database_identity_column = "identity";
	$type_database_title_column = "first_name || ' ' || last_name";
    $staff_identity_column = null;
    $required_perm = PERM_ADMIN;
	$feed_type = null;
	$feature_type = null;
    break;
  }
  case "group":
  {
    $type_database = "groups";
	$type_database_identity_column = "group_identity";
	$type_database_title_column = "title";
	$staff_identity_column = null;
	$required_perm = PERM_GROUPS;
	$feed_type = null;
	$feature_type = null;
	break;
  }
  case "subgroup":
  {
    $type_database = "group_subgroups";
	$type_database_identity_column = "subgroup_identity";
	$type_database_title_column = "name";
	$staff_identity_column = null;
	$required_perm = PERM_GROUPS;
	$feed_type = null;
	$feature_type = null;
	break;
  }
  case "poll":
  {
    $type_database = "polls";
	$type_database_identity_column = "identity";
	$type_database_title_column = "poll_question";
	$staff_identity_column = null;
	$required_perm = PERM_INTERACTIONS;
	$feed_type = null;
	$feature_type = null;
	break;
  }
  case "poll_response":
  {
    $type_database = "poll_responses";
	$type_database_identity_column = "response_identity";
	$type_database_title_column = "response";
	$staff_identity_column = null;
	$required_perm = PERM_INTERACTIONS;
	$feed_type = null;
	$feature_type = null;
	break;
  }
  case "leaf_item":
  {
    $type_database = "leaf_items";
	$type_database_identity_column = "item_identity";
	$type_database_title_column = "item_title";
	$staff_identity_column = null;
	$required_perm = PERM_LEAF;
	$feed_type = null;
	$feature_type = null;
	break;
  }
  default: exit("Type '" . $content_type . "' is not a valid content type yet.");
}

if (substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], $required_perm, 1) != "1" && substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], $required_perm, 1) != "2")
  exit ("You do not have the proper permissions to manage this type of content.");

if (!isset($special_case) || !$special_case)
{
  if ($database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" .
	$database->escapeString($_GET["identity"]) . "'") == 0)
    exit ("Could not find the specified content when looking by identity.");
  $identity = $database->escapeString($_GET["identity"]);

  if ($staff_identity_column !== null && $database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE " .
	$type_database_identity_column . "='" . $identity . "' AND " . $staff_identity_column . "='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
    exit ("You are not the owner of the targeted piece of content.");
}
  
/* Special work section */
if ($_GET["type"] == "staff")
{
  $image_files = $database->querySingle("SELECT image_file, icon_file FROM staff WHERE identity='" . $identity . "'", true);
  if ($image_files["image_file"] != "" && $image_files["image_file"] != "no-staff-picture.jpg")
    @unlink(DOCUMENT_ROOT . "/images/staff/" . $image_files["image_file"]);
  if ($image_files["icon_file"] != "" && $image_files["icon_file"] != "no-staff-icon.jpg")
    @unlink(DOCUMENT_ROOT . "/images/staff/" . $image_files["icon_file"]);
  $database->exec("DELETE FROM staff_email_changes WHERE staff_identity='" . $identity . "'");
  $database->exec("DELETE FROM staff_links WHERE staff_identity='" . $identity . "'");
  $staff_name = $database->querySingle("SELECT first_name || ' ' || last_name FROM staff WHERE identity='" . $identity . "' LIMIT 1");
  $database->exec("UPDATE staff_messages SET sender_identity='" . $staff_name . "' WHERE sender_identity='" . $identity . "'");
  $database->exec("UPDATE staff_messages SET recipient_identity='" . $staff_name . "' WHERE recipient_identity='" . $identity . "'");
  $database->exec("DELETE FROM manage_feedback WHERE target_staff_identity='" . $identity . "'");
  $database->exec("DELETE FROM manage_note_views WHERE staff_identity='" . $identity . "'");
  $manage_requests = $database->query("SELECT request_identity FROM manage_requests WHERE staff_identity='" . $identity . "'");
  while ($request = $manage_requests->fetchArray())
    $database->exec("DELETE FROM manage_requests_data WHERE request_identity='" . $request["request_identity"] . "'");
  $database->exec("DELETE FROM manage_requests WHERE staff_identity='" . $identity . "'");
  $database->exec("UPDATE beats SET staff_identity = null WHERE staff_identity='" . $identity . "'");
  $database->exec("UPDATE cartoons SET artist = null WHERE artist='" . $identity . "'");
  $database->exec("UPDATE infographics SET staff_identity = null WHERE staff_identity='" . $identity . "'");
  $database->exec("UPDATE multimedia_feed SET posting_staff_member=null WHERE posting_staff_member='" . $identity . "'");
  $database->exec("UPDATE pages SET staff_identity=null WHERE staff_identity='" . $identity . "'");
  $database->exec("UPDATE photo_galleries SET staff_identity=null WHERE staff_identity='" . $identity . "'");
  $database->exec("UPDATE podcasts SET staff_identity=null WHERE staff_identity='" . $identity . "'");
  $database->exec("DELETE FROM previous_staff_positions WHERE staff_identity='" . $identity . "'");
  $database->exec("UPDATE sound_slides SET staff_identity=null WHERE staff_identity='" . $identity . "'");
}
else if ($_GET["type"] == "subgroup")
{
  $image_file = $database->querySingle("SELECT image_file FROM group_subgroups WHERE subgroup_identity='" . $identity . "' LIMIT 1");
  if ($image_file != "" && file_exists(DOCUMENT_ROOT . "/images/groups/" . $image_file))
    if (!unlink(DOCUMENT_ROOT . "/images/groups/" . $image_file))
	  exit ("Could not delete the image file for this subgroup.");
  $database->exec("DELETE FROM group_roster WHERE subgroup_identity='" . $identity . "'");
} else if ($_GET["type"] == "group")
{
  $database->exec("DELETE FROM group_roster WHERE group_identity='" . $identity . "'");
  $database->exec("DELETE FROM group_advisors WHERE group_identity='" . $identity . "'");
  $get_subgroups = $database->query("SELECT subgroup_identity, image_file FROM group_subgroups WHERE group_identity='" . $identity . "'");
  while ($subgroup = $get_subgroups->fetchArray())
  {
    if ($subgroup["image_file"] != "" && file_exists(DOCUMENT_ROOT . "/images/groups/" . $subgroup["image_file"]))
	  unlink(DOCUMENT_ROOT . "/images/groups/" . $subgroup["image_file"]);
	$database->exec("DELETE FROM group_subgroups WHERE subgroup_identity='" . $subgroup["subgroup_identity"] . "'");
  }
  $image_files = $database->querySingle("SELECT image_file, icon_file FROM groups WHERE group_identity='". $identity . "' LIMIT 1", true);
  if ($image_files["image_file"] != "" && file_exists(DOCUMENT_ROOT . "/images/groups/" . $image_files["image_file"]))
	unlink(DOCUMENT_ROOT . "/images/groups/" . $image_files["image_file"]);
  if ($image_files["icon_file"] != "" && file_exists(DOCUMENT_ROOT . "/images/groups/" . $image_files["icon_file"]))
	unlink(DOCUMENT_ROOT . "/images/groups/" . $image_files["icon_file"]);
  $database->exec("UPDATE beats SET related_group=null WHERE related_group='" . $identity . "'");
  $database->exec("DELETE FROM calendar WHERE related_group='" . $identity . "'");
  $database->exec("UPDATE cartoons SET related_group=null WHERE related_group='" . $identity . "'");
  $database->exec("UPDATE infographics SET related_group=null WHERE related_group='" . $identity . "'");
  $database->exec("UPDATE leaf_items SET related_group=null WHERE related_group='" . $identity . "'");
  $database->exec("UPDATE pages SET related_group=null WHERE related_group='" . $identity . "'");
  $database->exec("UPDATE photo_galleries SET related_group=null WHERE related_group='" . $identity . "'");
  $database->exec("UPDATE podcasts SET related_group=null WHERE related_group='" . $identity . "'");
  $database->exec("UPDATE sound_slides SET related_group=null WHERE related_group='" . $identity . "'");
  $database->exec("UPDATE videos SET related_group=null WHERE related_group='" . $identity . "'");
} else if ($_GET["type"] == "poll")
{
  $database->exec("DELETE FROM poll_responses WHERE poll_identity='" . $identity . "'");
  $database->exec("DELETE FROM poll_votes WHERE poll_identity='" . $identity . "'");
} else if ($_GET["type"] == "poll_response")
{
  $poll_identity = $database->querySingle("SELECT poll_identity FROM poll_responses WHERE response_identity='" . $identity . "' LIMIT 1");
  if ($database->querySingle("SELECT count(*) FROM poll_responses WHERE poll_identity='" . $poll_identity . "'") == 1)
    exit ("This response cannot be deleted as there would be no responses remaining in the poll, and a poll must have a minimum of one response at all times.");
  $database->exec("DELETE FROM poll_votes WHERE response_identity='" . $identity . "'");
}
if ($_GET["type"] == "gallery")
{
  $all_photos = $database->query("SELECT picture_identity, handle, image_file, icon_file FROM photos WHERE gallery_identity='" .
  	$identity . "'");
  while ($photo = $all_photos->fetchArray())
  {
    if ($photo["image_file"] != "" && !@unlink(DOCUMENT_ROOT . "/images/photos/" . $photo["image_file"]))
      exit ("Could not delete the picture for photo '" . $photo["handle"] . "'.");
    if ($photo["icon_file"] != "" && !@unlink(DOCUMENT_ROOT . "/images/photos/" . $photo["icon_file"]))
      exit ("Could not delete the icon for photo '" . $photo["handle"] . "'.");
    if (!$database->exec("DELETE FROM photos WHERE picture_identity='" . $photo["picture_identity"] . "'"))
      exit ("Image files for photo '" . $photo["handle"] . "' were delete, but could not remove from database.");
  }
  if (isset($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$identity]))
    unset ($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$identity]);
}
if ($_GET["type"] == "photos")
{
  if (!isset($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$identity]))
	exit ("There are no selected photos.");
  $picture_files_deleted = array();
  foreach ($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$identity] as $picture_to_delete)
  {
    if ($database->querySingle("SELECT count(*) FROM photos WHERE picture_identity='" . $database->escapeString($picture_to_delete) . "' AND gallery_identity='" .
		$identity . "'") == 0)
		exit ("One of the pictures selected does not exist within the selected photo gallery.");
	$picture_identity = $database->escapeString($picture_to_delete);
	$photo_information = $database->querySingle("SELECT image_file, icon_file FROM photos WHERE picture_identity='" . $picture_identity . "' LIMIT 1", true);
	if ($photo_information["image_file"] != null && $photo_information["image_file"] != "" && file_exists(DOCUMENT_ROOT . "/images/photos/" . $photo_information["image_file"]))
	  if (!unlink(DOCUMENT_ROOT . "/images/photos/" . $photo_information["image_file"]))
	    exit ("Could not delete file '" . DOCUMENT_ROOT . "/images/photos/" . $photo_information["image_file"] . "'");
	$picture_files_deleted[] = $photo_information["image_file"];
	if ($photo_information["icon_file"] != null && $photo_information["icon_file"] != "" && file_exists(DOCUMENT_ROOT . "/images/photos/" . $photo_information["icon_file"]))
	  if (!unlink(DOCUMENT_ROOT . "/images/photos/" . $photo_information["icon_file"]))
	    exit ("Could not delete file '" . DOCUMENT_ROOT . "/images/photos/" . $photo_information["icon_file"] . "'");
	if (!$database->exec("DELETE FROM photos WHERE picture_identity='" . $picture_identity . "'"))
	  exit ("Could not delete a picture from the database (programming error).");
	$picture_files_deleted[] = $photo_information["icon_file"];
  }
  $number_to_delete = sizeof($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$identity]);
  unset ($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$identity]);
  $gallery_information = $database->querySingle("SELECT gallery_image, gallery_image_set FROM photo_galleries WHERE gallery_identity='" . $identity . "' LIMIT 1", true);
  $update_gallery_query = "UPDATE photo_galleries SET date_last_updated='" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "'";
  if (strlen($gallery_information["gallery_image"]) == 0 || in_array($gallery_information["gallery_image"], $picture_files_deleted))
    $update_gallery_query .= ", gallery_image_set='FALSE', gallery_image='" . $database->querySingle("SELECT icon_file FROM photos WHERE gallery_identity='" .
		$identity . "' ORDER BY picture_identity DESC LIMIT 1") . "'";
  if ($database->querySingle("SELECT count(*) FROM photos WHERE gallery_identity='" . $identity . "'") == 0)
   $update_gallery_query .= ",published='FALSE',is_complete='FALSE'";
  $number_photos = $database->querySingle("SELECT count(*) FROM photos WHERE gallery_identity='" . $identity . "'");
  $update_gallery_query .= ",number_photos='" . $number_photos . "'";
  $update_gallery_query .= " WHERE gallery_identity='" . $identity . "'";
  if (!$database->exec($update_gallery_query))
    exit ("Could not update the photo gallery after the deletion of the photos.");
  
  $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "delete-content.php", "SUCCESS" => true, "NUMBER_DELETED" => $number_to_delete);
  UpdateContentStatistic();
  exit ("success");
}
if ($_GET["type"] == "soundslide_slides")
{
  if (!isset($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$identity]))
	exit ("There are no selected slides.");
  $picture_files_deleted = array();
  foreach ($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$identity] as $slide_to_delete)
  {
    if ($database->querySingle("SELECT count(*) FROM sound_slides_slides WHERE slide_identity='" . $database->escapeString($slide_to_delete) . "' AND slideshow_identity='" .
		$identity . "'") == 0)
		exit ("One of the slides selected does not exist within the selected soundslide presentation.");
	$slide_identity = $database->escapeString($slide_to_delete);
	$slide_information = $database->querySingle("SELECT image_file, icon_file FROM sound_slides_slides WHERE slide_identity='" . $slide_identity . "' LIMIT 1", true);
	if ($slide_information["image_file"] != null && $slide_information["image_file"] != "" && file_exists(DOCUMENT_ROOT . "/images/sound_slides/" . $slide_information["image_file"]))
	  if (!unlink(DOCUMENT_ROOT . "/images/sound_slides/" . $slide_information["image_file"]))
	    exit ("Could not delete file '" . DOCUMENT_ROOT . "/images/sound_slides/" . $slide_information["image_file"] . "'");
	$picture_files_deleted[] = $slide_information["image_file"];
	if ($slide_information["icon_file"] != null && $slide_information["icon_file"] != "" && file_exists(DOCUMENT_ROOT . "/images/sound_slides/" . $slide_information["icon_file"]))
	  if (!unlink(DOCUMENT_ROOT . "/images/sound_slides/" . $slide_information["icon_file"]))
	    exit ("Could not delete file '" . DOCUMENT_ROOT . "/images/sound_slides/" . $slide_information["icon_file"] . "'");
	$picture_files_deleted[] = $slide_information["icon_file"];
	if (!$database->exec("DELETE FROM sound_slides_slides WHERE slide_identity='" . $slide_identity . "'"))
	  exit ("Could not delete a slide from the database (programming error).");
  }
  $number_to_delete = sizeof($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$identity]);
  unset ($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$identity]);
  $slideshow_information = $database->querySingle("SELECT thumbnail_image FROM sound_slides WHERE slideshow_identity='" . $identity . "' LIMIT 1", true);
  $update_slideshow_query = "UPDATE sound_slides SET date_last_updated='" . date("Y-m-d H:i:s") . "'";
  if (strlen($slideshow_information["thumbnail_image"]) == 0 || in_array($slideshow_information["thumbnail_image"], $picture_files_deleted))
    $update_slideshow_query .= ", thumbnail_image='" . $database->querySingle("SELECT icon_file FROM sound_slides_slides WHERE slideshow_identity='" .
		$identity . "' ORDER BY start_time ASC LIMIT 1") . "'";
  if ($database->querySingle("SELECT count(*) FROM sound_slides_slides WHERE slideshow_identity='" . $identity . "'") == 0)
   $update_slideshow_query .= ",published='FALSE'";
  $update_slideshow_query .= " WHERE slideshow_identity='" . $identity . "'";
  if (!$database->exec($update_slideshow_query))
    exit ("Could not update the slideshow after the deletion of the slides.");
  
  $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "delete-content.php", "SUCCESS" => true, "NUMBER_DELETED" => $number_to_delete);
  UpdateContentStatistic();
  exit ("success");
}
if ($_GET["type"] == "beat")
{
  // IMAGE/ICON file
  $image_files = $database->querySingle("SELECT image_file, icon_file FROM beats WHERE beat_identity='" .
	$identity . "' LIMIT 1", true);
  if ($image_files["image_file"] != "")
    if (!unlink(DOCUMENT_ROOT . "/images/articles/" . $image_files["image_file"]))
	  exit ("Could not delete file '" . $image_files["image_file"] . "'");
  if ($image_files["icon_file"] != "")
    if (!unlink(DOCUMENT_ROOT . "/images/articles/" . $image_files["icon_file"]))
	  exit ("Could not delete file '" . $image_files["icon_file"] . "'");
}
if ($_GET["type"] == "soundslide")
{
  $audio_file = $database->querySingle("SELECT sound_file FROM sound_slides WHERE slideshow_identity='" .
	$identity . "' LIMIT 1");
  if ($audio_file != "")
    if (!unlink(DOCUMENT_ROOT . "/components/soundslides_audio/" . $audio_file))
      exit ("Could not remove the audio file");
  $slides = $database->query("SELECT image_file FROM sound_slides_slides WHERE slideshow_identity='" . $identity . "' LIMIT 1");
  while ($slide_image = $slides->fetchArray())
    if (!unlink(DOCUMENT_ROOT . "/images/sound_slides/" . $slide_image["image_file"]))
	  exit ("Could not remove slide image '" . $slide_image["image_file"] . "'");
  if (!$database->exec("DELETE FROM sound_slides_slides WHERE slideshow_identity='" . $identity . "'"))
    exit ("Could not remove the slides from the database.");
}
if ($_GET["type"] == "podcast")
{
  $podcast_files = $database->querySingle("SELECT icon_file, audio_file, original_image_file FROM podcasts WHERE podcast_identity='" .
	$identity . "' LIMIT 1", true);
  if ($podcast_files["icon_file"] != "" && file_exists(DOCUMENT_ROOT . "/images/podcasts/" . $podcast_files["icon_file"]))
    if (!unlink(DOCUMENT_ROOT . "/images/podcasts/" . $podcast_files["icon_file"]))
	  exit ("Could not remove the icon file");
  if ($podcast_files["audio_file"] != "" && file_exists(DOCUMENT_ROOT . "/components/podcasts_audio/" .
	$podcast_files["audio_file"]))
    if (!unlink(DOCUMENT_ROOT . "/components/podcasts_audio/" . $podcast_files["audio_file"]))
	  exit ("Could not remove the audio file");
  if ($podcast_files["original_image_file"] != "" && file_exists(STORAGE_DOCUMENT_ROOT . "/original_images/podcasts/" .
	$podcast_files["original_image_file"]))
	if (!unlink(STORAGE_DOCUMENT_ROOT . "/original_images/podcasts/" . $podcast_files["original_image_file"]))
	  exit ("Could not remove the original image file from storage");
}
if ($_GET["type"] == "comic")
{
  $comic_files = $database->querySingle("SELECT image_file, icon_file FROM cartoons WHERE identity='" . $identity . "' LIMIT 1", true);
  if ($comic_files["image_file"] != "" && file_exists(DOCUMENT_ROOT . "/images/cartoons/" . $comic_files["image_file"]))
    if (!unlink(DOCUMENT_ROOT . "/images/cartoons/" . $comic_files["image_file"]))
	  exit ("Could not remove the image file");
  if ($comic_files["icon_file"] != "" && file_exists(DOCUMENT_ROOT . "/images/cartoons/" . $comic_files["icon_file"]))
    if (!unlink(DOCUMENT_ROOT . "/images/cartoons/" . $comic_files["icon_file"]))
	  exit ("Could not remove the icon file");
}
if ($_GET["type"] == "special")
{
  $banner_files = $database->querySingle("SELECT listing_image, banner_image FROM specials WHERE special_identity='" . $identity . "' LIMIT 1", true);
  if ($banner_files["listing_image"] != "" && file_exists(DOCUMENT_ROOT . "/images/specials/" . $banner_files["listing_image"]))
    if (!unlink(DOCUMENT_ROOT . "/images/specials/" . $banner_files["listing_image"]))
	  exit ("Could not remove the listing image file");
  if ($banner_files["banner_image"] != "" && file_exists(DOCUMENT_ROOT . "/images/specials/" . $banner_files["banner_image"]))
    if (!unlink(DOCUMENT_ROOT . "/images/specials/" . $banner_files["banner_image"]))
	  exit ("Could not remove the banner image file");
	  
  $database->exec("UPDATE beats SET special_identity='' WHERE special_identity='" . $identity . "'");
  $database->exec("UPDATE photo_galleries SET special_identity='' WHERE special_identity='" . $identity . "'");
  $database->exec("UPDATE documents SET special_identity='' WHERE special_identity='" . $identity . "'");
  $database->exec("UPDATE cartoons SET special_identity='' WHERE special_identity='" . $identity . "'");
  $database->exec("UPDATE podcasts SET special_identity='' WHERE special_identity='" . $identity . "'");
  $database->exec("UPDATE sound_slides SET special_identity='' WHERE special_identity='" . $identity . "'");
  $database->exec("UPDATE videos SET special_identity='' WHERE special_identity='" . $identity . "'");
}
/* End special work section */

if (isset($feature_type))
{
  if ($database->querySingle("SELECT count(*) FROM features WHERE feature_type='" . $feature_type . "' AND item_identity='" . $identity . "'") > 0)
  {
    $header_image = $database->querySingle("SELECT header_image FROM features WHERE feature_type='" . $feature_type . "' AND item_identity='" . $identity . "' LIMIT 1");
	if (!unlink(DOCUMENT_ROOT . "/images/features/" . $header_image))
	  exit ("Could not remove the header image for the feature on this piece of content.");
	if (!$database->exec("DELETE FROM features WHERE feature_type='" . $feature_type . "' AND item_identity='" . $identity . "'"))
	  exit ("Could not remove the featured status of this content from the database.");
  }
}

$title = $database->querySingle("SELECT " . (isset($type_database_title_column) ? $type_database_title_column : "title") .
	" FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" .
	$identity . "' LIMIT 1");
if (!$database->exec("DELETE FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" . $identity . "'"))
  exit ("Unable to delete the entry from the database (programming error).");
if ($feed_type != null && !$database->exec("DELETE FROM multimedia_feed WHERE media_type='" . $feed_type . "' AND media_identity='" . $identity . "'"))
  exit ("Unable to delete entries within the multimedia feed.");
  
$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "delete-content.php", "SUCCESS" => true, "TITLE" => $title);
exit ("success");
?>