<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_ADMIN, 1) < "1")
  exit ("[Error] In order to update a special, must be authenticated to the management panel and have admin permissions.");
  
if (!isset($_GET["identity"]) || !isset($_GET["permissions"]))
  exit ("[Error] Not all parameters were passed through.");
$staff_identity = $database->escapeString($_GET["identity"]);
if ($database->querySingle("SELECT count(*) FROM staff WHERE identity='" . $staff_identity. "'") == 0)
  exit ("[Error] Staff is not in the database.");

$permissions_string = $database->escapeString($_GET["permissions"]);  
$database->exec("UPDATE staff SET permissions='" . $permissions_string . "' WHERE identity='" . $staff_identity . "'");
$database->exec("INSERT INTO manage_feedback(target_staff_identity, feedback_handle) VALUES('" . $staff_identity . "','reload_permissions')");

exit("success");
?>