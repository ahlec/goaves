<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 5, 1) != "1")
  exit ("[Error] In order to create a soundslide presentation, must be authenticated to the management panel and have the permissions to manage soundslides.");
  
if (!isset($_GET["title"]) || !isset($_GET["audio"]))
  exit("[Error] Form values were not passed to the creation script.");
  
$title = $database->escapeString(preg_replace("/(<\?php|<\?)([^\?>]+)(\?>)/","", html_entity_decode($_GET["title"])));
if ($database->querySingle("SELECT count(*) FROM sound_slides WHERE title LIKE '" . $title . "'") > 0)
  exit("[Error] A soundslide presentation already exists in the database with this title.");
$handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $title))));
if ($database->querySingle("SELECT count(*) FROM sound_slides WHERE handle LIKE '" . $handle . "'") > 0)
  exit("[Error] A soundslide presentation already exists in the database with this handle.");
  
$audio = $database->escapeString($_GET["audio"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $audio))
  exit ("[Error] The uploaded audio file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $audio);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/purgatory")
  exit ("[Error] The audio file led to a file or directory that is outside of the temporary limbo directory.");
if (!FileIsAudio(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $audio))
  exit ("[Error] The uploaded file is not a valid audio file.");
if (!rename(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $audio, DOCUMENT_ROOT . "/components/soundslides_audio/" . $handle . ".mp3"))
  exit ("[Error] The uploaded file could not be moved to the audio directory.");
  
$related_group = (isset($_GET["related_group"]) ? $database->escapeString($_GET["related_group"]) : null);
$topic = (isset($_GET["topic"]) ? $database->escapeString($_GET["topic"]) : null);
if ($related_group !== null && $database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $related_group . "'") == 0)
  exit("[Error] Related group was provided, but the selected group does not exist within the database.");
if ($topic !== null && $database->querySingle("SELECT count(*) FROM topics WHERE topic_identity='" . $topic . "'") == 0)
  exit("[Error] Topic was provided, but the selected topic does not exist within the database.");

if (!$database->exec("INSERT INTO sound_slides(handle, title, published, post_date, related_group, sound_file, staff_identity, date_last_updated, topic) VALUES('" .
	$handle . "','" . $title . "','FALSE','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "','" . $related_group . "','" . $handle . ".mp3','" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "','" . $topic . "')"))
	exit("[Error] Could not create the soundslide presentation in the database.");

$soundslide_identity = $database->querySingle("SELECT slideshow_identity FROM sound_slides WHERE handle='" . $handle . "' LIMIT 1");
$database->exec("DELETE FROM multimedia_feed WHERE media_type='sound_slides' AND media_identity='" . $soundslide_identity . "'"); // clean up if any required
if (!$database->exec("INSERT INTO multimedia_feed(media_type, media_identity, post_time, post_type, posting_staff_member) VALUES('sound_slides','" .
	$soundslide_identity . "','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "','posted', '" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "')"))
	exit("[Error] Could not add a reference to the new slideshow presentation in the multimedia feed.");

exit($soundslide_identity);
?>