<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 5, 1) != "1")
  exit ("In order to upload a slide, must be authenticated to the management panel and have the permissions to manage soundslides.");
  
if (!isset($_GET["identity"]) || !isset($_GET["image"]) || !isset($_GET["icon"]) || !isset($_GET["x"]) || !isset($_GET["y"]) || !isset($_GET["width"]) ||
	!isset($_GET["height"]) || !isset($_GET["minutes"]) || !isset($_GET["seconds"]))
  exit("Form values were not passed to the upload script.");
  
if ($database->querySingle("SELECT count(*) FROM sound_slides WHERE slideshow_identity='" . $database->escapeString($_GET["identity"]) . "' AND staff_identity='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
	exit ("The soundslide presentation you attempted to modify does not exist, or you are not the author of it.");
$slideshow_identity = $database->escapeString($_GET["identity"]);

$image_file = $database->escapeString($_GET["image"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/limbo/" . $image_file))
  composeError("The image file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/limbo/" . $image_file);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/limbo")
  composeError("The image file led to a file or directory that is outside of the temporary limbo directory.");

$icon_file = $database->escapeString($_GET["icon"]);
if (!file_exists(MANAGE_DOCUMENT_ROOT . "/limbo/" . $icon_file))
  composeError("The icon file does not exist within the temporary directory.");
$filepath_info = pathinfo(MANAGE_DOCUMENT_ROOT . "/limbo/" . $icon_file);
if ($filepath_info["dirname"] != MANAGE_DOCUMENT_ROOT . "/limbo")
  composeError("The icon file led to a file or directory that is outside of the temporary limbo directory.");

if (!is_numeric($_GET["x"]))
  exit ("'" . $_GET["x"] . "' may not be used as an x-coordinate.");
$x = $database->escapeString($_GET["x"]);
if (!is_numeric($_GET["y"]))
  exit ("'" . $_GET["y"] . "' may not be used as a y-coordinate.");
$y = $_GET["y"];
if (!is_numeric($_GET["width"]))
  exit ("'" . $_GET["width"] . "' may not be used as a width.");
$width = $_GET["width"];
if (!is_numeric($_GET["height"]))
  exit ("'" . $_GET["height"] . "' may not be used as a height.");
$height = $_GET["height"];

if (!is_numeric($_GET["minutes"]))
  exit ("'" . $_GET["minutes"] . "' may not be used as a number of minutes to start from.");
$minutes = $_GET["minutes"];
if (!is_numeric($_GET["seconds"]))
  exit ("'" . $_GET["seconds"] . "' may not be used as a number of seconds to start from.");
$seconds = $_GET["seconds"];

$start_time = (($minutes * 60) + $seconds) * 1000;

$next_slide_number = $database->querySingle("SELECT count(*) FROM sound_slides_slides WHERE slideshow_identity='" . $slideshow_identity . "'") + 1;
$image_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/limbo/" . $image_file);
switch ($image_type)
{
  case IMAGETYPE_GIF: $imagetype = "gif"; break;
  case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
  case IMAGETYPE_PNG: $imagetype = "png"; break;
}
if (!rename(MANAGE_DOCUMENT_ROOT . "/limbo/" . $image_file, DOCUMENT_ROOT . "/images/sound_slides/" . $slideshow_identity . "-" . $next_slide_number .
	"." . $imagetype))
  composeError("Could not move the image to the proper location.");
$image_filename = $slideshow_identity . "-" . $next_slide_number . "." . $imagetype;
  
$icon_type = exif_imagetype(MANAGE_DOCUMENT_ROOT . "/limbo/" . $icon_file);
switch ($icon_type)
{
  case IMAGETYPE_GIF: $icontype = "gif"; break;
  case IMAGETYPE_JPEG: $icontype = "jpg"; break;
  case IMAGETYPE_PNG: $icontype = "png"; break;
}
if (!rename(MANAGE_DOCUMENT_ROOT . "/limbo/" . $icon_file, DOCUMENT_ROOT . "/images/sound_slides/icon-" . $slideshow_identity . "-" . $next_slide_number .
	"." . $icontype))
  composeError("Could not move the icon to the proper location.");
$icon_filename = "icon-" . $image_filename;
	
if (!$database->exec("INSERT INTO sound_slides_slides(slideshow_identity, image_file, start_time, caption, icon_file, crop_x, crop_y, crop_width, " .
	"crop_height) VALUES('" . $slideshow_identity . "','" . $image_filename . "','" . $start_time . "',NULL,'" . $icon_filename . "','" . $x .
	"','" . $y . "','" . $width . "','" . $height . "')"))
	exit("Could not add the slide to the database.");

if ($database->querySingle("SELECT count(*) FROM multimedia_feed WHERE media_type='sound_slides' AND media_identity='" . $slideshow_identity .
	"' AND post_time LIKE '" . date("Y-m-d") . "%'") == 0)
	{
	  $database->exec("INSERT INTO multimedia_feed(media_type, media_identity, post_time, post_type, posting_staff_member) VALUES('sound_slides','" .
		$slideshow_identity . "','" . date("Y-m-d H:i:s") . "','has new slides', '" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "')");
	}
$earliest_slide_icon = $database->querySingle("SELECT icon_file FROM sound_slides_slides WHERE slideshow_identity='" . $slideshow_identity .
	"' ORDER BY start_time ASC LIMIT 1");
$database->exec("UPDATE sound_slides SET thumbnail_image='" . $earliest_slide_icon . "', date_last_updated='" . date("Y-m-d H:i:s") .
	"' WHERE slideshow_identity='" . $slideshow_identity . "'");
UpdateContentStatistic();
$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "upload-soundslide-slide.php", "SUCCESS" => true);
exit("success");
?>