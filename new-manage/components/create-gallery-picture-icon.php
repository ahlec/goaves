<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);

if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 1, 1) != "1")
  exit ("[Error] In order to create a photo gallery icon, must be authenticated to the management panel and have the " .
  	"permissions to manage photo galleries.");

if (!isset($_GET["identity"]) || !isset($_GET["x"]) || !isset($_GET["y"]) || !isset($_GET["width"]) || !isset($_GET["height"]))
  exit ("[Error] Not all parameters were passed through.");

$identity = $database->escapeString($_GET["identity"]);
if ($database->querySingle("SELECT count(*) FROM photos WHERE picture_identity='" . $identity . "'") == 0)
  exit ("[Error] Target picture does not exist within the database.");
$gallery_identity = $database->querySingle("SELECT photos.gallery_identity FROM photos JOIN photo_galleries ON photos.gallery_identity = " .
	"photo_galleries.gallery_identity WHERE photos.picture_identity='" . $identity . "' AND staff_identity='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' LIMIT 1");
if ($gallery_identity === false || $gallery_identity == "")
  exit ("[Error] The photo does not exist within a database, or you are not the owner of the target photo.");

$image_file = $database->querySingle("SELECT image_file FROM photos WHERE picture_identity='" . $identity . "' LIMIT 1");

if (!is_numeric($_GET["x"]))
  exit ("[Error] '" . $_GET["x"] . "' may not be used as an x-coordinate.");
$x = $_GET["x"];
if (!is_numeric($_GET["y"]))
  exit ("[Error] '" . $_GET["y"] . "' may not be used as a y-coordinate.");
$y = $_GET["y"];
if (!is_numeric($_GET["width"]))
  exit ("[Error] '" . $_GET["width"] . "' may not be used as a width.");
$width = $_GET["width"];
if (!is_numeric($_GET["height"]))
  exit ("[Error] '" . $_GET["height"] . "' may not be used as a height.");
$height = $_GET["height"];

$picture_icon = new ProcessedImage();
$picture_icon->loadOriginal(DOCUMENT_ROOT . "/images/photos/" . $image_file);
$picture_icon->crop($x, $y, $width, $height, 100, 100, false);
$new_filename = $picture_icon->save(DOCUMENT_ROOT . "/images/photos/", "icon-" . $gallery_identity . "-" . $identity);
$picture_icon->close();

if (!$database->exec("UPDATE photos SET icon_file='" . $new_filename . "', icon_x='" . $x . "', icon_y='" . $y . "', icon_width='" .
	$width . "', icon_height='" . $height . "', published='TRUE' WHERE picture_identity='" . $identity . "'"))
  exit ("[Error] Was unable to update the database with the icon information.");
$number_photos = $database->querySingle("SELECT count(*) FROM photos WHERE gallery_identity='" . $gallery_identity . "'");
if (!$database->exec("UPDATE photo_galleries SET is_complete='TRUE', number_photos='" . $number_photos . "' WHERE gallery_identity='" . $gallery_identity . "'"))
  exit ("[Error] Was unable to update the photo gallery within the database.");
if ($database->querySingle("SELECT gallery_image_set FROM photo_galleries WHERE gallery_identity='" . $gallery_identity . "' LIMIT 1") != "TRUE")
  $database->exec("UPDATE photo_galleries SET gallery_image='" . $new_filename ."' WHERE gallery_identity='" . $gallery_identity . "'");
  
if ($database->querySingle("SELECT count(*) FROM multimedia_feed WHERE media_type='photo_gallery' AND media_identity='" . $gallery_identity .
	"' AND post_time LIKE '" . date("Y-m-d", strtotime("+1 hours")) . "%'") == 0)
	if (!$database->exec("INSERT INTO multimedia_feed(media_type, media_identity, post_time, post_type, posting_staff_member) VALUES('photo_gallery','" .
		$gallery_identity . "','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "','has new pictures','" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) .
		"')"))
		exit ("[Error] Was unable to update the multimedia feed with the new information");
  
UpdateContentStatistic();
exit ("success");
?>