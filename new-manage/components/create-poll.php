<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_INTERACTIONS, 1) < "1")
  exit ("[Error] In order to create a poll, you must be authenticated to the management panel and have the permissions to manage interactions.");
  
if (!isset($_GET["question"]) || !isset($_GET["start"]) || !isset($_GET["end"]))
  exit("[Error] Form values were not passed to the creation script.");
  
$poll_question = $database->escapeString(preg_replace("/(<\?php|<\?)([^\?>]+)(\?>)/","", html_entity_decode($_GET["question"])));
if ($database->querySingle("SELECT count(*) FROM polls WHERE poll_question LIKE '" . $poll_question . "'") > 0)
  exit("[Error] A poll already exists in the database with this question.");
$poll_handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $poll_question))));
if ($database->querySingle("SELECT count(*) FROM polls WHERE poll_handle LIKE '" . $poll_handle . "'") > 0)
  exit("[Error] A poll already exists in the database with this handle.");

$date_start = $database->escapeString($_GET["start"]);
if (strlen($date_start) < 10)
  exit ("[Error] Start date is not in the proper format.");
if (!checkdate(substr($date_start, 5, 2), substr($date_start, 8, 2), substr($date_start, 0, 4)))
  exit ("[Error] Start date does not represent a valid date.");
 
$date_end = $database->escapeString($_GET["end"]);
if (strlen($date_end) < 10)
  exit ("[Error] End date is not in the proper format.");
if (!checkdate(substr($date_end, 5, 2), substr($date_end, 8, 2), substr($date_end, 0, 4)))
  exit ("[Error] End date does not represent a valid date.");

if ($date_end < $date_start)
  exit ("[Error] End date cannot occur before the start date.");

$options = array();
foreach ($_GET as $parameter_name => $parameter)
{
  if (strpos($parameter_name, "option_") !== 0)
    continue;
  if (mb_strlen($parameter) > 0)
  {
    if (in_array($database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($parameter)))), $options))
	  exit ("[Error] Cannot have two poll options of the same value.");
    $options[] = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($parameter))));
  }
}
if (sizeof($options) == 0)
  exit ("[Error] There were no valid poll options provided.");
  
if (!$database->exec("INSERT INTO polls(poll_handle, date_start, date_end, poll_question) VALUES('" . $poll_handle . "','" . $date_start .
	"','" . $date_end . "','" . $poll_question . "')"))
	exit ("[Error] Unable to create the poll within the database.");
	
$poll_identity = $database->querySingle("SELECT identity FROM polls WHERE poll_handle='" . $poll_handle . "' LIMIT 1");

foreach ($options as $poll_response)
{
  if (!$database->exec("INSERT INTO poll_responses(poll_identity, response) VALUES('" . $poll_identity . "','" . $poll_response . "')"))
    exit ("[Error] Unable to add one or more of the poll responses to the database.");
}
  
exit($poll_identity);
?>