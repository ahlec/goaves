<?php
if (!defined("MANAGE_SESSION"))
{
	if (!isset($_SESSION))
	  @session_start();
	require_once ("../../config/main.inc.php");
	require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
	$database = new DeitloffDatabase(DATABASE_PATH);
	if (!isset($_SESSION[MANAGE_SESSION]))
	  exit ("[Error] You must be authenticated into the management panel in order to access this component.");
	if (!isset($_GET["post"]))
	  exit ("[Error] Parameters not passed to validation script.");

	if (!is_numeric($_GET["post"]) || $database->querySingle("SELECT count(*) FROM manage_blog WHERE blog_identity='" . $database->escapeString($_GET["post"]) . "'") == 0)
	  exit ("[Error] The specified blog post does not exist within the database.");
	exit (createBlogPost($database->escapeString($_GET["post"]), true));
}
function createBlogPost($blog_identity, $need_title = false)
{
  $database = new DeitloffDatabase(DATABASE_PATH);
  $post = $database->querySingle("SELECT blog_identity, staff_identity, first_name, last_name, icon_file, post_timestamp, " .
	"title, body FROM manage_blog JOIN staff ON manage_blog.staff_identity = staff.identity WHERE blog_identity='" . $blog_identity . "' LIMIT 1", true);
  $blog_post = ($need_title ? "  <div class=\"header\">Management Blog</div>\n" : "");
  $blog_post .= "  <div class=\"blogEntry\">\n";
  $blog_post .= "    <img src=\"" . WEB_PATH . "/images/staff/" . $post["icon_file"] . "\" class=\"staffIcon\" border=\"0\" />\n";
  $blog_post .= "    <div class=\"bar\">\n";
  $blog_post .= "      <div class=\"title\">" . format_content($post["title"]) . "</div>\n";
  $blog_post .= "      <div class=\"info\">Posted by <b>" . format_content($post["first_name"]) . " " . format_content($post["last_name"]) . "</b>" . 
			" on <b>" . date(DATE_FORMAT, strtotime($post["post_timestamp"])) . "</b> at <b>" . date(TIME_FORMAT, strtotime($post["post_timestamp"])) . "</b></div>\n";
  $blog_post .= "    </div>\n";
  $blog_post .= "    <div class=\"contents\">\n";
  $blog_post .= "      " . format_content($post["body"]) . "\n";
  $blog_post .= "    </div>\n";
  $blog_post .= "  </div>\n";
  $blog_post .= "  <div class=\"blogNavigation\">\n";
  $next_blog_post = $database->querySingle("SELECT blog_identity, title FROM manage_blog WHERE post_timestamp >= '" . $post["post_timestamp"] .
	"' AND blog_identity<>'" . $post["blog_identity"] . "' ORDER BY post_timestamp ASC LIMIT 1", true);
  if ($next_blog_post != null)
    $blog_post .= "    <span class=\"next\" onClick=\"loadBlogPost('" . $next_blog_post["blog_identity"] . "');\" title=\"" .
		format_content($next_blog_post["title"]) . "\">Next</span>\n";
  $previous_blog_post = $database->querySingle("SELECT blog_identity, title FROM manage_blog WHERE post_timestamp <= '" . $post["post_timestamp"] .
	"' AND blog_identity<>'" . $post["blog_identity"] . "' ORDER BY post_timestamp DESC LIMIT 1", true);
  if ($previous_blog_post != null)
    $blog_post .= "    <span class=\"previous\" onClick=\"loadBlogPost('" . $previous_blog_post["blog_identity"] . "');\" title=\"" .
		format_content($previous_blog_post["title"]) . "\">Previous</span>\n";
  $blog_post .= "  </div>\n";
  return $blog_post;
}
?>