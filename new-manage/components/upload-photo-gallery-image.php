<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
function abortProcess($error_message, $upload_identity = null)
{
  echo "<body onload=\"parent.document.getElementById('gallery-upload-error').innerHTML = '" . $error_message . "';if(parent.document.getElementById(" .
	"'gallery-upload-error').isFading != true) { parent.document.getElementById('gallery-upload-error').style.display = 'block'; parent.fadeOutAndHide(" .
	"'gallery-upload-error', 2400); } parent.document.getElementById('photo-gallery-image-input-" .
  	$upload_identity . "').className = 'inputFile formComponentInvalid';\">\n";
  exit();
}
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 1, 1) != "1")
  abortProcess("[Error] In order to create a photo gallery, must be authenticated to the management panel and have the " .
  	"permissions to manage photo galleries.");
if (!isset($_POST["upload-identity"]))
  abortProcess("[Error] There was no upload identity provided.");  

$upload_identity = $_POST["upload-identity"];
if (!isset($_FILES["photo-gallery-image-upload-" . $upload_identity]))
  abortProcess ("[Error] Unable to load the photo using the upload identity.", $upload_identity);
if (!isset($_POST["gallery-identity"]) || $database->querySingle("SELECT count(*) FROM photo_galleries WHERE gallery_identity='" .
	$database->escapeString($_POST["gallery-identity"]) . "' AND staff_identity='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
  abortProcess ("[Error] Specified destination gallery does not exist or is not your gallery.", $upload_identity);
  
$upload_status = UploadFile("photo-gallery-image-upload-" . $upload_identity);
if (!$upload_status["UPLOAD"])
  abortProcess ($upload_status["MESSAGE"], $upload_identity);

if (!FileIsImage(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]))
  abortProcess ("[Error] Uploaded file is not a valid image file.", $upload_identity);

$gallery_identity = $database->escapeString($_POST["gallery-identity"]);
$next_image_number_in_gallery = $database->querySingle("SELECT count(*) FROM photos WHERE gallery_identity='" .	$gallery_identity . "'") + 1;

$gallery_image = new ProcessedImage();
$gallery_image->loadOriginal(MANAGE_DOCUMENT_ROOT . "/purgatory/" . $upload_status["FILEPATH"]);
$gallery_image->scale(650, 500, false);
$new_filename = $gallery_image->save(DOCUMENT_ROOT . "/images/photos/", "photo-" . $gallery_identity . "-" . $next_image_number_in_gallery);
$gallery_image->close();
$gallery_image->deleteOriginalImage();

$add_image = $database->exec("INSERT INTO photos(gallery_identity, handle, post_date, image_file, published) VALUES('" . $gallery_identity .
	"','" . $next_image_number_in_gallery . "','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "','" . $new_filename . "','FALSE')");
if (!$add_image)
  abortProcess ("[Error] Unable to add photo to the database.", $upload_identity);
else
  UpdateContentStatistic();
  
$added_image_identity = $database->querySingle("SELECT picture_identity FROM photos WHERE gallery_identity='" . $gallery_identity . "' AND image_file='" .
	$new_filename . "' LIMIT 1");
if (!isset($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$gallery_identity]))
  $_SESION[SESSION_PREFIX . "ManageGalleryPictures"][$gallery_identity] = array();
$_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$gallery_identity][] = $added_image_identity;
$gallery_info = $database->querySingle("SELECT date_last_updated, date_created, gallery_image_set FROM photo_galleries WHERE gallery_identity='" .
	$gallery_identity . "' LIMIT 1", true);
$database->exec("UPDATE photo_galleries SET date_last_updated='" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "' WHERE gallery_identity='" . $gallery_identity . "'");

$image_size = getimagesize(DOCUMENT_ROOT . "/images/photos/" . $new_filename);
	
echo "<body onload=\"parent.document.getElementById('photo-gallery-image-input-" . $upload_identity . "').className = 'inputFile " .
	"formComponentInProgress';parent.document.getElementById('photo-gallery-image-input-textbox-" . $upload_identity . "').innerHTML = " .
	"parent.document.getElementById('photo-gallery-image-upload-" . $upload_identity . "').value;parent.setupIconCreation('" .
	$new_filename . "','" . $added_image_identity . "','" . $image_size[0] . "','" . $image_size[1] . "');\">\n";
	//parent.createNewUpload();parent.document.getElementById(" .
	//"'gallery-upload-continue').disabled=false;parent.photos_have_been_uploaded = true;\">\n";
?>