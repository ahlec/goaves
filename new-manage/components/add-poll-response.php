<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_INTERACTIONS, 1) < "1")
  exit ("[Error] You must be authenticated into the management panel in order to access this component and have permissions to manage interactions.");

if (!isset($_GET["poll_identity"]) || !isset($_GET["response"]))
  exit ("[Error] Parameters were not passed correctly to the component.");
  
if ($database->querySingle("SELECT count(*) FROM polls WHERE identity='" . $database->escapeString($_GET["poll_identity"]) . "'") == 0)
  exit ("[Error] Poll does not exist within the database.");
$poll_identity = $database->escapeString($_GET["poll_identity"]);

$value = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_GET["response"]))));

if (strlen(str_replace(" ", "", $value)) == 0)
  exit ("[Error] Reponse must be one more more characters in length.");

if ($database->querySingle("SELECT count(*) FROM poll_responses WHERE poll_identity='" . $poll_identity . "' AND REPLACE(response,' ','') LIKE " .
	"REPLACE('" . $value . "',' ','')") > 0)
	exit ("[Error] Another response for this poll already contains the same value.");

if (!$database->exec("INSERT INTO poll_responses(poll_identity, response) VALUES('" . $poll_identity . "','" . $value . "')"))
  exit ("[Error] Could not add response to the database.");

$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "add-poll-response", "RESPONSE" => $value);
exit("success"); 
?>