<?php
class GoavesManageSpecialEditDescriptions {
	private $_specialIdentity;
	private $_title;
	private $_shortDescription;
	private $_description;
	
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM specials WHERE special_identity='" .
		$database->escapeString($path[2]) . "'") == 0)
	  {
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-special-edit-images", "LOADED" => false, "IDENTITY" => $path[2]);
		return new GoavesManageSpecialManage();
	  }
	  
	  $info = $database->querySingle("SELECT special_identity, title, description, short_description FROM specials WHERE special_identity='" .
		$database->escapeString($path[2]) . "' LIMIT 1", true);
		
	  $this->_specialIdentity = $info["special_identity"];
	  $this->_title = format_content($info["title"]);
	  $this->_shortDescription = format_content($info["short_description"]);
	  $this->_description = format_content($info["description"]);
	  return true;
	}
	function getRequirePermission() { return PERM_SPECIALS; }
	function getPageHandle() { return "special"; }
	function getPageSubhandle() { return "manage"; }
	function getPageTitle() { return "[Special] Edit '" . $this->_title . "'"; }
	function getBreadTrail() { return array("special" => "Specials Listing", 
		("special/manage/" . $this->_specialIdentity) => "'" . $this->_title . "'", "[this]" => "Edit Descriptions"); }
	function getPageStylesheet() { return "stylesheet-special.css"; }
	function getBodyOnload() { return null; }
	function getPageJavascript()
	{
	  return '
		tinyMCE.init({
            mode : "exact",
            elements : "full-description",
            entity_encoding : "named",
            theme : "advanced",
            skin : "o2k7",
            editor_selector : "advanced-textarea",
            plugins : "safari,spellchecker,pagebreak,style,layer,save,advhr,advimage,advlink," +
	    	"iespell,inlinepopups,preview,media,searchreplace,print,contextmenu,paste,directionality," +
	    	"fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
            theme_advanced_buttons1 : "cut,copy,paste,pastetext,|,search,replace,|,undo,redo,|,cleanup,code,|," +
	    	"preview,|,fullscreen,removeformat,visualaid,|,styleprops,spellchecker,|,cite,abbr,acronym,del,inc,attribs,",
            theme_advanced_buttons2 : "bold,italic,underline,strikethrough,|,forecolor,link,unlink,|,sub,sup,|,charmap," +
	    	"iespell,|,bullist,numlist,|,outdent,indent",
	    theme_advanced_buttons3 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : false,
            template_external_list_url : "js/template_list.js",
            external_link_list_url : "js/link_list.js",
            remove_trailing_nbsp : true,
            height : "200"});
		tinyMCE.init({
			mode: "exact",
			elements : "short-description",
			theme : "simple",
			skin : "o2k7"});
	  function processSubmitUpdate()
	  {
		var editCall = web_path + "/components/update-special-descriptions.php?identity=' . $this->_specialIdentity . '";
		editCall += "&short=" + encodeURIComponent(tinyMCE.get("short-description").getContent());
		editCall += "&full=" + encodeURIComponent(tinyMCE.get("full-description").getContent());
		ManageWindow("process-in-action");
		alert(editCall);
		executeAJAX(editCall, function processCreate(result)
		{
		  if (result == "success")
		  {
		    special_updated = true;
		    window.location = "' . MANAGE_WEB_PATH . "/special/manage/" . $this->_specialIdentity . '/";
			return;
		  }
		  alert(result);
		  openWindow.closeWindow();
		});
	  }';
	}
	function getPageContents()
	{	  
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <a class=\"returnLink\" href=\"" . MANAGE_WEB_PATH . "/special/manage/" . $this->_specialIdentity . "/\">&laquo; Return</a>\n";
	  echo "  Edit Special '" . $this->_title . "'\n";
	  echo "</div>\n";
			
	  echo "<div class=\"createForm noWindow\">\n";
	  
	  echo "  <div class=\"formLabel separator\">Short Description:</div>\n";
	  echo "  <textarea id=\"short-description\" style=\"width:100%;\">" . $this->_shortDescription . "</textarea>\n";
	  echo "  <div class=\"formLabel separator\">Full Description:</div>\n";
	  echo "  <textarea id=\"full-description\" style=\"width:100%;\">" . $this->_description . "</textarea>\n";
	  
	  echo "  <center><input type=\"button\" class=\"submitButton separator\" id=\"special-edit\" " .
		"value=\"Update\" onClick=\"processSubmitUpdate();\" /></center>\n";
	  echo "</div>\n";
	  echo "<div style=\"clear:both;\"></div>\n";
	}
}
?>