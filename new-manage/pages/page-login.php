<?php
class GoAvesManageLogin {
	private $_attemptedLogin = false;
	function checkOrRedirect($path, $database)
	{
		if (isset($_POST["login-username"]) && isset($_POST["login-password"]))
		{
		  $this->_attemptedLogin = true;
		  $provided_name = preg_replace("/%/", "", $_POST["login-username"]);
		  $name_pieces = explode(" ", $database->escapeString($provided_name));
		  if (sizeof($name_pieces) == 2)
		  {
		    $first_name = $name_pieces[0];
		    $last_name = $name_pieces[1];
		    $password = $database->escapeString(md5($_POST["login-password"]));
			$loaded_staff = $database->querySingle("SELECT identity, permissions, first_name, last_name, icon_file FROM staff WHERE first_name LIKE '" .
				$first_name . "' AND last_name LIKE '" . $last_name . "' AND password='" . $password . "' AND active='TRUE' LIMIT 1", true);
			if ($loaded_staff != "" && sizeof($loaded_staff) > 1)
			  $_SESSION[MANAGE_SESSION] = array("IDENTITY" => $loaded_staff["identity"],
			  	"FIRST_NAME" => $loaded_staff["first_name"], "LAST_NAME" => $loaded_staff["last_name"],
			  	"ICON" => $loaded_staff["icon_file"], "PERMISSIONS" => $loaded_staff["permissions"]);
		  }
		}
		if (isset($_SESSION[MANAGE_SESSION]))
		{
		  header("Location: " . MANAGE_WEB_PATH . "/");
		  exit();
		}
		return true;
	}
	function getPageHandle() { return "login"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Login"; }
	function getBreadTrail() { return array("[this]" => "Login"); }
	function getHideSidebar() { return true; }
	function getHideNavigation() { return true; }
	function getPageStylesheet() { return null; }
	function getBodyOnload()
	{
	  $commands = "";
	  if ($this->_attemptedLogin)
	    $commands .= "fadeOutAndHide('login-failed', 3500);";
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]) && $_SESSION[MANAGE_TRANSFER_DATA]["FROM"] == "logout")
	    $commands .= "fadeOutAndHide('logout-success', 3500);";
	  return $commands;
	}
	function getPageContents()
	{
		if ($this->_attemptedLogin)
		  echo "<div class=\"contentError separatorBottom\" id=\"login-failed\">" .
		  	"Name and password combination is incorrect.</div>\n";
		if (isset($_SESSION[MANAGE_TRANSFER_DATA]) && $_SESSION[MANAGE_TRANSFER_DATA]["FROM"] == "logout")
		{
		  echo "<div class=\"contentSuccess separatorBottom\" id=\"logout-success\"><b>Logout Successful.</b> Your session has now ended. Thank you for your hard work.</div>\n";
		  unset ($_SESSION[MANAGE_TRANSFER_DATA]);
		}
		echo "<form method=\"POST\" action=\"" . MANAGE_WEB_PATH . "/login/\" onSubmit=\"return validateLoginAttempt();\">\n";
		echo "  <div class=\"contentHeader\">Login Required</div>\n";
		echo "  <div class=\"indentedContent\">\n";
		echo "    <div class=\"contentSubheader\">Name:</div>\n";
		echo "    <input type=\"text\" name=\"login-username\" id=\"login-username\" class=\"textInput\" autocomplete=\"off\" />\n";
		echo "    <div class=\"contentError miniSeparator\" id=\"login-username-error\" style=\"display:none;\"></div>\n";
		echo "    <div class=\"contentSubheader separator\">Password:</div>\n";
		echo "    <input type=\"password\" name=\"login-password\" id=\"login-password\" class=\"textInput\" autocomplete=\"off\" />\n";
		echo "    <div class=\"contentError miniSeparator\" id=\"login-password-error\" style=\"display:none;\"></div>\n";
		echo "    <center><input type=\"submit\" class=\"textButton separator\" value=\"Login\" /></center>\n";
		echo "  </div>\n";
		echo "  <a href=\"" . MANAGE_WEB_PATH . "/recover-password/\" class=\"bulletLink\">Recover Password</a><br />\n";
		echo "</form>\n";
	}
}
?>