<?php
class GoAvesManageLogout {
	function checkOrRedirect($path, $database)
	{
		if (isset($_SESSION[MANAGE_SESSION]))
		{
		  unset ($_SESSION[MANAGE_SESSION]);
		  $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "logout", "SUCCESS" => true);
		}
		return new GoAvesManageLogin;
	}
	function getPageHandle() { return "logout"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Logout"; }
	function getBreadTrail() { return array("[this]" => "Logout"); }
	function getPageStylesheet() { return null; }
	function getPageContents()
	{
	}
}
?>