<?php
class GoavesManagePhotoGalleriesUploadPictures {
	private $_galleryIdentity;
	private $_title;
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM photo_galleries WHERE gallery_identity='" .
	  	$database->escapeString($path[2]) . "' AND staff_identity='" .
	  	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
	  	{
	  	  $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-photo-galleries-edit-information", "LOADED" => false,
	  	  	"IDENTITY" => $path[2]);
	  	}
	  $info = $database->querySingle("SELECT gallery_identity, photo_galleries.handle, photo_galleries.title, " .
	  	"photo_galleries.date_last_updated, photo_galleries.published, photo_galleries.related_group, gallery_image, " .
	  	"groups.handle AS \"related_group_handle\", groups.title AS \"related_group_title\", photo_galleries.date_created, " .
	  	"photo_galleries.is_complete, photo_galleries.topic, topics.handle AS \"topic_handle\", topics.title AS " .
	  	"\"topic_title\" FROM photo_galleries LEFT JOIN groups ON photo_galleries.related_group = groups.group_identity " .
	  	"LEFT JOIN topics ON photo_galleries.topic = topics.topic_identity WHERE gallery_identity='" .
	  	$database->escapeString($path[2]) . "' LIMIT 1", true);
	  $this->_galleryIdentity = $info["gallery_identity"];
	  $this->_title = format_content($info["title"]);
	  if (isset($path[3]) && strpos($path[3], "success-") === 0)
	  {
	    $supposed_success = $database->escapeString(str_replace("success-", "", $path[2]));
		if ($database->querySingle("SELECT count(*) FROM photo_galleries WHERE gallery_identity='" . $supposed_success . "' AND staff_identity='" .
			$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") > 0)
			{
			  $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-photo-galleries-create", "SUCCESS" => true, "IDENTITY" => $supposed_success);
			  header("Location: " . MANAGE_WEB_PATH . "/photo-gallery/list/");
			  exit();
			}
	  }
	  return true;
	}
	function getRequirePermission() { return "2"; }
	function getPageHandle() { return "photo-gallery"; }
	function getPageSubhandle() { return "upload-pictures"; }
	function getPageTitle() { return "Upload pictures to '" . $this->_title . "'"; }
	function getBreadTrail() { return array("photo-gallery" => "Photo Galleries", "photo-gallery/list" => "My Photo Galleries",
		("photo-gallery/manage/" . $this->_galleryIdentity) => "'" . $this->_title . "'", 
		"[this]" => "Upload Pictures"); }
	function getPageStylesheet() { return "stylesheet-galleries.css"; }
	function getPageOnBeforeUnload() { return "return getWarnPageRefresh();"; }
	function getPageJavascript()
	{
	  return "var photos_have_been_uploaded = false;
	  var has_clicked_transfer_button = false;
	  var gallery_upload_identity = 1;
	  function setupIconCreation(image_filename, picture_identity, picture_width, picture_height)
	  {
	    document.getElementById('gallery-upload-continue').disabled = true;
		for (var upload_buttons = 0; upload_buttons < gallery_upload_identity; upload_buttons++)
		{
		  document.getElementById('photo-gallery-image-upload-' + (upload_buttons + 1)).disabled = true;
		  document.getElementById('photo-gallery-image-button-' + (upload_buttons + 1)).disabled = true;
		}
		cropbox_real_x = picture_width;
		cropbox_real_y = picture_height;
	    var center_box = document.createElement('center');
		center_box.setAttribute('id', 'center-icon-image-box');
		center_box.className = 'separator';
	    var image_icon_crop_box = document.createElement('img');
		var icon_submit_button = document.createElement('input');
		icon_submit_button.className = 'submitButton separator';
		icon_submit_button.type = 'button';
		icon_submit_button.setAttribute('id', 'icon-crop-submit');
		icon_submit_button.value = 'Create Icon';
		icon_submit_button.setAttribute('onClick', 'cropPictureIcon(\"' + image_filename + '\",\"' + picture_identity + '\")');
		center_box.appendChild(image_icon_crop_box);
		image_icon_crop_box.src = '" . WEB_PATH . "/images/photos/' + image_filename;
		image_icon_crop_box.className = 'photoGalleryCropBox';
		image_icon_crop_box.setAttribute('id', 'icon-crop-box');
		center_box.appendChild(icon_submit_button);
		document.getElementById('upload-panel-slot').appendChild(center_box);
	    jQuery(function(){
			jQuery('#icon-crop-box').Jcrop({
				aspectRatio: 1.0,
				bgOpacity: 0.4,
				onSelect: updateIconCoords,
				onChange: updateIconCoords
			});
		});
	  }
	  var cropbox_real_x;
	  var cropbox_real_y;
	  var cropbox_x;
	  var cropbox_y;
	  var crop_width;
	  var crop_height;
	  function updateIconCoords(cropbox)
	  {
	    if (cropbox.w == 0 || cropbox.h == 0)
		{
		  cropbox_x = null;
		  cropbox_y = null;
		  crop_width = null;
		  crop_height = null;
		  return;
		}
	    var image_ratio = (cropbox_real_x > 300 ? cropbox_real_x / 300 : cropbox_real_x);
	    cropbox_x = cropbox.x * (cropbox_real_x > 300 ? image_ratio : 1);
		cropbox_y = cropbox.y * (cropbox_real_x > 300 ? image_ratio : 1);
		crop_width = cropbox.w * (cropbox_real_x > 300 ? image_ratio : 1);
		crop_height = cropbox.h * (cropbox_real_x > 300 ? image_ratio : 1);
	  }
	  function cropPictureIcon(image_filename, picture_identity)
	  {
	    if (cropbox_x == null || cropbox_y == null || crop_width == null || crop_height == null)
		{
		  alert ('Must crop a selection for the picture icon before continuing.');
		  return;
		}
	    executeAJAX(web_path + '/components/create-gallery-picture-icon.php?identity=' + picture_identity + '&x=' + cropbox_x + '&y=' + cropbox_y +
			'&width=' + crop_width + '&height=' + crop_height, function process(results)
			{
			  if (results == 'success')
			  {
			    document.getElementById('upload-panel-slot').removeChild(document.getElementById('center-icon-image-box'));
				document.getElementById('gallery-upload-continue').disabled = false;
				document.getElementById('photo-gallery-image-input-' + gallery_upload_identity).className = 'inputFile formComponentValid';
				createNewUpload();
			  } else
			    alert(results);
			});
	  }
	  function createNewUpload()
	  {
	    gallery_upload_identity++;
	    var newContainer = document.createElement('div');
		newContainer.className = 'inputFile';
		newContainer.setAttribute('id', 'photo-gallery-image-input-' + gallery_upload_identity);
		var newButton = newContainer.appendChild(document.createElement('div'));
		newButton.className = 'button';
		newButton.innerHTML = 'Select';
		newButton.setAttribute('id', 'photo-gallery-image-button-' + gallery_upload_identity);
		var newTextbox = newContainer.appendChild(document.createElement('div'));
		newTextbox.className = 'textBox';
		newTextbox.setAttribute('id', 'photo-gallery-image-input-textbox-' + gallery_upload_identity);
		var newForm = newContainer.appendChild(document.createElement('form'));
		newForm.setAttribute('method', 'POST');
		newForm.setAttribute('enctype', 'multipart/form-data');
		newForm.setAttribute('target', 'uploadForm');
		newForm.setAttribute('action', '" . MANAGE_WEB_PATH . "/components/upload-photo-gallery-image.php');
		newForm.setAttribute('id', 'photo-gallery-image-upload-form-' + gallery_upload_identity);
		var uploadIdentity = document.createElement('input');
		uploadIdentity.type = 'hidden';
		uploadIdentity.name = 'upload-identity';
		uploadIdentity.value = gallery_upload_identity;
		newForm.appendChild(uploadIdentity);
		var galleryIdentity = document.createElement('input');
		galleryIdentity.type = 'hidden';
		galleryIdentity.name = 'gallery-identity';
		galleryIdentity.value = " . $this->_galleryIdentity . ";
		newForm.appendChild(galleryIdentity);
		var fileUpload = document.createElement('input');
		fileUpload.type = 'file';
		fileUpload.className = 'inputFile';
		fileUpload.name = 'photo-gallery-image-upload-' + gallery_upload_identity;
		fileUpload.setAttribute('onChange', 'document.getElementById(\"photo-gallery-image-upload-form-' + gallery_upload_identity + '\").submit();');
		fileUpload.setAttribute('id', 'photo-gallery-image-upload-' + gallery_upload_identity);
		newForm.appendChild(fileUpload);
		document.getElementById('upload-panel-slot').appendChild(newContainer);
	  }
	  function getWarnPageRefresh()
	  {
	    if (photos_have_been_uploaded == false)
		  return;
		if (has_clicked_transfer_button)
		  return;
		return 'You have uploaded new pictures to the gallery. However, you need to edit the pictures after they have been uploaded. Click the \'Continue\' button below.';
	  }";
	}
	function getPageContents()
	{
	  echo "<div class=\"contentHeader\">\n";
	  echo "  <a class=\"returnLink\" href=\"" . MANAGE_WEB_PATH . "/photo-gallery/manage/" . $this->_galleryIdentity . "/\">&laquo; Return</a>\n";
	  echo "  Upload pictures to '" . $this->_title . "'\n";
	  echo "</div>\n";
	  
	  echo "<iframe frameborder=\"0\" id=\"frame-photo-gallery-upload\" name=\"uploadForm\" class=\"formFrame\" style=\"display:block;\"></iframe>\n";
	  
	  echo "<div class=\"createForm separator\">\n";
	  
	  echo "<div id=\"upload-panel-slot\">\n";
	  echo "  <div class=\"inputFile\" id=\"photo-gallery-image-input-1\">\n";
	  echo "    <div class=\"button\" id=\"photo-gallery-image-button-1\">Select</div>\n";
	  echo "    <div class=\"textBox\" id=\"photo-gallery-image-input-textbox-1\"></div>\n";
	  echo "    <form method=\"POST\" enctype=\"multipart/form-data\" target=\"uploadForm\" action=\"" . MANAGE_WEB_PATH .
	  	"/components/upload-photo-gallery-image.php\" id=\"photo-gallery-image-upload-form-1\">\n";
	  echo "      <input type=\"hidden\" name=\"upload-identity\" value=\"1\" />\n";
	  echo "      <input type=\"hidden\" name=\"gallery-identity\" value=\"" . $this->_galleryIdentity . "\" />\n";
	  echo "      <input type=\"file\" class=\"inputFile\" name=\"photo-gallery-image-upload-1\" onChange=\"document.getElementById('" .
	  	"photo-gallery-image-upload-form-1').submit();\" id=\"photo-gallery-image-upload-1\" />\n";
	  echo "    </form>\n";
	  echo "  </div>\n";
	  echo "</div>\n";
	  echo "<div class=\"contentError miniSeparator\" id=\"gallery-upload-error\" style=\"display:none;\"></div>\n";
	  
	  echo "  <center><input type=\"button\" class=\"submitButton separator\" id=\"gallery-upload-continue\" value=\"Continue &raquo;\" disabled=\"disabled\" " .
		"onClick=\"has_clicked_transfer_button = true; window.location='" . MANAGE_WEB_PATH . "/photo-gallery/manage-pictures/" .
		$this->_galleryIdentity . "/';\" /></center>\n";
	  echo "</div>\n";
	  echo "  <div style=\"clear:both;\"></div>\n";
	}
}
?>