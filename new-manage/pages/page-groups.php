<?php
class GoavesManageGroups {
	private $_loadError;
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[1]))
	    $path[1] = "landing";
	  if ($path[1] == "manage")
	  {
	    if (!isset($_SESSION[MANAGE_TRANSFER_DATA]) || $_SESSION[MANAGE_TRANSFER_DATA]["FROM"] != "page-groups-manage")
		  return new GoavesManageGroupsManage();
		$this->_loadError = "Could not load specified group.";
	    return true;
	  }
	  switch ($path[1])
	  {
		default: return new GoavesManageGroupsList();
	  }
	  return true;
	}
	function getRequirePermission() { return "7"; }
	function getPageHandle() { return "group"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Groups"; }
	function getBreadTrail() { return array("[this]" => "Groups"); }
	function getPageStylesheet() { return "stylesheet-groups.css"; }
	function getPageContents()
	{
	}
}
?>