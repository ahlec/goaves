<?php
class GoavesManageSoundslideManageSlides {
	private $_slideshowIdentity;
	private $_title;
	private $_slides = array();
	function checkOrRedirect($path, $database)
	{
	  if (isset($_POST["submit-picture-change"]))
	  {
	    echo "<pre>\n";
	    print_r($_POST);
	    exit("</pre>");
	  }
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM sound_slides WHERE slideshow_identity='" .
	  	$database->escapeString($path[2]) . "' AND staff_identity='" .
	  	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
	  	{
	  	  $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-soundslide-manage-slides", "LOADED" => false,
	  	  	"IDENTITY" => $path[2]);
	  	}
	  $this->_slideshowIdentity = $database->escapeString($path[2]);
	  if (!isset($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$this->_slideshowIdentity]))
	  {
	    $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-soundslide-manage-slides", "LOADED" => false,
			"REASON" => "No selected pictures to manage.");
	    return new GoavesManageSoundslidesManage();
	  }
	  
	  $this->_title = format_content($database->querySingle("SELECT title FROM sound_slides WHERE slideshow_identity='" . $this->_slideshowIdentity . "' LIMIT 1"));
	  foreach ($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$this->_slideshowIdentity] as $slide_identity)
	  {
	    $info = $database->querySingle("SELECT slide_identity, icon_file, caption, start_time FROM sound_slides_slides WHERE slide_identity='" .
			$database->escapeString($slide_identity) . "' LIMIT 1", true);
		if ($info !== false)
		  $this->_slides[] = array("IDENTITY" => $info["slide_identity"], "CAPTION" => format_content($info["caption"]), "ICON_FILE" => $info["icon_file"],
			"START" => $info["start_time"]);
	  }
	  return true;
	}
	function getRequirePermission() { return "6"; }
	function getPageHandle() { return "soundslide"; }
	function getPageSubhandle() { return "manage"; }
	function getPageTitle() { return "Edit slides in '" . $this->_title . "'"; }
	function getBreadTrail() { return array("soundslide" => "Soundslides", "soundslide/list" => "My Soundslides",
		("soundslide/manage/" . $this->_slideshowIdentity) => "'" . $this->_title . "'", 
		"[this]" => "Edit Slides"); }
	function getPageStylesheet() { return "stylesheet-soundslides.css"; }
	function getPageOnBeforeUnload() { return "return getWarnPageRefresh();"; }
	function getPageJavascript()
	{
	  return "var photos_have_been_uploaded = false;
	  var has_clicked_transfer_button = false;
	  tinyMCE.init({
		mode: \"textareas\",
		theme : \"simple\",
		skin : \"o2k7\"
          });
	  function getWarnPageRefresh()
	  {
	    if (photos_have_been_uploaded == false)
		  return;
		if (has_clicked_transfer_button)
		  return;
		return 'You have uploaded new pictures to the gallery. However, you need to edit the pictures after they have been uploaded. Click the \'Continue\' button below.';
	  }";
	}
	function getPageContents()
	{
	  echo "<div class=\"contentHeader\">\n";
	  echo "  <div class=\"explainButton\" onClick=\"ManageWindow('explain','manage-soundslide-slides');\"></div>\n";
	  echo "  Edit slides in '" . $this->_title . "'\n";
	  echo "</div>\n";
	  
	  echo "<form method=\"POST\" action=\"" . MANAGE_WEB_PATH . "/photo-gallery/manage-pictures/" .
	  	$this->_slideshowIdentity . "/\">\n";
	  echo "<div class=\"createForm noWindow separator\">\n";
	  
	  foreach ($this->_slides as $index => $slide)
	  {
	    echo "  <div class=\"slideContainer" . ($index < sizeof($this->_slides) - 1 ? "" : " finalSlide") . "\">\n";
	    echo "    <img src=\"" . WEB_PATH . "/images/sound_slides/" . $slide["ICON_FILE"] . "\" class=\"icon\" />\n";
		echo "      <div class=\"formLabel separator\">Start time:</div>\n";
		$minutes_length = 8;
		echo "<select id=\"slide-upload-minutes-" . $slide["IDENTITY"] . "\" name=\"slide-upload-minutes-" . $slide["IDENTITY"] . "\">\n";
		for ($min_option = 0; $min_option <= $minutes_length; $min_option++)
		  echo "  <option value=\"" . $min_option . "\">" . ($min_option < 10 ? "0" : "") . $min_option . "</option>\n";
		echo "</select> : <select id=\"slide-upload-seconds\">\n";
		for ($sec_option = 0; $sec_option < 60; $sec_option++)
		  echo "  <option value=\"" . $sec_option . "\">" . ($sec_option < 10 ? "0" : "") . $sec_option . "</option>\n";
		echo "</select>\n";
	    echo "      <div class=\"formLabel separator\">Caption (if any):</div>\n";
	    echo "      <textarea id=\"slide-caption-" . $slide["IDENTITY"] . "\" name=\"slide-caption-" .
	    	$slide["IDENTITY"] . "\">" . $slide["CAPTION"] . "</textarea>\n";
	    echo "    <div style=\"margin-bottom:20px;clear:both;\"></div>\n";
	    echo "  </div>\n";
	  }
	  
	  echo "  <center><input type=\"submit\" class=\"submitButton separator\" value=\"Continue &raquo;\" " .
	  	"name=\"submit-picture-change\" /></center>\n";
	  echo "</div>\n";
	  echo "</form>\n";
	  echo "  <div style=\"clear:both;\"></div>\n";
	}
}
?>