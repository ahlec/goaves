<?php
class GoavesManageSoundslides {
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[1]))
	    $path[1] = "landing";
	  switch ($path[1])
	  {
	    case "create": return new GoavesManageSoundslidesCreate();
		case "manage": return new GoavesManageSoundslidesManage();
		case "list":
	    default: return new GoavesManageSoundslideList();
	  }
	  return true;
	}
	function getRequirePermission() { return "6"; }
	function getPageHandle() { return "soundslide"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Soundslides"; }
	function getBreadTrail() { return array("[this]" => "Soundslides"); }
	function getPageContents()
	{
	  echo "soundslides";
	}
}
?>