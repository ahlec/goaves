<?php
class GoAvesManage404 {
	function checkOrRedirect($path, $database) { return true; }
	function getPageHandle() { return null; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Page Not Found"; }
	function getBreadTrail() { return array("[this]" => "Missing Page"); }
	function getPageStylesheet() { return null; }
	function getPageContents()
	{
	  echo "        <div class=\"contentHeader\">Page Not Found (Error 404)</div>\n";
	  echo "The page you have attempted to load either does not exist, or is currently in the process of development. " .
	  	"If this is a case of the latter, returning to this page after a day or so may merit more desirable results. " .
	  	"If you feel that you have reached this page in error, feel free to send a message to the webmaster(s).<br />" .
	  	"<br /><b><a href=\"" . MANAGE_WEB_PATH . "/\">Return to the management panel</a></b>";
	  echo "      </div>\n";
	}
}
?>