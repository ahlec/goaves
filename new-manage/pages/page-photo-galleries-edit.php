<?php
class GoavesManagePhotoGalleriesEdit {
	private $_galleryIdentity;
	private $_title;
	private $_handle;
	private $_dateLastUpdated;
	private $_published;
	private $_relatedGroup;
	private $_dateCreated;
	private $_isComplete;
	private $_special;
	private $_galleryImage;
	
	private $_photos = array();
	
	private $_relatedGroupTypes = array();
	private $_allSpecials = array();
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM photo_galleries WHERE gallery_identity='" .
		$database->escapeString($path[2]) . "'" . (substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GALLERIES, 1) == "1" ?
		" AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'" : "")) == 0)
	  {
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-photo-galleries-manage", "LOADED" => false, "IDENTITY" => $path[2]);
		return new GoavesManagePhotoGalleriesList();
	  }
	  
	  $info = $database->querySingle("SELECT gallery_identity, photo_galleries.handle, photo_galleries.title, " .
	  	"photo_galleries.date_last_updated, photo_galleries.published, photo_galleries.related_group, gallery_image, " .
	  	"groups.handle AS \"related_group_handle\", groups.title AS \"related_group_title\", groups.type AS \"related_group_type\", photo_galleries.date_created, " .
	  	"photo_galleries.is_complete, photo_galleries.special_identity, specials.handle AS \"special_handle\", specials.title AS " .
	  	"\"special_title\" FROM photo_galleries LEFT JOIN groups ON photo_galleries.related_group = groups.group_identity " .
	  	"LEFT JOIN specials ON photo_galleries.special_identity = specials.special_identity WHERE gallery_identity='" .
	  	$database->escapeString($path[2]) . "' LIMIT 1", true);
	  $this->_galleryIdentity = $info["gallery_identity"];
	  $this->_title = format_content($info["title"]);
	  $this->_handle = $info["handle"];
	  $this->_dateLastUpdated = strtotime($info["date_last_updated"]);
	  $this->_published = ($info["published"] == "TRUE");
	  $this->_relatedGroup = ($info["related_group"] != "" ? array("IDENTITY" => $info["related_group"],
	  	"HANDLE" => $info["related_group_handle"], "TITLE" => format_content($info["related_group_title"]), "TYPE" => $info["related_group_type"]) : null);
	  $this->_dateCreated = strtotime($info["date_created"]);
	  $this->_isComplete = ($info["is_complete"] == "TRUE");
	  $this->_special = ($info["special_identity"] != "" ? array("IDENTITY" => $info["special_identity"], "HANDLE" => $info["special_handle"],
	  	"TITLE" => format_content($info["special_title"])) : null);
	  $this->_galleryImage = ($info["gallery_image"] != "" ? $info["gallery_image"] : "no-gallery-image.jpg");	  
	  
	  $group_types = $database->query("SELECT type_identity, plural_name, handle FROM group_types ORDER BY type_identity ASC");
	  while ($group_type = $group_types->fetchArray())
	  {
	    $get_groups = $database->query("SELECT group_identity, title, active FROM groups WHERE type='" .
			$group_type["type_identity"] . "' ORDER BY title ASC");
		$groups_with_type = array();
		while ($group = $get_groups->fetchArray())
		  $groups_with_type[] = array("IDENTITY" => $group["group_identity"], "TITLE" => format_content($group["title"]),
			"ACTIVE" => ($group["active"] == "TRUE"));
	    $this->_relatedGroupTypes[$group_type["handle"]] = array("NAME" => $group_type["plural_name"],
			"GROUPS" => $groups_with_type, "IDENTITY" => $group_type["type_identity"]);
	  }
	  
	  $get_specials = $database->query("SELECT special_identity, title, active, ended FROM specials ORDER BY active DESC, title ASC");
	  while ($special = $get_specials->fetchArray())
	    $this->_allSpecials[] = array("IDENTITY" => $special["special_identity"], "TITLE" => format_content($special["title"]),
			"ACTIVE" => ($special["active"] == "TRUE"), "ENDED" => ($special["active"] == "TRUE" ? null : strtotime($special["ended"])));
	  
	  $get_photos = $database->query("SELECT picture_identity, icon_file FROM photos WHERE gallery_identity='" . $this->_galleryIdentity . "' ORDER BY " .
		"post_date ASC");
	  while ($photo = $get_photos->fetchArray())
	    $this->_photos[] = array("IDENTITY" => $photo["picture_identity"], "ICON" => $photo["icon_file"]);
	  
	  return true;
	}
	function getRequirePermission() { return "2"; }
	function getPageHandle() { return "photo-gallery"; }
	function getPageSubhandle() { return "manage"; }
	function getPageTitle() { return "[Photo Gallery] Edit '" . $this->_title . "'"; }
	function getBreadTrail() { return array("photo-gallery" => "Photo Galleries", "photo-gallery/list" => "My Photo Galleries",
		("photo-gallery/manage/" . $this->_galleryIdentity) => "'" . $this->_title . "'", "[this]" => "Edit"); }
	function getPageStylesheet() { return "stylesheet-galleries.css"; }
	function getBodyOnload() { return null; }
	function getPageJavascript()
	{
	  $group_handles = "[";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	    $group_handles .= "'" . $handle . "',";
	  $group_handles = rtrim($group_handles, ",") . "]";
	  return "var gallery_updated = false;
	  var group_types = " . $group_handles . ";
	  function toggleUseRelatedGroup()
	  {
	    document.getElementById('gallery-related-group-type').selectedIndex = 0;
	    document.getElementById('gallery-related-group-type').style.display =
			(document.getElementById('gallery-related-group-use').value ? 'block' : 'none');
		for (var type_index = 0; type_index < group_types.length; type_index++)
		  document.getElementById('gallery-related-group-' + group_types[type_index]).style.display = 'none';
	  }
	  function toggleUseSpecial()
	  {
	    document.getElementById('gallery-special').selectedIndex = 0;
		document.getElementById('gallery-special').style.display = (document.getElementById('gallery-special-use').value ? 'block' : 'none');
	  }
	  function selectGroupByType(type_handle)
	  {
	    for (var type_index = 0; type_index < group_types.length; type_index++)
		  document.getElementById('gallery-related-group-' + group_types[type_index]).style.display = 'none';
	    if (type_handle == '[none]')
		  return;
		document.getElementById('gallery-related-group-' + type_handle).selectedIndex = 0;
	    document.getElementById('gallery-related-group-' + type_handle).style.display = 'block';
	  }
	  function selectNewIcon(photo_identity, photo_icon)
	  {
	    document.getElementById('gallery-icon').value = photo_icon;
	    var icons = document.getElementById('selectionIcons').getElementsByTagName('img');
        for (var icon = 0; icon < icons.length; icon++)
          icons[icon].className = 'icon';
		document.getElementById('icon-' + photo_identity).className = \"icon currentIcon\";
	  }
	  function checkFormValid()
	  {
	    if (document.getElementById('gallery-title').className == 'inputText formComponentValid')
		{
		  document.getElementById('gallery-edit').disabled = false;
		  return;
		}
		document.getElementById('gallery-edit').disabled = true;
	  }
	  function getWarnPageRefresh()
	  {
	    if (gallery_updated)
		  return null;
	    if (document.getElementById('gallery-title').value == '')
		  return null;
		return 'Are you sure you wish to leave this page? (Unsaved content will be lost unless you save it)';
	  }
	  function processSubmitUpdate()
	  {
	    if (document.getElementById('gallery-edit').disabled)
		  return;
		var createCall = web_path + '/components/update-photo-gallery.php?identity=" . $this->_galleryIdentity . "';
		createCall += '&title=' + encodeURIComponent(document.getElementById('gallery-title').value);
		if (document.getElementById('gallery-related-group-use').value)
		{
		  var selected_group_type = document.getElementById('gallery-related-group-type').value;
		  if (selected_group_type != '[none]' && document.getElementById('gallery-related-group-' + selected_group_type).value != '[none]')
		    createCall += '&related_group=' + document.getElementById('gallery-related-group-' + selected_group_type).value;
		  else
		    createCall += '&related_group=none';
		} else
		  createCall += '&related_group=none';
		if (document.getElementById('gallery-special') != undefined && document.getElementById('gallery-special-use').value &&
		  document.getElementById('gallery-special').value != '[none]')
		  createCall += '&special=' + document.getElementById('gallery-special').value;
		else
		  createCall += '&special=none';
		if (document.getElementById('gallery-icon').value != undefined && document.getElementById('gallery-icon').value != null &&
			document.getElementById('gallery-icon').value.length > 0)
			  createCall += '&icon=' + document.getElementById('gallery-icon').value;
		ManageWindow('process-in-action');
		executeAJAX(createCall, function processCreate(result)
		{
		  if (result == 'success')
		  {
		    gallery_updated = true;
		    window.location = '" . MANAGE_WEB_PATH . "/photo-gallery/manage/" . $this->_galleryIdentity . "/';
			return;
		  }
		  ManageWindow('error',result);
		});
	  }";
	}
	function getPageContents()
	{	  
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <a class=\"returnLink\" href=\"" . MANAGE_WEB_PATH . "/photo-gallery/manage/" . $this->_galleryIdentity . "/\">&laquo; Return</a>\n";
	  echo "  Edit Gallery '" . $this->_title . "'\n";
	  echo "</div>\n";
			
	  echo "<div class=\"createForm noWindow\">\n";
	  echo "  <div class=\"infoLine\"><b>Title:</b> " . $this->_title . "</div>\n";
	  echo "  <input type=\"text\" class=\"inputText\" name=\"gallery-title\" id=\"gallery-title\" autocomplete=\"off\" onKeyUp=\"executeAJAX(web_path + '/components/" .
		"check-title.php?type=gallery&minimum=6&title=' + escapeStringURL(this.value) + '&identity=' + " . $this->_galleryIdentity . ", function " .
		"processTitle(return_value) {
		  if (return_value.indexOf('[Error]') >= 0)
		  {
		    document.getElementById('gallery-title-error').innerHTML = return_value.replace('[Error] ', '');
			if (document.getElementById('gallery-title-error').isFading != true)
			{
		      document.getElementById('gallery-title-error').style.display = 'block';
			  fadeOutAndHide('gallery-title-error', 2400);
		    }
			document.getElementById('gallery-title').className = 'inputText formComponentInvalid';
			document.getElementById('gallery-edit').disabled = true;
		  } else
		  {
		    document.getElementById('gallery-title-error').style.display = 'none';
		    document.getElementById('gallery-title').className = 'inputText formComponentValid';
			checkFormValid();
		  } });\" value=\"" . addslashes($this->_title) . "\" />\n";
	  echo "  <div class=\"contentError\" id=\"gallery-title-error\" style=\"display:none;\"></div>\n";
	  
	  echo "  <div class=\"infoLine separator\"><b>Related Group:</b> " . ($this->_relatedGroup != null ? $this->_relatedGroup["TITLE"] : "None") . "</div>\n";
	  echo "  <input type=\"checkbox\" id=\"gallery-related-group-use\" onChange=\"toggleUseRelatedGroup();\" " . ($this->_relatedGroup != null ?
		"checked=\"checked\" " : "") . "/> " .
		"<label for=\"gallery-related-group-use\">Link to a related group?</label><br />\n";
	  echo "  <select id=\"gallery-related-group-type\" " . ($this->_relatedGroup != null ? "" : "style=\"display:none;\" ") . "class=\"inputSelect selectLeft\" " .
		"onChange=\"selectGroupByType(this.value);\">\n";
	  echo "    <option value=\"[none]\">(Group Type)</option>\n";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	  {
	    echo "    <option value=\"" . $handle . "\" " . ($this->_relatedGroup != null && $this->_relatedGroup["TYPE"] == $info["IDENTITY"] ? "selected=\"selected\" " : "") .
			">" . $info["NAME"] . "</option>\n";
	  }
	  echo "  </select>\n";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	  {
	    echo "  <select id=\"gallery-related-group-" . $handle .
			"\" class=\"inputSelect selectRight\" " . ($this->_relatedGroup != null && $this->_relatedGroup["TYPE"] == $info["IDENTITY"] ? "" :
			"style=\"display:none;\" ") . ">\n";
		echo "    <option value=\"[none]\">(Group)</option>\n";
		foreach ($info["GROUPS"] as $group)
		  echo "    <option value=\"" . $group["IDENTITY"] . "\"" . ($this->_relatedGroup != null && $this->_relatedGroup["IDENTITY"] == $group["IDENTITY"] ?
			" selected=\"selected\"" : "") . ">" . $group["TITLE"] .
			(!$group["ACTIVE"] ? " (Inactive)" : "") . "</option>\n";
	    echo "  </select>\n";
	  }
	  echo "  <div style=\"clear:both;\"></div>\n";
		
	  echo "  <div class=\"infoLine separator\"><b>Special:</b> " . ($this->_special != null ? $this->_special["TITLE"] : "None") . "</div>\n";
	  if (sizeof($this->_allSpecials) > 0)
	  {
	    echo "  <input type=\"checkbox\" id=\"gallery-special-use\" onChange=\"toggleUseSpecial();\" " . ($this->_special != null ? "checked=\"checked\" " : "") . "/> " .
		"<label for=\"gallery-special-use\">Include within a special?</label><br />\n";
	    echo "  <select id=\"gallery-special\" class=\"inputSelect\"" . ($this->_special != null ? "" : " style=\"display:none;\"") . ">\n";
	    echo "    <option value=\"[none]\">(Special)</option>\n";
	    foreach ($this->_allSpecials as $special)
	      echo "    <option value=\"" . $special["IDENTITY"] . "\"" . ($this->_special != null && $this->_special["IDENTITY"] == $special["IDENTITY"] ?
			" selected=\"selected\"" : "") . ">" . $special["TITLE"] . "</option>\n";
	    echo "  </select>\n";
		echo "  <div style=\"clear:both;\"></div>\n";
	  } else
	    echo "  <div>There are no specials within the database.</div>\n";
		
	  echo "  <div class=\"infoLine separator\"><b>Gallery Icon:</b></div>\n";
	  echo "  <input type=\"hidden\" id=\"gallery-icon\" value=\"" . $this->_galleryImage . "\" />\n";
	  echo "  <div class=\"editGalleryCurIcon\">\n";
	  echo "    <img src=\"" . WEB_PATH . "/images/photos/" . $this->_galleryImage . "\" /><br />\n";
	  echo "    (Current)\n";
	  echo "  </div>\n";
	  echo "  <div class=\"editGalleryIconSelections\">\n";
	  echo "    <div class=\"text\">Select an icon from below to set as the gallery icon.</div>\n";
	  if (sizeof($this->_photos) > 0)
	  {
	    echo "    <div id=\"selectionIcons\">\n";
	    foreach ($this->_photos as $photo)
	    {
	      echo "    <img src=\"" . WEB_PATH . "/images/photos/" . $photo["ICON"] . "\" class=\"icon" . ($this->_galleryImage == $photo["ICON"] ?
			" currentIcon" : "") . "\" onClick=\"selectNewIcon('" . $photo["IDENTITY"] . "','" .
			$photo["ICON"] . "');\" id=\"icon-" . $photo["IDENTITY"] . "\" />\n";
	    }
		echo "    </div>\n";
	  } else
	    echo "    <div class=\"noPhotos\">There are no photos in this photo gallery.</div>\n";
	  echo "  </div>\n";
	  echo "  <center><input type=\"button\" class=\"submitButton separator\" id=\"gallery-edit\" " .
		"value=\"Update\" onClick=\"processSubmitUpdate();\" /></center>\n";
	  echo "</div>\n";
	  echo "<div style=\"clear:both;\"></div>\n";
	}
}
?>