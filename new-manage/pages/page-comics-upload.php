<?php
class GoavesManageComicsUpload {
	private $_comics = array();
	private $_comicTitleMinimum = 6;
	function checkOrRedirect($path, $database)
	{
	  if (isset($path[2]) && strpos($path[2], "success-") === 0)
	  {
	    $supposed_success = $database->escapeString(str_replace("success-", "", $path[2]));
		if ($database->querySingle("SELECT count(*) FROM cartoons WHERE identity='" . $supposed_success . "' AND artist='" .
			$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") > 0)
			{
			  $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-comics-upload", "SUCCESS" => true, "IDENTITY" => $supposed_success);
			  header("Location: " . MANAGE_WEB_PATH . "/comic/list/");
			  exit();
			}
	  }
	  return true;
	}
	function getRequirePermission() { return "5"; }
	function getPageHandle() { return "comic"; }
	function getPageSubhandle() { return "upload"; }
	function getPageTitle() { return "Upload New Comic"; }
	function getBreadTrail() { return array("comic" => "Comics", "[this]" => "Upload New Comic"); }
	function getPageStylesheet() { return "stylesheet-comics.css"; }
	function getPageOnBeforeUnload() { return "return getWarnPageRefresh();"; }
	function getPageJavascript()
	{
	  return "var comic_submitted = false;
	  function checkFormValid()
	  {
	    if (document.getElementById('comic-title').className == 'inputText formComponentValid' & document.getElementById('comic-filepath').value != '')
		{
		  document.getElementById('comic-upload').disabled = false;
		  return;
		}
		document.getElementById('comic-upload').disabled = true;
	  }
	  function getWarnPageRefresh()
	  {
	    if (comic_submitted)
		  return null;
	    if (document.getElementById('comic-title').value == '' & document.getElementById('comic-filepath').value == '')
		  return null;
		return 'Are you sure you wish to leave this page? (Unsaved content will be lost unless you save it)';
	  }
	  function processSubmitUpload()
	  {
	    if (document.getElementById('comic-upload').disabled)
		  return;
		var uploadCall = web_path + '/components/upload-comic.php?title=' + encodeURIComponent(document.getElementById('comic-title').value) + 
			'&filepath=' + encodeURIComponent(document.getElementById('comic-filepath').value);
		executeAJAX(uploadCall, function processUpload(result)
		{
		  if (result.indexOf('[Error]') < 0)
		  {
		    comic_submitted = true;
		    window.location = '" . MANAGE_WEB_PATH . "/comic/upload/success-' + result + '/';
			return;
		  }
		  alert(result);
		});
	  }";
	}
	function getPageContents()
	{
	  echo "<div class=\"contentHeader\">Upload New Comic</div>\n";
	  
	  echo "<iframe id=\"frame-comic-image\" name=\"imageForm\" class=\"formFrame\"></iframe>\n";
	  echo "<div class=\"createForm\">\n";
	  echo "  <div class=\"formLabel\">Title:</div>\n";
	  echo "  <input type=\"text\" class=\"inputText\" name=\"comic-title\" id=\"comic-title\" autocomplete=\"off\" onKeyUp=\"executeAJAX(web_path + '/components/" .
		"check-title.php?type=comic&minimum=" . $this->_comicTitleMinimum . "&title=' + encodeURIComponent(this.value), function " .
		"processTitle(return_value) {
		  if (return_value.indexOf('[Error]') >= 0)
		  {
		    document.getElementById('comic-title-error').innerHTML = return_value.replace('[Error] ', '');
			if (document.getElementById('comic-title-error').isFading != true)
			{
		      document.getElementById('comic-title-error').style.display = 'block';
			  fadeOutAndHide('comic-title-error', 2400);
		    }
			document.getElementById('comic-title').className = 'inputText formComponentInvalid';
			document.getElementById('comic-upload').disabled = true;
		  } else
		  {
		    document.getElementById('comic-title-error').style.display = 'none';
		    document.getElementById('comic-title').className = 'inputText formComponentValid';
			checkFormValid();
		  } });\" />\n";
	  echo "  <div class=\"contentError\" id=\"comic-title-error\" style=\"display:none;\"></div>\n";
	  echo "  <div class=\"formLabel miniSeparator\">Image File:</div>\n";
	  echo "  <input type=\"hidden\" id=\"comic-filepath\" value=\"\" />\n";
	  echo "  <div class=\"inputFile\" id=\"comic-image-input\">\n";
	  echo "    <div class=\"button\">Select</div>\n";
	  echo "    <div class=\"textBox\" id=\"comic-image-input-textbox\"></div>\n";
	  echo "    <form method=\"POST\" enctype=\"multipart/form-data\" target=\"imageForm\" action=\"" . MANAGE_WEB_PATH . "/components/upload-comic-image.php\" " .
		"id=\"comic-image-upload-form\">\n";
	  echo "      <input type=\"file\" class=\"inputFile\" name=\"comic-image\" onChange=\"document.getElementById('comic-image-upload-form').submit();" .
		"document.getElementById('frame-comic-image').style.display = 'block';\" id=\"comic-image\" />\n";
	  echo "    </form>\n";
	  echo "  </div>\n";
	  echo "  <center><input type=\"button\" class=\"submitButton\" id=\"comic-upload\" value=\"Upload\" disabled=\"disabled\" " .
		"onClick=\"processSubmitUpload();\" /></center>\n";
	  echo "</div>\n";
	  echo "  <div style=\"clear:both;\"></div>\n";
	}
}
?>