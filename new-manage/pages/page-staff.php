<?php
class GoavesManageStaff {
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[1]))
	    $path[1] = "landing";
	  switch (strtolower($path[1]))
	  {
	    case "change-password": return new GoavesManageStaffChangePassword();
	    case "change-email": return new GoavesManageStaffChangeEmail();
	    case "view-statistics": return new GoavesManageStaffStatistics();
		case "change-picture": return new GoavesManageStaffChangePicture();
		case "change-profile": return new GoavesManageStaffChangeProfile();
		case "manage-social-media": return new GoavesManageStaffChangeSocialMedia();
	    default: return new GoavesManagePhotoGalleriesList();
	  }
	}
	function getPageHandle() { return "staff"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Staff"; }
	function getBreadTrail() { return array("[this]" => "Staff"); }
	function getPageContents()
	{
	  echo "You should not be here";
	}
}
?>