<?php
class GoavesManageStaffChangeProfile {
	private $_profile;
	private $_profileUpdated = null;
	function checkOrRedirect($path, $database)
	{
	  if (isset($_POST["profile-text"]))
	  {
	    $profile_text = $database->escapeString(prepare_content_for_insert($_POST["profile-text"]));
		$this->_profileUpdated = $database->exec("UPDATE staff SET short_bio='" . $profile_text . "' WHERE identity='" .
			$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'");
	  }
	  $get_profile = $database->querySingle("SELECT short_bio FROM staff WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) .
		"' LIMIT 1");
	  $this->_profile = format_content($get_profile);
	  if (isset($profile_text))
	    $this->_profile = $_POST["profile-text"];
	  return true;
	}
	function getPageHandle() { return null; }
	function getPageSubhandle() { return "change-profile"; }
	function getPageTitle() { return "Change Profile"; }
	function getBreadTrail() { return array("staff" => "My Account", "[this]" => "Change Profile"); }
	function getPageStylesheet() { return "stylesheet-staff.css"; }
	function getPageJavascript()
	{
	  return "tinyMCE.init({
            mode : \"textareas\",
            entity_encoding : \"named\",
            theme : \"advanced\",
            skin : \"o2k7\",
            plugins : \"safari,spellchecker,pagebreak,style,layer,save,advhr,advimage,advlink," .
				"iespell,inlinepopups,preview,media,searchreplace,print,contextmenu,paste,directionality," .
				"fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template\",
            theme_advanced_buttons1 : \"cut,copy,paste,pastetext,|,search,replace,|,undo,redo,|,cleanup,code,|,preview,|,fullscreen,removeformat,visualaid,|,styleprops,spellchecker,|,cite,abbr,acronym,del,inc,attribs\",
            theme_advanced_buttons2 : \"bold,italic,underline,strikethrough,|,forecolor,link,unlink,|,sub,sup,|,charmap,iespell,|,bullist,numlist,|,outdent,indent\",
			theme_advanced_buttons3 : \"\",
            theme_advanced_toolbar_location : \"top\",
            theme_advanced_toolbar_align : \"left\",
            theme_advanced_statusbar_location : \"bottom\",
            theme_advanced_resizing : false,
            template_external_list_url : \"js/template_list.js\",
            external_link_list_url : \"js/link_list.js\",
            remove_trailing_nbsp : true,
            height : '400'});";
	}
	function getPageContents()
	{
	  if ($this->_profileUpdated != null)
	  {
	    if ($this->_profileUpdated)
		  echo "<a href=\"" . WEB_PATH . "/staff-portfolio/" . mb_strtolower($_SESSION[MANAGE_SESSION]["FIRST_NAME"] . "-" . $_SESSION[MANAGE_SESSION]["LAST_NAME"]) .
			"/\" target=\"_blank\"><div class=\"contentSuccess separatorBottom\" id=\"profile-update-result\"><b>Profile Updated.</b> Your staff portfolio profile " .
			"has been updated. Click on this to load your staff portfolio.</div></a>\n";
		else
		  echo "<div class=\"contentError separatorBottom\">Profile could not be updated.</b> Your profile could not be updated. The content you tried to post " .
			"has been saved and placed below so you may try again. Note, however, that this has NOT been saved.</div>\n";
	  }
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <div class=\"explainButton\" onClick=\"ManageWindow('explain','change-profile');\"></div>\n";
	  echo "  Change Staff Profile\n";
	  echo "</div>\n";
	  echo "<form method=\"POST\" action=\"" . MANAGE_WEB_PATH . "/staff/change-profile/\">\n";
	  echo "  <textarea style=\"width:100%;\" name=\"profile-text\">" . $this->_profile . "</textarea><br />\n";
	  echo "  <center>\n";
	  echo "  <input type=\"submit\" value=\"Post\" class=\"inputButton\" />\n";
	  echo "  </center>\n";
	  echo "</form>\n";
	}
}
?>