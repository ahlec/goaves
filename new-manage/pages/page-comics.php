<?php
class GoavesManageComics {
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[1]))
	    $path[1] = "landing";
	  switch ($path[1])
	  {
		case "upload": return new GoavesManageComicsUpload();
		case "manage": return new GoavesManageComicsManage();
	    case "list": return new GoavesManageComicsList();
		default: return new GoavesManageComicsList();
	  }
	  return true;
	}
	function getRequirePermission() { return "5"; }
	function getPageHandle() { return "comic"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Comics"; }
	function getBreadTrail() { return array("[this]" => "Comics"); }
	function getPageStylesheet() { return "stylesheet-comics.css"; }
	function getPageContents()
	{
	  echo "comics";
	}
}
?>