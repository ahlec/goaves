<?php
class GoavesManageBeatList {
	private $_postedBeat = null;
	private $_deletedBeat = null;
	private $_forSchoolYear = null;
	private $_yearsWithBeats = array();
	private $_beats = array();
	private $_relatedGroups = array();
	function checkOrRedirect($path, $database)
	{
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]) && $_SESSION[MANAGE_TRANSFER_DATA]["FROM"] == "page-beat-compose" &&
	    $_SESSION[MANAGE_TRANSFER_DATA]["SUCCESS"])
	  {
	    $posted_information = $database->querySingle("SELECT handle, title FROM beats WHERE beat_identity='" .
	    	$database->escapeString($_SESSION[MANAGE_TRANSFER_DATA]["IDENTITY"]) . "' LIMIT 1", true);
	    $this->_postedBeat = array("HANDLE" => $posted_information["handle"],
	    	"TITLE" => format_content($posted_information["title"]));
	    unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]) && $_SESSION[MANAGE_TRANSFER_DATA]["FROM"] == "delete-content.php" &&
	    $_SESSION[MANAGE_TRANSFER_DATA]["SUCCESS"])
		{
		  $this->_deletedBeat = $_SESSION[MANAGE_TRANSFER_DATA]["TITLE"];
		  unset ($_SESSION[MANAGE_TRANSFER_DATA]);
		}
	  
	  if (isset($path[2]) && strlen(strval($path[2])) == 4 && ctype_digit($path[2]))
	    $this->_forSchoolYear = $database->escapeString($path[2]);
	  else if (isset($path[1]) && strlen(strval($path[1])) == 4 && ctype_digit($path[1]))
	    $this->_forSchoolYear = $database->escapeString($path[1]);
	  else
	    $this->_forSchoolYear = (date("m") < 6 ? date("Y") - 1 : date("Y"));
	
	  $unique_year_months = $database->query("SELECT DISTINCT SUBSTR(post_time, 0, 4) AS \"year\", SUBSTR(post_time, 6, 2) AS \"month\" " .
		"FROM beats WHERE staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'");
	  while ($year_month = $unique_year_months->fetchArray())
	  {
	    $year_in_question = $year_month["year"];
	    if ($year_month["month"] < 6)
		  $year_in_question -= 1;
		if (!in_array($year_in_question, $this->_yearsWithBeats))
		  $this->_yearsWithBeats[] = $year_in_question;
	  }
	
	  $related_groups_to_load = array();
	  $all_beats = $database->query("SELECT beat_identity, beats.handle, beats.title, post_time, " .
	  	"published, last_updated, related_group, topic, parameters, icon_file, image_file FROM beats WHERE staff_identity='" .
	  	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' AND (post_time >= '" . $this->_forSchoolYear . "-06' AND post_time < '" .
		($this->_forSchoolYear + 1) . "-06') ORDER BY post_time DESC");
	  while ($beat = $all_beats->fetchArray())
	  {
	    $date_created = strtotime($beat["post_time"]);
		$school_year_start_posted = date("Y", $date_created);
		if (date("m", $date_created) < 6)
		  $school_year_start_posted -= 1;
	    $this->_beats[] = array("IDENTITY" => $beat["beat_identity"], "HANDLE" => $beat["handle"],
	    	"TITLE" => format_content($beat["title"]), "POST_TIME" => strtotime($beat["post_time"]),
	    	"PUBLISHED" => ($beat["published"] == "TRUE"), "LAST_UPDATED" => strtotime($beat["last_updated"]),
	    	"RELATED_GROUP" => ($beat["related_group"] != "" ? $beat["related_group"] : null), 
	    	"TOPIC" => $beat["topic"], "PARAMETERS" => $beat["parameters"],
	    	"ICON_FILE" => ($beat["icon_file"] != "" ? $beat["icon_file"] : $beat["image_file"]),
			"SCHOOL_YEAR" => $school_year_start_posted . " - " . ($school_year_start_posted + 1));
	    if (!in_array($beat["related_group"], $related_groups_to_load))
	      $related_groups_to_load[] = $beat["related_group"];
	  }
	  
	  if (sizeof($related_groups_to_load) > 0)
	  {
	    $load_groups_query = "SELECT group_identity, groups.handle, title, group_types.singular_name AS \"group_type_name\", " .
	    	"group_types.handle AS \"group_type_handle\", active FROM groups JOIN group_types ON groups.type = " .
	    	"group_types.type_identity WHERE group_identity IN (";
	    foreach ($related_groups_to_load as $index => $group_identity)
	      $load_groups_query .= "'" . $group_identity . "'" . ($index < sizeof($related_groups_to_load) - 1 ? ", " : "");
	    $load_groups_query .= ") ORDER BY group_identity ASC";
	    $referenced_groups = $database->query($load_groups_query);
	    while ($group = $referenced_groups->fetchArray())
	      $this->_relatedGroups[$group["group_identity"]] = array("HANDLE" => $group["handle"],
	      	"TITLE" => format_content($group["title"]), "ACTIVE" => ($group["active"] == "TRUE"),
	      	"TYPE_NAME" => $group["group_type_name"], "TYPE_HANDLE" => $group["group_type_handle"]);
	      	
	  }
	  return true;
	}
	function getPageHandle() { return "beat"; }
	function getPageSubhandle() { return "list"; }
	function getPageTitle() { return "Past Beats"; }
	function getBreadTrail() { return array("beat" => "Beats", "[this]" => "Past Beats"); }
	function getPageStylesheet() { return "stylesheet-beat.css"; }
	function getBodyOnload() { return ($this->_deletedBeat != null ? "fadeOutAndHide('beat-delete-success','3000');" : null); }
	function getPageJavascript()
	{
	  return "function toggleSubgroup(month)
	  {
	    var monthContainer = document.getElementById('container-' + month);
		var monthHeader = document.getElementById('header-' + month);
	    if (monthContainer.className.indexOf('subgroupCollapsed') > -1)
		{
		  monthContainer.className = monthContainer.className.replace('subgroupCollapsed', 'subgroupExpanded');
		  monthHeader.title = 'Collapse';
		} else
		{
		  monthContainer.className = monthContainer.className.replace('subgroupExpanded', 'subgroupCollapsed');
		  monthHeader.title = 'Expand';
		}
	  }";
	}
	function getPageContents()
	{
	  if ($this->_postedBeat !== null)
	  {
	    echo "<a href=\"" . WEB_PATH . "/article/" . $this->_postedBeat["HANDLE"] . "/\" target=\"_blank\" " .
	    	"onClick=\"fadeOutAndHide('beat-posted-success');\" id=\"beat-posted-success\"><div class=\"contentSuccess " .
	    	"separatorBottom\"><b>Beat Posted.</b> '" . $this->_postedBeat["TITLE"] . "' has been posted. Click this to view " .
	    	"the beat.</div></a>\n";
	  }
	  if ($this->_deletedBeat !== null)
	    echo "<div class=\"contentSuccess separatorBottom\" id=\"beat-delete-success\"><b>Beat Deleted.</b> The beat '" . $this->_deletedBeat .
			"' has been deleted.</div>\n";
	
	  echo "<div class=\"contentHeader\">\n";
	  echo "  <div class=\"explainButton\" onClick=\"ManageWindow('explain','beat-list');\"></div>\n";
	  echo "  Past Beats\n";
	  echo "</div>\n";
	  echo "<div class=\"contentText miniSeparator\">Select a beat from below.</div>\n";
	  echo "<div class=\"beatYearCalendar\">\n";
	  echo "  <div class=\"currentYear\">" . $this->_forSchoolYear . " - " . ($this->_forSchoolYear + 1) . " beats</div>\n";
	  echo "  <div class=\"otherYears\">\n";
	  for ($year_index = 0; $year_index < sizeof($this->_yearsWithBeats); $year_index++)
	  {
	    if ($this->_yearsWithBeats[$year_index] == $this->_forSchoolYear)
		{
		  echo "    <b>" . $this->_yearsWithBeats[$year_index] . "-" . ($this->_yearsWithBeats[$year_index] + 1) . "</b>" . ($year_index < sizeof($this->_yearsWithBeats)
			- 1 ? " &bull; " : "") . "\n";
		} else
	      echo "    <a href=\"" . MANAGE_WEB_PATH . "/beat/list/" . $this->_yearsWithBeats[$year_index] . "/\">" . $this->_yearsWithBeats[$year_index] . 
			"-" . ($this->_yearsWithBeats[$year_index] + 1) . "</a>" . ($year_index < sizeof($this->_yearsWithBeats) - 1 ? " &bull; " : "") . "\n";
	  }
	  echo "  </div>\n";
	  echo "</div>\n";
	  echo "<div class=\"beatListingContainer listingContainer\">\n";
	  $previous_month = null;
	  foreach ($this->_beats as $beat)
	  {
	    if ($previous_month == null || $previous_month != date("m", $beat["POST_TIME"]))
		{
		  if ($previous_month != null)
		  {
		    echo "    </div>\n";
		    echo "  </div>\n";
		  }
		  echo "  <div class=\"monthContainer " . ($previous_month != null ? "separator subgroupCollapsed" : "subgroupExpanded") . "\" id=\"container-" .
			strtolower(date("F", $beat["POST_TIME"])) . "\">\n";
		  echo "    <div class=\"monthHeader\" onClick=\"toggleSubgroup('" . strtolower(date("F", $beat["POST_TIME"])) . "');\" id=\"header-" .
			strtolower(date("F", $beat["POST_TIME"])) . "\" title=\"" . ($previous_month != null ? "Expand" : "Collapse") . "\">\n";
		  echo "      <div class=\"collapseButton\"></div>\n";
		  echo "      " . date("F", $beat["POST_TIME"]) . "\n";
		  echo "    </div>\n";
		  echo "    <div class=\"subgroupContainer\">\n";
		  $previous_month = date("m", $beat["POST_TIME"]);
		}
		echo "  <a href=\"" . MANAGE_WEB_PATH . "/beat/manage/" . $beat["IDENTITY"] . "/\">\n";
	    echo "  <div class=\"beat\">\n";
	    echo "    <img src=\"" . WEB_PATH . "/images/articles/" . $beat["ICON_FILE"] . "\" class=\"beatIcon\" />\n";
	    echo "    <div class=\"beatTitle\">" . $beat["TITLE"] . (!$beat["PUBLISHED"] ? " <span class=\"unpublished\">" .
	    	"[Unpublished]" : "") . "</div>\n";
	    echo "    <div class=\"beatInfo\">\n";
	    echo "      <span class=\"infoLabel\">Posted</span> " . date(LONG_DATETIME_FORMAT, $beat["POST_TIME"]) . "<br />\n";
	    if ($beat["POST_TIME"] != $beat["LAST_UPDATED"])
	      echo "      <span class=\"infoLabel\">Last Updated</span> " . date(LONG_DATETIME_FORMAT,
	      	$beat["LAST_UPDATED"]) . "<br />\n";
	    if ($beat["RELATED_GROUP"] != null)
	      echo "      <span class=\"infoLabel\">Related Group(s):</span> " .
	      	$this->_relatedGroups[$beat["RELATED_GROUP"]]["TITLE"] . " (" .
	      	$this->_relatedGroups[$beat["RELATED_GROUP"]]["TYPE_NAME"] . ")<br />\n";
	    echo "    </div>\n";
	    echo "    <div style=\"clear:both;\"></div>\n";
	    echo "  </div>\n";
		echo "  </a>\n";
	  }
	  echo "    </div>\n";
	  echo "  </div>\n";
	  echo "</div>\n";
	}
}
?>