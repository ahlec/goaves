<?php
class GoavesManageSoundslidesCreate {
	private $_relatedGroupTypes = array();
	private $_topics = array();
	function checkOrRedirect($path, $database)
	{
	  if (isset($path[2]) && strpos($path[2], "success-") === 0)
	  {
	    $supposed_success = $database->escapeString(str_replace("success-", "", $path[2]));
		if ($database->querySingle("SELECT count(*) FROM sound_slides WHERE slideshow_identity='" . $supposed_success . "' AND staff_identity='" .
			$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") > 0)
			{
			  $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-soundslides-create", "SUCCESS" => true, "IDENTITY" => $supposed_success);
			  header("Location: " . MANAGE_WEB_PATH . "/soundslide/manage/" . $supposed_success);
			  exit();
			}
	  }
	  $group_types = $database->query("SELECT type_identity, plural_name, handle FROM group_types ORDER BY type_identity ASC");
	  while ($group_type = $group_types->fetchArray())
	  {
	    $get_groups = $database->query("SELECT group_identity, title, active FROM groups WHERE type='" .
			$group_type["type_identity"] . "' ORDER BY title ASC");
		$groups_with_type = array();
		while ($group = $get_groups->fetchArray())
		  $groups_with_type[] = array("IDENTITY" => $group["group_identity"], "TITLE" => format_content($group["title"]),
			"ACTIVE" => ($group["active"] == "TRUE"));
	    $this->_relatedGroupTypes[$group_type["handle"]] = array("NAME" => $group_type["plural_name"],
			"GROUPS" => $groups_with_type);
	  }
	  
	  $topics = $database->query("SELECT topic_identity, title FROM topics WHERE (date_start <= '" .
		date("Y-m-d", strtotime("+1 hours")) . "' AND date_end >= '" . date("Y-m-d", strtotime("+1 hours")) . "') OR (" .
		"date_start IS NULL AND date_end IS NULL) ORDER BY title ASC");
	  while ($topic = $topics->fetchArray())
	    $this->_topics[] = array("IDENTITY" => $topic["topic_identity"], "TITLE" => format_content($topic["title"]));
	  return true;
	}
	function getRequirePermission() { return "6"; }
	function getPageHandle() { return "soundslide"; }
	function getPageSubhandle() { return "create"; }
	function getPageTitle() { return "Create New Soundslide"; }
	function getBreadTrail() { return array("soundslide" => "Soundslides", "[this]" => "Create New Soundslide Presentation"); }
	function getPageStylesheet() { return "stylesheet-soundslides.css"; }
	function getPageOnBeforeUnload() { return "return getWarnPageRefresh();"; }
	function getPageJavascript()
	{
	  $group_handles = "[";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	    $group_handles .= "'" . $handle . "',";
	  $group_handles = rtrim($group_handles, ",") . "]";
	  return "var gallery_created = false;
	  var group_types = " . $group_handles . ";
	  function toggleUseRelatedGroup()
	  {
	    document.getElementById('soundslide-related-group-type').selectedIndex = 0;
	    document.getElementById('soundslide-related-group-type').style.display =
			(document.getElementById('soundslide-related-group-use').value ? 'block' : 'none');
		for (var type_index = 0; type_index < group_types.length; type_index++)
		  document.getElementById('soundslide-related-group-' + group_types[type_index]).style.display = 'none';
	  }
	  function toggleUseTopic()
	  {
	    document.getElementById('soundslide-topic').selectedIndex = 0;
		document.getElementById('soundslide-topic').style.display = (document.getElementById('soundslide-topic-use').value ? 'block' : 'none');
	  }
	  function selectGroupByType(type_handle)
	  {
	    for (var type_index = 0; type_index < group_types.length; type_index++)
		  document.getElementById('soundslide-related-group-' + group_types[type_index]).style.display = 'none';
	    if (type_handle == '[none]')
		  return;
		document.getElementById('soundslide-related-group-' + type_handle).selectedIndex = 0;
	    document.getElementById('soundslide-related-group-' + type_handle).style.display = 'block';
	  }
	  function checkFormValid()
	  {
	    if (document.getElementById('soundslide-title').className == 'inputText formComponentValid' &&
			document.getElementById('audio-filepath').value.length > 0)
		{
		  document.getElementById('soundslide-create').disabled = false;
		  return;
		}
		document.getElementById('soundslide-create').disabled = true;
	  }
	  function getWarnPageRefresh()
	  {
	    if (gallery_created)
		  return null;
	    if (document.getElementById('soundslide-title').value == '')
		  return null;
		return 'Are you sure you wish to leave this page? (Unsaved content will be lost unless you save it)';
	  }
	  function processSubmitCreate()
	  {
	    if (document.getElementById('soundslide-create').disabled)
		  return;
		var createCall = web_path + '/components/create-soundslide.php?title=' + escapeStringURL(document.getElementById('soundslide-title').value) + 
			'&audio=' + document.getElementById('audio-filepath').value;
		if (document.getElementById('soundslide-related-group-use').value)
		{
		  var selected_group_type = document.getElementById('soundslide-related-group-type').value;
		  if (selected_group_type != '[none]' && document.getElementById('soundslide-related-group-' + selected_group_type).value != '[none]')
		    createCall += '&related_group=' + document.getElementById('soundslide-related-group-' + selected_group_type).value;
		}
		if (document.getElementById('soundslide-topic') != undefined && document.getElementById('soundslide-topic-use').value &&
		  document.getElementById('soundslide-topic').value != '[none]')
		  createCall += '&topic=' + document.getElementById('soundslide-topic').value;
		executeAJAX(createCall, function processCreate(result)
		{
		  if (result.indexOf('[Error]') < 0)
		  {
		    gallery_created = true;
		    window.location = '" . MANAGE_WEB_PATH . "/soundslide/create/success-' + result + '/';
			return;
		  }
		  alert(result);
		});
	  }";
	}
	function getPageContents()
	{
	  echo "<div class=\"contentHeader\">Create New Soundslide Presentation</div>\n";
	  
	  echo "<div class=\"createForm\">\n";
	  echo "  <div class=\"formLabel\">Title:</div>\n";
	  echo "  <input type=\"text\" class=\"inputText\" name=\"soundslide-title\" id=\"soundslide-title\" autocomplete=\"off\" onKeyUp=\"executeAJAX(web_path + '/components/" .
		"check-title.php?type=soundslide&minimum=6&title=' + encodeURIComponent(this.value), function " .
		"processTitle(return_value) {
		  if (return_value.indexOf('[Error]') >= 0)
		  {
		    document.getElementById('soundslide-title-error').innerHTML = return_value.replace('[Error] ', '');
			if (document.getElementById('soundslide-title-error').isFading != true)
			{
		      document.getElementById('soundslide-title-error').style.display = 'block';
			  fadeOutAndHide('soundslide-title-error', 2400);
		    }
			document.getElementById('soundslide-title').className = 'inputText formComponentInvalid';
			document.getElementById('soundslide-create').disabled = true;
		  } else
		  {
		    document.getElementById('soundslide-title-error').style.display = 'none';
		    document.getElementById('soundslide-title').className = 'inputText formComponentValid';
			checkFormValid();
		  } });\" />\n";
	  echo "  <div class=\"contentError\" id=\"soundslide-title-error\" style=\"display:none;\"></div>\n";
	  
	  echo "<iframe id=\"frame-soundslide-audio\" name=\"audioForm\" class=\"formFrame\" style=\"display:none;\"></iframe>\n";
	  echo "  <div class=\"formLabel separator\">Audio File:</div>\n";
	  echo "  <input type=\"hidden\" id=\"audio-filepath\" value=\"\" />\n";
	  echo "  <div class=\"inputFile\" id=\"audio-input\">\n";
	  echo "    <div class=\"button\">Select</div>\n";
	  echo "    <div class=\"textBox\" id=\"audio-input-textbox\"></div>\n";
	  echo "    <form method=\"POST\" enctype=\"multipart/form-data\" target=\"audioForm\" action=\"" . MANAGE_WEB_PATH . "/components/upload-soundslide-audio.php\" " .
		"id=\"upload-soundslide-form\">\n";
	  echo "      <input type=\"file\" class=\"inputFile\" name=\"soundslide-audio\" onChange=\"document.getElementById('upload-soundslide-form').submit();" .
		"document.getElementById('upload-soundslide-form').disabled = true;\" id=\"soundslide-audio\" />\n";
	  echo "    </form>\n";
	  echo "  </div>\n";
	  
	  echo "  <div class=\"formLabel separator\">Related Group:</div>\n";
	  echo "  <input type=\"checkbox\" id=\"soundslide-related-group-use\" onChange=\"toggleUseRelatedGroup();\" /> " .
		"<label for=\"soundslide-related-group-use\">Link to a related group?</label><br />\n";
	  echo "  <select id=\"soundslide-related-group-type\" style=\"display:none;\" class=\"inputSelect selectLeft\" " .
		"onChange=\"selectGroupByType(this.value);\">\n";
	  echo "    <option value=\"[none]\">(Group Type)</option>\n";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	    echo "    <option value=\"" . $handle . "\">" . $info["NAME"] . "</option>\n";
	  echo "  </select>\n";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	  {
	    echo "  <select id=\"soundslide-related-group-" . $handle .
			"\" class=\"inputSelect selectRight\" style=\"display:none;\">\n";
		echo "    <option value=\"[none]\">(Group)</option>\n";
		foreach ($info["GROUPS"] as $group)
		  echo "    <option value=\"" . $group["IDENTITY"] . "\">" . $group["TITLE"] .
			(!$group["ACTIVE"] ? " (Inactive)" : "") . "</option>\n";
	    echo "  </select>\n";
	  }
	  echo "  <div class=\"formLabel separator\" style=\"clear:both;\">Topic:</div>\n";
	  if (sizeof($this->_topics) > 0)
	  {
	    echo "  <input type=\"checkbox\" id=\"soundslide-topic-use\" onChange=\"toggleUseTopic();\" /> " .
		"<label for=\"soundslide-topic-use\">Link to a topic?</label><br />\n";
	    echo "  <select id=\"soundslide-topic\" class=\"inputSelect\" style=\"display:none;\">\n";
	    echo "    <option value=\"[none]\">(Topic)</option>\n";
	    foreach ($this->_topics as $topic)
	      echo "    <option value=\"" . $topic["IDENTITY"] . "\">" . $topic["TITLE"] . "</option>\n";
	    echo "  </select>\n";
	  } else
	    echo "  <div>There are no topics currently active.</div>\n";
	  echo "  <center><input type=\"button\" class=\"submitButton\" id=\"soundslide-create\" value=\"Create\" disabled=\"disabled\" " .
		"onClick=\"processSubmitCreate();\" /></center>\n";
	  echo "</div>\n";
	  echo "  <div style=\"clear:both;\"></div>\n";
	}
}
?>