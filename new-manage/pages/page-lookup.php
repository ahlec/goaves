<?php
class GoavesManageLookup {
	private $_results = array();
	function checkOrRedirect($path, $database)
	{
	  return true;
	}
	
	function getPageHandle() { return null; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Staff"; }
	function getBreadTrail() { return array("[this]" => "Content Lookup"); }
	function getPageStylesheet() { return "stylesheet-lookup.css"; }
	function getPageJavascript()
	{
	  return "function queryLookup(parameter)
	  {
	    var queryCall = web_path + '/components/lookup-search.php?query=' + escapeStringURL(parameter);
		executeAJAX(queryCall, function display(results)
		{
		  document.getElementById('lookup-results').innerHTML = results;
		});
	  }";
	}
	function getPageContents()
	{
	  echo "<div class=\"lookupSearchBox\"><input type=\"text\" id=\"lookup-search\" onKeyUp=\"queryLookup(this.value);\" /></div>\n";
	  echo "<div id=\"lookup-results\" class=\"lookupResultsContainer\">\n";
	  echo "</div>\n";
	}
}
?>