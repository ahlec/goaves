<?php
class GoavesManageStaffChangeSocialMedia {
	private $_links = array();
	private $_linkDefinitions = array();
	function checkOrRedirect($path, $database)
	{
	  $get_linkings = $database->query("SELECT link_identity, url, link_type, sort_order FROM staff_links WHERE staff_identity='" .
		$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' ORDER BY sort_order ASC");
	  while ($linking = $get_linkings->fetchArray())
	    $this->_links[] = array("IDENTITY" => $linking["link_identity"], "URL" => $linking["url"], "TYPE" => $linking["link_type"], "ORDER" => $linking["sort_order"]);
	  
	  $get_link_definitions = $database->query("SELECT definition_identity, title, handle, icon_file, base_url FROM staff_link_definitions");
	  while ($link_definition = $get_link_definitions->fetchArray())
	    $this->_linkDefinitions[$link_definition["handle"]] = array("IDENTITY" => $link_definition["definition_identity"], "TITLE" => format_content($link_definition["title"]),
			"HANDLE" => $link_definition["handle"], "ICON" => $link_definition["icon_file"], "URL" => $link_definition["base_url"]);
	  return true;
	}
	function getPageHandle() { return "staff"; }
	function getPageSubhandle() { return "manage-social-media"; }
	function getPageTitle() { return "Manage social media accounts"; }
	function getBreadTrail() { return array("staff" => "My Account", "[this]" => "Manage social media accounts"); }
	function getPageStylesheet() { return "stylesheet-staff.css"; }
	function getPageContents()
	{
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <div class=\"explainButton\" onClick=\"ManageWindow('explain','manage-social-media');\"></div>\n";
	  echo "  Manage Social Media Accounts\n";
	  echo "</div>\n";
	  
	  if (sizeof($this->_links) > 0)
	  {
	    echo "<div class=\"socialMediaContainer\">\n";
		foreach ($this->_links as $link)
		{
		  echo "  <div class=\"linkContainer\">\n";
		  echo "    <img src=\"" . WEB_PATH . "/images/icons/" . $this->_linkDefinitions[$link["TYPE"]]["ICON"] .
			"\" class=\"icon\" />\n";
		  echo "    <div class=\"title\">" . $this->_linkDefinitions[$link["TYPE"]]["TITLE"] . "</div>\n";
		  echo "    <a href=\"" . $link["URL"] . "\" target=\"_blank\" />" . $link["URL"] . "</a>\n";
		  echo "  </div>\n";
		}
		echo "</div>\n";
	  } else
	    echo "<div class=\"contentText\">Your staff account is not linked with any other websites.</div>\n";
	  echo "<div class=\"editLink separator\">\n";
	  echo "  <span class=\"fauxLink firstAction\" onClick=\"ManageWindow('add-staff-link');\"><img src=\"" . MANAGE_WEB_PATH .
		"/interface/images/link_add.png\" class=\"actionLink\" /> Add Link</span>\n";
	  echo "</div>\n";
	  echo "<pre>\n";
	  print_r($this);
	  echo "</pre>\n";
	}
}
?>