<?php
class GoavesManageBeatManage {
	private $_beatIdentity;
	private $_title;
	private $_handle;
	private $_postTime;
	private $_contents;
	private $_imageFile;
	private $_iconFile;
	private $_imageAlignment;
	private $_imageCaption;
	private $_type;
	private $_published;
	private $_dateLastUpdated;
	private $_relatedGroup;
	private $_photoCredit;
	private $_topic;
	private $_parameters;
	private $_schoolYear;
	private $_isFeature = false;
	private $_isAwaitingFeaturing = false;
	private $_featureDate = null;
	private $_special;
	
	private $_featuringSuccess = null;
	private $_unfeaturingSuccess = null;
	private $_editSuccess = null;
	private $_publishToggled = null;
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM beats WHERE beat_identity='" .
		$database->escapeString($path[2]) . "'" . (isModeratorOver("1") ? "" : " AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) .
		"'")) == 0)
	  {
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-beats-manage", "LOADED" => false, "IDENTITY" => $path[2]);
		return new GoavesManageBeatList();
	  }
	  if (isset($path[3]) && strtolower($path[3]) == "edit-information")
	    return new GoavesManageBeatEditInfo();
	  if (isset($path[3]) && strtolower($path[3]) == "edit-text")
	    return new GoavesManageBeatEditText();
	  
	  $info = $database->querySingle("SELECT beat_identity, beats.handle, beats.title, post_time, contents, beats.image_file, beats.icon_file, image_alignment, image_caption, " .
		"beats.type, published, last_updated, related_group, photo_credit, beats.special_identity, parameters, beat_types.icon_file AS \"type_icon_file\", " .
		"beat_types.handle AS \"type_handle\", beat_types.title AS \"type_title\", groups.handle AS \"related_group_handle\", " .
		"groups.title AS \"related_group_title\", specials.handle AS \"special_handle\", specials.title AS \"special_title\" FROM beats LEFT JOIN " .
		"beat_types ON beat_types.type_identity = beats.type LEFT JOIN groups ON groups.group_identity = beats.related_group LEFT JOIN " .
		"specials ON specials.special_identity = beats.special_identity WHERE beat_identity='" .
	  	$database->escapeString($path[2]) . "' LIMIT 1", true);
	  $this->_beatIdentity = $info["beat_identity"];
	  $this->_title = format_content($info["title"]);
	  $this->_handle = $info["handle"];
	  $this->_postTime = strtotime($info["post_time"]);
	  $this->_contents = format_content($info["contents"]);
	  $this->_imageFile = ($info["image_file"] != "" ? $info["image_file"] : null);
	  $this->_iconFile = ($info["icon_file"] != "" ? $info["icon_file"] : null);
	  $this->_imageAlignment = $info["image_alignment"];
	  $this->_imageCaption = format_content($info["image_caption"]);
	  $this->_type = array("IDENTITY" => $info["type"], "HANDLE" => $info["type_handle"], "TITLE" => format_content($info["type_title"]),
		"ICON_FILE" => $info["type_icon_file"]);
	  $this->_published = ($info["published"] == "TRUE");
	  $this->_dateLastUpdated = strtotime($info["last_updated"]);
	  $this->_relatedGroup = ($info["related_group"] != "" ? array("IDENTITY" => $info["related_group"],
	  	"HANDLE" => $info["related_group_handle"], "TITLE" => format_content($info["related_group_title"])) : null);
	  $this->_special = ($info["special_identity"] != "" ? array("IDENTITY" => $info["special_identity"], "HANDLE" => $info["special_handle"],
		"TITLE" => format_content($info["special_title"])) : null);
	  /*$this->_topic = ($info["topic"] != "" ? array("IDENTITY" => $info["topic"], "HANDLE" => $info["topic_handle"],
	  	"TITLE" => format_content($info["topic_title"])) : null);*/
	  
	  $feature_info = $database->querySingle("SELECT date_featured FROM features WHERE feature_type='article' " .
		"AND item_identity='" . $this->_beatIdentity . "' LIMIT 1");
	  $this->_isFeature = ($feature_info != null && $feature_info != false);
	  $this->_featureDate = strtotime($feature_info);
	  
	  $this->_isAwaitingFeaturing = ($database->querySingle("SELECT count(*) FROM manage_requests LEFT JOIN manage_requests_data ON (manage_requests.request_identity = " .
			"manage_requests_data.request_identity AND manage_requests_data.data_handle='item_identity') WHERE manage_requests.request_type='feature_content' " .
			"AND data='" . $this->_beatIdentity . "' AND manage_requests.request_type_supplement='article'") > 0);
	  
	  
	  $this->_schoolYear = (date("m", $this->_postTime) <= 6 ? date("Y", $this->_postTime) - 1 : date("Y", $this->_postTime));
	  
	  $this->_photoCredit = ($info["photo_credit"] != "" ? $info["photo_credit"] : null);
	  
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]))
	  {
	    switch ($_SESSION[MANAGE_TRANSFER_DATA]["FROM"])
		{
		  case "toggle-publish.php": $this->_publishToggled = array("TOGGLED" => true, "TO_VALUE" => $_SESSION[MANAGE_TRANSFER_DATA]["NEW_VALUE"]); break;
		  case "update-beat-info": $this->_editSuccess = "info"; break;
		  case "update-beat-text": $this->_editSuccess = "text"; break;
		  case "set-feature-component.php":
		  {
		    $this->_featuringSuccess = true;
			break;
		  }
		  case "remove-feature.php": $this->_unfeaturingSuccess = true; break;
		}
		unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  return true;
	}
	function getRequirePermission() { return "1"; }
	function getPageHandle() { return "beat"; }
	function getPageSubhandle() { return "manage"; }
	function getPageTitle() { return "[Beat] '" . $this->_title . "'"; }
	function getBreadTrail() { return array("beat" => "Beats", "beat/list" => "My Beats",
		"[this]" => "'" . $this->_title . "'"); }
	function getPageStylesheet() { return "stylesheet-beat.css"; }
	function getBodyOnload() { return ($this->_publishToggled != null ? "fadeOutAndHide('toggle-publish-success', 3000); " : null) .
		($this->_editSuccess != null ? "fadeOutAndHide('edit-success', 3000); " : null) . ($this->_unfeaturingSuccess != null ? "fadeOutAndHide('unfeature-success', " .
		"3000);" : null); }
	function getPageJavascript()
	{
	  return "function togglePublish(beat_identity)
	  {
	    executeAJAX(web_path + '/components/toggle-publish.php?type=beat&identity=' + beat_identity, function results(value)
		{
		  if (value == 'success')
		  {
		    window.location = web_path + '/beat/manage/' + beat_identity + '/';
			return;
		  }
		  alert(value);
		});
	  }
	  function togglePictureManageLinks(to_value)
	  {
	    document.getElementById('picture-action-edit').className = (to_value ? '' : 'actionHidden');
		document.getElementById('picture-action-delete').className = 'fauxLink' + (to_value ? '' : ' actionHidden');
	  }";
	}
	function getPageContents()
	{	  
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <a class=\"returnLink\" href=\"" . MANAGE_WEB_PATH . "/beat/list/" . $this->_schoolYear . "/\">&laquo; Return</a>\n";
	  echo "  [Beat] '" . $this->_title . "'\n";
	  echo "</div>\n";
	  
	  if ($this->_publishToggled != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"toggle-publish-success\"><b>" . ($this->_publishToggled["TO_VALUE"] == "TRUE" ?
			"Publish" : "Unpublish") . " success.</b> This beat has been " . ($this->_publishToggled["TO_VALUE"] == "TRUE" ? "published" : "unpublished") .
			".</div>\n";
	  if ($this->_editSuccess != null)
	  {
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"edit-success\"><b>Edit success.</b> The ";
		switch ($this->_editSuccess)
		{
		  case "info": echo "general information"; break;
		  case "text": echo "text"; break;
		}
		echo " for this beat has been changed successfully.</div>\n";
	  }
	  if ($this->_featuringSuccess != null)
	  {
	    if ($this->_featuringSuccess)
		  echo "<div class=\"contentSuccess separator separatorBottom\"><b>Beat featured.</b> This beat is now a featured piece on the homepage.</div>\n";
		else
		  echo "<div class=\"contentError separator separatorBottom\"><b>Beat could not be featured.</b> This beat could not be featured on the homepage.</div>\n";
	  }
	  if ($this->_unfeaturingSuccess != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"unfeature-success\"><b>Featured status removed.</b> The featured status on " .
			"this beat has been removed.</div>\n";
			
	  echo "<div class=\"indentedContent\">\n";
	  if ($this->_imageFile != null || $this->_iconFile != null)
	    echo "  <img src=\"" . WEB_PATH . "/images/articles/" . ($this->_iconFile != null ? $this->_iconFile : $this->_imageFile) . "\" class=\"beatManageIcon " .
	  	  "separator\" />\n";
	  echo "  <div class=\"infoLine\"><b>Title:</b> " . $this->_title . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Identity:</b> " . $this->_beatIdentity . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Link:</b> " . ($this->_published ? "<a href=\"" . WEB_PATH . "/article/" .
	  	$this->_handle . "/\" target=\"_blank\">" . WEB_PATH . "/article/" . $this->_handle .
	  	"/</a> (Published)" : ($this->_isAwaitingFeaturing ? "<span class=\"awaitingFeaturing\">Featuring pending...</span>" :
		"<span class=\"unpublished\">Unpublished</span>")) . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Post Date/Time:</b> " . date(LONG_DATETIME_FORMAT, $this->_postTime) . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Date Last Updated:</b> " . date(LONG_DATETIME_FORMAT, $this->_dateLastUpdated) . "</div>\n";
	  if ($this->_isFeature)
	    echo "  <div class=\"infoLine separator\"><b>Featured:</b> " . date(DATE_FORMAT, $this->_featureDate) . "</div>\n";
	  
	  echo "  <div class=\"infoLine separator\"><b>Beat Category:</b> <a href=\"" . WEB_PATH . "/news/" . $this->_type["HANDLE"] . "/\" target=\"_blank\" />" .
		$this->_type["TITLE"] . "</a></div>\n";
	  echo "  <div class=\"infoLine\"><b>Related Group:</b> " . ($this->_relatedGroup != null ? "<a href=\"" .
	  	WEB_PATH . "/group/" . $this->_relatedGroup["HANDLE"] . "/\" target=\"_blank\">" . $this->_relatedGroup["TITLE"] .
	  	"</a>" : "None") . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Special:</b> " . ($this->_special != null ? "<a href=\"" . WEB_PATH . "/special/" .
	  	$this->_special["HANDLE"] . "/\" target=\"_blank\">" . $this->_special["TITLE"] . "</a>" : "None") . "</div>\n";
	  echo "</div>\n";
	  echo "<div style=\"clear:both;\"></div>\n";
	  echo "<div class=\"editLink separator\"><a href=\"" . MANAGE_WEB_PATH .
	  	"/beat/manage/" . $this->_beatIdentity . "/edit-information/\" class=\"firstAction\"><img src=\"" . MANAGE_WEB_PATH .
	  	"/interface/images/action_go.gif\" class=\"actionIcon\" border=\"0\" /> Edit Information</a> \n";
	  echo "<span class=\"fauxLink\" onClick=\"togglePublish('" . $this->_beatIdentity . "');\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/flag_" .
			($this->_published ? "red" : "green") . ".gif\" class=\"actionIcon\" border=\"0\" /> " . ($this->_published ? "Unpublish" : "Publish") . "</span>";
	  echo " \n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('feature-content', 'beat-" . $this->_beatIdentity . "');\"><img src=\"" . MANAGE_WEB_PATH .
		"/interface/images/" . ($this->_isFeature ? "feature_remove.png" : "feature_add.png") . "\" class=\"" .
		"actionIcon\" border=\"0\" /> " . ($this->_isFeature ? "Remove " : "") . "Feature</span>\n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('delete-content', 'beat-" . $this->_beatIdentity .
	  	"');\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/folder_delete.gif\" class=\"actionIcon\" border=\"0\" /> Delete</span>\n";
	  echo "</div>\n";
	  echo "<div class=\"dividingBar miniSeparator\"></div>\n";
	  echo "  <div class=\"indentedContent\">\n";
	  echo "    " . $this->_contents . "\n";
	  echo "  </div>\n";
	  echo "<div class=\"editLink separator\"><a href=\"" . MANAGE_WEB_PATH .
	  	"/beat/manage/" . $this->_beatIdentity . "/edit-text/\" class=\"firstAction\"><img src=\"" . MANAGE_WEB_PATH .
	  	"/interface/images/action_go.gif\" class=\"actionIcon\" border=\"0\" /> Edit Text</a> \n";
	  echo "</div>\n";
	  echo "<div class=\"dividingBar miniSeparator\"></div>\n";
	  echo "<div class=\"indentedContent\">\n";
	  echo "  <div class=\"infoLine separator\"><b>Beat Image:</b> " . ($this->_imageFile != null ? "<a href=\"" . WEB_PATH . "/images/articles/" .
		$this->_imageFile . "\" target=\"_blank\">" . WEB_PATH . "/images/articles/" . $this->_imageFile . "</a>" :
		"(None)") . "</div>\n";
	  if ($this->_imageFile != null)
	    echo "  <img src=\"" . WEB_PATH . "/images/articles/" . $this->_imageFile . "\" class=\"beatDisplayImage\" />\n";
	  echo "  <div class=\"infoLine separator\"><b>Beat Icon:</b> " . ($this->_iconFile != null ? "<a href=\"" . WEB_PATH . "/images/articles/" .
		$this->_iconFile . "\" target=\"_blank\">" . WEB_PATH . "/images/articles/" . $this->_iconFile . "</a>" :
		"(None)") . "</div>\n";
	  if ($this->_iconFile != null)
	    echo "  <img src=\"" . WEB_PATH . "/images/articles/" . $this->_iconFile . "\" class=\"beatDisplayImage\" />\n";
	  echo "<div class=\"infoLine separator\"><b>Image Alignment:</b> " . $this->_imageAlignment . "</div>\n";
	  echo "<div class=\"infoLine\"><b>Image Caption:</b> " . ($this->_imageCaption != null ? $this->_imageCaption : "(None)") . "</div>\n";
	  echo "<div class=\"infoLine\"><b>Photo Credit:</b> " . ($this->_photoCredit != null ? $this->_photoCredit : "(None)") . "</div>\n";
	  echo "</div>\n";
	}
}
?>