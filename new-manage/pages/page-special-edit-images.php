<?php
class GoavesManageSpecialEditImages {
	private $_specialIdentity;
	private $_title;
	private $_listingStyle;
	private $_bannerImage;
	private $_listingImage;
	
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM specials WHERE special_identity='" .
		$database->escapeString($path[2]) . "'") == 0)
	  {
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-special-edit-images", "LOADED" => false, "IDENTITY" => $path[2]);
		return new GoavesManageSpecialManage();
	  }
	  
	  $info = $database->querySingle("SELECT special_identity, handle, title, active, ended, listing_image, banner_image, " .
		"listing_style FROM specials WHERE special_identity='" . $database->escapeString($path[2]) . "' LIMIT 1", true);
		
	  $this->_specialIdentity = $info["special_identity"];
	  $this->_title = format_content($info["title"]);
	  $this->_listingStyle = $info["listing_style"];
	  $this->_bannerImage = $info["banner_image"];
	  $this->_listingImage = $info["listing_image"];
	  return true;
	}
	function getRequirePermission() { return PERM_SPECIALS; }
	function getPageHandle() { return "special"; }
	function getPageSubhandle() { return "manage"; }
	function getPageTitle() { return "[Special] Edit '" . $this->_title . "'"; }
	function getBreadTrail() { return array("special" => "Specials Listing", 
		("special/manage/" . $this->_specialIdentity) => "'" . $this->_title . "'", "[this]" => "Edit Images"); }
	function getPageStylesheet() { return "stylesheet-special.css"; }
	function getBodyOnload() { return null; }
	function getPageJavascript()
	{
	  return "var special_updated = false;
	  function changeListingStyle()
	  {
	    var new_layout = document.getElementById('listing-style').value;
		var old_layout = (new_layout == 'horizontal' ? 'vertical' : 'horizontal');
	    if (document.getElementById('original_listing_style').value != document.getElementById('listing-style').value)
		{
		  document.getElementById('change-listing-image').checked = true;
		  document.getElementById('change-listing-image').disabled = true;
		  document.getElementById('listing_image_container_' + new_layout).style.display = 'block';
		  document.getElementById('listing_image_container_' + old_layout).style.display = 'none';
		  toggleChangeListingImage();
		} else
		{
		  document.getElementById('change-listing-image').checked = false;
		  document.getElementById('change-listing-image').disabled = false;
		  document.getElementById('listing_image_container_' + old_layout).style.display = 'none';
		  document.getElementById('listing_image_container_' + new_layout).style.display = 'none';
		}
	  }
	  function toggleChangeListingImage()
	  {
	    document.getElementById('listing_image_container_' + document.getElementById('listing-style').value).style.display = (document.getElementById('change-listing-image').value ? 'block' : 'none');
	  }
	  function toggleChangeBanner()
	  {
	    document.getElementById('banner-change-container').style.display = (document.getElementById('change-banner-image').value ? 'block' : 'none');
	  }
	  function checkFormValid()
	  {
	    if ((!document.getElementById('change-banner-image').value || document.getElementById('banner-banner').value.length > 0))
		{
		  document.getElementById('special-edit').disabled = false;
		  return;
		}
		document.getElementById('special-edit').disabled = true;
	  }
	  function getWarnPageRefresh()
	  {
	    if (special_updated)
		  return null;
	    if (document.getElementById('special-title').value == '')
		  return null;
		return 'Are you sure you wish to leave this page? (Unsaved content will be lost unless you save it)';
	  }
	  function processSubmitUpdate()
	  {
	    if (document.getElementById('special-edit').disabled)
		  return;
		var editCall = web_path + '/components/update-special-images.php?identity=" . $this->_specialIdentity . "';
		if (document.getElementById('change-banner-image').value)
		  editCall += '&banner=' + encodeURIComponent(document.getElementById('banner-banner').value);
		if (document.getElementById('original_listing_style').value != document.getElementById('listing-style').value)
		  editCall += '&listing_style=' + encodeURIComponent(document.getElementById('listing-style').value);
		if (document.getElementById('change-listing-image').value)
		  editCall += '&listing_image=' + encodeURIComponent(document.getElementById(document.getElementById('listing-style').value + '-listing-banner').value);
		ManageWindow('process-in-action');
		alert(editCall);
		executeAJAX(editCall, function processCreate(result)
		{
		  if (result == 'success')
		  {
		    special_updated = true;
		    window.location = '" . MANAGE_WEB_PATH . "/special/manage/" . $this->_specialIdentity . "/';
			return;
		  }
		  alert(result);
		  openWindow.closeWindow();
		});
	  }";
	}
	function getPageContents()
	{	  
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <a class=\"returnLink\" href=\"" . MANAGE_WEB_PATH . "/special/manage/" . $this->_specialIdentity . "/\">&laquo; Return</a>\n";
	  echo "  Edit Special '" . $this->_title . "'\n";
	  echo "</div>\n";
			
	  echo "<div class=\"createForm noWindow\">\n";
	  echo "  <input type=\"hidden\" id=\"original_listing_style\" value=\"" . strtolower($this->_listingStyle) . "\" />\n";
	  echo "  <div class=\"infoLine separator\"><b>Listing Style:</b> <select id=\"listing-style\" onChange=\"changeListingStyle();checkFormValid();\"><option value=\"vertical\"" . ($this->_listingStyle == "VERTICAL" ?
		" selected=\"selected\"" : "") . ">Vertical</option><option value=\"horizontal\"" . ($this->_listingStyle == "HORIZONTAL" ? " selected=\"selected\"" : "") .
		">Horizontal</option></select></div>\n";
	  
	  require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
	  
	  echo "  <div class=\"infoLine separator\"><b>Listing Image:</b></div>\n";
	  echo "  <img src=\"" . WEB_PATH . "/images/specials/" . $this->_listingImage . "\" class=\"updateIcon\" /><br />\n";
	  echo "  <input type=\"checkbox\" id=\"change-listing-image\" onChange=\"toggleChangeListingImage();checkFormValid();\" />" .
		"<label for=\"change-listing-image\">Change listing image?</label><br />\n";
		
	  echo "  <div id=\"listing_image_container_horizontal\" style=\"display:none;\">\n";
	  echo "    <input type=\"hidden\" id=\"listing_image_horizontal_original\" />\n";
	  echo "    <input type=\"hidden\" id=\"horizontal-listing-banner\" />\n";
	  UploadImageInput("horizontal_listing_image", "HListing Image", "horizontal-listing-banner", "listing_image_horizontal_original", 670, 150, "checkFormValid()");
	  echo "  </div>\n";
	  
	  echo "  <div id=\"listing_image_container_vertical\" style=\"display:none;\">\n";
	  echo "    <input type=\"hidden\" id=\"listing_image_vertical_original\" />\n";
	  echo "    <input type=\"hidden\" id=\"vertical-listing-banner\" />\n";
	  UploadImageInput("vertical_listing_image", "VListing Image", "vertical-listing-banner", "listing_image_vertical_original", 325, 150, "checkFormValid()");
	  echo "  </div>\n";
	  
	  echo "  <div class=\"infoLine separator\"><b>Banner Image:</b></div>\n";
	  echo "  <img src=\"" . WEB_PATH . "/images/specials/" . $this->_bannerImage . "\" class=\"updateIcon\" /><br />\n";
	  echo "  <input type=\"checkbox\" id=\"change-banner-image\" onChange=\"toggleChangeBanner();checkFormValid();\" /><label for=\"change-banner-image\">Change banner image?</label><br />\n";
	  echo "  <div style=\"display:none;\" id=\"banner-change-container\">\n";
	  echo "  <input type=\"hidden\" id=\"banner_image_original\" />\n";
	  echo "  <input type=\"hidden\" id=\"banner-banner\" />\n";
	  UploadImageInput("banner_image", "Banner Image", "banner-banner", "banner_image_original", 600, 200, "checkFormValid()");
	  echo "  </div>\n";
	  
	  echo "  <center><input type=\"button\" class=\"submitButton separator\" id=\"special-edit\" " .
		"value=\"Update\" onClick=\"processSubmitUpdate();\" /></center>\n";
	  echo "</div>\n";
	  echo "<div style=\"clear:both;\"></div>\n";
	}
}
?>