<?php
class GoavesManageVideos {
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[1]))
	    $path[1] = "landing";
	  switch ($path[1])
	  {
	    default: return new GoavesManageVideosList();
	  }
	  return true;
	}
	function getRequirePermission() { return PERM_VIDEOS; }
	function getPageHandle() { return "video"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Videos"; }
	function getBreadTrail() { return array("[this]" => "Videos"); }
	function getPageStylesheet() { return "stylesheet-videos.css"; }
	function getPageContents()
	{
	  echo "videos";
	}
}
?>