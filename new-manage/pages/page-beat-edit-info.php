<?php
class GoavesManageBeatEditInfo {
	private $_beatIdentity;
	private $_title;
	private $_handle;
	private $_relatedGroup;
	private $_dateCreated;
	private $_special;
	private $_beatType;
	
	private $_categories = array();
	private $_relatedGroupTypes = array();
//	private $_activeTopics = array();
	private $_allSpecials = array();
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM beats WHERE beat_identity='" .
		$database->escapeString($path[2]) . "' AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
	  {
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-beat-edit-info", "LOADED" => false, "IDENTITY" => $path[2]);
		return new GoavesManageBeatManage();
	  }
	  
	  $info = $database->querySingle("SELECT beat_identity, beats.handle, beats.title, post_time, contents, beats.image_file, beats.icon_file, image_alignment, image_caption, " .
		"beats.type, published, last_updated, related_group, photo_credit, beats.special_identity, parameters, beat_types.icon_file AS \"type_icon_file\", " .
		"beat_types.handle AS \"type_handle\", beat_types.title AS \"type_title\", groups.handle AS \"related_group_handle\", " .
		"groups.title AS \"related_group_title\", groups.type AS \"related_group_type\", specials.handle AS \"special_handle\", specials.title AS \"special_title\" " .
		"FROM beats LEFT JOIN " .
		"beat_types ON beat_types.type_identity = beats.type LEFT JOIN groups ON groups.group_identity = beats.related_group LEFT JOIN " .
		"specials ON specials.special_identity = beats.special_identity WHERE beat_identity='" .
	  	$database->escapeString($path[2]) . "' LIMIT 1", true);
	  $this->_beatIdentity = $info["beat_identity"];
	  $this->_title = format_content($info["title"]);
	  $this->_handle = $info["handle"];
	  $this->_published = ($info["published"] == "TRUE");
	  $this->_relatedGroup = ($info["related_group"] != "" ? array("IDENTITY" => $info["related_group"],
	  	"HANDLE" => $info["related_group_handle"], "TITLE" => format_content($info["related_group_title"]), "TYPE" => $info["related_group_type"]) : null);
	  /*$this->_topic = ($info["topic"] != "" ? array("IDENTITY" => $info["topic"], "HANDLE" => $info["topic_handle"],
	  	"TITLE" => format_content($info["topic_title"])) : null);*/
		
	  $this->_special = ($info["special_identity"] != "" ? array("IDENTITY" => $info["special_identity"], "HANDLE" => $info["special_handle"],
		"TITLE" => format_content($info["special_title"])) : null);
	  $this->_beatType = array("IDENTITY" => $info["type"], "TITLE" => format_content($info["type_title"]), "HANDLE" => $info["type_handle"],
		"ICON" => $info["type_icon_file"]);
	  
	  $valid_post_categories = "'" . rtrim(str_replace("|", "','", $database->querySingle("SELECT valid_article_types FROM staff " .
	  	"WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' LIMIT 1")), ",'") . "'";
	  $get_categories = $database->query("SELECT type_identity, title, icon_file FROM beat_types WHERE " .
	  	"type_identity IN (" . $valid_post_categories . ") ORDER BY sort_order ASC");
	  while ($category = $get_categories->fetchArray())
	    $this->_categories[] = array("IDENTITY" => $category["type_identity"], "TITLE" => format_content($category["title"]),
	    	"ICON" => $category["icon_file"]);
	  
	  $group_types = $database->query("SELECT type_identity, plural_name, handle FROM group_types ORDER BY type_identity ASC");
	  while ($group_type = $group_types->fetchArray())
	  {
	    $get_groups = $database->query("SELECT group_identity, title, active FROM groups WHERE type='" .
			$group_type["type_identity"] . "' ORDER BY title ASC");
		$groups_with_type = array();
		while ($group = $get_groups->fetchArray())
		  $groups_with_type[] = array("IDENTITY" => $group["group_identity"], "TITLE" => format_content($group["title"]),
			"ACTIVE" => ($group["active"] == "TRUE"));
	    $this->_relatedGroupTypes[$group_type["handle"]] = array("NAME" => $group_type["plural_name"],
			"GROUPS" => $groups_with_type, "IDENTITY" => $group_type["type_identity"]);
	  }
	  
	  /*$topics = $database->query("SELECT topic_identity, title FROM topics WHERE (date_start <= '" .
		date("Y-m-d", strtotime("+1 hours")) . "' AND date_end >= '" . date("Y-m-d", strtotime("+1 hours")) . "') OR (" .
		"date_start IS NULL AND date_end IS NULL) ORDER BY title ASC");
	  while ($topic = $topics->fetchArray())
	    $this->_activeTopics[] = array("IDENTITY" => $topic["topic_identity"], "TITLE" => format_content($topic["title"]));*/
	  $get_specials = $database->query("SELECT special_identity, title, active, ended FROM specials ORDER BY active DESC, title ASC");
	  while ($special = $get_specials->fetchArray())
	    $this->_allSpecials[] = array("IDENTITY" => $special["special_identity"], "TITLE" => format_content($special["title"]),
			"ACTIVE" => ($special["active"] == "TRUE"), "ENDED" => ($special["active"] == "TRUE" ? null : strtotime($special["ended"])));
	  return true;
	}
	function getRequirePermission() { return "1"; }
	function getPageHandle() { return "beat"; }
	function getPageSubhandle() { return "manage"; }
	function getPageTitle() { return "[Beat] Edit '" . $this->_title . "'"; }
	function getBreadTrail() { return array("beat" => "Beats", "beat/list" => "My Beats",
		("beat/manage/" . $this->_beatIdentity) => "'" . $this->_title . "'", "[this]" => "Edit"); }
	function getPageStylesheet() { return "stylesheet-beat.css"; }
	function getBodyOnload() { return null; }
	function getPageJavascript()
	{
	  $group_handles = "[";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	    $group_handles .= "'" . $handle . "',";
	  $group_handles = rtrim($group_handles, ",") . "]";
	  return "var beat_updated = false;
	  var group_types = " . $group_handles . ";
	  function selectCategory(category_identity)
	  {
	    document.getElementById('article-type').value = category_identity;
	    var categoryIcons = document.getElementById('beatTypes').getElementsByTagName('img');
	    for (var icon = 0; icon < categoryIcons.length; icon++)
		  categoryIcons[icon].className = 'categoryIcon categoryUnselected';
	    document.getElementById('beatType-' + category_identity).className = 'categoryIcon categorySelected';
	  }
	  function toggleUseRelatedGroup()
	  {
	    document.getElementById('beat-related-group-type').selectedIndex = 0;
	    document.getElementById('beat-related-group-type').style.display =
			(document.getElementById('beat-related-group-use').value ? 'block' : 'none');
		for (var type_index = 0; type_index < group_types.length; type_index++)
		  document.getElementById('beat-related-group-' + group_types[type_index]).style.display = 'none';
	  }
	  function toggleUseSpecial()
	  {
	    document.getElementById('beat-special').selectedIndex = 0;
		document.getElementById('beat-special').style.display = (document.getElementById('beat-special-use').value ? 'block' : 'none');
	  }
	  function selectGroupByType(type_handle)
	  {
	    for (var type_index = 0; type_index < group_types.length; type_index++)
		  document.getElementById('beat-related-group-' + group_types[type_index]).style.display = 'none';
	    if (type_handle == '[none]')
		  return;
		document.getElementById('beat-related-group-' + type_handle).selectedIndex = 0;
	    document.getElementById('beat-related-group-' + type_handle).style.display = 'block';
	  }
	  function selectNewIcon(photo_identity, photo_icon)
	  {
	    document.getElementById('beat-icon').value = photo_icon;
	    var icons = document.getElementById('selectionIcons').getElementsByTagName('img');
        for (var icon = 0; icon < icons.length; icon++)
          icons[icon].className = 'icon';
		document.getElementById('icon-' + photo_identity).className = \"icon currentIcon\";
	  }
	  function checkFormValid()
	  {
	    if (document.getElementById('beat-title').className == 'inputText formComponentValid')
		{
		  document.getElementById('beat-edit').disabled = false;
		  return;
		}
		document.getElementById('beat-edit').disabled = true;
	  }
	  function getWarnPageRefresh()
	  {
	    if (beat_updated)
		  return null;
	    if (document.getElementById('beat-title').value == '')
		  return null;
		return 'Are you sure you wish to leave this page? (Unsaved content will be lost unless you save it)';
	  }
	  function processSubmitUpdate()
	  {
	    if (document.getElementById('beat-edit').disabled)
		  return;
		var createCall = web_path + '/components/update-beat-info.php?identity=" . $this->_beatIdentity . "';
		createCall += '&title=' + encodeURIComponent(document.getElementById('beat-title').value);
		if (document.getElementById('beat-related-group-use').value)
		{
		  var selected_group_type = document.getElementById('beat-related-group-type').value;
		  if (selected_group_type != '[none]' && document.getElementById('beat-related-group-' + selected_group_type).value != '[none]')
		    createCall += '&related_group=' + document.getElementById('beat-related-group-' + selected_group_type).value;
		  else
		    createCall += '&related_group=none';
		} else
		  createCall += '&related_group=none';
		if (document.getElementById('beat-special') != undefined && document.getElementById('beat-special-use').value &&
		  document.getElementById('beat-special').value != '[none]')
		  createCall += '&special=' + document.getElementById('beat-special').value;
		else
		  createCall += '&special=none';
		if (document.getElementById('article-type').value != undefined && document.getElementById('article-type').value != null &&
			document.getElementById('article-type').value.length > 0)
			  createCall += '&type=' + document.getElementById('article-type').value;
		ManageWindow('process-in-action');
		executeAJAX(createCall, function processCreate(result)
		{
		  if (result == 'success')
		  {
		    beat_updated = true;
		    window.location = '" . MANAGE_WEB_PATH . "/beat/manage/" . $this->_beatIdentity . "/';
			return;
		  }
		  alert(result);
		  openWindow.closeWindow();
		});
	  }";
	}
	function getPageContents()
	{	  
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <a class=\"returnLink\" href=\"" . MANAGE_WEB_PATH . "/beat/manage/" . $this->_beatIdentity . "/\">&laquo; Return</a>\n";
	  echo "  Edit Beat '" . $this->_title . "'\n";
	  echo "</div>\n";
			
	  echo "<div class=\"createForm noWindow\">\n";
	  echo "  <div class=\"infoLine\"><b>Title:</b> " . $this->_title . "</div>\n";
	  echo "  <input type=\"text\" class=\"inputText\" name=\"beat-title\" id=\"beat-title\" autocomplete=\"off\" onKeyUp=\"executeAJAX(web_path + '/components/" .
		"check-title.php?type=beat&minimum=6&title=' + escapeStringURL(this.value) + '&identity=' + " . $this->_beatIdentity . ", function " .
		"processTitle(return_value) {
		  if (return_value.indexOf('[Error]') >= 0)
		  {
		    document.getElementById('beat-title-error').innerHTML = return_value.replace('[Error] ', '');
			if (document.getElementById('beat-title-error').isFading != true)
			{
		      document.getElementById('beat-title-error').style.display = 'block';
			  fadeOutAndHide('beat-title-error', 2400);
		    }
			document.getElementById('beat-title').className = 'inputText formComponentInvalid';
			document.getElementById('beat-edit').disabled = true;
		  } else
		  {
		    document.getElementById('beat-title-error').style.display = 'none';
		    document.getElementById('beat-title').className = 'inputText formComponentValid';
			checkFormValid();
		  } });\" value=\"" . addslashes($this->_title) . "\" />\n";
	  echo "  <div class=\"contentError\" id=\"beat-title-error\" style=\"display:none;\"></div>\n";
	  
	  echo "  <div class=\"infoLine separator\"><b>Beat Category:</b> " . $this->_beatType["TITLE"] . "</div>\n";
	  echo "  <input type=\"hidden\" id=\"article-type\" value=\"" . ($this->_beatType != null ? $this->_beatType["IDENTITY"] : (sizeof($this->_categories) == 1 ? 
	  	$this->_categories[0]["IDENTITY"] : "")) . "\" />\n";
	  echo "  <div id=\"beatTypes\">\n";
	  foreach ($this->_categories as $category)
	  {
	    echo "  <img src=\"" . WEB_PATH . "/images/article_types/" . ($category["ICON"] != "" ? $category["ICON"] :
	    	"no-category-icon.jpg") . "\" class=\"categoryIcon" . ($this->_beatType != null ? ($this->_beatType["IDENTITY"] == $category["IDENTITY"] ?
			" categorySelected" : " categoryUnselected") : "") . "\" onClick=\"selectCategory('" .
	    	$category["IDENTITY"] . "');\" title=\"" . $category["TITLE"] . "\" id=\"beatType-" .
	    	$category["IDENTITY"] . "\"/>\n";
	  }
	  echo "</div>\n";
	  
	  echo "  <div class=\"infoLine separator\"><b>Related Group:</b> " . ($this->_relatedGroup != null ? $this->_relatedGroup["TITLE"] : "None") . "</div>\n";
	  echo "  <input type=\"checkbox\" id=\"beat-related-group-use\" onChange=\"toggleUseRelatedGroup();\" " . ($this->_relatedGroup != null ?
		"checked=\"checked\" " : "") . "/> " .
		"<label for=\"beat-related-group-use\">Link to a related group?</label><br />\n";
	  echo "  <select id=\"beat-related-group-type\" " . ($this->_relatedGroup != null ? "" : "style=\"display:none;\" ") . "class=\"inputSelect selectLeft\" " .
		"onChange=\"selectGroupByType(this.value);\">\n";
	  echo "    <option value=\"[none]\">(Group Type)</option>\n";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	  {
	    echo "    <option value=\"" . $handle . "\" " . ($this->_relatedGroup != null && $this->_relatedGroup["TYPE"] == $info["IDENTITY"] ? "selected=\"selected\" " : "") .
			">" . $info["NAME"] . "</option>\n";
	  }
	  echo "  </select>\n";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	  {
	    echo "  <select id=\"beat-related-group-" . $handle .
			"\" class=\"inputSelect selectRight\" " . ($this->_relatedGroup != null && $this->_relatedGroup["TYPE"] == $info["IDENTITY"] ? "" :
			"style=\"display:none;\" ") . ">\n";
		echo "    <option value=\"[none]\">(Group)</option>\n";
		foreach ($info["GROUPS"] as $group)
		  echo "    <option value=\"" . $group["IDENTITY"] . "\"" . ($this->_relatedGroup != null && $this->_relatedGroup["IDENTITY"] == $group["IDENTITY"] ?
			" selected=\"selected\"" : "") . ">" . $group["TITLE"] .
			(!$group["ACTIVE"] ? " (Inactive)" : "") . "</option>\n";
	    echo "  </select>\n";
	  }
	  echo "  <div style=\"clear:both;\"></div>\n";
		
	  echo "  <div class=\"infoLine separator\"><b>Special:</b> " . ($this->_special != null ? $this->_special["TITLE"] : "None") . "</div>\n";
	  if (sizeof($this->_allSpecials) > 0)
	  {
	    echo "  <input type=\"checkbox\" id=\"beat-special-use\" onChange=\"toggleUseSpecial();\" " . ($this->_special != null ? "checked=\"checked\" " : "") . "/> " .
		"<label for=\"beat-special-use\">Include within a special?</label><br />\n";
	    echo "  <select id=\"beat-special\" class=\"inputSelect\"" . ($this->_special != null ? "" : " style=\"display:none;\"") . ">\n";
	    echo "    <option value=\"[none]\">(Special)</option>\n";
	    foreach ($this->_allSpecials as $special)
	      echo "    <option value=\"" . $special["IDENTITY"] . "\"" . ($this->_special != null && $this->_special["IDENTITY"] == $special["IDENTITY"] ?
			" selected=\"selected\"" : "") . ">" . $special["TITLE"] . "</option>\n";
	    echo "  </select>\n";
		echo "  <div style=\"clear:both;\"></div>\n";
	  } else
	    echo "  <div>There are no specials within the database.</div>\n";
		
	  echo "  </div>\n";
	  echo "  <center><input type=\"button\" class=\"submitButton separator\" id=\"beat-edit\" " .
		"value=\"Update\" onClick=\"processSubmitUpdate();\" /></center>\n";
	  echo "</div>\n";
	  echo "<div style=\"clear:both;\"></div>\n";
	}
}
?>