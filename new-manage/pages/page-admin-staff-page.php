<?php
class GoAvesManageAdminStaffPage {
	private $_staffIdentity;
	private $_firstName;
	private $_lastName;
	private $_positions;
	private $_imageFile;
	private $_active;
	private $_permissions;
	private $_iconFile;
	private $_displayOnStaffListing;
	private $_email;
	private $_displayEmailOnPortfolio;
	private $_yearGraduating;
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM staff WHERE identity='" . $database->escapeString($path[2]) . "'") == 0)
		return new GoavesManageAdminStaffListing();
	  $info = $database->querySingle("SELECT identity, first_name, last_name, positions, image_file, active, permissions, icon_file, display_on_staff_listing, " .
		"email, display_email_on_portfolio, year_graduating FROM staff WHERE identity='" . $database->escapeString($path[2]) . "' LIMIT 1", true);
	  if (isset($path[3]))
	  {
	    if ($path[3] === "permissions")
		  return new GoavesManageAdminStaffChangePermissions();
	  }
	  $this->_staffIdentity = $info["identity"];
	  $this->_firstName = format_content($info["first_name"]);
	  $this->_lastName = format_content($info["last_name"]);
	  $this->_positions = format_content($info["positions"]);
	  $this->_imageFile = $info["image_file"];
	  $this->_active = ($info["active"] == "TRUE");
	  $this->_permissions = $info["permissions"];
	  $this->_iconFile = $info["icon_file"];
	  $this->_displayOnStaffListing = ($info["display_on_staff_listing"] == "TRUE");
	  $this->_email = ($info["email"] != "" ? $info["email"] : null);
	  $this->_displayEmailOnPortfolio = ($info["display_email_on_portfolio"] == "TRUE");
	  $this->_yearGraduating = $info["year_graduating"];
	  return true;
	}
	function getRequirePermission() { return PERM_ADMIN; }
	function getPageHandle() { return "admin"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "[Admin]"; }
	function getBreadTrail() { return array("admin" => "[Admin Level]", "admin/staff" => "Staff", "admin/staff/listing" => "Staff Listing",
		"[this]" => $this->_lastName . ", " . $this->_firstName); }
	function getPageStylesheet() { return "stylesheet-admin.css"; }
	function getPageContents()
	{
	  echo "<pre>\n";
	  print_r($this);
	  echo "</pre>\n";
	  echo "<a href=\"" . MANAGE_WEB_PATH . "/admin/staff/" . $this->_staffIdentity . "/permissions/\">Permissions</a><br />\n";
	  echo "<manage:button>Delete</manage:button><br />\n";
	  echo "<div class=\"button red\" onClick=\"ManageWindow('delete-content','staff-" . $this->_staffIdentity . "');\"><img class=\"icon\" />Delete</div>\n";
	}
}
?>