<?php
class GoavesManageVideosList {
	private $_videos = array();
	private $_successWithPost = null;
	private $_deletedGallery = null;
	function checkOrRedirect($path, $database)
	{
	  $all_videos = $database->query("SELECT video_identity, title, published, date_published, image_thumbnail, date_last_updated FROM videos WHERE staff_identity='" .
		$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) .
		"' ORDER BY date_published DESC");
	  while ($video = $all_videos->fetchArray())
	  {
	    $date_created = strtotime($video["date_published"]);
		$school_year_start_posted = date("Y", $date_created);
		if (date("m", $date_created) < 6)
		  $school_year_start_posted -= 1;
	    $this->_videos[] = array("IDENTITY" => $video["video_identity"], 
	    	"TITLE" => format_content($video["title"]), "DATE_PUBLISHED" => $date_created,
	    	"PUBLISHED" => ($video["published"] == "TRUE"),
			"SCHOOL_YEAR" => $school_year_start_posted . " - " . ($school_year_start_posted + 1),
			"DATE_LAST_UPDATED" => strtotime($video["date_last_updated"]), "ICON_FILE" => $video["image_thumbnail"]);
	  }
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]) && $_SESSION[MANAGE_TRANSFER_DATA]["FROM"] == "page-photo-galleries-create" &&
	  	$_SESSION[MANAGE_TRANSFER_DATA]["SUCCESS"])
	  {
		  $this->_successWithPost = $database->querySingle("SELECT title FROM photo_galleries WHERE gallery_identity='" .
			$database->escapeString($_SESSION[MANAGE_TRANSFER_DATA]["IDENTITY"]) . "' LIMIT 1");
		  unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]) && $_SESSION[MANAGE_TRANSFER_DATA]["FROM"] == "delete-content.php")
	  {
	    $this->_deletedGallery = $_SESSION[MANAGE_TRANSFER_DATA]["TITLE"];
	    unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  
	  return true;
	}
	function getRequirePermission() { return PERM_VIDEOS; }
	function getPageHandle() { return "video"; }
	function getPageSubhandle() { return "list"; }
	function getPageTitle() { return "My Videos"; }
	function getBreadTrail() { return array("video" => "Videos", "[this]" => "My Videos"); }
	function getPageStylesheet() { return "stylesheet-galleries.css"; }
	function getBodyOnload()
	{
	  if ($this->_deletedGallery != null)
	    return "fadeOutAndHide('gallery-deleted-success', 3000);";
	  return null;
	}
	function getPageContents()
	{
	  if ($this->_successWithPost != null)
		  echo "<div class=\"contentSuccess separatorBottom\" id=\"create-success\" onLoad=\"fadeOutAndHide('create-success', 3000);\"><b>Creation Successful.</b>" .
			"The gallery '" . $this->_successWithPost . "' has been created.</div>\n";
	  if ($this->_deletedGallery != null)
	    echo "<div class=\"contentSuccess separatorBottom\" id=\"gallery-deleted-success\"><b>Deletion successful.</b> Gallery '" .
	    	$this->_deletedGallery . "' and all photos within have been deleted.</div>\n";
	  echo "<div class=\"contentHeader\">My Videos</div>\n";
	  
	  if (sizeof($this->_videos) == 0)
	  {
	    echo "<div class=\"contentText\">You have no videos</div>\n";
		echo "<div class=\"contentText miniSeparator\"><a href=\"" . MANAGE_WEB_PATH .
			"/video/upload/\">Upload a video</a></div>\n";
		return;
	  }
	  echo "<div class=\"contentText\">Select a video from below.</div>\n";
	  echo "<div class=\"listingContainer\">\n";
	  $last_school_year = null;
	  foreach ($this->_videos as $video)
	  {
	    if ($last_school_year == null || $last_school_year != $video["SCHOOL_YEAR"])
		{
		  if ($last_school_year != null)
		    echo "  </div>\n";
		  echo "  <div class=\"yearContainer" . ($last_school_year != null ? " separator" : "") . "\">\n";
		  echo "    <div class=\"yearHeader\">" . $video["SCHOOL_YEAR"] . "</div>\n";
		  $last_school_year = $video["SCHOOL_YEAR"];
		}
		echo "  <a href=\"" . MANAGE_WEB_PATH . "/video/manage/" . $video["IDENTITY"] . "/\" class=\"item\" style=\"background-image:url('" .
			WEB_PATH . "/images/video_thumbnails/" . $video["ICON_FILE"] . "');\">\n";
		echo "    <div class=\"overlay\"></div>\n";
		echo "    <div class=\"status " . ($video["PUBLISHED"] ? "statusPublished" : "statusUnpublished") . "\"></div>\n";
		echo "    <div class=\"title\">" . $video["TITLE"] . "</div>\n";
		echo "  </a>\n";
	  }
	  echo "  </div>\n";
	  echo "</div>\n";
	}
}
?>