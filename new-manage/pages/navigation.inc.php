<?php
function GenerateNavigationMenuByPermissions($permissions)
{
  $navigation_menu = array();  
  if (substr($permissions, PERM_BEATS, 1) > 0)//$valid_permissions[1] > "0")
  {
    $beat_menu = new GoavesManageNavigationGroup("Beat", "beat");
    $beat_menu->addItem(new GoavesManageNavigationItem("Post New Beat", "compose"));
    $beat_menu->addItem(new GoavesManageNavigationItem("Edit Past Beats", "list"));
    $beat_menu->addItem(new GoavesManageNavigationItem("View Personal Beat Statistics", "personal-statistics"));
    $navigation_menu[] = $beat_menu;
  }
  if (substr($permissions, PERM_GALLERIES, 1) > 0)//$valid_permissions[2] === "1")
  {
    $gallery_menu = new GoavesManageNavigationGroup("Photo Galleries", "photo-gallery");
    $gallery_menu->addItem(new GoavesManageNavigationItem("Create New Gallery", "create"));
    $gallery_menu->addItem(new GoavesManageNavigationItem("Edit Galleries", "list"));
    $gallery_menu->addItem(new GoavesManageNavigationItem("View Gallery Statistics", "statistics"));
    $navigation_menu[] = $gallery_menu;
  }
  if (substr($permissions, PERM_PODCASTS, 1) > 0)//$valid_permissions[3] === "1")
  {
    $podcast_menu = new GoavesManageNavigationGroup("Podcasts", "podcast");
    $podcast_menu->addItem(new GoavesManageNavigationItem("Upload New Podcast", "upload"));
    $podcast_menu->addItem(new GoavesManageNavigationItem("Edit Podcasts", "list"));
    $podcast_menu->addItem(new GoavesManageNavigationItem("View Personal Podcast Statistics", "personal-statistics"));
    $navigation_menu[] = $podcast_menu;
  }
  if (substr($permissions, PERM_VIDEOS, 1) > 0)//substr($permissions, PERM_BEATS, 1) > 0)//$valid_permissions[4] === "1")
  {
    $videos_menu = new GoavesManageNavigationGroup("Videos", "video");
    $videos_menu->addItem(new GoavesManageNavigationItem("Create New Video", "create"));
    $videos_menu->addItem(new GoavesManageNavigationItem("Edit Videos", "list"));
    $videos_menu->addItem(new GoavesManageNavigationItem("View Video Statistics", "statistics"));
    $navigation_menu[] = $videos_menu;
  }
  if (substr($permissions, PERM_COMICS, 1) > 0)//substr($permissions, PERM_BEATS, 1) > 0)//$valid_permissions[5] === "1")
  {
    $comics_menu = new GoavesManageNavigationGroup("Comics", "comic");
    $comics_menu->addItem(new GoavesManageNavigationItem("Upload New Comic", "upload"));
    $comics_menu->addItem(new GoavesManageNavigationItem("Edit Past Comics", "list"));
    $comics_menu->addItem(new GoavesManageNavigationItem("View Personal Comics Statistics", "personal-statistics"));
    $navigation_menu[] = $comics_menu;
  }
  if (substr($permissions, PERM_SOUNDSLIDES, 1) > 0)//$valid_permissions[6] === "1")
  {
    $soundslides_menu = new GoavesManageNavigationGroup("Soundslides", "soundslide");
    $soundslides_menu->addItem(new GoavesManageNavigationItem("Create New Soundslide", "create"));
    $soundslides_menu->addItem(new GoavesManageNavigationItem("Edit Past Soundslides", "list"));
    $soundslides_menu->addItem(new GoavesManageNavigationItem("View Personal Soundslides Statistics", "personal-statistics"));
    $navigation_menu[] = $soundslides_menu;
  }
  if (substr($permissions, PERM_LEAF, 1) > 0)
  {
    $leaf_menu = new GoavesManageNavigationGroup("<i>The Leaf</i>", "leaf");
	$leaf_menu->addItem(new GoavesManageNavigationItem("Upload New Issue", "upload"));
	$leaf_menu->addItem(new GoavesManageNavigationItem("Edit Past Issues", "list"));
	$navigation_menu[] = $leaf_menu;
  }
  if (substr($permissions, PERM_SPORTS, 1) > 0)//$valid_permissions[7] === "1")
  {
    $sports_menu = new GoavesManageNavigationGroup("Sports", "sport");
    $sports_menu->addItem(new GoavesManageNavigationItem("Create New Sports Team", "create-team"));
    $sports_menu->addItem(new GoavesManageNavigationItem("Manage Sports Teams", "list"));
    $sports_menu->addItem(new GoavesManageNavigationItem("Provide Team Roster", "upload-roster"));
    $sports_menu->addItem(new GoavesManageNavigationItem("Edit Sports Information Page", "edit-information-page"));
    $sports_menu->addItem(new GoavesManageNavigationItem("View Sports Statistics", "statistics"));
    $navigation_menu[] = $sports_menu;
  }
  if (substr($permissions, PERM_GROUPS, 1) > 0)//$valid_permissions[8] === "1")
  {
    $groups_menu = new GoavesManageNavigationGroup("Groups", "group");
    $groups_menu->addItem(new GoavesManageNavigationItem("Create New Group", "create"));
    $groups_menu->addItem(new GoavesManageNavigationItem("Manage Groups", "list"));
    $groups_menu->addItem(new GoavesManageNavigationItem("View Groups Statistics", "statistics"));
    $navigation_menu[] = $groups_menu;
  }
  if (substr($permissions, PERM_INTERACTIONS, 1) > 0)//$valid_permissions[9] === "1")
  {
    $interactions_menu = new GoavesManageNavigationGroup("Interactions", "interactions");
	$interactions_menu->addItem(new GoavesManageNavigationItem("Create New Poll", "poll-create"));
    $navigation_menu[] = $interactions_menu;
  }
  if (substr($permissions, PERM_SPECIALS, 1) > 0)//$valid_permissions[11] === "1")
  {
    $specials_menu = new GoavesManageNavigationGroup("Specials", "special");
	$specials_menu->addItem(new GoavesManageNavigationItem("Create New Special", "create"));
	$specials_menu->addItem(new GoavesManageNavigationItem("Manage Specials", "list"));
	$specials_menu->addItem(new GoavesManageNavigationItem("View Specials Statistics", "statistics"));
	$navigation_menu[] = $specials_menu;
  }
  if (substr($permissions, PERM_ADMIN, 1) > 0)
  {
    $admin_menu = new GoavesManageNavigationGroup("Admin", "admin");
	$admin_menu->addItem(new GoavesManageNavigationItem("Blog", "blog"));
	$admin_menu->addItem(new GoavesManageNavigationItem("Staff", "staff"));
	$navigation_menu[] = $admin_menu;
  }
  
  $mail_menu = new GoavesManageNavigationGroup("Mail", "mail");
  $mail_menu->addItem(new GoavesManageNavigationItem("Inbox", "inbox"));
  $mail_menu->addItem(new GoavesManageNavigationItem("Send mail", "compose"));
  $navigation_menu[] = $mail_menu;
  return $navigation_menu;
}

class GoavesManageNavigationGroup
{
  private $_name;
  private $_refHandle;
  private $_items = array();
  function __construct($name, $referenceHandle)
  {
    $this->_name = $name;
    $this->_refHandle = $referenceHandle;
  }
  function addItem($item) { $this->_items[] = $item; }
  function getItems() { return $this->_items; }
  function getName() { return $this->_name; }
  function getReferenceHandle() { return $this->_refHandle; }
}
class GoavesManageNavigationItem
{
  private $_name;
  private $_pageHandle;
  function __construct($name, $page_handle)
  {
    $this->_name = $name;
    $this->_pageHandle = $page_handle;
  }
  function getName() { return $this->_name; }
  function getPageHandle() { return $this->_pageHandle; }
}
?>