<?php
class GoavesManageSpecialEditInfo {
	private $_specialIdentity;
	private $_title;
	private $_handle;
	private $_active;
	private $_ended;
	
	private $_categories = array();
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM specials WHERE special_identity='" .
		$database->escapeString($path[2]) . "'") == 0)
	  {
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-special-edit", "LOADED" => false, "IDENTITY" => $path[2]);
		return new GoavesManageSpecialManage();
	  }
	  
	  $info = $database->querySingle("SELECT special_identity, handle, title, active, ended, listing_image, banner_image, " .
		"listing_style FROM specials WHERE special_identity='" . $database->escapeString($path[2]) . "' LIMIT 1", true);
		
	  $this->_specialIdentity = $info["special_identity"];
	  $this->_title = format_content($info["title"]);
	  $this->_handle = $info["handle"];
	  $this->_active = ($info["active"] == "TRUE");
	  $this->_ended = ($this->_active ? null : strtotime($info["ended"]));
	  return true;
	}
	function getRequirePermission() { return PERM_SPECIALS; }
	function getPageHandle() { return "special"; }
	function getPageSubhandle() { return "manage"; }
	function getPageTitle() { return "[Special] Edit '" . $this->_title . "'"; }
	function getBreadTrail() { return array("special" => "Specials Listing", 
		("special/manage/" . $this->_specialIdentity) => "'" . $this->_title . "'", "[this]" => "Edit Information"); }
	function getPageStylesheet() { return "stylesheet-special.css"; }
	function getBodyOnload() { return null; }
	function getPageJavascript()
	{
	  return "var special_updated = false;
	  function toggleActive()
	  {
	    document.getElementById('date-ended-container').style.display = (document.getElementById('status-active').value ? 'none' : 'block');
	  }
	  function checkFormValid()
	  {
	    if (document.getElementById('special-title').className == 'inputText formComponentValid' &&
			(document.getElementById('status-active').value || document.getElementById('date-ended-timestamp').value != null))
		{
		  document.getElementById('special-edit').disabled = false;
		  return;
		}
		document.getElementById('special-edit').disabled = true;
	  }
	  function getWarnPageRefresh()
	  {
	    if (special_updated)
		  return null;
	    if (document.getElementById('special-title').value == '')
		  return null;
		return 'Are you sure you wish to leave this page? (Unsaved content will be lost unless you save it)';
	  }
	  function processSubmitUpdate()
	  {
	    if (document.getElementById('special-edit').disabled)
		  return;
		var editCall = web_path + '/components/update-special-info.php?identity=" . $this->_specialIdentity . "';
		editCall += '&title=' + encodeURIComponent(document.getElementById('special-title').value);
		editCall += '&active=' + (document.getElementById('status-active').value ? 'true' : 'false');
		if (!document.getElementById('status-active').value)
		  editCall += '&ended=' + encodeURIComponent(document.getElementById('date-ended-timestamp').value);
		ManageWindow('process-in-action');
		alert(editCall);
		executeAJAX(editCall, function processCreate(result)
		{
		  if (result == 'success')
		  {
		    special_updated = true;
		    window.location = '" . MANAGE_WEB_PATH . "/special/manage/" . $this->_specialIdentity . "/';
			return;
		  }
		  alert(result);
		  openWindow.closeWindow();
		});
	  }";
	}
	function getPageContents()
	{	  
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <a class=\"returnLink\" href=\"" . MANAGE_WEB_PATH . "/special/manage/" . $this->_specialIdentity . "/\">&laquo; Return</a>\n";
	  echo "  Edit Special '" . $this->_title . "'\n";
	  echo "</div>\n";
			
	  echo "<div class=\"createForm noWindow\">\n";
	  echo "  <div class=\"infoLine\"><b>Title:</b> " . $this->_title . "</div>\n";
	  echo "  <input type=\"text\" class=\"inputText\" name=\"special-title\" id=\"special-title\" autocomplete=\"off\" onKeyUp=\"executeAJAX(web_path + '/components/" .
		"check-title.php?type=beat&minimum=6&title=' + escapeStringURL(this.value) + '&identity=' + " . $this->_specialIdentity . ", function " .
		"processTitle(return_value) {
		  if (return_value.indexOf('[Error]') >= 0)
		  {
		    document.getElementById('special-title-error').innerHTML = return_value.replace('[Error] ', '');
			if (document.getElementById('special-title-error').isFading != true)
			{
		      document.getElementById('special-title-error').style.display = 'block';
			  fadeOutAndHide('special-title-error', 2400);
		    }
			document.getElementById('special-title').className = 'inputText formComponentInvalid';
			document.getElementById('special-edit').disabled = true;
		  } else
		  {
		    document.getElementById('special-title-error').style.display = 'none';
		    document.getElementById('special-title').className = 'inputText formComponentValid';
			checkFormValid();
		  } });\" value=\"" . addslashes($this->_title) . "\" />\n";
	  echo "  <div class=\"contentError\" id=\"special-title-error\" style=\"display:none;\"></div>\n";
		
	  echo "  <div class=\"infoLine separator\"><b>Status:</b> " . ($this->_active ? "Active" : "Ended") . "</div>\n";
	  echo "  <input type=\"checkbox\" " . ($this->_active ? "checked=\"checked\" " : "") . " id=\"status-active\" onChange=\"toggleActive();\"/> " .
		"<label for=\"status-active\">Active?</label><br />\n";
	  echo "  <div id=\"date-ended-container\"" . ($this->_active ? " style=\"display:none;\"" : "") . ">\n";
	  echo "  <div class=\"infoLine separator\"><b>Date Ended:</b></div>\n";
	  require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
	  FormDate("date-ended", ($this->_active ? null : $this->_ended));
	  echo "  </div>\n";
	  echo "  <center><input type=\"button\" class=\"submitButton separator\" id=\"special-edit\" " .
		"value=\"Update\" onClick=\"processSubmitUpdate();\" /></center>\n";
	  echo "</div>\n";
	  echo "<div style=\"clear:both;\"></div>\n";
	}
}
?>