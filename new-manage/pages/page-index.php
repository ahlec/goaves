<?php
class GoAvesManageIndex {
	private $_initialBlogIdentity;
	private $_blogEntries = array();
	private $_staffInformation = array();
	function checkOrRedirect($path, $database)
	{
	  $this->_initialBlogIdentity = $database->querySingle("SELECT blog_identity FROM manage_blog ORDER BY post_timestamp DESC LIMIT 1");
	  
	  /* Staff Information */
	  $staff_identity = $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]);
	  if (substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_BEATS, 1) != "0")
	  {
	    $this->_staffInformation["NUMBER_BEATS"] = $database->querySingle("SELECT count(*) FROM beats WHERE staff_identity='" . $staff_identity . "'");
		$this->_staffInformation["NUMBER_BEATS_PUBLISHED"] = $database->querySingle("SELECT count(*) FROM beats WHERE published='TRUE' AND staff_identity='" .
			$staff_identity . "'");
	  }
	  if (substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GALLERIES, 1) != "0")
	  {
	    $this->_staffInformation["NUMBER_GALLERIES"] = $database->querySingle("SELECT count(*) FROM photo_galleries WHERE staff_identity='" . $staff_identity . "'");
		$this->_staffInformation["NUMBER_GALLERIES_PUBLISHED"] = $database->querySingle("SELECT count(*) FROM photo_galleries WHERE staff_identity='" . $staff_identity .
			"' AND published='TRUE'");
	  }
	  return true;
	}
	function getPageHandle() { return null; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return null; }
	function getBreadTrail() { return array("[this]" => "Index"); }
	function getPageStylesheet() { return "stylesheet-index.css"; }
	function getPageJavascript()
	{
	  return "function loadBlogPost(blog_identity)
	  {
	    executeAJAX(web_path + '/components/load-blog-post.php?post=' + blog_identity, function display(post)
		{
		  document.getElementById('blogContainer').innerHTML = post;
		});
	  }";
	}
	function getPageContents()
	{
	  echo "<div class=\"blogContainer\" id=\"blogContainer\">\n";
	  echo "  <div class=\"header\">Management Blog</div>\n";
	  if ($this->_initialBlogIdentity != null)
	  {
		require_once (MANAGE_DOCUMENT_ROOT . "/components/load-blog-post.php");
		echo createBlogPost($this->_initialBlogIdentity);
	  } else
	    echo "There are no posts in the management blog.\n";
	  echo "</div>\n";
		  
		echo "<div class=\"quickInfoContainer\">\n";
		echo "  <div class=\"header\">You: At a glance</div>\n";
		echo "  <img src=\"" . WEB_PATH . "/images/staff/" . $_SESSION[MANAGE_SESSION]["ICON"] . "\" class=\"icon\" />\n";
		echo "  <div class=\"staffName\">" . $_SESSION[MANAGE_SESSION]["FIRST_NAME"] . " " . $_SESSION[MANAGE_SESSION]["LAST_NAME"] . "</div>\n";
		echo "  <div class=\"staffIdentity\">(Staff identity: " . $_SESSION[MANAGE_SESSION]["IDENTITY"] . ")</div>\n";
		echo "  <div class=\"line\"><span>Beats:</span> " . (substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_BEATS, 1) != "0" ? 
			$this->_staffInformation["NUMBER_BEATS_PUBLISHED"] . " published beat" . ($this->_staffInformation["NUMBER_BEATS_PUBLISHED"] != 1 ? "s" : "") . " (" . 
			$this->_staffInformation["NUMBER_BEATS"] . " total)" : "<sup>N</sup>/<sub>A</sub>") . "</div>\n";
		echo "  <div class=\"line\"><span>Photo Galleries:</span> " . (substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GALLERIES, 1) != "0" ?
			$this->_staffInformation["NUMBER_GALLERIES_PUBLISHED"] . " published galler" . ($this->_staffInformation["NUMBER_GALLERIES_PUBLISHED"] != 1 ? "ies" : "y") .
				" (" . $this->_staffInformation["NUMBER_GALLERIES"] . " total)" : "<sup>N</sup>/<sub>A</sub>") . "</div>\n";
		echo "</div>\n";
		echo "<div style=\"clear:both;\"></div>\n";
	}
}
?>