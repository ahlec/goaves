<?php
class GoavesManagePhotoGalleriesList {
	private $_galleries = array();
	private $_successWithPost = null;
	private $_deletedGallery = null;
	function checkOrRedirect($path, $database)
	{
	  $all_galleries = $database->query("SELECT gallery_identity, title, handle, date_created, date_last_updated, is_complete, published, title, " .
		"gallery_image FROM photo_galleries WHERE staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) .
		"' ORDER BY date_created DESC");
	  while ($gallery = $all_galleries->fetchArray())
	  {
	    $date_created = strtotime($gallery["date_created"]);
		$school_year_start_posted = date("Y", $date_created);
		if (date("m", $date_created) < 6)
		  $school_year_start_posted -= 1;
	    $this->_galleries[] = array("IDENTITY" => $gallery["gallery_identity"], "HANDLE" => $gallery["handle"],
	    	"TITLE" => format_content($gallery["title"]), "DATE_CREATED" => $date_created,
	    	"PUBLISHED" => ($gallery["published"] == "TRUE"), "IS_COMPLETE" => ($gallery["is_complete"] == "TRUE"),
			"SCHOOL_YEAR" => $school_year_start_posted . " - " . ($school_year_start_posted + 1),
			"DATE_LAST_UPDATED" => strtotime($gallery["date_last_updated"]), "ICON_FILE" => $gallery["gallery_image"]);
	  }
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]) && $_SESSION[MANAGE_TRANSFER_DATA]["FROM"] == "page-photo-galleries-create" &&
	  	$_SESSION[MANAGE_TRANSFER_DATA]["SUCCESS"])
	  {
		  $this->_successWithPost = $database->querySingle("SELECT title FROM photo_galleries WHERE gallery_identity='" .
			$database->escapeString($_SESSION[MANAGE_TRANSFER_DATA]["IDENTITY"]) . "' LIMIT 1");
		  unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]) && $_SESSION[MANAGE_TRANSFER_DATA]["FROM"] == "delete-content.php")
	  {
	    $this->_deletedGallery = $_SESSION[MANAGE_TRANSFER_DATA]["TITLE"];
	    unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  
	  return true;
	}
	function getRequirePermission() { return "2"; }
	function getPageHandle() { return "photo-gallery"; }
	function getPageSubhandle() { return "list"; }
	function getPageTitle() { return "My Photo Galleries"; }
	function getBreadTrail() { return array("photo-gallery" => "Photo Galleries", "[this]" => "My Photo Galleries"); }
	function getPageStylesheet() { return "stylesheet-galleries.css"; }
	function getBodyOnload()
	{
	  if ($this->_deletedGallery != null)
	    return "fadeOutAndHide('gallery-deleted-success', 3000);";
	  return null;
	}
	function getPageContents()
	{
	  if ($this->_successWithPost != null)
		  echo "<div class=\"contentSuccess separatorBottom\" id=\"create-success\" onLoad=\"fadeOutAndHide('create-success', 3000);\"><b>Creation Successful.</b>" .
			"The gallery '" . $this->_successWithPost . "' has been created.</div>\n";
	  if ($this->_deletedGallery != null)
	    echo "<div class=\"contentSuccess separatorBottom\" id=\"gallery-deleted-success\"><b>Deletion successful.</b> Gallery '" .
	    	$this->_deletedGallery . "' and all photos within have been deleted.</div>\n";
	  echo "<div class=\"contentHeader\">My Photo Galleries</div>\n";
	  if (sizeof($this->_galleries) == 0)
	  {
	    echo "<div class=\"contentText\">You have no photo galleries</div>\n";
		echo "<div class=\"contentText miniSeparator\"><a href=\"" . MANAGE_WEB_PATH .
			"/photo-gallery/create/\">Create a new photo gallery</a></div>\n";
		return;
	  }
	  echo "<div class=\"contentText\">Select a photo gallery from below.</div>\n";
	  echo "<div class=\"listingContainer\">\n";
	  $last_school_year = null;
	  foreach ($this->_galleries as $gallery)
	  {
	    if ($last_school_year == null || $last_school_year != $gallery["SCHOOL_YEAR"])
		{
		  if ($last_school_year != null)
		    echo "  </div>\n";
		  echo "  <div class=\"yearContainer" . ($last_school_year != null ? " separator" : "") . "\">\n";
		  echo "    <div class=\"yearHeader\">" . $gallery["SCHOOL_YEAR"] . "</div>\n";
		  $last_school_year = $gallery["SCHOOL_YEAR"];
		}
		echo "  <a href=\"" . MANAGE_WEB_PATH . "/photo-gallery/manage/" . $gallery["IDENTITY"] . "/\" class=\"item\" style=\"background-image:url('" .
			WEB_PATH . "/images/photos/" . $gallery["ICON_FILE"] . "');\">\n";
		echo "    <div class=\"overlay\"></div>\n";
		echo "    <div class=\"status " . ($gallery["PUBLISHED"] ? "statusPublished" : "statusUnpublished") . "\"></div>\n";
		echo "    <div class=\"title\">" . $gallery["TITLE"] . "</div>\n";
		echo "  </a>\n";
	  }
	  echo "  </div>\n";
	  echo "</div>\n";
	}
}
?>