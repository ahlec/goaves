<?php
class GoavesManageSpecialCreate {
	private $_specialTitleMinimum = 6;
	function checkOrRedirect($path, $database)
	{
	  if (isset($path[2]) && strpos($path[2], "success-") === 0)
	  {
	    $supposed_success = $database->escapeString(str_replace("success-", "", $path[2]));
		if ($database->querySingle("SELECT count(*) FROM specials WHERE special_identity='" . $supposed_success . "'") > 0)
			{
			  $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-specials-create", "SUCCESS" => true, "IDENTITY" => $supposed_success);
			  header("Location: " . MANAGE_WEB_PATH . "/special/manage/" . $supposed_success . "/");
			  exit();
			}
	  }
	  return true;
	}
	function getRequirePermission() { return PERM_SPECIALS; }
	function getPageHandle() { return "special"; }
	function getPageSubhandle() { return "create"; }
	function getPageTitle() { return "Create New Special"; }
	function getBreadTrail() { return array("special" => "Specials", "[this]" => "Create New Special"); }
	function getPageStylesheet() { return "stylesheet-specials.css"; }
	function getPageOnBeforeUnload() { return "return getWarnPageRefresh();"; }
	function getPageJavascript()
	{
	  return "var special_created = false;
	  function listingStyleChanged()
	  {
		if (getCurrentListingStyle() === false)
		  return;
		var currentListing = getCurrentListingStyle();
		var previousListing = (currentListing == 'horizontal' ? 'vertical' : 'horizontal');
		document.getElementById('listing_image_container_' + currentListing).style.display = 'block';
		document.getElementById('listing_image_container_' + previousListing).style.display = 'none';
	  }
	  function getCurrentListingStyle()
	  {
	    if (!document.getElementById('listing-style-horizontal').checked && !document.getElementById('listing-style-vertical').checked)
		  return false;
		return (document.getElementById('listing-style-horizontal').checked ? 'horizontal' : 'vertical');
	  }
	  function checkFormValid()
	  {
	    var currentListing = getCurrentListingStyle();
	    if (document.getElementById('special-title').className == 'inputText formComponentValid' &&
			document.getElementById('banner-banner').value.length > 0 &&
			currentListing !== false && document.getElementById(currentListing + '-listing-banner').value.length > 0)
		{
		  document.getElementById('special-create').disabled = false;
		  return;
		}
		document.getElementById('special-create').disabled = true;
	  }
	  function getWarnPageRefresh()
	  {
	    if (special_created)
		  return null;
	    if (document.getElementById('special-title').value == '')
		  return null;
		return 'Are you sure you wish to leave this page? (Unsaved content will be lost unless you save it)';
	  }
	  function processSubmitCreate()
	  {
	    if (document.getElementById('special-create').disabled)
		  return;
		var currentListingStyle = getCurrentListingStyle();
		var createCall = web_path + '/components/create-special.php?title=' + encodeURIComponent(document.getElementById('special-title').value) +
		'&page_banner=' + encodeURIComponent(document.getElementById('banner-banner').value) + '&listing_style=' + currentListingStyle + '&listing_image=' +
		encodeURIComponent(document.getElementById(currentListingStyle + '-listing-banner').value);
		executeAJAX(createCall, function processCreate(result)
		{
		  if (result.indexOf('[Error]') < 0)
		  {
		    special_created = true;
		    window.location = '" . MANAGE_WEB_PATH . "/special/create/success-' + result + '/';
			return;
		  }
		  alert(result);
		});
	  }";
	}
	function getPageContents()
	{
	  echo "<div class=\"contentHeader\">Create New Special</div>\n";
	  
	  echo "<div class=\"createForm\">\n";
	  echo "  <div class=\"formLabel\">Title:</div>\n";
	  echo "  <input type=\"text\" class=\"inputText\" name=\"special-title\" id=\"special-title\" autocomplete=\"off\" onKeyUp=\"executeAJAX(web_path + '/components/" .
		"check-title.php?type=special&minimum=" . $this->_specialTitleMinimum . "&title=' + encodeURIComponent(this.value), function " .
		"processTitle(return_value) {
		  if (return_value.indexOf('[Error]') >= 0)
		  {
		    document.getElementById('special-title-error').innerHTML = return_value.replace('[Error] ', '');
			if (document.getElementById('special-title-error').isFading != true)
			{
		      document.getElementById('special-title-error').style.display = 'block';
			  fadeOutAndHide('special-title-error', 2400);
		    }
			document.getElementById('special-title').className = 'inputText formComponentInvalid';
			document.getElementById('special-create').disabled = true;
		  } else
		  {
		    document.getElementById('special-title-error').style.display = 'none';
		    document.getElementById('special-title').className = 'inputText formComponentValid';
			checkFormValid();
		  } });\" />\n";
	  echo "  <div class=\"contentError\" id=\"special-title-error\" style=\"display:none;\"></div>\n";
	  
	  echo "  <div class=\"formLabel separator\">Listing Style:</div>\n";
	  echo "  <input type=\"radio\" name=\"listing_style\" value=\"horizontal\" id=\"listing-style-horizontal\" onChange=\"listingStyleChanged();\" /> " .
		"<label for=\"listing-style-horizontal\">Horizontal</label><br />\n";
	  echo "  <input type=\"radio\" name=\"listing_style\" value=\"vertical\" id=\"listing-style-vertical\" onChange=\"listingStyleChanged();\" /> " .
		"<label for=\"listing-style-vertical\">Vertical</label>\n";
	  
	  require_once(DOCUMENT_ROOT . "/config/functions-upload.inc.php");
	  
	  echo "  <div id=\"listing_image_container_horizontal\" style=\"display:none;\">\n";
	  echo "    <input type=\"hidden\" id=\"listing_image_horizontal_original\" />\n";
	  echo "    <input type=\"hidden\" id=\"horizontal-listing-banner\" />\n";
	  UploadImageInput("horizontal_listing_image", "Listing Image", "horizontal-listing-banner", "listing_image_horizontal_original", 670, 150, "checkFormValid()");
	  echo "  </div>\n";
	  
	  echo "  <div id=\"listing_image_container_vertical\" style=\"display:none;\">\n";
	  echo "    <input type=\"hidden\" id=\"listing_image_vertical_original\" />\n";
	  echo "    <input type=\"hidden\" id=\"vertical-listing-banner\" />\n";
	  UploadImageInput("vertical_listing_image", "Listing Image", "vertical-listing-banner", "listing_image_vertical_original", 325, 150, "checkFormValid()");
	  echo "  </div>\n";
	  
	  echo "  <input type=\"hidden\" id=\"banner_image_original\" />\n";
	  echo "  <input type=\"hidden\" id=\"banner-banner\" />\n";
	  UploadImageInput("banner_image", "Banner Image", "banner-banner", "banner_image_original", 600, 200, "checkFormValid()");
	  
	  echo "  <center><input type=\"button\" class=\"submitButton\" id=\"special-create\" value=\"Create\" disabled=\"disabled\" " .
		"onClick=\"processSubmitCreate();\" /></center>\n";
	  echo "</div>\n";
	  echo "  <div style=\"clear:both;\"></div>\n";
	}
}
?>