<?php
class GoAvesManageAdminStaff {
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]))
		return true;
	  if (is_numeric($path[2]) && $database->querySingle("SELECT count(*) FROM staff WHERE identity='" . $database->escapeString($path[2]) . "'") > 0)
	    return new GoavesManageAdminStaffPage();
	  switch ($path[2])
	  {
	    case "change-permissions": return new GoavesManageStaffChangePermissions();
		case "listing": return new GoavesManageAdminStaffListing();
		case "new": return new GoavesManageAdminStaffNew();
	  }
	  return true;
	}
	function getRequirePermission() { return PERM_ADMIN; }
	function getPageHandle() { return "admin"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "[Admin]"; }
	function getBreadTrail() { return array("admin" => "[Admin Level]", "[this]" => "Staff"); }
	function getPageStylesheet() { return "stylesheet-admin.css"; }
	function getPageContents()
	{
	  echo "<a href=\"" . MANAGE_WEB_PATH . "/admin/staff/listing/\">Staff Listing</a><br />\n";
	  echo "<a href=\"" . MANAGE_WEB_PATH . "/admin/staff/new/\">New Staff member</a>\n";
	}
}
?>