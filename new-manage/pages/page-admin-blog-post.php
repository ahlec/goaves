<?php
class GoavesManageAdminBlogPost
{
	private $_attemptedTitle;
	private $_attemptedBody;
	private $_postFailedReason;
	function checkOrRedirect($path, $database)
	{
	  if (isset($_POST["post-text"]))
	  {
		if ($database->querySingle("SELECT count(*) FROM manage_blog WHERE title LIKE '" . $database->escapeString(prepare_content_for_insert($_POST["post-title"])) .
			"'") == 0)
		{
		  $title = $database->escapeString(prepare_content_for_insert($_POST["post-title"]));
		  $body = $database->escapeString(prepare_content_for_insert($_POST["post-text"]));
		  $post_success = $database->exec("INSERT INTO manage_blog(staff_identity, post_timestamp, title, body) VALUES('" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) .
			"','" . date("Y-m-d H:i:s") . "','" . $title . "','" . $body . "')");
		  if ($post_success)
		  {
		    $blog_identity = $database->querySingle("SELECT blog_identity FROM manage_blog WHERE title='" . $title . "' LIMIT 1");
			$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-admin-blog-post", "SUCCESS" => true, "IDENTITY" => $blog_identity);
			return new GoavesManageAdminBlogList();
		  } else
		    $this->_postFailedReason = "Could not create the blog post within the database.";
		} else
		  $this->_postFailedReason = "A blog entry already exists with the title '" . format_content($_POST["post-title"]) . "'.";
	  }
	  return true;
	}
	function getRequirePermission() { return PERM_ADMIN; }
	function getPageHandle() { return "admin"; }
	function getPageSubhandle() { return "blog"; }
	function getPageTitle() { return "[Admin] New Blog Post"; }
	function getBreadTrail() { return array("admin" => "[Admin Level]", "admin/blog" => "Management Blog", "[this]" => "New Post"); }
	function getPageStylesheet() { return "stylesheet-admin.css"; }
	function getBodyOnload() { return null; }
	function getPageJavascript()
	{
	  return "tinyMCE.init({
            mode : \"textareas\",
            entity_encoding : \"named\",
            theme : \"simple\",
            skin : \"o2k7\",
            remove_trailing_nbsp : true,
            height : '200'});";
	}
	function getPageContents()
	{
	  if ($this->_postFailedReason != null)
	    echo "<div class=\"contentError separatorBottom\"><b>Posting Error.</b> " . $this->_postFailedReason . "</div>\n";
	  echo "<div class=\"contentHeader\">\n";
	  echo "  New Blog Post\n";
	  echo "</div>\n";
	  
	  echo "<div class=\"createForm noWindow separator\">\n";
	  echo "<form method=\"POST\" action=\"" . MANAGE_WEB_PATH . "/admin/blog/post/\">\n";
	  echo "  <div class=\"formLabel\">Title:</div>\n";
	  echo "  <input type=\"text\" class=\"inputText\" name=\"post-title\" />\n";
	  echo "  <div class=\"formLabel separator\">Post:</div>\n";
	  echo "  <textarea style=\"width:100%;\" name=\"post-text\"></textarea><br />\n";
	  echo "  <center>\n";
	  echo "  <input type=\"submit\" value=\"Post\" class=\"inputButton\" />\n";
	  echo "  </center>\n";
	  echo "</form>\n";
	  echo "</div>\n";
	}
}
?>