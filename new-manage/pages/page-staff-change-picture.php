<?php
class GoavesManageStaffChangePicture {
	private $_imageFile;
	private $_hasUploadedImage;
	private $_iconFile;
	private $_hasUploadedIcon;
	function checkOrRedirect($path, $database)
	{
	  $staff_info = $database->querySingle("SELECT image_file, icon_file FROM staff WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) .
		"' LIMIT 1", true);
	  $this->_imageFile = $staff_info["image_file"];
	  $this->_hasUploadedImage = ($staff_info["image_file"] != "no-staff-picture.jpg");
	  $this->_iconFile = $staff_info["icon_file"];
	  $this->_hasUploadedIcon = ($staff_info["icon_file"] != "no-staff-icon.jpg"); 
	  return true;
	}
	function getPageHandle() { return null; }
	function getPageSubhandle() { return "change-picture"; }
	function getPageTitle() { return "Change Staff Picture"; }
	function getBreadTrail() { return array("staff" => "My Account", "[this]" => "Change Staff Picture"); }
	function getPageStylesheet() { return "stylesheet-staff.css"; }
	function getBodyOnload() { return null; }
	function getPageJavascript()
	{
	  return "function updateIconCoords(cropbox)
	  {
		var image_ratio = document.getElementById('staff-image-image-ratio').value;
		document.getElementById('staff-icon-crop-x').value = cropbox.x * image_ratio;
		document.getElementById('staff-icon-crop-y').value = cropbox.y * image_ratio;
		document.getElementById('staff-icon-crop-width').value = cropbox.w * image_ratio;
		document.getElementById('staff-icon-crop-height').value = cropbox.h * image_ratio;
	  }
	  function staffImageUploaded()
	  {
	    document.getElementById('staff-image-image-final').style.display = 'none';
	    document.getElementById('preview-staff-image').src = '" . MANAGE_WEB_PATH . "/limbo/' + document.getElementById('staff-image').value;
	    document.getElementById('staff-icon-crop-img').src = '" . MANAGE_WEB_PATH . "/purgatory/' + document.getElementById('staff-image-crop-image-filepath').value;
	    jQuery(function() {
			jQuery('#staff-icon-crop-img').Jcrop({
			aspectRatio: 1,
			bgOpcity: 0.4,
			onSelect: updateIconCoords,
			onChange: updateIconCoords
			});
		});
		document.getElementById('staff-icon-crop-container').style.display = 'block';
	  }
	  function cropStaffIcon()
	  {
		var crop_x = parent.document.getElementById('staff-icon-crop-x').value;
		var crop_y = parent.document.getElementById('staff-icon-crop-y').value;
		var crop_width = parent.document.getElementById('staff-icon-crop-width').value;
		var crop_height = parent.document.getElementById('staff-icon-crop-height').value;
		if (crop_x == null || crop_y == null || crop_width == null || crop_height == null)
		{
			alert ('Must make a selection on the uploaded image before continuing.');
			return;
		}
		executeAJAX('" . MANAGE_WEB_PATH . "/components/crop-image.php?image=' + document.getElementById('staff-image-original').value + '&x=' + crop_x + '&y=' + crop_y +
			'&width=' + crop_width + '&height=' + crop_height + '&crop_width=60&crop_height=60&prefix=staff-icon-',
			function process(results)
			{
				if (results.indexOf('~') === 0)
				{
					document.getElementById('staff-icon').value = results.substring(1);
					document.getElementById('staff-icon-crop-container').style.display = 'none';
					document.getElementById('preview-staff-icon').src = '" . MANAGE_WEB_PATH . "/limbo/' + document.getElementById('staff-icon').value;
					document.getElementById('confirm-upload').disabled = false;
				} else
					alert(results.replace('[Error] ', ''));
			});
	  }
	  function uploadStaffImages()
	  {
	    if (document.getElementById('confirm-upload').disabled)
		  return;
		document.getElementById('confirm-upload').disabled = true;
		document.getElementById('cancel-upload').disabled = true;
		var ajaxCall = web_path + '/components/upload-staff-images.php?original=' + document.getElementById('staff-image-original').value +
			'&staff_image=' + document.getElementById('staff-image').value + '&staff_icon=' + document.getElementById('staff-icon').value +
			'&icon_x=' + document.getElementById('staff-icon-crop-x').value + '&icon_y=' + document.getElementById('staff-icon-crop-y').value +
			'&icon_width=' + document.getElementById('staff-icon-crop-width').value + '&icon_height=' + document.getElementById('staff-icon-crop-height').value;
		executeAJAX(ajaxCall, function finish(results)
		{
		  if (results === 'success')
		  {
		    window.location = '" . MANAGE_WEB_PATH . "/staff/change-picture/';
		    return;
		  }
		  alert(results.replace('[Error] ', ''));
		  document.getElementById('confirm-upload').disabled = false;
		  document.getElementById('cancel-upload').disabled = false;
		});
	  }";
	}
	function getPageContents()
	{	    	
	  echo "<div class=\"contentHeader\">\n";
	  echo "  <div class=\"explainButton\" onClick=\"ManageWindow('explain','change-picture');\"></div>\n";
	  echo "  Change Staff Picture\n";
	  echo "</div>\n";
	  
	  echo "<div class=\"staffPictureContainer\">\n";
	  
	  echo "  <div class=\"contentSubheader separator\">Staff Picture</div>\n";
	  echo "  <div class=\"pictureActions\">\n";
	  echo "    <span class=\"fauxLink\" onClick=\"ManageWindow('upload-staff-picture');\">" . ($this->_hasUploadedImage ? "Upload new picture" : "Upload a picture") . "</span><br />\n";
	  if ($this->_hasUploadedImage)
	    echo "    <span class=\"fauxLink\" onClick=\"ManageWindow('delete-staff-picture');\">Delete picture</span><br />\n";
	  echo "  </div>\n";
	  echo "  <img src=\"" . WEB_PATH . "/images/staff/" . $this->_imageFile . "\" class=\"staffPicture\" />\n";
	  if (!$this->_hasUploadedImage)
	    echo "  <div class=\"noUploadedPictureNotice\">No Picture Set</div>\n";
	  echo "  <div style=\"clear:both;\"></div>\n";
	  
	  echo "  <div class=\"contentSubheader separator\">Staff Icon</div>\n";
	  echo "  <div class=\"pictureActions\">\n";
	  if ($this->_hasUploadedImage)
	    echo "    <span class=\"fauxLink\">" . ($this->_hasUploadedIcon ? "Recrop" : "Crop") . " icon</span>\n";
	  else
	    echo "    (Icon may not be changed while you do not have a staff picture)";
	  echo "  </div>\n";
	  echo "  <img src=\"" . WEB_PATH . "/images/staff/" . $this->_iconFile . "\" class=\"staffIcon\" />\n";
	  echo "  <div style=\"clear:both;\"></div>\n";
	  
	  echo "</div>\n";
	}
}
?>