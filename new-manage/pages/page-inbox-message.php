<?php
class GoavesManageInboxMessage {
	private $_messageIdentity;
	private $_sender;
	private $_recipient;
	private $_timeSent;
	private $_message;
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM staff_messages WHERE message_identity='" . $database->escapeString($path[2]) .
		"' AND (recipient_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' OR sender_identity='" .
		$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "')") == 0)
	  {
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-inbox-message", "LOADED" => false, "IDENTITY" => $path[2]);
		return new GoavesManageInbox();
	  }
	  
	  $info = $database->querySingle("SELECT message_identity, sender_identity, recipient_identity, staff.first_name as \"sender_first_name\", staff.last_name AS \"sender_last_name\", " .
		"time_sent, message, read_yet FROM staff_messages LEFT JOIN staff ON staff.identity = staff_messages.sender_identity WHERE message_identity='" .
		$database->escapeString($path[2]) . "' LIMIT 1", true);
	  $this->_messageIdentity = $info["message_identity"];
	  if (is_numeric($info["sender_identity"]))
	    $this->_sender = array("IDENTITY" => $info["sender_identity"], "FIRST_NAME" => format_content($info["sender_first_name"]),
			"LAST_NAME" => format_content($info["sender_last_name"]));
	  else
	    $this->_sender = format_content($info["sender_identity"]);
	  if (is_numeric($info["recipient_identity"]))
	  {
	    $recipient = $database->querySingle("SELECT first_name, last_name FROM staff WHERE identity='" . $info["recipient_identity"] . "' LIMIT 1", true);
	    $this->_recipient = array("IDENTITY" => $info["recipient_identity"], "FIRST_NAME" => format_content($recipient["first_name"]),
			"LAST_NAME" => format_content($recipient["last_name"]));
	  } else
	    $this->_recipient = format_content($info["recipient_identity"]);
	  $this->_timeSent = strtotime($info["time_sent"]);
	  $this->_message = decodeMailMessage($info["message"]);
	  if ($info["read_yet"] != "TRUE")
	    $database->exec("UPDATE staff_messages SET read_yet='TRUE' WHERE message_identity='" . $this->_messageIdentity . "'");
	  return true;
	}
	function getPageHandle() { return "mail"; }
	function getPageSubhandle() { return "message"; }
	function getPageTitle() { return "Read Message"; }
	function getBreadTrail() { return array("mail" => "Mail", "mail/inbox" => "Inbox", "[this]" => "Message"); }
	function getPageStylesheet() { return "stylesheet-inbox.css"; }
	function getPageContents()
	{
	  echo "<div class=\"messageContainer\">\n";
	  echo "  <" . (is_array($this->_sender) ? "a href=\"" . MANAGE_WEB_PATH . "/staff/" . $this->_sender["IDENTITY"] . "/\" target=\"_blank\"" :
		"div") . " class=\"participant\"><b>Sender:</b> " . (is_array($this->_sender) ?
		$this->_sender["FIRST_NAME"] . " " . $this->_sender["LAST_NAME"] : $this->_sender) . "</" . (is_array($this->_sender) ? "a" : "div") . ">\n";
	  echo "  <" . (is_array($this->_recipient) ? "a href=\"" . MANAGE_WEB_PATH . "/staff/" . $this->_recipient["IDENTITY"] . "/\" target=\"_blank\"" :
		"div") . " class=\"participant\"><b>Recipient:</b> " . (is_array($this->_recipient) ?
		$this->_recipient["FIRST_NAME"] . " " . $this->_recipient["LAST_NAME"] : $this->_recipient) . "</" . (is_array($this->_recipient) ? "a" :"div") . ">\n";
	  echo "  <div class=\"message\">" . $this->_message . "</div>\n";
	  echo "</div>\n";
	  echo "<pre>\n";
	  print_r($this);
	  echo "</pre>\n";
	}
}
?>