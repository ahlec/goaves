<?php
class GoavesManagePhotoGalleriesManagePictures {
	private $_galleryIdentity;
	private $_title;
	private $_pictures = array();
	
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM photo_galleries WHERE gallery_identity='" .
	  	$database->escapeString($path[2]) . "' AND staff_identity='" .
	  	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
	  	{
	  	  $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-photo-galleries-edit-information", "LOADED" => false,
	  	  	"IDENTITY" => $path[2]);
	  	}
	  $this->_galleryIdentity = $database->escapeString($path[2]);
	  if (isset($_POST["submit-picture-change"]))
	  {
	    $number_photos_successfully_edited = 0;
	    foreach ($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$this->_galleryIdentity] as $picture_identity)
		{
		  $picture_title = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_POST["picture-title-" . $picture_identity]))));
		  $picture_credit = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_POST["picture-credit-" . $picture_identity]))));
		  $picture_caption = $database->escapeString(prepare_content_for_insert(unicode_decode(rawurldecode($_POST["picture-caption-" . $picture_identity]))));
		  $update_success = $database->exec("UPDATE photos SET title='" . $picture_title . "', photo_credit='" . $picture_credit . "', caption_text='" .
			$picture_caption . "' WHERE picture_identity='" . $database->escapeString($picture_identity) . "'");
		  if (!$update_success)
		  {
		    exit ("Could not update photo " . $picture_identity);
		  }
		  $number_photos_successfully_edited++;
		}
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-photo-galleries-manage-pictures", "IDENTITY" => $this->_galleryIdentity,
			"SUCCESS" => true, "NUMBER_EDITED" => $number_photos_successfully_edited);
	  }
	  if (!isset($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$this->_galleryIdentity]))
	  {
	    $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-photo-galleries-manage-pictures", "LOADED" => false,
			"REASON" => "No selected pictures to manage.");
	    return new GoavesManagePhotoGalleriesManage();
	  }
	  $info = $database->querySingle("SELECT gallery_identity, photo_galleries.handle, photo_galleries.title, " .
	  	"photo_galleries.date_last_updated, photo_galleries.published, photo_galleries.related_group, gallery_image, " .
	  	"groups.handle AS \"related_group_handle\", groups.title AS \"related_group_title\", photo_galleries.date_created, " .
	  	"photo_galleries.is_complete, photo_galleries.topic, topics.handle AS \"topic_handle\", topics.title AS " .
	  	"\"topic_title\" FROM photo_galleries LEFT JOIN groups ON photo_galleries.related_group = groups.group_identity " .
	  	"LEFT JOIN topics ON photo_galleries.topic = topics.topic_identity WHERE gallery_identity='" .
	  	$database->escapeString($path[2]) . "' LIMIT 1", true);
	  $this->_galleryIdentity = $info["gallery_identity"];
	  $this->_title = format_content($info["title"]);
	  foreach ($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$this->_galleryIdentity] as $picture_identity)
	  {
	    $info = $database->querySingle("SELECT picture_identity, photos.handle, photos.title, photos.picture_taken_date, photos.caption_text, photos.image_file,
			photos.icon_file, photos.sort_order, photos.photo_credit, photos.published FROM photos WHERE picture_identity='" .
			$database->escapeString($picture_identity) . "' LIMIT 1", true);
		if ($info === false)
		{
		}
		$this->_pictures[] = array("IDENTITY" => $info["picture_identity"], "HANDLE" => $info["handle"], "TITLE" => $info["title"],
			"PICTURE_TAKEN" => strtotime($info["picture_taken_date"]), "CAPTION" => $info["caption_text"], "IMAGE_FILE" => $info["image_file"],
			"ICON_FILE" => $info["icon_file"], "ORDER" => $info["sort_order"], "CREDIT" => $info["photo_credit"], "PUBLISHED" => ($info["published"] == "TRUE"));
	  }
	  return true;
	}
	function getRequirePermission() { return "2"; }
	function getPageHandle() { return "photo-gallery"; }
	function getPageSubhandle() { return "manage-pictures"; }
	function getPageTitle() { return "Upload pictures to '" . $this->_title . "'"; }
	function getBreadTrail() { return array("photo-gallery" => "Photo Galleries", "photo-gallery/list" => "My Photo Galleries",
		("photo-gallery/manage/" . $this->_galleryIdentity) => "'" . $this->_title . "'", 
		"[this]" => "Manage Pictures"); }
	function getPageStylesheet() { return "stylesheet-galleries.css"; }
	function getPageOnBeforeUnload() { return "return getWarnPageRefresh();"; }
	function getPageJavascript()
	{
	  return "var photos_have_been_uploaded = false;
	  var has_clicked_transfer_button = false;
	  tinyMCE.init({
		mode: \"textareas\",
		theme : \"simple\",
		skin : \"o2k7\"
          });
	  function getWarnPageRefresh()
	  {
	    if (photos_have_been_uploaded == false)
		  return;
		if (has_clicked_transfer_button)
		  return;
		return 'You have uploaded new pictures to the gallery. However, you need to edit the pictures after they have been uploaded. Click the \'Continue\' button below.';
	  }";
	}
	function getPageContents()
	{
	  echo "<div class=\"contentHeader\">Manage Uploaded Pictures in '" . $this->_title . "'</div>\n";
	  
	  echo "<form method=\"POST\" action=\"" . MANAGE_WEB_PATH . "/photo-gallery/manage-pictures/" .
	  	$this->_galleryIdentity . "/\">\n";
	  echo "<div class=\"createForm noWindow separator\">\n";
	  
	  foreach ($this->_pictures as $picture)
	  {
	    echo "  <div class=\"pictureContainer\">\n";
	    echo "    <img src=\"" . WEB_PATH . "/images/photos/" . $picture["IMAGE_FILE"] . "\" class=\"picture\" />\n";
	    echo "    <div class=\"pictureTitle\">" . ($picture["TITLE"] != "" ? $picture["TITLE"] : "<span class=\"noTitle\">" .
	    	"(No title)</span>") . "</div>\n";
	    echo "    <div class=\"managePictureContainer\">\n";
	    echo "      <div class=\"formLabel miniSeparator\">Title:</div>\n";
	    echo "      <input type=\"text\" id=\"picture-title-" . $picture["IDENTITY"] . "\" class=\"inputText\"" .
	    	($picture["TITLE"] != "" ? " value=\"" . $picture["TITLE"] . "\"" : "") . " name=\"picture-title-" .
	    	$picture["IDENTITY"] . "\" />\n";
	    echo "      <div class=\"formLabel separator\">Photo Credit:</div>\n";
	    echo "      <input type=\"text\" id=\"picture-credit-" . $picture["IDENTITY"] . "\" class=\"inputText\"" .
	    	($picture["CREDIT"] != "" ? " value=\"" . $picture["CREDIT"] . "\"" : "") . " name=\"picture-credit-" .
	    	$picture["IDENTITY"] . "\" />\n";
	    echo "      <div class=\"formLabel separator\">Caption (if any):</div>\n";
	    echo "      <textarea id=\"picture-caption-" . $picture["IDENTITY"] . "\" name=\"picture-caption-" .
	    	$picture["IDENTITY"] . "\">" . $picture["CAPTION"] . "</textarea>\n";
	    echo "    </div>\n";
	    echo "    <div style=\"margin-bottom:20px;clear:both;\"></div>\n";
	    echo "  </div>\n";
	  }
	  
	  echo "  <center><input type=\"submit\" class=\"submitButton separator\" value=\"Continue &raquo;\" " .
	  	"name=\"submit-picture-change\" /></center>\n";
	  echo "</div>\n";
	  echo "</form>\n";
	  echo "  <div style=\"clear:both;\"></div>\n";
	}
}
?>