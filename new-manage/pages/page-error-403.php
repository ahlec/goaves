<?php
class GoavesManageForbidden {
	private $_forbiddenPage;
	function checkOrRedirect($path, $database) { return true; }
	function getPageHandle() { return "forbidden"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Forbidden"; }
	function getBreadTrail()
	{
	  $bread_trail = $this->_forbiddenPage->getBreadTrail();
	  $bread_trail["[this]"] = "Forbidden";
	  return $bread_trail;
	}
	function getPageStylesheet() { return null; }
	function setForbiddenPage($forbidden_page) { $this->_forbiddenPage = $forbidden_page; }
	function getPageContents()
	{
	  $permission_required = null;
	  switch ($this->_forbiddenPage->getRequirePermission())
	  {
	    case "1": $permission_required = "Beat"; break;
		case "2": $permission_required = "Photo Galleries"; break;
		case "3": $permission_required = "Podcasts"; break;
		case "4": $permission_required = "Videos"; break;
		case "5": $permission_required = "Comics"; break;
		case "6": $permission_required = "Soundslides"; break;
		case "7": $permission_required = "Sports"; break;
		case "8": $permission_required = "Groups"; break;
		case "9": $permission_required = "Interactions"; break;
		default: $permission_required = "Unspecified"; break;
	  }
	  echo "<b>Denied:</b> " . $permission_required . " permissions required.";
	}
}
?>