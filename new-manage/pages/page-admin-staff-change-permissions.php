<?php
class GoavesManageAdminStaffChangePermissions {
	private $_staffIdentity;
	private $_firstName;
	private $_lastName;
	private $_permissions;
	private $_aspects = array(PERM_BEATS => "Beats", PERM_GALLERIES => "Photo Galleries", PERM_PODCASTS => "Podcasts", PERM_VIDEOS => "Videos", PERM_COMICS => "Comics",
		PERM_SOUNDSLIDES => "Soundslides", PERM_SPORTS => "Sports", PERM_GROUPS => "Groups", PERM_INTERACTIONS => "Interactions", PERM_SPECIALS => "Specials",
		PERM_ADMIN => "Admin", PERM_INFOGRAPHICS => "Infographics", PERM_LEAF => "<i>The Leaf</i>", PERM_YEARBOOK => "<i>The Log</i>");
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]))
	    exit("Need a staff identity");
	  $info = $database->querySingle("SELECT identity, first_name, last_name, permissions FROM staff WHERE identity='" . $database->escapeString($path[2]) . "' LIMIT 1", true);
	  if ($info === false)
	    exit ("Invalid staff identity.");
	  $this->_staffIdentity = $info["identity"];
	  $this->_firstName = format_content($info["first_name"]);
	  $this->_lastName = format_content($info["last_name"]);
	  $this->_permissions = $info["permissions"];
	  return true;
	}
	function getRequirePermission() { return PERM_ADMIN; }
	function getPageHandle() { return "admin"; }
	function getPageSubhandle() { return "staff"; }
	function getPageTitle() { return "[Admin] Change Permissions"; }
	function getBreadTrail() { return array("admin" => "[Admin Level]", "admin/staff" => "Staff",
		("admin/staff/" . $this->_staffIdentity) => $this->_lastName . ", " . $this->_firstName, "[this]" => "Change Permissions"); }
	function getPageStylesheet() { return "stylesheet-admin.css"; }
	function getBodyOnload() { return null; }
	function getPageJavascript()
	{
	  return "function setPerm(perm_index, value)
	  {
	    document.getElementById('perm-' + perm_index).value = value;
		var permLevelName = 'None';
		if (value == '1')
		{
		  document.getElementById('perm-' + perm_index + '-none').className = 'level none authContributor';
		  document.getElementById('perm-' + perm_index + '-contributor').className = 'level contributor authContributor';
		  document.getElementById('perm-' + perm_index + '-moderator').className = 'level moderator authContributor';
		  document.getElementById('perm-' + perm_index + '-authorization').innerHTML = 'Contributor';
		}
		else if (value == '2')
		{
		  document.getElementById('perm-' + perm_index + '-none').className = 'level none authModerator';
		  document.getElementById('perm-' + perm_index + '-contributor').className = 'level contributor authModerator';
		  document.getElementById('perm-' + perm_index + '-moderator').className = 'level moderator authModerator';
		  document.getElementById('perm-' + perm_index + '-authorization').innerHTML = 'Moderator';
		} else if (value == '0')
		{
		  document.getElementById('perm-' + perm_index + '-none').className = 'level none authNone';
		  document.getElementById('perm-' + perm_index + '-contributor').className = 'level contributor authNone';
		  document.getElementById('perm-' + perm_index + '-moderator').className = 'level moderator authNone';
		  document.getElementById('perm-' + perm_index + '-authorization').innerHTML = 'None';
		}
	  }
	  function changePermissions()
	  {
	    document.getElementById('change-submit').disabled = true;
	    var manageCall = web_path + '/components/change-staff-permissions.php?identity=" . $this->_staffIdentity . "&permissions=';
		for (var index = 0; index < " . sizeof($this->_aspects) . "; index++)
		  manageCall += document.getElementById('perm-' + index).value;
		executeAJAX(manageCall, function process(result)
		{
		  if (result == 'success')
		  {
		    window.location = '" . MANAGE_WEB_PATH . "/admin/staff/" . $this->_staffIdentity . "/';
		    return;
		  } 
		  alert(result);
		  document.getElementById('change-submit').disabled = false;
		});
	  }";
	}
	function getPageContents()
	{
	  echo "<div class=\"contentHeader\">\n";
	  echo "  Change Permissions\n";
	  echo "</div>\n";
	  echo "  <div class=\"indentedContent\">\n";
	  echo "    <div class=\"contentSubheader separator\">Staff Identity:</div>\n";
	  echo "    " . $this->_staffIdentity . "\n";
	  echo "    <div class=\"contentSubheader separator\">Name:</div>\n";
	  echo "    " . $this->_firstName . " " . $this->_lastName . "\n";
	  echo "    <div class=\"contentSubheader separator\">Permissions:</div>\n";
	  echo "    <div class=\"permissionsManageContainer\">\n";
	  for ($index = 0; $index < sizeof($this->_aspects); $index++)
	  {
	    $current_level = substr($this->_permissions, $index, 1);
		echo "    <input type=\"hidden\" id=\"perm-" . $index . "\" value=\"" . $current_level . "\" />\n";
		echo "    <div class=\"permissionManageRow" . ($index == sizeof($this->_aspects) - 1 ? " last" : "") . "\" id=\"perm-row-" . $index . "\">\n";
		echo "      <div class=\"name\">" . $this->_aspects[$index] . "</div>\n";
		$current_level_class_name = "none";
		switch ($current_level)
		{
		  case "1": $current_level_class_name = "Contributor"; break;
		  case "2": $current_level_class_name = "Moderator"; break;
		  default: $current_level_class_name = "None"; break;
		}
		echo "      <div class=\"level none auth" . $current_level_class_name . "\" id=\"perm-" . $index . "-none\" onclick=\"setPerm('" . $index . "','0');\"></div>\n";
		echo "      <div class=\"level contributor auth" . $current_level_class_name . "\" id=\"perm-" . $index . "-contributor\" onclick=\"setPerm('" . $index . "','1');\"></div>\n";
		echo "      <div class=\"level moderator auth" . $current_level_class_name . "\" id=\"perm-" . $index . "-moderator\" onclick=\"setPerm('" . $index . "','2');\"></div>\n";
		echo "      <div class=\"authorization\" id=\"perm-" . $index . "-authorization\">";
		switch ($current_level)
		{
		  case "1": echo "Contributor"; break;
		  case "2": echo "Moderator"; break;
		  default: echo "None";
		}
		echo "</div>\n";
		echo "      <div style=\"clear:both;\"></div>\n";
		echo "    </div>\n";
	    /*echo "    <input type=\"checkbox\"" . ($has_permission ? " checked=\"checked\"" : "") . " id=\"perm-" . $index . "\" /> <label for=\"perm-" . $index .
			"\">" . $this->_aspects[$index] . "</label><br />\n";*/
	  }
	  echo "    </div>\n";
	  echo "    <center><input type=\"submit\" id=\"change-submit\" class=\"submitButton separator\" value=\"Change Permissions\" onclick=\"changePermissions();\" /></center>\n";
	  echo "  </div>\n";
	}
}
?>