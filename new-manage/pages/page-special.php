<?php
class GoAvesManageSpecial {
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[1]))
	    $path[1] = "landing";
	  switch ($path[1])
	  {
	    case "create": return new GoavesManageSpecialCreate();
		case "manage": return new GoavesManageSpecialManage();
	    default: return new GoavesManageSpecialList();
	  }
	  return true;
	}
	function getPageHandle() { return "special"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return ""; }
	function getBreadTrail() { return array("[this]" => "Specials"); }
	function getPageStylesheet() { return "stylesheet-special.css"; }
	function getPageContents()
	{
	}
}
?>