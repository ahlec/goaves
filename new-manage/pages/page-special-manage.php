<?php
class GoavesManageSpecialManage {
	private $_specialIdentity;
	private $_title;
	private $_handle;
	private $_description;
	private $_shortDescription;
	private $_active;
	private $_ended;
	private $_listingImage;
	private $_bannerImage;
	private $_listingStyle;
	private $_isFeature;
	private $_featureDate;
	
	private $_createSuccess;
	private $_editSuccess;
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM specials WHERE special_identity='" .
		$database->escapeString($path[2]) . "'") == 0)
	  {
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-special-manage", "LOADED" => false, "IDENTITY" => $path[2]);
		return new GoavesManageSpecialList();
	  }
	  if (isset($path[3]) && strtolower($path[3]) == "edit-information")
	    return new GoavesManageSpecialEditInfo();
	  if (isset($path[3]) && strtolower($path[3]) == "edit-images")
	    return new GoavesManageSpecialEditImages();
	  if (isset($path[3]) && strtolower($path[3]) == "edit-descriptions")
	    return new GoavesManageSpecialEditDescriptions();
	  
	  $info = $database->querySingle("SELECT special_identity, handle, title, description, short_description, active, ended, listing_image, banner_image, " .
		"listing_style FROM specials WHERE special_identity='" . $database->escapeString($path[2]) . "' LIMIT 1", true);
	  $this->_specialIdentity = $info["special_identity"];
	  $this->_title = format_content($info["title"]);
	  $this->_handle = $info["handle"];
	  $this->_description = ($info["description"] != "" ? format_content($info["description"]) : null);
	  $this->_shortDescription = ($info["short_description"] != "" ? format_content($info["short_description"]) : null);
	  $this->_active = ($info["active"] == "TRUE");
	  $this->_ended = ($this->_active ? null : strtotime($info["ended"]));
	  $this->_listingImage = $info["listing_image"];
	  $this->_bannerImage = $info["banner_image"];
	  $this->_listingStyle = $info["listing_style"];
	  
	  $feature_info = $database->querySingle("SELECT date_featured FROM features WHERE feature_type='special' " .
		"AND item_identity='" . $this->_specialIdentity . "' LIMIT 1");
	  $this->_isFeature = ($feature_info != null && $feature_info != false);
	  $this->_featureDate = strtotime($feature_info);
	  
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]))
	  {
	    switch ($_SESSION[MANAGE_TRANSFER_DATA]["FROM"])
		{
		  case "page-specials-create": $this->_createSuccess = true; break;
		  case "update-special-info": $this->_editSuccess = "info"; break;
		  case "update-special-images": $this->_editSuccess = "images"; break;
		  case "update-special-description": $this->_editSuccess = "description"; break;
		  case "set-feature-component.php":
		  {
		    $this->_featuringSuccess = true;
			break;
		  }
		  case "remove-feature.php": $this->_unfeaturingSuccess = true; break;
		}
		unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  return true;
	}
	function getRequirePermission() { return PERM_SPECIALS; }
	function getPageHandle() { return "special"; }
	function getPageSubhandle() { return "manage"; }
	function getPageTitle() { return "[Special] '" . $this->_title . "'"; }
	function getBreadTrail() { return array("special" => "Specials Listing", "[this]" => "'" . $this->_title . "'"); }
	function getPageStylesheet() { return "stylesheet-specials.css"; }
	function getBodyOnload() { return ($this->_createSuccess != null ? "fadeOutAndHide('create-success', 3000);" : ""); }
	function getPageJavascript()
	{
	  return null;
	}
	function getPageContents()
	{	  
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <a class=\"returnLink\" href=\"" . MANAGE_WEB_PATH . "/special/list/\">&laquo; Return</a>\n";
	  echo "  [Special] '" . $this->_title . "'\n";
	  echo "</div>\n";
	  
	  if ($this->_createSuccess != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"create-success\"><b>Special created.</b> The special has been successfully created.</div>\n";
		
	  if ($this->_editSuccess != null)
	  {
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"edit-success\"><b>Edit success.</b> The ";
		switch ($this->_editSuccess)
		{
		  case "info": echo "general information"; break;
		  case "images": echo "images"; break;
		  case "description": echo "descriptions"; break;
		}
		echo " for this special has been changed successfully.</div>\n";
	  }
	  
	  echo "<div class=\"indentedContent\">\n";
	  echo "  <div class=\"infoLine\"><b>Title:</b> " . $this->_title . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Identity:</b> " . $this->_specialIdentity . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Link:</b> <a href=\"" . WEB_PATH . "/special/" .
	  	$this->_handle . "/\" target=\"_blank\">" . WEB_PATH . "/special/" . $this->_handle .
	  	"/</a></div>\n";
	  echo "  <div class=\"infoLine separator\"><b>Status:</b> " . ($this->_active ? "Active" : "Ended (Ended on: " . date(LONG_DATETIME_FORMAT, $this->_ended) . ")") .
		"</div>\n";
	  echo "<div style=\"clear:both;\"></div>\n";
	  echo "<div class=\"editLink separator\"><a href=\"" . MANAGE_WEB_PATH .
	  	"/special/manage/" . $this->_specialIdentity . "/edit-information/\" class=\"firstAction\"><img src=\"" . MANAGE_WEB_PATH .
	  	"/interface/images/action_go.gif\" class=\"actionIcon\" border=\"0\" /> Edit Information</a> \n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('feature-content', 'special-" . $this->_specialIdentity . "');\"><img src=\"" . MANAGE_WEB_PATH .
		"/interface/images/" . ($this->_isFeature ? "feature_remove.png" : "feature_add.png") . "\" class=\"" .
		"actionIcon\" border=\"0\" /> " . ($this->_isFeature ? "Remove " : "") . "Feature</span>\n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('delete-content', 'special-" . $this->_specialIdentity .
	  	"');\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/folder_delete.gif\" class=\"actionIcon\" border=\"0\" /> Delete</span>\n";
	  echo "</div>\n";
	  echo "<div class=\"dividingBar miniSeparator\"></div>\n";
	  echo "  <div class=\"infoLine separator\"><b>Listing Style:</b> " . ($this->_listingStyle == "HORIZONTAL" ? "Horizontal" : "Vertical") . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Listing Image:</b><br />\n";
	  echo "  <a href=\"" . WEB_PATH . "/images/specials/" . $this->_listingImage . "\" target=\"_blank\"><img src=\"" . WEB_PATH . "/images/specials/" .
		$this->_listingImage . "\" class=\"infoIcon\" border=\"0\" /><br />" . WEB_PATH . "/images/specials/" . $this->_listingImage . "</a></div>\n";
	  echo "  <div class=\"infoLine separator\"><b>Banner Image:</b><br />\n";
	  echo "  <a href=\"" . WEB_PATH . "/images/specials/" . $this->_bannerImage . "\" target=\"_blank\"><img src=\"" . WEB_PATH . "/images/specials/" .
		$this->_bannerImage . "\" class=\"infoIcon\" border=\"0\" /><br />" . WEB_PATH . "/images/specials/" . $this->_bannerImage . "</a></div>\n";
		echo "<div class=\"editLink separator\"><a href=\"" . MANAGE_WEB_PATH .
	  	"/special/manage/" . $this->_specialIdentity . "/edit-images/\" class=\"firstAction\"><img src=\"" . MANAGE_WEB_PATH .
	  	"/interface/images/action_go.gif\" class=\"actionIcon\" border=\"0\" /> Edit Images</a> \n";
	  echo "</div>\n";
	  echo "<div class=\"dividingBar miniSeparator\"></div>\n";
	  echo "  <div class=\"infoLine separator\"><b>Short Description:</b></div>\n";
	  echo "    " . ($this->_shortDescription != null ? $this->_shortDescription : "(None)") . "\n";
	  echo "  <div class=\"infoLine separator\"><b>Full Description:</b></div>\n";
	  echo "    " . ($this->_description != null ? $this->_description : "(None)") . "\n";
	  echo "<div class=\"editLink separator\"><a href=\"" . MANAGE_WEB_PATH .
	  	"/special/manage/" . $this->_specialIdentity . "/edit-descriptions/\" class=\"firstAction\"><img src=\"" . MANAGE_WEB_PATH .
	  	"/interface/images/action_go.gif\" class=\"actionIcon\" border=\"0\" /> Edit Descriptions</a> \n";
	  echo "</div>\n";
	  echo "</div>\n";
	}
}
?>