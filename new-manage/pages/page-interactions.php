<?php
class GoavesManageInteractions {
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[1]))
	    $path[1] = "landing";
	  switch ($path[1])
	  {
	    case "poll-create": return new GoavesManageInteractionsPollCreate();
		case "poll-manage": return new GoavesManageInteractionsPollManage();
		default: return new GoavesManageInteractionsList();
	  }
	  return true;
	}
	function getRequirePermission() { return PERM_INTERACTIONS; }
	function getPageHandle() { return "interactions"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Interactions"; }
	function getBreadTrail() { return array("[this]" => "Interactions"); }
	function getPageStylesheet() { return "stylesheet-comics.css"; }
	function getPageContents()
	{
	  echo "interactions";
	}
}
?>