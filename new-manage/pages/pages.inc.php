<?php
require_once (DOCUMENT_ROOT . "/config/related-group.inc.php");
/* **************************************
 *  Valid Pages Include                 *
 * **************************************/
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-index.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-login.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-logout.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-error-404.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-beat.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-beat-compose.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-beat-list.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-sandbox.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-comics.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-comics-list.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-error-403.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-comics-upload.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-photo-galleries.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-photo-galleries-create.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-photo-galleries-list.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-photo-galleries-manage.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-photo-galleries-edit.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-photo-galleries-upload-pictures.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-photo-galleries-manage-pictures.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-staff.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-staff-change-password.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-staff-change-email.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-staff-statistics.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-podcast.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-podcast-list.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-podcast-upload.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-soundslide.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-soundslide-list.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-beats-manage.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-staff-change-profile.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-staff-change-picture.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-soundslides-create.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-soundslides-manage.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-beat-edit-info.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-beat-edit-text.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-requests.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-soundslide-manage-slides.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-inbox.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-inbox-message.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-lookup.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-podcast-manage.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-comic-manage.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-groups.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-groups-list.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-special.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-specials-list.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-special-manage.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-specials-create.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-special-edit.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-special-edit-images.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-special-edit-descriptions.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-admin.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-admin-staff-change-permissions.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-admin-staff.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-admin-staff-listing.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-admin-staff-new.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-admin-staff-page.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-admin-blog.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-admin-blog-post.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-admin-blog-list.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-groups-manage.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-staff-change-social-media.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-videos.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-videos-list.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-interactions.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-interactions-list.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-poll-create.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-poll-manage.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-leaf.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-leaf-list.php");
require_once (MANAGE_DOCUMENT_ROOT . "/pages/page-leaf-manage.php");

$_PAGES = array("index" => "GoAvesManageIndex", "home" => "GoAvesManageIndex", "login" => "GoAvesManageLogin",
	"page-not-found" => "GoAvesManage404", "logout" => "GoAvesManageLogout", "beat" => "GoAvesManageBeat",
	"sandbox" => "GoavesManageSandbox", "comic" => "GoavesManageComics", "forbidden" => "GoavesManageForbidden",
	"photo-gallery" => "GoavesManagePhotoGalleries", "staff" => "GoavesManageStaff", "podcast" => "GoavesManagePodcasts",
	"soundslide" => "GoavesManageSoundslides", "requests" => "GoavesManageRequests", "mail" => "GoavesManageInbox",
	"lookup" => "GoavesManageLookup", "group" => "GoavesManageGroups", "special" => "GoAvesManageSpecial", "admin" => "GoAvesManageAdmin",
	"video" => "GoavesManageVideos", "interactions" => "GoavesManageInteractions", "leaf" => "GoavesManageLeaf");
$_ERROR_PAGES = array(404 => "page-not-found", 403 => "forbidden");

function parsePath($path, $page_definitions, $error_pages)
{
  $path_pieces = explode("/", $path);
  if (sizeof($path_pieces) == 0 || $path_pieces[0] == "")
    $path_pieces[0] = "index";
  else
    $path_pieces[0] = mb_strtolower($path_pieces[0]);
  if (!in_array($path_pieces[0], array_keys($page_definitions)))
  {
    if (is_dir(DOCUMENT_ROOT . "/new-manage/" . $path_pieces[0]))
	  return false;
    $path_pieces = array($error_pages[404], $path_pieces[0]);
  }
  return $path_pieces;
}
function selectPage($path_pieces, $page_definitions, $database)
{
  // Find Proper Page
  $selected_page = new $page_definitions[$path_pieces[0]]();
  $selected_page_final = $selected_page->checkOrRedirect($path_pieces, $database);
  while($selected_page_final !== true)
  {
    $selected_page = $selected_page_final;
	$selected_page_final = $selected_page->checkOrRedirect($path_pieces, $database);
  }
  if (method_exists($selected_page, "getRequirePermission") &&
	substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], $selected_page->getRequirePermission() - 1, 1) < "1")
	{
	  $forbidden_page = $selected_page;
	  $selected_page = new $page_definitions["forbidden"]();
	  $selected_page->setForbiddenPage($forbidden_page);
	}
  return $selected_page;
}
?>