<?php
class GoavesManageAdminStaffListing {
	private $_staff = array();
	function checkOrRedirect($path, $database)
	{
	  $all_staff = $database->query("SELECT identity, first_name, last_name, active, icon_file FROM staff ORDER BY last_name, first_name ASC");
	  while ($staff = $all_staff->fetchArray())
	    $this->_staff[] = array("IDENTITY" => $staff["identity"], "FIRST_NAME" => format_content($staff["first_name"]), "LAST_NAME" => format_content($staff["last_name"]),
			"ACTIVE" => ($staff["active"] == "TRUE"), "ICON" => $staff["icon_file"]);
	  return true;
	}
	function getRequirePermission() { return PERM_ADMIN; }
	function getPageHandle() { return "admin"; }
	function getPageSubhandle() { return "staff"; }
	function getPageTitle() { return "[Admin] Staff Listing"; }
	function getBreadTrail() { return array("admin" => "[Admin Level]", "admin/staff" => "Staff", "[this]" => "Staff Listing"); }
	function getPageStylesheet() { return "stylesheet-admin.css"; }
	function getBodyOnload() { return null; }
	function getPageJavascript()
	{
	  return "function toggleShowIcons(display)
	  {
	  }";
	}
	function getPageContents()
	{
	  if (sizeof($this->_staff) == 0)
	  {
	    echo "<div class=\"contentText\">There are no staff members in the database.</div>\n";
		return;
	  }
	  echo "<div class=\"staffTable\">\n";
	  echo "  <div class=\"header\">\n";
	  echo "    <div class=\"identity\">Identity</div>\n";
	  echo "    <div class=\"icon\">Icon</div>\n";
	  echo "    <div class=\"name\">Name</div>\n";
	  echo "    <div style=\"clear:both;\"></div>\n";
	  echo "  </div>\n";
	  foreach ($this->_staff as $staff)
	  {
	    echo "  <a href=\"" . MANAGE_WEB_PATH . "/admin/staff/" . $staff["IDENTITY"] . "/\" class=\"staffRow " . ($staff["ACTIVE"] ? "active" : "inactive") . "\">\n";
		echo "    <div class=\"name\">" . $staff["LAST_NAME"] . ", " . $staff["FIRST_NAME"] . "</div>\n";
	    echo "    <div class=\"identity\">" . $staff["IDENTITY"] . "</div>\n";
		//echo "    <img src=\"" . WEB_PATH . "/images/staff/" . $staff["ICON"] . "\" class=\"icon\" border=\"0\" />\n";
		echo "    <div style=\"clear:both;\"></div>\n";
		echo "  </a>\n";
	  }
	  echo "</div>\n";
	}
}
?>