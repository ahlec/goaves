<?php
class GoavesManagePhotoGalleries {
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[1]))
	    $path[1] = "landing";
	  switch ($path[1])
	  {
	    case "create": return new GoavesManagePhotoGalleriesCreate();
	    case "edit-info": return new GoavesManagePhotoGalleriesEditInformation();
	    case "list": return new GoavesManagePhotoGalleriesList();
	    case "manage": return new GoavesManagePhotoGalleriesManage();
	    case "upload-pictures": return new GoavesManagePhotoGalleriesUploadPictures();
		case "manage-pictures": return new GoavesManagePhotoGalleriesManagePictures();
	    default: return new GoavesManagePhotoGalleriesList();
	  }
	  return true;
	}
	function getRequirePermission() { return "2"; }
	function getPageHandle() { return "comic"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Comics"; }
	function getBreadTrail() { return array("[this]" => "Comics"); }
	function getPageStylesheet() { return "stylesheet-comics.css"; }
	function getPageContents()
	{
	  echo "comics";
	}
}
?>