<?php
class GoavesManageInteractionsPollManage {
	private $_pollIdentity;
	private $_handle;
	private $_dateStart;
	private $_dateEnd;
	private $_question;
	
	private $_responses = array();
	private $_totalVotes = 0;
	
	private $_responseDeleted = null;
	private $_responseAdded = null;
	private $_votesCleared = null;
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM polls WHERE identity='" .
		$database->escapeString($path[2]) . "'") == 0)
	  {
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-poll-manage", "LOADED" => false, "IDENTITY" => $path[2]);
		return new GoavesManageInteractionsList();
	  }
	  
	  $info = $database->querySingle("SELECT identity, poll_handle, date_start, date_end, poll_question FROM polls WHERE identity='" .
		$database->escapeString($path[2]) . "'", true);
	  $this->_pollIdentity = $info["identity"];
	  $this->_handle = $info["poll_handle"];
	  $this->_dateStart = strtotime($info["date_start"]);
	  $this->_dateEnd = strtotime($info["date_end"]);
	  $this->_question = format_content($info["poll_question"]);
		
	  $get_responses = $database->query("SELECT response_identity, response FROM poll_responses WHERE poll_identity='" . $this->_pollIdentity . "'");
	  while ($response = $get_responses->fetchArray())
	    $this->_responses[] = array("IDENTITY" => $response["response_identity"], "RESPONSE" => format_content($response["response"]),
			"VOTES" => $database->querySingle("SELECT count(*) FROM poll_votes WHERE poll_identity='" . $this->_pollIdentity . "' AND response_identity='" .
			$response["response_identity"] . "'"));
	  foreach ($this->_responses as $response)
	    $this->_totalVotes += $response["VOTES"];
		
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]))
	  {
	    switch ($_SESSION[MANAGE_TRANSFER_DATA]["FROM"])
		{
		  case "delete-content.php": $this->_responseDeleted = format_content($_SESSION[MANAGE_TRANSFER_DATA]["TITLE"]); break;
		  case "add-poll-response": $this->_responseAdded = format_content($_SESSION[MANAGE_TRANSFER_DATA]["RESPONSE"]); break;
		  case "clear-poll-votes": $this->_votesCleared = array("SCOPE" => $_SESSION[MANAGE_TRANSFER_DATA]["SCOPE"],
			"TITLE" => ($_SESSION[MANAGE_TRANSFER_DATA]["SCOPE"] == "response" ? format_content($database->querySingle("SELECT response FROM poll_responses WHERE response_identity='" .
			$database->escapeString($_SESSION[MANAGE_TRANSFER_DATA]["IDENTITY"]) . "' LIMIT 1")) : null)); break;
		}
		unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  return true;
	}
	function getRequirePermission() { return PERM_INTERACTIONS; }
	function getPageHandle() { return "interactions"; }
	function getPageSubhandle() { return "poll-manage"; }
	function getPageTitle() { return "[Poll] '" . $this->_question . "'"; }
	function getBreadTrail() { return array("interactions" => "Interactions", "interactions/list" => "Interactions Listing",
		"interactions/polls" => "Polls", "[this]" => "'" . $this->_question . "'"); }
	function getPageStylesheet() { return "stylesheet-interactions.css"; }
	function getBodyOnload() { return ($this->_responseDeleted != null ? "fadeOutAndHide('response-delete-success', 3000); " : null) .
		($this->_responseAdded != null ? "fadeOutAndHide('response-added-success', 3000); " : null) .
		($this->_votesCleared != null ? "fadeOutAndHide('votes-cleared-success', 3000); " : null); }
	function getPageContents()
	{	  
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <a class=\"returnLink\" href=\"" . MANAGE_WEB_PATH . "/interactions/polls/\">&laquo; Return</a>\n";
	  echo "  [Poll] '" . $this->_question . "'\n";
	  echo "</div>\n";
	  
	  if ($this->_responseDeleted != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"response-delete-success\"><b>Response deleted.</b> The response '" .
			$this->_responseDeleted . "' has been successfully removed from this poll, along with all votes cast for it.</div>\n";
	  if ($this->_responseAdded != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"response-added-success\"><b>Response added.</b> The response '" .
			$this->_responseAdded . "' has been added.</div>\n";
	  if ($this->_votesCleared != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"votes-cleared-success\"><b>Votes cleared.</b> " . ($this->_votesCleared["SCOPE"] == "poll" ?
			"All votes for this poll" : "Votes for the response '" . $this->_votesCleared["TITLE"] . "'") . " have been deleted.</div>\n";
	  
	  echo "<div class=\"indentedContent\">\n";
	  echo "  <div class=\"infoLine\"><b>Question:</b> " . $this->_question . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Identity:</b> " . $this->_pollIdentity . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Link:</b> <a href=\"" . WEB_PATH . "/poll/" .
	  	$this->_handle . "/\" target=\"_blank\">" . WEB_PATH . "/poll/" . $this->_handle .
	  	"/</a></div>\n";
	  
	  echo "  <div class=\"infoLine separator\"><b>Date start" . (date("Y-m-d") >= date("Y-m-d", $this->_dateStart) ? "ed" : "s") .
		":</b> " . date(LONG_DATE_FORMAT, $this->_dateStart) . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Date end" . (date("Y-m-d") > date("Y-m-d", $this->_dateEnd) ? "ed" : "s") . ":</b> " .
		date(LONG_DATE_FORMAT, $this->_dateEnd) . "</div>\n";
		
	  echo "  <div class=\"infoLine separator\"><b>Total votes:</b> " . $this->_totalVotes . "</div>\n";
	  
	  echo "</div>\n";
	  echo "<div style=\"clear:both;\"></div>\n";
	  echo "<div class=\"editLink separator\"><span class=\"fauxLink firstAction\" onClick=\"ManageWindow('edit-poll','" . 
		$this->_pollIdentity . "');\"><img src=\"" . MANAGE_WEB_PATH .
	  	"/interface/images/action_go.gif\" class=\"actionIcon\" /> Edit</span> \n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('delete-content', 'poll-" . $this->_pollIdentity .
	  	"');\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/folder_delete.gif\" class=\"actionIcon\" border=\"0\" /> Delete</span>\n";
	  echo "</div>\n";
	  echo "<div class=\"dividingBar miniSeparator\"></div>\n";
	  echo "<div class=\"indentedContent\">\n";
	  echo "  <div class=\"infoLine separator\"><b>Responses:</b></div>\n";
	  foreach ($this->_responses as $response)
	  {
	    echo "  <div class=\"pollResponseContainer\">\n";
		echo "    '<b>" . $response["RESPONSE"] . "</b>' <i>(Votes: " . $response["VOTES"] . ")</i>\n";
		echo "    <div class=\"actions\">\n";
		echo "      <span class=\"fauxLink\" onClick=\"ManageWindow('edit-poll-response','" . $response["IDENTITY"] . "');\"><img src=\"" .
			MANAGE_WEB_PATH . "/interface/images/table_edit.png\" class=\"actionIcon\" /> Edit</span>\n";
		if ($response["VOTES"] > 0)
			echo "      <span class=\"fauxLink\" onClick=\"ManageWindow('clear-poll-votes','" . $response["IDENTITY"] . "');\"><img src=\"" .
				MANAGE_WEB_PATH . "/interface/images/chart_line_delete.png\" class=\"actionIcon\" /> Delete votes</span>\n";
		if (sizeof($this->_responses) > 1)
			echo "      <span class=\"fauxLink\" onClick=\"ManageWindow('delete-content', 'poll_response-" . $response["IDENTITY"] . "');\"><img src=\"" .
				MANAGE_WEB_PATH . "/interface/images/table_delete.png\" class=\"actionIcon\" /> Delete</span>\n";
		echo "    </div>\n";
		echo "  </div>\n";
	  }
	  echo "</div>\n";
	  
	  echo "<div style=\"clear:both;\"></div>\n";
	  echo "<div class=\"editLink separator\"><span class=\"fauxLink firstAction\" onClick=\"ManageWindow('add-poll-response','" . 
		$this->_pollIdentity . "');\"><img src=\"" . MANAGE_WEB_PATH .
	  	"/interface/images/table_add.png\" class=\"actionIcon\" /> Add Response</span> \n";
	  if ($this->_totalVotes > 0)
		echo "<span class=\"fauxLink\" onClick=\"ManageWindow('clear-poll-votes', 'poll-" . $this->_pollIdentity . "');\"><img src=\"" .
			MANAGE_WEB_PATH . "/interface/images/chart_bar_delete.png\" class=\"actionIcon\" border=\"0\" /> Clear all votes</span> \n";
	  echo "</div>\n";
	}
}
?>