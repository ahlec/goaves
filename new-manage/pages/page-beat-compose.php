<?php
class GoavesManageBeatCompose {
	private $_beatTitleMinimum = 6;
	private $_categories = array();
	private $_relatedGroupTypes = array();
	private $_topics = array();
	function checkOrRedirect($path, $database)
	{
	  if (isset($path[2]) && strpos($path[2], "success-") === 0)
	  {
	    $supposed_success = $database->escapeString(str_replace("success-", "", $path[2]));
	    if ($database->querySingle("SELECT count(*) FROM beats WHERE beat_identity='" . $supposed_success .
	    	"' AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") > 0)
			{
			  $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-beat-compose", "SUCCESS" => true,
			  	"IDENTITY" => $supposed_success);
			  header("Location: " . MANAGE_WEB_PATH . "/beat/list/");
			  exit();
			}
	  }
	  
	  $valid_post_categories = "'" . rtrim(str_replace("|", "','", $database->querySingle("SELECT valid_article_types FROM staff " .
	  	"WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' LIMIT 1")), ",'") . "'";
	  $get_categories = $database->query("SELECT type_identity, title, icon_file FROM beat_types WHERE " .
	  	"type_identity IN (" . $valid_post_categories . ") ORDER BY sort_order ASC");
	  while ($category = $get_categories->fetchArray())
	    $this->_categories[] = array("IDENTITY" => $category["type_identity"], "TITLE" => format_content($category["title"]),
	    	"ICON" => $category["icon_file"]);
	  
	  $group_types = $database->query("SELECT type_identity, plural_name, handle FROM group_types ORDER BY type_identity ASC");
	  while ($group_type = $group_types->fetchArray())
	  {
	    $get_groups = $database->query("SELECT group_identity, title, active FROM groups WHERE type='" .
			$group_type["type_identity"] . "' ORDER BY title ASC");
		$groups_with_type = array();
		while ($group = $get_groups->fetchArray())
		  $groups_with_type[] = array("IDENTITY" => $group["group_identity"], "TITLE" => format_content($group["title"]),
			"ACTIVE" => ($group["active"] == "TRUE"));
	    $this->_relatedGroupTypes[$group_type["handle"]] = array("NAME" => $group_type["plural_name"],
			"GROUPS" => $groups_with_type);
	  }
	  
	  $topics = $database->query("SELECT topic_identity, title FROM topics WHERE (date_start <= '" .
		date("Y-m-d", strtotime("+1 hours")) . "' AND date_end >= '" . date("Y-m-d", strtotime("+1 hours")) . "') OR (" .
		"date_start IS NULL AND date_end IS NULL) ORDER BY title ASC");
	  while ($topic = $topics->fetchArray())
	    $this->_topics[] = array("IDENTITY" => $topic["topic_identity"], "TITLE" => format_content($topic["title"]));
	  
	  return true;
	}
	function getRequirePermission() { return "1"; }
	function getPageHandle() { return "beat"; }
	function getPageSubhandle() { return "compose"; }
	function getPageTitle() { return "Compose New Beat"; }
	function getBreadTrail() { return array("beat" => "Beats", "[this]" => "Compose New Beat"); }
	function getPageStylesheet() { return "stylesheet-beat.css"; }
	function getPageOnBeforeUnload() { return "return getWarnPageRefresh();"; }
	function getPageJavascriptFile() { return "javascript-compose-beat.js"; }
	function getPageJavascript()
	{
	  $group_handles = "[";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	    $group_handles .= "'" . $handle . "',";
	  $group_handles = rtrim($group_handles, ",") . "]";
	  return "var beat_posted = false;
	  var group_types = " . $group_handles . ";";
	}
	function getPageContents()
	{
	  echo "<div class=\"contentHeader\">Compose New Beat</div>\n";
	  
	  echo "<div class=\"timeline\">\n";
	  echo "<div class=\"piece pieceStart pieceCurrent\" id=\"timeline-step-one\"><b>Step One</b>Title</div>";
	  echo "<div class=\"piece\" id=\"timeline-step-two\"><b>Step Two</b>Linkings</div>";
	  echo "<div class=\"piece\" id=\"timeline-step-three\"><b>Step Three</b>Article</div>";
	  echo "<div class=\"piece\" id=\"timeline-step-four\"><b>Step Four</b>Pictures</div>";
	  echo "<div class=\"piece\" id=\"timeline-step-five\"><b>Step Five</b>Miscellaneous</div>";
	  echo "<div class=\"piece pieceEnd\" id=\"timeline-step-six\"><b>Step Six</b>Post</div>";
	  echo "</div>\n";
	  
	  echo "<div class=\"createForm noWindow\">\n";
	  
	  echo "<div id=\"container-step-one\" class=\"separator\">\n";
	  echo "  <div class=\"formLabel\">Title:</div>\n";
	  echo "  <input type=\"text\" class=\"inputText\" name=\"beat-title\" id=\"beat-title\" autocomplete=\"off\"
	  	onKeyUp=\"executeAJAX(web_path + '/components/" .
		"check-title.php?type=beat&minimum=" . $this->_beatTitleMinimum . "&title=' + escapeStringURL(this.value), function " .
		"processTitle(return_value) {
		  if (return_value.indexOf('[Error]') >= 0)
		  {
		    document.getElementById('beat-title-error').innerHTML = return_value.replace('[Error] ', '');
		    if (document.getElementById('beat-title-error').isFading != true)
		    {
		      document.getElementById('beat-title-error').style.display = 'block';
		      fadeOutAndHide('beat-title-error', 2400);
		    }
		    document.getElementById('beat-title').className = 'inputText formComponentInvalid';
		    document.getElementById('step-one-continue').disabled = true;
		  } else
		  {
		    document.getElementById('beat-title-error').style.display = 'none';
		    document.getElementById('beat-title').className = 'inputText formComponentValid';
		    document.getElementById('step-one-continue').disabled = false;
		  } });\" />\n";
	  echo "  <div class=\"contentError\" id=\"beat-title-error\" style=\"display:none;\"></div>\n";
	  echo "  <center><input type=\"button\" class=\"submitButton separator\" disabled=\"disabled\" value=\"Continue &raquo;\" " .
	  	"id=\"step-one-continue\" onClick=\"goStepTwo();\" /></center>\n";
	  echo "</div>\n";
	  
	  /* Step Two */
	  echo "<div id=\"container-step-two\" class=\"separator\" style=\"display:none;\">\n";
	  echo "  <div class=\"formLabel\">Category:</div>\n";
	  echo "  <input type=\"hidden\" id=\"article-type\"" . (sizeof($this->_categories) == 1 ? " value=\"" .
	  	$this->_categories[0]["IDENTITY"] . "\"" : "") . " />\n";
	  echo "  <div id=\"beatTypes\">\n";
	  foreach ($this->_categories as $category)
	    echo "  <img src=\"" . WEB_PATH . "/images/article_types/" . ($category["ICON"] != "" ? $category["ICON"] :
	    	"no-category-icon.jpg") . "\" class=\"categoryIcon\" onClick=\"selectCategory('" .
	    	$category["IDENTITY"] . "');\" title=\"" . $category["TITLE"] . "\" id=\"beatType-" .
	    	$category["IDENTITY"] . "\"/>\n";
	  echo "</div>\n";
	  
	  echo "  <div class=\"formLabel separator\">Related Group:</div>\n";
	  echo "  <input type=\"checkbox\" id=\"related-group-use\" onClick=\"toggleUseRelatedGroup();\" /> " .
		"<label for=\"related-group-use\">Link to a related group?</label><br />\n";
	  echo "  <select id=\"related-group-type\" style=\"display:none;\" class=\"inputSelect selectLeft\" " .
		"onChange=\"selectGroupByType(this.value);\">\n";
	  echo "    <option value=\"[none]\">(Group Type)</option>\n";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	    echo "    <option value=\"" . $handle . "\">" . $info["NAME"] . "</option>\n";
	  echo "  </select>\n";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	  {
	    echo "  <select id=\"related-group-" . $handle .
			"\" class=\"inputSelect selectRight\" style=\"display:none;\" " .
			"onChange=\"ProcessStepTwoCompletion();\">\n";
		echo "    <option value=\"[none]\">(Group)</option>\n";
		foreach ($info["GROUPS"] as $group)
		  echo "    <option value=\"" . $group["IDENTITY"] . "\">" . $group["TITLE"] .
			(!$group["ACTIVE"] ? " (Inactive)" : "") . "</option>\n";
	    echo "  </select>\n";
	  }
	  echo "  <div class=\"formLabel separator\" style=\"clear:both;\">Topic:</div>\n";
	  if (sizeof($this->_topics) > 0)
	  {
	    echo "  <input type=\"checkbox\" id=\"topic-use\" onClick=\"toggleUseTopic();\" /> " .
		"<label for=\"topic-use\">Link to a topic?</label><br />\n";
	    echo "  <select id=\"topic\" class=\"inputSelect\" style=\"display:none;\" onChange=\"ProcessStepTwoCompletion();\">\n";
	    echo "    <option value=\"[none]\">(Topic)</option>\n";
	    foreach ($this->_topics as $topic)
	      echo "    <option value=\"" . $topic["IDENTITY"] . "\">" . $topic["TITLE"] . "</option>\n";
	    echo "  </select>\n";
	  } else
	    echo "  <div>There are no topics currently active.</div>\n";
	  echo "  <center><input type=\"button\" class=\"submitButton separator\" disabled=\"disabled\" value=\"Continue &raquo;\" " .
	  	"id=\"step-two-continue\" onClick=\"goStepThree();\" /></center>\n";
	  echo "</div>\n";
	  
	  /* Step Three */
	  echo "<div id=\"container-step-three\" class=\"separator\" style=\"display:none;\">\n";
	  echo "  <div class=\"formLabel\">Article:</div>\n";
	  echo "  <textarea id=\"beat-article\" class=\"beatArticle\"></textarea>\n";
	  echo "  <input type=\"button\" class=\"submitButton\" disabled=\"disabled\" value=\"Continue &raquo;\" " .
	  	"id=\"step-three-continue\" onClick=\"goStepFour();\" />\n";
	  echo "</div>\n";
	  
	  /* Step Four */
	  echo "<div id=\"container-step-four\" class=\"separator\" style=\"display:none;\">\n";
	  echo "  <iframe frameborder=\"0\" id=\"frame-article-image\" name=\"imageForm\" " .
	  	"class=\"formFrame\"></iframe>\n";
	  echo "<div class=\"createForm\">\n";
	  echo "  <div class=\"formLabel\">Image File:</div>\n";
	  echo "  <input type=\"hidden\" id=\"image-filepath\" value=\"\" />\n";
	  echo "  <div class=\"inputFile\" id=\"image-input\">\n";
	  echo "    <div class=\"button\">Select</div>\n";
	  echo "    <div class=\"textBox\" id=\"image-input-textbox\"></div>\n";
	  echo "    <form method=\"POST\" enctype=\"multipart/form-data\" target=\"imageForm\" action=\"" .
	  	MANAGE_WEB_PATH . "/components/upload-beat-image.php\" " .
		"id=\"image-upload-form\">\n";
	  echo "      <input type=\"file\" class=\"inputFile\" name=\"beat-image\" " .
	  	"onChange=\"document.getElementById('image-upload-form').submit();" .
		"document.getElementById('frame-article-image').style.display = 'block';\" id=\"beat-image\" />\n";
	  echo "    </form>\n";
	  echo "  </div>\n";
	  
	  echo "  <div class=\"formLabel separator\">Icon File:</div>\n";
	  echo "  <input type=\"hidden\" id=\"image-icon\" value=\"\" />\n";
	  echo "  <div class=\"iconCreationContainer\" id=\"icon-creation-container\">\n";
	  echo "  You need to first upload an image to create an icon.\n";
	  echo "  </div>\n";
	  
	  echo "  <div class=\"formLabel separator\">Image Alignment:</div>\n";
	  echo "  <input type=\"hidden\" id=\"image-alignment\" value=\"left\" />\n";
	  echo "  <img src=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/icon-align-left.png\" " .
	  	"id=\"alignment-left\" class=\"alignmentIcon alignmentSelected\" onClick=\"changeAlignment('left');\" />\n";
	  echo "  <img src=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/icon-align-right.png\" " .
	  	"id=\"alignment-right\" class=\"alignmentIcon alignmentUnselected\" onClick=\"changeAlignment('right');\" />\n";
	  
	  echo "  <div class=\"formLabel separator\">Image Credit:</div>\n";
	  echo "  <input type=\"text\" class=\"inputText\" id=\"image-credit\" onKeyUp=\"processImageCredit(this.value);" .
	  	"processStepFourConfirmation();\" />\n";
	  echo "  <div id=\"preview-image-credit\" style=\"display:none;\" class=\"previewPhotoCredit\"></div>\n";
	  
	  echo "  <div class=\"formLabel separator\">Image Caption:</div>\n";
	  echo "  <textarea id=\"image-caption\" style=\"width:100%;\"></textarea>\n";
	  echo "</div>\n";
	  echo "<center>\n";
	  echo "  <input type=\"button\" class=\"submitButton separator\" disabled=\"disabled\" value=\"Continue &raquo;\" " .
	  	"id=\"step-four-continue\" onClick=\"goStepFive();\" />\n";
	  echo "</center>\n";
	  echo "</div>\n";
	  
	  echo "<div id=\"container-step-five\" class=\"separator\" style=\"display:none;\">\n";
	  echo "  <div class=\"contentText\">There are no extra options available yet.</div>\n";
	  echo "  <input type=\"button\" class=\"submitButton\" value=\"Continue &raquo;\" " .
	  	"id=\"step-five-continue\" onClick=\"goStepSix();\" />\n";
	  echo "</div>\n";
	  
	  echo "<div id=\"container-step-six\" class=\"separator\" style=\"display:none;\">\n";
	  echo "  <iframe frameborder=\"0\" id=\"frame-article-image\" name=\"composeForm\" " .
	  	"class=\"formFrame\"></iframe>\n";
	  echo "  <form method=\"POST\" target=\"composeForm\" action=\"" . MANAGE_WEB_PATH . "/components/compose-beat.php\">\n";
	  echo "    <input type=\"hidden\" name=\"title\" id=\"final-title\" />\n";
	  echo "    <input type=\"hidden\" name=\"type\" id=\"final-type\" />\n";
	  echo "    <input type=\"hidden\" name=\"related-group\" id=\"final-related-group\" />\n";
	  echo "    <input type=\"hidden\" name=\"topic\" id=\"final-topic\" />\n";
	  echo "    <input type=\"hidden\" name=\"article\" id=\"final-article\" />\n";
	  echo "    <input type=\"hidden\" name=\"image-file\" id=\"final-image-file\" />\n";
	  echo "    <input type=\"hidden\" name=\"icon-file\" id=\"final-icon-file\" />\n";
	  echo "    <input type=\"hidden\" name=\"icon-x\" id=\"final-icon-x\" />\n";
	  echo "    <input type=\"hidden\" name=\"icon-y\" id=\"final-icon-y\" />\n";
	  echo "    <input type=\"hidden\" name=\"icon-width\" id=\"final-icon-width\" />\n";
	  echo "    <input type=\"hidden\" name=\"icon-height\" id=\"final-icon-height\" />\n";
	  echo "    <input type=\"hidden\" name=\"alignment\" id=\"final-alignment\" />\n";
	  echo "    <input type=\"hidden\" name=\"photo-credit\" id=\"final-photo-credit\" />\n";
	  echo "    <input type=\"hidden\" name=\"caption\" id=\"final-caption\" />\n";
	  echo "    <center><input type=\"submit\" class=\"submitButton separator\" value=\"Post\" /></center>\n";
	  echo "  </form>\n";
	  echo "</div>\n";
	  
	  echo "</div>\n";
	  echo "  <div style=\"clear:both;\"></div>\n";
	}
}
?>