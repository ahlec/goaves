<?php
class GoavesManagePhotoGalleriesCreate {
	private $_galleryTitleMinimum = 6;
	private $_relatedGroupTypes = array();
	private $_topics = array();
	function checkOrRedirect($path, $database)
	{
	  if (isset($path[2]) && strpos($path[2], "success-") === 0)
	  {
	    $supposed_success = $database->escapeString(str_replace("success-", "", $path[2]));
		if ($database->querySingle("SELECT count(*) FROM photo_galleries WHERE gallery_identity='" . $supposed_success . "' AND staff_identity='" .
			$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") > 0)
			{
			  $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-photo-galleries-create", "SUCCESS" => true, "IDENTITY" => $supposed_success);
			  header("Location: " . MANAGE_WEB_PATH . "/photo-gallery/list/");
			  exit();
			}
	  }
	  $group_types = $database->query("SELECT type_identity, plural_name, handle FROM group_types ORDER BY type_identity ASC");
	  while ($group_type = $group_types->fetchArray())
	  {
	    $get_groups = $database->query("SELECT group_identity, title, active FROM groups WHERE type='" .
			$group_type["type_identity"] . "' ORDER BY title ASC");
		$groups_with_type = array();
		while ($group = $get_groups->fetchArray())
		  $groups_with_type[] = array("IDENTITY" => $group["group_identity"], "TITLE" => format_content($group["title"]),
			"ACTIVE" => ($group["active"] == "TRUE"));
	    $this->_relatedGroupTypes[$group_type["handle"]] = array("NAME" => $group_type["plural_name"],
			"GROUPS" => $groups_with_type);
	  }
	  
	  $topics = $database->query("SELECT topic_identity, title FROM topics WHERE (date_start <= '" .
		date("Y-m-d", strtotime("+1 hours")) . "' AND date_end >= '" . date("Y-m-d", strtotime("+1 hours")) . "') OR (" .
		"date_start IS NULL AND date_end IS NULL) ORDER BY title ASC");
	  while ($topic = $topics->fetchArray())
	    $this->_topics[] = array("IDENTITY" => $topic["topic_identity"], "TITLE" => format_content($topic["title"]));
	  return true;
	}
	function getRequirePermission() { return "2"; }
	function getPageHandle() { return "photo-gallery"; }
	function getPageSubhandle() { return "create"; }
	function getPageTitle() { return "Create New Photo Gallery"; }
	function getBreadTrail() { return array("photo-gallery" => "Photo Galleries", "[this]" => "Create New Photo Gallery"); }
	function getPageStylesheet() { return "stylesheet-galleries.css"; }
	function getPageOnBeforeUnload() { return "return getWarnPageRefresh();"; }
	function getPageJavascript()
	{
	  $group_handles = "[";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	    $group_handles .= "'" . $handle . "',";
	  $group_handles = rtrim($group_handles, ",") . "]";
	  return "var gallery_created = false;
	  var group_types = " . $group_handles . ";
	  function toggleUseRelatedGroup()
	  {
	    document.getElementById('gallery-related-group-type').selectedIndex = 0;
	    document.getElementById('gallery-related-group-type').style.display =
			(document.getElementById('gallery-related-group-use').value ? 'block' : 'none');
		for (var type_index = 0; type_index < group_types.length; type_index++)
		  document.getElementById('gallery-related-group-' + group_types[type_index]).style.display = 'none';
	  }
	  function toggleUseTopic()
	  {
	    document.getElementById('gallery-topic').selectedIndex = 0;
		document.getElementById('gallery-topic').style.display = (document.getElementById('gallery-topic-use').value ? 'block' : 'none');
	  }
	  function selectGroupByType(type_handle)
	  {
	    for (var type_index = 0; type_index < group_types.length; type_index++)
		  document.getElementById('gallery-related-group-' + group_types[type_index]).style.display = 'none';
	    if (type_handle == '[none]')
		  return;
		document.getElementById('gallery-related-group-' + type_handle).selectedIndex = 0;
	    document.getElementById('gallery-related-group-' + type_handle).style.display = 'block';
	  }
	  function checkFormValid()
	  {
	    if (document.getElementById('gallery-title').className == 'inputText formComponentValid')
		{
		  document.getElementById('gallery-create').disabled = false;
		  return;
		}
		document.getElementById('gallery-create').disabled = true;
	  }
	  function getWarnPageRefresh()
	  {
	    if (gallery_created)
		  return null;
	    if (document.getElementById('gallery-title').value == '')
		  return null;
		return 'Are you sure you wish to leave this page? (Unsaved content will be lost unless you save it)';
	  }
	  function processSubmitCreate()
	  {
	    if (document.getElementById('gallery-create').disabled)
		  return;
		var createCall = web_path + '/components/create-photo-gallery.php?title=' + encodeURIComponent(document.getElementById('gallery-title').value);
		if (document.getElementById('gallery-related-group-use').value)
		{
		  var selected_group_type = document.getElementById('gallery-related-group-type').value;
		  if (selected_group_type != '[none]' && document.getElementById('gallery-related-group-' + selected_group_type).value != '[none]')
		    createCall += '&related_group=' + document.getElementById('gallery-related-group-' + selected_group_type).value;
		}
		if (document.getElementById('gallery-topic') != undefined && document.getElementById('gallery-topic-use').value &&
		  document.getElementById('gallery-topic').value != '[none]')
		  createCall += '&topic=' + document.getElementById('gallery-topic').value;
		executeAJAX(createCall, function processCreate(result)
		{
		  if (result.indexOf('[Error]') < 0)
		  {
		    gallery_created = true;
		    window.location = '" . MANAGE_WEB_PATH . "/photo-gallery/create/success-' + result + '/';
			return;
		  }
		  alert(result);
		});
	  }";
	}
	function getPageContents()
	{
	  echo "<div class=\"contentHeader\">Create New Photo Gallery</div>\n";
	  
	  echo "<div class=\"createForm\">\n";
	  echo "  <div class=\"formLabel\">Title:</div>\n";
	  echo "  <input type=\"text\" class=\"inputText\" name=\"gallery-title\" id=\"gallery-title\" autocomplete=\"off\" onKeyUp=\"executeAJAX(web_path + '/components/" .
		"check-title.php?type=gallery&minimum=" . $this->_galleryTitleMinimum . "&title=' + encodeURIComponent(this.value), function " .
		"processTitle(return_value) {
		  if (return_value.indexOf('[Error]') >= 0)
		  {
		    document.getElementById('gallery-title-error').innerHTML = return_value.replace('[Error] ', '');
			if (document.getElementById('gallery-title-error').isFading != true)
			{
		      document.getElementById('gallery-title-error').style.display = 'block';
			  fadeOutAndHide('gallery-title-error', 2400);
		    }
			document.getElementById('gallery-title').className = 'inputText formComponentInvalid';
			document.getElementById('gallery-create').disabled = true;
		  } else
		  {
		    document.getElementById('gallery-title-error').style.display = 'none';
		    document.getElementById('gallery-title').className = 'inputText formComponentValid';
			checkFormValid();
		  } });\" />\n";
	  echo "  <div class=\"contentError\" id=\"gallery-title-error\" style=\"display:none;\"></div>\n";
	  echo "  <div class=\"formLabel separator\">Related Group:</div>\n";
	  echo "  <input type=\"checkbox\" id=\"gallery-related-group-use\" onChange=\"toggleUseRelatedGroup();\" /> " .
		"<label for=\"gallery-related-group-use\">Link to a related group?</label><br />\n";
	  echo "  <select id=\"gallery-related-group-type\" style=\"display:none;\" class=\"inputSelect selectLeft\" " .
		"onChange=\"selectGroupByType(this.value);\">\n";
	  echo "    <option value=\"[none]\">(Group Type)</option>\n";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	    echo "    <option value=\"" . $handle . "\">" . $info["NAME"] . "</option>\n";
	  echo "  </select>\n";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	  {
	    echo "  <select id=\"gallery-related-group-" . $handle .
			"\" class=\"inputSelect selectRight\" style=\"display:none;\">\n";
		echo "    <option value=\"[none]\">(Group)</option>\n";
		foreach ($info["GROUPS"] as $group)
		  echo "    <option value=\"" . $group["IDENTITY"] . "\">" . $group["TITLE"] .
			(!$group["ACTIVE"] ? " (Inactive)" : "") . "</option>\n";
	    echo "  </select>\n";
	  }
	  echo "  <div class=\"formLabel separator\" style=\"clear:both;\">Topic:</div>\n";
	  if (sizeof($this->_topics) > 0)
	  {
	    echo "  <input type=\"checkbox\" id=\"gallery-topic-use\" onChange=\"toggleUseTopic();\" /> " .
		"<label for=\"gallery-topic-use\">Link to a topic?</label><br />\n";
	    echo "  <select id=\"gallery-topic\" class=\"inputSelect\" style=\"display:none;\">\n";
	    echo "    <option value=\"[none]\">(Topic)</option>\n";
	    foreach ($this->_topics as $topic)
	      echo "    <option value=\"" . $topic["IDENTITY"] . "\">" . $topic["TITLE"] . "</option>\n";
	    echo "  </select>\n";
	  } else
	    echo "  <div>There are no topics currently active.</div>\n";
	  echo "  <center><input type=\"button\" class=\"submitButton\" id=\"gallery-create\" value=\"Create\" disabled=\"disabled\" " .
		"onClick=\"processSubmitCreate();\" /></center>\n";
	  echo "</div>\n";
	  echo "  <div style=\"clear:both;\"></div>\n";
	}
}
?>