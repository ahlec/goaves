<?php
class GoavesManageBeatEditText {
	private $_beatIdentity;
	private $_title;
	private $_text;
	private $_staffInfo = null;
	
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM beats WHERE beat_identity='" .
		$database->escapeString($path[2]) . "'" . (isModeratorOver("1") ? "" : " AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) .
		"'")) == 0)
	  {
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-beat-edit-text", "LOADED" => false, "IDENTITY" => $path[2]);
		return new GoavesManageBeatManage();
	  }
	  
	  $info = $database->querySingle("SELECT beat_identity, beats.title, contents, staff_identity, staff.first_name, staff.last_name, staff.icon_file AS \"staff_icon\" " .
		"FROM beats JOIN staff ON beats.staff_identity = staff.identity WHERE beat_identity='" . $database->escapeString($path[2]) . "' LIMIT 1", true);
	  $this->_beatIdentity = $info["beat_identity"];
	  if ($info["staff_identity"] != $_SESSION[MANAGE_SESSION]["IDENTITY"])
	    $this->_staffInfo = array("IDENTITY" => $info["staff_identity"], "NAME" => $info["first_name"] . " " . $info["last_name"], "ICON" => $info["staff_icon"]);
	  $this->_title = format_content($info["title"]);
	  $this->_text = format_content($info["contents"]);
	  
	  return true;
	}
	function getRequirePermission() { return "1"; }
	function getPageHandle() { return "beat"; }
	function getPageSubhandle() { return "manage"; }
	function getPageTitle() { return "[Beat] Edit Text '" . $this->_title . "'"; }
	function getBreadTrail() { return array("beat" => "Beats", "beat/list" => "My Beats",
		("beat/manage/" . $this->_beatIdentity) => "'" . $this->_title . "'", "[this]" => "Edit Text"); }
	function getPageStylesheet() { return "stylesheet-beat.css"; }
	function getBodyOnload() { return null; }
	function getPageJavascript()
	{
	   return 'tinyMCE.init({
            mode : "exact",
            elements : "beat-article",
            entity_encoding : "named",
            theme : "advanced",
            skin : "o2k7",
            editor_selector : "advanced-textarea",
            plugins : "safari,spellchecker,pagebreak,style,layer,save,advhr,advimage,advlink," +
	    	"iespell,inlinepopups,preview,media,searchreplace,print,contextmenu,paste,directionality," +
	    	"fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
            theme_advanced_buttons1 : "cut,copy,paste,pastetext,|,search,replace,|,undo,redo,|,cleanup,code,|," +
	    	"preview,|,fullscreen,removeformat,visualaid,|,styleprops,spellchecker,|,cite,abbr,acronym,del,inc,attribs,",
            theme_advanced_buttons2 : "bold,italic,underline,strikethrough,|,forecolor,link,unlink,|,sub,sup,|,charmap," +
	    	"iespell,|,bullist,numlist,|,outdent,indent",
	    theme_advanced_buttons3 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : false,
            template_external_list_url : "js/template_list.js",
            external_link_list_url : "js/link_list.js",
            remove_trailing_nbsp : true,
            height : "200"
          });
		  function processSubmitUpdate()
		  {
		    document.getElementById("beat-edit").disabled = true;
			document.getElementById("update-form").submit();
			document.getElementById("update-form").disabled = true;
			ManageWindow("process-in-action");
		  }
		  function displayError(message)
		  {
		    openWindow.closeWindow();
			ManageWindow("error", escapeStringURL(message));
			document.getElementById("beat-edit").disabled = false;
			document.getElementById("update-form").disabled = false;
		  }';
	}
	function getPageContents()
	{	  
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <a class=\"returnLink\" href=\"" . MANAGE_WEB_PATH . "/beat/manage/" . $this->_beatIdentity . "/\">&laquo; Return</a>\n";
	  echo "  " . ($this->_staffInfo != null ? "<span class=\"moderator\">[Moderate]</span> " : "") . "Edit Beat '" . $this->_title . "' Text\n";
	  echo "</div>\n";
			
	  echo "<div class=\"createForm noWindow\">\n";
	  echo "  <iframe id=\"frame-edit\" name=\"updateForm\" class=\"formFrame\" style=\"display:none;\"></iframe>\n";
	  echo "  <div class=\"infoLine\"><b>Text:</b></div>\n";
	  echo "  <form method=\"POST\" action=\"" . MANAGE_WEB_PATH . "/components/update-beat-text.php?identity=" .
		$this->_beatIdentity . "\" target=\"updateForm\" id=\"update-form\">\n";
	  echo "    <textarea id=\"beat-article\" class=\"beatArticle\" name=\"article\">" . 
		$this->_text . "</textarea>\n";
	  echo "  </form>\n";
	  echo "  <center><input type=\"button\" class=\"submitButton separator\" id=\"beat-edit\" " .
		"value=\"Update\" onClick=\"processSubmitUpdate();\" /></center>\n";
	  echo "</div>\n";
	  echo "<div style=\"clear:both;\"></div>\n";
	}
}
?>