<?php
class GoavesManageGroupsManage {
	private $_groupIdentity;
	private $_handle;
	private $_title;
	private $_imageFile;
	private $_iconFile;
	private $_description;
	private $_missionStatement;
	private $_type = array();
	private $_active;
	private $_yearsActive;
	private $_groupLastUpdated;
	
	private $_advisors = array();
	private $_subgroups = array();
	
	private $_editSuccess = null;
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" .
		$database->escapeString($path[2]) . "'") == 0)
	  {
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-groups-manage", "LOADED" => false, "IDENTITY" => $path[2]);
		return new GoavesManageGroupsList();
	  }
	  if (isset($path[3]) && strtolower($path[3]) == "edit-information")
	    return new GoavesManageBeatEditInfo();
	  if (isset($path[3]) && strtolower($path[3]) == "edit-text")
	    return new GoavesManageBeatEditText();
	  
	  $info = $database->querySingle("SELECT group_identity, groups.handle, title, image_file, icon_file, description, mission_statement, type, " .
		"group_types.handle AS \"type_handle\", group_types.singular_name AS \"type_singular_name\", group_types.plural_name AS \"type_plural_name\", " .
		"group_types.roster_title, group_types.roster_administrator_title_singular, group_types.roster_administrator_title_plural, active, years_active, " .
		"group_last_updated FROM groups JOIN group_types ON groups.type = group_types.type_identity WHERE group_identity='" .
		$database->escapeString($path[2]) . "'", true);
	  $this->_groupIdentity = $info["group_identity"];
	  $this->_handle = $info["handle"];
	  $this->_title = format_content($info["title"]);
	  $this->_imageFile = $info["image_file"];
	  $this->_iconFile = $info["icon_file"];
	  $this->_description = ($info["description"] != "" ? format_content($info["description"]) : null);
	  $this->_missionStatement = ($info["mission_statement"] != "" ? format_content($info["mission_statement"]) : null);
	  $this->_type = array("IDENTITY" => $info["type"], "HANDLE" => $info["type_handle"], "SINGULAR_NAME" => format_content($info["type_singular_name"]),
		"PLURAL_NAME" => format_content($info["type_plural_name"]), "ROSTER_TITLE" => format_content($info["roster_title"]), "ADMINISTRATOR_TITLE_SINGULAR" =>
		format_content($info["roster_administrator_title_singular"]), "ADMINISTRATOR_TITLE_PLURAL" => format_content($info["roster_administrator_title_plural"]));
	  $this->_active = ($info["active"] == "TRUE");
	  $this->_yearsActive = $info["years_active"];
	  $this->_groupLastUpdated = strtotime($info["group_last_updated"]);
		
	  $advisors = $database->query("SELECT advisor_identity, first_name, last_name, gender, position FROM group_advisors WHERE group_identity='" . $this->_groupIdentity .
		"'");
	  while ($advisor = $advisors->fetchArray())
	    $this->_advisors[] = array("IDENTITY" => $advisor["advisor_identity"], "FIRST_NAME" => format_content($advisor["first_name"]),
			"LAST_NAME" => format_content($advisor["last_name"]), "GENDER" => $advisor["gender"], "POSITION" => format_content($advisor["position"]));
		
	  $subgroups = $database->query("SELECT subgroup_identity, name, handle, image_file FROM group_subgroups WHERE group_identity='" . $this->_groupIdentity . "'");
	  while ($subgroup = $subgroups->fetchArray())
	    $this->_subgroups[] = array("IDENTITY" => $subgroup["subgroup_identity"], "NAME" => format_content($subgroup["name"]), "HANDLE" => $subgroup["handle"],
			"IMAGE" => ($subgroup["image_file"]));
		
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]))
	  {
	    switch ($_SESSION[MANAGE_TRANSFER_DATA]["FROM"])
		{
		  case "update-group": $this->_editSuccess = "info"; break;
		}
		unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  return true;
	}
	function getRequirePermission() { return PERM_GROUPS; }
	function getPageHandle() { return "group"; }
	function getPageSubhandle() { return "manage"; }
	function getPageTitle() { return "[Group] '" . $this->_title . "'"; }
	function getBreadTrail() { return array("group" => "Group Listing", "[this]" => "'" . $this->_title . "'"); }
	function getPageStylesheet() { return "stylesheet-groups.css"; }
	function getBodyOnload() { return ($this->_editSuccess != null ? "fadeOutAndHide('edit-success', 3000); " : null); }
	function getPageJavascript()
	{
	  return "function updateIconCoords(cropbox)
	  {
		var image_ratio = document.getElementById('group-image-image-ratio').value;
		document.getElementById('group-icon-crop-x').value = cropbox.x * image_ratio;
		document.getElementById('group-icon-crop-y').value = cropbox.y * image_ratio;
		document.getElementById('group-icon-crop-width').value = cropbox.w * image_ratio;
		document.getElementById('group-icon-crop-height').value = cropbox.h * image_ratio;
	  }
	  function groupImageUploaded()
	  {
	    document.getElementById('group-image-image-final').style.display = 'none';
	    document.getElementById('preview-group-image').src = '" . MANAGE_WEB_PATH . "/limbo/' + document.getElementById('group-image').value;
		document.getElementById('groupImageCaption').innerHTML = '<b>(New Image)</b>';
	    document.getElementById('group-icon-crop-img').src = '" . MANAGE_WEB_PATH . "/purgatory/' + document.getElementById('group-image-crop-image-filepath').value;
	    jQuery(function() {
			jQuery('#group-icon-crop-img').Jcrop({
			aspectRatio: 1,
			bgOpcity: 0.4,
			onSelect: updateIconCoords,
			onChange: updateIconCoords
			});
		});
		document.getElementById('group-icon-crop-container').style.display = 'block';
	  }
	  function cropGroupIcon()
	  {
		var crop_x = parent.document.getElementById('group-icon-crop-x').value;
		var crop_y = parent.document.getElementById('group-icon-crop-y').value;
		var crop_width = parent.document.getElementById('group-icon-crop-width').value;
		var crop_height = parent.document.getElementById('group-icon-crop-height').value;
		if (crop_x == null || crop_y == null || crop_width == null || crop_height == null)
		{
			alert ('Must make a selection on the uploaded image before continuing.');
			return;
		}
		executeAJAX('" . MANAGE_WEB_PATH . "/components/crop-image.php?image=' + document.getElementById('group-image-original').value + '&x=' + crop_x + '&y=' + crop_y +
			'&width=' + crop_width + '&height=' + crop_height + '&crop_width=150&crop_height=150&prefix=group-icon-',
			function process(results)
			{
				if (results.indexOf('~') === 0)
				{
					document.getElementById('group-icon').value = results.substring(1);
					document.getElementById('group-icon-crop-container').style.display = 'none';
					document.getElementById('preview-group-icon').src = '" . MANAGE_WEB_PATH . "/limbo/' + document.getElementById('group-icon').value;
					document.getElementById('confirm-upload').disabled = false;
					document.getElementById('groupIconCaption').innerHTML = '<b>(New Icon)</b>';
				} else
					alert(results.replace('[Error] ', ''));
			});
	  }
	  function uploadGroupImages()
	  {
	    if (document.getElementById('confirm-upload').disabled)
		  return;
		document.getElementById('confirm-upload').disabled = true;
		document.getElementById('cancel-upload').disabled = true;
		var ajaxCall = web_path + '/components/upload-group-images.php?identity=" . $this->_groupIdentity .
			"&original=' + document.getElementById('group-image-original').value +
			'&group_image=' + document.getElementById('group-image').value + '&group_icon=' + document.getElementById('group-icon').value;
		executeAJAX(ajaxCall, function finish(results)
		{
		  if (results === 'success')
		  {
		    window.location = '" . MANAGE_WEB_PATH . "/group/manage/" . $this->_groupIdentity . "/';
		    return;
		  }
		  alert(results.replace('[Error] ', ''));
		  document.getElementById('confirm-upload').disabled = false;
		  document.getElementById('cancel-upload').disabled = false;
		});
	  }";
	}
	function getPageContents()
	{	  
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <a class=\"returnLink\" href=\"" . MANAGE_WEB_PATH . "/group/list/\">&laquo; Return</a>\n";
	  echo "  [Group] '" . $this->_title . "'\n";
	  echo "</div>\n";
	  
	  if ($this->_editSuccess != null)
	  {
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"edit-success\"><b>Edit success.</b> The ";
		switch ($this->_editSuccess)
		{
		  case "info": echo "information"; break;
		  case "text": echo "text"; break;
		}
		echo " for this group has been changed successfully.</div>\n";
	  }
	  
	  echo "<div class=\"indentedContent\">\n";
	  if ($this->_imageFile != null || $this->_iconFile != null)
	    echo "  <img src=\"" . WEB_PATH . "/images/groups/" . ($this->_iconFile != null ? $this->_iconFile : $this->_imageFile) . "\" class=\"manageProfileIcon " .
	  	  "separator\" id=\"manageProfileGroupIcon\" />\n";
	  echo "  <div class=\"infoLine\"><b>Title:</b> " . $this->_title . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Identity:</b> " . $this->_groupIdentity . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Type:</b> <a href=\"" . MANAGE_WEB_PATH . "/group/list/" . $this->_type["HANDLE"] . "/\" target=\"_blank\">" .
		$this->_type["SINGULAR_NAME"] . "</a></div>\n";
	  echo "  <div class=\"infoLine\"><b>Link:</b> <a href=\"" . WEB_PATH . "/group/" .
	  	$this->_handle . "/\" target=\"_blank\">" . WEB_PATH . "/group/" . $this->_handle .
	  	"/</a></div>\n";
	  echo "  <div class=\"infoLine\"><b>Information Last Updated:</b> " . date(LONG_DATETIME_FORMAT, $this->_groupLastUpdated) . "</div>\n";
	  
	  echo "  <div class=\"infoLine separator\"><b>Status:</b> " . ($this->_active ? "Active" : "Inactive") . "</div>\n";
	  if ($this->_active)
	    echo "  <div class=\"infoLine\"><b>Years active:</b> " . $this->_yearsActive . "</div>\n";
	  
	  echo "  <div class=\"infoLine separator\"><b>Mission statement:</b> " . ($this->_missionStatement != null ? $this->_missionStatement : "(None provided)") . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Description:</b> " . ($this->_description != null ? $this->_description : "(None)") . "</div>\n";
	  
	  echo "</div>\n";
	  echo "<div style=\"clear:both;\"></div>\n";
	  echo "<div class=\"editLink separator\"><span class=\"fauxLink firstAction\" onClick=\"ManageWindow('edit-group-information','" . 
		$this->_groupIdentity . "');\"><img src=\"" . MANAGE_WEB_PATH .
	  	"/interface/images/action_go.gif\" class=\"actionIcon\" /> Edit Information</span> \n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('edit-group-images', '" . $this->_groupIdentity . "');\"><img src=\"" .
		MANAGE_WEB_PATH . "/interface/images/image_new.gif\" class=\"actionIcon\" border=\"0\" /> Edit Images</span> \n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('delete-content', 'group-" . $this->_groupIdentity .
	  	"');\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/folder_delete.gif\" class=\"actionIcon\" border=\"0\" /> Delete</span>\n";
	  echo "</div>\n";
	  echo "<div class=\"dividingBar miniSeparator\"></div>\n";
	  echo "<div class=\"indentedContent\">\n";
	  echo "  <div class=\"infoLine separator\"><b>" . $this->_type["ADMINISTRATOR_TITLE_" . (sizeof($this->_advisors) != 1 ? "PLURAL" : "SINGULAR")] . ":</b> \n";
	  if (sizeof($this->_advisors) > 0)
	  {
	    foreach ($this->_advisors as $index => $advisor)
		  echo ($advisor["GENDER"] == "MALE" ? "Mr." : "Ms.") . " " . $advisor["FIRST_NAME"] . " " . $advisor["LAST_NAME"] .
			($advisor["POSITION"] != null ? " <i>(" . $advisor["POSITION"] . ")</i>" : "") . ($index < sizeof($this->_advisors) - 1 ? ", " : "");
	  } else
	    echo "  (None)\n";
	  echo "</div>\n";
	  echo "</div>\n";
	  
	  echo "<div style=\"clear:both;\"></div>\n";
	  echo "<div class=\"editLink separator\"><span class=\"fauxLink firstAction\" onClick=\"ManageWindow('group-roster','" . 
		$this->_groupIdentity . "');\"><img src=\"" . MANAGE_WEB_PATH .
	  	"/interface/images/group_edit.png\" class=\"actionIcon\" /> Edit Roster</span> \n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('empty-group-roster', '" . $this->_groupIdentity . "');\"><img src=\"" .
		MANAGE_WEB_PATH . "/interface/images/group_delete.png\" class=\"actionIcon\" border=\"0\" /> Clear Roster</span> \n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('group-administrators', '" . $this->_groupIdentity .
	  	"');\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/user_suit.png\" class=\"actionIcon\" border=\"0\" /> Edit " .
		$this->_type["ADMINISTRATOR_TITLE_PLURAL"] . "</span>\n";
	  echo "</div>\n";
	  
	  echo "<div class=\"dividingBar separator\"></div>\n";
	  echo "<div class=\"indentedContent\">\n";
	  echo "  <div class=\"infoLine separator\"><b>Subgroups:</b> " . (sizeof($this->_subgroups) > 0 ? sizeof($this->_subgroups) . 
		" subgroup" . (sizeof($this->_subgroups) != 1 ? "s" : "") : "(None)") . "</div>\n";
	  foreach ($this->_subgroups as $subgroup)
	  {
	    echo "  <div class=\"subgroupContainer\" id=\"subgroup-" . $subgroup["IDENTITY"] . "\">\n";
		echo "    <img src=\"" . WEB_PATH . "/images/groups/" . $subgroup["IMAGE"] . "\" class=\"subgroupImage\" />\n";
		echo "    <div class=\"title\">" . $subgroup["NAME"] . "</div>\n";
		echo "    <div class=\"link\"><span class=\"fauxLink\" onClick=\"ManageWindow('subgroup-edit','" . $subgroup["IDENTITY"] . "');\"><img src=\"" . 
			MANAGE_WEB_PATH . "/interface/images/action_go.gif\" class=\"icon\" /> Edit Information</span></div>\n";
		echo "    <div class=\"link\"><span class=\"fauxLink\" onClick=\"ManageWindow('group-roster','" .
			$this->_groupIdentity . "-" . $subgroup["IDENTITY"] . "');\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/group_edit.png\" class=\"icon\" />" .
			" Edit Roster</span></div>\n";
		echo "    <div class=\"link\"><span class=\"fauxLink\" onClick=\"ManageWindow('empty-group-roster','" .
			$this->_groupIdentity . "-" . $subgroup["IDENTITY"] . "');\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/group_delete.png\" class=\"icon\" />" .
			" Clear Roster</span></div>\n";
		echo "    <div class=\"link\"><span class=\"fauxLink\" onClick=\"ManageWindow('delete-content','subgroup-" . $subgroup["IDENTITY"] . "');\"><img src=\"" .
			MANAGE_WEB_PATH . "/interface/images/folder_delete.gif\" class=\"icon\" /> Delete Subgroup</span></div>\n";
		echo "    <div style=\"clear:both;\"></div>\n";
		echo "  </div>\n";
	  }
	  echo "  <div class=\"editLink separator\"><span class=\"fauxLink firstAction\" onClick=\"ManageWindow('create-subgroup','" . $this->_groupIdentity . "');\"><img src=\"" .
		MANAGE_WEB_PATH . "/interface/images/group_add.png\" class=\"actionIcon\" /> Create new subgroup</span></div>\n";
	  echo "</div>\n";
	}
}
?>