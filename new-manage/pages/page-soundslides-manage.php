<?php
class GoavesManageSoundslidesManage {
	private $_slideshowIdentity;
	private $_title;
	private $_handle;
	private $_published;
	private $_postDate;
	private $_relatedGroup;
	private $_soundFile;
	private $_thumbnailImage;
	private $_dateLastUpdated;
	private $_topic;
	
	private $_isFeatured = false;
	private $_featureDate = null;
	
	private $_slides = array();	
	private $_canPublish;
		  
	private $_publishToggled = null;
	private $_successWithCreation = null;
	private $_featuringSuccess = null;
	private $_unfeaturingSuccess = null;
	private $_slideCreated = null;
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM sound_slides WHERE slideshow_identity='" .
		$database->escapeString($path[2]) . "' AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
	  {
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-soundslides-manage", "LOADED" => false, "IDENTITY" => $path[2]);
		return new GoavesManageSoundslideList();
	  }
	  if (isset($path[3]) && strtolower($path[3]) == "manage-slides")
	    return new GoavesManageSoundslideManageSlides();
		
	  $info = $database->querySingle("SELECT sound_slides.slideshow_identity, sound_slides.handle, sound_slides.title, sound_slides.published, " .
		"sound_slides.post_date, sound_slides.related_group, groups.handle AS \"related_group_handle\", groups.title AS \"related_group_title\", " .
		"sound_slides.sound_file, sound_slides.thumbnail_image, sound_slides.date_last_updated, sound_slides.topic, " .
		"topics.handle AS \"topic_handle\", topics.title AS \"topic_title\" FROM sound_slides LEFT JOIN groups ON sound_slides.related_group = " .
		"groups.group_identity LEFT JOIN topics ON sound_slides.topic = topics.topic_identity WHERE sound_slides.slideshow_identity='" .
		$database->escapeString($path[2]) . "' LIMIT 1", true);
	  $this->_slideshowIdentity = $info["slideshow_identity"];
	  $this->_title = format_content($info["title"]);
	  $this->_handle = $info["handle"];
	  $this->_dateLastUpdated = strtotime($info["date_last_updated"]);
	  $this->_published = ($info["published"] == "TRUE");
	  $this->_relatedGroup = ($info["related_group"] != "" ? array("IDENTITY" => $info["related_group"],
	  	"HANDLE" => $info["related_group_handle"], "TITLE" => format_content($info["related_group_title"])) : null);
	  $this->_postDate = strtotime($info["post_date"]);
	  $this->_topic = ($info["topic"] != "" ? array("IDENTITY" => $info["topic"], "HANDLE" => $info["topic_handle"],
	  	"TITLE" => format_content($info["topic_title"])) : null);
	  $this->_thumbnailImage = ($info["thumbnail_image"] != "" ? $info["thumbnail_image"] : null);
	  $this->_soundFile = $info["sound_file"];
	  
	  $feature_info = $database->querySingle("SELECT date_featured FROM features WHERE feature_type='soundslides' " .
		"AND item_identity='" . $this->_slideshowIdentity . "' LIMIT 1");
	  $this->_isFeature = ($feature_info != null && $feature_info != false);
	  $this->_featureDate = strtotime($feature_info);
	  
	  $get_slides = $database->query("SELECT slide_identity, icon_file, start_time, caption FROM sound_slides_slides WHERE slideshow_identity='" .
		$this->_slideshowIdentity . "' ORDER BY start_time ASC");
	  while ($slide = $get_slides->fetchArray())
	    $this->_slides[] = array("IDENTITY" => $slide["slide_identity"], "ICON" => $slide["icon_file"], "START_TIME" => $slide["start_time"],
			"CAPTION" => ($slide["caption"] != "" ? format_content($slide["caption"]) : null));
	  
	  $this->_canPublish = (sizeof($this->_slides) > 0);
	  
	  if (!isset($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$this->_slideshowIdentity]))
	    $_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$this->_slideshowIdentity] = array();
		
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]))
	  {
	    switch ($_SESSION[MANAGE_TRANSFER_DATA]["FROM"])
		{
		  case "page-soundslides-create": $this->_successWithCreation = true; break;
		  case "toggle-publish.php": $this->_publishToggled = array("TOGGLED" => true, "TO_VALUE" => $_SESSION[MANAGE_TRANSFER_DATA]["NEW_VALUE"]); break;
		  case "set-feature-component.php": $this->_featuringSuccess = true; break;
		  case "remove-feature.php": $this->_unfeaturingSuccess = true; break;
		  case "upload-soundslide-slide.php": $this->_slideCreated = true; break;
		}
		unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  
	  return true;
	}
	function getRequirePermission() { return "6"; }
	function getPageHandle() { return "soundslide"; }
	function getPageSubhandle() { return "manage"; }
	function getPageTitle() { return "[Soundslide Presentation] '" . $this->_title . "'"; }
	function getBreadTrail() { return array("soundslide" => "Soundslides", "soundslide/list" => "My Soundslides",
		"[this]" => "'" . $this->_title . "'"); }
	function getPageStylesheet() { return "stylesheet-soundslides.css"; }
	function getBodyOnload() { return ($this->_successWithCreation ? "fadeOutAndHide('soundslide-created-success', 3000);" : null) .
		($this->_publishToggled ? "fadeOutAndHide('publish-toggle-success', 3000);" : null) .
		($this->_featuringSuccess ? "fadeOutAndHide('feature-success', 3000);" : null) . ($this->_unfeaturingSuccess ? "fadeOutAndHide('unfeature-success', 3000);" :
		null) . ($this->_slideCreated ? "fadeOutAndHide('slide-created-success', 3000);" : null); }
	function getPageJavascript()
	{
	  return "function togglePublish(slideshow_identity)
	  {
	    executeAJAX(web_path + '/components/toggle-publish.php?type=soundslide&identity=' + slideshow_identity, function results(value)
		{
		  if (value == 'success')
		  {
		    window.location = web_path + '/soundslide/manage/' + slideshow_identity + '/';
			return;
		  }
		  alert(value);
		});
	  }
	  function togglePictureManageLinks(to_value)
	  {
	    document.getElementById('picture-action-edit').className = (to_value ? '' : 'actionHidden');
		document.getElementById('picture-action-delete').className = 'fauxLink' + (to_value ? '' : ' actionHidden');
	  }
	  function setupSlideIconCreation(filename, real_x, real_y)
	  {
	    alert(real_x);
	    var slideImageCrop = document.createElement('img');
		slideImageCrop.setAttribute('id', 'slide-image-crop');
		slideImageCrop.className = 'slideCropBox';
		document.getElementById('slide-image-crop-container').appendChild(slideImageCrop);
		document.getElementById('image-input').style.display = 'none';
		document.getElementById('slide-display-image').src = '" . MANAGE_WEB_PATH . "/limbo/' + filename;
		document.getElementById('slide-display-image').style.display = 'block';
		document.getElementById('slide-image-crop').style.display = 'block';
		document.getElementById('slide-image-crop').src = '" . MANAGE_WEB_PATH . "/limbo/' + filename;
		document.getElementById('slide-image-ratio').value = (real_x > 300 ? real_x / 300 : 300 / real_x);
		document.getElementById('slide-image-real-x').value = real_x;
		jQuery(function(){
			jQuery('#slide-image-crop').Jcrop({
				aspectRatio: 1,
				bgOpacity: 0.4,
				onSelect: updateSlideIconCoords,
				onChange: updateSlideIconCoords
			});
		});
	  }
	  function updateSlideIconCoords(cropbox)
	  {
		var image_ratio = document.getElementById('slide-image-ratio').value;
		var real_x = document.getElementById('slide-image-real-x').value;
		document.getElementById('slide-crop-x').value = cropbox.x * (real_x > 300 ? image_ratio : 1);
		document.getElementById('slide-crop-y').value = cropbox.y * (real_x > 300 ? image_ratio : 1);
		document.getElementById('slide-crop-width').value = cropbox.w * (real_x > 300 ? image_ratio : 1);
		document.getElementById('slide-crop-height').value = cropbox.h * (real_x > 300 ? image_ratio : 1);
		document.getElementById('slide-crop').disabled = (document.getElementById('slide-crop-width').value <= 0 ||
			document.getElementById('slide-crop-height').value <= 0);
	  }
	  function cropSlideIcon()
	  {
	    if (document.getElementById('slide-crop').disabled || document.getElementById('slide-crop-width').value <= 0 ||
			document.getElementById('slide-crop-height').value <= 0)
			{
			  alert ('You are not able to crop your icon yet!');
			  return;
			}
		document.getElementById('slide-crop').disabled = true;
		var cropCall = web_path + '/components/crop-slide-icon.php?image=' + document.getElementById('image-filepath').value +
			'&x=' + document.getElementById('slide-crop-x').value + '&y=' + document.getElementById('slide-crop-y').value +
			'&width=' + document.getElementById('slide-crop-width').value + '&height=' +
			document.getElementById('slide-crop-height').value;
		executeAJAX(cropCall, function process(result)
		{
		  if (result.indexOf('[Error] ') >= 0)
		  {
		    alert(result.replace('[Error] ', ''));
			document.getElementById('slide-crop').disabled = (document.getElementById('slide-crop-width').value <= 0 ||
				document.getElementById('slide-crop-height').value <= 0);
			processUploadFormComplete();
			return;
		  } else
		  {
		    document.getElementById('slide-icon').value = result;
			document.getElementById('slide-upload-icon').style.display = 'none';
			processUploadFormComplete();
		  }
		});
	  }
	  function processUploadFormComplete()
	  {
	    if (document.getElementById('slide-icon').value == null || document.getElementById('slide-icon').value.length == 0)
		{
		  document.getElementById('slide-add').disabled = true;
		  return;
		}
		document.getElementById('slide-add').disabled = false;
	  }
	  function uploadSlide()
	  {
	    var uploadCall = web_path + '/components/upload-soundslide-slide.php?identity=" . $this->_slideshowIdentity .
			"&image=' + document.getElementById('image-filepath').value +
			'&icon=' + document.getElementById('slide-icon').value + '&x=' + document.getElementById('slide-crop-x').value + '&y=' +
			document.getElementById('slide-crop-y').value + '&width=' + document.getElementById('slide-crop-width').value + '&height=' +
			document.getElementById('slide-crop-height').value + '&minutes=' + document.getElementById('slide-upload-minutes').value +
			'&seconds=' + document.getElementById('slide-upload-seconds').value;
		document.getElementById('slide-add').disabled = true;
		executeAJAX(uploadCall, function uploaded(result)
		{
		  if (result == 'success')
		  {
		    window.location = web_path + '/soundslide/manage/" . $this->_slideshowIdentity . "/';
			return;
		  } else
		  {
		    alert(result);
			processUploadFormComplete();
			return;
		  }
		});
	  }
	  function toggleSlideManageLinks(to_value)
	  {
	    document.getElementById('slide-action-edit').className = (to_value ? '' : 'actionHidden');
		document.getElementById('slide-action-delete').className = 'fauxLink' + (to_value ? '' : ' actionHidden');
	  }";
	}
	function getPageContents()
	{ 
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <div class=\"explainButton\" onClick=\"ManageWindow('explain','manage-soundslide');\"></div>\n";
	  echo "  <a class=\"returnLink\" href=\"" . MANAGE_WEB_PATH . "/soundslide/list/\">&laquo; Return</a>\n";
	  echo "  Manage Soundslide Presentation '" . $this->_title . "'\n";
	  echo "</div>\n";
	  
	  if ($this->_successWithCreation != null)
		  echo "<div class=\"contentSuccess separatorBottom\" id=\"soundslide-created-success\"><b>Creation Successful.</b>" .
			"The soundslide presentation has been successfully created.</div>\n";
	  if ($this->_publishToggled != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"publish-toggle-success\"><b>" . ($this->_publishToggled["TO_VALUE"] == "TRUE" ?
			"Publish" : "Unpublish") . " success.</b> This soundslide presentation has been " . ($this->_publishToggled["TO_VALUE"] == "TRUE" ? "published" : "unpublished") .
			".</div>\n";
	  if ($this->_featuringSuccess != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"feature-success\"><b>Soundslide presentation featured.</b> This soundslide " .
			"presentation is now a featured piece on the homepage.</div>\n";
	  if ($this->_unfeaturingSuccess != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"unfeature-success\"><b>Featured status removed.</b> The featured status on " .
			"this soundslide presentation has been removed.</div>\n";
	  if ($this->_slideCreated != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"slide-created-success\"><b>Slide created.</b> The slide has been uploaded successfully.</div>\n";
			
	  echo "<div class=\"indentedContent\">\n";
	  echo "  <img src=\"" . WEB_PATH . "/images/sound_slides/" . $this->_thumbnailImage . "\" class=\"thumbnailImage " .
	  	"separator\" />\n";
	  echo "  <div class=\"infoLine\"><b>Title:</b> " . $this->_title . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Link:</b> " . ($this->_published ? "<a href=\"" . WEB_PATH . "/sound-slides/" .
	  	$this->_handle . "/\" target=\"_blank\">" . WEB_PATH . "/sound-slides/" . $this->_handle .
	  	"/</a> (Published)" : "<span class=\"unpublished\">Unpublished</span>") . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Date Posted:</b> " . date(LONG_DATETIME_FORMAT, $this->_postDate) . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Date Last Updated:</b> " . date(LONG_DATETIME_FORMAT, $this->_dateLastUpdated) . "</div>\n";
	  if ($this->_isFeature)
	    echo "  <div class=\"infoLine separator\"><b>Featured:</b> " . date(DATE_FORMAT, $this->_featureDate) . "</div>\n";
	  
	  echo "  <div class=\"infoLine separator\"><b>Related Group:</b> " . ($this->_relatedGroup != null ? "<a href=\"" .
	  	WEB_PATH . "/group/" . $this->_relatedGroup["HANDLE"] . "/\" target=\"_blank\">" . $this->_relatedGroup["TITLE"] .
	  	"</a>" : "None") . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Topic:</b> " . ($this->_topic != null ? "<a href=\"" . WEB_PATH . "/topic/" .
	  	$this->_topic["HANDLE"] . "/\" target=\"_blank\">" . $this->_topic["TITLE"] . "</a>" : "None") . "</div>\n";
	  echo "</div>\n";
	  echo "<div style=\"clear:both;\"></div>\n";
	  echo "<div class=\"editLink separator\"><span href=\"" . MANAGE_WEB_PATH .
	  	"/photo-gallery/edit-info/" . $this->_slideshowIdentity . "/\" class=\"disabledAction firstAction\"><img src=\"" . MANAGE_WEB_PATH .
	  	"/interface/images/action_go.gif\" class=\"actionIcon\" border=\"0\" /> Edit Information (soon)</span> \n";
	  if ($this->_canPublish)
	  {
	    echo "<span class=\"fauxLink\" onClick=\"togglePublish('" . $this->_slideshowIdentity . "');\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/flag_" .
			($this->_published ?
	    	"red" : "green") . ".gif\" class=\"actionIcon\" border=\"0\" /> " . ($this->_published ? "Unpublish" : "Publish") . "</span>";
	  } else
	    echo "<span class=\"disabledAction\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/flag_white.gif\" class=\"" .
	    	"actionIcon\" /> Cannot publish yet</span>";
	  echo " \n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('feature-content', 'soundslides-" . $this->_slideshowIdentity . "');\"><img src=\"" . MANAGE_WEB_PATH .
		"/interface/images/" . ($this->_isFeature ? "feature_remove.png" : "feature_add.png") . "\" class=\"" .
		"actionIcon\" border=\"0\" /> " . ($this->_isFeature ? "Remove " : "") . "Feature</span>\n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('delete-content', 'soundslide-" . $this->_slideshowIdentity .
	  	"');\"><img src=\"" . MANAGE_WEB_PATH .
	  	"/interface/images/folder_delete.gif\" class=\"actionIcon\" border=\"0\" /> Delete</span>\n";
	  echo "</div>\n";
	  echo "<div class=\"dividingBar miniSeparator\"></div>\n";
	  if (sizeof($this->_slides) > 0)
	  {
		echo "<div class=\"indentedContent infoLine\"><b>Number of slides:</b> " . sizeof($this->_slides) . "</div>\n";
	    echo "<div class=\"indentedContent contentText separator separatorBottom\">Select the slide you wish to edit below, then click " .
			"the edit button.</div>\n";
	    echo "<div class=\"fauxTable fauxSlidesTable\">\n";
		foreach ($this->_slides as $slide)
		{
		  echo "<div class=\"cell " . (isset($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$this->_slideshowIdentity]) &&
	      		in_array($slide["IDENTITY"], $_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$this->_slideshowIdentity]) ? "toggleOn" : "toggleOff") . 
			"\" onClick=\"executeAJAX(web_path + '/components/select-soundslide-slide-manage.php?identity=" .
			$slide["IDENTITY"] . "', function processToggle(value)
			{
			  if (value.indexOf('[Error]') >= 0)
			  {
			    alert(value.replace('[Error]',''));
				return;
			  }
			  var toggleResults = value.split('-');
			  document.getElementById('manageSlide" . $slide["IDENTITY"] . "').className = (toggleResults[0] == 'checked' ? 'cell toggleOn' : 'cell toggleOff');
			  toggleSlideManageLinks(toggleResults[1] > 0);
			});\"id=\"manageSlide" . $slide["IDENTITY"] . "\"><img src=\"" . WEB_PATH . "/images/sound_slides/" . $slide["ICON"] .
	      	"\" class=\"icon\" /><div class=\"toggledStatusDisplay\"></div>";
		  $start_time = $slide["START_TIME"] / 1000;
		  $minutes = floor($start_time / 60);
		  $seconds = $start_time - $minutes * 60;
		  echo ($minutes < 10 ? "0" : "") . $minutes . ":" . ($seconds < 10 ? "0" : "") . $seconds;
		  echo "</div>\n";
		}
/*	    $this->_slides[] = array("IDENTITY" => $slide["slide_identity"], "IMAGE" => $slide["image_file"], "START_TIME" => $slide["start_time"],
			"CAPTION" => ($slide["caption"] != "" ? format_content($slide["caption"]) : null));*/
	  } else
	    echo "<center><div class=\"contentText separator\">This slideshow presentation has no slides yet.</div></center>\n";
	  
		echo "<div class=\"editLink separator\">\n";
		echo "<span class=\"fauxLink firstAction\" onClick=\"ManageWindow('upload-soundslide-slide', '" . $this->_slideshowIdentity .
			"');\" id=\"slide-action-add\"><img src=\"" .
			MANAGE_WEB_PATH . "/interface/images/icon-slide-add.png\" class=\"actionIcon\" border\"0\" /> Add slide</span>\n";
		echo "<a href=\"" . MANAGE_WEB_PATH . "/soundslide/manage/" . $this->_slideshowIdentity . "/manage-slides/\" " .
			(sizeof($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$this->_slideshowIdentity]) == 0 ? "class=\"actionHidden\" " : "") .
			"id=\"slide-action-edit\"><img src=\"" . MANAGE_WEB_PATH .
			"/interface/images/action_go.gif\" class=\"actionIcon\" border=\"0\" /> Edit selected slides</a> \n";
		echo "<span class=\"fauxLink" . (sizeof($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$this->_slideshowIdentity]) == 0 ?
			" actionHidden" : "") . "\" onClick=\"ManageWindow('delete-content', 'soundslide_slides-" . $this->_slideshowIdentity .
			"');\" id=\"slide-action-delete\"><img src=\"" .
			MANAGE_WEB_PATH . "/interface/images/folder_delete.gif\" class=\"actionIcon\" border\"0\" /> Delete " .
			"selected slides</span></div>\n";
		echo "</div>\n";
	}
}
?>