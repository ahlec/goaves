<?php
class GoavesManageAdminBlogList {
	private $_groups = array();
	function checkOrRedirect($path, $database)
	{
	  $all_groups = $database->query("SELECT group_identity, title, icon_file, group_types.type_identity, group_types.plural_name, group_types.handle, active, group_last_updated " .
		"FROM groups JOIN group_types ON groups.type = group_types.type_identity ORDER BY groups.type, title ASC");
	  while ($group = $all_groups->fetchArray())
	  {
	    $this->_groups[] = array("IDENTITY" => $group["group_identity"], "TITLE" => format_content($group["title"]), "ICON" => $group["icon_file"],
			"ACTIVE" => ($group["active"] == "TRUE"), "LAST_UPDATED" => strtotime($group["group_last_updated"]), "TYPE" => $group["type_identity"],
			"TYPE_TITLE" => format_content($group["plural_name"]), "TYPE_HANDLE" => $group["handle"]);
	  }
	  return true;
	}
	function getRequirePermission() { return "7"; }
	function getPageHandle() { return "group"; }
	function getPageSubhandle() { return "list"; }
	function getPageTitle() { return "Group Listing"; }
	function getBreadTrail() { return array("[this]" => "Group Listing"); }
	function getPageStylesheet() { return "stylesheet-groups.css"; }
	function getBodyOnload()
	{
	  return null;
	}
	function getPageContents()
	{
	  if (sizeof($this->_groups) == 0)
	  {
	    echo "<div class=\"contentText\">There are no groups in the database.</div>\n";
		return;
	  }
	  echo "<div class=\"contentText\">Select a group from below.</div>\n";
	  echo "<div class=\"groupListingContainer\">\n";
	  $last_group_type = null;
	  foreach ($this->_groups as $group)
	  {
	    if ($last_group_type == null || $last_group_type != $group["TYPE"])
		{
		  if ($last_group_type != null)
		    echo "  </div>\n";
		  echo "  <div class=\"groupContainer" . ($last_group_type != null ? " separator" : "") . "\">\n";
		  echo "    <div class=\"groupType\"><a href=\"" . MANAGE_WEB_PATH . "/group/" . $group["TYPE_HANDLE"] . "/\">" . $group["TYPE_TITLE"] . "</a></div>\n";
		  $last_group_type = $group["TYPE"];
		}
		echo "  <div class=\"item\">\n";
		echo "    " . (!$group["ACTIVE"] ? "(Inactive) " : "") . "<a href=\"" . MANAGE_WEB_PATH . "/group/manage/" . $group["IDENTITY"] . "/\" class=\"item\">" . $group["TITLE"] . "";
		echo "</a>\n";
	  }
	  echo "  </div>\n";
	  echo "</div>\n";
	}
}
?>