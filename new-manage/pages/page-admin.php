<?php
class GoAvesManageAdmin {
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[1]))
		return true;
	  switch ($path[1])
	  {
	    case "staff": return new GoavesManageAdminStaff();
		case "blog": return new GoavesManageAdminBlog();
//	    case "change-permissions": return new GoavesManageStaffChangePermissions();
//		case "staff-listing": return new GoavesManageStaffListing();
	  }
	  return true;
	}
	function getRequirePermission() { return PERM_ADMIN; }
	function getPageHandle() { return "admin"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "[Admin]"; }
	function getBreadTrail() { return array("[this]" => "[Admin Level]"); }
	function getPageStylesheet() { return "stylesheet-admin.css"; }
	function getPageContents()
	{
	  echo "<a href=\"" . MANAGE_WEB_PATH . "/admin/staff/\">Staff</a>\n";
	}
}
?>