<?php
class GoavesManageStaffStatistics {
	private $_totalBeatCount = 0;
	private $_publishedBeatCount = 0;
	private $_beatsByYear = array();
	function checkOrRedirect($path, $database)
	{
	  $get_beats = $database->query("SELECT post_time, published FROM beats WHERE staff_identity='" . 
	  	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'");
	  while ($beat = $get_beats->fetchArray())
	  {
	    $this->_totalBeatCount++;
	    if ($beat["published"] == "TRUE")
	      $this->_publishedBeatCount++;
	    $beat_school_year = date("Y", strtotime($beat["post_time"]));
	    if (date("m", strtotime($beat["post_time"])) < 6)
	      $beat_school_year -= 1;
	    if (!isset($this->_beatsByYear[$beat_school_year]))
	      $this->_beatsByYear[$beat_school_year] = 0;
	    $this->_beatsByYear[$beat_school_year]++;
	  }
	  return true;
	}
	function getPageHandle() { return null; }
	function getPageSubhandle() { return "view-statistics"; }
	function getPageTitle() { return "View Statistics"; }
	function getBreadTrail() { return array("staff" => "My Account", "[this]" => "View Statistics"); }
	function getPageStylesheet() { return "stylesheet-staff.css"; }
	function getPageContents()
	{	    	
	  echo "<div class=\"contentHeader\">\n";
	  echo "  <div class=\"explainButton\" onClick=\"ManageWindow('explain','staff-statistics');\"></div>\n";
	  echo "  Staff Statistics\n";
	  echo "</div>\n";
	  
	  echo "<div class=\"contentSubheader separator\">Beats</div>\n";
	  echo "<div class=\"indentedContent\">\n";
	  echo "  <b>Total Beats:</b> " . $this->_totalBeatCount . "<br />\n";
	  echo "  <b>Beats Published:</b> " . ($this->_totalBeatCount > 0 ? 
	  	floor(($this->_publishedBeatCount / $this->_totalBeatCount) * 100) : "0") . "% (" .
	  	$this->_publishedBeatCount . " of " . $this->_totalBeatCount . ")<br />\n";
	  echo "  <b>Beats by school year:</b><br />\n";
	  foreach ($this->_beatsByYear as $year => $count)
	    echo "  <div style=\"margin-left:10px;\"><b>" . $year . "-" . ($year + 1) . ":</b> " . $count . "</div>\n";
	  echo "</div>\n";
	}
}
?>