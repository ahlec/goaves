<?php
class GoAvesManageAdminBlog {
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]))
		return true;
	  switch ($path[2])
	  {
	    case "post": return new GoavesManageAdminBlogPost();
		case "list": return new GoavesManageAdminBlogList();
	  }
	  return true;
	}
	function getRequirePermission() { return PERM_ADMIN; }
	function getPageHandle() { return "admin"; }
	function getPageSubhandle() { return "blog"; }
	function getPageTitle() { return "[Admin] Management Blog"; }
	function getBreadTrail() { return array("admin" => "[Admin Level]", "[this]" => "Management Blog"); }
	function getPageStylesheet() { return "stylesheet-admin.css"; }
	function getPageContents()
	{
	  echo "<a href=\"" . MANAGE_WEB_PATH . "/admin/blog/list/\">Blog Post Listing</a><br />\n";
	  echo "<a href=\"" . MANAGE_WEB_PATH . "/admin/blog/post/\">New Blog Post</a>\n";
	}
}
?>