<?php
class GoavesManageSoundslideList {
	private $_soundslides = array();
	
	private $_deletedPresentation = null;
	function checkOrRedirect($path, $database)
	{		
	  $all_soundslides = $database->query("SELECT slideshow_identity, title, post_date, date_last_updated, published, thumbnail_image FROM sound_slides " .
		"WHERE staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' ORDER BY post_date DESC");
	  while ($soundslide = $all_soundslides->fetchArray())
	  {
	    $date_created = strtotime($soundslide["post_date"]);
		$school_year_start_posted = date("Y", $date_created);
		if (date("m", $date_created) < 6)
		  $school_year_start_posted -= 1;
	    $this->_soundslides[] = array("IDENTITY" => $soundslide["slideshow_identity"], "TITLE" => format_content($soundslide["title"]),
			"DATE_POSTED" => $date_created, "PUBLISHED" => ($soundslide["published"] == "TRUE"),
			"SCHOOL_YEAR" => $school_year_start_posted . " - " . ($school_year_start_posted + 1),
			"DATE_LAST_UPDATED" => strtotime($soundslide["date_last_updated"]), "ICON_FILE" => $soundslide["thumbnail_image"]);
	  }
	  
	  
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]) && $_SESSION[MANAGE_TRANSFER_DATA]["FROM"] == "delete-content.php")
	  {
	    $this->_deletedPresentation = $_SESSION[MANAGE_TRANSFER_DATA]["TITLE"];
	    unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  
	  return true;
	}
	function getRequirePermission() { return "6"; }
	function getPageHandle() { return "soundslide"; }
	function getPageSubhandle() { return "list"; }
	function getPageTitle() { return "My Soundslides"; }
	function getBreadTrail() { return array("soundslide" => "Soundslides", "[this]" => "My Soundslides"); }
	function getPageStylesheet() { return "stylesheet-soundslides.css"; }
	function getBodyOnload()
	{
	  return ($this->_deletedPresentation != null ? "fadeOutAndHide('presentation-deleted-success', '3000');" : null);
	}
	function getPageContents()
	{
	  if ($this->_deletedPresentation != null)
	    echo "<div class=\"contentSuccess separatorBottom\" id=\"presentation-deleted-success\"><b>Deletion successful.</b> Soundslide presentation '" .
	    	$this->_deletedPresentation . "' has been deleted.</div>\n";
			
	  echo "<div class=\"contentHeader\">My Soundslides</div>\n";
	  if (sizeof($this->_soundslides) == 0)
	  {
	    echo "<div class=\"contentText\">You have no soundslides</div>\n";
		echo "<div class=\"contentText miniSeparator\"><a href=\"" . MANAGE_WEB_PATH .
			"/soundslide/create/\">Create a new soundslide presentation</a></div>\n";
		return;
	  }
	  echo "<div class=\"contentText\">Select a soundslide from below to manage.</div>\n";
	  echo "<div class=\"listingContainer\">\n";
	  $last_school_year = null;
	  foreach ($this->_soundslides as $soundslide)
	  {
	    if ($last_school_year == null || $last_school_year != $soundslide["SCHOOL_YEAR"])
		{
		  if ($last_school_year != null)
		    echo "  </div>\n";
		  echo "  <div class=\"yearContainer" . ($last_school_year != null ? " separator" : "") . "\">\n";
		  echo "    <div class=\"yearHeader\">" . $soundslide["SCHOOL_YEAR"] . "</div>\n";
		  $last_school_year = $soundslide["SCHOOL_YEAR"];
		}
		echo "  <a href=\"" . MANAGE_WEB_PATH . "/soundslide/manage/" . $soundslide["IDENTITY"] . "/\" class=\"item\" style=\"background-image:url('" .
			WEB_PATH . "/images/sound_slides/" . $soundslide["ICON_FILE"] . "');\">\n";
		echo "    <div class=\"overlay\"></div>\n";
		echo "    <div class=\"status " . ($soundslide["PUBLISHED"] ? "statusPublished" : "statusUnpublished") . "\"></div>\n";
		echo "    <div class=\"title\">" . $soundslide["TITLE"] . "</div>\n";
		echo "  </a>\n";
	  }
	  echo "  </div>\n";
	  echo "</div>\n";
	}
}
?>