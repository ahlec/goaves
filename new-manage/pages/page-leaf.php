<?php
class GoAvesManageLeaf {
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[1]))
	    $path[1] = "landing";
	  switch ($path[1])
	  {
	    case "compose": return new GoavesManageBeatCompose();
		case "manage": return new GoavesManageLeafManage();
	    default: return new GoavesManageLeafList();
	  }
	  return true;
	}
	function getPageHandle() { return "leaf"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Login"; }
	function getBreadTrail() { return array("[this]" => "Beats"); }
	function getPageStylesheet() { return "stylesheet-beat.css"; }
	function getPageContents()
	{
	  echo "You can't be here. Programming error if you see this message.";
	}
}
?>