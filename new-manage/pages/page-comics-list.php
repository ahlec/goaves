<?php
class GoavesManageComicsList {
	private $_comics = array();
	private $_successWithPost = null;
	function checkOrRedirect($path, $database)
	{
	  $all_comics = $database->query("SELECT identity, handle, image_file, icon_file, date_posted, published, title " .
		"FROM cartoons WHERE artist='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) .
		"' ORDER BY date_posted DESC");
	  while ($comic = $all_comics->fetchArray())
	  {
	    $date_posted = strtotime($comic["date_posted"]);
		$school_year_start_posted = date("Y", $date_posted);
		if (date("m", $date_posted) < 6)
		  $school_year_start_posted -= 1;
	    $this->_comics[] = array("IDENTITY" => $comic["identity"], "HANDLE" => $comic["handle"],
	    	"TITLE" => format_content($comic["title"]), "DATE_POSTED" => $date_posted,
	    	"PUBLISHED" => ($comic["published"] == "TRUE"), "SCHOOL_YEAR" => $school_year_start_posted .
			" - " . ($school_year_start_posted + 1), "IMAGE_FILE" => $comic["image_file"],
			"ICON_FILE" => ($comic["icon_file"] != "" ? $comic["icon_file"] : $comic["image_file"]));
	  }
	  
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]) && $_SESSION[MANAGE_TRANSFER_DATA]["FROM"] == "page-comics-upload" && $_SESSION[MANAGE_TRANSFER_DATA]["SUCCESS"])
	  {
		  $this->_successWithPost = $database->querySingle("SELECT title FROM cartoons WHERE identity='" . $database->escapeString($_SESSION[MANAGE_TRANSFER_DATA]["IDENTITY"]) .
			"' LIMIT 1");
		  unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  
	  return true;
	}
	function getRequirePermission() { return PERM_COMICS; }
	function getPageHandle() { return "comic"; }
	function getPageSubhandle() { return "list"; }
	function getPageTitle() { return "My Comics"; }
	function getBreadTrail() { return array("comic" => "Comics", "[this]" => "My Comics"); }
	function getPageStylesheet() { return "stylesheet-comics.css"; }
	function getPageContents()
	{
	  if ($this->_successWithPost != null)
		  echo "<div class=\"contentSuccess separatorBottom\" id=\"upload-success\" onLoad=\"fadeOutAndHide('upload-success', 3000);\"><b>Upload Successful.</b>" .
			"The comic '" . $this->_successWithPost . "' has been uploaded to Goaves.com.</div>\n";
	  echo "<div class=\"contentHeader\">My Comics</div>\n";
	  if (sizeof($this->_comics) == 0)
	  {
	    echo "<div class=\"contentText\">You have no uploaded comics</div>\n";
		echo "<div class=\"contentText miniSeparator\"><a href=\"" . MANAGE_WEB_PATH .
			"/comic/upload/\">Upload a new comic</a></div>\n";
		return;
	  }
	  echo "<div class=\"contentText\">Select a comic from below.</div>\n";
	  echo "<div class=\"listingContainer\">\n";
	  $last_school_year = null;
	  foreach ($this->_comics as $comic)
	  {
	    if ($last_school_year == null || $last_school_year != $comic["SCHOOL_YEAR"])
		{
		  if ($last_school_year != null)
		    echo "  </div>\n";
		  echo "  <div class=\"yearContainer" . ($last_school_year != null ? " separator" : "") . "\">\n";
		  echo "    <div class=\"yearHeader\">" . $comic["SCHOOL_YEAR"] . "</div>\n";
		  $last_school_year = $comic["SCHOOL_YEAR"];
		}
		echo "  <a href=\"" . MANAGE_WEB_PATH . "/comic/manage/" . $comic["IDENTITY"] . "/\" class=\"item\" style=\"background-image:url('" .
			WEB_PATH . "/images/cartoons/" . $comic["ICON_FILE"] . "');\">\n";
		echo "    <div class=\"overlay\"></div>\n";
		echo "    <div class=\"status " . ($comic["PUBLISHED"] ? "statusPublished" : "statusUnpublished") . "\"></div>\n";
		echo "    <div class=\"title\">" . $comic["TITLE"] . "</div>\n";
		echo "  </a>\n";
	  }
	  echo "  </div>\n";
	  echo "</div>\n";
	}
}
?>