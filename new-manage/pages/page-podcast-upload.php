<?php
class GoavesManagePodcastUpload {
	private $_podcastTitleMinimum = 6;
	private $_relatedGroupTypes = array();
	private $_topics = array();
	function checkOrRedirect($path, $database)
	{
	  if (isset($path[2]) && strpos($path[2], "success-") === 0)
	  {
	    $supposed_success = $database->escapeString(str_replace("success-", "", $path[2]));
		if ($database->querySingle("SELECT count(*) FROM podcasts WHERE podcast_identity='" . $supposed_success . "' AND staff_identity='" .
			$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") > 0)
			{
			  $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-podcast-upload", "SUCCESS" => true, "IDENTITY" => $supposed_success);
			  header("Location: " . MANAGE_WEB_PATH . "/podcast/manage/" . $supposed_success . "/");
			  exit();
			}
	  }
	  $group_types = $database->query("SELECT type_identity, plural_name, handle FROM group_types ORDER BY type_identity ASC");
	  while ($group_type = $group_types->fetchArray())
	  {
	    $get_groups = $database->query("SELECT group_identity, title, active FROM groups WHERE type='" .
			$group_type["type_identity"] . "' ORDER BY title ASC");
		$groups_with_type = array();
		while ($group = $get_groups->fetchArray())
		  $groups_with_type[] = array("IDENTITY" => $group["group_identity"], "TITLE" => format_content($group["title"]),
			"ACTIVE" => ($group["active"] == "TRUE"));
	    $this->_relatedGroupTypes[$group_type["handle"]] = array("NAME" => $group_type["plural_name"],
			"GROUPS" => $groups_with_type);
	  }
	  
	  $topics = $database->query("SELECT topic_identity, title FROM topics WHERE (date_start <= '" .
		date("Y-m-d", strtotime("+1 hours")) . "' AND date_end >= '" . date("Y-m-d", strtotime("+1 hours")) . "') OR (" .
		"date_start IS NULL AND date_end IS NULL) ORDER BY title ASC");
	  while ($topic = $topics->fetchArray())
	    $this->_topics[] = array("IDENTITY" => $topic["topic_identity"], "TITLE" => format_content($topic["title"]));
	  return true;
	}
	function getRequirePermission() { return "3"; }
	function getPageHandle() { return "podcast"; }
	function getPageSubhandle() { return "upload"; }
	function getPageTitle() { return "Upload New Podcast"; }
	function getBreadTrail() { return array("podcast" => "Podcasts", "[this]" => "Upload New Podcast"); }
	function getPageStylesheet() { return null; }
	function getPageOnBeforeUnload() { return "return getWarnPageRefresh();"; }
	function getPageJavascript()
	{
	  $group_handles = "[";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	    $group_handles .= "'" . $handle . "',";
	  $group_handles = rtrim($group_handles, ",") . "]";
	  return "var podcast_created = false;
	  var group_types = " . $group_handles . ";
	  function toggleUseRelatedGroup()
	  {
	    document.getElementById('podcast-related-group-type').selectedIndex = 0;
	    document.getElementById('podcast-related-group-type').style.display =
			(document.getElementById('podcast-related-group-use').value ? 'block' : 'none');
		for (var type_index = 0; type_index < group_types.length; type_index++)
		  document.getElementById('podcast-related-group-' + group_types[type_index]).style.display = 'none';
	  }
	  function toggleUseTopic()
	  {
	    document.getElementById('podcast-topic').selectedIndex = 0;
		document.getElementById('podcast-topic').style.display = (document.getElementById('podcast-topic-use').value ? 'block' : 'none');
	  }
	  function selectGroupByType(type_handle)
	  {
	    for (var type_index = 0; type_index < group_types.length; type_index++)
		  document.getElementById('podcast-related-group-' + group_types[type_index]).style.display = 'none';
	    if (type_handle == '[none]')
		  return;
		document.getElementById('podcast-related-group-' + type_handle).selectedIndex = 0;
	    document.getElementById('podcast-related-group-' + type_handle).style.display = 'block';
	  }
	  function cropIcon()
	  {
	    if (document.getElementById('podcast-crop-width').value == null || document.getElementById('podcast-crop-width').value <= 0 ||
			document.getElementById('podcast-crop-height').value == null || document.getElementById('podcast-crop-height').value <= 0)
			{
			  alert ('You must make a selection on the uploaded image before you may crop the icon.');
			  return;
			}
	    document.getElementById('cropIconButton').disabled = true;
	    var cropCall = web_path + '/components/crop-podcast-icon.php?image=' + document.getElementById('podcast-crop-image-filepath').value + 
			'&x=' + document.getElementById('podcast-crop-x').value +
			'&y=' + document.getElementById('podcast-crop-y').value + '&width=' + document.getElementById('podcast-crop-width').value + 
			'&height=' + document.getElementById('podcast-crop-height').value;
		executeAJAX(cropCall, function process(results)
		{
		  document.getElementById('cropIconButton').disabled = false;
		  if (results.indexOf('[Error] ') < 0)
		  {
		    document.getElementById('icon-crop').style.display = 'none';
		    document.getElementById('icon-filepath').value = results;
			document.getElementById('icon-image-input').className = 'inputFile formComponentValid';
			checkFormValid();
		  } else
		  {
		    alert(results.replace('[Error] ', ''));
		  }
		});
	  }
	  function checkFormValid()
	  {
	    if (document.getElementById('podcast-title').className == 'inputText formComponentValid' && document.getElementById('audio-filepath').value != null &&
			document.getElementById('audio-filepath').value.length > 0 && document.getElementById('icon-filepath').value != null &&
			document.getElementById('icon-filepath').value.length > 0)
		{
		  document.getElementById('podcast-upload').disabled = false;
		  return;
		}
		document.getElementById('podcast-upload').disabled = true;
	  }
	  function getWarnPageRefresh()
	  {
	    if (podcast_created)
		  return null;
	    if (document.getElementById('gallery-title').value == '')
		  return null;
		return 'Are you sure you wish to leave this page? (Unsaved content will be lost unless you save it)';
	  }
	  function processSubmitUpload()
	  {
	    if (document.getElementById('podcast-upload').disabled)
		  return;
		document.getElementById('podcast-upload').disabled = true;
		ManageWindow('process-in-action');
		var createCall = web_path + '/components/create-podcast.php?title=' + encodeURIComponent(document.getElementById('podcast-title').value) +
			'&audio=' + document.getElementById('audio-filepath').value + '&icon=' + document.getElementById('icon-filepath').value + '&icon_x=' +
			document.getElementById('podcast-crop-x').value + '&icon_y=' + document.getElementById('podcast-crop-y').value + '&icon_width=' +
			document.getElementById('podcast-crop-width').value + '&icon_height=' + document.getElementById('podcast-crop-height').value +
			'&orig_image=' + document.getElementById('podcast-crop-image-filepath').value;
		if (document.getElementById('podcast-related-group-use').value)
		{
		  var selected_group_type = document.getElementById('podcast-related-group-type').value;
		  if (selected_group_type != '[none]' && document.getElementById('podcast-related-group-' + selected_group_type).value != '[none]')
		    createCall += '&related_group=' + document.getElementById('podcast-related-group-' + selected_group_type).value;
		}
		if (document.getElementById('podcast-topic') != undefined && document.getElementById('podcast-topic-use').value &&
		  document.getElementById('podcast-topic').value != '[none]')
		  createCall += '&topic=' + document.getElementById('podcast-topic').value;
		executeAJAX(createCall, function processCreate(result)
		{
		  openWindow.closeWindow();
		  if (result.indexOf('[Error]') < 0)
		  {
		    podcast_created = true;
		    window.location = '" . MANAGE_WEB_PATH . "/podcast/create/success-' + result + '/';
			return;
		  }
		  document.getElementById('podcast-upload').disabled = false;
		  alert(result.replace('[Error] ', ''));
		});
	  }";
	}
	function getPageContents()
	{
	  echo "<div class=\"contentHeader\">Upload New Podcast</div>\n";
	  
	  echo "<div class=\"createForm\">\n";
	  echo "  <div class=\"formLabel\">Title:</div>\n";
	  echo "  <input type=\"text\" class=\"inputText\" name=\"podcast-title\" id=\"podcast-title\" autocomplete=\"off\" onKeyUp=\"executeAJAX(web_path + '/components/" .
		"check-title.php?type=podcast&minimum=" . $this->_podcastTitleMinimum . "&title=' + escapeStringURL(this.value), function " .
		"processTitle(return_value) {
		  if (return_value.indexOf('[Error]') >= 0)
		  {
		    document.getElementById('title-error').innerHTML = return_value.replace('[Error] ', '');
			if (document.getElementById('title-error').isFading != true)
			{
		      document.getElementById('title-error').style.display = 'block';
			  fadeOutAndHide('title-error', 2400);
		    }
			document.getElementById('podcast-title').className = 'inputText formComponentInvalid';
			document.getElementById('podcast-upload').disabled = true;
		  } else
		  {
		    document.getElementById('title-error').style.display = 'none';
		    document.getElementById('podcast-title').className = 'inputText formComponentValid';
			checkFormValid();
		  } });\" />\n";
	  echo "  <div class=\"contentError\" id=\"title-error\" style=\"display:none;\"></div>\n";
	  
	  echo "<iframe id=\"frame-podcast-image\" name=\"iconForm\" class=\"formFrame\" style=\"display:none;\"></iframe>\n";
	  echo "  <div class=\"formLabel miniSeparator\">Icon File:</div>\n";
	  echo "  <input type=\"hidden\" id=\"icon-filepath\" value=\"\" />\n";
	  echo "  <div class=\"inputFile\" id=\"icon-image-input\">\n";
	  echo "    <div class=\"button\">Select</div>\n";
	  echo "    <div class=\"textBox\" id=\"icon-input-textbox\"></div>\n";
	  echo "    <form method=\"POST\" enctype=\"multipart/form-data\" target=\"iconForm\" action=\"" . MANAGE_WEB_PATH . "/components/upload-podcast-icon.php\" " .
		"id=\"upload-podcast-form\">\n";
	  echo "      <input type=\"file\" class=\"inputFile\" name=\"podcast-icon\" onChange=\"document.getElementById('upload-podcast-form').submit();" .
		"document.getElementById('upload-podcast-form').disabled = true;\" id=\"podcast-icon\" />\n";
	  echo "    </form>\n";
	  echo "  </div>\n";
	  echo "  <center id=\"icon-crop\" style=\"display:none;\">\n";
	  echo "    <input type=\"hidden\" id=\"podcast-crop-image-filepath\" />\n";
	  echo "    <input type=\"hidden\" id=\"podcast-crop-x\" />\n";
	  echo "    <input type=\"hidden\" id=\"podcast-crop-y\" />\n";
	  echo "    <input type=\"hidden\" id=\"podcast-crop-width\" />\n";
	  echo "    <input type=\"hidden\" id=\"podcast-crop-height\" />\n";
	  echo "    <img id=\"icon-crop-img\" /><br />\n";
	  echo "    <input type=\"button\" class=\"inputButton\" id=\"cropIconButton\" value=\"Crop\" onClick=\"cropIcon();\" />\n";
	  echo "  </center>\n";
	  
	  echo "<iframe id=\"frame-podcast-audio\" name=\"audioForm\" class=\"formFrame\" style=\"display:none;\"></iframe>\n";
	  echo "  <div class=\"formLabel miniSeparator\">Audio File:</div>\n";
	  echo "  <input type=\"hidden\" id=\"audio-filepath\" value=\"\" />\n";
	  echo "  <div class=\"inputFile\" id=\"audio-file-input\">\n";
	  echo "    <div class=\"button\">Select</div>\n";
	  echo "    <div class=\"textBox\" id=\"audio-input-textbox\"></div>\n";
	  echo "    <form method=\"POST\" enctype=\"multipart/form-data\" target=\"audioForm\" action=\"" . MANAGE_WEB_PATH . "/components/upload-podcast-audio.php\" " .
		"id=\"upload-podcast-audio-form\">\n";
	  echo "      <input type=\"file\" class=\"inputFile\" name=\"podcast-audio\" onChange=\"document.getElementById('upload-podcast-audio-form').submit();" .
		"document.getElementById('upload-podcast-audio-form').disabled = true;\" id=\"podcast-audio\" />\n";
	  echo "    </form>\n";
	  echo "  </div>\n";
	  
	  echo "  <div class=\"formLabel separator\">Related Group:</div>\n";
	  echo "  <input type=\"checkbox\" id=\"podcast-related-group-use\" onChange=\"toggleUseRelatedGroup();\" /> " .
		"<label for=\"podcast-related-group-use\">Link to a related group?</label><br />\n";
	  echo "  <select id=\"podcast-related-group-type\" style=\"display:none;\" class=\"inputSelect selectLeft\" " .
		"onChange=\"selectGroupByType(this.value);\">\n";
	  echo "    <option value=\"[none]\">(Group Type)</option>\n";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	    echo "    <option value=\"" . $handle . "\">" . $info["NAME"] . "</option>\n";
	  echo "  </select>\n";
	  foreach ($this->_relatedGroupTypes as $handle => $info)
	  {
	    echo "  <select id=\"podcast-related-group-" . $handle .
			"\" class=\"inputSelect selectRight\" style=\"display:none;\">\n";
		echo "    <option value=\"[none]\">(Group)</option>\n";
		foreach ($info["GROUPS"] as $group)
		  echo "    <option value=\"" . $group["IDENTITY"] . "\">" . $group["TITLE"] .
			(!$group["ACTIVE"] ? " (Inactive)" : "") . "</option>\n";
	    echo "  </select>\n";
	  }
	  echo "  <div class=\"formLabel separator\" style=\"clear:both;\">Topic:</div>\n";
	  if (sizeof($this->_topics) > 0)
	  {
	    echo "  <input type=\"checkbox\" id=\"podcast-topic-use\" onChange=\"toggleUseTopic();\" /> " .
		"<label for=\"podcast-topic-use\">Link to a topic?</label><br />\n";
	    echo "  <select id=\"podcast-topic\" class=\"inputSelect\" style=\"display:none;\">\n";
	    echo "    <option value=\"[none]\">(Topic)</option>\n";
	    foreach ($this->_topics as $topic)
	      echo "    <option value=\"" . $topic["IDENTITY"] . "\">" . $topic["TITLE"] . "</option>\n";
	    echo "  </select>\n";
	  } else
	    echo "  <div>There are no topics currently active.</div>\n";
	  echo "  <center><input type=\"button\" class=\"submitButton\" id=\"podcast-upload\" value=\"Upload\" disabled=\"disabled\" " .
		"onClick=\"processSubmitUpload();\" /></center>\n";
	  echo "</div>\n";
	  echo "  <div style=\"clear:both;\"></div>\n";
	}
}
?>