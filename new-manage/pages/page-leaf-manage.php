<?php
class GoavesManageLeafManage {
	private $_issuuIdentity;
	private $_title;
	private $_handle;
	private $_postDate;
	private $_loadingText;
	private $_bwCover;
	private $_colorCover;
	private $_numberPages;
	
	private $_schoolYear;
	private $_items = array();
	
	private $_featuringSuccess = null;
	private $_unfeaturingSuccess = null;
	private $_editSuccess = null;
	private $_publishToggled = null;
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM leaf_issuu WHERE issuu_identity='" . $database->escapeString($path[2]) . "'") == 0)
	  {
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-beats-manage", "LOADED" => false, "IDENTITY" => $path[2]);
		return new GoavesManageBeatList();
	  }
	  if (isset($path[3]) && strtolower($path[3]) == "edit-information")
	    return new GoavesManageBeatEditInfo();
	  if (isset($path[3]) && strtolower($path[3]) == "edit-text")
	    return new GoavesManageBeatEditText();
	  
	  $info = $database->querySingle("SELECT issuu_identity, document_id, document_name, loading_text, post_date, issue_name, handle, bw_cover, " .
		"color_cover, number_pages FROM leaf_issuu WHERE issuu_identity='" . $database->escapeString($path[2]) . "' LIMIT 1", true);
	  $this->_issuuIdentity = $info["issuu_identity"];
	  $this->_title = format_content($info["issue_name"]);
	  $this->_handle = $info["handle"];
	  $this->_postDate = strtotime($info["post_date"]);
	  $this->_loadingText = format_content($info["loading_text"]);
	  $this->_bwCover = $info["bw_cover"];
	  $this->_colorCover = $info["color_cover"];
	  $this->_numberPages = $info["number_pages"];
	  
	  $this->_schoolYear = (date("m", $this->_postDate) <= 6 ? date("Y", $this->_postDate) - 1 : date("Y", $this->_postDate));
	 
	  $get_items = $database->query("SELECT item_identity, page_number, staff_identities, item_title, subtitle, item_type, noteworthy, related_group, special_identity " .
		"FROM leaf_items WHERE leaf_identity='" . $this->_issuuIdentity . "' ORDER BY page_number ASC");
	  while ($item = $get_items->fetchArray())
	  {
	    $staff_identities = explode(",", $item["staff_identities"]);
		$item =  array("IDENTITY" => $item["item_identity"], "PAGE_NUMBER" => $item["page_number"],
			"TITLE" => format_content($item["item_title"]), "SUBTITLE" => format_content($item["subtitle"]),
			"NOTEWORTHY" => ($item["noteworthy"] == "TRUE"), "STAFF" => array(), "TYPE" => $item["item_type"]);
		foreach ($staff_identities as $staff_identity)
		{
		  $staff = $database->querySingle("SELECT identity, first_name, last_name FROM staff WHERE identity='" . $database->escapeString($staff_identity) .
			"' LIMIT 1", true);
		  $item["STAFF"][] = array("IDENTITY" => $staff["identity"], "FIRST_NAME" => format_content($staff["first_name"]),
			"LAST_NAME" => format_content($staff["last_name"]));
	    }
		$this->_items[] = $item;
	  }
			
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]))
	  {
	    switch ($_SESSION[MANAGE_TRANSFER_DATA]["FROM"])
		{
		  case "toggle-publish.php": $this->_publishToggled = array("TOGGLED" => true, "TO_VALUE" => $_SESSION[MANAGE_TRANSFER_DATA]["NEW_VALUE"]); break;
		  case "update-beat-info": $this->_editSuccess = "info"; break;
		  case "update-beat-text": $this->_editSuccess = "text"; break;
		  case "set-feature-component.php":
		  {
		    $this->_featuringSuccess = true;
			break;
		  }
		  case "remove-feature.php": $this->_unfeaturingSuccess = true; break;
		}
		unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  return true;
	}
	function getRequirePermission() { return PERM_LEAF; }
	function getPageHandle() { return "leaf"; }
	function getPageSubhandle() { return "manage"; }
	function getPageTitle() { return "[Leaf] '" . $this->_title . "'"; }
	function getBreadTrail() { return array("leaf" => "<i>The Leaf</i>", ("leaf/list/" . $this->_schoolYear) => ($this->_schoolYear . " - " . ($this->_schoolYear + 1)),
		"[this]" => $this->_title); }
	function getPageStylesheet() { return "stylesheet-leaf.css"; }
	function getBodyOnload() { return ($this->_publishToggled != null ? "fadeOutAndHide('toggle-publish-success', 3000); " : null) .
		($this->_editSuccess != null ? "fadeOutAndHide('edit-success', 3000); " : null) . ($this->_unfeaturingSuccess != null ? "fadeOutAndHide('unfeature-success', " .
		"3000);" : null); }
	function getPageJavascript()
	{
	  return "function toggleLeafItem(item_identity)
	  {
	    document.getElementById('item-' + item_identity).className = (document.getElementById('item-' + item_identity).className == 'leafItemContainer' ?
			'leafItemContainer open' : 'leafItemContainer');
	  }";
	}
	function getPageContents()
	{	  
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <a class=\"returnLink\" href=\"" . MANAGE_WEB_PATH . "/leaf/list/" . $this->_schoolYear . "/\">&laquo; Return</a>\n";
	  echo "  [<i>Leaf</i>] '" . $this->_title . "'\n";
	  echo "</div>\n";
	  
	  if ($this->_editSuccess != null)
	  {
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"edit-success\"><b>Edit success.</b> The ";
		switch ($this->_editSuccess)
		{
		  case "info": echo "general information"; break;
		  case "text": echo "text"; break;
		}
		echo " for this beat has been changed successfully.</div>\n";
	  }
	  if ($this->_featuringSuccess != null)
	  {
	    if ($this->_featuringSuccess)
		  echo "<div class=\"contentSuccess separator separatorBottom\"><b>Beat featured.</b> This beat is now a featured piece on the homepage.</div>\n";
		else
		  echo "<div class=\"contentError separator separatorBottom\"><b>Beat could not be featured.</b> This beat could not be featured on the homepage.</div>\n";
	  }
	  if ($this->_unfeaturingSuccess != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"unfeature-success\"><b>Featured status removed.</b> The featured status on " .
			"this beat has been removed.</div>\n";
			
	  echo "<div class=\"indentedContent\">\n";
	  echo "  <img src=\"" . WEB_PATH . "/images/leaf_covers/" . $this->_colorCover . "\" class=\"manageProfileIcon separator\" />\n";
	  echo "  <div class=\"infoLine\"><b>Title:</b> " . $this->_title . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Identity:</b> " . $this->_issuuIdentity . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Link:</b> <a href=\"" . WEB_PATH . "/leaf-issue/" .
	  	$this->_handle . "/\" target=\"_blank\">" . WEB_PATH . "/leaf-issue/" . $this->_handle . "/</a></div>\n";
	  echo "  <div class=\"infoLine\"><b>Post Date:</b> " . date(LONG_DATE_FORMAT, $this->_postDate) . "</div>\n";
	  
	  echo "  <div class=\"infoLine separator\"><b>Number of pages:</b> " . $this->_numberPages. "</div>\n";
	  
	  echo "</div>\n";
	  echo "<div style=\"clear:both;\"></div>\n";
	  echo "<div class=\"editLink separator\"><span class=\"fauxLink firstAction\" onClick=\"ManageWindow('edit-leaf','" . $this->_issuuIdentity .
		"');\"><img src=\"" . MANAGE_WEB_PATH .	"/interface/images/action_go.gif\" class=\"actionIcon\" border=\"0\" /> Edit</span> \n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('delete-content', 'leaf-" . $this->_issuuIdentity .
	  	"');\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/folder_delete.gif\" class=\"actionIcon\" border=\"0\" /> Delete</span>\n";
	  echo "</div>\n";

	  echo "<div class=\"dividingBar miniSeparator\"></div>\n";
	  echo "<div class=\"indentedContent separator\">\n";
	  echo "  <div class=\"infoLine separator separatorBottom\"><b>Leaf Items:</b> " . sizeof($this->_items) . "</div>\n";
	  foreach ($this->_items as $item)
	  {
	    echo "  <div class=\"leafItemContainer\" id=\"item-" . $item["IDENTITY"] . "\">\n";
		echo "    <div class=\"toggle\" onClick=\"toggleLeafItem('" . $item["IDENTITY"] . "');\"></div>\n";
		echo "    <div class=\"item\"><b>Title:</b> " . $item["TITLE"] . "</div>\n";
		echo "    <div class=\"item supplement\"><b>Subtitle:</b> " . ($item["SUBTITLE"] != null ? $item["SUBTITLE"] : "<i>(None)</i>") . "</div>\n";
		echo "    <div class=\"item supplement\"><b>Page:</b> " . $item["PAGE_NUMBER"] . "</div>\n";
		echo "    <div class=\"item supplement\"><b>Displays on issue brief:</b> " . ($item["NOTEWORTHY"] ? "Yes" : "No") . "</div>\n";
		echo "    <div class=\"item supplement\"><b>Type:</b> ";
		switch ($item["TYPE"])
		{
		  case "article": echo "Article"; break;
		  default: "'" . $item["TYPE"] . "'";
		}
		echo "</div>\n";
		echo "    <div class=\"item supplement\"><b>Contributors:</b> ";
		foreach ($item["STAFF"] as $index => $contributor)
		  echo $contributor["FIRST_NAME"] . " " . $contributor["LAST_NAME"] . ($index < sizeof($item["STAFF"]) - 1 ? ", " : "");
		echo "</div>\n";
		echo "    <div class=\"actions\">\n";
		echo "      <span class=\"fauxLink\" onClick=\"ManageWindow('edit-leaf-item','" . $item["IDENTITY"] . "');\"><img src=\"" .
			MANAGE_WEB_PATH . "/interface/images/action_go.gif\" class=\"actionIcon\" /> Edit</span>\n";
		echo "      <span class=\"fauxLink\" onClick=\"ManageWindow('delete-content','leaf_item-" . $item["IDENTITY"] . "');\"><img src=\"" .
			MANAGE_WEB_PATH . "/interface/images/folder_delete.gif\" class=\"actionItem\" /> Delete</span>\n";
		echo "    </div>\n";
		echo "  </div>\n";
	  }
	  echo "  <div class=\"editLink separator\"><span class=\"firstAction fauxLink\" onClick=\"ManageWindow('add-leaf-item','" . $this->_issuuIdentity .
		"');\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/newspaper_add.png\" class=\"actionIcon\" /> Add</span></div>\n";
	  echo "</div>\n";
	}
}
?>