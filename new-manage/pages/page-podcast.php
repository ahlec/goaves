<?php
class GoavesManagePodcasts {
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[1]))
	    $path[1] = "landing";
	  switch ($path[1])
	  {
	    case "upload": return new GoavesManagePodcastUpload();
		case "manage": return new GoavesManagePodcastManage();
		case "list":
	    default: return new GoavesManagePodcastList();
	  }
	  return true;
	}
	function getRequirePermission() { return "3"; }
	function getPageHandle() { return "podcast"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Podcasts"; }
	function getBreadTrail() { return array("[this]" => "Podcasts"); }
	function getPageContents()
	{
	  echo "podcasts";
	}
}
?>