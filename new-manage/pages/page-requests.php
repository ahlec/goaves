<?php
class GoavesManageRequests {
	private $_requests = array();
	function checkOrRedirect($path, $database)
	{
	  $all_requests = $database->query("SELECT request_identity, manage_requests.staff_identity, staff.first_name, staff.last_name, request_datestamp, " .
		"request_type, request_type_supplement FROM manage_requests JOIN staff ON manage_requests.staff_identity = staff.identity ORDER BY request_datestamp DESC");
	  while ($request = $all_requests->fetchArray())
	  {
	    $this->_requests[] = array("IDENTITY" => $request["request_identity"], "STAFF_IDENTITY" => $request["staff_identity"],
			"STAFF_NAME" => ($request["first_name"] . " " . $request["last_name"]), "TYPE" => $request["request_type"],
			"TYPE_SUPPLEMENT" => $request["request_type_supplement"], "DATESTAMP" => strtotime($request["request_datestamp"]));
		$request_data = $database->query("SELECT data_handle, data FROM manage_requests_data WHERE request_identity='" . $request["request_identity"] . "'");
		$data_array = array();
		while ($data = $request_data->fetchArray())
		  $data_array[$data["data_handle"]] = $data["data"];
		$this->_requests[sizeof($this->_requests) - 1]["DATA"] = $data_array;
	  }
	  
	  /*if (isset($_SESSION[MANAGE_TRANSFER_DATA]) && $_SESSION[MANAGE_TRANSFER_DATA]["FROM"] == "page-comics-upload" && $_SESSION[MANAGE_TRANSFER_DATA]["SUCCESS"])
	  {
		  $this->_successWithPost = $database->querySingle("SELECT title FROM cartoons WHERE identity='" . $database->escapeString($_SESSION[MANAGE_TRANSFER_DATA]["IDENTITY"]) .
			"' LIMIT 1");
		  unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }*/
	  
	  return true;
	}
	function getRequirePermission() { return "11"; }
	function getPageHandle() { return "requests"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Manage Requests"; }
	function getBreadTrail() { return array("[this]" => "Admin Requests"); }
	function getPageStylesheet() { return "stylesheet-admin.css"; }
	function getPageContents()
	{
	  /*if ($this->_successWithPost != null)
		  echo "<div class=\"contentSuccess separatorBottom\" id=\"upload-success\" onLoad=\"fadeOutAndHide('upload-success', 3000);\"><b>Upload Successful.</b>" .
			"The comic '" . $this->_successWithPost . "' has been uploaded to Goaves.com.</div>\n";*/
	  echo "<div class=\"contentHeader\">Admin Requests</div>\n";
	  if (sizeof($this->_requests) == 0)
	  {
	    echo "<div class=\"contentText\">There are no pending requests.</div>\n";
		return;
	  }
	  echo "<div class=\"contentText\">All pending requests are listed below.</div>\n";
	  echo "<table class=\"requestTable\">\n";
	  echo "  <tr class=\"header\">\n";
	  echo "    <td></td>\n";
	  echo "    <td>Staff Member</td>\n";
	  echo "    <td>Request Information</td>\n";
	  echo "    <td></td>\n";
	  echo "  </tr>\n";
	  foreach ($this->_requests as $request)
	  {
	    echo "  <tr>\n";
		echo "    <td>\n";
		echo "      <div class=\"requestType\">";
		switch ($request["TYPE"])
		{
		  case "feature_content": echo "Feature Content"; break;
		  default: echo $request["TYPE"];
		}
		echo "</div>\n";
		echo "      <div class=\"requestDate\">" . date(DATETIME_FORMAT, $request["DATESTAMP"]) . "</div>\n";
		echo "    </td>\n";
		echo "    <td>\n";
		echo "      <div class=\"requestStaff\">" . $request["STAFF_NAME"] . "</div>\n";
		echo "    </td>\n";
		echo "    <td class=\"content\">\n";
		switch ($request["TYPE"])
		{
		  case "feature_content":
		  {
		    echo "<b>Content:</b>";
			switch ($request["DATA"]["feature_type"])
			{
			  case "article": $content_type = "beat"; $content_name = "Beat"; break;
			}
			echo " <a href=\"" . MANAGE_WEB_PATH . "/lookup/" . $content_type . "/" . $request["DATA"]["item_identity"] . "/\" target=\"_blank\" />[" .
				$content_name . "] #" . $request["DATA"]["item_identity"] . "</a><br />\n";
			echo "<b>Image:</b> <a href=\"" . MANAGE_WEB_PATH . "/limbo/" . $request["DATA"]["header_image"] . "\" target=\"_blank\">[Link]</a><br />\n";
			echo "<b>Feature Date:</b> " . date(DATETIME_FORMAT, strtotime($request["DATA"]["feature_date"])) . "<br />\n";
		    break;
		  }
		  default:
		  {
		    echo "<pre>\n";
			print_r($request["DATA"]);
			echo "</pre>\n";
		  }
		}
		echo "    </td>\n";
		echo "    <td><input type=\"button\" value=\"Approve\" /> <input type=\"button\" value=\"Reject\" /></td>\n";
		echo "  </tr>\n";
	  }
	  echo "</table>\n";
	  echo "</div>\n";
	}
}
?>