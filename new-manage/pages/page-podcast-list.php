<?php
class GoavesManagePodcastList {
	private $_podcasts = array();
	
	private $_deletedGallery = null;
	function checkOrRedirect($path, $database)
	{	
	  $all_podcasts = $database->query("SELECT podcast_identity, title, date_posted, published, icon_file FROM podcasts " .
		"WHERE staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' ORDER BY date_posted DESC");
	  while ($podcast = $all_podcasts->fetchArray())
	  {
	    $date_created = strtotime($podcast["date_posted"]);
		$school_year_start_posted = date("Y", $date_created);
		if (date("m", $date_created) < 6)
		  $school_year_start_posted -= 1;
	    $this->_podcasts[] = array("IDENTITY" => $podcast["podcast_identity"], "TITLE" => format_content($podcast["title"]),
			"DATE_POSTED" => $date_created, "PUBLISHED" => ($podcast["published"] == "TRUE"),
			"SCHOOL_YEAR" => $school_year_start_posted . " - " . ($school_year_start_posted + 1),
			"ICON_FILE" => $podcast["icon_file"]);
	  }
	  
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]) && $_SESSION[MANAGE_TRANSFER_DATA]["FROM"] == "delete-content.php")
	  {
	    $this->_deletedGallery = $_SESSION[MANAGE_TRANSFER_DATA]["TITLE"];
	    unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  
	  return true;
	}
	function getRequirePermission() { return "3"; }
	function getPageHandle() { return "podcast"; }
	function getPageSubhandle() { return "list"; }
	function getPageTitle() { return "My Podcasts"; }
	function getBreadTrail() { return array("podcast" => "Podcasts", "[this]" => "My Podcasts"); }
	function getPageStylesheet() { return "stylesheet-podcasts.css"; }
	function getBodyOnload()
	{
	  if ($this->_deletedGallery != null)
	    return "fadeOutAndHide('gallery-deleted-success', 3000);";
	  return null;
	}
	function getPageContents()
	{
	  if ($this->_deletedGallery != null)
	    echo "<div class=\"contentSuccess separatorBottom\" id=\"gallery-deleted-success\"><b>Deletion successful.</b> Gallery '" .
	    	$this->_deletedGallery . "' and all photos within have been deleted.</div>\n";
			
	  echo "<div class=\"contentHeader\">My Podcasts</div>\n";
	  if (sizeof($this->_podcasts) == 0)
	  {
	    echo "<div class=\"contentText\">You have no podcasts</div>\n";
		echo "<div class=\"contentText miniSeparator\"><a href=\"" . MANAGE_WEB_PATH .
			"/podcast/upload/\">Upload a new podcast</a></div>\n";
		return;
	  }
	  echo "<div class=\"contentText\">Select a podcast from below to manage.</div>\n";
	  echo "<div class=\"listingContainer\">\n";
	  $last_school_year = null;
	  foreach ($this->_podcasts as $podcast)
	  {
	    if ($last_school_year == null || $last_school_year != $podcast["SCHOOL_YEAR"])
		{
		  if ($last_school_year != null)
		    echo "  </div>\n";
		  echo "  <div class=\"yearContainer" . ($last_school_year != null ? " separator" : "") . "\">\n";
		  echo "    <div class=\"yearHeader\">" . $podcast["SCHOOL_YEAR"] . "</div>\n";
		  $last_school_year = $podcast["SCHOOL_YEAR"];
		}
		echo "  <a href=\"" . MANAGE_WEB_PATH . "/podcast/manage/" . $podcast["IDENTITY"] . "/\" class=\"item\" style=\"background-image:url('" .
			WEB_PATH . "/images/podcasts/" . $podcast["ICON_FILE"] . "');\">\n";
		echo "    <div class=\"overlay\"></div>\n";
		echo "    <div class=\"status " . ($podcast["PUBLISHED"] ? "statusPublished" : "statusUnpublished") . "\"></div>\n";
		echo "    <div class=\"title\">" . $podcast["TITLE"] . "</div>\n";
		echo "  </a>\n";
	  }
	  echo "  </div>\n";
	  echo "</div>\n";
	}
}
?>