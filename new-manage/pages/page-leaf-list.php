<?php
class GoavesManageLeafList {
	private $_postedBeat = null;
	private $_deletedBeat = null;
	private $_forSchoolYear = null;
	private $_years = array();
	private $_issues = array();
	function checkOrRedirect($path, $database)
	{
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]) && $_SESSION[MANAGE_TRANSFER_DATA]["FROM"] == "page-beat-compose" &&
	    $_SESSION[MANAGE_TRANSFER_DATA]["SUCCESS"])
	  {
	    $posted_information = $database->querySingle("SELECT handle, title FROM beats WHERE beat_identity='" .
	    	$database->escapeString($_SESSION[MANAGE_TRANSFER_DATA]["IDENTITY"]) . "' LIMIT 1", true);
	    $this->_postedBeat = array("HANDLE" => $posted_information["handle"],
	    	"TITLE" => format_content($posted_information["title"]));
	    unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]) && $_SESSION[MANAGE_TRANSFER_DATA]["FROM"] == "delete-content.php" &&
	    $_SESSION[MANAGE_TRANSFER_DATA]["SUCCESS"])
		{
		  $this->_deletedBeat = $_SESSION[MANAGE_TRANSFER_DATA]["TITLE"];
		  unset ($_SESSION[MANAGE_TRANSFER_DATA]);
		}
	  
	  if (isset($path[2]) && strlen(strval($path[2])) == 4 && ctype_digit($path[2]))
	    $this->_forSchoolYear = $database->escapeString($path[2]);
	  else if (isset($path[1]) && strlen(strval($path[1])) == 4 && ctype_digit($path[1]))
	    $this->_forSchoolYear = $database->escapeString($path[1]);
	  else
	    $this->_forSchoolYear = (date("m") < 6 ? date("Y") - 1 : date("Y"));
	
	  $unique_year_months = $database->query("SELECT DISTINCT SUBSTR(post_date, 0, 4) AS \"year\", SUBSTR(post_date, 6, 2) AS \"month\" FROM leaf_issuu");
	  while ($year_month = $unique_year_months->fetchArray())
	  {
	    $year_in_question = $year_month["year"];
	    if ($year_month["month"] < 6)
		  $year_in_question -= 1;
		if (!in_array($year_in_question, $this->_years))
		  $this->_years[] = $year_in_question;
	  }
	  sort($this->_years);
	
	  $all_issues = $database->query("SELECT issuu_identity, issue_name, post_date, color_cover, number_pages FROM leaf_issuu WHERE (post_date >= '" .
		$this->_forSchoolYear . "-06' AND post_date < '" . ($this->_forSchoolYear + 1) . "-06') ORDER BY post_date DESC");
	  while ($issue = $all_issues->fetchArray())
	  {
	    $date_posted = strtotime($issue["post_date"]);
		$school_year_start_posted = date("Y", $date_posted);
		if (date("m", $date_posted) < 6)
		  $school_year_start_posted -= 1;
	    $this->_issues[] = array("IDENTITY" => $issue["issuu_identity"],
			"TITLE" => format_content($issue["issue_name"]), "POST_DATE" => $date_posted,
	    	"ICON_FILE" => $issue["color_cover"], "SCHOOL_YEAR" => $school_year_start_posted . " - " . ($school_year_start_posted + 1));
	  }
	  return true;
	}
	function getRequirePermission() { return PERM_LEAF; }
	function getPageHandle() { return "leaf"; }
	function getPageSubhandle() { return "list"; }
	function getPageTitle() { return "The Leaf Listing"; }
	function getBreadTrail() { return array("leaf" => "<i>The Leaf</i>", "[this]" => ($this->_forSchoolYear . " - " . ($this->_forSchoolYear + 1))); }
	function getPageStylesheet() { return "stylesheet-leaf.css"; }
	function getBodyOnload() { return ($this->_deletedBeat != null ? "fadeOutAndHide('beat-delete-success','3000');" : null); }
	function getPageJavascript()
	{
	  return "function toggleSubgroup(month)
	  {
	    var monthContainer = document.getElementById('container-' + month);
		var monthHeader = document.getElementById('header-' + month);
	    if (monthContainer.className.indexOf('subgroupCollapsed') > -1)
		{
		  monthContainer.className = monthContainer.className.replace('subgroupCollapsed', 'subgroupExpanded');
		  monthHeader.title = 'Collapse';
		} else
		{
		  monthContainer.className = monthContainer.className.replace('subgroupExpanded', 'subgroupCollapsed');
		  monthHeader.title = 'Expand';
		}
	  }";
	}
	function getPageContents()
	{
	  if ($this->_postedBeat !== null)
	  {
	    echo "<a href=\"" . WEB_PATH . "/article/" . $this->_postedBeat["HANDLE"] . "/\" target=\"_blank\" " .
	    	"onClick=\"fadeOutAndHide('beat-posted-success');\" id=\"beat-posted-success\"><div class=\"contentSuccess " .
	    	"separatorBottom\"><b>Beat Posted.</b> '" . $this->_postedBeat["TITLE"] . "' has been posted. Click this to view " .
	    	"the beat.</div></a>\n";
	  }
	  if ($this->_deletedBeat !== null)
	    echo "<div class=\"contentSuccess separatorBottom\" id=\"beat-delete-success\"><b>Beat Deleted.</b> The beat '" . $this->_deletedBeat .
			"' has been deleted.</div>\n";
	
	  echo "<div class=\"contentHeader\">\n";
	  echo "  <i>The Leaf</i> online\n";
	  echo "</div>\n";
	  echo "<div class=\"contentText miniSeparator\">Select an issue from below.</div>\n";
	  
	  echo "<div class=\"leafYearCalendar\">\n";
	  echo "  <div class=\"currentYear\">" . $this->_forSchoolYear . " - " . ($this->_forSchoolYear + 1) . " <i>Leaf</i> issues</div>\n";
	  echo "  <div class=\"otherYears\">\n";
	  for ($year_index = 0; $year_index < sizeof($this->_years); $year_index++)
	  {
	    if ($this->_years[$year_index] == $this->_forSchoolYear)
		{
		  echo "    <b>" . $this->_years[$year_index] . "-" . ($this->_years[$year_index] + 1) . "</b>" . ($year_index < sizeof($this->_years)
			- 1 ? " &bull; " : "") . "\n";
		} else
	      echo "    <a href=\"" . MANAGE_WEB_PATH . "/leaf/list/" . $this->_years[$year_index] . "/\">" . $this->_years[$year_index] . 
			"-" . ($this->_years[$year_index] + 1) . "</a>" . ($year_index < sizeof($this->_years) - 1 ? " &bull; " : "") . "\n";
	  }
	  echo "  </div>\n";
	  
	  echo "</div>\n";
	  echo "<div class=\"leafListingContainer listingContainer\">\n";
	  foreach ($this->_issues as $issue)
	  {
		echo "  <a href=\"" . MANAGE_WEB_PATH . "/leaf/manage/" . $issue["IDENTITY"] . "/\">\n";
	    echo "  <div class=\"issue\">\n";
	    echo "    <img src=\"" . WEB_PATH . "/images/leaf_covers/" . $issue["ICON_FILE"] . "\" class=\"icon\" />\n";
	    echo "    <div class=\"title\">" . $issue["TITLE"] . "</div>\n";
	    echo "    <div class=\"info\">\n";
	    echo "      <span class=\"infoLabel\">Posted</span> " . date(LONG_DATETIME_FORMAT, $issue["POST_DATE"]) . "<br />\n";
	    echo "    </div>\n";
	    echo "    <div style=\"clear:both;\"></div>\n";
	    echo "  </div>\n";
		echo "  </a>\n";
	  }
	  echo "    </div>\n";
	  echo "  </div>\n";
	  echo "</div>\n";
	}
}
?>