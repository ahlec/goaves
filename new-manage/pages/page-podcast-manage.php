<?php
class GoavesManagePodcastManage {
	private $_podcastIdentity;
	private $_authorInformation = array();
	private $_handle;
	private $_title;
	private $_datePosted;
	private $_published;
	private $_audioFile;
	private $_iconFile;
	private $_description;
	private $_transcript;
	private $_dateLastUpdated;
	private $_relatedGroup;
	private $_topic;
	
	private $_isFeatured = false;
	private $_featureDate = null;
		  
	private $_successWithCreation = null;
	private $_publishToggled = null;
	private $_featuringSuccess = null;
	private $_unfeaturingSuccess = null;
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM podcasts WHERE podcast_identity='" .
		$database->escapeString($path[2]) . "' AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
	  {
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-podcast-manage", "LOADED" => false, "IDENTITY" => $path[2]);
		return new GoavesManagePodcastList();
	  }
	
	  $info = $database->querySingle("SELECT podcast_identity, podcasts.staff_identity, staff.first_name, staff.last_name, " .
		"podcasts.handle, podcasts.title, podcasts.date_posted, podcasts.published, podcasts.audio_file, podcasts.icon_file, " .
		"podcasts.description, podcasts.transcript, podcasts.date_last_updated, podcasts.related_group, groups.handle AS " .
		"\"related_group_handle\", groups.title AS \"related_group_title\", podcasts.topic, " .
		"topics.handle AS \"topic_handle\", topics.title AS \"topic_title\" FROM podcasts JOIN staff ON podcasts.staff_identity = " .
		"staff.identity LEFT JOIN groups ON podcasts.related_group = groups.group_identity LEFT JOIN topics ON podcasts.topic = " .
		"topics.topic_identity WHERE podcasts.podcast_identity='" . $database->escapeString($path[2]) . "' LIMIT 1", true);
	  $this->_podcastIdentity = $info["podcast_identity"];
	  $this->_title = format_content($info["title"]);
	  $this->_handle = $info["handle"];
	  $this->_datePosted = strtotime($info["date_posted"]);
	  $this->_audioFile = $info["audio_file"];
	  $this->_iconFile = $info["icon_file"];
	  $this->_authorInformation = array("IDENTITY" => $info["staff_identity"], "FIRST_NAME" => $info["first_name"],
		"LAST_NAME" => $info["last_name"]);
	  $this->_dateLastUpdated = strtotime($info["date_last_updated"]);
	  $this->_description = $info["description"];
	  $this->_transcript = $info["transcript"];
	  $this->_published = ($info["published"] == "TRUE");
	  $this->_relatedGroup = ($info["related_group"] != "" ? array("IDENTITY" => $info["related_group"],
	  	"HANDLE" => $info["related_group_handle"], "TITLE" => format_content($info["related_group_title"])) : null);
	  $this->_topic = ($info["topic"] != "" ? array("IDENTITY" => $info["topic"], "HANDLE" => $info["topic_handle"],
	  	"TITLE" => format_content($info["topic_title"])) : null);
	  
	  $feature_info = $database->querySingle("SELECT date_featured FROM features WHERE feature_type='podcast' " .
		"AND item_identity='" . $this->_podcastIdentity . "' LIMIT 1");
	  $this->_isFeature = ($feature_info != null && $feature_info != false);
	  $this->_featureDate = strtotime($feature_info);
	  
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]))
	  {
	    switch ($_SESSION[MANAGE_TRANSFER_DATA]["FROM"])
		{
		  case "toggle-publish.php": $this->_publishToggled = array("TOGGLED" => true, "TO_VALUE" => $_SESSION[MANAGE_TRANSFER_DATA]["NEW_VALUE"]); break;
		  case "set-feature-component.php": $this->_featuringSuccess = true; break;
		  case "remove-feature.php": $this->_unfeaturingSuccess = true; break;
		}
		unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  
	  return true;
	}
	function getRequirePermission() { return "3"; }
	function getPageHandle() { return "podcast"; }
	function getPageSubhandle() { return "manage"; }
	function getPageTitle() { return "[Podcast] '" . $this->_title . "'"; }
	function getBreadTrail() { return array("podcast" => "Podcasts", "podcast/list" => "My Podcasts",
		"[this]" => "'" . $this->_title . "'"); }
	function getPageStylesheet() { return "stylesheet-podcasts.css"; }
	function getBodyOnload() { return ($this->_successWithCreation ? "fadeOutAndHide('podcast-created-success', 3000);" : null) .
		($this->_publishToggled ? "fadeOutAndHide('publish-toggle-success', 3000);" : null) .
		($this->_featuringSuccess ? "fadeOutAndHide('feature-success', 3000);" : null) . ($this->_unfeaturingSuccess ? "fadeOutAndHide('unfeature-success', 3000);" :
		null); }
	function getPageJavascript()
	{
	  return "function togglePublish(podcast_identity)
	  {
	    executeAJAX(web_path + '/components/toggle-publish.php?type=podcast&identity=' + podcast_identity, function results(value)
		{
		  if (value == 'success')
		  {
		    window.location = web_path + '/podcast/manage/' + podcast_identity + '/';
			return;
		  }
		  alert(value);
		});
	  }";
	}
	function getPageContents()
	{ 
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <div class=\"explainButton\" onClick=\"ManageWindow('explain','manage-podcast');\"></div>\n";
	  echo "  <a class=\"returnLink\" href=\"" . MANAGE_WEB_PATH . "/podcast/list/\">&laquo; Return</a>\n";
	  echo "  Manage Podcast '" . $this->_title . "'\n";
	  echo "</div>\n";
	  
	  if ($this->_successWithCreation != null)
		  echo "<div class=\"contentSuccess separatorBottom\" id=\"podcast-created-success\"><b>Upload Successful.</b>" .
			"The podcast has been successfully uploaded.</div>\n";
	  if ($this->_publishToggled != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"publish-toggle-success\"><b>" . ($this->_publishToggled["TO_VALUE"] == "TRUE" ?
			"Publish" : "Unpublish") . " success.</b> This podcast has been " . ($this->_publishToggled["TO_VALUE"] == "TRUE" ? "published" : "unpublished") .
			".</div>\n";
	  if ($this->_featuringSuccess != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"feature-success\"><b>Soundslide presentation featured.</b> This soundslide " .
			"presentation is now a featured piece on the homepage.</div>\n";
	  if ($this->_unfeaturingSuccess != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"unfeature-success\"><b>Featured status removed.</b> The featured status on " .
			"this soundslide presentation has been removed.</div>\n";
			
	  echo "<div class=\"indentedContent\">\n";
	  echo "  <img src=\"" . WEB_PATH . "/images/podcasts/" . $this->_iconFile . "\" class=\"iconFile " .
	  	"separator\" />\n";
	  echo "  <div class=\"infoLine\"><b>Title:</b> " . $this->_title . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Link:</b> " . ($this->_published ? "<a href=\"" . WEB_PATH . "/podcast/" .
	  	$this->_handle . "/\" target=\"_blank\">" . WEB_PATH . "/podcast/" . $this->_handle .
	  	"/</a> (Published)" : "<span class=\"unpublished\">Unpublished</span>") . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Date Posted:</b> " . date(LONG_DATETIME_FORMAT, $this->_datePosted) . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Date Last Updated:</b> " . date(LONG_DATETIME_FORMAT, $this->_dateLastUpdated) . "</div>\n";
	  if ($this->_isFeature)
	    echo "  <div class=\"infoLine separator\"><b>Featured:</b> " . date(DATE_FORMAT, $this->_featureDate) . "</div>\n";
	  
	  echo "  <div class=\"infoLine separator\"><b>Related Group:</b> " . ($this->_relatedGroup != null ? "<a href=\"" .
	  	WEB_PATH . "/group/" . $this->_relatedGroup["HANDLE"] . "/\" target=\"_blank\">" . $this->_relatedGroup["TITLE"] .
	  	"</a>" : "None") . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Topic:</b> " . ($this->_topic != null ? "<a href=\"" . WEB_PATH . "/topic/" .
	  	$this->_topic["HANDLE"] . "/\" target=\"_blank\">" . $this->_topic["TITLE"] . "</a>" : "None") . "</div>\n";
	  echo "</div>\n";
	  echo "<div style=\"clear:both;\"></div>\n";
	  echo "<div class=\"editLink separator\"><span href=\"" . MANAGE_WEB_PATH .
	  	"/podcast/manage/" . $this->_podcastIdentity . "/edit-info/\" class=\"disabledAction firstAction\"><img src=\"" . MANAGE_WEB_PATH .
	  	"/interface/images/action_go.gif\" class=\"actionIcon\" border=\"0\" /> Edit Information (soon)</span> \n";
	  echo "<span class=\"fauxLink\" onClick=\"togglePublish('" . $this->_podcastIdentity . "');\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/flag_" .
			($this->_published ?
	    	"red" : "green") . ".gif\" class=\"actionIcon\" border=\"0\" /> " . ($this->_published ? "Unpublish" : "Publish") . "</span>";
	  echo " \n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('feature-content', 'podcast-" . $this->_podcastIdentity . "');\"><img src=\"" . MANAGE_WEB_PATH .
		"/interface/images/" . ($this->_isFeature ? "feature_remove.png" : "feature_add.png") . "\" class=\"" .
		"actionIcon\" border=\"0\" /> " . ($this->_isFeature ? "Remove " : "") . "Feature</span>\n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('delete-content', 'podcast-" . $this->_podcastIdentity .
	  	"');\"><img src=\"" . MANAGE_WEB_PATH .
	  	"/interface/images/folder_delete.gif\" class=\"actionIcon\" border=\"0\" /> Delete</span>\n";
	  echo "</div>\n";
	  echo "<div class=\"dividingBar miniSeparator\"></div>\n";
	}
}
?>