<?php
class GoavesManageAdminStaffNew {

	function checkOrRedirect($path, $database)
	{
	  return true;
	}
	function getRequirePermission() { return PERM_ADMIN; }
	function getPageHandle() { return "admin"; }
	function getPageSubhandle() { return "staff"; }
	function getPageTitle() { return "[Admin] New Staff Member"; }
	function getBreadTrail() { return array("admin" => "[Admin Level]", "admin/staff" => "Staff", "[this]" => "New Staff Member"); }
	function getPageStylesheet() { return "stylesheet-admin.css"; }
	function getBodyOnload() { return null; }
	function getPageJavascript()
	{
	  return "function yearValid()
	  {
	    if (document.getElementById('year-graduating').value.length != 4)
		  return false;
		if (document.getElementById('year-graduating').value != parseInt(document.getElementById('year-graduating').value))
		  return false;
		return true;
	  }
	  function checkFormValid()
	  {
	    if (document.getElementById('first-name').className.indexOf('formComponentValid') > 0 &&
			document.getElementById('last-name').className.indexOf('formComponentValid') > 0 &&
			document.getElementById('position').className.indexOf('formComponentValid') > 0 &&
			document.getElementById('year-graduating').className.indexOf('formComponentValid') > 0)
			{
			  document.getElementById('add-member').disabled = false;
			  return;
			}
		document.getElementById('add-member').disabled = true;
	  }
	  function addMember()
	  {
	    if (document.getElementById('add-member').disabled)
		  return;
		document.getElementById('add-member').disabled = true;
		var addCall = web_path + '/components/create-new-staff-member.php?first_name=' + encodeURIComponent(document.getElementById('first-name').value) + '&last_name=' +
			encodeURIComponent(document.getElementById('last-name').value) + '&position=' + encodeURIComponent(document.getElementById('position').value) +
			'&year_graduating=' + document.getElementById('year-graduating').value;
		executeAJAX(addCall, function process(result)
		{
		  if (result.indexOf('success-') > -1)
		  {
		    window.location = web_path + '/admin/staff/' + result.replace('success-','') + '/';
			return;
		  } else
		  {
		    alert(result);
			document.getElementById('add-member').disabled = false;
		  }
		});
	  }
	  ";
	}
	function getPageContents()
	{
	  echo "<div class=\"contentHeader\">\n";
	  echo "  New Staff Member\n";
	  echo "</div>\n";
	  
	  echo "<div class=\"createForm noWindow\">\n";
	  
	  echo "  <div class=\"formLabel\">First Name:</div>\n";
	  echo "  <input type=\"text\" class=\"inputText formComponentInvalid\" name=\"first-name\" id=\"first-name\" autocomplete=\"off\" onKeyUp=\"executeAJAX(web_path + '/components/" .
		"check-name.php?value=' + encodeURIComponent(this.value), function " .
		"processName(return_value) {
		  if (return_value.indexOf('[Error]') >= 0)
		  {
		    document.getElementById('first-name-error').innerHTML = return_value.replace('[Error] ', '');
			if (document.getElementById('first-name-error').isFading != true)
			{
		      document.getElementById('first-name-error').style.display = 'block';
			  fadeOutAndHide('first-name-error', 2400);
		    }
			document.getElementById('first-name').className = 'inputText formComponentInvalid';
			document.getElementById('add-member').disabled = true;
		  } else
		  {
		    document.getElementById('first-name-error').style.display = 'none';
		    document.getElementById('first-name').className = 'inputText formComponentValid';
			checkFormValid();
		  } });\" />\n";
	  echo "  <div class=\"contentError\" id=\"first-name-error\" style=\"display:none;\"></div>\n";
	  
	  echo "  <div class=\"formLabel separator\">Last Name:</div>\n";
	  echo "  <input type=\"text\" class=\"inputText formComponentInvalid\" name=\"last-name\" id=\"last-name\" autocomplete=\"off\" onKeyUp=\"executeAJAX(web_path + '/components/" .
		"check-name.php?value=' + encodeURIComponent(this.value), function " .
		"processName(return_value) {
		  if (return_value.indexOf('[Error]') >= 0)
		  {
		    document.getElementById('last-name-error').innerHTML = return_value.replace('[Error] ', '');
			if (document.getElementById('last-name-error').isFading != true)
			{
		      document.getElementById('last-name-error').style.display = 'block';
			  fadeOutAndHide('last-name-error', 2400);
		    }
			document.getElementById('last-name').className = 'inputText formComponentInvalid';
			document.getElementById('add-member').disabled = true;
		  } else
		  {
		    document.getElementById('last-name-error').style.display = 'none';
		    document.getElementById('last-name').className = 'inputText formComponentValid';
			checkFormValid();
		  } });\" />\n";
	  echo "  <div class=\"contentError\" id=\"last-name-error\" style=\"display:none;\"></div>\n";
	  
	  echo "  <div class=\"formLabel separator\">Current Staff Position:</div>\n";
	  echo "  <input type=\"text\" class=\"inputText formComponentValid\" name=\"position\" id=\"position\" autocomplete=\"off\" value=\"Staff Writer\" />\n";
	  
	  echo "  <div class=\"formLabel separator\">Year Graduating:</div>\n";
	  echo "  <input type=\"text\" class=\"inputYear formComponentInvalid\" id=\"year-graduating\" autocomplete=\"off\" maxlength=\"4\" onKeyUp=\"this.className = (yearValid() ? " .
		"'inputYear formComponentValid' : 'inputYear formComponentInvalid'); checkFormValid();\" />\n";
	  
	  echo "</div>\n";
	  echo "    <center><input type=\"submit\" disabled=\"disabled\" id=\"add-member\" class=\"submitButton separator\" value=\"Add Member\" onclick=\"addMember();\" /></center>\n";
	  echo "  </div>\n";
	}
}
?>