<?php
class GoavesManagePhotoGalleriesManage {
	private $_galleryIdentity;
	private $_title;
	private $_handle;
	private $_dateLastUpdated;
	private $_published;
	private $_relatedGroup;
	private $_dateCreated;
	private $_isComplete;
	private $_special;
	private $_galleryImage;
	private $_photos = array();
	private $_isFeature = false;
	private $_featureDate = null;
	
	private $_editSuccess = null;
	private $_publishToggled = null;
	private $_photosDeleted = null;
	private $_photosEdited = null;
	function checkOrRedirect($path, $database)
	{
	  if (!isset($path[2]) || $database->querySingle("SELECT count(*) FROM photo_galleries WHERE gallery_identity='" .
		$database->escapeString($path[2]) . "'" . (substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GALLERIES, 1) == "1" ?
		" AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'" : "")) == 0)
	  {
		$_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-photo-galleries-manage", "LOADED" => false, "IDENTITY" => $path[2]);
		return new GoavesManagePhotoGalleriesList();
	  }
	  if (isset($path[3]) && strtolower($path[3]) == "edit")
	    return new GoavesManagePhotoGalleriesEdit();
	  
	  $info = $database->querySingle("SELECT gallery_identity, photo_galleries.handle, photo_galleries.title, " .
	  	"photo_galleries.date_last_updated, photo_galleries.published, photo_galleries.related_group, gallery_image, " .
	  	"groups.handle AS \"related_group_handle\", groups.title AS \"related_group_title\", photo_galleries.date_created, " .
	  	"photo_galleries.is_complete, photo_galleries.special_identity, specials.handle AS \"special_handle\", specials.title AS " .
	  	"\"special_title\" FROM photo_galleries LEFT JOIN groups ON photo_galleries.related_group = groups.group_identity " .
	  	"LEFT JOIN specials ON photo_galleries.special_identity = specials.special_identity WHERE gallery_identity='" .
	  	$database->escapeString($path[2]) . "' LIMIT 1", true);
	  $this->_galleryIdentity = $info["gallery_identity"];
	  $this->_title = format_content($info["title"]);
	  $this->_handle = $info["handle"];
	  $this->_dateLastUpdated = strtotime($info["date_last_updated"]);
	  $this->_published = ($info["published"] == "TRUE");
	  $this->_relatedGroup = ($info["related_group"] != "" ? array("IDENTITY" => $info["related_group"],
	  	"HANDLE" => $info["related_group_handle"], "TITLE" => format_content($info["related_group_title"])) : null);
	  $this->_dateCreated = strtotime($info["date_created"]);
	  $this->_isComplete = ($info["is_complete"] == "TRUE");
	  $this->_special = ($info["special_identity"] != "" ? array("IDENTITY" => $info["special_identity"], "HANDLE" => $info["special_handle"],
	  	"TITLE" => format_content($info["special_title"])) : null);
	  $this->_galleryImage = ($info["gallery_image"] != "" ? $info["gallery_image"] : "no-gallery-image.jpg");	  
	  
	  $feature_info = $database->querySingle("SELECT date_featured FROM features WHERE feature_type='photo_gallery' " .
		"AND item_identity='" . $this->_galleryIdentity . "' LIMIT 1");
	  $this->_isFeature = ($feature_info != null && $feature_info != false);
	  $this->_featureDate = strtotime($feature_info);
	  
	  if (!isset($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$this->_galleryIdentity]))
	    $_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$this->_galleryIdentity] = array();
	  
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]))
	  {
	    switch ($_SESSION[MANAGE_TRANSFER_DATA]["FROM"])
		{
		  case "toggle-publish.php": $this->_publishToggled = array("TOGGLED" => true, "TO_VALUE" => $_SESSION[MANAGE_TRANSFER_DATA]["NEW_VALUE"]); break;
		  case "delete-content.php": $this->_photosDeleted = $_SESSION[MANAGE_TRANSFER_DATA]["NUMBER_DELETED"]; break;
		  case "update-photo-gallery": $this->_editSuccess = true; break;
		  case "page-photo-galleries-manage-pictures": $this->_photosEdited = $_SESSION[MANAGE_TRANSFER_DATA]["NUMBER_EDITED"]; break;
		}
		unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  
	  $get_photos = $database->query("SELECT picture_identity, title, icon_file FROM photos WHERE " .
	  	"gallery_identity='" . $this->_galleryIdentity . "' ORDER BY sort_order ASC");
	  while ($photo = $get_photos->fetchArray())
	    $this->_photos[] = array("IDENTITY" => $photo["picture_identity"], "TITLE" => format_content($photo["title"]),
	    "ICON_FILE" => $photo["icon_file"]);
	  return true;
	}
	function getRequirePermission() { return "2"; }
	function getPageHandle() { return "photo-gallery"; }
	function getPageSubhandle() { return "manage"; }
	function getPageTitle() { return "[Photo Gallery] '" . $this->_title . "'"; }
	function getBreadTrail() { return array("photo-gallery" => "Photo Galleries", "photo-gallery/list" => "My Photo Galleries",
		"[this]" => "'" . $this->_title . "'"); }
	function getPageStylesheet() { return "stylesheet-galleries.css"; }
	function getBodyOnload() { return ($this->_publishToggled != null ? "fadeOutAndHide('toggle-publish-success', 3000); " : null) .
		($this->_photosDeleted != null ? "fadeOutAndHide('photos-deleted-success', 3000); " : null) . ($this->_editSuccess != null ?
		"fadeOutAndHide('edit-success', 3000); " : null) . ($this->_photosEdited != null ? "fadeOutAndHide('photos-edited-success', 3000); " : null); }
	function getPageJavascript()
	{
	  return "function togglePublish(gallery_identity)
	  {
	    executeAJAX(web_path + '/components/toggle-publish.php?type=gallery&identity=' + gallery_identity, function results(value)
		{
		  if (value == 'success')
		  {
		    window.location = web_path + '/photo-gallery/manage/' + gallery_identity + '/';
			return;
		  }
		  alert(value);
		});
	  }
	  function togglePictureManageLinks(to_value)
	  {
	    document.getElementById('picture-action-edit').className = (to_value ? '' : 'actionHidden');
		document.getElementById('picture-action-delete').className = 'fauxLink' + (to_value ? '' : ' actionHidden');
	  }";
	}
	function getPageContents()
	{
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]))
	  {
	    if ($_SESSION[MANAGE_TRANSFER_DATA]["FROM"] == "page-photo-galleries-manage-pictures" && !$_SESSION[MANAGE_TRANSFER_DATA]["LOADED"])
		  echo "<div class=\"contentError separator separatorBottom\" id=\"errorManagingPictures\" onLoad=\"fadeOutAndHide('errorManagingPictures', 2400);\">" .
			$_SESSION[MANAGE_TRANSFER_DATA]["REASON"] . "</div>\n";
		unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  
	  echo "<div class=\"contentHeader separatorBottom\">\n";
	  echo "  <a class=\"returnLink\" href=\"" . MANAGE_WEB_PATH . "/photo-gallery/list/\">&laquo; Return</a>\n";
	  echo "  Manage Gallery '" . $this->_title . "'\n";
	  echo "</div>\n";
	  
	  if ($this->_publishToggled != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"toggle-publish-success\"><b>" . ($this->_publishToggled["TO_VALUE"] == "TRUE" ?
			"Publish" : "Unpublish") . " success.</b> This photo gallery has been " . ($this->_publishToggled["TO_VALUE"] == "TRUE" ? "published" : "unpublished") .
			".</div>\n";
	  if ($this->_photosDeleted != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"photos-deleted-success\"><b>Photos deleted.</b> " . $this->_photosDeleted . 
			" photo" . ($this->_photosDeleted != 1 ? "s have" : " has") . " been deleted from this photo gallery.</div>\n";
	  if ($this->_editSuccess != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"edit-success\"><b>Edits successful.</b> The edits to the photo gallery have been completed " .
			"successfully.</div>\n";
	  if ($this->_photosEdited != null)
	    echo "<div class=\"contentSuccess separator separatorBottom\" id=\"photos-edited-success\"><b>Photos edited.</b> " . $this->_photosEdited .
			" photo" . ($this->_photosEdited != 1 ? "s have" : " has") . " been edited.</div>\n";
			
	  echo "<div class=\"indentedContent\">\n";
	  echo "  <img src=\"" . WEB_PATH . "/images/photos/" . $this->_galleryImage . "\" class=\"galleryImage " .
	  	"separator\" />\n";
	  echo "  <div class=\"infoLine\"><b>Title:</b> " . $this->_title . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Link:</b> " . ($this->_published ? "<a href=\"" . WEB_PATH . "/photo-gallery/" .
	  	$this->_handle . "/\" target=\"_blank\">" . WEB_PATH . "/photo-gallery/" . $this->_handle .
	  	"/</a> (Published)" : "<span class=\"unpublished\">Unpublished</span>") . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Date Created:</b> " . date(LONG_DATETIME_FORMAT, $this->_dateCreated) . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Date Last Updated:</b> " . date(LONG_DATETIME_FORMAT, $this->_dateLastUpdated) . "</div>\n";
	  if ($this->_isFeature)
	    echo "  <div class=\"infoLine separator\"><b>Featured:</b> " . date(DATE_FORMAT, $this->_featureDate) . "</div>\n";
	  
	  echo "  <div class=\"infoLine separator\"><b>Related Group:</b> " . ($this->_relatedGroup != null ? "<a href=\"" .
	  	WEB_PATH . "/group/" . $this->_relatedGroup["HANDLE"] . "/\" target=\"_blank\">" . $this->_relatedGroup["TITLE"] .
	  	"</a>" : "None") . "</div>\n";
	  echo "  <div class=\"infoLine\"><b>Special:</b> " . ($this->_special != null ? "<a href=\"" . WEB_PATH . "/special/" .
	  	$this->_special["HANDLE"] . "/\" target=\"_blank\">" . $this->_special["TITLE"] . "</a>" : "None") . "</div>\n";
	  echo "</div>\n";
	  echo "<div style=\"clear:both;\"></div>\n";
	  echo "<div class=\"editLink separator\"><a href=\"" . MANAGE_WEB_PATH .
	  	"/photo-gallery/manage/" . $this->_galleryIdentity . "/edit/\" class=\"firstAction\"><img src=\"" . MANAGE_WEB_PATH .
	  	"/interface/images/action_go.gif\" class=\"actionIcon\" border=\"0\" /> Edit</a> \n";
	  if ($this->_isComplete)
	  {
	    echo "<span class=\"fauxLink\" onClick=\"togglePublish('" . $this->_galleryIdentity . "');\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/flag_" .
			($this->_published ?
	    	"red" : "green") . ".gif\" class=\"actionIcon\" border=\"0\" /> " . ($this->_published ? "Unpublish" : "Publish") . "</span>";
	  } else
	    echo "<span class=\"disabledAction\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/flag_white.gif\" class=\"" .
	    	"actionIcon\" /> Cannot publish yet</span>";
	  echo " \n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('feature-content', 'gallery-" . $this->_galleryIdentity . "');\"><img src=\"" . MANAGE_WEB_PATH .
		"/interface/images/" . ($this->_isFeature ? "feature_remove.png" : "feature_add.png") . "\" class=\"" .
		"actionIcon\" border=\"0\" /> " . ($this->_isFeature ? "Remove " : "") . "Feature</span>\n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('delete-content', 'gallery-" . $this->_galleryIdentity .
	  	"');\"><img src=\"" . MANAGE_WEB_PATH .
	  	"/interface/images/folder_delete.gif\" class=\"actionIcon\" border=\"0\" /> Delete</span>\n";
	  echo "</div>\n";
	  echo "<div class=\"dividingBar miniSeparator\"></div>\n";
	  if (sizeof($this->_photos) > 0)
	  {
	    echo "<div class=\"indentedContent infoLine\"><b>Number of photos:</b> " . sizeof($this->_photos) . "</div>\n";
	    echo "<div class=\"indentedContent contentText separator separatorBottom\">Select the pictures you wish to edit below, then click " .
			"the edit button.</div>\n";
	    echo "<div class=\"fauxTable fauxPhotosTable\">\n";
	    foreach ($this->_photos as $photo)
	      echo "  <div class=\"cell " . (isset($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$this->_galleryIdentity]) &&
	      		in_array($photo["IDENTITY"],
			$_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$this->_galleryIdentity]) ? "toggleOn" : "toggleOff") . 
			"\" onClick=\"executeAJAX(web_path + '/components/select-gallery-picture-manage.php?identity=" .
			$photo["IDENTITY"] . "', function processToggle(value)
			{
			  if (value.indexOf('[Error]') >= 0)
			  {
			    alert(value.replace('[Error]',''));
				return;
			  }
			  var toggleResults = value.split('-');
			  document.getElementById('managePicture" . $photo["IDENTITY"] . "').className = (toggleResults[0] == 'checked' ? 'cell toggleOn' : 'cell toggleOff');
			  togglePictureManageLinks(toggleResults[1] > 0);
			});\" id=\"managePicture" . $photo["IDENTITY"] . "\"><img src=\"" . WEB_PATH . "/images/photos/" . $photo["ICON_FILE"] .
	      	"\" class=\"icon\" /><div class=\"toggledStatusDisplay\"></div>" . $photo["TITLE"] . "</div>\n";
	    echo "</div>\n";
	    echo "<div class=\"editLink separator\"><a href=\"" . MANAGE_WEB_PATH . "/photo-gallery/upload-pictures/" .
	    	$this->_galleryIdentity . "/\" class=\"firstAction\"><img src=\"" . MANAGE_WEB_PATH . "/interface/images/image_new.gif\" class=\"actionIcon\"> " .
	    	"Upload More Pictures</a> ";
		echo "<a href=\"" . MANAGE_WEB_PATH . "/photo-gallery/manage-pictures/" . $this->_galleryIdentity . "/\" " .
			(sizeof($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$this->_galleryIdentity]) == 0 ? "class=\"actionHidden\" " : "") .
			"id=\"picture-action-edit\"><img src=\"" . MANAGE_WEB_PATH .
			"/interface/images/action_go.gif\" class=\"actionIcon\" border=\"0\" /> Edit selected pictures</a> \n";
		echo "<span class=\"fauxLink" . (sizeof($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$this->_galleryIdentity]) == 0 ?
			" actionHidden" : "") . "\" onClick=\"ManageWindow('delete-content', 'photos-" . $this->_galleryIdentity .
			"');\" id=\"picture-action-delete\"><img src=\"" .
			MANAGE_WEB_PATH . "/interface/images/folder_delete.gif\" class=\"actionIcon\" border\"0\" /> Delete " .
			"selected photos</span></div>\n";
	  } else
	    echo "<div class=\"contentText separator\">This photo gallery has no pictures.<br /><a href=\"" . MANAGE_WEB_PATH .
	    	"/photo-gallery/upload-pictures/" . $this->_galleryIdentity . "/\">Upload pictures for this photo gallery</a></div>\n";
	}
}
?>