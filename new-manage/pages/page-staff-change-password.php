<?php
class GoavesManageStaffChangePassword {
	private $_changeAttempted = false;
	private $_changeFailedReason = null;
	private $_smartPasswordDetectEnabled = true;
	private $_forbiddenPasswords = array("default", "password");
	function processChangePassword($database)
	{
	  $this->_changeAttempted = true;
	  $current_password = md5($_POST["current-password"]);
	  if ($database->querySingle("SELECT password FROM staff WHERE identity='" .
	  	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' LIMIT 1") !== $current_password)
	  {
	    $this->_changeFailedReason = "Provided current password is not your current password.";
	    return;
	  }
	  
	  $new_password = $_POST["new-password"];
	  if (mb_strlen($new_password) < 6 || preg_match('/[0-9]/', $new_password) === 0)
	  {
	    $this->_changeFailedReason = "Passwords may not be less than six characters in length, and must contain at least one " .
	    	"(1) number.";
	    return;
	  }
	  
	  if ($this->_smartPasswordDetectEnabled)
	  {
	    $alpha_password = str_replace(array('4', '13', '1', '3', '5', '6', '7', '0'),
	 				  array('a', 'b', 'l', 'e', 's', 'b', 't', 'o'), $new_password);
	    if (mb_strtolower($new_password) == mb_strtolower($_SESSION[MANAGE_SESSION]["FIRST_NAME"]) ||
	    	mb_strtolower($new_password) == mb_strtolower($_SESSION[MANAGE_SESSION]["LAST_NAME"]))
	    {
	      $this->_changeFailedReason = "Passwords may not be set to your first or last name.";
	      return;
	    }
	    if (mb_strtolower($alpha_password) == mb_strtolower($_SESSION[MANAGE_SESSION]["FIRST_NAME"]) ||
	    	mb_strtolower($alpha_password) == mb_strtolower($_SESSION[MANAGE_SESSION]["LAST_NAME"]))
	    	{
	    	  $this->_changeFailedReason = "Passwords may not be set to your first or last name, even by replacing common characters " .
	    	  	"with numbers.";
	    	  return;
	    	}
	    foreach ($this->_forbiddenPasswords as $forbidden)
	      if (mb_strpos(mb_strtolower($new_password), $forbidden) !== false ||
	      	  mb_strpos(mb_strtolower($alpha_password), $forbidden) !== false)
	      	  {
	      	    $this->_changeFailedReason = "Passwords may not contain forbidden components (in this case, the component '" .
	      	    	$forbidden . "').";
	      	    return;
	      	  }
	  }
	  
	  $confirm_password = $_POST["confirm-password"];
	  if ($new_password !== $confirm_password)
	  {
	    $this->_changeFailedReason = "Your new password does not match your confirmation password.";
	    return;
	  }
	  
	  if (md5($new_password) === $current_password)
	  {
	    $this->_changeFailedReason = "Your new password is identical to your old password.";
	    return;
	  }
	  
	  if ($database->exec("UPDATE staff SET password='" . md5($new_password) . "' WHERE identity='" . 
	  	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'"))
	  	{
	  	  $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-staff-change-password", "SUCCESS" => true);
	  	  header ("Location: " . MANAGE_WEB_PATH . "/staff/");
	  	  return;
	  	}
	  $this->_changeFailedReason = "Password could not be changed within the database (programming error).";
	  return;
	  
	}
	function checkOrRedirect($path, $database)
	{
	  if (isset($_POST["current-password"]))
	    $this->processChangePassword($database);
	  return true;
	}
	function getPageHandle() { return null; }
	function getPageSubhandle() { return "change-password"; }
	function getPageTitle() { return "Change Password"; }
	function getBreadTrail() { return array("staff" => "My Account", "[this]" => "Change Password"); }
	function getPageStylesheet() { return null; }
	function getBodyOnload() { return ($this->_changeFailedReason !== null ? "fadeOutAndHide('change-failed', 2400);" : null); }
	function getPageJavascript()
	{
	  return "function processFormValid()
	  {
	    if (document.getElementById('current-password').value != '' &&
	    	document.getElementById('new-password').value.length >= 6 &&
	    	document.getElementById('confirm-password').value == document.getElementById('new-password').value)
	    	{
	    	  document.getElementById('submit-change-password').disabled = false;
	    	  return;
	    	}
	    document.getElementById('submit-change-password').disabled = true;
	  }";
	}
	function getPageContents()
	{
	  if ($this->_changeAttempted && $this->_changeFailedReason !== null)
	    echo "<div class=\"contentError separatorBottom\" id=\"change-failed\"><b>Error.</b> " . $this->_changeFailedReason .
	    	"</div>\n";
	    	
	  echo "<div class=\"contentHeader\">\n";
	  echo "  <div class=\"explainButton\" onClick=\"ManageWindow('explain','change-password');\"></div>\n";
	  echo "  Change Password\n";
	  echo "</div>\n";
	  echo "<form method=\"POST\" action=\"" . MANAGE_WEB_PATH . "/staff/change-password/\" " .
	  	"onSubmit=\"return validateChangePasswordAttempt();\">\n";
	  echo "  <div class=\"indentedContent\">\n";
	  echo "    <div class=\"contentSubheader separator\">Current password:</div>\n";
	  echo "    <input type=\"password\" name=\"current-password\" id=\"current-password\" class=\"textInput\" ".	
	  	"autocomplete=\"off\" onKeyUp=\"processFormValid();\" />\n";
	  echo "    <div class=\"contentSubheader separator\">New password:</div>\n";
	  echo "    <input type=\"password\" name=\"new-password\" id=\"new-password\" class=\"textInput\" " .
	  	"autocomplete=\"off\" onKeyUp=\"processFormValid();\" />\n";
	  echo "    <div class=\"contentError miniSeparator\" id=\"new-password-error\" style=\"display:none;\"></div>\n";
	  echo "    <div class=\"contentSubheader miniSeparator\">Confirm new password:</div>\n";
	  echo "    <input type=\"password\" name=\"confirm-password\" id=\"confirm-password\" class=\"textInput\" " .
	  	"autocomplete=\"off\" onKeyUp=\"processFormValid();\" />\n";
	  echo "    <div class=\"contentSubheader miniSeparator\ id=\"confirm-password-error\" style=\"display:none;\"></div>\n";
	  echo "    <center><input type=\"submit\" class=\"submitButton separator\" value=\"Change Password\" " .
	  	"disabled=\"disabled\" id=\"submit-change-password\" /></center>\n";
	  echo "  </div>\n";
	  
	  echo "</form>\n";
	}
}
?>