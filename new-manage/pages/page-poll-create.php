<?php
class GoavesManageInteractionsPollCreate {
	private $_relatedGroupTypes = array();
	private $_topics = array();
	function checkOrRedirect($path, $database)
	{
	  if (isset($path[2]) && strpos($path[2], "success-") === 0)
	  {
	    $supposed_success = $database->escapeString(str_replace("success-", "", $path[2]));
		if ($database->querySingle("SELECT count(*) FROM sound_slides WHERE slideshow_identity='" . $supposed_success . "' AND staff_identity='" .
			$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") > 0)
			{
			  $_SESSION[MANAGE_TRANSFER_DATA] = array("FROM" => "page-soundslides-create", "SUCCESS" => true, "IDENTITY" => $supposed_success);
			  header("Location: " . MANAGE_WEB_PATH . "/soundslide/manage/" . $supposed_success);
			  exit();
			}
	  }
	  $group_types = $database->query("SELECT type_identity, plural_name, handle FROM group_types ORDER BY type_identity ASC");
	  while ($group_type = $group_types->fetchArray())
	  {
	    $get_groups = $database->query("SELECT group_identity, title, active FROM groups WHERE type='" .
			$group_type["type_identity"] . "' ORDER BY title ASC");
		$groups_with_type = array();
		while ($group = $get_groups->fetchArray())
		  $groups_with_type[] = array("IDENTITY" => $group["group_identity"], "TITLE" => format_content($group["title"]),
			"ACTIVE" => ($group["active"] == "TRUE"));
	    $this->_relatedGroupTypes[$group_type["handle"]] = array("NAME" => $group_type["plural_name"],
			"GROUPS" => $groups_with_type);
	  }
	  
	  $topics = $database->query("SELECT topic_identity, title FROM topics WHERE (date_start <= '" .
		date("Y-m-d", strtotime("+1 hours")) . "' AND date_end >= '" . date("Y-m-d", strtotime("+1 hours")) . "') OR (" .
		"date_start IS NULL AND date_end IS NULL) ORDER BY title ASC");
	  while ($topic = $topics->fetchArray())
	    $this->_topics[] = array("IDENTITY" => $topic["topic_identity"], "TITLE" => format_content($topic["title"]));
	  return true;
	}
	function getRequirePermission() { return PERM_INTERACTIONS; }
	function getPageHandle() { return "interactions"; }
	function getPageSubhandle() { return "poll-create"; }
	function getPageTitle() { return "Create New Poll"; }
	function getBreadTrail() { return array("interactions" => "Interactions",
		"interactions/list" => "Interactions Listing", "[this]" => "Create New Poll"); }
	function getPageStylesheet() { return "stylesheet-interactions.css"; }
	function getPageJavascript()
	{
	  return "var numberOfOptions = 0;
	  function createPollOption()
	  {
	    var optionContainer = document.createElement('div');
		optionContainer.className = 'pollOption';
		optionContainer.setAttribute('id', 'poll-option-container-' + numberOfOptions);
		var optionText = document.createElement('input');
		optionText.setAttribute('type','text');
		optionText.setAttribute('id','poll-option-' + numberOfOptions);
		optionText.className = 'inputText';
		optionText.setAttribute('onKeyDown','processOptionInput(event, ' + numberOfOptions + ');');
		optionText.setAttribute('onKeyUp','validateOption(' + numberOfOptions + ');');
		var optionTextError = document.createElement('div');
		optionTextError.setAttribute('id', 'poll-option-error-' + numberOfOptions);
		optionTextError.className = 'contentError';
		optionTextError.style.display = 'none';
		optionContainer.appendChild(optionText);
		optionContainer.appendChild(optionTextError);
		document.getElementById('poll-options-container').appendChild(optionContainer);
		numberOfOptions++;
	  }
	  function processOptionInput(e, option_identity)
	  {
	    var event = (e ? e : event);
	    if ((event ? event.which : event.keyCode) != 9)
		  return true;
		if (event.shiftKey)
		  return false;
		if (option_identity + 1 != numberOfOptions)
		  return false;
		createPollOption();
	  }
	  function validateOption(option_identity)
	  {
	    document.getElementById('poll-create').disabled = true;
		var option_text = document.getElementById('poll-option-' + option_identity).value.replace(/ /g, '').toLowerCase();
		if (option_text.length == 0)
		  return;
		for (var option_index = 0; option_index < numberOfOptions; option_index++)
		  if (option_index != option_identity && document.getElementById('poll-option-' + option_index).value.replace(/ /g, '').toLowerCase() == option_text)
		  {
		    document.getElementById('poll-option-error-' + option_identity).innerHTML = 'Another response already contains this same text.';
			document.getElementById('poll-option-error-' + option_identity).style.display = 'block';
			document.getElementById('poll-option-' + option_identity).className = 'inputText formComponentInvalid';
			checkFormValid();
			return;
		  }
		document.getElementById('poll-option-error-' + option_identity).style.display = 'none';
		document.getElementById('poll-option-' + option_identity).className = 'inputText formComponentValid';
		checkFormValid();
	  }
	  function checkFormValid()
	  {
	    document.getElementById('poll-create').disabled = (document.getElementById('poll-question').className != 'inputText formComponentValid' ||
			document.getElementById('poll-start-timestamp').value.length == 0 || document.getElementById('poll-end-timestamp').value.length == 0 ||
			document.getElementById('poll-end-timestamp').value < document.getElementById('poll-start-timestamp').value || numberOfOptions == 0);
		if (document.getElementById('poll-create').disabled)
		  return;
		for (var option_index = 0; option_index < numberOfOptions; option_index++)
		  if (document.getElementById('poll-option-' + option_index).className == 'inputText formComponentInvalid')
		  {
		    document.getElementById('poll-create').disabled = true;
			return;
		  }
	  }
	  function createPoll()
	  {
	    if (document.getElementById('poll-create').disabled)
		  return;
		var createCall = web_path + '/components/create-poll.php?question=' + escapeStringURL(document.getElementById('poll-question').value) + 
			'&start=' + escapeStringURL(document.getElementById('poll-start-timestamp').value) + '&end=' + escapeStringURL(document.getElementById('poll-end-timestamp').value);
		for (var option_index = 0; option_index < numberOfOptions; option_index++)
		  createCall += '&option_' + option_index + '=' + escapeStringURL(document.getElementById('poll-option-' + option_index).value);
		executeAJAX(createCall, function processCreate(result)
		{
		  if (result.indexOf('[Error]') < 0)
		  {
		    window.location = '" . MANAGE_WEB_PATH . "/interactions/poll-manage/' + result + '/';
			return;
		  }
		  alert(result.replace('[Error] ', ''));
		});
	  }";
	}
	function getPageContents()
	{
	  echo "<div class=\"contentHeader\">Create New Poll</div>\n";
	  
	  echo "<div class=\"createForm noWindow\">\n";
	  echo "  <div class=\"formLabel separator\">Question:</div>\n";
	  echo "  <input type=\"text\" class=\"inputText\" id=\"poll-question\" autocomplete=\"off\" onKeyUp=\"executeAJAX(web_path + '/components/" .
		"check-title.php?type=poll&minimum=6&title=' + encodeURIComponent(this.value), function " .
		"processTitle(return_value)
		{
		  if (return_value.indexOf('[Error]') >= 0)
		  {
		    document.getElementById('poll-question-error').innerHTML = return_value.replace('[Error] ', '');
			document.getElementById('poll-question-error').style.display = 'block';
			document.getElementById('poll-question').className = 'inputText formComponentInvalid';
		  } else
		  {
		    document.getElementById('poll-question-error').style.display = 'none';
			document.getElementById('poll-question').className = 'inputText formComponentValid';
		  }
		  checkFormValid();
		});\" />\n";
	  echo "  <div class=\"contentError\" id=\"poll-question-error\" style=\"display:none;\"></div>\n";
	  
	  require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");
	  echo "  <div class=\"formLabel separator\"><b>Begins:</b></div>";
	  FormDate("poll-start", null, "checkFormValid");
	  echo "  <div class=\"formLabel\"><b>Ends:</b></div>";
	  FormDate("poll-end", null, "checkFormValid");
	  
	  echo "  <div class=\"formLabel separator\"><b>Options:</b></div>";
	  echo "  <div id=\"poll-options-container\" class=\"pollOptionsContainer\">\n";
	  echo "  </div>\n";
	  echo "  <input type=\"button\" class=\"newPollOption\" onClick=\"createPollOption();\" />\n";
	  
	  echo "  <center><input type=\"button\" class=\"submitButton\" id=\"poll-create\" value=\"Create\" disabled=\"disabled\" " .
		"onClick=\"createPoll();\" /></center>\n";
	  echo "</div>\n";
	}
}
?>