<?php
class GoavesManageSpecialList {
	private $_specials = array();
	function checkOrRedirect($path, $database)
	{
	  $all_specials = $database->query("SELECT special_identity, title, active, ended FROM specials ORDER BY active DESC, title ASC");
	  while ($special = $all_specials->fetchArray())
	    $this->_specials[] = array("IDENTITY" => $special["special_identity"], "TITLE" => format_content($special["title"]),
			"ACTIVE" => ($special["active"] == "TRUE"), "ENDED" => ($special["active"] == "TRUE" ? null : strtotime($special["ended"])));
	  return true;
	}
	function getRequirePermission() { return PERM_SPECIALS; }
	function getPageHandle() { return "special"; }
	function getPageSubhandle() { return "list"; }
	function getPageTitle() { return "Specials Listing"; }
	function getBreadTrail() { return array("[this]" => "Specials Listing"); }
	function getPageStylesheet() { return "stylesheet-specials.css"; }
	function getBodyOnload()
	{
	  return null;
	}
	function getPageContents()
	{
	  if (sizeof($this->_specials) == 0)
	  {
	    echo "<div class=\"contentText\">There are no specials in the database.</div>\n";
		return;
	  }
	  echo "<div class=\"contentText\">Select a special from the list below.</div>\n";
	  echo "<div class=\"specialListingContainer\">\n";
	  $previous_special_active = null;
	  foreach ($this->_specials as $special)
	  {
	    if ($previous_special_active != $special["ACTIVE"])
		{
		  if ($previous_special_active != null)
		    echo "  </div>\n";
		  echo "  <div class=\"specialContainer" . ($previous_special_active != null ? " separator" : "") . "\">\n";
		  echo "    <div class=\"title\">" . ($special["ACTIVE"] ? "Active" : "Ended") . " Specials</div>\n";
		  $previous_special_active = $special["ACTIVE"];
		}
		echo "  <a href=\"" . MANAGE_WEB_PATH . "/special/manage/" . $special["IDENTITY"] . "/\" class=\"item\">" . $special["TITLE"] . 
			($special["ENDED"] != null ? " <span>(Ended " . date(DATE_FORMAT, $special["ENDED"]) . ")</span>" : "") . "</a>\n";
	  }
	  echo "  </div>\n";
	  echo "</div>\n";
	}
}
?>