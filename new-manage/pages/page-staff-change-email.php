<?php
class GoavesManageStaffChangeEmail {
	private $_currentEmail;
	private $_changeAttempted = false;
	private $_changeFailedReason = null;
	function createRequestHandler()
	{
	  $handler = "";
	  for ($index = 0; $index < 20; $index++)
	    $handler .= dechex(mt_rand(1, 16));
	  return $handler;
	}
	function processEmailChange($database)
	{
	  $this->_changeAttempted = true;
	  $new_email = $_POST["new-email"];
	  if (mb_strlen($new_email) == 0 || !mb_strpos($new_email, "@") || !mb_strpos($new_email, ".", mb_strpos($new_email, "@")))
	  {
	    $this->_changeFailedReason = "E-mail did not contain the proper formatting.";
	    return;
	  }
	  if ($database->querySingle("SELECT count(*) FROM staff_email_changes WHERE staff_identity='" .
	  	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") > 0)
	  	{
	  	  $this->_changeFailedReason = "You already have an e-mail change open. Please confirm or cancel that change before " .
	  	  	"attempting to make another.";
	  	  return;
	  	}
	  $request_handler = $this->createRequestHandler();
	  while ($database->querySingle("SELECT count(*) FROM staff_email_changes WHERE request_handler LIKE '" .
	  	$database->escapeString($request_handler) . "'") > 0)
	  	$request_handler = $this->createRequestHandler();
	
	  require_once (DOCUMENT_ROOT . "/config/class.phpmailer.php");
	  $conf_mail = new PHPMailer(true);
	  $conf_mail->IsSMTP();
	  $conf_mail->Host = "smtp.gmail.com";
	  $conf_mail->SMTPDebug = 2;
	  $conf_mail->SMTPAuth = true;
	  $conf_mail->SMTPSecure = "tls";
	  $conf_mail->Host = "smtp.gmail.com";
	  $conf_mail->Port = 587;
	  $conf_mail->Username = "jacob.deitloff@gmail.com";
	  //$conf_mail->Password = "";
	  $conf_mail->SetFrom('jacob.deitloff@gmail.com', 'Jacob Deitloff');
	  $conf_mail->AddAddress($new_email, $new_email);
	  $conf_mail->Subject = "Goaves.com E-mail Change Confirmation";
	  $confirmation_message = "This e-mail has been listed as the new contact e-mail for " . $_SESSION[MANAGE_SESSION]["FIRST_NAME"] .
	  	" " . $_SESSION[MANAGE_SESSION]["LAST_NAME"] . " on Goaves.com.\n\n";
	  $confirmation_message .= "If this is correct, please go to <a href=\"" . MANAGE_WEB_PATH . "/confirm-email/" .
	  	$request_handler . "/\">" . MANAGE_WEB_PATH . "/confirm-email/" . $request_handler . "/</a> and log in to confirm " .
	  	"the change.\n\n";
	  $confirmation_message .= "If this is incorrect, please ignore this message or go to <a href=\"" . MANAGE_WEB_PATH .
	  	"/cancel-email/" . $request_handler . "/\">" . MANAGE_WEB_PATH . "/cancel-email/" . $request_handler . "/</a> to " .
	  	"cancel the change, if it has not already been done.\n\n";
	  $confirmation_message .= "Jacob Deitloff\n";
	  $confirmation_message .= "<a href=\"" . WEB_PATH . "/\">www.Goaves.com</a> webmaster";
	  $conf_mail->MsgHTML($confirmation_message);

	  if (!$conf_mail->Send())
	  {
	    $this->_changeFailedReason = "Unable to send the confirmation e-mail to the specified address.";
	    return;
	  }
	  if (!$database->exec("INSERT INTO staff_email_changes(staff_identity, new_email, date_requested, request_handler) VALUES('" .
	  	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "','" . $database->escapeString($new_email) .
	  	"','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "','" . $request_handler . "')"))
	  	{
	  	  $this->_changeFailedReason = "Unable to insert the change request into the database.";
	  	  return;
	  	}
	  	
	}
	function checkOrRedirect($path, $database)
	{
	  $this->_currentEmail = $database->querySingle("SELECT email FROM staff WHERE identity='" .
	  	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' LIMIT 1");
	  if ($this->_currentEmail == "")
	    $this->_currentEmail = null;
	  if (isset($_POST["new-email"]))
	    $this->processEmailChange($database);
	  return true;
	}
	function getPageHandle() { return null; }
	function getPageSubhandle() { return "change-email"; }
	function getPageTitle() { return "Change E-mail"; }
	function getBreadTrail() { return array("staff" => "My Account", "[this]" => "Change E-mail"); }
	function getPageStylesheet() { return "stylesheet-staff.css"; }
	function getBodyOnload() { return ($this->_changeFailedReason !== null ? "fadeOutAndHide('change-failed', 2400);" : null); }
	function getPageJavascript()
	{
	  return "function processFormValid()
	  {
	    if (document.getElementById('new-email').value.length > 0 &&
	    	document.getElementById('new-email').value.indexOf('@') > 0 &&
	    	document.getElementById('new-email').value.indexOf('.', document.getElementById('new-email').value.indexOf('@')) > 0)
	    	{
	    	  document.getElementById('submit-change-email').disabled = false;
	    	  return;
	    	}
	    document.getElementById('submit-change-email').disabled = true;
	  }";
	}
	function getPageContents()
	{
	  if ($this->_changeAttempted && $this->_changeFailedReason !== null)
	    echo "<div class=\"contentError separatorBottom\" id=\"change-failed\"><b>Error.</b> " . $this->_changeFailedReason .
	    	"</div>\n";
	    	
	  echo "<div class=\"contentHeader\">\n";
	  echo "  <div class=\"explainButton\" onClick=\"ManageWindow('explain','change-email');\"></div>\n";
	  echo "  Change E-mail\n";
	  echo "</div>\n";
	  
	  echo "<form method=\"POST\" action=\"" . MANAGE_WEB_PATH . "/staff/change-email/\">\n";
	  echo "  <div class=\"indentedContent\">\n";
	  echo "    <div class=\"contentSubheader separator\">Current e-mail:</div>\n";
	  echo "    <div class=\"contentLine " . ($this->_currentEmail === null ? "emailNone" : "emailSet") .
	  	"\">" . ($this->_currentEmail === null ? "(None set)" : $this->_currentEmail) . "</div>\n";
	  echo "    <div class=\"contentSubheader separator\">New email:</div>\n";
	  echo "    <input type=\"text\" name=\"new-email\" id=\"new-email\" class=\"textInput\" " .
	  	"autocomplete=\"off\" onKeyUp=\"processFormValid();\" />\n";
	  echo "    <center><input type=\"submit\" class=\"submitButton separator\" value=\"Change E-mail\" " .
	  	"disabled=\"disabled\" id=\"submit-change-email\" /></center>\n";
	  echo "  </div>\n";
	  
	  echo "</form>\n";
	}
}
?>