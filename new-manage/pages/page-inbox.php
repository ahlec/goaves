<?php
class GoavesManageInbox {
	private $_messages = array();
	private $_messagesPerPage = 30;
	private $_loadMessageFail = null;
	function checkOrRedirect($path, $database)
	{
	  if (isset($_SESSION[MANAGE_TRANSFER_DATA]))
	  {
	    switch ($_SESSION[MANAGE_TRANSFER_DATA]["FROM"])
		{
		  case "page-inbox-message":
		  {
		    $this->_loadMessageFail = true;
		    break;
		  }
		}
	    unset ($_SESSION[MANAGE_TRANSFER_DATA]);
	  }
	  if (isset($path[1]) && strtolower($path[1]) == "message" && $this->_loadMessageFail !== true)
	    return new GoavesManageInboxMessage();
	  $get_messages = $database->query("SELECT message_identity, sender_identity, staff.first_name as \"sender_first_name\", staff.last_name AS \"sender_last_name\", " .
		"time_sent, message, read_yet FROM staff_messages JOIN staff ON staff.identity = staff_messages.sender_identity WHERE recipient_identity='" .
		$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' ORDER BY time_sent DESC LIMIT " . $this->_messagesPerPage);
	  while ($message = $get_messages->fetchArray())
	  {
	    $decoded_message = utf8_encode(decodeMailMessage($message["message"]));
	    $message_blurb = get_smart_blurb($decoded_message, 100);
		if ($message_blurb != format_content($decoded_message))
		  $message_blurb .= "...";
	    $this->_messages[] = array("IDENTITY" => $message["message_identity"], "SENDER" => $message["sender_identity"],
			"SENDER_NAME" => ($message["sender_first_name"] . " " . $message["sender_last_name"]), 
			"TIME_SENT" => strtotime($message["time_sent"]), "READ_YET" => ($message["read_yet"] == "TRUE"),
			"MESSAGE_BLURB" => $message_blurb);
	  }
	  return true;
	}
	function getPageHandle() { return "mail"; }
	function getPageSubhandle() { return "inbox"; }
	function getPageTitle() { return "Staff Mail - Inbox"; }
	function getBreadTrail() { return array("mail" => "Mail", "[this]" => "Inbox"); }
	function getPageStylesheet() { return "stylesheet-inbox.css"; }
	function getBodyOnload() { return ($this->_loadMessageFail != null ? "fadeOutAndHide('message-load-fail', 3000);" : null); }
	function getPageContents()
	{
	  if ($this->_loadMessageFail != null)
	    echo "<div class=\"contentError separator separatorBottom\" id=\"message-load-fail\"><b>Message read failed.</b> The message you attempted to " .
			"load does not exist, or you are not an involved party in the message.</div>\n";
	  echo "<span class=\"fauxLink\" onClick=\"ManageWindow('compose-message');\">Compose</span>\n";
	  echo "<div class=\"inboxContainer\">\n";
	  foreach ($this->_messages as $message)
	  {
	    echo "  <a href=\"#\" id=\"message-" . $message["IDENTITY"] . "\" onClick=\"ManageWindow('mail-message','" . $message["IDENTITY"] .
			"');\" class=\"inboxMessage " . //"');\">\n";//href=\"" . MANAGE_WEB_PATH . "/mail/message/" . $message["IDENTITY"] . "/\" class=\"inboxMessage " .
			(!$message["READ_YET"] ? "unread" : "read") . "\">\n";
	    echo "    <div class=\"header\"><b>From:</b> " . $message["SENDER_NAME"] . " - <b>Sent:</b> " . date(LONG_DATETIME_FORMAT, $message["TIME_SENT"]) . "</div>\n";
		echo "    <div class=\"blurb\">" . $message["MESSAGE_BLURB"] . "</div>\n";
		echo "  </a>\n";
	  }
	  echo "</div>\n";
	}
}
?>