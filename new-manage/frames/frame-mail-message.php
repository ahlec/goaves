<?php
if (isset($_GET["parameters"]))
  $message_identity = $_GET["parameters"];
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to load this frame, you must be authenticated to the management panel.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'400px',\n";
  echo "  height:'200px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px'\n";
  echo "}}\n";
  exit();
}

$staff_identity = $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]);
if ($database->querySingle("SELECT count(*) FROM staff_messages WHERE message_identity='" . $database->escapeString($message_identity) .
	"' AND (recipient_identity='" . $staff_identity . "' OR sender_identity='" . $staff_identity . "')") > 0)
{
  echo "<div class=\"windowHeader\">Message</div>\n";
	$info = $database->querySingle("SELECT message_identity, sender_identity, recipient_identity, staff.first_name as \"sender_first_name\", staff.last_name AS \"sender_last_name\", " .
		"time_sent, message, read_yet FROM staff_messages LEFT JOIN staff ON staff.identity = staff_messages.sender_identity WHERE message_identity='" .
		$database->escapeString($message_identity) . "' LIMIT 1", true);
	if (is_numeric($info["sender_identity"]))
	  $sender = array("IDENTITY" => $info["sender_identity"], "FIRST_NAME" => format_content($info["sender_first_name"]), "LAST_NAME" => format_content($info["sender_last_name"]));
	else
	  $sender = format_content($info["sender_identity"]);
	if (is_numeric($info["recipient_identity"]))
	{
	  $recipient = $database->querySingle("SELECT first_name, last_name FROM staff WHERE identity='" . $info["recipient_identity"] . "' LIMIT 1", true);
	  $recipient = array("IDENTITY" => $info["recipient_identity"], "FIRST_NAME" => format_content($recipient["first_name"]), "LAST_NAME" => format_content($recipient["last_name"]));
	} else
		$recipient = format_content($info["recipient_identity"]);
	
	echo "<div class=\"messageContainer\">\n";
	echo "  <div class=\"header\">\n";
	echo "  <" . (is_array($sender) ? "a href=\"" . MANAGE_WEB_PATH . "/staff/" . $sender["IDENTITY"] . "/\" target=\"_blank\"" :
		"div") . " class=\"participant\"><b>Sender:</b> " . (is_array($sender) ?
		$sender["FIRST_NAME"] . " " . $sender["LAST_NAME"] : $sender) . "</" . (is_array($sender) ? "a" : "div") . ">\n";
	echo "  <" . (is_array($recipient) ? "a href=\"" . MANAGE_WEB_PATH . "/staff/" . $recipient["IDENTITY"] . "/\" target=\"_blank\"" :
		"div") . " class=\"participant\"><b>Recipient:</b> " . (is_array($recipient) ?
		$recipient["FIRST_NAME"] . " " . $recipient["LAST_NAME"] : $recipient) . "</" . (is_array($recipient) ? "a" :"div") . ">\n";
	echo "    <div class=\"time\"><b>Sent:</b> " . date(LONG_DATETIME_FORMAT, strtotime($info["time_sent"])) . "</div>\n";
	echo "  </div>\n";
	echo "  <div class=\"message\">" . decodeMailMessage($info["message"]) . "</div>\n";
	echo "</div>\n";
	  
	if ($info["read_yet"] != "TRUE")
	{
	  $database->exec("UPDATE staff_messages SET read_yet='TRUE' WHERE message_identity='" . $info["message_identity"] . "'");
	  echo "<script language=\"JavaScript\">document.getElementById('message-" . $info["message_identity"] . "').className = 'inboxMessage read';</script>\n";
	}
} else
{
  echo "<div class=\"windowHeader\">Mail Message</div>\n";
  echo "The message you attempted to load either no longer exists, or you are not a participating party to the message.\n";
}
?>