<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to load this frame, you must be authenticated to the management panel.");
if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'300px',\n";
  echo "  height:'150px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px',\n";
  echo "  hasCloseButton:false\n";
  echo "}}\n";
  exit();
}
echo "<div class=\"inProcessText\" id=\"processFrameDisplay\">In Process...</div>\n";
?>