<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_INTERACTIONS, 1) < "1")
  exit ("In order to load this frame, you must be authenticated to the management panel and have permissions to manage interactions.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'300px',\n";
  echo "  height:'150px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px'\n";
  echo "}}\n";
  exit();
}

echo "<div class=\"windowHeader\">Add poll option</div>\n";
	
echo "<div class=\"createForm noWindow\">\n";
echo "  <div class=\"infoLine\"><b>Value:</b> <input type=\"text\" class=\"inputText\" id=\"add-poll-option-text\" " .
	"onKeyUp=\"executeAJAX(manage_web_path + '/components/check-poll-response.php?poll_identity=" . $_GET["parameters"] . "&value=' +
		escapeStringURL(this.value), function process(result)
		{
		  if (result.indexOf('[Error] ') < 0)
		  {
		    document.getElementById('add-poll-option-text').className = 'inputText formComponentValid';
			document.getElementById('confirm-add').disabled = false;
			document.getElementById('add-poll-option-error').style.display = 'none';
		  } else
		  {
		    document.getElementById('add-poll-option-text').className = 'inputText formComponentInvalid';
			document.getElementById('confirm-add').disabled = true;
			document.getElementById('add-poll-option-error').innerHTML = result.replace('[Error] ', '');
			document.getElementById('add-poll-option-error').style.display = 'block';
		  }
		});\" /></div>\n";
echo "  <div class=\"contentError\" id=\"add-poll-option-error\" style=\"display:none;\"></div>\n";
echo "  <div style=\"text-align:center;margin-top:10px;\">\n";
echo "    <input type=\"button\" class=\"inputButton buttonGreen\" value=\"Add\" disabled=\"disabled\" id=\"confirm-add\" " .
	"onClick=\"executeAJAX(manage_web_path + '/components/add-poll-response.php?poll_identity=" . $_GET["parameters"] . "&response=' +
		escapeStringURL(document.getElementById('add-poll-option-text').value), function process(result)
		{
			if (result == 'success')
			{
			  window.location = manage_web_path + '/interactions/poll-manage/" . $_GET["parameters"] . "/';
			  return;
			} else
			  alert(result.replace('[Error] ', ''));
		});\" />\n";
echo "    <input type=\"button\" class=\"inputButton buttonRed\" value=\"Cancel\" id=\"cancel-add\" " .
	"onClick=\"this.disabled=true;document.getElementById('confirm-add').disabled=true;openWindow.closeWindow();\" />\n";
echo "  </div>\n";
echo "</div>\n";

?>