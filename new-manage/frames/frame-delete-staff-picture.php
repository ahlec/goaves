<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to load this frame, you must be authenticated to the management panel.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'350px',\n";
  echo "  height:'150px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px',\n";
  echo "  closeButton:'false'\n";
  echo "}}\n";
  exit();
}

if ($database->querySingle("SELECT count(*) FROM staff WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) ."' AND image_file<>'" .
	"no-staff-picture.jpg'") == 0)
	exit ("You do not have a staff picture uploaded, so you cannot delete your picture.");
	
echo "<div class=\"windowHeader\">Delete Staff Picture?</div>\n";
$current_image = $database->querySingle("SELECT image_file FROM staff WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' LIMIT 1");
echo "<div class=\"frameDeleteCurrentPicture\"><img src=\"" . WEB_PATH . "/images/staff/" . $current_image . "\" /><br />(Current)</div>\n";
echo "<div class=\"frameDeleteNoPicture\"><img src=\"" . WEB_PATH . "/images/staff/no-staff-picture.jpg\" /><br />(None)</div>\n";
echo "<div class=\"contentText separatorBottom\">Are you sure you wish to reset your current staff picture?</div>\n";

echo "<center>\n";
echo "  <input type=\"button\" class=\"inputButton buttonGreen\" value=\"Delete\" id=\"confirm-delete\" onClick=\"this.disabled=true;" .
	"document.getElementById('cancel-delete').disabled=true;executeAJAX('" . MANAGE_WEB_PATH .
	"/components/delete-staff-picture.php', function finish(result)
	{
	  if (result == 'success')
	  {
	    window.location = '" . MANAGE_WEB_PATH . "/staff/change-picture/';
	    return;
	  }
	  alert(result);
	  document.getElementById('cancel-delete').disabled = false;
	  document.getElementById('confirm-delete').disabled = false;
	});\" />\n";
echo "  <input type=\"button\" class=\"inputButton buttonRed\" value=\"Cancel\" id=\"cancel-delete\" " .
	"onClick=\"this.disabled=true;document.getElementById('confirm-delete').disabled=true;openWindow.closeWindow();\" />\n";
echo "</center>\n";
?>