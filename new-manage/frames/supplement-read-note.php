<?php
if (!isset($_SESSION))
  @session_start();
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("You must be authenticated to the management panel in order to load this frame supplement.");
if (!isset($_GET["note_identity"]))
  exit ("Note identity must be present in order to mark the note as being read.");
$note_identity = $database->escapeString($_GET["note_identity"]);
if ($database->querySingle("SELECT count(*) FROM manage_notes WHERE note_identity='" . $note_identity . "'") == 0)
  exit ("Note must be an existing note within the database.");

$read_success = false;
if ($database->querySingle("SELECT count(*) FROM manage_note_views WHERE note_identity='" . $note_identity .
 	"' AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
  	$read_success = $database->exec("INSERT INTO manage_note_views(note_identity, staff_identity, view_date) VALUES('" .
  		$note_identity . "','" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) .
  		"','" . date("Y-m-d H:i:s") . "')");
exit ($read_success ? "success" : "error");
?>