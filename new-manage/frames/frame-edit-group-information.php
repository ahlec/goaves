<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GROUPS, 1) < "1")
  exit ("In order to load this frame, you must be authenticated to the management panel and have the permission to manage groups.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'550px',\n";
  echo "  height:'550px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px'\n";
  //echo "  onCloseButtonClick:'if (tinyMCE.get(\"group-description\") != \"undefined\") { tinyMCE.execCommand(\"mceRemoveControl\", false, \"group-description\"); alert(\"hi\"); }'\n";
  echo "}}\n";
  exit();
}

if (!isset($_GET["parameters"]) || $_GET["parameters"] === false)
  exit ("[Error] Parameters must be passed.");
if ($database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $database->escapeString($_GET["parameters"]) ."'") == 0)
	exit ("The requested group does not exist within the database.");
$group_information = $database->querySingle("SELECT group_identity, title, description, mission_statement, type, active, years_active FROM groups WHERE group_identity='" .
	$database->escapeString($_GET["parameters"]) . "' LIMIT 1", true);
	
echo "<div class=\"windowHeader\">Edit Group Information</div>\n";
echo "<div class=\"createForm noWindow\">\n";

echo "<script>\n";
echo "checkEditInformationForm = function() {
		if (document.getElementById('group-title').className == 'inputText formComponentInvalid' ||
		(document.getElementById('group-active').checked && (document.getElementById('group-years-active').value.length == 0 ||
		document.getElementById('group-years-active').value != parseInt(document.getElementById('group-years-active').value))))
		{
		  document.getElementById('confirm-edit').disabled = true;
		  return;
		}
		document.getElementById('confirm-edit').disabled = false;
	};
	editGroup = function() {
		if (document.getElementById('confirm-edit').disabled)
		  return;
		document.getElementById('confirm-edit').disabled = true;
		document.getElementById('cancel-edit').disabled = true;
		var editCall = manage_web_path + '/components/update-group.php?identity=" . $group_information["group_identity"] . "';
		editCall += '&title=' + escapeStringURL(document.getElementById('group-title').value);
		editCall += '&type=' + document.getElementById('group-type').value;
		editCall += '&active=' + (document.getElementById('group-active').checked ? 'true' : 'false');
		if (document.getElementById('group-active').checked)
		  editCall += '&years_active=' + document.getElementById('group-years-active').value;
		editCall += '&description=' + escapeStringURL(tinyMCE.get('group-description').getContent());
		editCall += '&mission_statement=' + escapeStringURL(tinyMCE.get('group-mission-statement').getContent());
		executeAJAX(editCall, function process(result)
			{
			  if (result == 'success')
			  {
			    window.location = manage_web_path + '/group/manage/" . $group_information["group_identity"] . "/';
				return;
			  } else
			  {
			    alert(result.replace('[Error] ', ''));
				document.getElementById('confirm-edit').disabled = false;
				document.getElementById('cancel-edit').disabled = false;
			  }
			});
	};\n";
echo "</script>\n";

/* Name */
echo "  <div class=\"infoLine\"><b>Title:</b> " . format_content($group_information["title"]) . "</div>\n";
echo "  <input type=\"text\" class=\"inputText formComponentValid\" name=\"group-title\" id=\"group-title\" autocomplete=\"off\" onKeyUp=\"executeAJAX(web_path + '/components/" .
	"check-title.php?type=group&minimum=6&title=' + escapeStringURL(this.value) + '&identity=' + " . $group_information["group_identity"] . ", function " .
	"processTitle(return_value) {
	  if (return_value.indexOf('[Error]') >= 0)
	  {
	    document.getElementById('group-title-error').innerHTML = return_value.replace('[Error] ', '');
		if (document.getElementById('group-title-error').isFading != true)
		{
	      document.getElementById('group-title-error').style.display = 'block';
		  fadeOutAndHide('group-title-error', 2400);
	    }
		document.getElementById('group-title').className = 'inputText formComponentInvalid';
		document.getElementById('confirm-edit').disabled = true;
	  } else
	  {
	    document.getElementById('group-title-error').style.display = 'none';
	    document.getElementById('group-title').className = 'inputText formComponentValid';
		checkEditInformationForm();
	  } });\" value=\"" . addslashes(format_content($group_information["title"])) . "\" />\n";
echo "  <div class=\"contentError\" id=\"group-title-error\" style=\"display:none;\"></div>\n";

/* Group Type */
$get_group_types = $database->query("SELECT type_identity, singular_name FROM group_types");
echo "  <div class=\"infoLine separator\"><b>Type:</b> <select id=\"group-type\" onChange=\"checkEditInformationForm();\">";
while ($group_type = $get_group_types->fetchArray())
  echo "<option value=\"" . $group_type["type_identity"] . "\"" . ($group_type["type_identity"] == $group_information["type"] ? " selected=\"selected\"" : "") .
	">" . format_content($group_type["singular_name"]) . "</option>";
echo "</select></div>\n";

/* Active */
echo "  <div class=\"infoLine separator\"><b>Status:</b> <input type=\"checkbox\" class=\"inputCheckbox\" id=\"group-active\"" .
	($group_information["active"] == "TRUE" ? " checked=\"checked\"" : "") .
	" onClick=\"document.getElementById('years-active-line').style.display = (this.value ? 'block' : 'none');checkEditInformationForm();\" /> " .
	"<label for=\"group-active\">Currently active</label></div>\n";
echo "  <div class=\"infoLine\" id=\"years-active-line\" " . ($group_information["active"] != "TRUE" ? "style=\"display:none;\" " : "") . 
	"><b>Consecutive years active:</b> <input type=\"text\" class=\"inputText formComponentValid\" id=\"group-years-active\" autocomplete=\"off\" value=\"" .
	$group_information["years_active"] . "\" onKeyUp=\"checkEditInformationForm();\" /></div>\n";

/* Description */
echo "  <div class=\"infoLine separator\"><b>Description:</b></div>\n";
echo "  <textarea id=\"group-description\">" . format_content($group_information["description"]) . "</textarea>\n";
echo "  <script>\n";
echo '    tinyMCE.init({
            mode : "exact",
            elements : "group-description",
            entity_encoding : "named",
            theme : "advanced",
            skin : "o2k7",
            editor_selector : "advanced-textarea",
            plugins : "safari,spellchecker,pagebreak,style,layer,save,advhr,advimage,advlink," +
	    	"iespell,inlinepopups,preview,media,searchreplace,print,contextmenu,paste,directionality," +
	    	"fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
            theme_advanced_buttons1 : "cut,copy,paste,pastetext,|,search,replace,|,undo,redo,|,cleanup,code,|," +
	    	"preview,|,fullscreen,removeformat,visualaid,|,styleprops,spellchecker,|,cite,abbr,acronym,del,inc,attribs,",
            theme_advanced_buttons2 : "bold,italic,underline,strikethrough,|,forecolor,link,unlink,|,sub,sup,|,charmap," +
	    	"iespell,|,bullist,numlist,|,outdent,indent",
	    theme_advanced_buttons3 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : false,
            template_external_list_url : "js/template_list.js",
            external_link_list_url : "js/link_list.js",
            remove_trailing_nbsp : true,
            height : "100"
          });';
echo "\n  </script>\n";

/* Mission Statement */
echo "  <div class=\"infoLine separator\"><b>Mission Statement:</b></div>\n";
echo "  <textarea id=\"group-mission-statement\" style=\"width:100%;\">" .
	format_content($group_information["mission_statement"]) . "</textarea>\n";
echo "  <script>\n";
echo "    tinyMCE.init({
            mode : \"exact\",
			elements : \"group-mission-statement\",
            entity_encoding : \"named\",
            theme : \"simple\",
            skin : \"o2k7\",
            remove_trailing_nbsp : true,
            height : '100'});\n";
echo "  </script>\n";	

echo "  <div style=\"text-align:center;margin-top:10px;\">\n";
echo "    <input type=\"button\" class=\"inputButton buttonGreen\" value=\"Save\" disabled=\"disabled\" id=\"confirm-edit\" onClick=\"editGroup();\" />\n";
echo "    <input type=\"button\" class=\"inputButton buttonRed\" value=\"Cancel\" id=\"cancel-edit\" " .
	"onClick=\"this.disabled=true;document.getElementById('confirm-edit').disabled=true;openWindow.closeWindow();\" />\n";
echo "  </div>\n";
echo "</div>\n";
?>