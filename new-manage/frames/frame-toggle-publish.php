<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to load this frame, you must be authenticated to the management panel.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'400px',\n";
  echo "  height:'200px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px',\n";
  echo "  closeButton:'false'\n";
  echo "}}\n";
  exit();
}

if (!isset($_GET["parameters"]))
  exit ("[Error] Parameters must be passed.");
list($content_type, $identity) = explode("-", $_GET["parameters"]);
if (!isset($content_type) || $content_type === false || $content_type === null || !isset($identity) || $identity === false ||
	$identity === null)
  exit ("[Error] Parameters were not passed correctly, or we are missing a parameter for the delete method.");
  
switch ($content_type)
{
  case "podcast":
  {
    $type_database = "sound_clips";
    $type_database_identity_column = "sound_identity";
    $type_name = "podcast"; $plural_name = "podcasts";
    $staff_identity_column = null;
    $landing_page = "podcast/list";
	$required_perm = PERM_PODCASTS;
    break;
  }
  case "beat":
  {
    $type_database = "beats";
    $type_database_identity_column = "beat_identity";
    $type_name = "beat"; $plural_name = "beats";
    $staff_identity_column = "staff_identity";
    $required_perm = PERM_BEATS;
    $landing_page = "beat/list";
    break;
  }
  case "gallery":
  {
    $type_database = "photo_galleries";
    $type_database_identity_column = "gallery_identity";
    $type_name = "photo gallery"; $plural_name = "photo galleries";
    $staff_identity_column = "staff_identity";
    $required_perm = PERM_GALLERIES;
    $landing_page = "photo-gallery/list";
    break;
  }
  case "video":
  {
    $type_database = "videos";
    $type_database_identity_column = "video_identity";
    $type_name = "video"; $plural_name = "videos";
    $staff_identity_column = null;
    $landing_page = "video/list";
	$required_perm = PERM_VIDEOS;
    break;
  }
  case "comic":
  {
    $type_database = "cartoons";
    $type_database_identity_column = "identity";
    $type_name = "comic"; $plural_name = "comics";
    $staff_identity_column = "artist";
    $required_perm = PERM_COMICS;
    $landing_page = "comic/list";
    break;
  }
  case "soundslide":
  {
    $type_database = "sound_slides";
    $type_database_identity_column = "slideshow_identity";
    $type_name = "sound slideshow"; $plural_name = "sound slideshows";
    $staff_identity_column = "staff_identity";
    $landing_page = "soundslide/list";
	$required_perm = PERM_SOUNDSLIDES;
    break;
  }
  default: exit("[Error] Type '" . $content_type . "' is not a valid content type yet.");
}

if (substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], $required_perm, 1) < 1)
  exit ("[Error] You do not have the proper permissions to manage " . $plural_name . ".");

if ($database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" .
	$database->escapeString($identity) . "'") == 0)
  exit ("[Error] There is no " . $type_name . " with the identity '" . $identity . "'.");
$identity = $database->escapeString($identity);

if ($staff_identity_column !== null && $database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE " .
	$type_database_identity_column . "='" . $identity . "' AND " . $staff_identity_column . "='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
  exit ("[Error] You are not the owner of the " . $type_name . " with the identity '" . $identity . "'.");

$content_title = $database->querySingle("SELECT title FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" .
	$identity . "' LIMIT 1");
echo "<div class=\"windowHeader\">Delete?</div>\n";
echo "<div class=\"contentText separatorBottom\">Are you sure you wish to delete the <span class=\"specialText\">" . $type_name .
	"</span>\n";
echo "<div class=\"specialText separator separatorBottom\">'" . $content_title . "'</div>\n";
echo "and all related content and media?\n";
echo "<div class=\"warning miniSeparator separatorBottom\">(<b>Warning:</b> This action cannot be undone.)</div>\n";

echo "<center>\n";
echo "  <input type=\"button\" class=\"inputButton buttonGreen\" value=\"Delete\" id=\"confirm-delete\" onClick=\"this.disabled=true;" .
	"document.getElementById('cancel-delete').disabled=true;executeAJAX('" . MANAGE_WEB_PATH .
	"/components/delete-content.php?type=" . $content_type . "&identity=" . $identity . "', function finish(result)
	{
	  if (result == 'success')
	  {
	    window.location = '" . MANAGE_WEB_PATH . "/" . $landing_page . "/';
	    return;
	  }
	  alert(result);
	  document.getElementById('cancel-delete').disabled = false;
	  document.getElementById('confirm-delete').disabled = false;
	});\" />\n";
echo "  <input type=\"button\" class=\"inputButton buttonRed\" value=\"Cancel\" id=\"cancel-delete\" " .
	"onClick=\"this.disabled=true;document.getElementById('confirm-delete').disabled=true;openWindow.closeWindow();\" />\n";
echo "</center>\n";

echo "</div>\n";
?>