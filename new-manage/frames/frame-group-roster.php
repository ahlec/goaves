<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GROUPS, 1) < "1")
  exit ("In order to load this frame, you must be authenticated to the management panel and have permissions to manage groups.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'550px',\n";
  echo "  height:'325px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px',\n";
  echo "  closeButton:'false'\n";
  echo "}}\n";
  exit();
}

if (!isset($_GET["parameters"]) || $_GET["parameters"] === false)
  exit ("[Error] Parameters must be passed.");
$parameters = explode("-", $_GET["parameters"]);
if ($database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $database->escapeString($parameters[0]) ."'") == 0)
	exit ("The requested group does not exist within the database.");
$group_identity = $database->escapeString($parameters[0]);
if (isset($parameters[1]))
{
  if ($database->querySingle("SELECT count(*) FROM group_subgroups WHERE group_identity='" . $group_identity . "' AND subgroup_identity='" .
	$database->escapeString($parameters[1]) . "'") == 0)
	  exit ("The requested subgroup does not exist within the database, or is not a subgroup of the larger group also specified.");
  $subgroup_identity = $database->escapeString($parameters[1]);
}

echo "<script>\n";
echo "  toggleRosterEntry = function(entry_identity)
	{
	  if (document.getElementById('roster-entry-' + entry_identity).className.indexOf('open') >= 0)
	  {
	    if (document.getElementById('roster-entry-toggle-' + entry_identity).className == 'toggleButton save')
		{
		  var editCall = manage_web_path + '/components/edit-group-roster-entry.php?group_identity=" . $group_identity . (isset($subgroup_identity) ?
			"&subgroup_identity=" . $subgroup_identity : "") . "&entry_identity=' + entry_identity;
		  editCall += '&grade=' + document.getElementById('roster-entry-grade-' + entry_identity).value;
		  editCall += '&position=' + escapeStringURL(document.getElementById('roster-entry-position-' + entry_identity).value);
		  executeAJAX(editCall, function processSave(results)
			{
			  if (results == 'success')
			  {
				document.getElementById('roster-entry-' + entry_identity).className = document.getElementById('roster-entry-' + entry_identity).className.replace(' open', '');
				document.getElementById('roster-entry-toggle-' + entry_identity).innerHTML = 'Edit';
				document.getElementById('roster-entry-toggle-' + entry_identity).className = 'toggleButton';
				document.getElementById('roster-entry-position-span-' + entry_identity).innerHTML =
					(document.getElementById('roster-entry-position-' + entry_identity).value.length == 0 ? '<i>(None specified)</i>' :
					document.getElementById('roster-entry-position-' + entry_identity).value);
				document.getElementById('roster-entry-grade-span-' + entry_identity).innerHTML = document.getElementById('roster-entry-grade-' + entry_identity).value;
			  } else
			    alert(results.replace('[Error] ', ''));
			});
		} else
		{
			document.getElementById('roster-entry-' + entry_identity).className = document.getElementById('roster-entry-' + entry_identity).className.replace(' open', '');
			document.getElementById('roster-entry-toggle-' + entry_identity).innerHTML = 'Edit';
			document.getElementById('roster-entry-toggle-' + entry_identity).className = 'toggleButton';
		}
	  } else
	  {
	    document.getElementById('roster-entry-' + entry_identity).className += ' open';
		document.getElementById('roster-entry-toggle-' + entry_identity).className += ' close';
		document.getElementById('roster-entry-toggle-' + entry_identity).innerHTML = 'Close';
	  }
	};\n";
echo "  rosterEntryChanged = function(entry_identity)
	{
	  document.getElementById('roster-entry-toggle-' + entry_identity).className = 'toggleButton save';
	  document.getElementById('roster-entry-toggle-' + entry_identity).innerHTML = 'Save';
	};\n";
echo "  startCreateNewMember = function()
	{
	  if (document.getElementById('edit-roster-roster').style.display == 'none')
	    return;
	  document.getElementById('edit-roster-roster').style.display = 'none';
	  document.getElementById('roster-create-member').setAttribute('onClick', 'cancelCreateNewMember()');
	  document.getElementById('roster-create-member').value = 'Cancel';
	  document.getElementById('edit-roster-create-member').style.display = 'block';
	  document.getElementById('roster-create-first-name').value = \"\";
	  document.getElementById('roster-create-first-name').className = 'inputText formComponentInProgress';
	  document.getElementById('first-name-error').style.display = 'none';
	  document.getElementById('roster-create-last-name').value = \"\";
	  document.getElementById('roster-create-last-name').className = 'inputText formComponentInProgress';
	  document.getElementById('last-name-error').style.display = 'none';
	  document.getElementById('roster-create-positions').value = \"\";
	  document.getElementById('roster-create-member-number').value = \"\";
	  document.getElementById('roster-create-member-number').className = 'inputText formComponentValid';
	  document.getElementById('member-number-error').style.display = 'none';
	  document.getElementById('roster-create-grade').selectedIndex = 0;
	};
	cancelCreateNewMember = function()
	{
	  if (document.getElementById('edit-roster-roster').style.display == 'block')
	    return;
	  document.getElementById('roster-create-member').setAttribute('onClick','startCreateNewMember()');
	  document.getElementById('roster-create-member').value = 'Add Member';
	  document.getElementById('edit-roster-create-member').style.display = 'none';
	  document.getElementById('edit-roster-roster').style.display = 'block';
	  document.getElementById('roster-create-member').disabled = false;
	};
	checkCreateNewMemberForm = function()
	{
	  document.getElementById('roster-create-create').disabled = (document.getElementById('roster-create-first-name').className != 'inputText formComponentValid' ||
		document.getElementById('roster-create-last-name').className != 'inputText formComponentValid' ||
		document.getElementById('roster-create-grade').selectedIndex == 0 || document.getElementById('member-number-error').style.display == 'block');
	};
	createRosterEntry = function()
	{
	  if (document.getElementById('roster-create-create').disabled)
	    return;
	  document.getElementById('roster-create-create').disabled = true;
	  document.getElementById('roster-create-member').disabled = true;
	  var createCall = manage_web_path + '/components/create-roster-entry.php?group_identity=" . $group_identity . (isset($subgroup_identity) ?
		"&subgroup_identity=" . $subgroup_identity : "") . "';
	  createCall += '&first_name=' + escapeStringURL(document.getElementById('roster-create-first-name').value);
	  createCall += '&last_name=' + escapeStringURL(document.getElementById('roster-create-last-name').value);
	  createCall += '&grade=' + document.getElementById('roster-create-grade').value;
	  createCall += '&position=' + escapeStringURL(document.getElementById('roster-create-positions').value);
	  createCall += '&member_number=' + document.getElementById('roster-create-member-number').value;
	  executeAJAX(createCall, function processCreation(result)
		{
		  if (result.indexOf('[Error] ') < 0)
		  {
		    var entryContainer = document.createElement('div');
			entryContainer.setAttribute('id','roster-entry-' + result);
			entryContainer.className = 'entry ' + (number_roster_entries % 2 == 0 ? 'even' : 'odd');
			var editButton = document.createElement('div');
			editButton.setAttribute('onClick','toggleRosterEntry(\"' + result + '\")');
			editButton.setAttribute('id','roster-entry-toggle-' + result);
			editButton.className = 'toggleButton';
			editButton.innerHTML = 'Edit';
			var name = document.createElement('div');
			name.className = 'name';
			name.innerHTML = '<span>' + document.getElementById('roster-create-first-name').value + ' ' + document.getElementById('roster-create-last-name').value +
				'</span>';
			var position = document.createElement('div');
			position.className = 'position';
			position.innerHTML = '<b>Position:</b> <span id=\"roster-entry-position-span-' + result + '\">' + (document.getElementById('roster-create-positions').value.length > 0 ?
				document.getElementById('roster-create-positions').value : '<i>(None specified)</i>') + '</span> <input type=\"text\"' +
				(document.getElementById('roster-create-positions').value.length > 0 ? ' value=\"' + document.getElementById('roster-create-positions').value +
				'\"' : '') + ' id=\"roster-entry-position-' + result + '\" class=\"inputText\" onKeyUp=\"rosterEntryChanged(\'' +
				result + '\');\" />';
			var grade = document.createElement('div');
			grade.className = 'grade';
			var created_grade = document.getElementById('roster-create-grade').value;
			grade.innerHTML = '<b>Grade:</b> <span id=\"roster-entry-grade-span-' + result + '\">' + document.getElementById('roster-create-grade').value +
				'</span> <select onChange=\"rosterEntryChanged(\'' + result + '\');\" id=\"roster-entry-grade-' + result + '\"><option value=\"9\"' + (created_grade == 9 ?
				' selected=\"selected\"' : '') + '>9</option><option value=\"10\"' + (created_grade == 10 ?
				' selected=\"selected\"' : '') + '>10</option><option value=\"11\"' + (created_grade == 11 ?
				' selected=\"selected\"' : '') + '>11</option><option value=\"12\"' + (created_grade == 12 ?
				' selected=\"selected\"' : '') + '>12</option></select>';
			entryContainer.appendChild(editButton);
			entryContainer.appendChild(name);
			entryContainer.appendChild(position);
			entryContainer.appendChild(grade);
			document.getElementById('edit-roster-roster').appendChild(entryContainer);
			number_roster_entries++;
			cancelCreateNewMember();
		  } else
		  {
		    alert(result.replace('[Error] ',''));
			document.getElementById('roster-create-create').disabled = false;
			document.getElementById('roster-create-member').disabled = false;
		  }
		});
	};\n";
echo "</script>\n";

echo "<div class=\"windowHeader\">Edit " . (isset($subgroup_identity) ? "Subgroup" : "Group") . " Roster</div>\n";
echo "<div class=\"editGroupRosterContainer\">\n";
echo "  <div id=\"edit-roster-roster\">\n";
$current_roster = $database->query("SELECT roster_entry_identity, first_name, last_name, position, member_number, sort_order, grade FROM group_roster " .
	"WHERE group_identity='" . $group_identity . "' AND subgroup_identity" . (isset($subgroup_identity) ? "='" . $subgroup_identity . "'" :
	" is null"));
$index = 0;
while ($roster_entry = $current_roster->fetchArray())
{
  echo "  <div class=\"entry " . ($index % 2 == 0 ? "even" : "odd") . "\" id=\"roster-entry-" . $roster_entry["roster_entry_identity"] . "\">\n";
  echo "    <div class=\"toggleButton\" onClick=\"toggleRosterEntry('" . $roster_entry["roster_entry_identity"] . "');\" id=\"roster-entry-toggle-" .
	$roster_entry["roster_entry_identity"] . "\">Edit</div>\n";
  echo "    <div class=\"name\"><span>" . format_content($roster_entry["first_name"]) . " " . format_content($roster_entry["last_name"]) . "</span></div>\n";
  echo "    <div class=\"position\"><b>Position:</b> <span id=\"roster-entry-position-span-" . $roster_entry["roster_entry_identity"] . "\">" .
	($roster_entry["position"] != "" ? format_content($roster_entry["position"]) :
	"<i>(None specified)</i>") . "</span> <input type=\"text\"" . ($roster_entry["position"] != "" ?
		" value=\"" . addslashes(format_content($roster_entry["position"])) . "\"" : "") . " id=\"roster-entry-position-" .
	$roster_entry["roster_entry_identity"] . "\" class=\"inputText\" onKeyUp=\"rosterEntryChanged('" . $roster_entry["roster_entry_identity"] . "');\" /></div>\n";
  echo "    <div class=\"grade\"><b>Grade:</b> <span id=\"roster-entry-grade-span-" . $roster_entry["roster_entry_identity"] . "\">" .
	$roster_entry["grade"] . "</span> <select onChange=\"rosterEntryChanged('" .
	$roster_entry["roster_entry_identity"] . "');\" id=\"roster-entry-grade-" .
	$roster_entry["roster_entry_identity"] . "\"><option value=\"9\"" . ($roster_entry["grade"] == "9" ? " selected=\"selected\"" : "") .
	">9</option><option value=\"10\"" . ($roster_entry["grade"] == "10" ? " selected=\"selected\"" : "") . ">10</option><option value=\"11\"" .
	($roster_entry["grade"] == "11" ? " selected=\"selected\"" : "") . ">11</option><option value=\"12\"" .
	($roster_entry["grade"] == "12" ? " selected=\"selected\"" : "") . ">12</option></select></div>\n";
  echo "  </div>\n";
  $index++;
}
echo "  </div>\n";
echo "<script> var number_roster_entries = " . $index . "; </script>\n";

echo "  <div style=\"text-align:center;margin-top:10px;\">\n";
echo "    <input type=\"button\" class=\"inputButton\" id=\"roster-create-member\" value=\"Add Member\" onClick=\"startCreateNewMember();\" />\n";
echo "  </div>\n";

echo "  <div class=\"createForm noWindow\" id=\"edit-roster-create-member\" style=\"display:none;\">\n";
echo "    <div class=\"infoLine\"><b>First Name:</b> <input type=\"text\" class=\"inputText formComponentInProgress\" id=\"roster-create-first-name\" " .
	"onKeyUp=\"executeAJAX(manage_web_path + '/components/check-name.php?value=' + this.value, function processFirstName(results)
	{
	  if (results.indexOf('[Error]') >= 0)
	  {
	    document.getElementById('first-name-error').innerHTML = results.replace('[Error] ','');
		document.getElementById('first-name-error').style.display = 'block';
		document.getElementById('roster-create-first-name').className = 'inputText formComponentInvalid';
	  } else
	  {
	    document.getElementById('first-name-error').style.display = 'none';
		document.getElementById('roster-create-first-name').className = 'inputText formComponentValid';
	  }
	  checkCreateNewMemberForm();
	});\" /></div>\n";
echo "    <div class=\"contentError\" id=\"first-name-error\" style=\"display:none;\"></div>\n";
echo "    <div class=\"infoLine\"><b>Last Name:</b> <input type=\"text\" class=\"inputText formComponentInProgress\" id=\"roster-create-last-name\" " .
	"onKeyUp=\"executeAJAX(manage_web_path + '/components/check-name.php?value=' + escapeStringURL(this.value), function process(results)
	{
	  if (results.indexOf('[Error]') >= 0)
	  {
	    document.getElementById('last-name-error').innerHTML = results.replace('[Error] ','');
		document.getElementById('last-name-error').style.display = 'block';
		document.getElementById('roster-create-last-name').className = 'inputText formComponentInvalid';
	  } else
	  {
	    document.getElementById('last-name-error').style.display = 'none';
		document.getElementById('roster-create-last-name').className = 'inputText formComponentValid';
	  }
	  checkCreateNewMemberForm();
	});\" /></div>\n";
echo "    <div class=\"contentError\" id=\"last-name-error\" style=\"display:none;\"></div>\n";
echo "    <div class=\"infoLine separator\"><b>Position(s):</b> <small>(Optional)</small><input type=\"text\" class=\"inputText formComponentValid\" " .
	"id=\"roster-create-positions\" /></div>\n";
echo "    <div class=\"infoLine separator\"><b>Member number:</b> <small>(Optional)</small><input type=\"text\" class=\"inputText formComponentValid\" " .
	"id=\"roster-create-member-number\" " .
	"onKeyUp=\"if (this.value.length > 0) { document.getElementById('member-number-error').style.display = (this.value != parseInt(this.value) ? 'block' : 'none'); } " .
	"checkCreateNewMemberForm();\" /></div>\n";
echo "    <div class=\"contentError\" id=\"member-number-error\" style=\"display:none;\">Member number, if provided, must be numeric values only.</div>\n";
echo "    <div class=\"infoLine separator\"><b>Grade:</b> <select id=\"roster-create-grade\" onChange=\"checkCreateNewMemberForm();\">" .
	"<option value=\"[none]\">(Select)</option><option value=\"9\">9</option>" .
	"<option value=\"10\">10</option><option value=\"11\">11</option><option value=\"12\">12</option></select></div>\n";
echo "    <center><input type=\"button\" onClick=\"createRosterEntry();\" disabled=\"disabled=\" id=\"roster-create-create\" value=\"Create\" " .
	"class=\"inputButton buttonGreen\" /></center>\n";
echo "  </div>\n";

echo "</div>\n";
?>