<?php
if (isset($_GET["parameters"]))
  $note_handle = $_GET["parameters"];
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to load this frame, you must be authenticated to the management panel.");
$database = new DeitloffDatabase(DATABASE_PATH);
$note_information = $database->querySingle("SELECT note_identity, published_date, staff_identity, staff_authority, note_handle FROM " .
  	"manage_notes WHERE note_handle LIKE '" .
  	$database->escapeString($note_handle) . "' LIMIT 1", true);
if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'300px',\n";
  echo "  height:'300px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px',\n";
  echo "  onCloseButtonClick:'executeAJAX(web_path + \"/frames/supplement-read-note.php?note_identity=" .
  	$note_information["note_identity"] . "\", function process(value) { if (value != \"success\") alert(value); })'\n";
  echo "}}\n";
  exit();
}
if ($database->querySingle("SELECT count(*) FROM manage_notes WHERE note_handle LIKE '" . $database->escapeString($note_handle) . "'") > 0)
{
  $staff_information = $database->querySingle("SELECT first_name, last_name, icon_file FROM staff WHERE identity = '" .
  	$note_information["staff_identity"] . "' LIMIT 1", true); 
  
  echo "<div class=\"windowHeader\">A note" . ($note_information["staff_authority"] != "" ? " from the " .
  	$note_information["staff_authority"] : "") . "</div>\n";
  echo "  <div class=\"staffBit\">\n";
  echo "    <img src=\"" . WEB_PATH . "/images/staff/" . $staff_information["icon_file"] . "\" class=\"staffIcon\" />\n";
  echo "    <div class=\"staffName\">" . $staff_information["first_name"] . " " . $staff_information["last_name"] . "</div>\n";
  echo "  </div>\n";
  echo file_get_contents(MANAGE_DOCUMENT_ROOT . "/notes/" . $note_information["note_handle"] . ".txt");
  echo "  <div style=\"clear:both;\"></div>\n";
  echo "  <div class=\"inputButton\" onClick=\"executeAJAX(web_path + '/frames/supplement-read-note.php?note_identity=" .
  	$note_information["note_identity"] . "', function processRead(value) { if (value == 'success') openWindow.closeWindow(); else " .
  	"alert(value); });\" onMouseOver=\"this.className='inputButton inputButtonHover';\" onMouseOut=\"this.className='" .
  	"inputButton';\">Okay</div>\n";
/*  if ($database->querySingle("SELECT count(*) FROM manage_note_views WHERE note_identity='" . $note_information["note_identity"] .
  	"' AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
  	$database->exec("INSERT INTO manage_note_views(note_identity, staff_identity, view_date) VALUES('" .
  		$note_information["note_identity"] . "','" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) .
  		"','" . date("Y-m-d H:i:s") . "')");*/
} else
{
  echo "<div class=\"windowHeader\">Note</div>\n";
  echo "The note that was attempting to load could not be found within the database.\n";
}
?>