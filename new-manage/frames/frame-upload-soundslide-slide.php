<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
require_once (DOCUMENT_ROOT . "/config/class.mp3.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to load this frame, you must be authenticated to the management panel.");
$database = new DeitloffDatabase(DATABASE_PATH);
if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'400px',\n";
  echo "  height:'400px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px'\n";
  echo "}}\n";
  exit();
}
$slideshow_identity = $database->querySingle("SELECT slideshow_identity FROM sound_slides WHERE slideshow_identity='" .
  	$database->escapeString($_GET["parameters"]) . "' AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' LIMIT 1");
if ($slideshow_identity === false)
  exit ("The slideshow presentation you attempted to add a slide for does not exist, or is not owned by you.");

$slideshow_info = $database->querySingle("SELECT title, sound_file FROM sound_slides WHERE slideshow_identity='" . $slideshow_identity . "' LIMIT 1", true);
echo "<div class=\"windowHeader\">Add slide to presentation '" . format_content($slideshow_info["title"]) . "'</div>\n";
echo "<div class=\"createForm noWindow\">\n";

echo "<iframe id=\"frame-soundslide-image\" name=\"uploadForm\" class=\"formFrame\" style=\"display:none;\"></iframe>\n";
echo "  <div class=\"formLabel separator\">Image File:</div>\n";
echo "  <input type=\"hidden\" id=\"image-filepath\" value=\"\" />\n";
echo "  <div class=\"inputFile\" id=\"image-input\">\n";
echo "    <div class=\"button\">Select</div>\n";
echo "    <div class=\"textBox\" id=\"image-input-textbox\"></div>\n";
echo "    <form method=\"POST\" enctype=\"multipart/form-data\" target=\"uploadForm\" action=\"" . MANAGE_WEB_PATH . "/components/upload-soundslide-image.php\" " .
	"id=\"upload-image-form\">\n";
echo "      <input type=\"file\" class=\"inputFile\" name=\"soundslide-image\" onChange=\"document.getElementById('upload-image-form').submit();" .
	"document.getElementById('upload-image-form').disabled = true;this.disabled = true;\" id=\"soundslide-image\" />\n";
echo "    </form>\n";
echo "  </div>\n";
  echo "  <img id=\"slide-display-image\" class=\"slideUploadImage\" style=\"display:none;\" />\n";

  echo "  <div class=\"formLabel separator\">Icon File:</div>\n";
  echo "  <input type=\"hidden\" id=\"slide-image-ratio\" value=\"\" />\n";
  echo "  <input type=\"hidden\" id=\"slide-image-real-x\" value=\"\" />\n";
  echo "  <input type=\"hidden\" id=\"slide-crop-x\" value=\"\" />\n";
  echo "  <input type=\"hidden\" id=\"slide-crop-y\" value=\"\" />\n";
  echo "  <input type=\"hidden\" id=\"slide-crop-width\" value=\"\" />\n";
  echo "  <input type=\"hidden\" id=\"slide-crop-height\" value=\"\" />\n";
  
  echo "  <center class=\"separator\" id=\"slide-upload-icon\">\n";
  echo "    <div id=\"slide-image-crop-container\"></div>\n";
  
  echo "    <input type=\"button\" id=\"slide-crop\" disabled=\"disabled\" value=\"Crop\" class=\"inputButton separator\" " .
	"onClick=\"cropSlideIcon();\" />\n";
  echo "  </center>\n";
  echo "  <input type=\"hidden\" id=\"slide-icon\" value=\"\" />\n";

echo "  <div class=\"formLabel separator\">Start Time:</div>\n";
$minutes_length = 8;
echo "<select id=\"slide-upload-minutes\">\n";
for ($min_option = 0; $min_option <= $minutes_length; $min_option++)
  echo "  <option value=\"" . $min_option . "\">" . ($min_option < 10 ? "0" : "") . $min_option . "</option>\n";
echo "</select> : <select id=\"slide-upload-seconds\">\n";
for ($sec_option = 0; $sec_option < 60; $sec_option++)
  echo "  <option value=\"" . $sec_option . "\">" . ($sec_option < 10 ? "0" : "") . $sec_option . "</option>\n";
echo "</select>\n";
echo " 00:00\n";

/*echo "  <div class=\"formLabel separator\">Caption:</div>\n";
echo "  <textarea onLoad=\'tinyMCE.init({
		mode: \"textareas\",
		theme : \"simple\",
		skin : \"o2k7\"
          });\'></textarea>\n";*/

echo "  <div style=\"clear:both;\"></div>\n";
echo "  <center>\n";
echo "    <input type=\"button\" id=\"slide-add\" disabled=\"disabled\" value=\"Add Slide\" class=\"inputButton separator\" onClick=\"uploadSlide();\" />\n";
echo "  </center>\n";
echo "</div>\n";
?>