<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to load this frame, you must be authenticated to the management panel.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'400px',\n";
  echo "  height:'150px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px',\n";
  echo "  closeButton:'false'\n";
  echo "}}\n";
  exit();
}

if (!isset($_GET["parameters"]))
  exit ("[Error] Parameters must be passed.");
list($content_type, $identity) = explode("-", $_GET["parameters"]);
if (!isset($content_type) || $content_type === false || $content_type === null || !isset($identity) || $identity === false ||
	$identity === null)
  exit ("[Error] Parameters were not passed correctly, or we are missing a parameter for the delete method.");
  
switch ($content_type)
{
  case "podcast":
  {
    $type_database = "podcasts";
    $type_database_identity_column = "podcast_identity";
    $type_name = "podcast"; $plural_name = "podcasts";
    $staff_identity_column = "staff_identity";
	$required_perm = PERM_PODCASTS;
    $landing_page = "podcast/list";
    break;
  }
  case "beat":
  {
    $type_database = "beats";
    $type_database_identity_column = "beat_identity";
    $type_name = "beat"; $plural_name = "beats";
    $staff_identity_column = "staff_identity";
    $required_perm = PERM_BEATS;
    $landing_page = "beat/list";
    break;
  }
  case "gallery":
  {
    $type_database = "photo_galleries";
    $type_database_identity_column = "gallery_identity";
    $type_name = "photo gallery"; $plural_name = "photo galleries";
    $staff_identity_column = "staff_identity";
    $required_perm = PERM_GALLERIES;
    $landing_page = "photo-gallery/list";
    break;
  }
  case "photos":
  {
    $type_name = "photo"; $plural_name = "photos";
    $special_case = true;
    $required_perm = PERM_GALLERIES;
	$landing_page = "photo-gallery/manage/" . $identity;
	break;
  }
  case "video":
  {
    $type_database = "videos";
    $type_database_identity_column = "video_identity";
    $type_name = "video"; $plural_name = "videos";
    $staff_identity_column = null;
    $landing_page = "video/list";
    break;
  }
  case "comic":
  {
    $type_database = "cartoons";
    $type_database_identity_column = "identity";
    $type_name = "comic"; $plural_name = "comics";
    $staff_identity_column = "artist";
    $required_perm = PERM_COMICS;
    $landing_page = "comic/list";
    break;
  }
  case "soundslide":
  {
    $type_database = "sound_slides";
    $type_database_identity_column = "slideshow_identity";
    $type_name = "sound slideshow"; $plural_name = "sound slideshows";
    $staff_identity_column = "staff_identity";
    $landing_page = "soundslide/list";
	$required_perm = PERM_SOUNDSLIDES;
    break;
  }
  case "soundslide_slides":
  {
    $type_name = "slide"; $plural_name = "slides";
    $special_case = true;
    $required_perm = PERM_SOUNDSLIDES;
	$landing_page = "soundslide/manage/" . $identity;
	break;
  }
  case "special":
  {
    $type_database = "specials";
	$type_database_identity_column = "special_identity";
	$type_name = "special"; $plural_name = "specials";
	$landing_page = "special";
	$required_perm = PERM_SPECIALS;
	$staff_identity_column = null;
	break;
  }
  case "staff":
  {
    $type_database = "staff";
	$type_database_identity_column = "identity";
	$type_database_title_column = "first_name || ' ' || last_name";
	$type_name = "staff member"; $plural_name = "staff members";
	$landing_page = "admin/staff";
	$required_perm = PERM_ADMIN;
	$staff_identity_column = null;
	break;
  }
  case "group":
  {
    $type_database = "groups";
	$type_database_identity_column = "group_identity";
	$type_database_title_column = "title";
	$type_name = "group"; $plural_name = "groups";
	$landing_page = "group/list";
	$required_perm = PERM_GROUPS;
	$staff_identity_column = null;
	break;
  }
  case "subgroup":
  {
    $type_database = "group_subgroups";
	$type_database_identity_column = "subgroup_identity";
	$type_database_title_column = "name";
	$type_name = "subgroup"; $plural_name = "subgroups";
	$landing_page = "group/manage/" . $database->querySingle("SELECT group_identity FROM group_subgroups WHERE subgroup_identity='" .
		$database->escapeString($identity) . "' LIMIT 1");
	$required_perm = PERM_GROUPS;
	$staff_identity_column = null;
	break;
  }
  case "poll":
  {
    $type_database = "polls";
	$type_database_identity_column = "identity";
	$type_database_title_column = "poll_question";
	$type_name = "poll"; $plural_name = "polls";
	$landing_page = "interactions/list";
	$required_perm = PERM_INTERACTIONS;
	$staff_identity_column = null;
	break;
  }
  case "poll_response":
  {
    $type_database = "poll_responses";
	$type_database_identity_column = "response_identity";
	$type_database_title_column = "response";
	$type_name = "response"; $plural_name = "responses";
	$landing_page = "interactions/poll-manage/" . $database->querySingle("SELECT poll_identity FROM poll_responses WHERE response_identity='" .
		$database->escapeString($identity) . "' LIMIT 1");
	$required_perm = PERM_INTERACTIONS;
	$staff_identity_column = null;
	break;
  }
  case "leaf_item":
  {
    $type_database = "leaf_items";
	$type_database_identity_column = "item_identity";
	$type_database_title_column = "item_title";
	$type_name = "leaf item"; $plural_name = "leaf items";
	$landing_page = "leaf/manage/" . $database->querySingle("SELECT leaf_identity FROM leaf_items WHERE item_identity='" .
		$database->escapeString($identity) . "' LIMIT 1");
	$required_perm = PERM_LEAF;
	$staff_identity_column = null;
	break;
  }
  default: exit("[Error] Type '" . $content_type . "' is not a valid content type yet.");
}

if (substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], $required_perm, 1) < 1)
  exit ("[Error] You do not have the proper permissions to manage " . $plural_name . ".");
  
if (!isset($special_case) || !$special_case)
{

if ($database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" .
	$database->escapeString($identity) . "'") == 0)
  exit ("[Error] There is no " . $type_name . " with the identity '" . $identity . "'.");
$identity = $database->escapeString($identity);

if ($type_database == "staff" && $identity == $_SESSION[MANAGE_SESSION]["IDENTITY"])
  exit ("[Error] You cannot delete your own staff account.");

if ($staff_identity_column !== null && $database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE " .
	$type_database_identity_column . "='" . $identity . "' AND " . $staff_identity_column . "='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
  exit ("[Error] You are not the owner of the " . $type_name . " with the identity '" . $identity . "'.");

$content_title = $database->querySingle("SELECT " . (isset($type_database_title_column) ? $type_database_title_column : "title") .
	" FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" .
	$identity . "' LIMIT 1");
echo "<div class=\"windowHeader\">Delete?</div>\n";
echo "<div class=\"contentText separatorBottom\">Are you sure you wish to delete the <span class=\"specialText\">" . $type_name .
	"</span>\n";
echo "<div class=\"specialText separator separatorBottom\">'" . $content_title . "'</div>\n";
echo "and all related content and media?\n";

echo "</div>\n";
} else
{
  if ($content_type == "photos")
  {
	if ($database->querySingle("SELECT count(*) FROM photo_galleries WHERE gallery_identity='" . $database->escapeString($identity) .
		"' AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
		exit ("[Error] The photo gallery does not exist, or you are not the author of the gallery.");
	$identity = $database->escapeString($identity);
	
    if (!isset($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$identity]))
	  exit ("[Error] There are no selected photos.");
    echo "<div class=\"windowHeader\">Delete?</div>\n";
	
	$photos_to_delete = array();
	foreach ($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$identity] as $picture)
	{
	  if ($database->querySingle("SELECT count(*) FROM photos WHERE picture_identity='" . $database->escapeString($picture) . "' AND gallery_identity='" .
		$identity . "'") == 0)
		exit ("[Error] A selected photo does not exist, or is not in this photo gallery.");
	  $picture_icon = $database->querySingle("SELECT icon_file FROM photos WHERE picture_identity='" . $database->escapeString($picture) .
		"' AND gallery_identity='" . $identity . "' LIMIT 1");
	  $photos_to_delete[] = $picture_icon;
	}
	
	$gallery_title = $database->querySingle("SELECT title FROM photo_galleries WHERE gallery_identity='" . $identity . "' LIMIT 1");
	
	echo "<div class=\"contentText separatorBottom\">Are you sure you wish to delete the following <span class=\"specialText\">" . 
		sizeof($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$identity]) . " photo" . (sizeof($_SESSION[SESSION_PREFIX . "ManageGalleryPictures"][$identity]) == 1 ?
		"" : "s") . "</span> from the photo gallery <span class=\"specialText\">" . $gallery_title . "</span>?\n</div>\n";
	echo "<center>\n";
	foreach ($photos_to_delete as $photo)
	  echo "<img src=\"" . WEB_PATH . "/images/photos/" . $photo . "\" class=\"deletePhotoIcon\" />\n";
	echo "</center>\n";
	
  }
  if ($content_type == "soundslide_slides")
  {
	if ($database->querySingle("SELECT count(*) FROM sound_slides WHERE slideshow_identity='" . $database->escapeString($identity) .
		"' AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
		exit ("[Error] The slideshow does not exist, or you are not the author of the prsentation.");
	$identity = $database->escapeString($identity);
	
    if (!isset($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$identity]))
	  exit ("[Error] There are no selected slides.");
    echo "<div class=\"windowHeader\">Delete?</div>\n";
	
	$slides_to_delete = array();
	foreach ($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$identity] as $slide)
	{
	  if ($database->querySingle("SELECT count(*) FROM sound_slides_slides WHERE slide_identity='" . $database->escapeString($slide) . "' AND slideshow_identity='" .
		$identity . "'") == 0)
		exit ("[Error] A selected slide does not exist, or is not in this presentation.");
	  $slide_icon = $database->querySingle("SELECT icon_file FROM sound_slides_slides WHERE slide_identity='" . $database->escapeString($slide) .
		"' AND slideshow_identity='" . $identity . "' LIMIT 1");
	  $slides_to_delete[] = $slide_icon;
	}
	
	$soundslide_title = $database->querySingle("SELECT title FROM sound_slides WHERE slideshow_identity='" . $identity . "' LIMIT 1");
	
	echo "<div class=\"contentText separatorBottom\">Are you sure you wish to delete the following <span class=\"specialText\">" . 
		sizeof($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$identity]) . " slide" . (sizeof($_SESSION[SESSION_PREFIX . "ManageSoundslidesSlides"][$identity]) == 1 ?
		"" : "s") . "</span> from the soundslide presentation <span class=\"specialText\">" . format_content($soundslide_title) . "</span>?\n</div>\n";
	echo "<center>\n";
	foreach ($slides_to_delete as $slide)
	  echo "<img src=\"" . WEB_PATH . "/images/sound_slides/" . $slide . "\" class=\"deleteSlideIcon\" />\n";
	echo "</center>\n";
	
  }
}
echo "<div class=\"contentText warning miniSeparator separatorBottom\">(<b>Warning:</b> This action cannot be undone.)</div>\n";

echo "<center>\n";
echo "  <input type=\"button\" class=\"inputButton buttonGreen\" value=\"Delete\" id=\"confirm-delete\" onClick=\"this.disabled=true;" .
	"document.getElementById('cancel-delete').disabled=true;executeAJAX('" . MANAGE_WEB_PATH .
	"/components/delete-content.php?type=" . $content_type . "&identity=" . $identity . "', function finish(result)
	{
	  if (result == 'success')
	  {
	    window.location = '" . MANAGE_WEB_PATH . "/" . $landing_page . "/';
	    return;
	  }
	  alert(result);
	  document.getElementById('cancel-delete').disabled = false;
	  document.getElementById('confirm-delete').disabled = false;
	});\" />\n";
echo "  <input type=\"button\" class=\"inputButton buttonRed\" value=\"Cancel\" id=\"cancel-delete\" " .
	"onClick=\"this.disabled=true;document.getElementById('confirm-delete').disabled=true;openWindow.closeWindow();\" />\n";
echo "</center>\n";
?>