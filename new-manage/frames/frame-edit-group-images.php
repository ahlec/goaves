<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to load this frame, you must be authenticated to the management panel.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'550px',\n";
  echo "  height:'300px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px',\n";
  echo "  closeButton:'false'\n";
  echo "}}\n";
  exit();
}

if (!isset($_GET["parameters"]) || $_GET["parameters"] === false)
  exit ("[Error] Parameters must be passed.");
if ($database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $database->escapeString($_GET["parameters"]) ."'") == 0)
	exit ("The requested group does not exist within the database.");
$group_identity = $database->escapeString($_GET["parameters"]);

echo "<div class=\"windowHeader\">Edit Group Images</div>\n";
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");

echo "<div class=\"windowManagePictureContainer\">\n";

$current_images = $database->querySingle("SELECT image_file, icon_file FROM groups WHERE group_identity='" . $group_identity . "' LIMIT 1", true);

echo "  <div class=\"preview\">\n";
echo "    <div class=\"title\">Previews</div>\n";
echo "    <img id=\"preview-group-image\" class=\"previewImage\" src=\"" . WEB_PATH . "/images/groups/" . ($current_images["image_file"] != "" ?
	$current_images["image_file"] : "no-group-image.jpg") . "\" />\n";
echo "    <div class=\"caption\" id=\"groupImageCaption\">(Current Image)</div>\n";
echo "    <img id=\"preview-group-icon\" class=\"previewIcon separator\" src=\"" . WEB_PATH . "/images/groups/" . ($current_images["icon_file"] != "" ?
	$current_images["icon_file"] : "no-group-icon.jpg") . "\" />\n";
echo "    <div class=\"caption\" id=\"groupIconCaption\">(Current Icon)</div>\n";
echo "  </div>\n";

echo "  <div class=\"createForm noWindow\">\n";
echo "    <input type=\"hidden\" id=\"group-image-original\" />\n";
echo "    <input type=\"hidden\" id=\"group-image\" />\n";
// max of width 650
UploadImageInput("group-image", "Group Image", "group-image", "group-image-original", 650, null, "groupImageUploaded()");
echo "  <center id=\"group-icon-crop-container\" style=\"display:none;\">\n";
echo "    <input type=\"hidden\" id=\"group-icon\" />\n";
echo "    <input type=\"hidden\" id=\"group-icon-crop-x\" />\n";
echo "    <input type=\"hidden\" id=\"group-icon-crop-y\" />\n";
echo "    <input type=\"hidden\" id=\"group-icon-crop-width\" />\n";
echo "    <input type=\"hidden\" id=\"group-icon-crop-height\" />\n";
echo "    <img id=\"group-icon-crop-img\" class=\"imageCropBox\" style=\"display:block;\" /><br />\n";
echo "    <input type=\"button\" class=\"inputButton\" id=\"cropGroup-iconButton\" onClick=\"cropGroupIcon();\" value=\"Crop\" />\n";
echo "  </center>\n";
echo "    <div style=\"clear:both;\"></div>\n";
echo "  </div>\n";

echo "  <div class=\"uploadButtons\">\n";
echo "    <input type=\"button\" class=\"inputButton buttonGreen\" value=\"Upload\" disabled=\"disabled\" id=\"confirm-upload\" onClick=\"uploadGroupImages();\" />\n";
echo "    <input type=\"button\" class=\"inputButton buttonRed\" value=\"Cancel\" id=\"cancel-upload\" " .
	"onClick=\"this.disabled=true;document.getElementById('confirm-upload').disabled=true;openWindow.closeWindow();\" />\n";
echo "  </div>\n";
echo "</div>\n";
?>