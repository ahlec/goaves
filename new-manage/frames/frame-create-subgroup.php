<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GROUPS, 1) < "1")
  exit ("In order to load this frame, you must be authenticated to the management panel and have the permissions required to manage groups.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'550px',\n";
  echo "  height:'300px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px'\n";
  echo "}}\n";
  exit();
}

if (!isset($_GET["parameters"]) || $_GET["parameters"] === false)
  exit ("[Error] Parameters must be passed.");
if ($database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $database->escapeString($_GET["parameters"]) ."'") == 0)
	exit ("The requested group does not exist within the database.");
$group_identity = $database->escapeString($_GET["parameters"]);

echo "<script>\n";
echo "checkCreateSubgroupForm = function() {
		if (document.getElementById('subgroup-title').className != 'inputText formComponentValid' ||
		document.getElementById('subgroup-image').value.length < 1)
		{
		  document.getElementById('confirm-create').disabled = true;
		  return;
		}
		document.getElementById('confirm-create').disabled = false;
	};
	createSubgroup = function() {
		if (document.getElementById('confirm-create').disabled)
		  return;
		document.getElementById('confirm-create').disabled = true;
		document.getElementById('cancel-create').disabled = true;
		var createCall = manage_web_path + '/components/create-subgroup.php?group=" . $group_identity . "';
		createCall += '&title=' + escapeStringURL(document.getElementById('subgroup-title').value);
		createCall += '&image=' + document.getElementById('subgroup-image').value;
		executeAJAX(createCall, function process(result)
			{
				if (result.indexOf('[Error] ') < 0)
				{
				  window.location = manage_web_path + '/group/manage/" . $group_identity . "/';
				  return;
				} else
				{
				  alert(result.replace('[Error] ',''));
				  document.getElementById('confirm-create').disabled = false;
				  document.getElementById('cancel-create').disabled = false;
				}
			});
	};\n";
echo "</script>\n";

echo "<div class=\"windowHeader\">Create new subgroup</div>\n";
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");

echo "<div class=\"createForm noWindow\">\n";
echo "  <div class=\"formLabel\">Title:</div>\n";
echo "  <input type=\"text\" class=\"inputText\" name=\"subgroup-title\" id=\"subgroup-title\" autocomplete=\"off\" onKeyUp=\"executeAJAX(web_path + '/components/" .
	"check-title.php?type=subgroup&minimum=6&title=' + encodeURIComponent(this.value), function " .
	"processTitle(return_value) {
	  if (return_value.indexOf('[Error]') >= 0)
	  {
	    document.getElementById('subgroup-title-error').innerHTML = return_value.replace('[Error] ', '');
		if (document.getElementById('subgroup-title-error').isFading != true)
		{
	      document.getElementById('subgroup-title-error').style.display = 'block';
		  fadeOutAndHide('subgroup-title-error', 2400);
	    }
		document.getElementById('subgroup-title').className = 'inputText formComponentInvalid';
		document.getElementById('confirm-create').disabled = true;
	  } else
	  {
	    document.getElementById('subgroup-title-error').style.display = 'none';
	    document.getElementById('subgroup-title').className = 'inputText formComponentValid';
		checkCreateSubgroupForm();
	  } });\" />\n";
	 echo "  <div class=\"contentError\" id=\"subgroup-title-error\" style=\"display:none;\"></div>\n";

echo "  <input type=\"hidden\" id=\"subgroup-image\" />\n";
echo "  <input type=\"hidden\" id=\"subgroup-image-original\" />\n";
UploadImageInput("subgroup-image", "Subgroup Image", "subgroup-image", "subgroup-image-original", 500, null, "checkCreateSubgroupForm()");

echo "</div>\n";

echo "  <div style=\"text-align:center;margin-top:10px;\">\n";
echo "    <input type=\"button\" class=\"inputButton buttonGreen\" value=\"Create\" disabled=\"disabled\" id=\"confirm-create\" onClick=\"createSubgroup();\" />\n";
echo "    <input type=\"button\" class=\"inputButton buttonRed\" value=\"Cancel\" id=\"cancel-create\" " .
	"onClick=\"this.disabled=true;document.getElementById('confirm-create').disabled=true;openWindow.closeWindow();\" />\n";
echo "  </div>\n";
?>