<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_LEAF, 1) < "1")
  exit ("In order to load this frame, you must be authenticated to the management panel and have the permissions required to manage The Leaf.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'550px',\n";
  echo "  height:'300px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px'\n";
  echo "}}\n";
  exit();
}

if (!isset($_GET["parameters"]) || $_GET["parameters"] === false)
  exit ("[Error] Parameters must be passed.");
if ($database->querySingle("SELECT count(*) FROM leaf_issuu WHERE issuu_identity='" . $database->escapeString($_GET["parameters"]) ."'") == 0)
	exit ("The requested issue does not exist within the database.");
$issuu_identity = $database->escapeString($_GET["parameters"]);

echo "<script>\n";
echo "checkCreateSubgroupForm = function() {
		if (document.getElementById('subgroup-title').className != 'inputText formComponentValid' ||
		document.getElementById('subgroup-image').value.length < 1)
		{
		  document.getElementById('confirm-create').disabled = true;
		  return;
		}
		document.getElementById('confirm-create').disabled = false;
	};
	toggleUseRelatedGroup = function() {
	    document.getElementById('item-related-group-type').selectedIndex = 0;
	    document.getElementById('item-related-group-type').style.display =
			(document.getElementById('item-related-group-use').value ? 'block' : 'none');
		for (var type_index = 0; type_index < group_types.length; type_index++)
		  document.getElementById('item-related-group-' + group_types[type_index]).style.display = 'none';
	  };
	selectGroupByType = function(type_handle)
	  {
	    for (var type_index = 0; type_index < group_types.length; type_index++)
		  document.getElementById('item-related-group-' + group_types[type_index]).style.display = 'none';
	    if (type_handle == '[none]')
		  return;
		document.getElementById('item-related-group-' + type_handle).selectedIndex = 0;
	    document.getElementById('item-related-group-' + type_handle).style.display = 'block';
	  }
	createSubgroup = function() {
		if (document.getElementById('confirm-create').disabled)
		  return;
		document.getElementById('confirm-create').disabled = true;
		document.getElementById('cancel-create').disabled = true;
		var createCall = manage_web_path + '/components/create-subgroup.php?group=" . $issuu_identity . "';
		createCall += '&title=' + escapeStringURL(document.getElementById('subgroup-title').value);
		createCall += '&image=' + document.getElementById('subgroup-image').value;
		executeAJAX(createCall, function process(result)
			{
				if (result.indexOf('[Error] ') < 0)
				{
				  window.location = manage_web_path + '/group/manage/" . $issuu_identity . "/';
				  return;
				} else
				{
				  alert(result.replace('[Error] ',''));
				  document.getElementById('confirm-create').disabled = false;
				  document.getElementById('cancel-create').disabled = false;
				}
			});
	};\n";
echo "</script>\n";

echo "<div class=\"windowHeader\">Add new <i>Leaf</i> item</div>\n";

echo "<div class=\"createForm noWindow\">\n";
echo "  <div class=\"formLabel\">Title:</div>\n";
echo "  <input type=\"text\" class=\"inputText\" name=\"subgroup-title\" id=\"subgroup-title\" autocomplete=\"off\" onKeyUp=\"executeAJAX(web_path + '/components/" .
	"check-title.php?type=subgroup&minimum=6&title=' + encodeURIComponent(this.value), function " .
	"processTitle(return_value) {
	  if (return_value.indexOf('[Error]') >= 0)
	  {
	    document.getElementById('subgroup-title-error').innerHTML = return_value.replace('[Error] ', '');
		if (document.getElementById('subgroup-title-error').isFading != true)
		{
	      document.getElementById('subgroup-title-error').style.display = 'block';
		  fadeOutAndHide('subgroup-title-error', 2400);
	    }
		document.getElementById('subgroup-title').className = 'inputText formComponentInvalid';
		document.getElementById('confirm-create').disabled = true;
	  } else
	  {
	    document.getElementById('subgroup-title-error').style.display = 'none';
	    document.getElementById('subgroup-title').className = 'inputText formComponentValid';
		checkCreateSubgroupForm();
	  } });\" />\n";
	 echo "  <div class=\"contentError\" id=\"subgroup-title-error\" style=\"display:none;\"></div>\n";
echo "  <div class=\"formLabel\">Subtitle:</div>\n";
echo "  <input type=\"text\" class=\"inputText\" name=\"item-subtitle\" id=\"item-subtitle\" autocomplete=\"off\" onKeyUp=\"checkItemFormComplete();\" />\n";
$number_of_pages = $database->querySingle("SELECT number_pages FROM leaf_issuu WHERE issuu_identity='" . $issuu_identity . "' LIMIT 1");
echo "  <div class=\"formLabel separator\">Page number:</div> <select id=\"item-page-number\" onChange=\"checkItemFormComplete();\">\n";
echo "    <option value=\"none\">(Select)</option>\n";
for ($page = 1; $page <= $number_of_pages; $page++)
  echo "    <option value=\"" . $page . "\">" . $page . "</option>\n";
echo "  </select>\n";
echo "  <div class=\"formLabel\">Item type:</div> <select id=\"item-type\" onChange=\"checkItemFormComplete();\">\n";
echo "    <option value=\"none\">(Select)</option>\n";
foreach ($leaf_item_types as $value => $title)
  echo "    <option value=\"" . $value . "\">" . $title . "</option>\n";
echo "  </select>\n";
echo "  <div class=\"formLabel\">Display on <i>Leaf</i> brief?</div>\n";
echo "  <input type=\"checkbox\" onChange=\"checkItemFormComplete();\" id=\"item-noteworthy\" /> <label for=\"item-noteworthy\">Display</label>\n";

echo "  <div class=\"formLabel separator\">Related Group:</div>\n";
echo "  <input type=\"checkbox\" id=\"item-related-group-use\" onChange=\"toggleUseRelatedGroup();\" /> " .
	"<label for=\"item-related-group-use\">Link to a related group?</label><br />\n";
$group_types = array();
$get_group_types = $database->query("SELECT type_identity, plural_name, handle FROM group_types ORDER BY type_identity ASC");
while ($group_type = $get_group_types->fetchArray())
{
  $get_groups = $database->query("SELECT group_identity, title, active FROM groups WHERE type='" .
	$group_type["type_identity"] . "' ORDER BY title ASC");
  $groups_with_type = array();
  while ($group = $get_groups->fetchArray())
	$groups_with_type[] = array("IDENTITY" => $group["group_identity"], "TITLE" => format_content($group["title"]),
		"ACTIVE" => ($group["active"] == "TRUE"));
  $type = array("NAME" => format_content($group_type["plural_name"]), "HANDLE" => $group_type["handle"], "GROUPS" => $groups_with_type);
  $group_types[$group_type["handle"]] = $type;
}
$group_handles = "[";
foreach ($group_types as $handle => $info)
	$group_handles .= "'" . $handle . "',";
$group_handles = rtrim($group_handles, ",") . "]";
echo "  <script> var group_types = " . $group_handles . "; </script>\n";

echo "  <select id=\"item-related-group-type\" style=\"display:none;\" class=\"inputSelect selectLeft\" " .
	"onChange=\"selectGroupByType(this.value);\">\n";
echo "    <option value=\"[none]\">(Group Type)</option>\n";
foreach ($group_types as $handle => $info)
	echo "    <option value=\"" . $handle . "\">" . $info["NAME"] . "</option>\n";
echo "  </select>\n";
foreach ($group_types as $handle => $info)
{
	echo "  <select id=\"item-related-group-" . $handle .
		"\" class=\"inputSelect selectRight\" style=\"display:none;\">\n";
	echo "    <option value=\"[none]\">(Group)</option>\n";
	foreach ($info["GROUPS"] as $group)
		echo "    <option value=\"" . $group["IDENTITY"] . "\">" . $group["TITLE"] .
			(!$group["ACTIVE"] ? " (Inactive)" : "") . "</option>\n";
	echo "  </select>\n";
}

echo "</div>\n";

echo "  <div style=\"text-align:center;margin-top:10px;\">\n";
echo "    <input type=\"button\" class=\"inputButton buttonGreen\" value=\"Create\" disabled=\"disabled\" id=\"confirm-create\" onClick=\"createSubgroup();\" />\n";
echo "    <input type=\"button\" class=\"inputButton buttonRed\" value=\"Cancel\" id=\"cancel-create\" " .
	"onClick=\"this.disabled=true;document.getElementById('confirm-create').disabled=true;openWindow.closeWindow();\" />\n";
echo "  </div>\n";
?>