<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_GROUPS, 1) < 1)
  exit ("In order to load this frame, you must be authenticated to the management panel and have the permissions to manage groups.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'400px',\n";
  echo "  height:'150px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px',\n";
  echo "  closeButton:'false'\n";
  echo "}}\n";
  exit();
}

if (!isset($_GET["parameters"]))
  exit ("[Error] Parameters must be passed.");
$parameters = explode("-", $_GET["parameters"]);
if (!isset($parameters[0]) || $parameters[0] === false)
  exit ("[Error] Parameters were not passed correctly.");

if ($database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $database->escapeString($parameters[0]) . "'") == 0)
  exit ("[Error] The selected group does not exist within the database.");
$group_identity = $database->escapeString($parameters[0]);

if (isset($parameters[1]))
{
  if ($database->querySingle("SELECT count(*) FROM group_subgroups WHERE group_identity='" . $group_identity . "' AND subgroup_identity='" .
	$database->escapeString($parameters[1]) . "'") == 0)
	exit ("[Error] The specified subgroup does not exist within the database, or is not a subgroup of the larger group.");
  $subgroup_identity = $database->escapeString($parameters[1]);
} else
  $subgroup_identity = null;

echo "<div class=\"windowHeader\">Clear Roster?</div>\n";
$number_records = $database->querySingle("SELECT count(*) FROM group_roster WHERE group_identity='" . $group_identity . "' AND subgroup_identity" .
	($subgroup_identity != null ? "='" . $subgroup_identity . "'" : " is null"));
echo "<div class=\"contentText separatorBottom\">Are you sure you wish to clear the <span class=\"specialText\">" . $number_records .
	" record" . ($number_records != 1 ? "s" : "") . "</span> in the " . ($subgroup_identity != null ?
	"subgroup" : "group") . " <span class=\"specialText\">" . ($subgroup_identity != null ?
	$database->querySingle("SELECT name FROM group_subgroups WHERE subgroup_identity='" . $subgroup_identity . "' LIMIT 1") :
	$database->querySingle("SELECT title FROM groups WHERE group_identity='" . $group_identity . "' LIMIT 1")) . "</span>?\n";
echo "</div>\n";

echo "<div class=\"contentText warning miniSeparator separatorBottom\">(<b>Warning:</b> This action cannot be undone.)</div>\n";

echo "<center>\n";
echo "  <input type=\"button\" class=\"inputButton buttonGreen\" value=\"Delete\" id=\"confirm-delete\" onClick=\"this.disabled=true;" .
	"document.getElementById('cancel-delete').disabled=true;executeAJAX('" . MANAGE_WEB_PATH .
	"/components/empty-group-roster.php?group_identity=" . $group_identity . ($subgroup_identity != null ? "&subgroup_identity=" .
		$subgroup_identity : "") . "', function finish(result)
	{
	  if (result == 'success')
	  {
	    window.location = '" . MANAGE_WEB_PATH . "/group/manage/" . $group_identity . "';
	    return;
	  }
	  alert(result);
	  document.getElementById('cancel-delete').disabled = false;
	  document.getElementById('confirm-delete').disabled = false;
	});\" />\n";
echo "  <input type=\"button\" class=\"inputButton buttonRed\" value=\"Cancel\" id=\"cancel-delete\" " .
	"onClick=\"this.disabled=true;document.getElementById('confirm-delete').disabled=true;openWindow.closeWindow();\" />\n";
echo "</center>\n";
?>