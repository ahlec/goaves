<?php
if (isset($_GET["parameters"]))
  $explanation_handle = $_GET["parameters"];
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to load this frame, you must be authenticated to the management panel.");
$database = new DeitloffDatabase(DATABASE_PATH);
if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'400px',\n";
  echo "  height:'400px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px'\n";
  echo "}}\n";
  exit();
}
if ($database->querySingle("SELECT count(*) FROM manage_explanations WHERE handle LIKE '" .
	$database->escapeString($explanation_handle) . "'") > 0)
{
  $explanation_info = $database->querySingle("SELECT subject, filepath FROM manage_explanations WHERE handle LIKE '" .
  	$database->escapeString($explanation_handle) . "' LIMIT 1", true);
  echo "<div class=\"windowHeader\">" . $explanation_info["subject"] . "</div>\n";
  require_once (MANAGE_DOCUMENT_ROOT . "/explanations/" . $explanation_info["filepath"] . ".php");
  echo "  <div style=\"clear:both;\"></div>\n";
  echo "  <div class=\"inputButton\" onClick=\"openWindow.closeWindow();\" onMouseOver=\"this.className='inputButton " .
  	"inputButtonHover';\" onMouseOut=\"this.className='inputButton';\">Okay</div>\n";
} else
{
  echo "<div class=\"windowHeader\">Error</div>\n";
  echo "The explanation could not be found within the database.\n";
}
?>