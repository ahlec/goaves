<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to load this frame, you must be authenticated to the management panel.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'550px',\n";
  echo "  height:'300px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px',\n";
  echo "  closeButton:'false'\n";
  echo "}}\n";
  exit();
}

if ($database->querySingle("SELECT count(*) FROM staff WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) ."'") == 0)
	exit ("The requested staff member does not exist within the database.");
$staff_identity = $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]);
$has_staff_image = ($database->querySingle("SELECT image_file FROM staff WHERE identity='" . $staff_identity . "'") != "no-staff-picture.jpg");
	
echo "<div class=\"windowHeader\">Upload Staff Picture?</div>\n";
require_once (DOCUMENT_ROOT . "/config/functions-upload.inc.php");

echo "<div class=\"uploadStaffPictureContainer\">\n";

echo "  <div class=\"preview\">\n";
echo "    <div class=\"title\">Previews</div>\n";
echo "    <img id=\"preview-staff-image\" class=\"previewStaffImage\" src=\"" . WEB_PATH . "/images/staff/no-staff-picture.jpg\" />\n";
echo "    <div class=\"caption\">(New Image)</div>\n";
echo "    <img id=\"preview-staff-icon\" class=\"previewStaffIcon separator\" src=\"" . WEB_PATH . "/images/staff/no-staff-icon.jpg\" />\n";
echo "    <div class=\"caption\">(New Icon)</div>\n";
echo "  </div>\n";

echo "  <div class=\"createForm noWindow\">\n";
echo "    <input type=\"hidden\" id=\"staff-image-original\" />\n";
echo "    <input type=\"hidden\" id=\"staff-image\" />\n";
UploadImageInput("staff-image", "Staff Image", "staff-image", "staff-image-original", 300, 400, "staffImageUploaded()");
echo "  <center id=\"staff-icon-crop-container\" style=\"display:none;\">\n";
echo "    <input type=\"hidden\" id=\"staff-icon\" />\n";
echo "    <input type=\"hidden\" id=\"staff-icon-crop-x\" />\n";
echo "    <input type=\"hidden\" id=\"staff-icon-crop-y\" />\n";
echo "    <input type=\"hidden\" id=\"staff-icon-crop-width\" />\n";
echo "    <input type=\"hidden\" id=\"staff-icon-crop-height\" />\n";
echo "    <img id=\"staff-icon-crop-img\" class=\"imageCropBox\" style=\"display:block;\" /><br />\n";
echo "    <input type=\"button\" class=\"inputButton\" id=\"cropstaff-iconButton\" onClick=\"cropStaffIcon();\" value=\"Crop\" />\n";
echo "  </center>\n";
echo "    <div style=\"clear:both;\"></div>\n";
echo "  </div>\n";

echo "  <div class=\"uploadButtons\">\n";
echo "    <input type=\"button\" class=\"inputButton buttonGreen\" value=\"Upload\" disabled=\"disabled\" id=\"confirm-upload\" onClick=\"uploadStaffImages();\" />\n";
echo "    <input type=\"button\" class=\"inputButton buttonRed\" value=\"Cancel\" id=\"cancel-upload\" " .
	"onClick=\"this.disabled=true;document.getElementById('confirm-upload').disabled=true;openWindow.closeWindow();\" />\n";
echo "  </div>\n";
echo "</div>\n";
?>