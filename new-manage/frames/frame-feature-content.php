<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to load this frame, you must be authenticated to the management panel.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'600px',\n";
  echo "  height:'400px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px',\n";
  echo "  closeButton:'false'\n";
  echo "}}\n";
  exit();
}

if (!isset($_GET["parameters"]))
  exit ("[Error] Parameters must be passed.");
list($content_type, $identity) = explode("-", $_GET["parameters"]);
if (!isset($content_type) || $content_type === false || $content_type === null || !isset($identity) || $identity === false ||
	$identity === null)
  exit ("[Error] Parameters were not passed correctly, or we are missing a parameter for the delete method.");
  
switch ($content_type)
{
  case "beat":
  {
    $type_database = "beats";
    $type_database_identity_column = "beat_identity";
    $type_name = "beat"; $plural_name = "beats";
    $staff_identity_column = "staff_identity";
    $required_perm = PERM_BEATS;
	$feature_type = "article";
	$landing_page = "beat/manage/";
    break;
  }
  case "gallery":
  {
    $type_database = "photo_galleries";
    $type_database_identity_column = "gallery_identity";
    $type_name = "photo gallery"; $plural_name = "photo galleries";
    $staff_identity_column = "staff_identity";
    $required_perm = PERM_GALLERIES;
	$feature_type = "photo_gallery";
    $landing_page = "photo-gallery/manage/";
    break;
  }
  case "soundslides":
  {
    $type_database = "sound_slides";
    $type_database_identity_column = "slideshow_identity";
    $type_name = "slideshow presentation"; $plural_name = "soundslide presentations";
    $staff_identity_column = "staff_identity";
    $required_perm = PERM_SOUNDSLIDES;
	$feature_type = "soundslides";
    $landing_page = "soundslide/manage/";
    break;
  }
  case "special":
  {
    $type_database = "specials";
	$type_database_identity_column = "special_identity";
	$type_name = "special"; $plural_name = "specials";
	$staff_identity_column = null;
	$required_perm = PERM_SPECIALS;
	$feature_type = "special";
	$landing_page = "special/manage/";
	break;
  }
  default: exit("[Error] Type '" . $content_type . "' is not a valid content type yet.");
}

if (substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], $required_perm, 1) < 1)
  exit ("[Error] You do not have the proper permissions to manage " . $plural_name . ".");

if ($database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" .
	$database->escapeString($identity) . "'") == 0)
  exit ("[Error] There is no " . $type_name . " with the identity '" . $identity . "'.");
$identity = $database->escapeString($identity);

if ($staff_identity_column !== null && $database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE " .
	$type_database_identity_column . "='" . $identity . "' AND " . $staff_identity_column . "='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'") == 0)
  exit ("[Error] You are not the owner of the " . $type_name . " with the identity '" . $identity . "'.");

$content_title = format_content($database->querySingle("SELECT title FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" .
	$identity . "' LIMIT 1"));
$is_featured = ($database->querySingle("SELECT count(*) FROM features WHERE feature_type='" . $feature_type .
	"' AND item_identity='" . $identity . "'") > 0);
echo "<div class=\"windowHeader\">" . ($is_featured ? "Remove Featured Status" : "Feature this " . $type_name . "?") . "</div>\n";

if (!$is_featured)
{
  echo "<div class=\"contentText separatorBottom\">You have opted to feature the <span class=\"specialText\">" .
	$type_name . "</span> entitled '<span class=\"specialText\">" . $content_title . "</span>'.</div>\n";
  echo "<div class=\"normalText bottomBorder\">Complete the form below to feature your beat.</div>\n";
  echo "<div class=\"createForm separator noWindow\">\n";
  echo "<iframe id=\"frame-feature-image-form\" name=\"imageForm\" class=\"formFrame\" style=\"display:none;\"></iframe>\n";
  echo "  <div class=\"formLabel separatorBottom\">Feature Header Image File:</div>\n";
  echo "  <input type=\"hidden\" id=\"feature-image-filepath\" value=\"\" />\n";
  echo "  <div class=\"inputFile\" id=\"feature-image-input-form\">\n";
  echo "    <div class=\"button\">Select</div>\n";
  echo "    <div class=\"textBox\" id=\"image-input-textbox\"></div>\n";
  echo "    <form method=\"POST\" enctype=\"multipart/form-data\" target=\"imageForm\" action=\"" . MANAGE_WEB_PATH . "/components/upload-feature-header-image.php\" " .
	"id=\"upload-feature-form\">\n";
  echo "      <input type=\"file\" class=\"inputFile\" name=\"feature-image\" onChange=\"document.getElementById('upload-feature-form').submit();" .
	"document.getElementById('upload-feature-form').disabled = true;\" id=\"feature-image\" />\n";
  echo "    </form>\n";
  echo "  </div>\n";
  
  echo "  <input type=\"hidden\" id=\"feature-header-crop-x\" value=\"\" />\n";
  echo "  <input type=\"hidden\" id=\"feature-header-crop-y\" value=\"\" />\n";
  echo "  <input type=\"hidden\" id=\"feature-header-crop-width\" value=\"\" />\n";
  echo "  <input type=\"hidden\" id=\"feature-header-crop-height\" value=\"\" />\n";
  
  echo "  <center class=\"separator\">\n";
  echo "    <img id=\"feature-image-crop\" class=\"featureHeaderCropBox\" style=\"display:none;\" />\n";
  
  echo "    <input type=\"button\" id=\"feature-submit\" disabled=\"disabled\" value=\"Submit\" class=\"inputButton separator\" onClick=\"processFeatureSubmit('" .
	$content_type . "','" . $identity . "');\" />\n";
  echo "  </center>\n";
  
  echo "</div>\n";
} else
{
  echo "<div class=\"contentText separatorBottom\">Are you sure you wish to remove featured status from " .
	"the <span class=\"specialText\">" . $type_name .
	"</span>\n";
  echo "<div class=\"separator separatorBottom\">'<span class=\"specialText\">" . $content_title . "</span>'?</div>\n";

  echo "</div>\n";

echo "<center>\n";
echo "  <input type=\"button\" class=\"inputButton buttonGreen\" value=\"Remove\" id=\"confirm-remove\" onClick=\"this.disabled=true;" .
	"document.getElementById('cancel-remove').disabled=true;executeAJAX('" . MANAGE_WEB_PATH .
	"/components/remove-feature.php?type=" . $content_type . "&identity=" . $identity . "', function finish(result)
	{
	  if (result == 'success')
	  {
	    window.location = '" . MANAGE_WEB_PATH . "/" . $landing_page . $identity . "/';
	    return;
	  }
	  alert(result);
	  document.getElementById('cancel-remove').disabled = false;
	  document.getElementById('confirm-remove').disabled = false;
	});\" />\n";
echo "  <input type=\"button\" class=\"inputButton buttonRed\" value=\"Cancel\" id=\"cancel-remove\" " .
	"onClick=\"this.disabled=true;document.getElementById('confirm-remove').disabled=true;openWindow.closeWindow();\" />\n";
echo "</center>\n";
}
?>