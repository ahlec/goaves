<?php
if (isset($_GET["parameters"]))
  $message_identity = $_GET["parameters"];
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]))
  exit ("In order to load this frame, you must be authenticated to the management panel.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'540px',\n";
  echo "  height:'300px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px'\n";
  echo "}}\n";
  exit();
}

echo "<div class=\"windowHeader\">Compose message</div>\n";
	
echo "<div class=\"createForm noWindow\">\n";
echo "  <div class=\"infoLine\"><b>To:</b> <select id=\"mail-message-to\" onChange=\"document.getElementById('confirm-send').disabled = (this.value == '[none]');\">\n";
echo "    <option value=\"[none]\">(Select staff member)</option>\n";
$staff_members = $database->query("SELECT identity, first_name, last_name FROM staff WHERE active='TRUE' ORDER BY last_name, first_name ASC");
$parametered_recipient = (isset($_GET["parameters"]) ? $_GET["parameters"] : false);
$has_recipient = false;
while ($staff_member = $staff_members->fetchArray())
{
  echo "    <option value=\"" . $staff_member["identity"] . "\"" . ($parametered_recipient !== false && $parametered_recipient ==
	$staff_member["identity"] ? " selected=\"selected\"" : "") . ">" . format_content($staff_member["last_name"]) . ", " .
	format_content($staff_member["first_name"]) . "</option>\n";
  if ($parametered_recipient !== false && $parametered_recipient == $staff_member["identity"])
	$has_recipient = true;
}
echo "  </select></div>\n";
echo "  <div class=\"infoLine separator\"><b>Message:</b></div>\n";
echo "  <textarea id=\"mail-message-text\" style=\"width:200px;\"></textarea>\n";
echo "  <script>\n";
echo '    tinyMCE.init({
            mode : "exact",
            elements : "mail-message-text",
            entity_encoding : "named",
            theme : "advanced",
            skin : "o2k7",
            editor_selector : "advanced-textarea",
            plugins : "safari,spellchecker,pagebreak,style,layer,save,advhr,advimage,advlink," +
	    	"iespell,inlinepopups,preview,media,searchreplace,print,contextmenu,paste,directionality," +
	    	"fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
            theme_advanced_buttons1 : "cut,copy,paste,pastetext,|,search,replace,|,undo,redo,|,cleanup,code,|," +
	    	"preview,|,fullscreen,removeformat,visualaid,|,styleprops,spellchecker,|,cite,abbr,acronym,del,inc,attribs,",
            theme_advanced_buttons2 : "bold,italic,underline,strikethrough,|,forecolor,link,unlink,|,sub,sup,|,charmap," +
	    	"iespell,|,bullist,numlist,|,outdent,indent",
	    theme_advanced_buttons3 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : false,
            template_external_list_url : "js/template_list.js",
            external_link_list_url : "js/link_list.js",
            remove_trailing_nbsp : true,
            height : "100"
          });';
echo "\n  </script>\n";
echo "  <div style=\"text-align:center;margin-top:10px;\">\n";
echo "    <input type=\"button\" class=\"inputButton buttonGreen\" value=\"Send\"" . (!$has_recipient ? " disabled=\"disabled\" " : "") . " id=\"confirm-send\" " .
	"onClick=\"if (tinyMCE.get('mail-message-text').getContent().length == 0) { alert('You cannot send a blank message'); return; } 
	executeAJAX(manage_web_path + '/components/compose-message.php?to=' + document.getElementById('mail-message-to').value +
	'&message=' + escapeStringURL(tinyMCE.get('mail-message-text').getContent()), function process(result)
	{
	  if (result == 'success')
	  {
	    alert('Message sent successfully');
		openWindow.closeWindow();
		return;
	  } else
	    alert(result);
	});\" />\n";
echo "    <input type=\"button\" class=\"inputButton buttonRed\" value=\"Cancel\" id=\"cancel-send\" " .
	"onClick=\"this.disabled=true;document.getElementById('confirm-send').disabled=true;openWindow.closeWindow();\" />\n";
echo "  </div>\n";
echo "</div>\n";

?>