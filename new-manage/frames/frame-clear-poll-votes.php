<?php
require_once ("../../config/main.inc.php");
require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
@session_start();
if (!isset($_SESSION[MANAGE_SESSION]) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_INTERACTIONS, 1) < "1")
  exit ("In order to load this frame, you must be authenticated to the management panel and have permissions to manage interactions.");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset ($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{window: {\n";
  echo "  width:'300px',\n";
  echo "  height:'150px',\n";
  echo "  posX:'center',\n";
  echo "  posY:'center',\n";
  echo "  padding:'5px'\n";
  echo "}}\n";
  exit();
}

$parameters = explode("-", $_GET["parameters"]);
$emptying_poll = ($parameters[0] == "poll");
if ($emptying_poll)
{
  if ($database->querySingle("SELECT count(*) FROM polls WHERE identity='" . $database->escapeString($parameters[1]) . "'") == 0)
    exit ("[Error] The specified poll does not exist within the database.");
  $number_votes = $database->querySingle("SELECT count(*) FROM poll_votes WHERE poll_identity='" . $database->escapeString($parameters[1]) . "'");
  $title = format_content($database->querySingle("SELECT poll_question FROM polls WHERE identity='" . $database->escapeString($parameters[1]) . "' LIMIT 1"));
} else
{
  if ($database->querySingle("SELECT count(*) FROM poll_responses WHERE response_identity='" . $database->escapeString($parameters[0]) . "'") == 0)
    exit ("[Error] The specified poll response does not exist within the database.");
  $number_votes = $database->querySingle("SELECT count(*) FROM poll_votes WHERE response_identity='" . $database->escapeString($parameters[0]) . "'");
  $title = format_content($database->querySingle("SELECT response FROM poll_responses WHERE response_identity='" . $database->escapeString($parameters[0]) . "' LIMIT 1"));
}

echo "<div class=\"windowHeader\">Clear " . ($emptying_poll ? "all poll" : "response") . " votes?</div>\n";

echo "<div class=\"contentText separatorBottom\">Are you sure you wish to delete all <span class=\"specialText\">" . $number_votes .
	" vote" . ($number_votes != 1 ? "s" : "") . "</span> " . ($emptying_poll ? "in the poll" : "for the poll response") . " <span class=\"specialText\">" .
	$title . "</span>?\n";
echo "</div>\n";

echo "<div class=\"contentText warning miniSeparator separatorBottom\">(<b>Warning:</b> This action cannot be undone.)</div>\n";

echo "  <div style=\"text-align:center;margin-top:10px;\">\n";
echo "    <input type=\"button\" class=\"inputButton buttonGreen\" value=\"Delete\" id=\"confirm-delete\" " .
	"onClick=\"executeAJAX(manage_web_path + '/components/clear-poll-votes.php?scope=" . ($emptying_poll ? "poll" : "response") . "&identity=" .
	($emptying_poll ? $parameters[1] : $parameters[0]) . "', function process(result)
		{
			if (result == 'success')
			{
			  window.location = manage_web_path + '/interactions/poll-manage/" . ($emptying_poll ? $parameters[1] :
				$database->querySingle("SELECT poll_identity FROM poll_responses WHERE response_identity='" . $database->escapeString($parameters[0]) . "' LIMIT 1")) . "/';
			  return;
			} else
			  alert(result.replace('[Error] ', ''));
		});\" />\n";
echo "    <input type=\"button\" class=\"inputButton buttonRed\" value=\"Cancel\" id=\"cancel-delete\" " .
	"onClick=\"this.disabled=true;document.getElementById('confirm-delete').disabled=true;openWindow.closeWindow();\" />\n";
echo "  </div>\n";

?>