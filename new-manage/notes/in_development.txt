Welcome!<br /><br />
This has been in development for quite some time, and will continue to be under development for at least another month. Regardless, most of the features from the old management
panel have made it to the new one, and we're officially switching over. The changes that have been made are too many to enumerate right here, and doing so would take all the fun
out of exploring. Explore around for now, pardon our dust, and as usual, if you have any questions, feel free to ask (or use the new 'explain' buttons found on most of the
pages -- at least, they will be, eventually).