<?php
  $start_time = microtime();
  ini_set("session.cookie_domain", "goaves.com");
  @session_start();
  require_once ("../config/main.inc.php");
  require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
  require_once (MANAGE_DOCUMENT_ROOT . "/pages/pages.inc.php");
  require_once (MANAGE_DOCUMENT_ROOT . "/pages/navigation.inc.php");
  $database = new DeitloffDatabase(DATABASE_PATH);
  
/* Feedback */
  $done_feedback = array();
  if (isset($_SESSION[MANAGE_SESSION]))
  {
    $all_feedback = $database->query("SELECT DISTINCT feedback_handle FROM manage_feedback WHERE target_staff_identity='" .
		$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'");
	while ($feedback = $all_feedback->fetchArray())
	{
	  switch ($feedback["feedback_handle"])
	  {
	    case "reload_permissions":
		{
		  $_SESSION[MANAGE_SESSION]["PERMISSIONS"] = $database->querySingle("SELECT permissions FROM staff WHERE identity='" .
			$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' LIMIT 1");
		  $database->query("DELETE FROM manage_feedback WHERE feedback_handle='reload_permissions' AND target_staff_identity='" .
			$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "'");
		  $done_feedback[] = "Staff permissions have been reloaded.";
		  break;
		}
	  }
	}
  }
  
/* Load Page */
  $called_path = (isset($_GET["path"]) ? $_GET["path"] : "");
  if (!isset($_SESSION[MANAGE_SESSION]))
    $called_path = "login/" . $called_path;
  if (isset($_SESSION[MANAGE_SESSION]) && !is_array($_SESSION[MANAGE_SESSION]))
    $called_path = "logout/" . $called_path;
  $path_pieces = parsePath($called_path, $_PAGES, $_ERROR_PAGES);
  if ($path_pieces === false)
    exit ("<404>");
  $page = selectPage($path_pieces, $_PAGES, $database);
  
  if (!method_exists($page, "getHideNavigation"))
  {
      $navigation_menu = GenerateNavigationMenuByPermissions($database->querySingle("SELECT permissions FROM staff WHERE identity='" .
  	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' LIMIT 1"));
	$initial_navigation_offset = 0;
	if (sizeof($navigation_menu) > 8 && $page->getPageHandle() != null)
	{
	  $groups_processed_so_far = 0;
	  $current_group = $navigation_menu[0];
	  $found_navigation_item = true;
	  while ($page->getPageHandle() != $current_group->getReferenceHandle())
	  {
	    $groups_processed_so_far++;
		if ($groups_processed_so_far == sizeof($navigation_menu))
		{
		  $found_navigation_item = false;
		  break;
		}
		$current_group = $navigation_menu[$groups_processed_so_far];
	    if ($groups_processed_so_far > 8)
		 $initial_navigation_offset++;
	  }
	  if (!$found_navigation_item)
	   $initial_navigation_offset = 0;
	}
  }

/* HTML Header */
  header("Cache-Control: no-cache, must-revalidate"); 
  echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
  echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\n";
  echo "<HTML XMLNS:Manage>\n";
  echo "<head>\n";
  
  echo "<?IMPORT namespace='manage' implementation='" . MANAGE_WEB_PATH . "/button.htc' >\n";
  
  echo "  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=8\" />\n";
  echo "  <meta http-equiv=\"Pragma\" content=\"no-cache\">\n";
  echo "  <meta http-equiv=\"cache-control\" content=\"no-cache\">\n";
  echo "  <meta http-equiv=\"expires\" content=\"0\">\n";
  echo "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n";
  echo "  <meta http-equiv=\"Content-Language\" content=\"en-US\" />\n";
  echo "  <meta name=\"description\" content=\"The Online Voice of Sycamore\" />\n";
  echo "  <meta name=\"copyright\" content=\"(c)2010 Sycamore Community High School\" />\n";
  echo "  <meta name=\"author\" content=\"Sycamore High School Journalism Staff\" />\n";
  
  $page_title = $page->getPageTitle();
  echo "  <title>" . ($page_title !== null ? $page_title . " &ndash; " : "") . "GoAves.com Management Panel</title>\n";
  
  echo "  <link rel=\"shortcut icon\" href=\"" . WEB_PATH . "/layout/shortcut-icon.ico\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/stylesheet-main.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/stylesheet-forms.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/stylesheet-windows.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/stylesheet-display.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/stylesheet-timeline.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/stylesheet-lists.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/stylesheet-buttons.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/Jcrop/css/jquery.Jcrop.css\" media=\"screen\" />\n";
  if ($page->getPageStylesheet() != null)
    echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/" . $page->getPageStylesheet() .
  	"\" />\n";  
  
  echo "  <script>\n";
  echo "    var manage_web_path = '" . MANAGE_WEB_PATH . "';\n";
  echo "    var web_path = '" . MANAGE_WEB_PATH . "';\n";
  if (isset($navigation_menu))
  {
    echo "    var navigationOffset = " . $initial_navigation_offset . ";\n";
    echo "    var navigation_groups = Array(";
    foreach ($navigation_menu as $index => $navigation_group)
      echo "'" . $navigation_group->getReferenceHandle() . "'" . ($index < sizeof($navigation_menu) - 1 ? ", " : "");
    echo ");\n";
  }
  echo "  </script>\n";
  echo "  <script src=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/javascript-main.js\"></script>\n";
  echo "  <script src=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/javascript-windows.js\"></script>\n";
  echo "  <script src=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/javascript-unparented.js\"></script>\n";
  echo "  <script src=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/javascript-form.js\"></script>\n";
  echo "  <script src=\"" . WEB_PATH . "/layout/tiny_mce/tiny_mce.js\"></script>\n";
  echo "  <script src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.min.js\"></script>\n";
  echo "  <script src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.Jcrop.js\"></script>\n";
  if (method_exists($page, "getPageJavascriptFile"))
    echo "  <script src=\"" . WEB_PATH . "/layout/" . MANAGE_THEME . "/" . $page->getPageJavascriptFile() . "\"></script>\n";
  if (method_exists($page, "getPageJavascript"))
  {
    echo "  <script>\n";
	echo $page->getPageJavascript();
	echo "  </script>\n";
  }
  
  echo "</head>\n";
  
  /* Display the first unread note for this page */
  if (isset($_SESSION[MANAGE_SESSION]))
    $get_note = $database->querySingle("SELECT note_handle FROM manage_notes LEFT JOIN manage_note_views ON " .
  	"(manage_note_views.staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' AND " .
  	 "manage_notes.note_identity = manage_note_views.note_identity) WHERE manage_note_views.staff_identity IS NULL " .
  	"AND (on_pages='[all]' OR on_pages LIKE '%[" .
  	$page->getPageHandle() . "/]%' OR on_pages LIKE '%[" . $page->getPageHandle() . "/" . $page->getPageSubhandle() . "/]%') " .
  	"ORDER BY published_date ASC LIMIT 1");
  else
    $get_note = "";
  echo "<body onLoad=\"" . (isset($navigation_menu) ? "adjustPageNavigation();" : "") . ($get_note != "" ?
  	"new ManageWindow('note','" . $get_note . "');" : "") . (method_exists($page, "getBodyOnload") ?
  	$page->getBodyOnload() : "") . "\"" . (method_exists($page, "getPageOnBeforeUnload") ? " onBeforeUnload=\"" .
	$page->getPageOnBeforeUnload() . "\"" : "") . " >\n";

  /* Unread Messages */
  if (isset($_SESSION[MANAGE_SESSION]) && $database->querySingle("SELECT count(*) FROM staff_messages WHERE recipient_identity='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' AND read_yet='FALSE'") > 0)
  {
    $number_unread_messages = $database->querySingle("SELECT count(*) FROM staff_messages WHERE recipient_identity='" .
		$database->escapeString($_SESSION[MANAGE_SESSION]["IDENTITY"]) . "' AND read_yet='FALSE'");
    echo "    <a href=\"" . MANAGE_WEB_PATH . "/mail/inbox/\" class=\"floatNotification\">You have <b>" . $number_unread_messages . "</b> unread message" .
		($number_unread_messages != 1 ? "s" : "") . " in your inbox.</a>\n";
  }  

  foreach ($done_feedback as $change)
    echo "    <div class=\"feedback-result\">" . $change . "</div>\n";
  
  echo "    <div class=\"pageHeader\" onClick=\"window.location='" . MANAGE_WEB_PATH . "/'\"></div>\n";
  
  echo "    <div class=\"breadcrumbs\">\n";
  echo "      <a href=\"" . WEB_PATH . "/\" target=\"_blank\">Goaves.com</a> &rsaquo; \n";
  echo "      <a href=\"" . MANAGE_WEB_PATH . "/\">Management Panel</a> &rsaquo; \n";
  foreach ($page->getBreadTrail() as $handle => $title)
    echo "      " . ($handle != "[this]" ? "<a href=\"" . MANAGE_WEB_PATH . "/" . $handle . "/\">" : "<b>") .
		$title . ($handle != "[this]" ? "</a> &rsaquo; " : "</b>") . "\n";
  echo "    </div>\n";
  
  if (!method_exists($page, "getHideNavigation"))
  {
    echo "    <div class=\"pageNavigation\">\n";
	echo "      <div class=\"navigationMove left" . ($initial_navigation_offset > 0 ? "" : " disabled") .
		"\" id=\"navigationMove-Left\" onClick=\"navigationMove(-1);\"></div>\n";
    foreach ($navigation_menu as $index => $navigation_group)
    {
      echo "      <div class=\"navigationGroup" . ($navigation_group->getReferenceHandle() == $page->getPageHandle() ?
      	" selectedGroup" : "") . ($index > 8 + $initial_navigation_offset || $index < $initial_navigation_offset ? " hidden" : "") . "\" onMouseOver=\"openNavigationMenu('" .
      	$navigation_group->getReferenceHandle() . "');\" onMouseOut=\"closeNavigationMenu('" .
      	$navigation_group->getReferenceHandle() . "');\" id=\"navigationGroup-" . $navigation_group->getReferenceHandle() . "\">" .
      	"<a href=\"" . MANAGE_WEB_PATH . "/" . $navigation_group->getReferenceHandle() . "/\" class=\"groupLink\" />" .
      	$navigation_group->getName() . "</a>\n";
      if (sizeof($navigation_group->getItems()) > 0)
      {
        echo "        <div class=\"navigationMenu\" id=\"navigationMenu-" . $navigation_group->getReferenceHandle() . "\" style=\"" .
      	  "display:none;\">\n";
        foreach ($navigation_group->getItems() as $navigation_item)
          echo "          <a class=\"item\" href=\"" . MANAGE_WEB_PATH . "/" . $navigation_group->getReferenceHandle() . "/" .
           $navigation_item->getPageHandle() . "/\">&raquo; " . $navigation_item->getName() . "</a>\n";
        echo "        </div>\n";
      }
      echo "      </div>\n";
    }
	echo "      <div class=\"navigationMove right" . (sizeof($navigation_menu) > 9 + $initial_navigation_offset ? "" : " disabled") .
		"\" id=\"navigationMove-Right\" onClick=\"navigationMove(1);\"></div>\n";
    echo "    </div>\n";
  }
   
  echo "    <div class=\"pageContainer" . (method_exists($page, "getHideSidebar") ? " noSidebar" : "") . "\">\n";
  if (!method_exists($page, "getHideSidebar"))
  {
    echo "      <div class=\"sidebar\">\n";
    echo "        <div class=\"name\">" . $_SESSION[MANAGE_SESSION]["FIRST_NAME"] . " " . $_SESSION[MANAGE_SESSION]["LAST_NAME"] .
    	"</div>\n";
    echo "        <img src=\"" . WEB_PATH . "/images/staff/" . $_SESSION[MANAGE_SESSION]["ICON"] . "\" class=\"staffAvatar\" />\n";
    echo "        <a href=\"" . MANAGE_WEB_PATH . "/staff/view-statistics/\" class=\"bulletLink\">View statistics</a><br />\n";
    echo "        <br />\n";
    echo "        <a href=\"" . MANAGE_WEB_PATH . "/staff/change-password/\" class=\"bulletLink\">Change password</a><br />\n";
    echo "        <a href=\"" . MANAGE_WEB_PATH . "/staff/change-email/\" class=\"bulletLink\">Change e-mail</a><br />\n";
    echo "        <a href=\"" . MANAGE_WEB_PATH . "/staff/change-picture/\" class=\"bulletLink\">" .
    	($_SESSION[MANAGE_SESSION]["ICON"] == "no-staff-icon.jpg" ? "Upload" : "Change") . " staff picture</a><br />\n";
    echo "        <a href=\"" . MANAGE_WEB_PATH . "/staff/change-profile/\" class=\"bulletLink\">Change profile</a><br />\n";
    echo "        <a href=\"" . MANAGE_WEB_PATH . "/staff/manage-positions/\" class=\"bulletLink\">Manage staff positions</a><br />\n";
    echo "        <a href=\"" . MANAGE_WEB_PATH . "/staff/manage-social-media/\" class=\"bulletLink\">Manage social media " .
    	"accounts</a><br />\n";
    echo "        <br />\n";
    echo "        <a href=\"" . MANAGE_WEB_PATH . "/logout/\" class=\"bulletLink\">Logout</a>\n";
	echo "        <div class=\"tipBox\">\n";
    $manage_tip = $database->querySingle("SELECT tip_identity, title, tip, link_href, link_text, has_link FROM manage_tips ORDER BY Random() LIMIT 1", true);
	echo "          <div class=\"tipHeader\">Tip #" . $manage_tip["tip_identity"] . " &ndash; " . $manage_tip["title"] . "</div>\n";
	echo "          " . $manage_tip["tip"] . "\n";
	if ($manage_tip["has_link"] == "TRUE")
	  echo "          <br /><a href=\"" . MANAGE_WEB_PATH . "/" . $manage_tip["link_href"] . "\" target=\"_blank\">" . $manage_tip["link_text"] . "&raquo;</a>\n";
	echo "        </div>\n";
    echo "      </div>\n";
  }
  $page->getPageContents();
  echo "      <div style=\"clear:both;\"></div>\n";
  echo "    </div>\n";
  
/* Footer */
  echo "    <div class=\"pageFooter\"><a href=\"" . WEB_PATH . "/\" target=\"_blank\">Goaves.com</a> &ndash; All rules and " .
  	"regulations which apply to goaves.com likewise apply here.</div>\n";
 
  echo "</body>\n";
  echo "</html>\n";
?>