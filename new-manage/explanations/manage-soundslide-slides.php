<?php
echo "<b class=\"miniSeparator\">What is 'Start Time', and how do I use it?</b><br />Start time is the position in the presentation where the slide in question will appear.
The format of this is a simple 00:00 -- where the first two numbers indicate the minutes, and the second two the seconds -- that the slide should appear at. The duration the
slide is present is not present, as during the slideshow, the last slide loaded will be displayed until a new slide is displayed. Use start time to space out your slides.<br />
<br />\n";
echo "<b class=\"miniSeparator\">What should I use for start time?</b><br />That is up to you. A simple solution is to divide the length of the slideshow by the number of slides
that you have, and then to give each slide each time. A better solution, however, is to match slides to the audio of the presentation, so that slides depicting what is being
narrated are displayed. This shows a professional level of design.<br /><br />\n";
echo "<b class=\"miniSeparator\">How do I change the slide image?</b><br />Unfortunately, changing slide images is not possible. This is due to the fact that, as the slides 
are primarily picture-based, changing slide images would be a more complicated process than simply deleting the slide and creating a new one in its place. Simply delete the
slide(s) that you wish to change pictures for, then create a new slide(s) in the old one(s) place(s). <i>Though slide images cannot be changed, slide icons <u>may</u> be
recropped.</i><br /><br />\n";
echo "<b class=\"miniSeparator\">How do I delete slides?</b><br />If you are looking to delete slides, this is the wrong page. To delete slides, return to the management page
for this presentation. Select the slides that you wish to delete by clicking on them, and then click the 'Delete selected slides' option that appears at the bottom of the
list of slides.<br /><br />\n";
?>