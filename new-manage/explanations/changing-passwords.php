<?php
echo "<b class=\"miniSeparator\">Must be six (6) or more characters.</b><br />Passwords must not be too short. Therefore, " .
	"all passwords must be six or more characters.<br /><br />\n";
echo "<b class=\"miniSeparator\">Passwords must contain at least one (1) nonalpha character.</b><br />Passwords that " .
	"contain characters only a-z are much easier to break than passwords containin non-alpha characters.<br /><br />\n";
echo "<b class=\"miniSeparator\">Passwords must not contain certain phrases.</b><br />Such phrases include 'default', " .
	"'password', and either your first or last name.<br /><br />\n";
?>