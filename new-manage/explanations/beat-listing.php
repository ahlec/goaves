<?php
echo "<b class=\"miniSeparator\">Sorted by school year.</b><br />Due to the high number of beats each staff member produces, " .
	"beats are sorted by which school year they were written during, and then further sorted by the month they were " .
	"posted.<br /><br />\n";
echo "<b class=\"miniSeparator\">Month-based sorting.</b><br />After the beats have been sorted by school year and month, " .
	"all months other than the month the last beat was written during are closed. If the beat in question was written " .
	"during this month, simply select the beat and move on; if the beat was from a previous month, the different months may " .
	"be opened or closed by clicking on the title of the month (which contains the month name) or by clicking on the " .
	"expand or close button next to the name.<br /><br />\n";
echo "<b class=\"miniSeparator\">Selecting a different school year.</b><br />All school years during which you wrote a beat " .
	"are listed at the top of your beats listing. Clicking on one of the school years will take you to the list of your beats " .
	"from that year. By default, every time you load your beats list, it automatically takes you to the list for the current " .
	"school year.<br /><br />\n";
echo "<b class=\"miniSeparator\">Selecting a beat.</b><br />When you have found the beat you wish to manage, clicking on the " .
	"beat (either the title, the image, or any of the text given on the beat) will take you to the page where you can see " .
	"more detailed information, and where you can publish/unpublish, delete, or edit the beat.\n";
?>