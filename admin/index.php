<?php
require_once ("../config/manage.inc.php");
require_once (DOCUMENT_ROOT . "/config/admin.inc.php");
processManageSession();
if ($_SESSION[MANAGE_PERMISSIONS_SESSION][9] != "1")
  header("Location: " . WEB_PATH . "/manage/index.php?admin=denied");

require_once (DOCUMENT_ROOT . "/admin/pages/hub.inc.php");
require_once (DOCUMENT_ROOT . "/admin/pages/pages.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);

/* Load Page */
  $called_path = (isset($_GET["path"]) ? $_GET["path"] : "");
  $path_pieces = parsePath($called_path, $_PAGES, $_ERROR_PAGES);
  $page = selectPage($path_pieces, $_PAGES, $database);

/* Begin Output */
  echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
  echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\n";
  echo "<head>\n";
  
  echo "  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=8\" />\n";
  echo "  <meta http-equiv=\"Pragma\" content=\"no-cache\">\n";
  echo "  <meta http-equiv=\"cache-control\" content=\"no-cache\">\n";
  echo "  <meta http-equiv=\"expires\" content=\"0\">\n";
  echo "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n";
  echo "  <meta http-equiv=\"Content-Language\" content=\"en-US\" />\n";
  echo "  <meta name=\"description\" content=\"The Online Voice of Sycamore\" />\n";
  echo "  <meta name=\"copyright\" content=\"(c)2010 Sycamore Community High School\" />\n";
  echo "  <meta name=\"author\" content=\"Sycamore High School Journalism Staff\" />\n";

  $page_title = $page->getPageTitle();
  echo "  <title>" . ($page_title !== null ? $page_title . " &ndash; " : "") . "GoAves.com Admin Panel</title>\n";

  echo "  <link rel=\"shortcut icon\" href=\"" . WEB_PATH . "/layout/shortcut-icon.ico\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/admin/stylesheet-main.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/admin/stylesheet-filesystem.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/admin/stylesheet-database.css\" />\n";

/* Body Output */
  echo "<body>\n";
  echo "  <div class=\"adminPageContainer\">\n";
  echo "    <div class=\"middlePageContainer\">\n";
  echo "      <div class=\"innerPageContainer\">\n";
  echo "        <div class=\"panelHeader\">\n";
  echo "          <div class=\"middleHeader\">\n";
  echo "            <div class=\"innerHeader\">\n";
  echo "              <div class=\"superscript\"><a href=\"" . WEB_PATH . "/\" target=\"_blank\">GoAves.com</a></div>\n";
  echo "              <div class=\"mainline\">Webmaster Panel</div>\n";
  echo "            </div>\n";
  echo "          </div>\n";
  echo "        </div>\n";
  echo "        <div class=\"panelNavigation\">\n";
  foreach ($_navigation_panel as $nav_item_handle => $nav_item_text)
    echo "         <a href=\"" . WEB_PATH . "/admin/" . $nav_item_handle . "/\"><div class=\"item\">" . $nav_item_text . "</div></a>\n";
  echo "        </div>\n";

/* Handling Page Feedback */
  if (isset($_SESSION[ADMIN_PAGE_FEEDBACK]))
  {
    echo "<div class=\"";
    switch ($_SESSION[ADMIN_PAGE_FEEDBACK]["STATUS"])
    {
      case FEEDBACK_STATUS_SUCCESS: echo "success"; break;
      case FEEDBACK_STATUS_ERROR: echo "error"; break;
      case FEEDBACK_STATUS_NOTICE: echo "notice"; break;
    }
    echo "\"><b>" . $_SESSION[ADMIN_PAGE_FEEDBACK]["FEEDBACK_TITLE"] . "</b>. " . $_SESSION[ADMIN_PAGE_FEEDBACK]["FEEDBACK_DESCRIPTION"] .
        "</div>\n";
    unset($_SESSION[ADMIN_PAGE_FEEDBACK]);
  }

/* Handle Current Page */
  echo "        <div class=\"panelPageContent\">\n";
  $page->outputPage();
  echo "        </div>\n";
  echo "        <div style=\"clear:both;\"></div>\n";

  echo "      </div>\n";
  echo "    </div>\n";
  echo "  </div>\n";
  echo "</body>\n";
  echo "</html>\n";
?>