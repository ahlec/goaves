<?php
class AdminPageCreateFile
{
  private $_current_directory;
  private $_file_create_error = null;
  function checkOrRedirect($path_pieces, $database)
  {
    for ($piece = 1; $piece < sizeof($path_pieces); $piece++)
      $this->_current_directory .= $path_pieces[$piece] . "/";
    $this->_current_directory = rtrim($this->_current_directory, "/");
    if (!file_exists("F:/goaves/" . $this->_current_directory) || !is_dir("F:/goaves/" . $this->_current_directory))
      return new AdminPageBrowse();
    
    if (isset($_POST["create-file"]))
    {
      if (!file_exists("F:/goaves/" . $this->_current_directory . "/" . $_POST["filename"]))
      {
        fclose(fopen("F:/goaves/" . $this->_current_directory . "/" . $_POST["filename"], 'w'));
        $_SESSION[ADMIN_PAGE_FEEDBACK] = array("FROM_PAGE" => "create", "STATUS" => FEEDBACK_STATUS_SUCCESS, "FEEDBACK_TITLE" => "File created",
            "FEEDBACK_DESCRIPTION" => "'" . $_POST["filename"] . "' has been created.");
        header("Location: " . WEB_PATH . "/admin/browse/" . $this->_current_directory);
      } else
        $this->_file_create_error = "<div class=\"error\"><b>File already exists.</b> There is already a file in directory '<b>" .
            $this->_current_directory . "</b>' entited '<b>" . $_POST["filename"] . "</b>'.</div>\n";
    }

    return true;
  }
  function getPageTitle() { return "Create Page"; }
  function outputPage()
  {
    echo "<form method=\"post\">\n";
    echo "<b>Current Directory:</b> /" . $this->_current_directory . "<br />\n";
    echo "<b>Desired Filename:</b> <input type=\"text\" name=\"filename\" /><br />\n";
    echo "<input type=\"submit\" name=\"create-file\" value=\"Create File\" />\n";
    echo "</form>\n";
  }
}
?>