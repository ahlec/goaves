<?php
class AdminPageCreateDirectory
{
  private $_current_directory;
  private $_create_error = null;
  function checkOrRedirect($path_pieces, $database)
  {
    for ($piece = 1; $piece < sizeof($path_pieces); $piece++)
      $this->_current_directory .= $path_pieces[$piece] . "/";
    $this->_current_directory = rtrim($this->_current_directory, "/");
    if (!file_exists("F:/goaves/" . $this->_current_directory) || !is_dir("F:/goaves/" . $this->_current_directory))
      return new AdminPageBrowse();
    
    if (isset($_POST["create-directory"]))
    {
      if (!file_exists("F:/goaves/" . $this->_current_directory . "/" . $_POST["directory-name"]))
      {
        mkdir("F:/goaves/" . $this->_current_directory . "/" . $_POST["directory-name"]);
        $_SESSION[ADMIN_PAGE_FEEDBACK] = array("FROM_PAGE" => "create-directory", "STATUS" => FEEDBACK_STATUS_SUCCESS,
            "FEEDBACK_TITLE" => "Directory created", "FEEDBACK_DESCRIPTION" => "'" . $_POST["filename"] . "' has been created.");
        header("Location: " . WEB_PATH . "/admin/browse/" . $this->_current_directory);
      } else
        $this->_create_error = "<div class=\"error\"><b>Directory already exists.</b> There is already a directory in directory '<b>" .
            $this->_current_directory . "</b>' entited '<b>" . $_POST["directory-name"] . "</b>'.</div>\n";
    }

    return true;
  }
  function getPageTitle() { return "Create Page"; }
  function outputPage()
  {
    if ($this->_create_error != null)
      echo $this->_create_error;
    echo "<form method=\"post\">\n";
    echo "<b>Current Directory:</b> /" . $this->_current_directory . "<br />\n";
    echo "<b>Desired Directory Name:</b> <input type=\"text\" name=\"directory-name\" /><br />\n";
    echo "<input type=\"submit\" name=\"create-directory\" value=\"Create Directory\" />\n";
    echo "</form>\n";
  }
}
?>