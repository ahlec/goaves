<?php
/* **************************************
 *  Valid Pages Include                 *
 * **************************************/
 
$_PAGES = array("index" => "AdminPageIndex", "home" => "AdminPageIndex", "browse" => "AdminPageBrowse", "edit" => "AdminPageEdit",
    "create" => "AdminPageCreateFile", "create-directory" => "AdminPageCreateDirectory", "upload" => "AdminPageUploadFile",
    "admin-corkboard" => "AdminPageCorkboard", "page-not-found" => "AdminPagePageNotFound", "delete" => "AdminPageDelete",
    "database-schema" => "AdminPageDatabaseSchema", "database-table" => "AdminPageDatabaseTable",
    "filesystem-unpack" => "AdminPageFilesystemUnpack", "database-insert" => "AdminPageDatabaseInsert", "database-browse" => "AdminPageDatabaseBrowse",
    "database-sql" => "AdminPageDatabaseSql");
$_ERROR_PAGES = array(404 => "page-not-found");

$_navigation_panel = array("home" => "home", "browse" => "file system", "database-schema" => "database");

function parsePath($path, $page_definitions, $error_pages)
{
  $path_pieces = explode("/", $path);
  if (sizeof($path_pieces) == 0 || $path_pieces[0] == "")
    $path_pieces[0] = "index";
  else
    $path_pieces[0] = mb_strtolower($path_pieces[0]);
  if (!in_array($path_pieces[0], array_keys($page_definitions)))
    $path_pieces = array($error_pages[404], $path_pieces[0]);
  return $path_pieces;
}
function selectPage($path_pieces, $page_definitions, $database)
{
  // Find Proper Page
  $selected_page = new $page_definitions[$path_pieces[0]]();
  $selected_page_final = $selected_page->checkOrRedirect($path_pieces, $database);
  while($selected_page_final !== true)
  {
    $selected_page = $selected_page_final;
	$selected_page_final = $selected_page->checkOrRedirect($path_pieces, $database);
  }
  return $selected_page;
}
?>