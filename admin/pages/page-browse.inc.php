<?php
class AdminPageBrowse
{
  private $_full_path;
  private $_url_path;
  private $_items = array();
  private $_number_files = 0;
  private $_number_directories = 0;
  function checkOrRedirect($path_pieces, $database)
  {
    $this->_full_path = "F:/goaves/";
    for ($piece = 1; $piece < sizeof($path_pieces); $piece++)
      if (file_exists($this->_full_path . $path_pieces[$piece] . "/") && is_dir($this->_full_path . $path_pieces[$piece]))
      {
        $this->_url_path .= $path_pieces[$piece] . "/";
        $this->_full_path .= $path_pieces[$piece] . "/";
      }
    $this->_url_path = rtrim($this->_url_path, "/");
    $items = scandir($this->_full_path);
    foreach ($items as $item)
    {
      $file_info = pathinfo($this->_full_path . $item);
      if (isset($file_info["extension"]))
      {
        switch ($file_info["extension"])
        {
          case "zip": $item_default_action = "filesystem-unpack"; break;
          case "php":
          case "txt":
          case "js":
          case "txt":
          case "htm":
          case "html":
          case "css": $item_default_action = "edit"; break;
          default: $item_default_action = null; break;
        }
      } else if ($item == "." || $item == "..")
      {
        $item_default_action = "browse";
      } else
        $item_default_action = "edit";
     
      if (is_dir($this->_full_path . $item))
      {
        if ($item != "." && $item != "..")
          $this->_number_directories += 1;
      } else
        $this->_number_files += 1;
      $this->_items[] = array("NAME" => $item, "IS_DIR" => is_dir($this->_full_path . $item), "DEFAULT_ACTION" => $item_default_action);
    }
    function sortDirectoryList($item_a, $item_b)
    {
      if ($item_a["IS_DIR"] && !$item_b["IS_DIR"])
        return -1;
      if (!$item_a["IS_DIR"] && $item_b["IS_DIR"])
        return 1;
      if ($item_a["IS_DIR"] == $item_b["IS_DIR"])
        return ($item_a["NAME"] > $item_b["NAME"] ? 1 : -1);
    }
    usort($this->_items, "sortDirectoryList");
    return true;
  }
  function getPageTitle() { return "Browsing '/" . $this->_url_path . "'"; }
  function outputPage()
  {
    echo "<big><b>/" . $this->_url_path . "</b></big><br />";
    echo "<b>" . $this->_number_directories . " director" . ($this->_number_directories == 1 ? "y" : "ies") . ", " .
        $this->_number_files . " file" . ($this->_number_files == 1 ? "" : "s") . "</b><br />\n";
    echo "<a href=\"" . WEB_PATH . "/admin/create/" . $this->_url_path . "/\">Create File</a> &bull; \n";
    echo "<a href=\"" . WEB_PATH . "/admin/create-directory/" . $this->_url_path . "/\">Create Directory</a> &bull; \n";
    echo "<a href=\"" . WEB_PATH . "/admin/upload/" . $this->_url_path . "/\">Upload File</a>\n";
    foreach ($this->_items as $item)
    {
      echo "<div class=\"list_item\">";
      echo ($item["IS_DIR"] ? "[DIR] " : "");
      if ($item["DEFAULT_ACTION"] !== null || $item["IS_DIR"])
      {
        echo "<a href=\"" . WEB_PATH . "/admin/" . ($item["IS_DIR"] ? "browse" : $item["DEFAULT_ACTION"]) . "/";
        if ($item["NAME"] == "..")
          echo str_replace("F:/goaves/","",dirname($this->_full_path));
        else
          echo $this->_url_path . "/" . $item["NAME"];
        echo "/\">";
      }
      echo $item["NAME"];
      if ($item["DEFAULT_ACTION"] !== null || $item["IS_DIR"])
        echo "</a>";
      if ($item["NAME"] !== "." && $item["NAME"] !== "..")
      {
        echo " <span class=\"itemActions\">(";
        echo "<a href=\"" . WEB_PATH . "/admin/move/" . $this->_url_path . "/" . $item["NAME"] . "/\"><img src=\"" . WEB_PATH .
            "/images/icons/action_go.gif\" border=\"0\" />Move</a> &bull; ";
        echo "<a href=\"" . WEB_PATH . "/admin/delete/" . $this->_url_path . "/" . $item["NAME"] . "/\"><img src=\"" . WEB_PATH .
            "/images/icons/folder_delete.gif\" border=\"0\" />Delete</a>";
        echo ")</span>";
      }
      echo "</div>\n";
    }
  }
}
?>