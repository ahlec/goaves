<?php
class AdminPageDatabaseBrowse
{
  private $_table_name;
  private $_columns = array();
  private $_data = array();
  private $_browse_offset = 0;
  private $_browse_limit = 30;
  private $_primary_key;
  private $_number_of_records = 0;
  private $_number_of_records_after = 0;
  function checkOrRedirect($path_pieces, $database)
  {
    if (!isset($path_pieces[1]) || $database->querySingle("SELECT count(*) FROM sqlite_master WHERE type='table' AND name LIKE '" .
        $database->escapeString($path_pieces[1]) . "'") == 0)
    {
        $_SESSION[ADMIN_PAGE_FEEDBACK] = array("FROM_PAGE" => "database-table", "STATUS" => FEEDBACK_STATUS_ERROR,
            "FEEDBACK_TITLE" => "Table not found", "FEEDBACK_DESCRIPTION" => "The table '" . $path_pieces[1] .
            "' could not be found in the database.");
        return new AdminPageDatabaseSchema();
    }
    if (isset($path_pieces[2]) && ctype_digit($path_pieces[2]))
      $this->_browse_offset = $database->escapeString($path_pieces[2]);

    $table_information = $database->querySingle("SELECT name, sql FROM sqlite_master WHERE type='table' AND name LIKE '" . 
        $database->escapeString($path_pieces[1]) . "' LIMIT 1", true);
    $this->_table_name = $table_information["name"];
    $column_creation_sql = preg_split("/([\(\)])+/", $table_information["sql"]);
    $sql_exploded = explode(",", $column_creation_sql[1]);
    $table_primary_key = null;
    foreach ($sql_exploded as $column_piece)
    {
      $uses_brackets = (strpos($column_piece, "[") !== false);
      if ($uses_brackets)
      {
        preg_match("/\[([^\]])*\]/",$column_piece, $column_name);
        preg_match("/\] ([^\ ])*/",$column_piece, $column_type);
        $column_type[0] = str_replace("]","", $column_type[0]);
        $table_modifiers = preg_replace("/]([^\]])*\]/","", $column_piece);
      } else
      {
        preg_match("/'([^'])*'/",$column_piece, $column_name);
        preg_match("/' ([^\ ])*/",$column_piece, $column_type);
        $table_modifiers = preg_replace("/'([^'])*'/","", $column_piece);

      }
      if (strpos($table_modifiers, "PRIMARY KEY") !== false)
        $this->_primary_key = str_replace(array("[","]","'"), "", $column_name[0]);
      $this->_columns[] = array("NAME" => str_replace(array("[","]","'"), "", $column_name[0]), "TYPE" => str_replace(array(" ","'"), "", $column_type[0]),
          "PRIMARY" => (strpos($table_modifiers, "PRIMARY KEY") !== false), "UNIQUE" => (strpos($table_modifiers, "UNIQUE") !== false));
    }
    $this->_number_of_records = $database->querySingle("SELECT count(*) FROM " . $this->_table_name);
    $this->_number_of_records_after = $this->_number_of_records - $this->_browse_offset;
    $get_data = $database->query("SELECT * FROM " . $this->_table_name . " ORDER BY " . $this->_primary_key . " ASC LIMIT " . $this->_browse_limit . " OFFSET " . $this->_browse_offset);
    while ($data = $get_data->fetchArray())
      $this->_data[] = $data;
    return true;
  }
  function getPageTitle() { return "Browse Database Table '" . $this->_table_name . "'"; }
  function outputPage()
  {
    echo "<a href=\"" . WEB_PATH . "/admin/database-table/" . $this->_table_name . "/\">Return to table</a> &ndash; ";
    echo "<a href=\"" . WEB_PATH . "/admin/database-schema/\">Return to database schema</a><br /><br />\n";
    echo "<big><b>Database Table '" . $this->_table_name . "'</b></big><br />";
    echo "<small>Displaying records " . $this->_data[0][$this->_primary_key] . " &ndash; " . $this->_data[sizeof($this->_data) - 1][$this->_primary_key] . 
    	" (" . sizeof($this->_data) . " of " . $this->_number_of_records . " record" . ($this->_number_of_records == 1 ? "" : "s") . ")</small><br /><br />\n";
    echo "  <table class=\"database_records_table\">\n";
    echo "    <tr class=\"header\">\n";
    foreach ($this->_columns as $column_number => $column)
      echo "      <td" . ($column_number > 0 ? "" : " class=\"first\"") . "><b>" . $column["NAME"] . "</b></td>\n";
    echo "    </tr>\n";
    foreach ($this->_data as $data)
    {
      echo "    <tr class=\"data_row\">\n";
      foreach ($data as $column_name => $cell_data)
        echo "      <td" . ($column_name == $this->_columns[0]["NAME"] ? " class=\"first\"" : "") . ">" . htmlentities($cell_data) . "</td>\n";
      echo "    </tr>\n";
    }
    echo "  </table>\n";
    if ($this->_number_of_records_after > 0)
      echo "<a href=\"" . WEB_PATH . "/admin/database-browse/" . $this->_table_name . "/" . $this->_data[sizeof($this->_data) - 1][$this->_primary_key] . "/\">Next Page</a>\n";
  }
}
?>