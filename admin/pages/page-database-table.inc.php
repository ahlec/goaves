<?php
class AdminPageDatabaseTable
{
  private $_table_name;
  private $_columns = array();
  function checkOrRedirect($path_pieces, $database)
  {
    if (!isset($path_pieces[1]) || $database->querySingle("SELECT count(*) FROM sqlite_master WHERE type='table' AND name LIKE '" .
        $database->escapeString($path_pieces[1]) . "'") == 0)
    {
        $_SESSION[ADMIN_PAGE_FEEDBACK] = array("FROM_PAGE" => "database-table", "STATUS" => FEEDBACK_STATUS_ERROR,
            "FEEDBACK_TITLE" => "Table not found", "FEEDBACK_DESCRIPTION" => "The table '" . $path_pieces[1] .
            "' could not be found in the database.");
        return new AdminPageDatabaseSchema();
    }

    $table_information = $database->querySingle("SELECT name, sql FROM sqlite_master WHERE type='table' AND name LIKE '" . 
        $database->escapeString($path_pieces[1]) . "' LIMIT 1", true);
    $this->_table_name = $table_information["name"];
    echo "<pre>" . $table_information["sql"] . "</pre><br /><br />\n";
    $column_creation_sql = preg_split("/([\(\)])+/", $table_information["sql"]);
    $sql_exploded = explode(",", $column_creation_sql[1]);
    foreach ($sql_exploded as $column_piece)
    {
      $uses_brackets = (strpos($column_piece, "[") !== false);
      if ($uses_brackets)
      {
        preg_match("/\[([^\]])*\]/",$column_piece, $column_name);
        preg_match("/\] ([^\ ])*/",$column_piece, $column_type);
        $column_type[0] = str_replace("]","", $column_type[0]);
        $table_modifiers = preg_replace("/]([^\]])*\]/","", $column_piece);
      } else
      {
        preg_match("/'([^'])*'/",$column_piece, $column_name);
        preg_match("/' ([^\ ])*/",$column_piece, $column_type);
        $table_modifiers = preg_replace("/'([^'])*'/","", $column_piece);

      }
      $this->_columns[] = array("NAME" => str_replace("'", "", $column_name[0]), "TYPE" => str_replace(array(" ","'"), "", $column_type[0]),
          "PRIMARY" => (strpos($table_modifiers, "PRIMARY KEY") !== false), "UNIQUE" => (strpos($table_modifiers, "UNIQUE") !== false));
    }
    return true;
  }
  function getPageTitle() { return "Displaying Database '" . $this->_table_name . "'"; }
  function outputPage()
  {
    echo "<a href=\"" . WEB_PATH . "/admin/database-schema/\">Return to schema</a><br /><br />\n";
    echo "<big><b>Database Table '" . $this->_table_name . "'</b></big><br />";
    echo "<a href=\"" . WEB_PATH . "/admin/database-insert/" . $this->_table_name . "/\">Insert Record</a> &bull; \n";
    echo "<a href=\"" . WEB_PATH . "/admin/database-browse/" . $this->_table_name . "/\">Browse Records</a>\n";
    echo "<pre>";
    foreach ($this->_columns as $column)
    {
      echo $column["NAME"] . " <small>[" . $column["TYPE"];
      if ($column["PRIMARY"])
        echo ", Primary Key";
      if ($column["UNIQUE"])
        echo ", Unique";
      echo "]</small>";
      echo "\n";
    }
    echo "</pre>";
  }
}
?>