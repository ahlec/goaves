<?php
class AdminPageEdit
{
  private $_full_path;
  private $_url_path;
  private $_file_name;
  private $_save_error = false;
  private $_file_directory_path;
  private $_file_contents;
  private $_file_type;
  function checkOrRedirect($path_pieces, $database)
  {
    $this->_full_path = "F:/goaves/";
    for ($piece = 1; $piece < sizeof($path_pieces); $piece++)
    {
      $this->_url_path .= $path_pieces[$piece] . "/";
      $this->_full_path .= $path_pieces[$piece] . "/";
    }
    $this->_full_path = rtrim($this->_full_path, "/");
    if (!file_exists($this->_full_path))
      return new AdminPageBrowse();

    $this->_file_directory_path = str_replace("F:/goaves/", "", dirname($this->_full_path));
    $this->_file_name = rtrim(str_replace("F:/goaves/" . $this->_file_directory_path . "/", "", $this->_full_path), "/");
    $file_information = pathinfo($this->_full_path);
    $this->_file_type = (isset($file_information["extension"]) ? $file_information["extension"] : "txt");
    if (isset($_POST["file-save"]))
    {
      $new_file_contents = utf8_decode($_POST["file-contents"]);
      if (file_put_contents($this->_full_path, $new_file_contents) === false)
        $save_error = true;
      else
      {
        $database->exec("UPDATE statistics SET value='" . date("Y-m-d", strtotime("+1 hours")) . "' WHERE stat_handle='code_last_updated'");
      }
    }

    $this->_file_contents = file_get_contents($this->_full_path);
    return true;
  }
  function getPageTitle() { return "Edit File '" . $this->_file_name . "'"; }
  function outputPage()
  {
    echo "<script language=\"Javascript\" type=\"text/javascript\" src=\"" . WEB_PATH . "/layout/editarea_0_8_2/edit_area/edit_area_full.js\"></script>\n";
    echo "<script language=\"Javascript\" type=\"text/javascript\">\n";
    echo "  editAreaLoader.init({\n";
    echo "    id: \"admin-edit-textarea\",\n";	
    echo "    start_highlight: true,\n";
    echo "    allow_resize: \"both\",\n";
    echo "    allow_toggle: true,\n";
    echo "    word_wrap: true,\n";
    echo "    language: \"en\",\n";
    echo "    syntax: \"" . $this->_file_type . "\"\n";	
    echo "  });\n";
    echo "</script>\n";

    if ($this->_save_error)
      echo "<div class=\"error\"><b>Error Saving.</b> Could not save file.</div>\n";
    else if (isset($_POST["file-save"]))
      echo "<div class=\"success\"><b>Success.</b> File has been changed.</div>\n";
    echo "<b>Directory:</b> <a href=\"" . WEB_PATH . "/admin/browse/" . $this->_file_directory_path . "/\">/" . 
      $this->_file_directory_path . "</a><br />\n";
    echo "<b>File:</b> " . $this->_file_name . "<br />";
    echo "<form method=\"post\">\n";
    echo "<textarea name=\"file-contents\" style=\"width:100%;\" id=\"admin-edit-textarea\" rows=\"40\">" .
        htmlentities($this->_file_contents) . "</textarea>\n";
    echo "<input type=\"submit\" value=\"Save\" name=\"file-save\" />\n";
    echo "<input type=\"button\" value=\"Cancel\" onClick=\"window.location='" . WEB_PATH . "/admin/browse/" . $this->_file_directory_path . "/';\" />\n";
    echo "</form>\n";
  }
}
?>