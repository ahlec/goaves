<?php
class AdminPageDelete
{
  private $_target;
  function checkOrRedirect($path_pieces, $database)
  {
    for ($piece = 1; $piece < sizeof($path_pieces); $piece++)
      $this->_target .= $path_pieces[$piece] . "/";
    $this->_target = rtrim($this->_target, "/");
    if (!file_exists(DOCUMENT_ROOT . "/" . $this->_target))
      return new AdminPageBrowse();
    
    if (isset($_POST["cancel-delete"]))
      return new AdminPageBrowse();
    
    if (isset($_POST["delete-file"]))
    {
      if (file_exists(DOCUMENT_ROOT . "/" . $this->_target))
      {
        $delete_success = (is_dir(DOCUMENT_ROOT . "/" . $this->_target) ? rmdir(DOCUMENT_ROOT . "/" . $this->_target) :
            unlink(DOCUMENT_ROOT . "/" . $this->_target));
        $_SESSION[ADMIN_PAGE_FEEDBACK] = array("FROM_PAGE" => "delete", "STATUS" => ($delete_success ? FEEDBACK_STATUS_SUCCESS :
            FEEDBACK_STATUS_ERROR), "FEEDBACK_TITLE" => ($delete_success ? "Delete success" : "Delete failed"), "FEEDBACK_DESCRIPTION" =>
            ($delete_success ? "Deleted '<b>" . $this->_target . "</b>'." : "Could not delete '<b>" . $this->_target . "</b>'."));
        return new AdminPageBrowse();
      } else
      {
        $_SESSION[ADMIN_PAGE_FEEDBACK] = array("FROM_PAGE" => "delete", "STATUS" => FEEDBACK_STATUS_ERROR,
            "FEEDBACK_TITLE" => "File does not exist", "FEEDBACK_DESCRIPTION" => "'<b>" . $this->_target . "</b>' does not exist.");
        return new AdminPageBrowse();
      }
    }

    return true;
  }
  function getPageTitle() { return "Delete '" . $this->_target . "'"; }
  function outputPage()
  {
    echo "<form method=\"post\">\n";
    echo "<b>File:</b> /" . $this->_target . "<br />\n";
    echo "<b>Delete?</b><br />\n";
    echo "<input type=\"submit\" name=\"delete-file\" value=\"Delete\" />\n";
    echo "<input type=\"submit\" name=\"cancel-delete\" value=\"Cancel\" />\n";
    echo "</form>\n";
  }
}
?>