<?php
class AdminPageDatabaseSql
{
  private $_executedSQLQuery = null;
  function checkOrRedirect($path_pieces, $database)
  {
      if (isset($_POST["sql-query"]))
      {
        $query = $_POST["sql-query"];
        $query_is_update = true;
        if (strpos($query, "SELECT") === 0)
          $query_is_update = false;
        if ($query_is_update)
        {
          $execute_success = $database->exec($query);
          $_SESSION[ADMIN_PAGE_FEEDBACK] = array("FROM_PAGE" => "database-sql", "STATUS" => ($execute_success ? FEEDBACK_STATUS_SUCCESS : FEEDBACK_STATUS_ERROR),
            "FEEDBACK_TITLE" => ($execute_success ? "Successful execution" : "Error in Execution"), "FEEDBACK_DESCRIPTION" => ($execute_success ? "Query executed successfully" :
            "There was an error executing the command in the database"));
        }
        $this->_executedSQLQuery = $query;
      }
      return true;
  }
  function getPageTitle() { return "Execute SQL queries"; }
  function outputPage()
  {
    echo "<a href=\"" . WEB_PATH . "/admin/database-schema/\">Return to schema</a><br /><br />\n";
    if ($this->_executedSQLQuery !== null)
      echo "<div class=\"database-sql\">" . htmlentities($this->_executedSQLQuery) . "</div>\n";
    echo "<form method=\"POST\">\n";
    echo "<textarea name=\"sql-query\" style=\"width:100%;\" rows=\"20\"></textarea><br />\n";
    echo "<input type=\"submit\" value=\"Execute\" name=\"submit-sql-query\" />\n";
    echo "</form>\n";
  }
}
?>