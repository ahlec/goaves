<?php
class AdminPageDatabaseInsert
{
  private $_table_name;
  private $_columns = array();
  function checkOrRedirect($path_pieces, $database)
  {
    if (!isset($path_pieces[1]) || $database->querySingle("SELECT count(*) FROM sqlite_master WHERE type='table' AND name LIKE '" .
        $database->escapeString($path_pieces[1]) . "'") == 0)
    {
        $_SESSION[ADMIN_PAGE_FEEDBACK] = array("FROM_PAGE" => "database-table", "STATUS" => FEEDBACK_STATUS_ERROR,
            "FEEDBACK_TITLE" => "Table not found", "FEEDBACK_DESCRIPTION" => "The table '" . $path_pieces[1] .
            "' could not be found in the database.");
        return new AdminPageDatabaseSchema();
    }

    $table_information = $database->querySingle("SELECT name, sql FROM sqlite_master WHERE type='table' AND name LIKE '" . 
        $database->escapeString($path_pieces[1]) . "' LIMIT 1", true);
    $this->_table_name = $table_information["name"];
    $column_creation_sql = preg_split("/([\(\)])+/", $table_information["sql"]);
    $sql_exploded = explode(",", $column_creation_sql[1]);
    foreach ($sql_exploded as $column_piece)
    {
      $uses_brackets = (strpos($column_piece, "[") !== false);
      if ($uses_brackets)
      {
        preg_match("/\[([^\]])*\]/",$column_piece, $column_name);
        preg_match("/\] ([^\ ])*/",$column_piece, $column_type);
        $column_type[0] = str_replace("]","", $column_type[0]);
        $table_modifiers = preg_replace("/]([^\]])*\]/","", $column_piece);
      } else
      {
        preg_match("/'([^'])*'/",$column_piece, $column_name);
        preg_match("/' ([^\ ])*/",$column_piece, $column_type);
        $table_modifiers = preg_replace("/'([^'])*'/","", $column_piece);

      }
      $this->_columns[] = array("NAME" => str_replace("'", "", $column_name[0]), "TYPE" => str_replace(array(" ","'"), "", $column_type[0]),
          "PRIMARY" => (strpos($table_modifiers, "PRIMARY KEY") !== false), "UNIQUE" => (strpos($table_modifiers, "UNIQUE") !== false));
    }
    if (isset($_POST["submit-insert-record"]))
    {
      $insert_data = array();
      foreach ($this->_columns as $column)
        $insert_data[$column["NAME"]] = str_replace("insert-record-","",$_POST["insert-record-" . $column["NAME"]]);
      $insert_query = "INSERT INTO " . $this->_table_name . "(";
      foreach ($insert_data as $column_name => $data)
        if ($data !== "")
          $insert_query .= $database->escapeString($column_name) . ",";
      $insert_query = rtrim($insert_query, ",");
      $insert_query .= ") VALUES(";
      foreach ($insert_data as $column_name => $data)
        if ($data !== "")
          $insert_query .= "'" . $database->escapeString($data) . "',";
      $insert_query = rtrim($insert_query, ",") . ")";
      if ($database->exec($insert_query))
      {
        $_SESSION[ADMIN_PAGE_FEEDBACK] = array("FROM_PAGE" => "database-insert", "STATUS" => FEEDBACK_STATUS_SUCCESS,
            "FEEDBACK_TITLE" => "Record Inserted", "FEEDBACK_DESCRIPTION" => "The new record was inserted into '" . $this->_table_name .
            "'.");
        return new AdminPageDatabaseTable();
      } else
        echo "'" . $insert_query . "'<br /><br />";
    }
    return true;
  }
  function getPageTitle() { return "Insert Record in Table '" . $this->_table_name . "'"; }
  function outputPage()
  {
    echo "<a href=\"" . WEB_PATH . "/admin/database-schema/\">Return to schema</a><br /><br />\n";
    echo "<big><b>Database Table '" . $this->_table_name . "'</b></big><br />";
    echo "<form method=\"POST\">\n";
    echo "  <table>\n";
    foreach ($this->_columns as $column)
    {
      echo "    <tr>\n";
      echo "      <td>" . $column["NAME"] . "</td>\n";
      echo "      <td>" . $column["TYPE"] . "</td>\n";
      echo "      <td><input type=\"text\" name=\"insert-record-" . $column["NAME"] . "\" /></td>\n";
      echo "    </tr>\n";
    }
    echo "  </table>\n";
    echo "<input type=\"submit\" value=\"Insert\" name=\"submit-insert-record\" />\n";
    echo "</form>\n";
  }
}
?>