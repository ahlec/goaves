<?php
class AdminPageDatabaseSchema
{
  private $_number_of_tables = 0;
  private $_tables = array();
  function checkOrRedirect($path_pieces, $database)
  {
    $database_tables = $database->query("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name ASC");
    while ($table = $database_tables->fetchArray())
    {
      $this->_number_of_tables += 1;
      $this->_tables[] = $table["name"];
    }
    return true;
  }
  function getPageTitle() { return "Displaying Database Schema"; }
  function outputPage()
  {
    echo "<big><b>Database Schema</b></big><br />";
    echo "<b>" . $this->_number_of_tables . " table" . ($this->_number_of_tables == 1 ? "" : "s") . "</b><br />\n";
    foreach ($this->_tables as $table)
    {
      echo "<div class=\"list_item\"><a href=\"" . WEB_PATH . "/admin/database-table/" . $table . "/\">";
      echo $table;
      echo "</a></div>\n";
    }
  }
}
?>