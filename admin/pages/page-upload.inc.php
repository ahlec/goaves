<?php
class AdminPageUploadFile
{
  private $_current_directory;
  private $_file_upload_error = null;
  function checkOrRedirect($path_pieces, $database)
  {
    for ($piece = 1; $piece < sizeof($path_pieces); $piece++)
      $this->_current_directory .= $path_pieces[$piece] . "/";
    $this->_current_directory = rtrim($this->_current_directory, "/");
    if (!file_exists("F:/goaves/" . $this->_current_directory) || !is_dir("F:/goaves/" . $this->_current_directory))
      return new AdminPageBrowse();
    
    if (isset($_POST["upload-file"]))
    {
      if (move_uploaded_file($_FILES["file"]["tmp_name"], "F:/goaves/" . $this->_current_directory . "/" . $_FILES["file"]["name"]))
      {
        $_SESSION[ADMIN_PAGE_FEEDBACK] = array("FROM_PAGE" => "upload", "STATUS" => FEEDBACK_STATUS_SUCCESS, "FEEDBACK_TITLE" => "File uploaded",
            "FEEDBACK_DESCRIPTION" => "'" . $_FILES["file"]["name"] . "' has been uploaded.");
        header("Location: " . WEB_PATH . "/admin/browse/" . $this->_current_directory);
      } else
        $this->_file_create_error = "<div class=\"error\"><b>Upload failed.</b> Unable to upload file.</div>\n";
    }

    return true;
  }
  function getPageTitle() { return "Create Page"; }
  function outputPage()
  {
    if ($this->_file_upload_error !== null)
      echo $this->_file_upload_error;
    echo "<form method=\"post\" enctype=\"multipart/form-data\">\n";
    echo "<b>Current Directory:</b> /" . $this->_current_directory . "<br />\n";
    echo "<b>File:</b> <input type=\"file\" name=\"file\" /><br />\n";
    echo "<input type=\"submit\" name=\"upload-file\" value=\"Upload\" />\n";
    echo "</form>\n";
  }
}
?>