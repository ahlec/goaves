<?php
class AdminPageFilesystemUnpack
{
  private $_full_path;
  private $_url_path;
  private $_file_name;
  private $_file_directory_path;
  function checkOrRedirect($path_pieces, $database)
  {
    $this->_full_path = "F:/goaves/";
    for ($piece = 1; $piece < sizeof($path_pieces); $piece++)
    {
      $this->_url_path .= $path_pieces[$piece] . "/";
      $this->_full_path .= $path_pieces[$piece] . "/";
    }
    $this->_full_path = rtrim($this->_full_path, "/");
    if (!file_exists($this->_full_path))
    {
      $_SESSION[ADMIN_PAGE_FEEDBACK] = array("FROM_PAGE" => "filesystem-unpack", "STATUS" => FEEDBACK_STATUS_ERROR,
          "FEEDBACK_TITLE" => "File not found", "FEEDBACK_DESCRIPTION" => "'/" . $this->_url_path . "' does not exist.");
      return new AdminPageBrowse();
    }
    $file_info = pathinfo($this->_full_path);
    if (!isset($file_info["extension"]) || ($file_info["extension"] !== "zip"))
    {
      $_SESSION[ADMIN_PAGE_FEEDBACK] = array("FROM_PAGE" => "filesystem-unpack", "STATUS" => FEEDBACK_STATUS_ERROR,
          "FEEDBACK_TITLE" => "Cannot unpack file", "FEEDBACK_DESCRIPTION" => "'/" . $this->_url_path . "' is not a zipped directory.");
      return new AdminPageBrowse();
    }

    $this->_file_directory_path = str_replace("F:/goaves/", "", dirname($this->_full_path));
    $this->_file_name = rtrim(str_replace("F:/goaves/" . $this->_file_directory_path . "/", "", $this->_full_path), "/");

    return true;
  }
  function getPageTitle() { return "Edit File '" . $this->_file_name . "'"; }
  function outputPage()
  {
    echo "<b>Directory:</b> <a href=\"" . WEB_PATH . "/admin/browse/" . $this->_file_directory_path . "/\">/" . 
      $this->_file_directory_path . "</a><br />\n";
    echo "<b>File:</b> " . $this->_file_name . "<br />";
    echo "<form method=\"post\">\n";
    echo "Unpack into directory: <input type=\"text\" name=\"destination-directory\" value=\"" . $this->_file_directory_path . "\" /><br />\n";
    echo "Delete packed directory when finished? <input type=\"checkbox\" name=\"delete-original-yes\" /><br />\n";
    echo "<input type=\"submit\" value=\"Unpack\" name=\"confirmed-unpack\" />\n";
    echo "<input type=\"button\" value=\"Cancel\" onClick=\"window.location='" . WEB_PATH . "/admin/browse/" . $this->_file_directory_path . "/';\" />\n";
    echo "</form>\n";
  }
}
?>