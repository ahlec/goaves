function checkTitleValid(textbox_id, validation_error_box_id, type, desired_title, minimum_length, open_material_identity)
{
  document.getElementById(textbox_id).className = "largeTextbox validating";
  var validate_call = web_path + "/manage/processes/check-title-valid.php?";
  validate_call += "type=" + type;
  validate_call += "&desired_title=" + encodeURIComponent(desired_title);
  validate_call += "&min_length=" + minimum_length;
  if (open_material_identity != undefined)
    validate_call += "&open_material_identity=" + open_material_identity;
  executeAJAX(validate_call, function validate(value) {
    if (value == "valid")
    {
      document.getElementById(textbox_id).className = "largeTextbox valid";
      document.getElementById(validation_error_box_id).style.display = "none";
    }else {
      document.getElementById(textbox_id).className = "largeTextbox invalid";
      document.getElementById(validation_error_box_id).style.display = "block";
      document.getElementById(validation_error_box_id).innerHTML = value;
    }
  });
}

function generateHandle(generated_handle_container_id, desired_title)
{
  var generate_call = web_path + "/manage/processes/generate-handle.php?desired_title=" + encodeURIComponent(desired_title);
  executeAJAX(generate_call, function generated(value) { document.getElementById(generated_handle_container_id).innerHTML = value; });
}


function generateHandleFromText(text)
{
  return text.replace(/[^a-zA-Z0-9 ]/g,"").replace(/[ ]/g,"-").toLowerCase();
}
function executeAJAX(ajaxcall, evaluate_function)
{
  var AJAX;
  try
  {
    AJAX = new XMLHttpRequest();
  }
  catch(e)
  {
    try
    {
      AJAX = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch(e)
    {
      try
      {
        AJAX = new ActiveXObject("Microsoft.XMLHTTP");
      }
      catch(e)
      {
        alert("Your browser does not support AJAX.");
        return false;
      }
    }
  }
  AJAX.onreadystatechange = function()
  {
    if (AJAX.readyState == 4)
      if (AJAX.status == 200)
        evaluate_function(AJAX.responseText);
	  else
	    theOutputElement.innerHTML = "Error: " + AJAX.statusText + ". " + AJAX.status;
  }
  AJAX.open("get", ajaxcall, true);
  AJAX.send(null);
}
function getElementsByClassName(classname, node) // shamefully stolen until I find the time to rewrite it and make it mine :3
{
    if(!node) node = document.getElementsByTagName("body")[0];
    var a = [];
    var re = new RegExp('\\b' + classname + '\\b');
    var els = node.getElementsByTagName("*");
    for(var i=0,j=els.length; i<j; i++)
        if(re.test(els[i].className))a.push(els[i]);
    return a;
}
function toggleBoxRadioButton(radio_id)
{
  var radio_element = document.getElementById(radio_id);
  var label_element = document.getElementById("label-" + radio_id);
  var previous_elements = getElementsByClassName("boxRadioChecked");
  for (var prev = 0; prev < previous_elements.length; prev++)
    previous_elements[prev].className = "boxRadioButtons";
  label_element.className = "boxRadioButtons boxRadioChecked";
  radio_element.checked = true;
}

/* Post Article */
function checkArticleTitle()
{
  document.getElementById("article-title").className = "largeTextbox validating";
  var articleTitle = document.getElementById("article-title").value;
  var updateQuery = web_path + "/manage/processes/article-name-valid.php?name=" + articleTitle;
  if (document.getElementById("article-identity"))
    updateQuery += "&article=" + document.getElementById("article-identity").value;
  executeAJAX(updateQuery, function interpret(value) { document.getElementById("article-title").className = (value === "1" ? 
	"largeTextbox valid" : "largeTextbox invalid"); });
}
function generateArticleHandle()
{
  var articleTitle = document.getElementById("article-title").value;
  var updateQuery = web_path + "/manage/processes/generate-article-handle.php?name=" + articleTitle;
  if (document.getElementById("article-identity"))
    updateQuery += "&article=" + document.getElementById("article-identity").value;
  executeAJAX(updateQuery, function interpret(value) { document.getElementById("generated-article-handle").innerHTML = value; });
}

/* Migrate Article */
function validateArticleMigration()
{
  // Check Title
  if (confirm("Have you checked over ALL fields to ensure that the article you are migrating has been correctly read?"))
    if (confirm("Are you ABSOLUTELY sure that everything on this page is correct? If you haven't taken a second look over ALL of the information, do so now."))
      return true;
  return false;
}

/* Post Comic */
function checkComicTitle()
{
  document.getElementById("comic-title").className = "largeTextbox validating";
  var comicTitle = document.getElementById("comic-title").value;
  var updateQuery = web_path + "/manage/processes/comic-name-valid.php?name=" + comicTitle;
  executeAJAX(updateQuery, function interpret(value) { document.getElementById("comic-title").className = (value === "1" ? 
	"largeTextbox valid" : "largeTextbox invalid"); });
}
function generateComicHandle()
{
  var comicTitle = document.getElementById("comic-title").value;
  var updateQuery = web_path + "/manage/processes/generate-comic-handle.php?name=" + comicTitle;
  executeAJAX(updateQuery, function interpret(value) { document.getElementById("comic-handle").innerHTML = value; });
}

/* Staff Picture */
function confirmStaffPictureChangeOne()
{
  staffPictureCropper.cropSave();
  window.location = web_path + "/manage/change-staff-picture.php?step=2";
}
function confirmStaffPictureChangeTwo()
{
  document.title = staffPictureCropper.cropSave();
  window.location = web_path + "/manage/change-staff-picture.php?changed=true";
}
function cancelStaffPictureChange()
{
  var cancelQuery = web_path + "/manage/processes/cancel-staff-picture-change.php?tmp_name=" + document.getElementById("staff-image").src;
  executeAJAX(cancelQuery, function finish(value) { window.location = web_path + "/manage/change-staff-picture.php?cancel=" + value; });
}
function confirmGroupPictureChange()
{
  document.title = groupPictureCropper.cropSave();
  window.location = web_path + "/manage/change-group-picture.php?changed=true";
}
function cancelGroupPictureChange()
{
  var cancelQuery = web_path + "/manage/processes/cancel-group-picture-change.php?tmp_name=" + document.getElementById("group-image").src;
  executeAJAX(cancelQuery, function finish(value) { window.location = web_path + "/manage/change-group-picture.php?cancel=" + value; });
}

/* Photo Galleries */
function enableDoGallery()
{
  document.getElementById("gallery-title").disabled = false;
  document.getElementById("gallery-related-group").disabled = false;
  document.getElementById("submit-gallery").disabled = false;
}

/* XML/Excel parsing */
var xml_custom_fields = 0;
function addMoreCustomDataField()
{
  var container = document.getElementById("xml-parse-custom-data-field-container");
  var field_binding = document.createElement("input");
  field_binding.setAttribute("name", "xml-parse-custom-" + xml_custom_fields);
  field_binding.setAttribute("type", "input");
  container.appendChild(field_binding);
  var field_data = document.createElement("input");
  field_data.setAttribute("name", "xml-parse-custom-data-" + xml_custom_fields);
  field_data.setAttribute("type", "input");
  container.appendChild(field_data);
  container.appendChild(document.createElement("br"));
  xml_custom_fields++;
}
function loadXMLParsingBinding(database_table)
{
  var binding_container = document.getElementById('xml-parse-bindings');
  if (database_table != "")
  {
    binding_container.style.display = "block";
    document.getElementById('xml-parse-form').setAttribute("onSubmit", "return true;");
	executeAJAX(web_path + "/manage/processes/xml-parse-select-database.php?database=" + database_table, function display(value) { binding_container.innerHTML = value; });
  } else
  {
    binding_container.style.display = "none";
	document.getElementById('xml-parse-form').setAttribute("onSubmit", "return false;");
  }
}