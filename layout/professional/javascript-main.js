/* AJAX */
function executeAJAX(ajaxcall, evaluate_function)
{
  var AJAX;
  try
  {
    AJAX = new XMLHttpRequest();
  }
  catch(e)
  {
    try
    {
      AJAX = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch(e)
    {
      try
      {
        AJAX = new ActiveXObject("Microsoft.XMLHTTP");
      }
      catch(e)
      {
        alert("Your browser does not support AJAX.");
        return false;
      }
    }
  }
  AJAX.onreadystatechange = function()
  {
    if (AJAX.readyState == 4)
      if (AJAX.status == 200)
        evaluate_function(AJAX.responseText);
      else if (AJAX.status != 0)
        alert("Error: " + AJAX.statusText + ". " + AJAX.status);
  }
  AJAX.open("get", ajaxcall, true);
  AJAX.send(null);
}

/* Navigation */
function navigationTab(element)
{
  var tabs = document.getElementById("mainSiteNavigation").getElementsByTagName("a");
  for (var tabIndex = 0; tabIndex < tabs.length; tabIndex++)
    if (tabs[tabIndex].className && tabs[tabIndex].className.indexOf("Hover") > -1)
	  tabs[tabIndex].className = tabs[tabIndex].className.replace("Hover", "");
  document.getElementById("tab-" + element).className = (document.getElementById("tab-" + element).className.toLowerCase().indexOf("loaded") > -1 ? "itemLoadedHover" : "itemHover");
  
  var localContainers = document.getElementById("localSiteNavigation").getElementsByTagName("div");
  for (var index = 0; index < localContainers.length; index++)
    if (localContainers[index].className)
	  localContainers[index].className = "localContainer";
  document.getElementById("local-navigation-" + element).className = "localContainer loadedLocal";
}
function clearNavigation(e)
{
  if (!e) var e = window.event;
  var srcTarg = (window.event) ? e.srcElement : e.target;
  var relTarg = e.relatedTarget || e.toElement;
  if (relTarg == document.getElementById("localSiteNavigation") && (srcTarg.className.indexOf("item") > -1))
    return;
  if (relTarg.className.indexOf("local") > -1)
    return;
	
  var tabs = document.getElementById("mainSiteNavigation").getElementsByTagName("a");
  var loadedElement;
  for (var tabIndex = 0; tabIndex < tabs.length; tabIndex++)
  {
    if (tabs[tabIndex].className)
	  tabs[tabIndex].className = (tabs[tabIndex].className.indexOf("Loaded") > -1 ? "itemLoadedHover" : "item");
	if (tabs[tabIndex].id && tabs[tabIndex].className.indexOf("Loaded") > -1)
	  loadedElement = tabs[tabIndex].id.replace("tab-", "");
  }
	  
  var localContainers = document.getElementById("localSiteNavigation").getElementsByTagName("div");
  for (var index = 0; index < localContainers.length; index++)
    if (localContainers[index].className)
	  localContainers[index].className = "localContainer";
  document.getElementById("local-navigation-" + loadedElement).className = "localContainer loadedLocal";
}
function toggleSearch(e)
{
  if (!e) var e = window.event;
  var srcTarg = (window.event) ? e.srcElement : e.target;
  if (srcTarg.id == "pageSearch-Scope" || srcTarg.id == "pageSearch-Search")
    return;
  if (document.getElementById("pageSearch").className.indexOf("open") > -1)
    document.getElementById("pageSearch").className = document.getElementById("pageSearch").className.replace(" open", "");
  else
    document.getElementById("pageSearch").className += " open";
}

/* Index - Feature */


var fadeSpeed = 60;
function featureFade(previous_amount)
{
  if (previous_amount == undefined)
    var previous_amount = 1;
  if (document.getElementById("feature-image-back").style.opacity != undefined)
    var backOpacity = document.getElementById("feature-image-back").style.opacity * 10;
  else
    var backOpacity = document.getElementById("feature-image-back").style.filter.replace("alpha(opacity='", "").replace("')", "");
  if (backOpacity == undefined || backOpacity == "")
    backOpacity = 100;
  backOpacity -= previous_amount * 2;
  if (backOpacity < 0)
    backOpacity = 0;
	
  if (document.getElementById("feature-image-front").style.opacity != undefined)
    var frontOpacity = document.getElementById("feature-image-front").style.opacity * 10;
  else
    var frontOpacity = document.getElementById("feature-image-front").style.filter.replace("alpha(opacity='", "").replace("')", "");
  if (frontOpacity == undefined || frontOpacity == "")
    frontOpacity = 0;
  frontOpacity += previous_amount * 2;
  if (frontOpacity > 100)
    frontOpacity = 100;  
    
  /*if (backOpacity > 0)
  {
    if (document.getElementById("feature-image-back").style.opacity != undefined)
	  document.getElementById("feature-image-back").style.opacity = backOpacity / 10;
	else
	  document.getElementById("feature-image-back").style.filter = "alpha(opacity='" + backOpacity + "')";
  } else
  {
    document.getElementById("feature-image-back").style.display = "none";
	if (document.getElementById("feature-image-back").style.opacity != undefined)
	  document.getElementById("feature-image-back").style.opacity = 1.0;
	else
	  document.getElementById("feature-image-back").style.filter = "alpha(opacity='100')";
  }*/
  
  if (frontOpacity < 100)
  {
    if (document.getElementById("feature-image-front").style.opacity != undefined)
	  document.getElementById("feature-image-front").style.opacity = frontOpacity / 10;
	else
	  document.getElementById("feature-image-front").style.filter = "alpha(opacity='" + frontOpacity + "')";
  } else
  {
	if (document.getElementById("feature-image-front").style.opacity != undefined)
	  document.getElementById("feature-image-front").style.opacity = 1.0;
	else
	  document.getElementById("feature-image-front").style.filter = "alpha(opacity='100')";
  }
  
  if (backOpacity > 0 || frontOpacity < 100)
    setTimeout("featureFade(" + (previous_amount * 2) + ")", fadeSpeed);
}


function selectFeature(featureIndex)
{
  resetFeatures();
  document.getElementById("feature-" + featureIndex).className += " selected";
  document.getElementById("feature-type").innerHTML = features[featureIndex][1];
  if (features[featureIndex][2] != null)
  {
    document.getElementById("feature-title").innerHTML = features[featureIndex][2];
	document.getElementById("feature-title").style.display = "block";
  }
  else
    document.getElementById("feature-title").style.display = "none";
  if (features[featureIndex][4] != null)
  {
    document.getElementById("feature-content").innerHTML = features[featureIndex][4];
	document.getElementById("feature-content").style.display = "block";
  } else
    document.getElementById("feature-content").style.display = "none";
  
  document.getElementById("feature-action-link").href = features[featureIndex][5];
  document.getElementById("feature-action-link").innerHTML = features[featureIndex][6];
  
  document.getElementById("feature-image-back").style.backgroundImage = document.getElementById("feature-image-front").style.backgroundImage;
  document.getElementById("feature-image-back").style.display = "block";
  if (document.getElementById("feature-image-front").style.opacity != undefined)
    document.getElementById("feature-image-front").style.opacity = 0.0;
  else
    document.getElementById("feature-image-front").style.filter = "alpha(opacity='0')";
  document.getElementById("feature-image-front").style.backgroundImage = "url(" + web_path + "/images/features/" + features[featureIndex][3] + ")";
  featureFade();
  currentFeatureStory = featureIndex;
}
function resetFeatures()
{
  var feature_items = document.getElementById("feature-navigation").getElementsByTagName("div");
  for (var item = 0; item < feature_items.length; item++)
    if (feature_items[item].className != undefined && feature_items[item].className.indexOf(" selected") >= 0)
      feature_items[item].className = feature_items[item].className.replace(" selected", "");
}
var currentFeatureStory = 0;
var featuresInterval = null;
function startFeaturesRotation()
{
  if (featuresInterval == null)
    featuresInterval = setInterval("rotateFeature()", 10000);
}
function stopFeaturesRotation()
{
  clearInterval(featuresInterval);
  featuresInterval = null;
}
function rotateFeature()
{
  stopFeaturesRotation();
  currentFeatureStory++;
  if (document.getElementById("feature-" + currentFeatureStory) == undefined)
    currentFeatureStory = 0;
  selectFeature(currentFeatureStory);
  startFeaturesRotation();
}


/* Index - Features */
var features_current_story = 1;
var features_interval_id = null;
function startFeaturesInterval()
{
  if (features_interval_id == null)
    features_interval_id = setInterval("toggleFeaturesInterval()", 12500);
}
function stopFeaturesInterval()
{
  clearInterval(features_interval_id);
  features_interval_id = null;
}
function toggleFeaturesInterval()
{
  features_current_story++;
  if (document.getElementById("feature-" + features_current_story) == undefined)
    features_current_story = 1;
  stopFeaturesInterval();
  toggleFeature(features_current_story);
  startFeaturesInterval();
}
function toggleFeature(feature_identity)
{
  clearFeatures();
  features_current_story = feature_identity;
  document.getElementById("feature-type").innerHTML = features_definitions[feature_identity - 1][1];
  document.getElementById("feature-title").innerHTML = features_definitions[feature_identity - 1][2];
  if (features_definitions[feature_identity - 1][3] == "none")
  {
    document.getElementById("feature-info-container").style.display = "none";
  } else
  {
    document.getElementById("feature-contents").innerHTML = features_definitions[feature_identity - 1][3];
	document.getElementById("feature-info-container").style.display = "block";
  }
  document.getElementById("feature-container").style.backgroundImage = "url('" + features_definitions[feature_identity - 1][4] + "')";
  document.getElementById("feature-" + feature_identity).className = "item active";
}
function clearFeatures()
{
  var stories = 1;
  while (document.getElementById("feature-" + stories) != undefined)
  {
    document.getElementById("feature-" + stories).className = "item";
    stories++;
  }
}

/* Index - Latest Stories */
function clearLatestStoryTabs()
{
  if (document.getElementById("latest-story-1") != undefined)
    document.getElementById("latest-story-1").className = "newsItem";
  if (document.getElementById("latest-story-2") != undefined)
    document.getElementById("latest-story-2").className = "newsItem";
  if (document.getElementById("latest-story-3") != undefined)
    document.getElementById("latest-story-3").className = "newsItem";
  if (document.getElementById("latest-story-4") != undefined)
    document.getElementById("latest-story-4").className = "newsItem";
  if (document.getElementById("latest-story-5") != undefined)
    document.getElementById("latest-story-5").className = "newsItem";
}
function hoverLatestStoryTab(element, image, url)
{
  clearLatestStoryTabs();
  element.className = 'newsItemHover';
  document.getElementById('latest-image-container').setAttribute("onClick", "window.location='" + url + "';");
  document.getElementById('latest-story-image').src = image;
}

function outLatestStoryTab(element, first_story_image, first_story_url)
{
  if (!e) var e = window.event;
  var relTarg = e.relatedTarget || e.toElement;
  var isLeavingLatestStories = !(relTarg.className == "newsItem" || relTarg.className == "newsItemHover" ||
	relTarg.className == "latestStories" || relTarg.className == "newsImage" || relTarg.className == "imageContainer");
  
  if (relTarg.className == "newsItem" || relTarg.className == "newsItemHover")
    element.className = 'newsItem';
  if (isLeavingLatestStories)
  {
    clearLatestStoryTabs();
	document.getElementById('latest-image-container').setAttribute("onClick", "window.location='" + first_story_url + "';");
    document.getElementById("latest-story-1").className = 'newsItemHover';
	document.getElementById("latest-story-image").src = first_story_image;
  }
}

/* Index - Poll Voting */
function processPollVoting()
{
  var poll_identity = document.getElementById("latest-poll-identity").value;
  var response_identity = -1;
  for (var response_number = 0; response_number < document.latest_poll.response.length; response_number++)
    if (document.latest_poll.response[response_number].checked)
      response_identity = document.latest_poll.response[response_number].id;
  if (response_identity == -1)
  {
    alert("You must select a response to participate in the poll.");
    return;
  }
  document.getElementById("poll-vote-button").disabled = true;
  
  executeAJAX(web_path + "/components/poll-vote.php?poll=" + poll_identity + "&response=" +
	response_identity, function process(value)
	{
		if (value == "success")
		{
		    executeAJAX(web_path + "/components/current-poll-results.php?poll=" + poll_identity, function display(value)
				{
				  document.getElementById("pollFormContainer").className = "responses";
				  document.getElementById("pollFormContainer").innerHTML = value;
				});
			//alert("Your vote has been processed. Thank you!");
			//window.location = web_path + "/poll/" + document.getElementById("latest-poll-handle").value + "/";
			return;
		}
		alert(value);
		document.getElementById("poll-vote-button").disabled = false;
	});
}

/* Sound Slides */
function changeSoundSlide(to_slide)
{
	document.getElementById("slide-image").className = "";
	if ((slides[to_slide][1]).length > 0)
	{
		document.getElementById("slide-caption").innerHTML = slides[to_slide][1];
		document.getElementById("slide-caption").style.display = "block";
	} else
		document.getElementById("slide-caption").style.display = "none";
	document.getElementById("slide-image").src = slides[to_slide][0];
}
function changeSoundSlidesByCues(clip, point)
{
	changeSoundSlide(point);
}
function seekSoundSlides(clip)
{
	var current_start_time = null;
	var current_position = Math.floor(this.getTime() * 1000);
	for (var start_time in slides)
	{
		if (start_time <= current_position)
			current_start_time = start_time;
	}
	if (current_start_time == null)
		changeSoundSlide("0000");
	else
		changeSoundSlide(current_start_time);
}

/* Staff Portfolio */
function staffPortfolioWorksChange(to_section)
{
  var sections = document.getElementById("staff-works").childNodes;
  for (section = 0; section < sections.length; section++)
    if (sections[section].style != undefined)
      sections[section].style.display = "none";
  document.getElementById('staff-works-' + to_section).style.display = "block";
  var nav_sections = document.getElementById("staff-works-navigation").childNodes;
  for (nav_section = 0; nav_section < nav_sections.length; nav_section++)
    if (nav_sections[nav_section].className != undefined && nav_sections[nav_section].className != "disabled")
      nav_sections[nav_section].className = "enabled";
  document.getElementById('staff-works-nav-' + to_section).className = "enabled selected";
  window.location.hash = to_section;
}
function staffPortfolioLoad(first_name, last_name)
{
  if (window.location.hash == "")
    return;
  window.location = web_path + "/staff-portfolio/" + first_name + "-" + last_name + "/" + window.location.hash.replace("#", "") + "/";
}

/* Staff Listing */
function searchStaffListing(for_value)
{
  if (!document.getElementById('staff-listing-viewport'))
    return;
  var execute_call_base = web_path + "/components/page_components/staff-listing-display.php?called=true";
  if (staff_listing_filter != null)
    execute_call_base += "&filter=" + staff_listing_filter;
  if (for_value.length == 0)
    executeAJAX(execute_call_base, function load(value) { loadStaffListing(value); });
  else
    executeAJAX(execute_call_base + "&search=true&query=" + encodeURIComponent(for_value),
	function load(value) { loadStaffListing(value); });
}
function processReturnKeySearchStaff(e)
{
  var key_code = (window.event) ? event.keyCode : e.keyCode;
  if (key_code == 13)
  {
    var execute_base_filters = "";
    if (staff_listing_filter != null)
      execute_base_filters += "&filter=" + staff_listing_filter;
    if (document.getElementById('staff-listing-search').className != "search empty")
      execute_base_filters += "&search=true&query=" + encodeURIComponent(document.getElementById('staff-listing-search').value);
    executeAJAX(web_path + "/components/page_components/query-staff-listing-search.php?get=num_results" + execute_base_filters,
    	function evaluate(value) {
	if (value == "1")
	  executeAJAX(web_path + "/components/page_components/query-staff-listing-search.php?get=first_result" + execute_base_filters,
	  	function processSelect(value)
	  	{
	  	  window.location = web_path + "/staff-portfolio/" + value + "/";
	  	});
    	});
  }
}
function focusStaffListingSearch()
{
  if (!document.getElementById('staff-listing-search'))
    return;
  var search_box = document.getElementById('staff-listing-search');
  if (search_box.className == "search empty")
  {
    search_box.className = "search";
    search_box.value = "";
  }
}
function blurStaffListingSearch()
{
  if (!document.getElementById('staff-listing-search'))
    return;
  var search_box = document.getElementById('staff-listing-search');
  if (search_box.value == "")
  {
    search_box.className = "search empty";
    search_box.value = "Search for staff...";
    var execute_call = web_path + "/components/page_components/staff-listing-display.php?called=true";
    if (staff_listing_filter != null)
      execute_call += "&filter=" + staff_listing_filter;
    executeAJAX(execute_call, function load(value) { loadStaffListing(value); });
  }
}
function loadStaffListing(results)
{
  document.getElementById('staff-listing-viewport').innerHTML = results;
}
function blurAllOtherIconsStaffListing(focus_id)
{
  if (!document.getElementById('staff-listing-viewport'))
    return;
  var staff_icons = document.getElementById('staff-listing-viewport').getElementsByTagName("img");
  for (var icon = 0; icon < staff_icons.length; icon++)
    if (staff_icons[icon].className != undefined && staff_icons[icon].id != ("staff-icon-" + focus_id))
      staff_icons[icon].className = "staffIcon iconNoFocus";
}
function unblurAllStaffListingIcons()
{
  if (!document.getElementById('staff-listing-viewport'))
    return;
  var staff_icons = document.getElementById('staff-listing-viewport').getElementsByTagName("img");
  for (var icon = 0; icon < staff_icons.length; icon++)
    if (staff_icons[icon].className != undefined)
      staff_icons[icon].className = "staffIcon";
}
function createStaffListingHoverBox(staff_first_name, staff_last_name)
{
  return;
  if (document.getElementById('staff-listing-hover-box'))
    return;
  var hover_box = document.createElement("div");
  hover_box.setAttribute("id", "staff-listing-hover-box");
  hover_box.className = "staffListHoverBox";
  hover_box.innerHTML = "<b>" + staff_first_name + " " + staff_last_name + "</b>";
  hover_box.style.top = "10px";
  hover_box.style.left = "10px";
  document.body.appendChild(hover_box);
}
function deleteStaffListingHoverBox()
{
  return;
  if (!document.getElementById('staff-listing-hover-box'))
    return;
  document.body.removeChild(document.getElementById('staff-listing-hover-box'));
}
var staff_listing_filter = null;
function filterStaffListing(filter)
{
  if(!document.getElementById("staff-listing-filter-" + filter))
    return;
  var execute_call_base = web_path + "/components/page_components/staff-listing-display.php?called=true";
  if (document.getElementById('staff-listing-search').className != "search empty")
    execute_call_base += "&search=true&query=" + encodeURIComponent(document.getElementById('staff-listing-search').value);
  if (document.getElementById("staff-listing-filter-" + filter).className == "item selectedItem")
  {
    document.getElementById("staff-listing-filter-" + filter).className = "item";
    staff_listing_filter = null;
    executeAJAX(execute_call_base, function load(value) { loadStaffListing(value); });
    return;
  }
  var filters = document.getElementById('staff-listing-filters').getElementsByTagName("div");
  for (var filter_iterations = 0; filter_iterations < filters.length; filter_iterations++)
    if (filters[filter_iterations].className != undefined)
      filters[filter_iterations].className = "item";
  document.getElementById("staff-listing-filter-" + filter).className = "item selectedItem";
  staff_listing_filter = filter;
  executeAJAX(execute_call_base + "&filter=" + filter, function load(value) { loadStaffListing(value); });
}

/* Group Listing */
function searchGroupListing(for_value)
{
  if (!document.getElementById('group-listing-viewport'))
    return;
  var execute_call_base = web_path + "/components/page_components/group-listing-display.php?called=true";
  if (group_listing_filter != null)
    execute_call_base += "&filter=" + group_listing_filter;
  if (for_value.length == 0)
    executeAJAX(execute_call_base, function load(value) { loadGroupListing(value); });
  else
    executeAJAX(execute_call_base + "&search=true&query=" + encodeURIComponent(for_value),
	function load(value) { loadGroupListing(value); });
}
function loadGroupListing(results)
{
  document.getElementById('group-listing-viewport').innerHTML = results;
}
function focusGroupListingSearch()
{
  if (!document.getElementById('group-listing-search'))
    return;
  var search_box = document.getElementById('group-listing-search');
  if (search_box.className == "search empty")
  {
    search_box.className = "search";
    search_box.value = "";
  }
}
function blurGroupListingSearch()
{
  if (!document.getElementById('group-listing-search'))
    return;
  var search_box = document.getElementById('group-listing-search');
  if (search_box.value == "")
  {
    search_box.className = "search empty";
    search_box.value = "Search for group...";
    var execute_call = web_path + "/components/page_components/group-listing-display.php?called=true";
    if (group_listing_filter != null)
      execute_call += "&filter=" + group_listing_filter;
    executeAJAX(execute_call, function load(value) { loadGroupListing(value); });
  }
}
var group_listing_filter = null;
function filterGroupListing(filter)
{
  if(!document.getElementById("group-listing-filter-" + filter))
    return;
  var execute_call_base = web_path + "/components/page_components/group-listing-display.php?called=true";
  if (document.getElementById('group-listing-search').className != "search empty")
    execute_call_base += "&search=true&query=" + encodeURIComponent(document.getElementById('group-listing-search').value);
  if (document.getElementById("group-listing-filter-" + filter).className == "item selectedItem")
  {
    document.getElementById("group-listing-filter-" + filter).className = "item";
    group_listing_filter = null;
    executeAJAX(execute_call_base, function load(value) { loadGroupListing(value); });
    return;
  }
  var filters = document.getElementById('group-listing-filters').getElementsByTagName("div");
  for (var filter_iterations = 0; filter_iterations < filters.length; filter_iterations++)
    if (filters[filter_iterations].className != undefined)
      filters[filter_iterations].className = "item";
  document.getElementById("group-listing-filter-" + filter).className = "item selectedItem";
  group_listing_filter = filter;
  executeAJAX(execute_call_base + "&filter=" + filter, function load(value) { loadGroupListing(value); });
}
function groupListingHoverIcon(group_identity)
{
  /*var group_results = document.getElementById("group-listing-results").getElementsByTagName("a");
  for (var result = 0; result < group_results.length; result++)
  {
    if (group_results[result].childNodes != undefined && group_results[result].childNodes[0].childNodes != undefined &&
		group_results[result].childNodes[0].childNodes[2].className == "groupListIconOverlay")
	{
	  group_results[result].childNodes[0].childNodes[2].className = "groupListIconOverlay glUnfocused";
	}
  }*/
  document.getElementById('group-list-name-' + group_identity).style.display = "block";
}
function groupListingLeaveIcon(group_identity)
{
  document.getElementById("group-list-name-" + group_identity).style.display = "none";
}

/* Group Page */
function groupPageChangeTab(group_identity, to_tab)
{
  if (document.getElementById('group-page-tab-' + to_tab).className == "tab selected")
    return;
  executeAJAX(web_path + "/components/page_components/group-page-tab-pages.php?group_identity=" + group_identity + "&tab=" + to_tab,
	function update(value)
	{
	  window.location.hash = to_tab;
	  var tabs = document.getElementById('group-page-tabs').getElementsByTagName("div");
	  for (var tab_index = 0; tab_index < tabs.length; tab_index++)
	  if (tabs[tab_index].className != undefined)
		tabs[tab_index].className = "tab";
	  document.getElementById('group-page-tab-' + to_tab).className = "tab selected";
	  document.getElementById('group-tab-page').innerHTML = value;
	});
}
function groupPageLoad()
{
  if (window.location.hash == "")
    return;
  var hash_tab = window.location.hash.replace("#", "");
  var tab_url = window.location.href;
  if (tab_url.indexOf("/", tab_url.indexOf("/group/") + 7) == -1)
    tab_url += "/" + hash_tab + "/";
  else
    tab_url = tab_url.substring(0, tab_url.indexOf("/", tab_url.indexOf("/group/") + 7) + 1) + hash_tab + "/";
  window.location = tab_url;
}
function groupPageRelatedMultimediaHover(multimedia_display_identity)
{
  var media_icons = document.getElementById('related-multimedia').getElementsByTagName("a");
  for (var icon_index = 0; icon_index < media_icons.length; icon_index++)
  {
    if (media_icons[icon_index].childNodes != undefined && media_icons[icon_index].childNodes[0].childNodes != undefined &&
		media_icons[icon_index].childNodes[0].childNodes[3].className == "mediaImageOverlay")
	{
	  media_icons[icon_index].childNodes[0].childNodes[3].className = "mediaImageOverlay miPeripheral";
	}
  }
  document.getElementById('related-multimedia-overlay-' + multimedia_display_identity).className = "mediaImageOverlay miCurrent";
  document.getElementById('related-multimedia-title-' + multimedia_display_identity).style.display = 'block';
}
function groupPageRelatedMultimediaOut(multimedia_display_identity)
{
  var media_icons = document.getElementById('related-multimedia').getElementsByTagName("a");
  for (var icon_index = 0; icon_index < media_icons.length; icon_index++)
  {
    if (media_icons[icon_index].childNodes != undefined && media_icons[icon_index].childNodes[0].childNodes != undefined &&
		media_icons[icon_index].childNodes[0].childNodes[3].className == "mediaImageOverlay miPeripheral")
	{
	  media_icons[icon_index].childNodes[0].childNodes[3].className = "mediaImageOverlay";
	}
  }
  document.getElementById('related-multimedia-overlay-' + multimedia_display_identity).className = "mediaImageOverlay";
  document.getElementById('related-multimedia-title-' + multimedia_display_identity).style.display = 'none';
}

/* Leaf */
function goLeafPage(page_number)
{
  document.getElementById("leafIssue").setPageNumber(page_number);
}