function getWindowSize()
{
  var width = 0;
  var height = 0;
  if (parseInt(navigator.appVersion)>3) {
   if (navigator.appName=="Netscape") {
    width = window.innerWidth;
    height = window.innerHeight;
   }
   if (navigator.appName.indexOf("Microsoft")!=-1) {
    width = document.body.offsetWidth;
    height = document.body.offsetHeight;
   }
  }
  return [width, height];
}

var openWindow = null;
function ManageWindow(windowHandle, parameters)
{
  openWindow = this;
  var _window = document.createElement("div");
  _window.setAttribute("id", "window" + windowHandle);
  _window.className = "manageWindow";
  var _backScreen = document.createElement("div");
  _backScreen.setAttribute("id", "backScreen" + windowHandle);
  _backScreen.className = "windowBackScreen";
  document.body.appendChild(_backScreen);
  var _frame = document.createElement("div");
  var _closebutton = document.createElement("div");
  _closebutton.setAttribute("id", "windowClose" + windowHandle);
  _closebutton.className = "closeButton";
  _closebutton.onmouseover = function() { _closebutton.className = "closeButton closeButtonHover"; }
  _closebutton.onmouseout = function() { _closebutton.className = "closeButton"; }
  _closebutton.onclick = function() { openWindow.closeWindow(); }
  _window.appendChild(_closebutton);
  window.onResize = function(e)
  	{
  	  if (!e) var e = window.event;
  	  document.title = 'resized';
  	}
  this.wind = _window;
  this.frame = _frame;
  this.closeWindow = function()
  {
    document.getElementsByTagName("html")[0].style.overflow = 'auto';
	document.body.removeChild(document.getElementById('window' + windowHandle));
	document.body.removeChild(document.getElementById('backScreen' + windowHandle));
  }
  executeAJAX(web_path + "/frames/frame-" + windowHandle + ".php?design=JSON&parameters=" + encodeURIComponent(parameters),
  	function create(value)
  	{
	  if (value == "<404>")
	  {
	    alert("The requested frame does not exist.");
		_backScreen.style.display = "none";
	    return;
	  }
	  var jsonValue = eval("(" + value + ")");
	  if (jsonValue.window.hasCloseButton == false)
	  {
	    _window.removeChild(_closebutton);
		alert('no close button');
//	    _closebutton.style.display = "none";
	  }
	  if (jsonValue.window.onCloseButtonClick != undefined)
	    _closebutton.onclick = function() { eval(jsonValue.window.onCloseButtonClick); openWindow.closeWindow(); }
		
	  window.scroll(0,0);
	  document.getElementsByTagName("html")[0].style.overflow = 'hidden';
	  
	  _frame.style.padding = (jsonValue.window.padding != undefined ? jsonValue.window.padding : "5px");
	  _window.style.width = (parseInt(jsonValue.window.width.replace("px", "")) +
	  	parseInt((_frame.style.padding == undefined || _frame.style.padding == "none" ? "0px" :
	  	_frame.style.padding).replace("px", "") * 2)) + "px";
	  _frame.style.width = jsonValue.window.width;
	  _window.style.height = (parseInt(jsonValue.window.height.replace("px", "")) +
	  	parseInt((_frame.style.padding == undefined || _frame.style.padding == "none" ? "0px" :
	  	_frame.style.padding).replace("px", "") * 2)) + "px";
	  _frame.style.height = jsonValue.window.height;
	  _window.style.left = (jsonValue.window.posX == "center" ? ((getWindowSize()[0] -
	    	_window.style.width.replace("px", "")) / 2) + "px" : jsonValue.window.posX);
	  _window.style.top = (jsonValue.window.posY == "center" ? ((getWindowSize()[1] - _window.style.height.replace("px", "")) /
	  	2) + "px" : jsonValue.window.posY);
	  	
	  if (jsonValue.window.canClose == undefined || jsonValue.window.canClose == true)
	    _closebutton.style.display = "block";
  
  
	  _frame.setAttribute("id", "frame" + windowHandle);
	  _frame.className = "frame loading";
	  executeAJAX(web_path + "/frames/frame-" + windowHandle + ".php?parameters=" + encodeURIComponent(parameters),
	  	function finish(frameContents)
	  	{
	  	  _frame.className = "frame";
	  	  _frame.innerHTML = frameContents;
		  var javascriptCodeBlocks = _frame.getElementsByTagName("script");
		  for (var codeBlockIndex = 0; codeBlockIndex < javascriptCodeBlocks.length; codeBlockIndex++)
		  {
//		    alert(javascriptCodeBlocks[codeBlockIndex].text);
		    eval(javascriptCodeBlocks[codeBlockIndex].text);
		  }
		
	  	});
	  _window.appendChild(_frame);
  
	  document.body.appendChild(_window);
	});

}