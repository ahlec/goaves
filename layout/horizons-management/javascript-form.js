/* Date selection */
function updateDaysDateSelection(form_handle)
{
  document.getElementById(form_handle + "-day").disabled = true;
  document.getElementById(form_handle + "-month").disabled = true;
  document.getElementById(form_handle + "-year").disabled = true;
  executeAJAX(web_path + '/components/number-days-month.php?month=' + 
	document.getElementById(form_handle + "-month").value + '&year=' + document.getElementById(form_handle + "-year").value,
	function update(result)
	{
	  if (result.indexOf('[Error] ') < 0)
	  {
	    var day_differences = result - document.getElementById(form_handle + "-day").length;
		if (day_differences > 0)
		{
		  for (var addIteration = 0; addIteration < day_differences; addIteration++)
		    document.getElementById(form_handle + "-day").add(new Option(document.getElementById(form_handle + "-day").length + 1,
				document.getElementById(form_handle + "-day").length + 1));
		} else if (day_differences < 0)
		{
		  for (var removeIteration = 0; removeIteration < (-1 * day_differences); removeIteration++)
		    document.getElementById(form_handle + "-day").remove(document.getElementById(form_handle + "-day").length - 1);
			
	    /*for (var day = 0; day < result; day++)
		{
		  document.getElementById(form_handle + "-day").add(new Option(day + 1, day + 1));
		}*/
		}
	  } else if (result.indexOf('[Soft]') < 0)
		alert(result.replace('[Error] ',''));
      document.getElementById(form_handle + "-day").disabled = false;
	  document.getElementById(form_handle + "-month").disabled = false;
	  document.getElementById(form_handle + "-year").disabled = false;
	});
}
function validateDate(form_handle, callback)
{
  document.getElementById(form_handle + "-day").disabled = true;
  document.getElementById(form_handle + "-month").disabled = true;
  document.getElementById(form_handle + "-year").disabled = true;
  executeAJAX(web_path + '/components/validate-date.php?day=' + document.getElementById(form_handle + "-day").value + '&month=' + 
	document.getElementById(form_handle + "-month").value + '&year=' + document.getElementById(form_handle + "-year").value,
	function validate(result)
	{
	  if (result.indexOf('[Error] ') < 0)
	  {
	    document.getElementById(form_handle + "-timestamp").value = result;
	  } else
	  {
	    document.getElementById(form_handle + "-timestamp").value = null;
		if (result.indexOf('[Soft]') < 0)
			alert(result.replace('[Error] ',''));
	  }
      document.getElementById(form_handle + "-day").disabled = false;
	  document.getElementById(form_handle + "-month").disabled = false;
	  document.getElementById(form_handle + "-year").disabled = false;
	  if (typeof callback === 'function')
		callback();
	});
}