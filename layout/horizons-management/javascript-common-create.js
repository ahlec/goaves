/* Related Groups */
function toggleUseRelatedGroup()
{
  document.getElementById('related-group-type').selectedIndex = 0;
  document.getElementById('related-group-type').style.display =
  	(document.getElementById('related-group-use').value ? 'block' : 'none');
  for (var type_index = 0; type_index < group_types.length; type_index++)
    document.getElementById('related-group-' + group_types[type_index]).style.display = 'none';
}

function selectGroupByType(type_handle)
{
  for (var type_index = 0; type_index < group_types.length; type_index++)
    document.getElementById('related-group-' + group_types[type_index]).style.display = 'none';
  if (type_handle == '[none]')
    return;
  document.getElementById('related-group-' + type_handle).selectedIndex = 0;
  document.getElementById('related-group-' + type_handle).style.display = 'block';
}

/* Topic */
function toggleUseTopic()
{
  document.getElementById('topic').selectedIndex = 0;
  document.getElementById('topic').style.display = (document.getElementById('topic-use').value ? 'block' : 'none');
}