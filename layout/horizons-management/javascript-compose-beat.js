/* Universal */
function collapseAllStepContainers()
{
  var steps = Array('one', 'two', 'three', 'four', 'five', 'six');
  for (var index = 0; index < steps.length; index++)
    document.getElementById('container-step-' + steps[index]).style.display = "none";
}
/* Step One */
function returnStepOne()
{
  collapseAllStepContainers();
  document.getElementById('container-step-one').style.display = "block";
}
function goStepTwo()
{
  document.getElementById('timeline-step-one').className = "piece pieceStart piecePast";
  document.getElementById('timeline-step-two').className = "piece pieceCurrent";
  document.getElementById('container-step-one').style.display = "none";
  document.getElementById('container-step-two').style.display = "block";
//  document.getElementById('timeline-step-two').onclick = goStepTwo;
}

/* Step Two */
function selectCategory(category_identity)
{
  document.getElementById('article-type').value = category_identity;
  var categoryIcons = document.getElementById('beatTypes').getElementsByTagName('img');
  for (var icon = 0; icon < categoryIcons.length; icon++)
    categoryIcons[icon].className = 'categoryIcon categoryUnselected';
  document.getElementById('beatType-' + category_identity).className = 'categoryIcon categorySelected';
  ProcessStepTwoCompletion();
}
function toggleUseRelatedGroup()
{
  document.getElementById('related-group-type').selectedIndex = 0;
  document.getElementById('related-group-type').style.display =
  	(document.getElementById('related-group-use').checked ? 'block' : 'none');
  for (var type_index = 0; type_index < group_types.length; type_index++)
    document.getElementById('related-group-' + group_types[type_index]).style.display = 'none';
  ProcessStepTwoCompletion();
}

function selectGroupByType(type_handle)
{
  for (var type_index = 0; type_index < group_types.length; type_index++)
    document.getElementById('related-group-' + group_types[type_index]).style.display = 'none';
  if (type_handle == '[none]')
  {
    ProcessStepTwoCompletion();
    return;
  }
  document.getElementById('related-group-' + type_handle).selectedIndex = 0;
  document.getElementById('related-group-' + type_handle).style.display = 'block';
  ProcessStepTwoCompletion();
}
function toggleUseTopic()
{
  document.getElementById('topic').selectedIndex = 0;
  document.getElementById('topic').style.display = (document.getElementById('topic-use').checked ? 'block' : 'none');
  ProcessStepTwoCompletion();
}
function ProcessStepTwoCompletion()
{
  if (document.getElementById('article-type').value == "" || (document.getElementById('topic-use').checked &&
  	document.getElementById('topic').selectedIndex == 0) || (document.getElementById('related-group-use').checked &&
  	document.getElementById('related-group-type').selectedIndex == 0) || (document.getElementById('related-group-use').checked &&
  	document.getElementById('related-group-type').selectedIndex > 0 && document.getElementById('related-group-' +
  	document.getElementById('related-group-type').options[document.getElementById('related-group-' +
  	'type').selectedIndex].value).selectedIndex == 0))
  	{
  	  document.getElementById('step-two-continue').disabled = true;
  	  return;
  	}
  document.getElementById('step-two-continue').disabled = false;
}
function returnStepTwo()
{
  collapseAllStepContainers();
  document.getElementById('container-step-two').style.display = "block";
}
function goStepThree()
{
  document.getElementById('timeline-step-two').className = "piece piecePast";
  document.getElementById('timeline-step-three').className = "piece pieceCurrent";
  document.getElementById('container-step-two').style.display = "none";
  document.getElementById('container-step-three').style.display = "block";
//  document.getElementById('timeline-step-three').onclick = goStepThree;
}

/* Step Three */
tinyMCE.init({
            mode : "exact",
            elements : "beat-article",
            entity_encoding : "named",
            theme : "advanced",
            skin : "o2k7",
            editor_selector : "advanced-textarea",
            plugins : "safari,spellchecker,pagebreak,style,layer,save,advhr,advimage,advlink," +
	    	"iespell,inlinepopups,preview,media,searchreplace,print,contextmenu,paste,directionality," +
	    	"fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
            theme_advanced_buttons1 : "cut,copy,paste,pastetext,|,search,replace,|,undo,redo,|,cleanup,code,|," +
	    	"preview,|,fullscreen,removeformat,visualaid,|,styleprops,spellchecker,|,cite,abbr,acronym,del,inc,attribs,",
            theme_advanced_buttons2 : "bold,italic,underline,strikethrough,|,forecolor,link,unlink,|,sub,sup,|,charmap," +
	    	"iespell,|,bullist,numlist,|,outdent,indent",
	    theme_advanced_buttons3 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : false,
            template_external_list_url : "js/template_list.js",
            external_link_list_url : "js/link_list.js",
            remove_trailing_nbsp : true,
            height : '200',
            setup : function (ed) {
            	ed.onKeyUp.add(processStepThreeConfirmation); }
          });
function processStepThreeConfirmation(ed, e)
{
  var article_input = ed.getContent().replace(/<\/?[^>]+(>|$)/g, "");
  document.getElementById('step-three-continue').disabled = (article_input.length < 40);
}
function goStepFour()
{
  document.getElementById('timeline-step-three').className = "piece piecePast";
  document.getElementById('timeline-step-four').className = "piece pieceCurrent";
  document.getElementById('container-step-three').style.display = "none";
  document.getElementById('container-step-four').style.display = "block";
}

/* Step Four */
function changeAlignment(alignment)
{
  document.getElementById('image-alignment').value = alignment;
  if (alignment == 'left')
  {
    document.getElementById('alignment-left').className = 'alignmentIcon alignmentSelected';
    document.getElementById('alignment-right').className = 'alignmentIcon alignmentUnselected';
  } else if (alignment == 'right')
  {
    document.getElementById('alignment-left').className = 'alignmentIcon alignmentUnselected';
    document.getElementById('alignment-right').className = 'alignmentIcon alignmentSelected';
  }
}
var cropbox_real_x;
var cropbox_real_y;
var cropbox_x;
var cropbox_y;
var crop_width;
var crop_height;
function processStartIconCreation(image_file, image_width, image_height)
{
  document.getElementById('icon-creation-container').innerHTML = "";
  cropbox_real_x = image_width;
  cropbox_real_y = image_height;
  var center_box = document.createElement('center');
  center_box.setAttribute('id', 'center-icon-image-box');
  center_box.className = 'separator';
  var image_icon_crop_box = document.createElement('img');
  var icon_submit_button = document.createElement('input');
  icon_submit_button.className = 'submitButton separator';
  icon_submit_button.type = 'button';
  icon_submit_button.setAttribute('id', 'icon-crop-submit');
  icon_submit_button.value = 'Create Icon';
  icon_submit_button.setAttribute('onClick', 'cropIcon(\"' + image_file + '\")');
  center_box.appendChild(image_icon_crop_box);
  image_icon_crop_box.src = web_path + "/purgatory/" + image_file;
  image_icon_crop_box.className = 'cropBox';
  image_icon_crop_box.setAttribute('id', 'icon-crop-box');
  center_box.appendChild(icon_submit_button);
  var icon_info_text = document.createElement("div");
  icon_info_text.setAttribute("id", "icon-info-text");
  document.getElementById('icon-creation-container').appendChild(icon_info_text);
  document.getElementById('icon-creation-container').appendChild(center_box);
  icon_info_text.innerHTML = "Crop an icon from the picture you have uploaded, then click 'Create Icon' to save the icon file.";
  jQuery(function(){
	jQuery('#icon-crop-box').Jcrop({
		aspectRatio: (74/58),
		bgOpacity: 0.4,
		onSelect: updateIconCoords,
		onChange: updateIconCoords
	});
  });
}
function updateIconCoords(cropbox)
{
  var image_ratio = (cropbox_real_x >= 300 ? cropbox_real_x / 300 : cropbox_real_x);
  cropbox_x = cropbox.x * (cropbox_real_x > 300 ? image_ratio : 1);
  cropbox_y = cropbox.y * (cropbox_real_x > 300 ? image_ratio : 1);
  crop_width = cropbox.w * (cropbox_real_x > 300 ? image_ratio : 1);
  crop_height = cropbox.h * (cropbox_real_x > 300 ? image_ratio : 1);
}
function cropIcon(image_file)
{
  if (cropbox_x == null || cropbox_y == null || crop_width == null || crop_height == null)
  {
    alert("You must make a selection for the icon before you can crop an icon.");
	return;
  }
  document.getElementById('center-icon-image-box').style.display = "none";
  document.getElementById('icon-info-text').innerHTML = "Cropping icon...";
  executeAJAX(web_path + "/components/crop-beat-icon.php?image=" + image_file + "&x=" + cropbox_x + "&y=" + cropbox_y + "&width=" + crop_width +
	"&height=" + crop_height, function processResults(result)
	{
	  if (result.indexOf("[Error]") > -1)
	  {
	    document.getElementById('center-icon-image-box').style.display = "block";
		document.getElementById('icon-info-text').innerHTML = "Crop an icon from the picture you have uploaded, then click 'Create Icon' to save the icon file.";
	    alert(result.replace("[Error] ", ""));
		return;
	  } else
	  {
	    document.getElementById('icon-info-text').innerHTML = "Crop successful!";
	    document.getElementById('image-icon').value = result;
		var icon_image = document.createElement("img");
		icon_image.src = web_path + "/limbo/" + result;
		icon_image.className = "beatIcon";
		document.getElementById('icon-creation-container').appendChild(icon_image);
		processStepFourConfirmation();
	  }
	});
}
function processImageCredit(imageCredit)
{
  imageCredit = imageCredit.replace(/<\/?[^>]+(>|$)/g, "");
  if (imageCredit.length > 0)
  {
    document.getElementById('preview-image-credit').style.display = 'block';
    document.getElementById('preview-image-credit').innerHTML = '<b>[Preview]</b> Photo courtesy of ' + imageCredit + '.';
  } else if (document.getElementById('preview-image-credit').style.display == 'block')
    fadeOutAndHide('preview-image-credit');
  document.getElementById('image-credit').className = (imageCredit.length > 4 ? "inputText formComponentValid" :
  	"inputText formComponentInvalid");
}
tinyMCE.init({
	mode: "exact",
	elements : "image-caption",
	theme : "simple",
	skin : "o2k7",
        setup : function (ed) {
            ed.onKeyUp.add(processStepFourConfirmation); }
        });
function processStepFourConfirmation(ed, e)
{
  if (!ed) var ed = tinyMCE.get('image-caption');
  if (document.getElementById("image-filepath").value == "" ||
  	document.getElementById("image-credit").value.replace(/<\/?[^>]+(>|$)/g, "").length < 5 ||
  	ed.getContent().replace(/<\/?[^>]+(>|$)/g, "").length < 10 ||
	document.getElementById("image-icon").value == "")
  	{
  	  document.getElementById('step-four-continue').disabled = true;
  	  return;
  	}
  document.getElementById('step-four-continue').disabled = false;
}
function goStepFive()
{
  document.getElementById('timeline-step-four').className = "piece piecePast";
  document.getElementById('timeline-step-five').className = "piece pieceCurrent";
  document.getElementById('container-step-four').style.display = "none";
  document.getElementById('container-step-five').style.display = "block";
}

/* Step Five */
function goStepSix()
{
  updateFormFinal();
  document.getElementById('timeline-step-five').className = "piece piecePast";
  document.getElementById('timeline-step-six').className = "piece pieceEnd pieceCurrent";
  document.getElementById('container-step-five').style.display = "none";
  document.getElementById('container-step-six').style.display = "block";
}

/* Step Six */
function updateFormFinal()
{
  document.getElementById('final-title').value = document.getElementById('beat-title').value;
  document.getElementById('final-type').value = document.getElementById('article-type').value;
  if (document.getElementById('related-group-use').checked)
  {
    var groupSubtypeSelect = document.getElementById('related-group-' +
  	document.getElementById('related-group-type').options[document.getElementById('related-group-' +
  	'type').selectedIndex].value);
    document.getElementById('final-related-group').value = groupSubtypeSelect.options[groupSubtypeSelect.selectedIndex].value;
  } else
    document.getElementById('final-related-group').value = null;
  document.getElementById('final-topic').value = (document.getElementById('topic-use').checked ? document.getElementById('topic').value : null);
  document.getElementById('final-article').value = tinyMCE.get('beat-article').getContent();
  document.getElementById('final-image-file').value = document.getElementById('image-filepath').value;
  document.getElementById('final-alignment').value = document.getElementById('image-alignment').value;
  document.getElementById('final-photo-credit').value = document.getElementById('image-credit').value;
  document.getElementById('final-caption').value = tinyMCE.get('image-caption').getContent();
  document.getElementById('final-icon-file').value = document.getElementById('image-icon').value;
}
function processSubmitCompose()
{
  var composeCall = web_path + '/components/compose-beat.php?';
  composeCall += "title=" + escapeStringURL(document.getElementById('beat-title').value);
  composeCall += "&type=" + document.getElementById('article-type').value;
  if (document.getElementById('related-group-use').value)
  {
    var groupSubtypeSelect = document.getElementById('related-group-' +
  	document.getElementById('related-group-type').options[document.getElementById('related-group-' +
  	'type').selectedIndex].value);
    composeCall += "&group=" + groupSubtypeSelect.options[groupSubtypeSelect.selectedIndex].value;
  }
  if (document.getElementById('topic-use').value)
    composeCall += "&topic=" + document.getElementById("topic").options[document.getElementById("topic").selectedIndex].value;
  composeCall += "&article=" + escapeStringURL(tinyMCE.get('beat-article').getContent());
  composeCall += "&image=" + escapeStringURL(document.getElementById('image-filepath').value);
  composeCall += "&alignment=" + document.getElementById('image-alignment').value;
  composeCall += "&image_credit=" + escapeStringURL(document.getElementById('image-credit').value);
  composeCall += "&caption=" + escapeStringURL(tinyMCE.get('image-caption').getContent());
  document.write(composeCall);
  executeAJAX(composeCall, function processPost(result)
  {
    if (result.indexOf('[Error]') < 0)
    {
      beat_posted = true;
      window.location = web_path + "/beat/compose/success-" + result + "/";
      return;
    }
    alert(result);
  });
}
	  
function getWarnPageRefresh()
	  {
	    if (comic_submitted)
		  return null;
	    if (document.getElementById('comic-title').value == '' & document.getElementById('comic-filepath').value == '')
		  return null;
		return 'Are you sure you wish to leave this page? (Unsaved content will be lost unless you save it)';
	  }