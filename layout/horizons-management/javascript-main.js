function executeAJAX(ajaxcall, evaluate_function)
{
  var AJAX;
  try
  {
    AJAX = new XMLHttpRequest();
  }
  catch(e)
  {
    try
    {
      AJAX = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch(e)
    {
      try
      {
        AJAX = new ActiveXObject("Microsoft.XMLHTTP");
      }
      catch(e)
      {
        alert("Your browser does not support AJAX.");
        return false;
      }
    }
  }
  AJAX.onreadystatechange = function()
  {
    if (AJAX.readyState == 4)
      if (AJAX.status == 200)
        evaluate_function(AJAX.responseText);
      else
        alert("Error: " + AJAX.statusText + ". " + AJAX.status);
  }
  AJAX.open("get", ajaxcall, true);
  AJAX.send(null);
}

function validateLoginAttempt()
{
  var returnValue = true;
  if (document.getElementById("login-username").value.length == 0)
  {
    document.getElementById("login-username-error").innerHTML = "Name must be one or more characters.";
    document.getElementById("login-username-error").style.display = "block";
    fadeOutAndHide("login-username-error", 2500);
    returnValue = false;
  } else if (document.getElementById("login-username").value.indexOf(" ") == -1)
  {
    document.getElementById("login-username-error").innerHTML = "Name must be both first and last name, separated by a space.";
    document.getElementById("login-username-error").style.display = "block";
    fadeOutAndHide("login-username-error", 2500);
    returnValue = false;
  }
  if (document.getElementById("login-password").value.length < 6)
  {
    document.getElementById("login-password-error").innerHTML = "Password must be six or more characters.";
    document.getElementById("login-password-error").style.display = "block";
    fadeOutAndHide("login-password-error", 2500);
    returnValue = false;
  }
  return returnValue;
}

function fadeOutAndHide(elem, after_time)
{
  if (after_time != undefined)
  {
    setTimeout("fadeOutAndHide('" + elem + "')", after_time);
    return;
  }
  document.getElementById(elem).isFading = true;
  fadeOutIncrement(elem);
}
var fadeSpeed = 50;
function fadeOutIncrement(elem, previous_amount_taken)
{
  if (previous_amount_taken == undefined)
    var previous_amount_taken = 1;
  if (document.getElementById(elem).style.opacity != undefined)
    var opacity = document.getElementById(elem).style.opacity;
  else
    var opacity = document.getElementById(elem).style.filter.replace("alpha(opacity='", "").replace("')", "");
  if (opacity == undefined || opacity == "")
    opacity = 100;
  opacity -= previous_amount_taken * 2;
  if (opacity < 0)
    opacity = 0;
    
  if (opacity > 0)
  {
    if (document.getElementById(elem).style.opacity != undefined)
      document.getElementById(elem).style.opacity = opacity / 10;
    else
      document.getElementById(elem).style.filter = "alpha(opacity='" + opacity + "')";
    setTimeout("fadeOutIncrement('" + elem + "', " + previous_amount_taken * 2 + ")", fadeSpeed);
  } else
  {
    document.getElementById(elem).style.display = "none";
    if (document.getElementById(elem).style.opacity != undefined)
      document.getElementById(elem).style.opacity = 1.0;
    else
      document.getElementById(elem).style.filter = "alpha(opacity='100')";
	document.getElementById(elem).isFading = false;
  }
}

function navigationMove(direction)
{
  if (navigationOffset + direction < 0 || navigationOffset + direction + 8 >= navigation_groups.length ||
	(direction < 0 && document.getElementById("navigationMove-Left").className.indexOf("disabled") > 0) ||
	(direction > 0 && document.getElementById("navigationMove-Right").className.indexOf("disabled") > 0))
    return;
  var toHide = (direction > 0 ? navigationOffset : navigationOffset + 8);
  var toShow = (direction > 0 ? navigationOffset + 9 : navigationOffset - 1);
  document.getElementById("navigationGroup-" + navigation_groups[toHide]).className += " hidden";
  document.getElementById("navigationGroup-" + navigation_groups[toShow]).className = 
	document.getElementById("navigationGroup-" + navigation_groups[toShow]).className.replace(" hidden", "");
  navigationOffset += direction;
  document.getElementById("navigationMove-Left").className = "navigationMove left" + (navigationOffset == 0 ? " disabled" : "");
  document.getElementById("navigationMove-Right").className = "navigationMove right" + (navigationOffset + 9 >= navigation_groups.length ? " disabled" : "");
}

function openNavigationMenu(reference_handle)
{
  if (document.getElementById("navigationMenu-" + reference_handle) == undefined)
    return;
  document.getElementById("navigationMenu-" + reference_handle).style.display = "block";
}
function closeNavigationMenu(reference_handle)
{
  if (document.getElementById("navigationMenu-" + reference_handle) == undefined)
    return;
  document.getElementById("navigationMenu-" + reference_handle).style.display = "none";
}
function adjustPageNavigation()
{
  var current_left = 3;  // Navigation container left padding
  for (var index = 0; index < navigation_groups.length; index++)
  {
    current_left += 4; // Width of element left margin and left border
    if (document.getElementById("navigationMenu-" + navigation_groups[index]) != undefined)
      if (current_left + 258 > 952) // Exceeded the width of the navigation bar. Reverse positioning
        document.getElementById("navigationMenu-" + navigation_groups[index]).className = "navigationMenuRight";
    current_left += 100; // Width of individual navigation group element and right border
  }
}


var escapable = /[\\\"\x00-\x1f\x7f-\uffff]/g,
        meta = {    // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        };
function escapeStringURL(string)
{
  escapable.lastIndex = 0;
        return escapable.test(string) ?
            encodeURIComponent(string.replace(escapable, function (a) {
                var c = meta[a];
                return typeof c === 'string' ? c :
                    '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
            })) :
            encodeURIComponent(string);
}