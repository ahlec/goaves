/* Featuring Content */
function processFeatureSubmit(feature_type, feature_identity)
{
  if (!document.getElementById('feature-header-crop-x') || !document.getElementById('feature-header-crop-y') || !document.getElementById('feature-header-crop-width') ||
	!document.getElementById('feature-header-crop-height'))
	{
	  alert("[Error] The feature parameters do not exist. It is unlikely that you are in within a feature content window.")
	  return;
	}
  if (!document.getElementById('feature-submit') || document.getElementById('feature-submit').disabled)
  {
    alert("[Error] You cannot yet post your feature. Please complete all steps.");
	return;
  }
  if (!document.getElementById('feature-image-filepath') || document.getElementById('feature-image-filepath').value == null)
  {
    alert ("[Error] Must upload an image in order to feature content.");
	return;
  }
  if (document.getElementById('feature-header-crop-x').value == null || document.getElementById('feature-header-crop-x').value.length == 0 ||
	document.getElementById('feature-header-crop-y').value == null || document.getElementById('feature-header-crop-y').value.length == 0 ||
	document.getElementById('feature-header-crop-width').value == null || document.getElementById('feature-header-crop-width').value.length == 0 ||
	document.getElementById('feature-header-crop-width').value < 1 || document.getElementById('feature-header-crop-height').value == null ||
	document.getElementById('feature-header-crop-height').value.length == 0 || document.getElementById('feature-header-crop-height').value < 1)
  {
    alert ("[Error] You must crop a part of your image in order to feature it.");
	return;
  }
  document.getElementById('feature-submit').disabled = true;
  var featureCall = web_path + "/components/set-feature-component.php?type=" + feature_type + "&identity=" + feature_identity +
	"&image=" + document.getElementById('feature-image-filepath').value + "&x=" + document.getElementById('feature-header-crop-x').value + 
	"&y=" + document.getElementById('feature-header-crop-y').value + "&width=" + document.getElementById('feature-header-crop-width').value +
	"&height=" + document.getElementById('feature-header-crop-height').value;
  executeAJAX(featureCall, function processResults(result)
  {
    if (result.indexOf("[Error] ") >= 0)
	{
	  alert(result.replace("[Error] ", ""));
	  document.getElementById('feature-submit').disabled = false;
	  return;
	}
    window.location = web_path + "/" + result;
  });
}