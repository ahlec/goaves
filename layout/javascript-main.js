/* Header-based Javascript */
function headerLogoMouseOver()
{
  document.getElementById("pageHeader").className = "headerContainerOver";
}
function headerLogoMouseOut()
{
  document.getElementById("pageHeader").className = "headerContainer";
}

/* Navigation-based Javascript */
function sideColumnPostMouseOver(element)
{
  element.className = "sidePostContainerHover";
}
function sideColumnPostMouseOut(element)
{
  element.className = "sidePostContainer";
}
function sideColumnPostClick(url)
{
  window.location = url;
}

/* Beats-based Javascript */
function beatsListMouseOver(element)
{
  element.className = "beatsListContainerHover";
}
function beatsListMouseOut(element)
{
  element.className = "beatsListContainer"
}

/* Staff-based Javascript */
function staffListMouseOver(element)
{
  element.className = "staffListCellHover";
}
function staffListMouseOut(element)
{
  element.className = "staffListCell";
}

/* Photo Gallery-based Javascript */
function photoGalleryMouseOver(element)
{
  element.className = "photoGalleryIconHover";
}
function photoGalleryMouseOut(element)
{
  element.className = "photoGalleryIcon";
}