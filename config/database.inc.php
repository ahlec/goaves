<?php
function sqlite_group_concat_step(&$context, $string, $separator = ',') { $context .= $separator . $string; }
function sqlite_group_concat_finalize(&$context) { return $context; }
function sqlite_trim($string, $characters = null) { return ($characters != null ? trim($string, $characters) : trim($string)); }
function sqlite_replace($string, $needle, $replace) { return str_replace($needle, $replace, $string); }
class DeitloffDatabase
{
  private $_database;
  function __construct($path)
  {
    try
	{
      $this->_database = sqlite_open($path);
	  $this->exec("PRAGMA short_column_names = ON");
	  $this->exec("PRAGMA encoding = \"UTF-8\"");
	  $this->createAggregate("group_concat", "sqlite_group_concat_step", "sqlite_group_concat_finalize", 2);
	  $this->createFunction("trim", "sqlite_trim", 2);
	  $this->createFunction("replace", "sqlite_replace", 3);
    } catch (Exception $e)
	{
	  return false;
	}
	return true;
  }
  function query($query)
  {
    $results = sqlite_query($this->_database, $query);
    return ($results == FALSE ? FALSE : new DeitloffDatabaseResult($results));
  }
  function querySingle($query, $return_full_row = false)
  {
    $results = sqlite_fetch_array(sqlite_query($this->_database, $query, true), SQLITE_ASSOC);
	if (sizeof($results) == 0 || is_bool($results) || $return_full_row)
	  return $results;
	$result_column_names = array_keys($results);
	return $results[$result_column_names[0]];
  }
  function exec($query)
  {
    return sqlite_exec($this->_database, $query);
  }
  function escapeString($string)
  {
    return sqlite_escape_string($string);
  }
  function close()
  {
    sqlite_close($this->_database);
  }
  function lastErrorCode()
  {
    return sqlite_last_error($this->_database);
  }
  function lastErrorMsg()
  {
    return sqlite_error_string($this->lastErrorCode());
  }
  function libEncoding()
  {
    return sqlite_libencoding();
  }
  function createFunction($function_name, $callback, $number_args)
  {
    sqlite_create_function ($this->_database, $function_name, $callback, $number_args);
  }
  function createAggregate($function_name, $step_function, $finalize_function, $number_args)
  {
    sqlite_create_aggregate ($this->_database, $function_name, $step_function, $finalize_function, 2);
  }
}
class DeitloffDatabaseResult
{
  private $_results;
  function __construct($results)
  {
	$this->_results = $results;
  }
  function fetchArray()
  {
    return sqlite_fetch_array($this->_results, SQLITE_ASSOC);
  }
  function reset()
  {
    return sqlite_rewind($this->_results);
  }
  function numberRows()
  {
    return sqlite_num_rows($this->_results);
  }
  function fieldName($position)
  {
    return @sqlite_field_name($this->_results, $position);
  }
}
?>