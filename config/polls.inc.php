<?php
function loadLatestPoll($database, $already_voted)
{
  if (!defined("DOCUMENT_ROOT"))
    require_once ("functions.inc.php");
  $get_poll = $database->querySingle("SELECT identity, poll_handle, date_start, date_end, poll_question FROM polls WHERE date_start <= '" .
	date("Y-m-d", strtotime("+1 hours")) . "' AND date_end >= '" . date("Y-m-d", strtotime("+1 hours")) . "' ORDER BY date_start DESC, date_end DESC LIMIT 1", true);
  if ($get_poll === false)
    return false;
  $poll = array("IDENTITY" => $get_poll["identity"], "HANDLE" => $get_poll["poll_handle"], "QUESTION" => format_content($get_poll["poll_question"]), "ALREADY_VOTED" => $already_voted,
	"TOTAL_VOTES" => $database->querySingle("SELECT count(*) FROM poll_votes WHERE poll_identity='" . $get_poll["identity"] . "'"));
    $get_responses = $database->query("SELECT response_identity, response FROM poll_responses WHERE poll_identity='" . $get_poll["identity"] . "'");
    $poll["RESPONSES"] = array();
    while ($response = $get_responses->fetchArray())
      $poll["RESPONSES"][] = array("IDENTITY" => $response["response_identity"], "TEXT" => $response["response"], "VOTES" => $database->querySingle("SELECT count(*) FROM poll_votes WHERE " .
		"poll_identity='" . $get_poll["identity"] . "' AND response_identity='" . $response["response_identity"] . "'"));
  return $poll;
}
function outputProgressBar($label, $reg_value, $reg_value_counter, $perc_value, $width, $short_form = true)
{
  echo "  <div class=\"progressBarContainer" . ($short_form ? " short" : "") . "\">\n";
  echo "    <div class=\"label\">" . ($short_form ? "" : "<b>") . $label . ($short_form ? "" : "</b> (" . $reg_value . " " . $reg_value_counter . ($reg_value != 1 ? "s" : "") . ")") . "</div>\n";
  echo "    <div class=\"barContainer\" style=\"width:" . $width . "px;\">\n";
  echo "      <div class=\"bar\" style=\"width:" . ($perc_value == 0 ? 0 : floor(($perc_value / 100) * $width)) .
	"px;" . ($perc_value >= 95 || $perc_value < 1 ? "border-right:0px;" : "") . "\"></div>\n";
  echo "      <div class=\"value\" style=\"width:" . $width . "px;\">" . $perc_value . "%</div>\n";
  echo "    </div>\n";
  echo "  </div>\n";
}
?>