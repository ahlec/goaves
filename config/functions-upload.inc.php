<?php
function UploadFile($upload_handle)
{
  if ($_FILES[$upload_handle]["error"] != 0)
  {
    switch ($_FILES[$upload_handle]["error"])
	{
	  case UPLOAD_ERR_INI_SIZE:
	  case UPLOAD_ERR_FORM_SIZE: return array("UPLOAD" => false, "MESSAGE" => "The file that you attempted to upload had a filesize that was too large.", 
		"FILEPATH" => "");
	  case UPLOAD_ERR_PARTIAL: return array("UPLOAD" => false, "MESSAGE" => "The file upload process was interrupted.", "FILEPATH" => "");
	  case UPLOAD_ERR_NO_FILE: return array("UPLOAD" => false, "MESSAGE" => "No file uploaded.", "FILEPATH" => "");
	  default: return array("UPLOAD" => false, "MESSAGE" => "Encountered an unhandled exception when attempting to upload (Upload error #" .
		$_FILES[$upload_handle]["error"] . ").", "FILEPATH" => "");
	}
  }
  if (!is_uploaded_file($_FILES[$upload_handle]["tmp_name"]))
    return array("UPLOAD" => false, "MESSAGE" => "Selected file was not an uploaded file.", "FILEPATH" => "");
   
   /*
  $image_size = getimagesize($_FILES["image-file"]["tmp_name"]);
  if ($image_size === false)
    return array("STATUS" => false, "MESSAGE" => "Uploaded file is not an image in the proper format.", "IMAGE_FILE" => "");
	
  $image_type = exif_imagetype($_FILES["image-file"]["tmp_name"]);
  if ($image_type != IMAGETYPE_GIF && $image_type != IMAGETYPE_JPEG && $image_type != IMAGETYPE_PNG)
    return array("STATUS" => false, "MESSAGE" => "Uploaded image file is not a file of an allowed type (JPEG, PNG, or GIF only).", "IMAGE_FILE" => "");

  switch ($image_type)
  {
    case IMAGETYPE_GIF: $imagetype = "gif"; break;
	case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
	case IMAGETYPE_PNG: $imagetype = "png"; break;
  }*/
  $tmp_name = preg_split("(\\\\|/)", $_FILES[$upload_handle]["tmp_name"]);
  $file_extension = explode(".", $_FILES[$upload_handle]["name"]);
  $tmp_name = $tmp_name[sizeof($tmp_name) - 1] . "." . $file_extension[sizeof($file_extension) - 1];
  
  if (!move_uploaded_file($_FILES[$upload_handle]["tmp_name"], MANAGE_DOCUMENT_ROOT . "/purgatory/" . $tmp_name))
    return array("UPLOAD" => false, "MESSAGE" => "Could not move uploaded file to purgatory.", "FILEPATH" => "");
  return array("UPLOAD" => true, "MESSAGE" => "", "FILEPATH" => $tmp_name);
}
function FileIsImage($filename)
{
  return (getimagesize($filename) !== false);
}
function FileIsAudio($filename)
{
  return true;
}

function UploadImageInput($upload_handle, $title, $cropped_javascript_variable, $original_javascript_variable, $crop_size_x, $crop_size_y, $post_crop_callback)
{
  if ($crop_size_x == null && $crop_size_y == null)
    exit ("Crop sizes cannot both be null.");
  echo "<iframe id=\"frame-" . $upload_handle . "-image\" name=\"" . $upload_handle . "Form\" class=\"formFrame\" style=\"display:none;\"></iframe>\n";
  echo "  <div class=\"formLabel miniSeparator\">" . $title . ":</div>\n";
  echo "  <input type=\"hidden\" id=\"" . $upload_handle . "-filepath\" value=\"\" />\n";
  echo "  <input type=\"hidden\" id=\"" . $upload_handle . "-image-ratio\" />\n";
  echo "  <div class=\"inputFile\" id=\"" . $upload_handle . "-image-input\">\n";
  echo "    <div class=\"button\">Select</div>\n";
  echo "    <div class=\"textBox\" id=\"" . $upload_handle . "-input-textbox\"></div>\n";
  if ($crop_size_x != null && $crop_size_y != null)
	$aspect_ratio = ($crop_size_x / $crop_size_y);
  else
    $aspect_ratio = 'false';
  echo "    <form method=\"POST\" enctype=\"multipart/form-data\" target=\"" . $upload_handle .
	"Form\" action=\"" . MANAGE_WEB_PATH . "/components/upload-image.php?aspect=" . $aspect_ratio . "&cropped_variable=" .
	$cropped_javascript_variable . "&original_variable=" . $original_javascript_variable . "&handle=" . $upload_handle . "&crop_width=" .
	$crop_size_x . "&crop_height=" . $crop_size_y . "&callback=" . $post_crop_callback . "\" " .
	"id=\"upload-" . $upload_handle . "-form\">\n";
  echo "      <input type=\"file\" class=\"inputFile\" name=\"" . $upload_handle . "-image\" onChange=\"document.getElementById('upload-" . $upload_handle . "-form').submit();" .
	"document.getElementById('upload-" . $upload_handle . "-form').disabled = true;\" id=\"" . $upload_handle . "-image\" />\n";
  echo "    </form>\n";
  echo "  </div>\n";
  echo "  <center id=\"" . $upload_handle . "-image-crop\" style=\"display:none;padding-top:10px;\">\n";
  echo "    <input type=\"hidden\" id=\"" . $upload_handle . "-crop-image-filepath\" />\n";
  echo "    <input type=\"hidden\" id=\"" . $upload_handle . "-crop-x\" />\n";
  echo "    <input type=\"hidden\" id=\"" . $upload_handle . "-crop-y\" />\n";
  echo "    <input type=\"hidden\" id=\"" . $upload_handle . "-crop-width\" />\n";
  echo "    <input type=\"hidden\" id=\"" . $upload_handle . "-crop-height\" />\n";
  echo "    <img id=\"" . $upload_handle . "-crop-img\" class=\"imageCropBox\" style=\"display:block;\" /><br />\n";
  echo "    <input type=\"button\" class=\"inputButton\" id=\"crop" . $upload_handle . "Button\" value=\"Crop\" />\n";
  echo "  </center>\n";
  echo "  <img id=\"" . $upload_handle . "-image-final\" style=\"display:none;\" class=\"imageCropBox\" />\n";
}

function FormDate($form_handle, $timestamp = null, $callback = null)
{
  if ($timestamp == null)
    $timestamp = time();
  echo "  <input type=\"hidden\" id=\"" . $form_handle . "-timestamp\" value=\"" . date("Y-m-d", $timestamp) . "\" />\n";
  echo "  <div class=\"inputDate\" id=\"" . $form_handle . "-container\">\n";
  echo "    <select id=\"" . $form_handle . "-day\" class=\"day\" onchange=\"validateDate('" . $form_handle . "'" . ($callback != null ?
	"," . $callback : "") . ");\">\n";
  for ($day = 0; $day < date("t", $timestamp); $day++)
    echo "      <option value=\"" . ($day + 1) . "\"" . (date("d", $timestamp) == ($day + 1) ? " selected=\"selected\"" : "") . ">" . ($day + 1) . "</option>\n";
  echo "    </select>\n";
  echo "    <select id=\"" . $form_handle . "-month\" class=\"month\" onchange=\"updateDaysDateSelection('" . $form_handle . "');validateDate('" . $form_handle . "'" .
	($callback != null ? "," . $callback : "") . ");\">\n";
  $months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
  foreach ($months as $index => $month)
    echo "      <option value=\"" . ($index + 1) . "\"" . (date("m", $timestamp) == ($index + 1) ? " selected=\"selected\"" : "") . ">" . $month . "</option>\n";
  echo "    </select>\n";
  echo "    <input type=\"text\" class=\"year\" maxlength=\"4\" id=\"" . $form_handle . "-year\" onkeyup=\"updateDaysDateSelection('" . $form_handle . "');validateDate('" .
	$form_handle . "'" . ($callback != null ? "," . $callback : "") . ");\" value=\"" . date("Y", $timestamp) . "\" />\n";
  echo "  </div>\n";
}
?>