<?php
function parse_time_pieces($string)
{
  $event_array = array();
  $date_pieces = explode("|", $string);
  if (strpos($string, " ") !== FALSE)
  {
    $pieces = explode(" ", $string);
	switch ($pieces[0])
	{
	  case "every": $event_array["EVERY"] = $pieces[1]; break;
	}
  } else if(sizeof($date_pieces) == 2)
  {
    $event_array["DATE_START"] = strtotime($date_pieces[0]);
    $event_array["DATE_END"] = strtotime($date_pieces[1]);
  } else if (sizeof($date_pieces) == 1)
    $event_array["DATE"] = strtotime($date_pieces[0]);
  return $event_array;
}
function sort_by_time_pieces($valA, $valB)
{
  if (isset($valA["EVERY"]) && isset($valB["EVERY"]))
    return 0;
  if (isset($valA["EVERY"]))
    return -1;
  if (isset($valB["EVERY"]))
    return 1;
 
  $dateA = (isset($valA["DATE"]) ? $valA["DATE"] : $valA["DATE_START"]);
  $dateB = (isset($valB["DATE"]) ? $valB["DATE"] : $valB["DATE_START"]);
  if ($dateA == $dateB)
    return 0;
  else if ($dateA > $dateB)
    return 1;
  return -1;
}
?>