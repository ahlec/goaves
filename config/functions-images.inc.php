<?php
class ProcessedImage
{
  private $_imageExtension;
  private $_imageType;
  private $_destWidth;
  private $_destHeight;

  private $_sourceFilename;
  private $_sourceImage = null;
  private $_destImage = null;
  function loadOriginal($filename)
  {
    $this->_sourceFilename = $filename;
    $image_info = getimagesize($filename);
    if (!$image_info)
      trigger_error("Image file not supported.");
    $this->_imageType = $image_info[2];
    $filename_pieces = explode(".", $filename);
    $this->_imageExtension = strtolower($filename_pieces[sizeof($filename_pieces) - 1]);

    switch ($image_info[2])
    {
      case IMAGETYPE_GIF:
        if (!$this->_sourceImage = imagecreatefromgif($filename))
          trigger_error("Could not open GIF '" . $filename . "'");
        break;
      case IMAGETYPE_PNG:
        if (!$this->_sourceImage = imagecreatefrompng($filename))
          trigger_error("Could not open PNG '" . $filename . "'");
        break;
      case IMAGETYPE_JPEG:
        if (!$this->_sourceImage = imagecreatefromjpeg($filename))
          trigger_error("Could not open JPEG '" . $filename . "'");
        break;
      default:
        trigger_error("Image file not supported");
    }

  }

  function scale($width, $height, $can_be_less_than = false, $keep_ratio = false, $ratio_width = null,
    $ratio_height = null)
  {
    $source_sizes = getimagesize($this->_sourceFilename);
    if (!$can_be_less_than)
    {
      if ($source_sizes[0] > $source_sizes[1])
      {
        $scaled_width = ($source_sizes[0] > $width ? $width : $source_sizes[0]);
        $scaled_height = ($source_sizes[1] / $source_sizes[0]) * $scaled_width;
      } else
      {
        $scaled_height = ($source_sizes[1] > $height ? $height : $source_sizes[1]);
        $scaled_width = ($source_sizes[0] / $source_sizes[1]) * $scaled_height;
      }
    } else
    {
      $scaled_width = $width;
      $scaled_height = $height;
      if ($keep_ratio)
      {
        if ($ratio_width == null || $ratio_height == null)
          trigger_error("While scaling, asked to maintain ratio, and did not supply ratio parameters.");
        if (($scaled_width / $scaled_height) != ($ratio_width / $ratio_height))
          trigger_error("Unable to maintain ratio while scaling. Supplied parameters invalid.");
      }
    }

    if (!$this->_destImage = imagecreatetruecolor($scaled_width, $scaled_height))
      trigger_error ("Unable to create image when scaling");
    if ($this->_imageType == IMAGETYPE_GIF || $this->_imageType == IMAGETYPE_PNG)
    {
      if ($this->_imageType == IMAGETYPE_PNG)
        imageAntiAlias($this->_destImage, true);
      imagecolortransparent($this->_destImage, imagecolorallocatealpha($this->_destImage, 0, 0, 0, 127));
      imagealphablending($this->_destImage, false);
      imagesavealpha($this->_destImage, true);
    }
    if (!imagecopyresampled($this->_destImage, $this->_sourceImage, 0, 0, 0, 0, $scaled_width, $scaled_height,
      $source_sizes[0], $source_sizes[1]))
	trigger_error ("Unable to scale image");
  }

  function crop($source_x, $source_y, $source_width, $source_height, $dest_width, $dest_height,
    $keep_ratio = false, $ratio_width = null, $ratio_height = null)
  {
    if ($keep_ratio && (($source_width / $source_height) != ($dest_width / $dest_height) ||
      ($dest_width / $dest_height) != ($ratio_width / $ratio_height)))
      trigger_error("While cropping, asked to maintain ratio, and unable to do so.");

    if (!$this->_destImage = imagecreatetruecolor($dest_width, $dest_height))
      trigger_error ("Unable to create image when croping");
    if ($this->_imageType == IMAGETYPE_GIF || $this->_imageType == IMAGETYPE_PNG)
    {
      if ($this->_imageType == IMAGETYPE_PNG)
        imageAntiAlias($this->_destImage, true);
      imagecolortransparent($this->_destImage, imagecolorallocatealpha($this->_destImage, 0, 0, 0, 127));
      imagealphablending($this->_destImage, false);
      imagesavealpha($this->_destImage, true);
    }
    if (!imagecopyresampled($this->_destImage, $this->_sourceImage, 0, 0, $source_x, $source_y,
      $dest_width, $dest_height, $source_width, $source_height))
	trigger_error ("Unable to crop image");
  }

  function save($destination_folder, $new_filename)
  {
    if ($this->_destImage == null)
      trigger_error("Destination image does not exist. Cannot save.");
    if ($this->_sourceImage == null)
      trigger_error("Source image does not exist. Cannot save.");
    $filename = $destination_folder . $new_filename;
    switch ($this->_imageType)
    {
      case IMAGETYPE_GIF:
        if (!imagegif($this->_destImage, $filename . "." . $this->_imageExtension))
          trigger_error("Unable to save GIF file");
        break;
      case IMAGETYPE_PNG:
        if (!imagepng($this->_destImage, $filename . "." . $this->_imageExtension, 8, 0))
          trigger_error("Unable to save PNG file");
        break;
      case IMAGETYPE_JPEG:
        if (!imagejpeg($this->_destImage, $filename . "." . $this->_imageExtension, 100))
          trigger_error("Unable to save JPEG file");
        break;
    }
    return $new_filename . "." . $this->_imageExtension;
  }
  
  function close()
  {
    imagedestroy($this->_destImage);
    imagedestroy($this->_sourceImage);
  }

  function deleteOriginalImage()
  {
    if (!unlink($this->_sourceFilename))
     trigger_error("Unable to delete the original image file from the temporary directory");
  }
}
?>