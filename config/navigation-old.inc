<?php
class MainTab
{
  private $_title;
  private $_linkSrc;
  function __construct($title, $link_src)
  {
    $this->_title = $title;
	$this->_linkSrc = $link_src;
  }
  function getTitle() { return $this->_title; }
  function getLinkSrc() { return $this->_linkSrc; }
}

function getMainTabs()
{
  return array(new MainTab("GoAves", "home"), new MainTab("News", "news"), new MainTab("Staff", "staff"),
	new MainTab("<i class=\"localLink\">The Leaf</i>", "leaf"), new MainTab("<i class=\"localLink\">The Log</i>", "log"),
	new MainTab("Multimedia", "multimedia"), new MainTab("Sports", "sports"), new MainTab("Groups", "group-listing"),
	new MainTab("Interactions", "interactions"));
}

function outputSiteNavigation($page)
{
  echo "    <div class=\"siteNavigation\">\n";
  
  echo "      <div class=\"main\" id=\"mainSiteNavigation\">\n";
  $main_tabs = getMainTabs();
  $loaded_tab = $page->getMainTab();
  foreach ($main_tabs as $tab)
    echo "        <a href=\"" . WEB_PATH . "/" . $tab->getLinkSrc() . "/\" class=\"item" . (strtolower($tab->getLinkSrc()) == $loaded_tab ? "LoadedHover" : "") .
    	"\" id=\"tab-" . strtolower($tab->getLinkSrc()) . "\" onMouseOver=\"navigationTab(this);\" onMouseOut=\"clearNavigation();\">" . $tab->getTitle() . "</a>\n";
    
  /*echo "<span class=\"item" . (strtolower($tab->getLinkSrc()) == $loaded_tab ? "LoadedHover" : "") . "\" id=\"tab-" . strtolower($tab->getLinkSrc()) .
		"\" onMouseOver=\"navigationTab(this);\" onMouseOut=\"clearNavigation();\" onClick=\"window.location='" . WEB_PATH . "/" .
		$tab->getLinkSrc() . "';\">" . $tab->getTitle() ."</span>\n";*/
  echo "      </div>\n";
  
  echo "      <div class=\"local\" id=\"localSiteNavigation\" onMouseOut=\"clearNavigation();\">\n";
  require_once ("components/local-navigation.php");
  outputLocalNavigation($loaded_tab, $page->getPageHandle(), $page->getPageSubhandle());
  echo "      </div>\n";
  echo "    </div>\n";
}

?>