<?php
class GoAvesLog
{
	private $_faq = array();
	function loadLogInfo($path, $database)
	{
	  if (isset($path[1]))
	  {
	    switch (strtolower($path[1]))
	    {
	      case "confirmations": return new GoAvesLogConfirmations();
	    }
	  }
	  $questions = $database->query("SELECT question, answer, requires_webroot FROM yearbook_faq_questions ORDER BY question_identity");
	  while ($question = $questions->fetchArray())
		$this->_faq[] = array("QUESTION" => $question["question"], "ANSWER" => ($question["requires_webroot"] ? 
			preg_replace("[\{WEB_PATH\}]", WEB_PATH, $question["answer"]) : $question["answer"]));
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  return $this->loadLogInfo($path, $database);
	}
	function getMainTab() { return "log"; }
	function getPageHandle() { return "log"; }
	function getPageSubhandle() { return null; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "<i>The Log</i> Information"); }
	function getPageTitle() { return "The Log FAQ"; }
	function getPageContents()
	{
	  echo "      <div class=\"yearbookHeader\">\n";
	  echo "        <div class=\"header\"><i>The Log</i> FAQ</div>\n";
	  echo "        <div class=\"subheader\">Questions with Mrs. Cheralyn Jardine, Yearbook Advisor</div>\n";
	  echo "      </div>\n";
	  echo "      <div class=\"yearbookContainer\">\n";
	  foreach ($this->_faq as $question)
	  {
	    echo "        <div class=\"entry\">\n";
	    echo "          <div class=\"question\"><b>Q:</b> " . $question["QUESTION"] . "</div>\n";
	    echo "          <div class=\"answer\"><b>A:</b> " . $question["ANSWER"] . "</div>\n";
	    echo "        </div>\n";
	  }
	  echo "      </div>\n"; 
	}
}
?>