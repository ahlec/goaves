<?php
class GoAvesStaffPortfolio {
	private $_identity;
	private $_firstName;
	private $_lastName;
	private $_shortBio;
	private $_currentPositions;
	private $_oldPositions = array();
	private $_links = array();
	private $_imageFile;
	private $_active;
	private $_works = array();
	private $_sectionsWithWorks = array();
	private $_adminLoadedValues = null;
	private $_email = null;
	private $_yearGraduating;
	private $_currentGrade;
	private $_loadedTab = null;
	function loadPortfolio($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "" || strpos($path[1], "-") === false)
	    return false;
	  $name = explode("-", $path[1], 2);
	  $info = $database->querySingle("SELECT identity, first_name, last_name, short_bio, positions, image_file, active, email, display_email_on_portfolio, year_graduating FROM staff WHERE first_name LIKE '" .
		$database->escapeString($name[0]) . "' AND last_name LIKE '" . $database->escapeString($name[1]) . "' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_identity = $info["identity"];
	  $this->_firstName = $info["first_name"];
	  $this->_lastName = $info["last_name"];
	  $this->_shortBio = format_content($info["short_bio"]);
	  $this->_currentPositions = $info["positions"];
	  $this->_imageFile = $info["image_file"];
	  $this->_active = ($info["active"] == "TRUE");
	  if ($info["display_email_on_portfolio"] == "TRUE")
	    $this->_email = $info["email"];
	  $this->_yearGraduating = $info["year_graduating"];
	  $current_grade = (date("m") < 6 ? $this->_yearGraduating - date("Y") : $this->_yearGraduating - date("Y") - 1);
	  $grades_by_year = array("Senior","Junior","Sophomore","Freshman");
	  if ($current_grade < 0)
	    $this->_currentGrade = "Graduated " . $this->_yearGraduating;
          else
	    $this->_currentGrade = $grades_by_year[$current_grade];

	  $all_old_positions = $database->query("SELECT start_year, end_year, position, approval_pending FROM previous_staff_positions WHERE staff_identity='" . $this->_identity . "'" . 
		(!ADMIN_VIEW ? " AND approval_pending='FALSE'" : "") . " ORDER BY sort_order, start_year DESC");
	  while ($old_position = $all_old_positions->fetchArray())
	    $this->_oldPositions[] = array("START" => $old_position["start_year"], "END" => $old_position["end_year"], "POSITION" => $old_position["position"], "APPROVAL_PENDING" =>
			($old_position["approval_pending"] == "TRUE"));
	  $staff_links = $database->query("SELECT url, link_type FROM staff_links WHERE staff_identity='" . $this->_identity . "' ORDER BY sort_order ASC");
	  while ($link = $staff_links->fetchArray())
	    $this->_links[] = array("URL" => $link["url"], "TYPE" => $link["link_type"]);

	  $load_articles = $database->query("SELECT handle, title, last_updated, beat_identity, published, contents FROM beats WHERE staff_identity='" .
		$this->_identity . "'" . (ADMIN_VIEW ? "" : " AND (published='TRUE' OR published IS NULL)") . " ORDER BY last_updated DESC");
	  while ($article = $load_articles->fetchArray())
	  {
	    $this->_works[] = array("TYPE" => "article", "HANDLE" => $article["handle"], "TITLE" => format_content($article["title"]),
			"DATETIME" => strtotime($article["last_updated"]), "PUBLISHED" => ($article["published"] == "TRUE"),
			"EXTRA" => $article["contents"]);
		if (!in_array("article", $this->_sectionsWithWorks))
		  $this->_sectionsWithWorks[] = "article";
	  }
		
	  /*$load_works = $database->query("SELECT * FROM staff_works WHERE staff_identity='" . $this->_identity . "'" . (ADMIN_VIEW ? "" : " AND (published='TRUE' OR published IS NULL)") . " ORDER BY work_type ASC, datestamp DESC");
	  while ($work = $load_works->fetchArray())
	  {
	    $this->_works[] = array("TYPE" => $work["work_type"], "HANDLE" => $work["handle"], "TITLE" => format_content($work["title"]),
			"DATETIME" => strtotime($work["datestamp"]), "PUBLISHED" => ($work["published"] == "TRUE" || $work["published"] == "" ? true : false),
			"EXTRA" => $work["extras"]);
	    if (!in_array($work["work_type"], $this->_sectionsWithWorks))
	      $this->_sectionsWithWorks[] = $work["work_type"];
	  }*/
	  $load_leaf_items = $database->query("SELECT leaf_issuu.handle, leaf_issuu.issue_name, leaf_items.leaf_identity, leaf_issuu.color_cover, leaf_items.page_number, " .
		"leaf_items.item_title, leaf_items.item_type, leaf_issuu.issue_name AS \"leaf_title\", leaf_items.subtitle FROM leaf_items JOIN leaf_issuu ON leaf_items.leaf_identity = leaf_issuu.issuu_identity WHERE " .
		"staff_identities = '" . $this->_identity . "' OR staff_identities LIKE '%," . $this->_identity . "' OR staff_identities LIKE '" . $this->_identity . ",%' OR " .
		"staff_identities LIKE '%," . $this->_identity . ",%' ORDER BY leaf_issuu.post_date DESC");
	  while ($leaf_item = $load_leaf_items->fetchArray())
	  {
	    $this->_works[] = array("TYPE" => "leaf", "HANDLE" => $leaf_item["handle"], "TITLE" => format_content($leaf_item["item_title"]), "DATETIME" => null,
		  "PAGE_NUMBER" => $leaf_item["page_number"], "LEAF_IDENTITY" => $leaf_item["leaf_identity"], "COVER" => $leaf_item["color_cover"],
		  "LEAF_TITLE" => $leaf_item["leaf_title"], "SUBTITLE" => format_content($leaf_item["subtitle"]));
		if (!in_array("leaf", $this->_sectionsWithWorks))
		  $this->_sectionsWithWorks[] = "leaf";
      }
		
	  if (isset($path[2]) && in_array(mb_strtolower($path[2]), $this->_sectionsWithWorks))
	    $this->_loadedTab = mb_strtolower($path[2]);
		
	  if (ADMIN_VIEW)
	  {
	    $loaded_values = $database->querySingle("SELECT permissions, valid_article_types, password, active, display_on_staff_listing FROM " .
		"staff WHERE identity='" . $this->_identity . "' LIMIT 1", true);
	    $this->_adminLoadedValues = array("PERMISSIONS" => $loaded_values["permissions"], "VALID_ARTICLE_TYPES" => $loaded_values["valid_article_types"],
		"DEFAULT_PASSWORD" => ($loaded_values["password"] == md5("default")), "ACTIVE" => ($loaded_values["active"] == "TRUE"),
		"DISPLAY_ON_STAFF_LISTING" => ($loaded_values["display_on_staff_listing"] == "TRUE"));
	  }
	  return true;
	}
	function checkOrRedirect($path, $database)
	{	
	  if (!$this->loadPortfolio($path, $database))
	    return new GoAvesStaff();
	  return true;
	}
	function getMainTab() { return "staff"; }
	function getPageHandle() { return "staff-portfolio"; }
	function getPageSubhandle() { return mb_strtolower($this->_firstName . "-" . $this->_lastName); }
	function getBreadTrail() { return array("home" => "GoAves.com", "staff" => "Staff Listing", "[this]" => "[Portfolio] " . $this->_firstName .
		" " . $this->_lastName); }
	function getPageTitle() { return $this->_firstName . " " . $this->_lastName; }
	function getBodyOnload() { return "staffPortfolioLoad('" . mb_strtolower($this->_firstName) . "','" . mb_strtolower($this->_lastName) . "')"; }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/staff/\">&laquo; Return to staff listing</a></div>\n";

// BEGIN
	  echo "      <div class=\"portfolioContainer\">\n";
	  echo "        <div class=\"staffName\">" . $this->_firstName . " " . $this->_lastName . "</div>\n";
	  echo "        <div class=\"staffSubtitle\">" . $this->_currentGrade . " &ndash; <b>" . ($this->_active ? $this->_currentPositions :
	  	"No longer on staff") . "</b></div>\n";
	  echo "        <img class=\"staffPicture\" src=\"" . WEB_PATH . "/images/staff/" . $this->_imageFile . "\" />\n";
	  echo "        <div class=\"staffBiography\">" . $this->_shortBio . "</div>\n";
	  echo "        <div class=\"staffLinks\">\n";
	  foreach ($this->_links as $link)
	  {
	    echo "          <a href=\"" . $link["URL"] . "\" target=\"_blank\" /><img src=\"" . WEB_PATH . "/images/icons/";
	    switch ($link["TYPE"])
	    {
	      case "FACEBOOK": echo "icon-fcbk.png"; break;
	      case "TWITTER": echo "icon-twitter.png"; break;
	      case "YOUTUBE": echo "icon-youtube.png"; break;
	      case "MYSPACE": echo "icon-myspace.png"; break;
	      case "STUMBLE_UPON": echo "icon-stumbleupon.png"; break;
	      case "DEVIANT_ART": echo "icon-deviantart.png"; break;
	    }
	    echo "\" border=\"0\" class=\"staffLinkIcon\" /></a>\n";
	  }
	  echo "        </div>\n";
	  echo "        <div style=\"clear:both;\"></div>\n";
	  echo "        <div class=\"staffHistory\">\n";
	  if ($this->_active)
	  {
	    echo "          <div class=\"historyHeader\">Current Position</div>\n";
	    echo "          <div class=\"historyText\">" . $this->_currentPositions . "</div>\n";
	  }
	  echo "          <div class=\"historyHeader\">Previous Positions</div>\n";
	  if (sizeof($this->_oldPositions) > 0)
	  {
	    echo "          <ul>\n";
	    foreach ($this->_oldPositions as $previous_position)
	      echo "          <li><b>" . $previous_position["START"] . "-" . $previous_position["END"] . "</b> &ndash; " . $previous_position["POSITION"] .
			($previous_position["APPROVAL_PENDING"] ? " <i>(Approval Pending</i>" : "") . "</li>\n";
	    echo "          </ul>\n";
	  } else
	    echo "          <div class=\"historyText\">No previous positions on staff</div>\n";
	  echo "        </div>\n";
	  echo "        <div class=\"staffWorksNavigation\" id=\"staff-works-navigation\">\n";
	  $works_navigations = array();
	  $works_navigations[] = array("HANDLE" => "article", "TEXT" => "Articles");
	  $works_navigations[] = array("HANDLE" => "cartoon", "TEXT" => "Comics");
	  $works_navigations[] = array("HANDLE" => "leaf", "TEXT" => "<i>Leaf</i> Contributions");
	  $works_navigations[] = array("HANDLE" => "sound_slides", "TEXT" => "Soundslides");
	  foreach ($works_navigations as $identity => $nav)
	  {
		echo "          <span id=\"staff-works-nav-" . $nav["HANDLE"] . "\" class=\"" . (in_array($nav["HANDLE"], $this->_sectionsWithWorks) ? "enabled" : "disabled");
		if ($this->_loadedTab !== null)
		  echo ($this->_loadedTab == $nav["HANDLE"] ? " selected" : "");
		else
		  echo (array_search($nav["HANDLE"], $this->_sectionsWithWorks) === 0 ? " selected" : "");
		echo "\"" . (in_array($nav["HANDLE"], $this->_sectionsWithWorks) ? " onClick=\"staffPortfolioWorksChange('" . $nav["HANDLE"] . "');\"" : "") .
			">" . $nav["TEXT"] . "</span>" . ($identity + 1 < sizeof($works_navigations) ?
			" | \n" : "\n");
	  }
	  echo "        </div>\n";
	  echo "        <div class=\"staffWorks\" id=\"staff-works\">\n";
	  $previous_work_type = null;
	  $previous_leaf_issue = null;
	  foreach ($this->_works AS $work)
	  {
	    if ($previous_work_type == null OR $previous_work_type != $work["TYPE"])
	    {
	      if ($previous_work_type != null)
	        echo "          </div>\n";
	      echo "          <div id=\"staff-works-" . $work["TYPE"] . "\"";
		  if ($this->_loadedTab !== null)
		    echo ($this->_loadedTab == $work["TYPE"] ? "" : " style=\"display:none;\"");
		  else
		    echo (array_search($work["TYPE"], $this->_sectionsWithWorks) !== 0 ? " style=\"display:none;\"" : "");
	      echo ">\n";
	      $previous_work_type = $work["TYPE"];
	    }
	    if ($work["TYPE"] == "cartoon")
	      echo "<a href=\"" . WEB_PATH . "/comic/" . $work["HANDLE"] . "/\" title=\"" . $work["TITLE"] . "\"><img src=\"" . WEB_PATH . "/images/cartoons/" . $work["EXTRA"] . "\" border=\"0\" " .
			"class=\"worksImageIcon" . (!$work["PUBLISHED"] ? " unpublishedMaterialNote" : "") . "\" /></a>\n";
	    else if ($work["TYPE"] == "article")
	      echo "<div class=\"worksArticleContainer" . (!$work["PUBLISHED"] ? " unpublishedMaterialNote" : "") .
			"\"><div class=\"title\"><a href=\"" . WEB_PATH . "/article/" . $work["HANDLE"] .
			"/\" title=\"" . $work["TITLE"] . "\">" . $work["TITLE"] . "</a>" . (!$work["PUBLISHED"] ? " <span class=\"unpublishedMaterialNote\">[Unpublished]</span>" : "") . "</div><div class=\"" .
			"subtitle\">Last Updated: " . date(DATE_FORMAT, $work["DATETIME"]) . "</div><div class=\"snippet\">" .
			get_smart_blurb($work["EXTRA"], 30) . "... <b><a href=\"" . WEB_PATH . "/article/" . $work["HANDLE"] . 
			"/\">Read More &raquo;</a></b></div></div>\n";
		else if ($work["TYPE"] == "sound_slides")
		{
		  echo ($previous_leaf_issue != null ? "<div style=\"clear:both;\"></div></div>\n" : ""); // previous type on profile: leaf articles
		  echo "<div class=\"worksSlideshowContainer" . (!$work["PUBLISHED"] ? " unpublishedMaterialNote" : "") .
			"\"><a href=\"" . WEB_PATH . "/sound-slides/" . $work["HANDLE"] . "/\"><img src=\"" . WEB_PATH . "/images/sound_slides/" .
			$work["EXTRA"] . "\" border=\"0\"><br />" . $work["TITLE"] . "</div></a>\n";
		}
		else if ($work["TYPE"] == "leaf")
		{
		  if ($previous_leaf_issue != $work["LEAF_IDENTITY"])
		  {
		    echo ($previous_leaf_issue != null ? "<div style=\"clear:both;\"></div></div>\n" : "");
			echo "<div class=\"worksLeafContainer\">\n";
			echo "  <a href=\"" . WEB_PATH . "/leaf-issue/" . $work["HANDLE"] . "/\" target=\"_blank\"><img src=\"" . WEB_PATH . "/images/leaf_covers/" .
				$work["COVER"] . "\" border=\"0\" class=\"cover\" /></a>\n";
			echo "  <a href=\"" . WEB_PATH . "/leaf-issue/" . $work["HANDLE"] . "/\" target=\"_blank\"><div class=\"title\">" . $work["LEAF_TITLE"] . "</div></a>\n";
		    $previous_leaf_issue = $work["LEAF_IDENTITY"];
		  }
		  echo "  <a href=\"" . WEB_PATH . "/leaf-issue/" . $work["HANDLE"] . "/page-" . $work["PAGE_NUMBER"] . "/\" target=\"_blank\" class=\"leafItem\">(" . 
			$work["TYPE"] . ") " . $work["TITLE"];
			if (!in_array(substr($work["TITLE"], -1), array(".", "?", "!", ";", ":")) && $work["SUBTITLE"] != "")
			  echo ": ";
			if ($work["SUBTITLE"] != "")
			  echo $work["SUBTITLE"];
			echo "</a><br />\n";
		}
		else
	     echo "- " . $work["TITLE"] . " (" . $work["HANDLE"] . " - " . $work["TYPE"] . ")<br />\n";
	  }
	  if (sizeof($this->_works) > 0)
	  echo "          </div>\n";
	  echo "        </div>\n";
	  echo "        <div style=\"clear:both;\"></div>\n";
	  echo "      </div>\n";

	  if (ADMIN_VIEW)
	  {
	    echo "<div class=\"adminViewBox\">\n";
	    echo "<b>Staff Identity:</b> " . $this->_identity . "<br />\n";
	    echo "<b>Graduation Year:</b> " . $this->_yearGraduating . "<br />\n";
	    echo "<b>Permissions:</b> " . $this->_adminLoadedValues["PERMISSIONS"] . "<br />\n";
	    echo "<b>Valid Article Types:</b> " . $this->_adminLoadedValues["VALID_ARTICLE_TYPES"] . "<br />\n";
	    echo "<b>Using default password:</b> " . ($this->_adminLoadedValues["DEFAULT_PASSWORD"] ? "<font color=\"red\">YES</font>" : "No") . "<br />\n";
	    echo "<b>Can log in:</b> " . ($this->_adminLoadedValues["ACTIVE"] ? "Yes" : "No") . "<br />\n";
	    echo "<b>Display on staff listing:</b> " . ($this->_adminLoadedValues["DISPLAY_ON_STAFF_LISTING"] ? "Yes": "No") . "<br />\n";
	    echo "<a href=\"" . WEB_PATH . "/manage/admin-staff-info.php?staff_id=" . $this->_identity . "\" target=\"_blank\">Edit Information</a><br />\n";
	    echo "<a href=\"" . WEB_PATH . "/manage/admin-staff-reset-password.php?staff_id=" . $this->_identity . "\" target=\"_blank\">Reset Password to Default Password</a><br />\n";
	    echo "<a href=\"" . WEB_PATH . "/manage/admin-staff-permissions.php?staff_id=" . $this->_identity . "\" target=\"_blank\">Edit Permissions</a>\n";
	    echo "</div>\n";
	  }
	}
}
?>