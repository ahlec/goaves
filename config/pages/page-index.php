<?php
class GoAvesIndex {
	private $_recentFeatures = array();
	private $_recentMajorStories = array();
	private $_homepageBeatTypes = array();
	private $_goavesSnippet;
	private $_latestPoll;
	private $_calendarEvents = array();
	function loadIndex($path, $database)
	{ 
	  $get_features = $database->query("SELECT * FROM consolidated_features " . (ADMIN_VIEW ? "" : "WHERE published='TRUE' ") . "ORDER BY datestamp DESC LIMIT " . MAJOR_STORIES_TO_DISPLAY);
	  while ($feature = $get_features->fetchArray())
	  {
		switch ($feature["type"])
		{
		  case "article": $page_handle = "article"; $type_text = "Article"; $see_more_text = "Full Story"; $blurb_text = true; $bar_present = true; break;
		  case "infographic": $page_handle = "infographic"; $type_text = "Infographic"; $see_more_text = null; $blurb_text = true; $bar_present = false; break;
		  case "photo_gallery": $page_handle = "photo-gallery"; $type_text = "Photo Gallery"; $see_more_text = "Browse Photos"; $blurb_text = false; $bar_present = true; break;
		  case "soundslides": $page_handle = "sound-slides"; $type_text = "Slideshow"; $see_more_text= "Watch"; $blurb_text = false; $bar_present = true; break;
		  case "special": $page_handle = "special"; $type_text = "Special"; $see_more_text = "Full Coverage"; $blurb_text = false; $bar_present = true; break;
		}
		if ($blurb_text)
		{
		  $smart_blurb = get_smart_blurb(format_content($feature["text"]), 54);
		  $final_blurb_character = substr($smart_blurb, strlen($smart_blurb)*-1);
		  $smart_blurb = (mb_strlen($smart_blurb) == 0 ? null : $smart_blurb . "... ");
		} else
		{
		  $smart_blurb = format_content($feature["text"]);
		  $final_blurb_character = null;
		}
		$this->_recentFeatures[] = array("HANDLE" => $feature["handle"], "TITLE" => format_content($feature["title"]), "TYPE" => $feature["type"], "TYPE_TEXT" => $type_text,
			"TEXT" => $smart_blurb, "IMAGE" => $feature["header_image"],
			"PUBLISHED" => ($feature["published"] == "TRUE"), "LINK" => WEB_PATH . "/" . $page_handle . "/" . $feature["handle"] . "/", "LINK_TEXT" => $see_more_text . "&raquo;",
			"DATE" => ($feature["last_updated"] != "" ? strtotime($feature["last_updated"]) : null));
		if (!$bar_present)
		  $this->_recentFeatures[sizeof($this->_recentFeatures) - 1]["TEXT"] = null;
	  }
	  
	  /*$get_major_stories = $database->query("SELECT header_image, beats.title, beats.handle, beats.post_time, beats.contents FROM " .
		"major_stories JOIN beats ON major_stories.article_identity = beats.beat_identity WHERE beats.published='TRUE' ORDER BY " .
		"beats.post_time DESC LIMIT " . MAJOR_STORIES_TO_DISPLAY);
	  while ($major_story = $get_major_stories->fetchArray())
	  {
	    $smart_blurb = get_smart_blurb(format_content($major_story["contents"]), 50);
		$final_blurb_character = substr($smart_blurb, strlen($smart_blurb)*-1);
	    $this->_recentMajorStories[] = array("HEADER_IMAGE" => $major_story["header_image"],
			"TITLE" => format_content($major_story["title"]), "HANDLE" => $major_story["handle"], "POST_TIME" => strtotime($major_story["post_time"]),
			"CONTENTS" => ($smart_blurb . "... <a href='" . WEB_PATH . "/article/" . $major_story["handle"] . "/'>Full story &raquo;</a>"));
	  }*/

	  $get_frontpage_beat_types = $database->query("SELECT type_identity, handle, title, icon_file FROM beat_types WHERE " .
		"feature_on_front_page='TRUE' ORDER BY sort_order ASC, title ASC");
	  while ($fbt = $get_frontpage_beat_types->fetchArray())
	  {
	    $this->_homepageBeatTypes[] = array("HANDLE" => $fbt["handle"], "TITLE" => $fbt["title"], "ICON" => $fbt["icon_file"]);
		$articles = array();
		$get_frontpage_articles = $database->query("SELECT handle, title, contents, image_file, icon_file FROM beats WHERE published='TRUE' AND type='" .
			$fbt["type_identity"] . "' ORDER BY post_time DESC LIMIT " . FEATURED_ARTICLE_TYPES_ARTICLES_TO_DISPLAY);
		while ($article = $get_frontpage_articles->fetchArray())
		  $articles[] = array("HANDLE" => $article["handle"], "TITLE" => format_content($article["title"]), "CONTENTS" => get_smart_blurb(
			utf8_encode($article["contents"]), 40) . "...", "IMAGE_FILE" => $article["image_file"], "ICON_FILE" => $article["icon_file"]);
		$this->_homepageBeatTypes[sizeof($this->_homepageBeatTypes) - 1]["ARTICLES"] = $articles;
	  }
	  
	  require_once (DOCUMENT_ROOT . "/config/polls.inc.php");
	  $this->_latestPoll = loadLatestPoll($database, isset($_SESSION[POLL_VOTING_SESSION]));
	/*exit("SELECT date, calendar.title, groups.title AS \"group_title\", groups.handle AS \"group_handle\" FROM calendar JOIN groups ON " .
		"calendar.related_group = groups.group_identity WHERE date >= '" . date("Y-m-d", strtotime("+1 hours")) . "' OR date LIKE 'every %' OR (date LIKE '%|%' AND " .
		"substr(date,0,10) <= '" . date("Y-m-d", strtotime("+1 hours")) . "' AND substr(date,11) >= '" . date("Y-m-d", strtotime("+1 hours")) . "') ORDER BY date LIMIT " . DATES_ON_FRONT_PAGE);*/
	  /*$get_calendar_events = $database->query("SELECT date, calendar.title, groups.title AS \"group_title\", groups.handle AS \"group_handle\" FROM calendar JOIN groups ON " .
		"calendar.related_group = groups.group_identity WHERE date >= '" . date("Y-m-d", strtotime("+1 hours")) . "' OR date LIKE 'every %' OR (date LIKE '%|%' AND " .
		"substr(date,0,10) <= '" . date("Y-m-d", strtotime("+1 hours")) . "' AND substr(date,11,10) >= '" . date("Y-m-d", strtotime("+1 hours")) . "') ORDER BY date LIMIT " . DATES_ON_FRONT_PAGE);
	  while ($calendar_event = $get_calendar_events->fetchArray())
	    $this->_calendarEvents[] = array("TITLE" => $calendar_event["title"], "RELATED_GROUP_HANDLE" => $calendar_event["group_handle"], "RELATED_GROUP_TITLE" => $calendar_event["group_title"],
		parse_time_pieces($calendar_event["date"]));
	  usort($this->_calendarEvents, "sort_by_time_pieces");*/
	}
	function checkOrRedirect($path, $database) { $this->loadIndex($path, $database); return true; }
	function getMainTab() { return "home"; }
	function getPageHandle() { return "home"; }
	function getPageSubhandle() { return null; }
	function getBreadTrail() { return null; }
	function getPageTitle() { return null; }
	function getBodyOnload() { return "startFeaturesRotation()"; }
	function getPageJavascript()
	{
	  $javascript = "var features = new Array();\n";
	  foreach ($this->_recentFeatures as $index => $feature)
	  {
	    $javascript .= "features[" . $index . "] = new Array('" . $feature["TYPE"] . "','" . $feature["TYPE_TEXT"] . "','" . addslashes($feature["TITLE"]) . "','" .
			$feature["IMAGE"] . "','" . addslashes($feature["TEXT"]) . "','" . addslashes($feature["LINK"]) . "','" . addslashes($feature["LINK_TEXT"]) . "');\n";
	  }
	  return $javascript;
	}
	function getPageContents()
	{
	  echo "      <div class=\"indexContainer\">\n";
	  
	  echo "        <div class=\"featuresContainer\">\n";
	  echo "          <div class=\"view\">\n";
	  echo "            <div class=\"image back\" id=\"feature-image-back\"></div>\n";
	  echo "            <div class=\"image front\" id=\"feature-image-front\" style=\"background-image:url('" . WEB_PATH . "/images/features/" .
		$this->_recentFeatures[0]["IMAGE"] . "');\"></div>\n";
	  echo "            <div class=\"type\" id=\"feature-type\">" . $this->_recentFeatures[0]["TYPE_TEXT"] . "</div>\n";
	  echo "            <div class=\"title\" id=\"feature-title\">" . $this->_recentFeatures[0]["TITLE"] . "</div>\n";
	  echo "            <div class=\"content\" onMouseOver=\"stopFeaturesRotation();\" onMouseOut=\"startFeaturesRotation();\" id=\"feature-content\">" .
		$this->_recentFeatures[0]["TEXT"] . "</div>\n";
	  echo "            <div class=\"actionBar\" onMouseOver=\"stopFeaturesRotation();\" onMouseOut=\"startFeaturesRotation();\">\n";
	  echo "              <b><a href=\"" . $this->_recentFeatures[0]["LINK"] . "\" id=\"feature-action-link\">" . $this->_recentFeatures[0]["LINK_TEXT"] . "</a></b>\n";
	  echo "            </div>\n";
	  echo "          </div>\n";
	  echo "          <div class=\"navigation\" id=\"feature-navigation\">\n";
	  foreach ($this->_recentFeatures as $index => $feature)
	  {
	    echo "            <div class=\"item" . ($index == 0 ? " selected" : "") . ($index < sizeof($this->_recentFeatures) - 1 ? "" : " last") .
			"\" id=\"feature-" . $index . "\" onClick=\"selectFeature(" . $index .
			");\" onMouseOver=\"stopFeaturesRotation();\" onMouseOut=\"startFeaturesRotation();\">\n";
		echo "              <div class=\"title\">" . $feature["TITLE"] . "</div>\n";
		echo "              <div class=\"type\">" . $feature["TYPE_TEXT"] . "</div>\n";
		echo "              <div class=\"date\">" . ($feature["DATE"] != null ? date(DATE_FORMAT, $feature["DATE"]) : "") . "</div>\n";
		echo "            </div>\n";
	  }
	  echo "          </div>\n";
	  echo "        </div>\n";
	  
	  // Tier One: GoAves Snippet (Left), Major Stories (Right)
	  /*
	  echo "        <script>\n";
	  echo "          var features_definitions = new Array();\n";
	  for ($populate_array = 0; $populate_array < sizeof($this->_recentFeatures); $populate_array++)
	  {
	    echo "          features_definitions[" . $populate_array . "] = new Array('" . ($populate_array + 1) . "','";
		switch ($this->_recentFeatures[$populate_array]["TYPE"])
		{
		  case "infographic": echo "Infographic"; break;
		  case "article": echo "Article"; break;
		  case "photo_gallery": echo "Photo Gallery"; break;
		  case "soundslides": echo "Soundslides"; break;
		}
		echo "','" . addslashes((!$this->_recentFeatures[$populate_array]["PUBLISHED"] ? "<span class=\"unpublishedMaterialNote\">[Unpublished]" .
			"</span> " : "") . $this->_recentFeatures[$populate_array]["TITLE"]) . "','";
		if ($this->_recentFeatures[$populate_array]["TEXT"] == null)
		  echo "none";
		else
		  echo addslashes($this->_recentFeatures[$populate_array]["TEXT"]);
		echo "','" . WEB_PATH . "/images/features/" . $this->_recentFeatures[$populate_array]["IMAGE"] . "');\n";
	  }
	  echo "        </script>\n";
	  echo "        <div class=\"majorStoryContainer\" style=\"background-image:url('" . WEB_PATH . "/images/features/" .
		$this->_recentFeatures[0]["IMAGE"] . "');\" id=\"feature-container\">\n";
	  echo "          <div class=\"type\" id=\"feature-type\">";
	  switch ($this->_recentFeatures[0]["TYPE"])
	  {
	    case "article": echo "Article"; break;
		case "infographic": echo "Infographic"; break;
		case "photo_gallery": echo "Photo Gallery"; break;
		case "soundslides": echo "Soundslides"; break;
	  }
	  echo "</div>\n";
	  echo "          <div class=\"storyContent\" id=\"feature-info-container\" " . ($this->_recentFeatures[0]["TEXT"] == null ?
		"style=\"display:none;\" " : "") . "onMouseOver=\"stopFeaturesInterval();\" onMouseOut=\"startFeaturesInterval();\">";
	  echo "            <div class=\"title\" id=\"feature-title\">" . (!$this->_recentFeatures[0]["PUBLISHED"] ? "<span class=\"unpublishedMaterialNote\">[Unpublished]" .
		"</span> " : "") . $this->_recentFeatures[0]["TITLE"] . "</div>\n";
	  echo "            <div id=\"feature-contents\">" . $this->_recentFeatures[0]["TEXT"] . "</div>\n";
	  echo "          </div>\n";
	  echo "        </div>\n";
	  echo "        <div class=\"majorStoryNavigation\" id=\"feature-navigation\">\n";
	  $number_words = array("One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten");
	  for ($navigation_out = 0; $navigation_out < sizeof($this->_recentFeatures); $navigation_out++)
	    echo "          <div class=\"item" . ($navigation_out == 0 ? " active" : "") . "\" onClick=\"toggleFeature('" .
			($navigation_out + 1) . "');\" id=\"feature-" . ($navigation_out + 1) . "\" onMouseOver=\"stopFeaturesInterval();\" onMouseOut=\"startFeaturesInterval();\">" .
			$number_words[$navigation_out] . "</div>" . ($navigation_out < sizeof($this->_recentFeatures) - 1 ?
			" | " : "") . "\n";
	  echo "        </div>\n";*/
	  
	  echo "        <div class=\"goavesSnippetContainer\" id=\"snippet\">\n";
	  echo preg_replace("/\/\*([^*]*)\*\//sU", "", preg_replace("/\{WEB_PATH\}/",WEB_PATH, utf8_encode(file_get_contents(DOCUMENT_ROOT . "/components/index-goaves-snippet.txt"))));
	  echo "        </div>\n";

	  echo "        <div style=\"clear:both;\"></div>\n";
	  
	  /* Latest Poll */
	  echo "      <div class=\"pollContainer\" id=\"current-poll-container\">\n";
	  echo "        <div class=\"header\">" . ($this->_latestPoll === false ? "Current Poll" : "<a href=\"" . WEB_PATH . "/poll/" . $this->_latestPoll["HANDLE"] . "/\">Current Poll</a>") . "</div>\n";
	  if ($this->_latestPoll === false)
	    echo "        <div class=\"no_poll\">There are no current polls. Check back soon, though!</div>\n";
	  else
	  {
	    echo "        <div class=\"question\">" . $this->_latestPoll["QUESTION"] . "</div>\n";
	    if (!$this->_latestPoll["ALREADY_VOTED"])
	    {
		  echo "        <div id=\"pollFormContainer\">\n";
	      echo "        <form name=\"latest_poll\" onSubmit=\"return false;\">\n";
	      echo "          <input type=\"hidden\" name=\"latest-poll-identity\" id=\"latest-poll-identity\" value=\"" . $this->_latestPoll["IDENTITY"] . "\" />\n";
	      echo "          <input type=\"hidden\" name=\"latest-poll-handle\" id=\"latest-poll-handle\" value=\"" . $this->_latestPoll["HANDLE"] . "\" />\n";
	      foreach ($this->_latestPoll["RESPONSES"] as $response)
	        echo "          <div class=\"entry\"><input type=\"radio\" name=\"response\" id=\"poll-response-" . $response["IDENTITY"] . "\" value=\"" .
			$response["IDENTITY"] . "\"> <label for=\"poll-response-" . $response["IDENTITY"] . "\">" . $response["TEXT"] . "</label></div>\n";
	      echo "        </form>\n";
	      echo "        <div class=\"submit\"><button onClick=\"processPollVoting()\" id=\"poll-vote-button\">Vote</button></div>\n";
		  echo "        </div>\n";
	    } else
	    {
	      echo "        <div class=\"responses\">\n";
	      foreach ($this->_latestPoll["RESPONSES"] as $response)
	        outputProgressBar($response["TEXT"], null, null, ($this->_latestPoll["TOTAL_VOTES"] == 0 ? 0 : floor(($response["VOTES"] / $this->_latestPoll["TOTAL_VOTES"]) * 100)), 215);
	      echo "        </div>\n";
	    }
	  }
	  echo "      </div>\n";

	  /* Featured Article Types */
	  echo "      <div class=\"featuredArticleTypesContainer\">\n";
	  foreach ($this->_homepageBeatTypes as $featured_type)
	  {
	    echo "        <div class=\"container\">\n";
		echo "          <div class=\"title\"><span>" . ($featured_type["HANDLE"] == "beat" ? "Main" :
			"<a href=\"" . WEB_PATH . "/news/" . $featured_type["HANDLE"] . "/\">" . format_content($featured_type["TITLE"]) . "</a>") . "</span></div>\n";
		if (sizeof($featured_type["ARTICLES"]) > 0)
		{
		  echo "          <a href=\"" . WEB_PATH . "/article/" . $featured_type["ARTICLES"][0]["HANDLE"] . "/\" class=\"first title\">" .
			$featured_type["ARTICLES"][0]["TITLE"] . "</a><br />\n";
		  if ($featured_type["ARTICLES"][0]["IMAGE_FILE"] != "")
		    echo "          <a href=\"" . WEB_PATH . "/article/" . $featured_type["ARTICLES"][0]["HANDLE"] . "/\" class=\"no_underline\"><img src=\"" .
			WEB_PATH . "/images/articles/" . $featured_type["ARTICLES"][0]["IMAGE_FILE"] . "\" border=\"0\" class=\"articleImage\" /></a>\n";
		  echo "          <div class=\"firstBlurb\">" . $featured_type["ARTICLES"][0]["CONTENTS"] . "</div>\n";
		  echo "          <a href=\"" . WEB_PATH . "/article/" . $featured_type["ARTICLES"][0]["HANDLE"] .
			"/\" class=\"fullArticle\">Read More &raquo;</a>\n";
		  echo "          <div style=\"clear:both;\"></div>\n";
		  
		  if (sizeof($featured_type["ARTICLES"]) > 1)
		    echo "          <div class=\"divider\"></div>\n";
		  for ($article = 1; $article < sizeof($featured_type["ARTICLES"]); $article++)
		    echo "          <a href=\"" . WEB_PATH . "/article/" . $featured_type["ARTICLES"][$article]["HANDLE"] .
				"/\" class=\"other\"><img src=\"" . WEB_PATH . "/images/articles/" . ($featured_type["ARTICLES"][$article]["ICON_FILE"] != "" ?
				$featured_type["ARTICLES"][$article]["ICON_FILE"] : $featured_type["ARTICLES"][$article]["IMAGE_FILE"]) . "\" border=\"0\" />" .
				$featured_type["ARTICLES"][$article]["TITLE"] . "</a>\n";
		} else
		  echo "          <center>No articles found.</center>\n";
		echo "        </div>\n";

	  }
	  echo "      </div>\n";

	  echo "      <div style=\"clear:both;\"></div>\n";
	  
	  /* Latest Multimedia AND Polls */
	  
	  echo "      </div>\n";
	  echo "      <div style=\"clear:both;\"></div>\n";
	}
}
?>