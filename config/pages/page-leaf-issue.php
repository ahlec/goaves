<?php
class GoAvesLeafIssue {
	private $_leafIdentity;
	private $_title;
	private $_handle;
	private $_documentID;
	private $_documentName;
	private $_loadingText;
	private $_postDate;
	private $_numberPages;
	private $_colorCover;
	
	private $_schoolYearPublished;
	
	private $_loadedPage = null;
	private $_items = array();
	private $_contributors = array();
	function loadIssue($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT document_id, document_name, loading_text, post_date, issue_name, handle, number_pages, issuu_identity, color_cover " .
		"FROM leaf_issuu WHERE handle LIKE '" .
		$database->escapeString($path[1]) . "' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_leafIdentity = $info["issuu_identity"];
	  $this->_documentID = $info["document_id"];
	  $this->_docmentName = $info["document_name"];
	  $this->_loadingText = $info["loading_text"];
	  $this->_postDate = strtotime($info["post_date"]);
	  $this->_title = $info["issue_name"];
	  $this->_handle = $info["handle"];
	  $this->_numberPages = $info["number_pages"];
	  $this->_colorCover = $info["color_cover"];
	  if (isset($path[2]) && strpos($path[2], "page-") === 0)
	  {
	    $loaded_page_number = str_replace("page-", "", $path[2]);
		if (ctype_digit($loaded_page_number) && $loaded_page_number > 0 && $loaded_page_number <= $this->_numberPages)
		  $this->_loadedPage = $database->escapeString($loaded_page_number);
	  }
	  $this->_schoolYearPublished = date("Y", $this->_postDate);
	  if (date("m", $this->_postDate) < 8)
	    $this->_schoolYearPublished--;
	  $current_school_year = (date("m") < 8 ? date("Y") - 1 : date("Y"));
	  
	  $get_items = $database->query("SELECT page_number, item_title, subtitle, item_type, noteworthy, related_group, topic FROM leaf_items WHERE leaf_identity='" .
		$this->_leafIdentity . "' ORDER BY page_number ASC");
	  while ($item = $get_items->fetchArray())
	    $this->_items[] = array("PAGE_NUMBER" => $item["page_number"], "TITLE" => format_content($item["item_title"]), "SUBTITLE" => ($item["subtitle"] != "" ?
			format_content($item["subtitle"]) : null), "TYPE" => $item["item_type"], "NOTEWORTHY" => ($item["noteworthy"] == "TRUE"));
	  $contrib_line = $database->querySingle("SELECT trim((SELECT group_concat(staff_identities,\",\") FROM leaf_items WHERE leaf_identity='" . $this->_leafIdentity .
		"' GROUP BY leaf_identity),\",\")");
	  if ($contrib_line != "")
	  {
	    $get_contributors = $database->query("SELECT identity, first_name, last_name, " . ($this->_schoolYearPublished != $current_school_year ?
			"previous_staff_positions.position" : "staff.positions AS \"position\"") . ", staff.icon_file FROM staff" . ($this->_schoolYearPublished != $current_school_year ?
			" LEFT JOIN previous_staff_positions ON (staff.identity = previous_staff_positions.staff_identity AND previous_staff_positions.start_year='" .
			$this->_schoolYearPublished . "' AND previous_staff_positions.approval_pending='FALSE')" : "") . " WHERE identity IN (" . $contrib_line . ") ORDER BY " .
			"staff.last_name, staff.first_name ASC");
	    while ($contributor = $get_contributors->fetchArray())
	    $this->_contributors[] = array("IDENTITY" => $contributor["identity"], "FIRST_NAME" => format_content($contributor["first_name"]),
			"LAST_NAME" => format_content($contributor["last_name"]), "POSITION" => ($contributor["position"] != "" ? format_content($contributor["position"]) :
			"Staff writer"), "ICON" => $contributor["icon_file"]);
	  }
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadIssue($path, $database))
	    return new GoAvesLeaf();
	  return true;
	}
	function getMainTab() { return "leaf"; }
	function getPageHandle() { return "leaf-issue"; }
	function getPageSubhandle() { return $this->_handle; }
	function getBreadTrail() { return array("leaf" => "<i>The Leaf</i> Archives", "[this]" => $this->_title); }
	function getPageTitle() { return $this->_title . " (The Leaf)"; }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/leaf/\">&laquo; Return to <i>The Leaf</i> archives</a></div>\n";
	  echo "      <div class=\"sidebarSpecContent\">\n";
	  require_once (DOCUMENT_ROOT . "/components/sidebar_modules/sidebar_leaf_items.php");
	  echo "      </div>\n";
	  
	  echo "      <div class=\"specContentContainer\">\n";
	  echo "        <div class=\"title\">" . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\"><span class=\"content-type\"><i>The Leaf</i></span> &ndash; Distributed in " . date("F Y", $this->_postDate) . "</div>\n";
	  
	  echo "        <div class=\"leafIssue\">\n";
	  echo "          <object class=\"leaf\" id=\"leafIssue\">\n";
	  echo "            <param name=\"movie\" value=\"http://static.issuu.com/webembed/viewers/style1/v1/IssuuViewer.swf?mode=embed" .
		"&amp;layout=" . WEB_PATH . "/layout/issuu/whiteMenu/layout.xml&amp;showFlipBtn=true&amp;documentId=" .
		$this->_documentID . "&amp;docName=" . $this->_documentName . "&amp;username=TheLeaf&amp;loadingInfoText=" .
		$this->_loadingText . "&amp;et=1280256249565&amp;er=66" . ($this->_loadedPage != null ? "&pageNumber=" . $this->_loadedPage : "") . "\" />\n";
      echo "            <param name=\"allowfullscreen\" value=\"true\"/>\n";
	  echo "            <param name=\"menu\" value=\"false\"/>\n";
	  echo "            <embed src=\"http://static.issuu.com/webembed/viewers/style1/v1/IssuuViewer.swf\" type=\"application/x-shockwave-flash\" " .
		"allowfullscreen=\"true\" menu=\"false\" style=\"width:600px;height:464px\" flashvars=\"mode=embed&amp;layout=" .
		"http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&amp;showFlipBtn=true&amp;documentId=" . $this->_documentID . 
		"&amp;docName=" . $this->_documentName . "&amp;username=TheLeaf&amp;loadingInfoText=" . $this->_loadingText .
		"&amp;et=1280256249565&amp;er=66\" />\n";
	  echo "          </object>\n";
	  echo "          <div class=\"issuuSubline\"><a href=\"http://issuu.com/TheLeaf/docs/" . $this->_documentName .
		"?mode=embed&amp;layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&amp;showFlipBtn=true\" target=\"_blank\">Open publication</a>" .
		" - Free <a href=\"http://issuu.com\" target=\"_blank\">publishing</a> - <a href=\"http://issuu.com/search?q=sycamore\" target=\"_blank\">" .
		"More sycamore</a></div>\n";
	  
	  echo "        <div class=\"contributorsContainer\">\n";
	  echo "          <div class=\"header\">In this issue:</div>\n";
	  foreach ($this->_contributors as $contributor)
	  {
	    echo "          <a href=\"" . WEB_PATH . "/staff-portfolio/" . mb_strtolower($contributor["FIRST_NAME"] . "-" . $contributor["LAST_NAME"]) .
			"/\" class=\"contributor\" target=\"_blank\" /><img src=\"" . WEB_PATH . "/images/staff/" . $contributor["ICON"] . "\" border=\"0\" />" . 
			"<div class=\"name\">" . $contributor["FIRST_NAME"] . " " . $contributor["LAST_NAME"] . "</div>" . $contributor["POSITION"] . 
			"<div style=\"clear:both;\"></div></a>\n";
	  }
	  echo "          <div style=\"clear:both;\"></div>\n";
	  echo "        </div>\n";
	  echo "        </div>\n";
	  echo "      </div>\n";
	  
	  if (ADMIN_VIEW)
	  {
	    echo "<div class=\"adminViewBox\">\n";
	    echo "<b>Identity:</b> " . $this->_leafIdentity . "<br />\n";
		echo "<b>Number of pages:</b> " . $this->_numberPages . "<br />\n";
		echo "<b>Loaded page:</b> " . ($this->_loadedPage !== null ? $this->_loadedPage : "None") . "<br />\n";
		echo "</div>\n";
	  }
	}
}
?>