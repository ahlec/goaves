<?php
class GoAvesArticle {
	private $_maxArticlesDisplayOnSide = 7;
	private $_identity;
	private $_staffIdentity;
	private $_authorTitle = "Author";
	private $_handle;
	private $_title;
	private $_postTime;
	private $_contents;
	private $_imageFile;
	private $_imageAlignment;
	private $_imageCaption;
	private $_authorFirstName;
	private $_authorLastName;
	private $_authorImage;
	private $_authorPosition;
	private $_typeIdentity;
	private $_typeTitle;
	private $_typeHandle;
	//private $_authorsOtherArticles = array();
	private $_otherTypeArticles = array();
	//private $_groupInformation;
	private $_relatedGroup;
	private $_photoCredit;
	private $_topic = null;

	private $_published;
	private $_isFeature;
	function loadArticle($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT beats.beat_identity, beats.handle, beats.title, beats.post_time, beats.contents, beats.published, " .
		"beats.image_file, beats.image_alignment, beats.image_caption, beats.type, beat_types.title AS \"type_title\", " .
		"beat_types.handle AS \"type_handle\", staff.first_name, staff.last_name, " .
		"staff.icon_file AS \"staff_picture\", staff.positions, beats.staff_identity, related_group, photo_credit, topics.topic_identity, topics.handle AS \"topic_handle\", topics.title AS \"topic_title\" " .
		"FROM beats JOIN staff ON beats.staff_identity = staff.identity JOIN beat_types ON " .
		"beat_types.type_identity = beats.type LEFT JOIN topics ON topics.topic_identity = beats.topic WHERE beats.handle LIKE '" . 
		$database->escapeString($path[1]) . "'" . (!ADMIN_VIEW ? " AND published='TRUE'" : "") . " LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_identity = $info["beat_identity"];
	  $this->_published = ($info["published"] == "TRUE");
	  $this->_isFeature = ($database->querySingle("SELECT count(*) FROM features WHERE feature_type='article' AND item_identity='" . $this->_identity . "'") > 0);
	  $this->_staffIdentity = $info["staff_identity"];
	  $this->_handle = $info["handle"];
	  $this->_title = format_content($info["title"]);
	  $this->_postTime = strtotime($info["post_time"]);
	  $this->_contents = format_content($info["contents"]);
	  $this->_imageFile = ($info["image_file"] != "" ? $info["image_file"] : null);
	  $this->_imageAlignment = ($info["image_file"] != "" ? strtolower($info["image_alignment"]) : null);
	  $this->_imageCaption = ($info["image_file"] != "" ? format_content($info["image_caption"]) : null);
	  $this->_authorFirstName = utf8_encode($info["first_name"]);
	  $this->_authorLastName = utf8_encode($info["last_name"]);
	  $this->_authorImage = ($info["staff_picture"] != "" ? $info["staff_picture"] : "no-staff-icon.jpg");
	  $this->_authorPosition = $info["positions"];
	  $this->_photoCredit = ($info["photo_credit"] != "" ? format_content($info["photo_credit"]) : null);
	  $this->_relatedGroup = ($info["related_group"] != "" ? $info["related_group"] : null);
	  /*if ($info["related_group"] != "" && ctype_digit($info["related_group"]))
	    $this->_groupInformation = getGroupInformation($database, $info["related_group"], $info["beat_identity"], "article");
	  else
	    $this->_groupInformation = null;*/
	  /*$other_articles = $database->query("SELECT handle, title FROM beats WHERE staff_identity='" . $info["staff_identity"] . "' AND beat_identity<>'" .
		$info["beat_identity"] . "' AND published='TRUE' ORDER BY post_time DESC LIMIT " .
		$this->_maxArticlesDisplayOnSide);*/
	  $other_type_articles = $database->query("SELECT handle, title FROM beats WHERE beat_identity<>'" . $info["beat_identity"] . "' AND published='TRUE' " .
		"AND type='" . $info["type"] . "' ORDER BY post_time DESC LIMIT " . $this->_maxArticlesDisplayOnSide);
	  $this->_typeIdentity = $info["type"];
	  $this->_typeTitle = $info["type_title"];
	  $this->_typeHandle = $info["type_handle"];
	  //while ($oa = $other_articles->fetchArray())
	  //  $this->_authorsOtherArticles[] = array("HANDLE" => $oa["handle"], "TITLE" => $oa["title"]);
	  while ($ota = $other_type_articles->fetchArray())
	    $this->_otherTypeArticles[] = array("HANDLE" => $ota["handle"], "TITLE" => format_content($ota["title"]));

	  if ($info["topic_identity"] != "")
	  {
	    $this->_topic = array("HANDLE" => $info["topic_handle"], "TITLE" => $info["topic_title"], "CONTENT" => array());
	    $get_other_items = $database->query("SELECT title, handle, type FROM topic_content WHERE ((type='article' AND identity<>'" .
		$info["beat_identity"] . "') OR (type<>'article')) AND topic='" . $info["topic"] . "' ORDER BY datestamp DESC LIMIT " .
		$this->_maxArticlesDisplayOnSide);
	    while ($content = $get_other_items->fetchArray())
	      $this->_topic["CONTENT"][] = array("HANDLE" => $content["handle"], "TITLE" => format_content($content["TITLE"]));
	  }

	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadArticle($path, $database))
	    return new GoAvesNews();
	  return true;
	}
	function getMainTab() { return "news"; }
	function getPageHandle() { return "article"; }
	function getPageSubhandle() { return $this->_handle; }
	function getBreadTrail() { return array("home" => "GoAves.com", "news" => "Latest News", "[this]" => $this->_title); }
	function getPageTitle() { return $this->_title; }

	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/news/\">&laquo; Return to news listing</a></div>\n";
	  echo "      <div class=\"sidebarSpecContent\">\n";
	  /*echo "        <a href=\"" . WEB_PATH . "/staff-portfolio/" . mb_strtolower($this->_authorFirstName . "-" . $this->_authorLastName) .
		"/\"><img src=\"" . WEB_PATH . "/images/staff/" . $this->_authorImage . "\" class=\"staffPicture\" border=\"0\" /></a>\n";
	  echo "        <div class=\"authorInfo\">\n";
	  echo "          <b><a href=\"" . WEB_PATH . "/staff-portfolio/" . mb_strtolower($this->_authorFirstName . "-" . $this->_authorLastName) . "/\">" .
		$this->_authorFirstName . " " . $this->_authorLastName . "</a></b><br />\n";
	  echo "          " . $this->_authorPosition . "\n";
	  echo "        </div>\n";
	  echo "        <div style=\"clear:left;\"></div>\n";*/
	  /*echo "        <div class=\"otherHeader\">Other articles by " . $this->_authorFirstName . " " . $this->_authorLastName . ":</div>\n";
	  if (sizeof($this->_authorsOtherArticles) == 0)
	    echo "        <center>" . $this->_authorFirstName . " has no other articles</center>\n";
	  else
	  {
	    echo "        <ul>\n";
	    foreach ($this->_authorsOtherArticles as $other_article)
		  echo "          <li><a href=\"" . WEB_PATH . "/article/" . $other_article["HANDLE"] . "/\">" . $other_article["TITLE"] . "</a></li>\n";
		echo "        </ul>\n";
	  }*/
	  
	  require_once (DOCUMENT_ROOT . "/components/page_components/sidebar_author_info.php");
	  if ($this->_typeIdentity > 1)
	  {
	    echo "        <div class=\"separator\"></div>\n";
		require_once (DOCUMENT_ROOT . "/components/page_components/sidebar_article_type.php");
	  }
	  if ($this->_relatedGroup != null)
	  {
	    echo "        <div class=\"separator\"></div>\n";
	    require_once (DOCUMENT_ROOT . "/components/page_components/sidebar_related_group.php");
	  }
	  
	  if ($this->_topic != null)
	  {
	    echo "        <div class=\"sectionHeader\">More content on <a href=\"" . WEB_PATH . "/topic/" . $this->_topic["HANDLE"] . 
			"/\">" . $this->_topic["TITLE"] . "</a>:</div>\n";
	    if (sizeof($this->_topic["CONTENTS"]) == 0)
             echo "        <div class=\"noResults\">No other content on this topic</div>\n";
	    else
	    {
		echo "        <ul>\n";
		foreach ($this->_topic["CONTENTS"] as $topic_content)
		  echo "          <li><a href=\"" . WEB_PATH . "/article/" . $topic_content["HANDLE"] . "/\">" . $topic_content["TITLE"] .
			"</a></li>\n";
		echo "        </ul>\n";
	    }
	  }
	  //if ($this->_groupInformation != null)
	  //  outputGroupInformation($this->_groupInformation);
	 /* if ($this->_typeIdentity > 1)
	  {
	    echo "        <div class=\"sectionHeader\">More articles in <a href=\"" . WEB_PATH . "/news/" . $this->_typeHandle . 
			"/\">" . $this->_typeTitle . "</a>:</div>\n";
		if (sizeof($this->_otherTypeArticles) == 0)
		  echo "        <div class=\"noResults\">No other articles have been posted in this category</div>\n";
		else
		{
		  echo "        <ul>\n";
		  foreach ($this->_otherTypeArticles as $other_type_articles)
		    echo "          <li><a href=\"" . WEB_PATH . "/article/" . $other_type_articles["HANDLE"] . "/\">" . $other_type_articles["TITLE"] .
			"</a></li>\n";
		  echo "        </ul>\n";
		}
	  }*/
	  echo "      </div>\n";
	  echo "      <div class=\"articleContainer\">\n";
	  echo "        <div class=\"title\">" . (!$this->_published ? "<span class=\"unpublishedMaterialNote\">[Unpublished]</span> " : "") . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\">Posted " . date(LONG_DATETIME_FORMAT, $this->_postTime) . "</div>\n";
	  echo "        <div class=\"article\">\n";
	  if ($this->_imageFile != null)
	  {
	    echo "          <div class=\"image " . $this->_imageAlignment . "\">\n";
	    echo "            <center><img src=\"" . WEB_PATH . "/images/articles/" . $this->_imageFile . "\" /></center>\n";
	    echo "            " . $this->_imageCaption . ($this->_photoCredit != null ? " <span class=\"credit\">Photo courtesy of " . $this->_photoCredit . ".</span>" : "") . "\n";
	    echo "          </div>\n";
	  }
	  echo $this->_contents;
	  echo "          <div style=\"clear:both;\"></div>\n";
	  echo "        </div>\n";
	  echo "        <div style=\"clear:both;\"></div>\n";
	  if (isset($_SESSION[MANAGE_SESSION]) && ((substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_BEATS, 1) == "1" &&
		$_SESSION[MANAGE_SESSION]["IDENTITY"] === $this->_staffIdentity) || substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], PERM_BEATS, 1) == "2"))//ADMIN_VIEW)
	  {
	    echo "<div class=\"managementBox\">\n";
		echo "  <div class=\"title\">Manage Beat</div>\n";
		echo "  <div class=\"actionsContainer\">\n";
		echo "    <div class=\"label\">Actions</div>\n";
		echo "    <a href=\"" . MANAGE_WEB_PATH . "/beat/manage/" . $this->_identity . "/\" target=\"_blank\" class=\"fauxLink\">Edit beat</a><br />\n";
		echo "    <span class=\"fauxLink\" onClick=\"executeAJAX('" . MANAGE_WEB_PATH . "/components/toggle-publish.php?type=beat&identity=" . 
			$this->_identity . "', function results(value)
			{
			if (value == 'success')
			{
				window.location = '" . WEB_PATH . "/article/" . $this->_handle . "/';
				return;
			}
			alert(value);
			});\">" . ($this->_published ? "Unpublish" : "Publish") . "</span><br />\n";
		echo "    <span class=\"fauxLink\" onClick=\"OpenManageWindow('feature-content', 'beat-" . $this->_identity . "');\">" . ($this->_isFeature ?
			"Remove as" : "Make") . " featured story</span><br />\n";
		echo "    <span class=\"fauxLink\" onClick=\"OpenManageWindow('delete-content','beat-' + " . $this->_identity . ");\">Delete beat</span><br />\n";
		echo "  </div>\n";
		echo "  <div class=\"label\">Information</div>\n";
		echo "  <div class=\"item\"><span>Beat identity:</span> " . $this->_identity . "</div>\n";
		echo "  <div class=\"item\"><span>Author's staff identity:</span> " . $this->_staffIdentity . "</div>\n";
		echo "  <div style=\"clear:both;\"></div>\n";
		echo "</div>\n";
	  }
	}
}
?>