<?php
class GoAvesStaffPortfolioOld {
	private $_identity;
	private $_firstName;
	private $_lastName;
	private $_shortBio;
	private $_positions;
	private $_imageFile;
	private $_active;
	private $_works = array();
	private $_staffListingPage;
	private $_adminLoadedValues = null;
	function loadPortfolio($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "" || strpos($path[1], "-") === false)
	    return false;
	  $name = explode("-", $path[1], 2);
	  $info = $database->querySingle("SELECT identity, first_name, last_name, short_bio, positions, image_file, active FROM staff WHERE first_name LIKE '" .
		$database->escapeString($name[0]) . "' AND last_name LIKE '" . $database->escapeString($name[1]) . "' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_identity = $info["identity"];
	  $this->_firstName = $info["first_name"];
	  $this->_lastName = $info["last_name"];
	  $this->_shortBio = format_content($info["short_bio"]);
	  $this->_positions = $info["positions"];
	  $this->_imageFile = $info["image_file"];
	  $this->_active = ($info["active"] == "TRUE");

         $staff_before = $database->querySingle("SELECT count(*) FROM staff WHERE ((last_name < '" . $info["last_name"] . "') OR (last_name = '" . $info["last_name"] . "' AND " .
		"first_name < '" . $info["first_name"] . "')) AND active='TRUE' AND display_on_staff_listing='TRUE'");
         $this->_staffListingPage = ($staff_before == 0 ? 1 : floor($staff_before / STAFF_PER_LISTING_PAGE) + 1);
	  
	  $get_works = $database->query("SELECT handle, title, post_time AS \"datestamp\", \"article\" AS \"type\", null AS \"image\" FROM beats WHERE staff_identity='" .
		$info["identity"] . "' AND published='TRUE' UNION SELECT handle, title, date_posted AS \"datestamp\", \"comic\" AS \"type\", image_file AS \"image\"" .
		"FROM cartoons WHERE artist='" . $info["identity"] . "' UNION SELECT handle, title, post_date AS \"datestamp\", \"photo\" AS \"type\", " .
		"image_file AS \"image\" FROM photos WHERE photographer_identity='" . $info["identity"] . "' ORDER BY type ASC, datestamp DESC");
	  while ($work = $get_works->fetchArray())
	    $this->_works[] = array("TYPE" => $work["type"], "HANDLE" => $work["handle"], "TITLE" => format_content($work["title"]),
			"IMAGE" => ($work["image"] != "null" ? $work["image"] : null));

	  if (ADMIN_VIEW)
	  {
	    $loaded_values = $database->querySingle("SELECT permissions, valid_article_types, password, active, display_on_staff_listing FROM " .
		"staff WHERE identity='" . $this->_identity . "' LIMIT 1", true);
	    $this->_adminLoadedValues = array("PERMISSIONS" => $loaded_values["permissions"], "VALID_ARTICLE_TYPES" => $loaded_values["valid_article_types"],
		"DEFAULT_PASSWORD" => ($loaded_values["password"] == md5("default")), "ACTIVE" => ($loaded_values["active"] == "TRUE"),
		"DISPLAY_ON_STAFF_LISTING" => ($loaded_values["display_on_staff_listing"] == "TRUE"));
	  }
	  return true;
	}
	function checkOrRedirect($path, $database)
	{	
	  if (!$this->loadPortfolio($path, $database))
	    return new GoAvesStaff();
	  return true;
	}
	function getMainTab() { return "staff"; }
	function getPageHandle() { return "staff-portfolio"; }
	function getPageSubhandle() { return mb_strtolower($this->_firstName . "-" . $this->_lastName); }
	function getBreadTrail() { return array("home" => "GoAves.com", "staff" => "Staff Listing", "[this]" => "[Portfolio] " . $this->_firstName .
		" " . $this->_lastName); }
	function getPageTitle() { return $this->_firstName . " " . $this->_lastName; }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/staff/page-" . $this->_staffListingPage . "/\">&laquo; Return to staff listing</a></div>\n";
	  echo "      <img src=\"" . WEB_PATH . "/images/staff/" . $this->_imageFile . "\" class=\"staffPortfolioImageOld\" />\n";
	  echo "      <div class=\"portfolioContainerOld\">\n";
	  echo "        <div class=\"name\">" . $this->_firstName . " " . $this->_lastName . "</div>\n";
	  echo "        <div class=\"positions\">" . $this->_positions . "</div>\n";
	  echo "        <div class=\"blurb\">" . $this->_shortBio . "</div>\n";
	  
	  if (sizeof($this->_works) > 0)
	  {
	    echo "      <div class=\"works\">\n";
		$work_type = null;
		foreach ($this->_works as $work)
		{
		  if ($work_type != $work["TYPE"])
		  {
		    $work_type = $work["TYPE"];
			echo "        <div class=\"header\"" . ($work_type != "article" ? " style=\"margin-bottom:5px;\"" : "") . ">";
			switch ($work_type)
			{
			   case "article": echo "Articles"; break;
			   case "comic": echo "Comics"; break;
			   case "photo": echo "Photos"; break;
			}
			echo "</div>\n";
		  }
		  if ($work["TYPE"] == "article")
		    echo "        <a href=\"" . WEB_PATH . "/article/" . $work["HANDLE"] . "/\" class=\"entry\">" . $work["TITLE"] . "</a><br/>\n";
		  else if ($work["TYPE"] == "comic" || $work["type"] == "photo")
		    echo "        <a href=\"" . WEB_PATH . "/" . $work["TYPE"] . "/" . $work["HANDLE"] . "/\"><img src=\"" . WEB_PATH . "/images/" .
				($work["TYPE"] == "comic" ? "cartoons" : "photos") . "/" . $work["IMAGE"] . "\" class=\"entry\" border=\"0\"></a>\n";
		}
	    echo "      </div>\n";
	  }
	  echo "      <div style=\"clear:right;\"></div>\n";
	  if (ADMIN_VIEW)
	  {
	    echo "<div class=\"adminViewBox\">\n";
	    echo "<b>Staff Identity:</b> " . $this->_identity . "<br />\n";
	    echo "<b>Permissions:</b> " . $this->_adminLoadedValues["PERMISSIONS"] . "<br />\n";
	    echo "<b>Valid Article Types:</b> " . $this->_adminLoadedValues["VALID_ARTICLE_TYPES"] . "<br />\n";
	    echo "<b>Using default password:</b> " . ($this->_adminLoadedValues["DEFAULT_PASSWORD"] ? "<font color=\"red\">YES</font>" : "No") . "<br />\n";
	    echo "<b>Can log in:</b> " . ($this->_adminLoadedValues["ACTIVE"] ? "Yes" : "No") . "<br />\n";
	    echo "<b>Display on staff listing:</b> " . ($this->_adminLoadedValues["DISPLAY_ON_STAFF_LISTING"] ? "Yes": "No") . "<br />\n";
	    echo "<a href=\"" . WEB_PATH . "/manage/admin-staff-info.php?staff_id=" . $this->_identity . "\" target=\"_blank\">Edit Information</a><br />\n";
	    echo "<a href=\"" . WEB_PATH . "/manage/admin-staff-reset-password.php?staff_id=" . $this->_identity . "\" target=\"_blank\">Reset Password to Default Password</a><br />\n";
	    echo "<a href=\"" . WEB_PATH . "/manage/admin-staff-permissions.php?staff_id=" . $this->_identity . "\" target=\"_blank\">Edit Permissions</a>\n";
	    echo "</div>\n";
	  }
	}
}
?>