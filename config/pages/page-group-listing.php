<?php
class GoAvesGroupListing {
	private $_groupTypes = array();
	private $_currentGroupTypeIdentity;
	function loadGroupListing($path, $database)
	{
	  if (isset($path[1]))
	  {
	    if ($database->querySingle("SELECT count(*) FROM group_types WHERE handle LIKE '" . $database->escapeString($path[1]) . "'") > 0)
		  $this->_currentGroupTypeIdentity = $database->querySingle("SELECT type_identity FROM group_types WHERE handle LIKE '" . $database->escapeString($path[1]) . "' LIMIT 1");
	  } else
	    $this->_currentGroupTypeIdentity = null;
	  
	  $get_group_types = $database->query("SELECT type_identity, plural_name FROM group_types");
	  while ($group_type = $get_group_types->fetchArray())
	    $this->_groupTypes[] = array("IDENTITY" => $group_type["type_identity"], "NAME" => $group_type["plural_name"]);
	}
	function checkOrRedirect($path,$database)
	{
	  $this->loadGroupListing($path, $database);
	  return true;
	}
	function getMainTab() { return "group-listing"; }
	function getPageHandle() { return "group-listing"; }
	function getPageSubhandle() { return null; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "Group Listing"); }
	function getPageTitle() { return "Group Listing"; }
	function getPageContents()
	{ 
	  echo "      <div class=\"groupListContainer\">\n";
	  echo "        <div class=\"pageHeader\">Group Listing</div>\n";
	  echo "        <div class=\"sidebar\">\n";
	  echo "          <div class=\"header\">Search Groups</div>\n";
	  echo "          <input type=\"text\" id=\"group-listing-search\" class=\"search empty\" value=\"Search for group...\" " .
	  		"onKeyUp=\"searchGroupListing(this.value);\" onFocus=\"focusGroupListingSearch();\" onBlur=\"blurGroupListingSearch();\" " . "/>\n<br />\n";
		//"onKeyPress=\"processReturnKeySearchStaff();\" />\n<br />\n";
	  echo "          <div class=\"header\">Filter Groups</div>\n";
	  echo "          <div id=\"group-listing-filters\">\n";
	  echo "            <div class=\"item" . ($this->_currentGroupTypeIdentity == null ? " selectedItem" : "") . "\" id=\"group-listing-filter-active\" onClick=\"filterGroupListing('active');\">&rsaquo; Active Groups</div>\n";
	  echo "            <div class=\"item\" id=\"group-listing-filter-inactive\" onClick=\"filterGroupListing('inactive');\">&rsaquo; Inactive Groups</div>\n";
	  foreach ($this->_groupTypes as $type)
	    echo "            <div class=\"item" .($this->_currentGroupTypeIdentity == $type["IDENTITY"] ? " selectedItem" : "") . "\" id=\"group-listing-filter-type-" .
			$type["IDENTITY"] . "\" onClick=\"filterGroupListing('type-" . $type["IDENTITY"] . "');\">&rsaquo; " .
	        $type["NAME"] . "</div>\n";
	  echo "          </div>\n";
	  echo "        </div>\n";
	  echo "        <div class=\"viewport\" id=\"group-listing-viewport\">\n";
	  require_once (DOCUMENT_ROOT . "/components/page_components/group-listing-display.php");
	  echo "        </div>\n";
	  echo "        <div style=\"clear:both;\"></div>\n";
	  echo "      </div>\n";
	  return;
	  
	  echo "      <center>\n";
	  foreach ($this->_items as $item)
	  {
		echo "        <div onMouseOver=\"this.className='listItemHover';\" onMouseOut=\"this.className='listItem';\" " .
			"onClick=\"window.location='" . WEB_PATH . "/group/" . $item["HANDLE"] . "/';\" class=\"listItem\">\n";
		echo "          <div>" . $item["TITLE"] . "</div>\n";
		echo "          <img src=\"" . WEB_PATH . "/images/groups/" . $item["ICON"] . "\" />\n";
		echo "        </div>\n";
	  }
	  echo "      </center>\n";
	}
}
?>