<?php
class GoAvesTopicPage {
	private $_handle;
	private $_title;
	private $_description;
	private $_articles = array();
	function loadTopic($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT topic_identity, handle, title, description FROM topics WHERE handle LIKE '" .
		$database->escapeString($path[1]) . "' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_handle = $info["handle"];
	  $this->_title = $info["title"];
	  $this->_description = $info["description"];
	  $get_articles = $database->query("SELECT handle, title, post_time FROM beats WHERE topic='" . $info["topic_identity"] .
		"' AND published='TRUE' ORDER BY post_time DESC");
	  while ($article = $get_articles->fetchArray())
	    $this->_articles[] = array("HANDLE" => $article["handle"], "TITLE" => $article["title"], "POST_TIME" =>
		strtotime($article["post_time"]));
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadTopic($path, $database))
	    return new GoAvesTopicHub();
	  return true;
	}
	function getMainTab() { return "news"; }
	function getPageHandle() { return "topic"; }
	function getPageSubhandle() { return $this->_handle; }
	function getPageTitle() { return $this->_title; }
	function getBreadTrail() { return array("home" => "GoAves.com", "topic-hub" => "Topic Hub", "[this]" => $this->_title); }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/topic-hub/\">&laquo; Return to topic hub</a></div>\n";
	  
	  echo "      <div class=\"articleSide topicSide\">\n";
	  echo "        " . $this->_description . "\n";
	  echo "      </div>\n";
	  
	  echo "      <div class=\"articleContainer topicContainer\">\n";
	  echo "        <div class=\"title\">" . $this->_title . "</div>\n";
	  echo "          <ul>\n";
	  foreach ($this->_articles as $article)
	  {
	    echo "          <li><b><a href=\"" . WEB_PATH . "/article/" . $article["HANDLE"] . "/\" class=\"groupRelated\">" .
			format_content($article["TITLE"]) . "</a></b> <small>(" . date(LONG_DATE_FORMAT, $article["POST_TIME"]) .
			")</small></li>\n";
	  }
	  echo "        </ul>\n";
	  if (sizeof($this->_articles) == 0)
	    echo "        <center>There are no articles concerning this topic yet</center>\n";
	  echo "      </div>\n";
	}
}
?>