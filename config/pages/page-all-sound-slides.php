<?php
class GoAvesSoundSlidesListing {
	private $_soundSlides = array();
	function load($path, $database)
	{
		$get_slideshows = $database->query("SELECT handle, title, published, post_date, thumbnail_image, date_last_updated FROM sound_slides " . (!ADMIN_VIEW ? 
			"WHERE published='TRUE' " : "") . "ORDER BY date_last_updated DESC");
		while ($slideshow = $get_slideshows->fetchArray())
		  $this->_soundSlides[] = array("HANDLE" => $slideshow["handle"], "TITLE" => $slideshow["title"], "PUBLISHED" => ($slideshow["published"] == "TRUE"),
				"IMAGE" => $slideshow["thumbnail_image"], "DATE_LAST_UPDATED" => strtotime($slideshow["date_last_updated"]));
	}
	function checkOrRedirect($path, $database)
	{
	  $this->load($path, $database);
	  return true;
	}
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "all-sound-slides"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Soundslide Listing"; }
	function getBreadTrail() { return array("home" => "GoAves.com", "multimedia" => "All Multimedia", "[this]" => "Soundslide Listing"); }
	function getPageContents()
	{
	  echo "      <center>\n";
	  echo "<pre>";
	  print_r($this->_soundSlides);
	  echo "</pre>";
	  echo "      </center>\n";
	}
}
?>