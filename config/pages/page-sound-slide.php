<?php
class GoAvesSoundSlides {
	private $_identity;
	private $_handle;
	private $_title;
	private $_dateLastUpdated;
	private $_datePosted;
	private $_soundFile;
	private $_slides = array();
	private $_groupInformation;
	private $_published;
	private $_staffIdentity;
	private $_authorTitle = "Narrator";
	private $_staffInfo = array();
	private $_relatedGroup;
	function loadSoundSlides($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT slideshow_identity, handle, title, post_date, date_last_updated, related_group, sound_file, published, staff.first_name, staff.last_name, " .
		"staff.icon_file AS \"staff_icon\", staff.positions, sound_slides.staff_identity, staff.year_graduating FROM sound_slides JOIN staff ON sound_slides.staff_identity = staff.identity " .
		"WHERE handle LIKE '" . $database->escapeString($path[1]) . "'" . (ADMIN_VIEW ? "" : " AND published='TRUE'") . " LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_identity = $info["slideshow_identity"];
	  $this->_published = ($info["published"] == "TRUE");
	  $this->_handle = $info["handle"];
	  $this->_title = $info["title"];
	  $this->_datePosted = strtotime($info["post_date"]);
	  $this->_dateLastUpdated = strtotime($info["date_last_updated"]);
	  $this->_soundFile = $info["sound_file"];
	  $this->_staffIdentity = $info["staff_identity"];
	  $this->_relatedGroup = ($info["related_group"] != "0" ? $info["related_group"] : null);
	  
	  $get_slides = $database->query("SELECT image_file, start_time, caption FROM sound_slides_slides WHERE slideshow_identity='" .
		$info["slideshow_identity"] . "' ORDER BY start_time ASC");
	  while ($slide = $get_slides->fetchArray())
	    $this->_slides[] = array("IMAGE_FILE" => $slide["image_file"], "START" => $slide["start_time"], "CAPTION" => $slide["caption"]);
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadSoundSlides($path, $database))
	    return new GoAvesMultimedia();
	  return true;
	}
	
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "sound-slides"; }
	function getPageSubhandle() { return $this->_handle; }
	function getPageTitle() { return $this->_title; }
	function getBreadTrail() { return array("home" => "GoAves.com", "multimedia" => "All Multimedia", "[this]" => "[Sound Slides] " . $this->_title); }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/multimedia/sound-slides/\">&laquo; Return to soundslides listing</a></div>\n";
	  
	  echo "      <div class=\"sidebarSpecContent\">\n";
	  require_once (DOCUMENT_ROOT . "/components/page_components/sidebar_author_info.php");
	  if ($this->_relatedGroup != null)
	  {
	    echo "        <div class=\"separator\"></div>\n";
	    require_once (DOCUMENT_ROOT . "/components/page_components/sidebar_related_group.php");
	  }
	  echo "      </div>\n";
	  
	  echo "      <div class=\"specContentContainer\">\n";
	  echo "        <div class=\"title\">" . (!$this->_published ? "<span class=\"unpublishedMaterialNote\">[Unpublished]</span> " : "") . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\"><span class=\"content-type\">Soundslides</span> &ndash; " . (date("Y-m-d", $this->_dateLastUpdated) != date("Y-m-d", $this->_datePosted) ?
	  	"<b>Last updated on</b> " . date(LONG_DATE_FORMAT, $this->_dateLastUpdated) . " &ndash; " : "") . "<b>Posted on</b> " . date(LONG_DATE_FORMAT, $this->_datePosted) . "</div>\n";

	  echo "	    <div class=\"soundSlideContainer\">\n";
	  echo "	      <center><div class=\"slideImage\"><img id=\"slide-image\" class=\"blurred\" src=\"" . WEB_PATH . "/images/sound_slides/" . $this->_slides[0]["IMAGE_FILE"] .
	  	"\" /></div></center>\n";
	  echo "              <div id=\"slide-caption\" class=\"slideCaption\">" . $this->_slides[0]["CAPTION"] . "</div>\n";
	  echo "              <div id=\"audio-track\" href=\"" . WEB_PATH . "/components/soundslides_audio/" . $this->_soundFile . "\" class=\"slideAudio\"></div>\n";
	  echo "	    </div>\n";
	  echo "        </div>\n";

	  echo "          <script src=\"" . WEB_PATH . "/layout/flowplayer/flowplayer-3.2.4.min.js\"></script>\n";
	  echo "          <script language=\"JavaScript\">\n";
	  echo "            var slides = new Array();\n";
	  echo "            var slides_start = new Array();\n";
	  for ($output_slide = 0; $output_slide < sizeof($this->_slides); $output_slide++)
	  {
	    echo "            slides[" . $this->_slides[$output_slide]["START"] . "] = ['" . WEB_PATH . "/images/sound_slides/" . $this->_slides[$output_slide]["IMAGE_FILE"] . "','" . addslashes($this->_slides[$output_slide]["CAPTION"]) .
		"']\n";
	    //echo "            slides_start[" . $output_slide . "] = " . $this->_slides[$output_slide]["START"] . ";\n";
	  }
	  echo "            flowplayer(\"audio-track\",\n";
	  echo "		  \"" . WEB_PATH . "/layout/flowplayer/flowplayer-3.2.5.swf\",\n";
	  echo "		  {\n";
	  echo "			plugins: {\n";
	  echo "                      controls: {\n";
	  echo "			    fullscreen: false,\n";
	  echo "			    height: 30,\n";
	  echo "			    autoHide: false,\n";
	  echo "                        backgroundGradient: 'none',\n";
	  echo "                        volume: false,\n";
	  echo "			    mute: false\n";
	  echo "			  }\n";
	  echo "			},\n";
	  echo "			clip: {\n";
	  echo "			  autoPlay: false,\n";
	  echo "			  onCuepoint: [[";
	  $output_first = false;
	  foreach ($this->_slides as $cues)
	    if ($cues["START"] > 0)
	    {
		if ($output_first === false)
		  $output_first = true;
		else
		  echo ", ";
	      echo ($cues["START"]);
	    }
	  echo "], changeSoundSlidesByCues],\n";
	  echo "			  onSeek: seekSoundSlides\n";
	  echo "			},\n";
	  echo "			onFinish: function() {\n";
	  echo "			  changeSoundSlide(0);\n";
	  echo "                          document.getElementById('slide-image').className = \"blurred\";\n";
	  echo "			}\n";
	  echo "		  });\n";
	  echo "          </script>\n";
	  echo "          <div style=\"clear:both;\"></div>\n";
	  if (ADMIN_VIEW)
	  {
	    echo "<div class=\"adminViewBox\">\n";
	    echo "<b>Slideshow Identity:</b> " . $this->_identity . "<br />\n";
	    echo "<b>Number of slides:</b> " . sizeof($this->_slides) . "<br />\n";
	    echo "<span class=\"action\" onClick=\"executeAJAX('" . WEB_PATH . "/manage/admin-master-published-remote-toggle.php?type=sound_slide&identity=" . $this->_identity . "'," .
		"function evaluate(value) { if (value == 'published') alert('Soundslides published'); else if (value == 'unpublished') alert('Soundslides unpublished'); else alert(value); " .
		"window.location = '" . WEB_PATH . "/sound-slides/" . $this->_handle . "/'; });\">" .
		($this->_published ? "Unpublish" : "Publish") . "</span>\n";
	    echo "</div>\n";
	  }
	}
}
?>