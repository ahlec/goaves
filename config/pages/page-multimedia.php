<?php
class GoAvesMultimedia {
	private $_specificMediaType = null;
	private $_feedItems = array();
	private $_comics = array();
	private $_videos = array();
	private $_photoGalleries = array();
	private $_podcasts = array();
	private $_soundSlides = array();
	function loadMultimedia($path, $database)
	{
	  if (isset($path[1]))
	  {
	    switch (strtolower($path[1]))
		{
		  case "sound-slides": $germane_name = "Soundslides"; $database_handle = "sound_slides"; $page_handle = "sound-slides"; break;
		  case "comics": $germane_name = "Comics"; $database_handle = "cartoon"; $page_handle = "comics"; break;
		  case "photo-galleries": $germane_name = "Photo Galleries"; $database_handle = "photo_gallery"; $page_handle = "photo-galleries"; break;
		  case "videos": $germane_name = "Videos"; $database_handle = "video"; $page_handle = "videos"; break;
		  case "podcasts": $germane_name = "Podcasts"; $database_handle = "podcast"; $page_handle = "podcasts"; break;
		  default: $page_handle = null; break;
		}
		if (isset($germane_name))
		  $this->_specificMediaType = array("GERMANE" => $germane_name, "PAGE_HANDLE" => $page_handle);
	  }
	
	  $get_feed_items = $database->query("SELECT all_published_multimedia.handle, all_published_multimedia.title, all_published_multimedia.type, " .
		"multimedia_feed.post_time, multimedia_feed.post_type FROM multimedia_feed JOIN all_published_multimedia ON " .
		"all_published_multimedia.type = multimedia_feed.media_type AND all_published_multimedia.identity = multimedia_feed.media_identity " .
		(isset($database_handle) ? "WHERE media_type='" . $database_handle . "' " : "") . "ORDER BY multimedia_feed.post_time DESC LIMIT " . FEED_ITEMS_DISPLAY);
	  while ($feed_item = $get_feed_items->fetchArray())
	  {
	    $short_feed_title = get_smart_blurb(format_content($feed_item["title"]), 2);
	    $this->_feedItems[] = array("HANDLE" => $feed_item["handle"], "TITLE" => $short_feed_title . ($short_feed_title != format_content($feed_item["title"]) ? "..." : ""),
			"TYPE" => $feed_item["type"], "POST_TIME" => strtotime($feed_item["post_time"]), "POST_TEXT" => $feed_item["post_type"]);
	  }

	  if ($page_handle == null || $page_handle == "comics")
	  {
	    $get_comics = $database->query("SELECT image_file, handle, title, published FROM cartoons" . (ADMIN_VIEW ? "" : " WHERE published='TRUE'") .
			" ORDER BY date_posted DESC" . ($page_handle == null ? " LIMIT 10" : ""));
	    while ($comic = $get_comics->fetchArray())
	      $this->_comics[] = array("IMAGE" => $comic["image_file"], "HANDLE" => $comic["handle"], "TITLE" => $comic["title"], "PUBLISHED" => ($comic["published"] == "TRUE"));
	  }
	  
	  if ($page_handle == null || $page_handle == "videos")
	  {
	    $get_videos = $database->query("SELECT handle, title, captions, image_thumbnail, published FROM videos" . (ADMIN_VIEW ? "" : " WHERE published='TRUE'") . " ORDER BY date_published DESC LIMIT 4");
	    while ($video = $get_videos->fetchArray())
	      $this->_videos[] = array("HANDLE" => $video["handle"], "TITLE" => $video["title"], "CAPTION" => $video["captions"], "THUMBNAIL" => $video["image_thumbnail"],
		  "PUBLISHED" => ($video["published"] == "TRUE"));
	  }

	  if ($page_handle == null || $page_handle == "photo-galleries")
	  {
	    $get_photo_galleries = $database->query("SELECT photo_galleries.gallery_identity, photo_galleries.handle, photo_galleries.title, gallery_image, " .
		  "photo_galleries.date_last_updated, photo_galleries.published FROM photo_galleries" .
		  (ADMIN_VIEW ? "" : " WHERE photo_galleries.published=" .
		  "'TRUE'") . " ORDER BY date_last_updated DESC" . ($page_handle == null ? " LIMIT 7" : ""));
	    while ($photo_gallery = $get_photo_galleries->fetchArray())
	      $this->_photoGalleries[] = array("HANDLE" => $photo_gallery["handle"], "TITLE" => $photo_gallery["title"], "IMAGE" => $photo_gallery["gallery_image"],
		  "DATE_LAST_UPDATED" => strtotime($photo_gallery["date_last_updated"]), "NUMBER_PHOTOS" => $database->querySingle("SELECT count(*) FROM photos WHERE gallery_identity='" .
		  $photo_gallery["gallery_identity"] . "'" . (ADMIN_VIEW ? " AND published='TRUE'" : "")), "PUBLISHED" => ($photo_gallery["published"] == "TRUE"));
	  }
	
	  if ($page_handle == null || $page_handle == "podcasts")
	  {
	    $get_podcasts = $database->query("SELECT handle, title, icon_file, description, published FROM podcasts" . (ADMIN_VIEW ? "" : " WHERE published='TRUE'") . " ORDER BY date_posted DESC LIMIT 4");
	    while ($podcast = $get_podcasts->fetchArray())
	      $this->_podcasts[] = array("HANDLE" => $podcast["handle"], "TITLE" => $podcast["title"], "DESCRIPTION" => $podcast["description"],
			"PUBLISHED" => ($podcast["published"] == "TRUE"), "ICON" => $podcast["icon_file"]);
	  }
		
	  if ($page_handle == null || $page_handle == "sound-slides")
	  {
	    $get_sound_slides = $database->query("SELECT slideshow_identity, handle, title, published, thumbnail_image, date_last_updated FROM sound_slides" .
			(ADMIN_VIEW ? "" : " WHERE published='TRUE'") . " ORDER BY date_last_updated DESC" . ($page_handle == null ? " LIMIT 4" : ""));
	    while ($slideshow = $get_sound_slides->fetchArray())
	      $this->_soundSlides[] = array("HANDLE" => $slideshow["handle"], "TITLE" => $slideshow["title"], "THUMBNAIL_IMAGE" => $slideshow["thumbnail_image"], "FIRST_SLIDE_CAPTION" =>
		    $database->querySingle("SELECT caption FROM sound_slides_slides WHERE slideshow_identity='" . $slideshow["slideshow_identity"] . "' ORDER BY slide_identity ASC LIMIT 1"),
		    "PUBLISHED" => ($slideshow["published"] == "TRUE"), "IDENTITY" => $slideshow["slideshow_identity"], "DATE_LAST_UPDATED" => strtotime($slideshow["date_last_updated"]));
	  }
	  
	}
	function checkOrRedirect($path,$database)
	{
	  $this->loadMultimedia($path, $database);
	  return true;
	}
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "multimedia"; }
	function getPageSubhandle() { return ($this->_specificMediaType != null ? $this->_specificMediaType["PAGE_HANDLE"] : null); }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "All Multimedia"); }
	function getPageTitle() { return "Multimedia"; }
	function getPageContents()
	{
	  if ($this->_specificMediaType != null)
	    echo "<div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/multimedia/\">&laquo; Return to all multimedia</a></div>\n";

	  echo "<div class=\"feedPanel\">\n";
	  echo "<div class=\"title\">" . ($this->_specificMediaType != null ? $this->_specificMediaType["GERMANE"] : "Multimedia") . " Feed</div>\n";
	  foreach ($this->_feedItems as $feed_item)
	  {
	    echo "<div class=\"item\"><a href=\"" . WEB_PATH . "/";
	    switch ($feed_item["TYPE"])
	    {
	      case "cartoon": echo "comic"; break;
	      case "video": echo "video"; break;
	      case "photo_gallery": echo "photo-gallery"; break;
	      case "podcast": echo "podcast"; break;
	      case "sound_slides": echo "sound-slides"; break;
	    }
	    echo "/" . $feed_item["HANDLE"] . "/\"><img src=\"" . WEB_PATH . "/images/icons/";
	    switch ($feed_item["TYPE"])
	    {
	      case "cartoon": echo "icon-comic.png"; break;
	      case "video": echo "icon-video.png"; break;
	      case "photo_gallery": echo "icon-photo-gallery.png"; break;
	      case "podcast": echo "icon-audio.png"; break;
	      case "sound_slides": echo "icon-sound-slides.png"; break;
	    }
	    echo "\" border=\"0\">" . $feed_item["TITLE"] . "</a> " . $feed_item["POST_TEXT"] . ". <span class=\"date\">(" . date(SHORT_DATE_FORMAT, $feed_item["POST_TIME"]) . ")</span></div>\n";
	  }
	  echo "</div>\n";
	  echo "<div class=\"multimediaContainer\">\n";
	  
	  // Comics
	  if (sizeof($this->_comics) > 0)
	  {
	    echo "  <div class=\"" . ($this->_specificMediaType != null ? "fullFrame" : "frame") . "\">\n";
	    echo "    <div class=\"header\"><span>" . ($this->_specificMediaType != null ? "All " : "<a href=\"" . WEB_PATH . "/multimedia/comics/\">") .
			"Comics" . ($this->_specificMediaType == null ? "</a>" : "") . "</span></div>\n";
	    echo "    <center>\n";
	    echo "    <a href=\"" . WEB_PATH . "/comic/" . $this->_comics[0]["HANDLE"] . "/\"><img src=\"" . WEB_PATH . "/images/cartoons/" .
		$this->_comics[0]["IMAGE"] . "\" class=\"comic" . (!$this->_comics[0]["PUBLISHED"] ? " unpublishedMaterialNote" : "") . "\" border=\"0\"></a>\n";
	    if (sizeof($this->_comics) > 1)
	      echo "    <div class=\"divider\"></div>\n";
	    for ($other_comics = 1; $other_comics < sizeof($this->_comics); $other_comics++)
	      echo "    <a href=\"" . WEB_PATH . "/comic/" . $this->_comics[$other_comics]["HANDLE"] . "/\"><img src=\"" . WEB_PATH . "/images/cartoons/" .
		$this->_comics[$other_comics]["IMAGE"] . "\" class=\"miniIcons" . (!$this->_comics[$other_comics]["PUBLISHED"] ? " unpublishedMaterialNote" : "") . "\" border=\"0\"></a>\n";
	    echo "    </center>\n";
		if ($this->_specificMediaType == null)
	      echo "    <div class=\"all\"><a href=\"" . WEB_PATH . "/multimedia/comics/\">&laquo; all comics</a></div>\n";
	    echo "  </div>\n";
	  }
	  // Videos
	  if (sizeof($this->_videos) > 0)
	  {
	    echo "  <div class=\"" . ($this->_specificMediaType != null ? "fullFrame" : "frame") . "\">\n";
	    echo "    <div class=\"header\"><span>" . ($this->_specificMediaType != null ? "All " : "<a href=\"" . WEB_PATH . "/multimedia/videos/\">") .
			"Videos" . ($this->_specificMediaType == null ? "</a>" : "") . "</span></div>\n";
	    echo "    <center>\n";
	    echo "    <a href=\"" . WEB_PATH . "/video/" . $this->_videos[0]["HANDLE"] . "/\"><img src=\"" . WEB_PATH . "/images/video_thumbnails/" .
		$this->_videos[0]["THUMBNAIL"] . "\" class=\"video" . (!$this->_videos[0]["PUBLISHED"] ? " unpublishedMaterialNote" : "") . "\" border=\"0\"></a>\n";
	    echo "    <div class=\"topContainer\">\n";
	    echo "      <div class=\"itemTitle\">" . (!$this->_videos[0]["PUBLISHED"] ? "<span class=\"unpublishedMaterialNote\">[Unpublished]</span>" : "") .
		"<a href=\"" . WEB_PATH . "/video/" . $this->_videos[0]["HANDLE"] . "/\">" . $this->_videos[0]["TITLE"] . "</a></div>\n";
	    echo "      <div class=\"itemCaption\">" . get_smart_blurb($this->_videos[0]["CAPTION"], 70) . "</div>\n";
	    echo "    </div>\n";
	    if (sizeof($this->_videos) > 1)
	      echo "    <div class=\"divider\"></div>\n";
	    for ($other_videos = 1; $other_videos < sizeof($this->_videos); $other_videos++)
	      echo "    <a href=\"" . WEB_PATH . "/video/" . $this->_videos[$other_videos]["HANDLE"] . "/\"><img src=\"" . WEB_PATH . "/images/video_thumbnails/" .
		$this->_videos[$other_videos]["THUMBNAIL"] . "\" class=\"miniIcons" . (!$this->_videos[$other_videos]["PUBLISHED"] ? " unpublishedMaterialNote" : "") . "\" border=\"0\"></a>\n";
	    echo "    </center>\n";
		if ($this->_specificMediaType == null)
	      echo "    <div class=\"all\"><a href=\"" . WEB_PATH . "/multimedia/videos/\">&laquo; all videos</a></div>\n";
	    echo "  </div>\n";
	  }
	  
	  // Photo Galleries
	  if (sizeof($this->_photoGalleries) > 0)
	  {
	    echo "  <div class=\"" . ($this->_specificMediaType == null ? "frame" : "fullFrame") . "\">\n";
	    echo "    <div class=\"header\"><span>" . ($this->_specificMediaType == null ? "<a href=\"" . WEB_PATH . "/multimedia/photo-galleries/\">" : "All ") . 
			"Photo Galleries" . ($this->_specificMediaType == null ? "</a>" : "") . "</span></div>\n";
	    echo "    <center>\n";
	    echo "    <a href=\"" . WEB_PATH . "/photo-gallery/" . $this->_photoGalleries[0]["HANDLE"] . "/\"><img src=\"" . WEB_PATH . "/images/photos/" .
		$this->_photoGalleries[0]["IMAGE"] . "\" class=\"video" . (!$this->_photoGalleries[0]["PUBLISHED"] ? " unpublishedMaterialNote" : "") . "\" border=\"0\"></a>\n";
	    echo "    <div class=\"topContainer\">\n";
	    echo "      <div class=\"itemTitle\">" . (!$this->_photoGalleries[0]["PUBLISHED"] ? "<span class=\"unpublishedMaterialNote\">[Unpublished]</span>" : "") .
		"<a href=\"" . WEB_PATH . "/photo-gallery/" . $this->_photoGalleries[0]["HANDLE"] . "/\">" . $this->_photoGalleries[0]["TITLE"] . "</a></div>\n";
	    echo "      <div class=\"itemCaption\">" . $this->_photoGalleries[0]["NUMBER_PHOTOS"] . " photo" . ($this->_photoGalleries[0]["NUMBER_PHOTOS"] !== 1 ? "s" : "") . "<br />\n";
	    echo "      Last Updated: " . date(DATE_FORMAT, $this->_photoGalleries[0]["DATE_LAST_UPDATED"]) . "</div>\n";
	    echo "    </div>\n";
	    if (sizeof($this->_photoGalleries) > 1)
	      echo "    <div class=\"divider\"></div>\n";
	    for ($other_galleries = 1; $other_galleries < sizeof($this->_photoGalleries); $other_galleries++)
	      echo "    <a href=\"" . WEB_PATH . "/photo-gallery/" . $this->_photoGalleries[$other_galleries]["HANDLE"] . "/\"><img src=\"" . WEB_PATH . "/images/photos/" .
		$this->_photoGalleries[$other_galleries]["IMAGE"] . "\" class=\"miniIcons" . (!$this->_photoGalleries[$other_galleries]["PUBLISHED"] ? " unpublishedMaterialNote" : "") . "\" border=\"0\"></a>\n";
	    echo "    </center>\n";
		if ($this->_specificMediaType == null)
	      echo "    <div class=\"all\"><a href=\"" . WEB_PATH . "/multimedia/photo-galleries/\">&laquo; all photo galleries</a></div>\n";
	    echo "  </div>\n";
	  }
	  
	  // Podcasts
	  if (sizeof($this->_podcasts) > 0)
	  {
	    echo "  <div class=\"" . ($this->_specificMediaType == null ? "frame" : "fullFrame") . "\">\n";
	    echo "    <div class=\"header\"><span>" . ($this->_specificMediaType == null ? "<a href=\"" . WEB_PATH . "/multimedia/podcasts/\">" : "All ") .
			"Podcasts" . ($this->_specificMediaType == null ? "</a>" : "") . "</span></div>\n";
	    echo "    <center>\n";
	    echo "    <div class=\"topContainer\">\n";
		echo "      <a href=\"" . WEB_PATH . "/podcast/" . $this->_podcasts[0]["HANDLE"] . "/\"><img src=\"" . WEB_PATH .
			"/images/podcasts/" . $this->_podcasts[0]["ICON"] . "\" class=\"icon\" border=\"0\" /></a>\n";
	    echo "      <div class=\"itemTitle\">" . (!$this->_podcasts[0]["PUBLISHED"] ? "<span class=\"unpublishedMaterialNote\">[Unpublished]</span>" : "") .
		"<a href=\"" . WEB_PATH . "/podcast/" . $this->_podcasts[0]["HANDLE"] . "/\">" . $this->_podcasts[0]["TITLE"] . "</a></div>\n";
	    echo "      <div class=\"itemCaption\">" . get_smart_blurb($this->_podcasts[0]["DESCRIPTION"], 50) . "</div>\n";
	    echo "    </div>\n";
	    if (sizeof($this->_podcasts) > 1)
	      echo "    <div class=\"divider\"></div>\n";
	    for ($other_podcast = 1; $other_podcast < sizeof($this->_podcasts); $other_podcast++)
	      echo "    <div class=\"entry" . ($other_podcast < sizeof($this->_podcasts) - 1 ? "Last" : "") . "\">" . (!$this->_podcasts[$other_podcast]["PUBLISHED"] ?
			"<span class=\"unpublishedMaterialNote\">[Unpublished]</span>" : "") . "<a href=\"" . WEB_PATH . "/audio/" .
			$this->_podcasts[$other_podcast]["HANDLE"] . "/\"><b>" . $this->_podcasts[$other_podcast]["TITLE"] . "</b>. " .
			get_smart_blurb($this->_podcasts[$other_podcast]["DESCRIPTION"], 10) . "</a></div>\n";
	    echo "    </center>\n";
		if ($this->_specificMediaType == null)
	      echo "    <div class=\"all\"><a href=\"" . WEB_PATH . "/multimedia/podcasts/\">&laquo; all podcasts</a></div>\n";
	    echo "  </div>\n";
	  }
	  // Sound Slides
	  if (sizeof($this->_soundSlides) > 0)
	  {
	    echo "  <div class=\"" . ($this->_specificMediaType == null ? "frame" : "fullFrame") . "\">\n";
	    echo "    <div class=\"header\"><span>" . ($this->_specificMediaType != null ? "All " : "<a href=\"" . WEB_PATH . "/multimedia/sound-slides/\">") .
			"Soundslides" . ($this->_specificMediaType == null ? "</a>" : "") . "</span></div>\n";
	    echo "    <center>\n";
	    echo "    <a href=\"" . WEB_PATH . "/sound-slides/" . $this->_soundSlides[0]["HANDLE"] . "/\"><img src=\"" . WEB_PATH . "/images/sound_slides/" .
		$this->_soundSlides[0]["THUMBNAIL_IMAGE"] . "\" class=\"video" . (!$this->_soundSlides[0]["PUBLISHED"] ? " unpublishedMaterialNote" : "") . "\" border=\"0\"></a>\n";
	    echo "    <div class=\"topContainer\">\n";
	    echo "      <div class=\"itemTitle\">" . (!$this->_soundSlides[0]["PUBLISHED"] ? "<span class=\"unpublishedMaterialNote\">[Unpublished]</span> " : "") .
		"<a href=\"" . WEB_PATH . "/sound-slides/" . $this->_soundSlides[0]["HANDLE"] . "/\">" . $this->_soundSlides[0]["TITLE"] . "</a></div>\n";
	    echo "      <div class=\"itemCaption\">" . get_smart_blurb($this->_soundSlides[0]["FIRST_SLIDE_CAPTION"], 20) . "</div>\n";
	    echo "    </div>\n";
		if ($this->_specificMediaType != null)
		{
		  $last_day_updated = null;
		  for ($other_slideshows = 1; $other_slideshows < sizeof($this->_soundSlides); $other_slideshows++)
		  {
		    if ($last_day_updated != date("Y-m-d", $this->_soundSlides[$other_slideshows]["DATE_LAST_UPDATED"]))
			{
			  $last_day_updated = date("Y-m-d", $this->_soundSlides[$other_slideshows]["DATE_LAST_UPDATED"]);
			  echo "    <div class=\"subheader\">" . date(DATE_FORMAT, $this->_soundSlides[$other_slideshows]["DATE_LAST_UPDATED"]) . "</div>\n";
			}
		    echo "    <a href=\"" . WEB_PATH . "/sound-slides/" . $this->_soundSlides[$other_slideshows]["HANDLE"] . "/\"><div class=\"iconContainer" .
				(!$this->_soundSlides[$other_slideshows]["PUBLISHED"] ? " unpublishedMaterialNote" : "") . "\" onMouseOver=\"document.getElementById('soundslides-overlay-" .
				$this->_soundSlides[$other_slideshows]["IDENTITY"] . "').style.display='block';\" onMouseOut=\"document.getElementById('soundslides-overlay-" .
				$this->_soundSlides[$other_slideshows]["IDENTITY"] . "').style.display='none';\"><img src=\"" . WEB_PATH .
				"/images/sound_slides/" . $this->_soundSlides[$other_slideshows]["THUMBNAIL_IMAGE"] . "\" border=\"0\"><div class=\"overlay\" id=\"soundslides-overlay-" .
				$this->_soundSlides[$other_slideshows]["IDENTITY"] . "\">" .
				$this->_soundSlides[$other_slideshows]["TITLE"] . "</div></div></a>\n";
		  }
		} else
		{
	      if (sizeof($this->_soundSlides) > 1)
	        echo "    <div class=\"divider\"></div>\n";
		  for ($other_slideshows = 1; $other_slideshows < sizeof($this->_soundSlides); $other_slideshows++)
		  {
		    echo "    <a href=\"" . WEB_PATH . "/sound-slides/" . $this->_soundSlides[$other_slideshows]["HANDLE"] . "/\"><div class=\"iconContainer" .
				(!$this->_soundSlides[$other_slideshows]["PUBLISHED"] ? " unpublishedMaterialNote" : "") . "\"><img src=\"" . WEB_PATH .
				"/images/sound_slides/" . $this->_soundSlides[$other_slideshows]["THUMBNAIL_IMAGE"] . "\" border=\"0\"></div></a>\n";
		  }
		}
	    echo "    </center>\n";
		if ($this->_specificMediaType == null)
	      echo "    <div class=\"all\"><a href=\"" . WEB_PATH . "/multimedia/sound-slides/\">&laquo; all sound slides</a></div>\n";
	    echo "  </div>\n";
	  }
	  echo "</div>\n";
	  echo "<div style=\"clear:both;height:10px;\"></div>\n";
	}
}
?>