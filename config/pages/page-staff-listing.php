<?php
class GoAvesStaff  {
        private $_past_years = array();
	function checkOrRedirect($path, $database)
	{
          $get_past_years = $database->query("SELECT DISTINCT start_year FROM previous_staff_positions WHERE approval_pending='FALSE' ORDER BY start_year DESC");
          while ($past_years = $get_past_years->fetchArray())
            $this->_past_years[] = array("START" => $past_years["start_year"], "END" => ($past_years["start_year"] + 1));
	  return true;
	}
	function getMainTab() { return "staff"; }
	function getPageHandle() { return "staff"; }
	function getPageSubhandle() { return null; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => $this->getPageTitle()); }
	function getPageTitle() { return "Staff Listing"; }
	function getPageContents()
	{

	  echo "        <div class=\"staffListContainer\">\n";
	  echo "          <div class=\"pageHeader\">Staff Listing</div>\n";
	  echo "          <div class=\"sidebar\">\n";
	  echo "            <div class=\"header\">Search Staff</div>\n";
	  echo "            <input type=\"text\" id=\"staff-listing-search\" class=\"search empty\" value=\"Search for staff...\" " .
		"onKeyUp=\"searchStaffListing(this.value);\" onFocus=\"focusStaffListingSearch();\" onBlur=\"blurStaffListingSearch();\" " .
		"onKeyPress=\"processReturnKeySearchStaff();\" />\n<br />\n";
	  echo "            <div class=\"header\">Filter Staff</div>\n";
	  echo "            <div id=\"staff-listing-filters\">\n";
	  echo "              <div class=\"item selectedItem\" id=\"staff-listing-filter-active\" onClick=\"filterStaffListing('active');\">&rsaquo; Current Staff</div>\n";
          foreach ($this->_past_years as $previous_year)
          {
            echo "              <div class=\"item\" id=\"staff-listing-filter-year-start-" . $previous_year["START"] . "\" onClick=\"filterStaffListing('year-start-" . $previous_year["START"] .
                "');\">&rsaquo; " . $previous_year["START"] . " &ndash; " . $previous_year["END"] . " Staff</div>\n";
          }
	  echo "            </div>\n";
	  echo "          </div>\n";
	  echo "          <div class=\"viewport\" id=\"staff-listing-viewport\">\n";
	  require_once (DOCUMENT_ROOT . "/components/page_components/staff-listing-display.php");
	  echo "          </div>\n";
	  echo "          <div style=\"clear:both;\"></div>\n";
         echo "        </div>\n";
	}
}
?>