<?php
class GoAvesTopicHub {
	private $_topics = array();
	function loadTopics($path, $database)
	{
	  $get_topics = $database->query("SELECT handle, title, date_start, date_end, description FROM topics ORDER BY handle ASC");
	  while ($topic = $get_topics->fetchArray())
	    $this->_topics[] = array("HANDLE" => $topic["handle"], "TITLE" => $topic["title"], "DESCRIPTION" => $topic["description"]);
	}
	function checkOrRedirect($path,$database)
	{
	  $this->loadTopics($path, $database);
	  return true;
	}
	function getMainTab() { return "news"; }
	function getPageHandle() { return "topic-hub"; }
	function getPageSubhandle() { return $this->_specificSearch; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "Topic Hub"); }
	function getPageTitle() { return "Topic Hub"; }
	function getPageContents()
	{ 
	  echo "      <center>\n";
	  echo "        <b>Under Design</b><br />\n";
	  foreach ($this->_topics as $topic)
	  {
		echo "        <div onMouseOver=\"this.className='listItemHover';\" onMouseOut=\"this.className='listItem';\" " .
			"onClick=\"window.location='" . WEB_PATH . "/topic/" . $topic["HANDLE"] . "/';\" class=\"listItem\">\n";
		echo "          <div>" . $topic["TITLE"] . "</div>\n";
		echo "          " . get_smart_blurb($topic["DESCRIPTION"], 10) . "\n";
		echo "        </div>\n";
	  }
	  echo "      </center>\n";
	  echo "      <br />\n";
	}
}
?>