<?php
class GoAvesVideo {
	private $_identity;
	private $_handle;
	private $_title;
	private $_datePublished;
	private $_videoFile;
	private $_caption;
	private $_groupInformation;
	private $_published;
	function loadVideo($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT videos.video_identity, videos.title, videos.handle, videos.date_published, videos.video_file, " .
		"videos.captions, videos.related_group, videos.published FROM videos WHERE videos.handle LIKE '" . 
		$database->escapeString($path[1]) . "'" . (ADMIN_VIEW ? "" : " AND videos.published='TRUE'") . " LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_identity = $info["video_identity"];
	  $this->_published = ($info["published"] == "TRUE");
	  $this->_handle = $info["handle"];
	  $this->_title = $info["title"];
	  $this->_datePublished = strtotime($info["date_published"]);
	  $this->_videoFile = $info["video_file"];
	  $this->_caption = $info["captions"];
	  if ($info["related_group"] != "" && ctype_digit($info["related_group"]))
	    $this->_groupInformation = getGroupInformation($database, $info["related_group"], $info["video_identity"], "video");
	  else
	    $this->_groupInformation = null;
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadVideo($path, $database))
	    return new GoAvesMultimedia();
	  return true;
	}
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "video"; }
	function getPageSubhandle() { return $this->_handle; }
	function getPageTitle() { return $this->_title; }
	function getBreadTrail() { return array("home" => "GoAves.com", "multimedia" => "Multimedia", "[this]" => "[Video] " . $this->_title); }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/multimedia/\">&laquo; Return to multimedia</a></div>\n";
	  
	  echo "      <div class=\"sidebarSpecContent\">\n";
	  if ($this->_groupInformation != null)
	    outputGroupInformation($this->_groupInformation);
	  echo "      </div>\n";
	  
	  echo "      <div class=\"articleContainer\">\n";
	  echo "        <div class=\"title\">" . (!$this->_published ? "<span class=\"unpublishedMaterialNote\">[Unpublished]</span> " : "") . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\">Posted " . date(LONG_DATE_FORMAT, $this->_datePublished) . "</div>\n";
	  echo "        <center>\n";
	  echo "          <a href=\"" . WEB_PATH . "/components/videos/" . $this->_videoFile . "\" class=\"video\" id=\"player\"></a>\n";
	  echo "          <script src=\"" . WEB_PATH . "/layout/flowplayer/flowplayer-3.2.2.min.js\"></script>\n";
	  echo "          <script language=\"JavaScript\">\n";
	  echo "            flowplayer(\"player\", \"" . WEB_PATH . "/layout/flowplayer/flowplayer-3.2.2.swf\", { clip : { url : '" . WEB_PATH .
		"/components/videos/" . $this->_videoFile . "', autoPlay : false} });\n";
	  echo "          </script>\n";
	  echo "        </center>\n";
	  if ($this->_caption != "")
	    echo "        <div class=\"caption\">" . $this->_caption . "</div>\n";
	  echo "      </div>\n";
	  if (ADMIN_VIEW)
	  {
	    echo "<div class=\"adminViewBox\">\n";
	    echo "Video Identity: " . $this->_identity . "<br />\n";
	    echo "<span class=\"action\" onClick=\"executeAJAX('" . WEB_PATH . "/manage/admin-master-published-remote-toggle.php?type=video&identity=" . $this->_identity . "'," .
		"function evaluate(value) { if (value == 'published') alert('Video published'); else if (value == 'unpublished') alert('Video unpublished'); else alert(value); " .
		"window.location = '" . WEB_PATH . "/video/" . $this->_handle . "/'; });\">" .
		($this->_published ? "Unpublish" : "Publish") . "</span>\n";
	    echo "</div>\n";
	  }
	}
}
?>