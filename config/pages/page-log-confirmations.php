<?php
class GoAvesLogConfirmations {
	private $_confirmation = array();
	function loadLogConfirmations($path, $database)
	{
	  $confirmations = $database->query("SELECT customer_name, quantity, date_purchased, grade FROM yearbook_confirmations ORDER BY " .
		"customer_name ASC");
	  while ($confirmation = $confirmations->fetchArray())
	    $this->_confirmation[] = array("NAME" => $confirmation["customer_name"], "QUANTITY" => $confirmation["quantity"],
		"DATE_PURCHASED" => $confirmation["date_purchased"], "GRADE" => $confirmation["grade"]);
	}
	function checkOrRedirect($path, $database)
	{
	  $this->loadLogConfirmations($path, $database);
	  return true;
	}
	function getMainTab() { return "log"; }
	function getPageHandle() { return "log"; }
	function getPageSubhandle() { return "confirmations"; }
	function getBreadTrail() { return array("home" => "GoAves.com", "log" => "<i>The Log</i>", "[this]" => "Order Confirmations"); }
	function getPageTitle() { return "The Log Order Confirmations"; }
	function getPageContents()
	{
	    echo "      <div class=\"yearbookHeader\">\n";
		echo "        <div class=\"header\"><i>The Log</i></div>\n";
		echo "        <div class=\"subheader\">Order Confirmations</div>\n";
		echo "      </div>\n";
		
		if (sizeof($this->_confirmation) > 0)
		{
		  echo "      <table class=\"yearbookConfirmations\">\n";
		  echo "        <tr class=\"header\"><td>Name</td><td>Quantity</td><td>Date Ordered</td></tr>\n";
		  foreach ($this->_confirmation as $order)
		  {
		    echo "      <tr class=\"row\" onMouseOver=\"this.className='rowHover';\" onMouseOut=\"this.className='row';\"><td>" .
			  $order["NAME"] . "</td><td>" .
			  $order["QUANTITY"] . "</td><td class=\"last\">" . ($order["DATE_PURCHASED"] != "" ? date(DATE_FORMAT, strtotime($order["DATE_PURCHASED"])) :
			  "Unlisted") . "</td></tr>\n";
		  }
		  echo "      </table>\n";
		} else
		  echo "      <br /><center>The Yearbook Confirmations list is being updated. Check back soon!</center><br />\n";
	}
}
?>