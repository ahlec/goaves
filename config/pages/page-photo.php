<?php
class GoAvesPhoto {
	private $_galleryHandle;
	private $_galleryTitle;
	private $_pictureIdentity;
	private $_title;
	private $_postDate;
	private $_pictureTakenDate;
	private $_captionText;
	private $_imageFile;
	private $_photographer = array();
	private $_photographerFirstName;
	private $_photographerLastName;
	private $_previousImage;
	private $_nextImage;
	private $_numPicturesInGallery;
	private $_pictureNumber;
	function loadPhoto($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT photo_galleries.handle AS \"gallery_handle\", photo_galleries.title AS \"gallery_title\", " .
		"photos.title, photos.post_date, photos.picture_taken_date, photos.caption_text, photos.image_file, staff.first_name AS \"photographer_first_name\", " .
		"staff.last_name AS \"photographer_last_name\", staff.icon_file AS \"photographer_icon\", photos.photographer_identity, photos.picture_identity, " .
		"photo_galleries.gallery_identity, related_group FROM photos JOIN " .
		"photo_galleries ON photos.gallery_identity = photo_galleries.gallery_identity LEFT JOIN staff ON photos.photographer_identity = " .
		"staff.identity WHERE photos.handle LIKE '" . 
		$database->escapeString($path[1]) . "' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_galleryHandle = $info["gallery_handle"];
	  $this->_galleryTitle = $info["gallery_title"];
	  $this->_pictureIdentity = $info["picture_identity"];
	  $this->_title = $info["title"];
	  $this->_postDate = strtotime($info["post_date"]);
	  $this->_pictureTakenDate = strtotime($info["picture_taken_date"]);
	  $this->_captionText = $info["caption_text"];
	  $this->_numPicturesInGallery = $database->querySingle("SELECT count(*) FROM photos WHERE gallery_identity='" . $info["gallery_identity"] . "'");
	  $this->_pictureNumber = $database->querySingle("SELECT count(*) FROM photos WHERE gallery_identity='" . $info["gallery_identity"] . "' AND " .
		"picture_identity <='" . $info["picture_identity"] . "'");
	  $this->_imageFile = $info["image_file"];
	  if ($info["photographer_identity"] == "FALSE")
	    $this->_photographer = false;
	  else
	    $this->_photographer = array("FIRST_NAME" => $info["photographer_first_name"], "LAST_NAME" => $info["photographer_last_name"],
			"ICON" => $info["photographer_icon"]);
	  $this->_photographerFirstName = $info["photographer_first_name"];
	  $this->_photographerLastName = $info["photographer_last_name"];
	  
	  $prev_photo = $database->querySingle("SELECT handle, title, icon_file FROM photos WHERE " .
		"picture_identity < '" . $info["picture_identity"] . "' AND gallery_identity='" . $info["gallery_identity"] . "' ORDER BY " .
		"picture_identity DESC LIMIT 1", true);
	  if ($prev_photo === false)
	    $this->_previousImage = null;
	  else
	    $this->_previousImage = array("HANDLE" => $prev_photo["handle"], "TITLE" => $prev_photo["title"], "ICON" => $prev_photo["icon_file"]);
	  $next_photo = $database->querySingle("SELECT handle, title, icon_file FROM photos WHERE " .
		"picture_identity > '" . $info["picture_identity"] . "' AND gallery_identity='" . $info["gallery_identity"] . "' ORDER BY " .
		"picture_identity ASC LIMIT 1", true);
	  if ($next_photo === false)
	    $this->_nextImage = null;
	  else
	    $this->_nextImage = array("HANDLE" => $next_photo["handle"], "TITLE" => $next_photo["title"], "ICON" => $next_photo["icon_file"]);
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadPhoto($path, $database))
	    return new GoAvesMultimedia();
	  return true;
	}
	
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "photo"; }
	function getPageSubhandle() { return $this->_handle; }
	function getBreadTrail() { return array("home" => "GoAves.com", "multimedia" => "All Multimedia", "gallery/" . $this->_galleryHandle =>
		"[Photo Gallery] " . $this->_galleryTitle, "[this]" => "[Photo] " . $this->_handle); }
	function getPageTitle() { return $this->_title; }

	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/gallery/" . $this->_galleryHandle . "/\">&laquo; Return to gallery</a></div>\n";
		
	  echo "      <div class=\"articleSide\">\n";
	  if ($this->_photographer !== false)
	  {
	    echo "        <img src=\"" . WEB_PATH . "/images/staff/" . $this->_photographer["ICON"] . "\" class=\"staffPicture\" />\n";
	    echo "        <div class=\"authorInfo\">\n";
	    echo "          <b><a href=\"" . WEB_PATH . "/staff-portfolio/" . mb_strtolower($this->_photographer["FIRST_NAME"] . "-" . 
			$this->_photographer["LAST_NAME"]) . "/\">" . $this->_photographer["FIRST_NAME"] . " " . $this->_photographer["LAST_NAME"] . 
			"</a></b><br />\n";
	    //echo "          " . $this->_artistPositions . "\n";
	    echo "        </div>\n";
	    echo "        <div style=\"clear:left;\"></div>\n";
	  }
	  
	  echo "        <div class=\"galleryHeader\">Photo " . $this->_pictureNumber . " of " . $this->_numPicturesInGallery . 
		" in<br /><a href=\"" . WEB_PATH . "/gallery/" .
		$this->_galleryHandle . "/\">" . $this->_galleryTitle . "</a></div>\n";
	  echo "        <div class=\"photoNavHeader\">Previous:</div>\n";
	  echo "        <div class=\"photoNavigation\">\n";
	  if ($this->_previousImage != null)
	  {
		echo "        <a href=\"" . WEB_PATH . "/photo/" . $this->_previousImage["HANDLE"] . "/\">\n";
		echo "          <img src=\"" . WEB_PATH . "/images/photos/" . $this->_previousImage["ICON"] . "\" class=\"photo\" /><br />\n";
		echo "          " . $this->_previousImage["TITLE"] . "\n";
		echo "        </a>\n";
		echo "        <br />\n";
	  }
	  echo "        </div>\n";
	  echo "        <div class=\"photoNavHeader\">Next:</div>\n";
	  echo "        <div class=\"photoNavigation\">\n";
	  if ($this->_nextImage != null)
	  {
		echo "        <a href=\"" . WEB_PATH . "/photo/" . $this->_nextImage["HANDLE"] . "/\">\n";
		echo "          <img src=\"" . WEB_PATH . "/images/photos/" . $this->_nextImage["ICON"] . "\" class=\"photo\" /><br />\n";
		echo "          " . $this->_nextImage["TITLE"] . "\n";
		echo "        </a>\n";
	  }
	  echo "        </div>\n";
	  echo "      </div>\n";
	
	  echo "      <div class=\"articleContainer\">\n";
	  echo "        <div class=\"title\">" . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\">Posted " . date(LONG_DATE_FORMAT, $this->_postDate) . "</div>\n";
	  
	  echo "        <img src=\"" . WEB_PATH . "/images/photos/" . $this->_imageFile . "\" class=\"photo\" border=\"0\"/>";
	  echo "\n";
	  if ($this->_captionText != "")
	    echo "        <div class=\"caption\">" . $this->_captionText . "</div>\n";
		
	  /*generateArbitraryPageNavigation(($this->_previousImage != null ? "photo/" . $this->_previousImage["HANDLE"] : null),
		($this->_previousImage != null ? $this->_previousImage["TITLE"] : ""), $this->_title, ($this->_nextImage != null ? "photo/" .
		$this->_nextImage["HANDLE"] : null), ($this->_nextImage != null ? $this->_nextImage["TITLE"] : ""));*/
	  echo "      </div>\n";
	}
}
?>