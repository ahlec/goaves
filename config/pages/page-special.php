<?php
class GoAvesSpecial {
	private $_identity;
	private $_handle;
	private $_title;
	private $_description;
	private $_active;
	private $_ended;
	private $_banner;
	
	private $_content = array();
	private $_pictures = array();
	private $maxNumberPictures = 10;
	
	function loadSpecial($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT special_identity, handle, title, description, active, ended, banner_image FROM specials WHERE handle LIKE '" .
		$database->escapeString($path[1]) . "' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_identity = $info["special_identity"];
	  $this->_handle = $info["handle"];
	  $this->_title = format_content($info["title"]);
	  $this->_description = format_content($info["description"]);
	  $this->_active = ($info["active"] == "TRUE");
	  $this->_ended = ($this->_active ? null : strtotime($info["ended"]));
	  $this->_banner = $info["banner_image"];
	  
	  $get_pictures = $database->query("SELECT photos.image_file, photos.icon_file, photos.handle AS \"photo_handle\", photo_galleries.published, " .
		"photo_galleries.handle AS \"gallery_handle\" FROM photos LEFT JOIN photo_galleries ON photos.gallery_identity = photo_galleries.gallery_identity WHERE " .
		"special_identity='" . $this->_identity . "'" . (!ADMIN_VIEW ? " AND photo_galleries.published = 'TRUE'" : " LIMIT " . $this->maxNumberPictures));
	  while ($picture = $get_pictures->fetchArray())
	   $this->_pictures[] = array("IMAGE" => ($picture["icon_file"] != "" ? $picture["icon_file"] : $picture["image_file"]), "PHOTO_HANDLE" => $picture["photo_handle"],
			"GALLERY_HANDLE" => $picture["gallery_handle"], "PUBLISHED" => ($picture["published"] == "TRUE"));
			
	  $get_documents = $database->query("SELECT document_identity, title, path, published, post_date FROM documents WHERE special_identity='" . $this->_identity . "'" .
		(ADMIN_VIEW ? "" : " AND published='TRUE'") . " ORDER BY post_date ASC");
	  while ($document = $get_documents->fetchArray())
	    $this->_content[] = array("IDENTITY" => $document["document_identity"], "TYPE" => "document", "TITLE" => format_content($document["title"]),
			"DATE" => strtotime($document["post_date"]), "PATH" => format_content($docment["path"]), "TYPE_NAME" => "Document",
			"HANDLE" => $document["path"], "END_SLASH" => false);
	
	  $get_beats = $database->query("SELECT beat_identity, staff.first_name, staff.last_name, staff.icon_file AS \"staff_icon\", beats.handle, beats.title, beats.post_time, " .
		"beats.contents, beats.image_file AS \"beat_image\", beats.icon_file AS \"beat_icon\", beat_types.type_identity, beat_types.title AS \"type_title\", beat_types.handle AS \"" .
		"beat_type_handle\", published, beats.last_updated FROM beats JOIN staff ON beats.staff_identity = staff.identity JOIN beat_types ON beats.type = " .
		"beat_types.type_identity WHERE beats.special_identity='" . $this->_identity . "'" . (ADMIN_VIEW ? "" : " AND published='TRUE'"));
	  while ($beat = $get_beats->fetchArray())
	    $this->_content[] = array("IDENTITY" => $beat["beat_identity"], "TYPE" => "beat", "TITLE" => format_content($beat["title"]),
			"HANDLE" => $beat["handle"], "DATE" => strtotime($beat["last_updated"]),
			"STAFF_NAME" => format_content($beat["first_name"]) . " " . format_content($beat["last_name"]), "STAFF_ICON" => $beat["staff_icon"], "STAFF_HANDLE" =>
			mb_strtolower($beat["first_name"] . "-" . $beat["last_name"]), "ICON" => ($beat["beat_icon"] != "" ? $beat["beat_icon"] : $beat["beat_image"]),
			"BEAT_TYPE" => $beat["type_identity"], "TYPE_HANDLE" => $beat["beat_type_handle"], "TYPE_TITLE" => format_content($beat["type_title"]),
			"PUBLISHED" => ($beat["published"] == "TRUE"), "BLURB" => get_smart_blurb($beat["contents"], 60) . "...", "TYPE_NAME" => "Article",
			"TYPE_PAGE_HANDLE" => "article", "TYPE_IMAGE_DIRECTORY" => "articles");
	  
	  $get_galleries = $database->query("SELECT gallery_identity, handle, title, date_last_updated, published, gallery_image, number_photos, description FROM " .
		"photo_galleries WHERE special_identity='" . $this->_identity . "'" . (ADMIN_VIEW ? "" : " AND published='TRUE'"));
	  while ($gallery = $get_galleries->fetchArray())
	    $this->_content[] = array("IDENTITY" => $gallery["gallery_identity"], "TYPE" => "photo_gallery", "TITLE" => format_content($gallery["title"]),
			"HANDLE" => $gallery["handle"], "DATE" => strtotime($gallery["date_last_updated"]), "ICON" => $gallery["gallery_image"],
			"PUBLISHED" => ($gallery["published"] == "TRUE"), "BLURB" => "[photos]", "TYPE_NAME" => "Photo Gallery", "TYPE_PAGE_HANDLE" => "photo-gallery",
			"TYPE_IMAGE_DIRECTORY" => "photos");
	  
	  $get_videos = $database->query("SELECT video_identity, handle, title, date_published, published, captions, image_thumbnail FROM videos WHERE special_identity='" .
		$this->_identity . "'" . (ADMIN_VIEW ? "" : " AND published='TRUE'"));
	  while ($video = $get_videos->fetchArray())
	    $this->_content[] = array("IDENTITY" => $video["video_identity"], "TYPE" => "video", "TITLE" => format_content($video["title"]),
			"HANDLE" => $video["handle"], "DATE" => strtotime($video["date_published"]), "ICON" => $video["image_thumbnail"],
			"PUBLISHED" => ($video["published"] == "TRUE"), "CAPTION" => get_smart_blurb($video["caption"], 60) . "...", "TYPE_NAME" => "Video",
			"TYPE_PAGE_HANDLE" => "video", "TYPE_IMAGE_DIRECTORY" => "video_thumbnails");
	  
	  $get_comics = $database->query("SELECT identity, image_file, icon_file, handle, title, date_posted, published FROM cartoons WHERE special_identity='" .
		$this->_identity . "'" . (ADMIN_VIEW ? "" : " AND published='TRUE'"));
	  while ($comic = $get_comics->fetchArray())
	    $this->_content[] = array("IDENTITY" => $comic["identity"], "TYPE" => "comic", "TITLE" => format_content($comic["title"]),
			"HANDLE" => $comic["handle"], "DATE" => strtotime($comic["date_posted"]), "ICON" => ($comic["icon_file"] != "" ? $comic["icon_file"] : $comic["image_file"]),
			"PUBLISHED" => ($comic["published"] == "TRUE"), "TYPE_NAME" => "Comic", "TYPE_PAGE_HANDLE" => "comic", "TYPE_IMAGE_DIRECTORY" => "cartoons");
	  
	  $get_soundslides = $database->query("SELECT slideshow_identity, handle, title, published, date_last_updated, thumbnail_image FROM sound_slides " .
		"WHERE special_identity='" . $this->_identity . "'" . (ADMIN_VIEW ? "" : " AND published='TRUE'"));
	  while ($soundslide = $get_soundslides->fetchArray())
	    $this->_content[] = array("IDENTITY" => $soundslide["slideshow_identity"], "TYPE" => "soundslide", "TITLE" => format_content($soundslide["title"]),
			"HANDLE" => $soundslide["handle"], "DATE" => strtotime($soundslide["date_last_updated"]), "ICON" => $soundslide["thumbnail_image"],
			"PUBLISHED" => ($soundslide["published"] == "TRUE"), "TYPE_NAME" => "Soundslide", "TYPE_PAGE_HANDLE" => "sound-slides",
			"TYPE_IMAGE_DIRECTORY" => "sound_slides");
			
	  $get_podcasts = $database->query("SELECT podcast_identity, handle, title, published, icon_file, description, transcript, date_last_updated FROM podcasts " .
		"WHERE special_identity='" . $this->_identity . "'" . (ADMIN_VIEW ? "" : " AND published='TRUE'"));
	  while ($podcast = $get_podcasts->fetchArray())
	    $this->_content[] = array("IDENTITY" => $podcast["podcast_identity"], "TYPE" => "podcast", "TITLE" => format_content($podcast["title"]),
			"HANDLE" => $podcast["handle"], "DATE" => strtotime($podcast["date_last_updated"]), "ICON" => $podcast["icon_file"],
			"PUBLISHED" => ($podcast["published"] == "TRUE"), "TYPE_NAME" => "Podcast", "TYPE_PAGE_HANDLE" => "podcast", "TYPE_IMAGE_DIRECTORY" => "podcasts");
	  
	  usort($this->_content, array("GoAvesSpecial", "sortContent"));
	  return true;
	}
	static function sortContent($resultA, $resultB)
	{
	  if ($resultA["DATE"] === $resultB["DATE"])
	    return 0;
	  return ($resultA["DATE"] < $resultB["DATE"] ? 1 : -1);
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadSpecial($path, $database))
	    return new GoAvesSpecials();
	  return true;
	}
	function getMainTab() { return "specials"; }
	function getPageHandle() { return "special"; }
	function getPageSubhandle() { return $this->_handle; }
	function getPageTitle() { return $this->_title; }
	function getBreadTrail() { return array("home" => "GoAves.com", "topic-hub" => "Topic Hub", "[this]" => $this->_title); }
	function getPageContents()
	{
	  echo "      <div class=\"specialContainer\">\n";
	  echo "        <div class=\"photos\">\n";
	  $picture_square_offset = rand(0, 1);
	  for ($square = 0; $square < $this->maxNumberPictures; $square++)
	  {
	    if (($picture_square_offset + $square + (($square / 2) % 2)) % 2 == 0 || rand(0, 3) == 1)
		{
		  if (isset($this->_pictures[$square]))
			echo "        <a href=\"" . WEB_PATH . "/photo-gallery/" . $this->_pictures[$square]["GALLERY_HANDLE"] . "/" .
				$this->_pictures[$square]["PHOTO_HANDLE"] . "/\" target=\"_blank\"><img src=\"" .
				WEB_PATH . "/images/photos/" . $this->_pictures[$square]["IMAGE"] . "\" border=\"0\" /></a>\n";
		  else
			echo "        <span class=\"placeholder\"></span>\n";
		} else
		{
		  $number_squares_empty++;
		  echo "        <span></span>\n";
		}
	  }
	  echo "        </div>\n";
	  echo "        <div class=\"title\">" . $this->_title . "</div>\n";
	  echo "        <div class=\"content\">\n";
	  echo "          <img src=\"" . WEB_PATH . "/images/specials/" . $this->_banner . "\" class=\"banner\" />\n";
	  echo "          <div class=\"description\">" . $this->_description . "</div>\n";
	  echo "          <div class=\"contentsContainer\">\n";
	  $previous_special_day = null;
	  foreach ($this->_content as $content)
	  {
	    echo "            <a href=\"" . WEB_PATH . "/" . $content["TYPE_PAGE_HANDLE"] . "/" . $content["HANDLE"] . (isset($content["END_SLASH"]) &&
			!$content["END_SLASH"] ? "" : "/") . "\"";
		if ($previous_special_day == null || $previous_special_day != date(DATE_FORMAT, $content["DATE"]))
		{
		  echo " class=\"item newDay\">\n";
		  echo "              <div class=\"datetime" .
			"\"><span>" . date(DATE_FORMAT, $content["DATE"]) . "</span></div>\n";
		  $previous_special_day = date(DATE_FORMAT, $content["DATE"]);
		} else
		  echo " class=\"item\">\n";
		echo "              <img src=\"" . WEB_PATH . "/images/" . $content["TYPE_IMAGE_DIRECTORY"];
		echo "/" . $content["ICON"] . "\" border=\"0\" class=\"icon\" />\n";
		echo "              <div class=\"info\">\n";
		echo "                <div class=\"type\">" . $content["TYPE_NAME"] . "</div>\n";
		echo "                <div class=\"name\"><span>" . $content["TITLE"] . "</span></div>\n";
		echo "                <div class=\"excerpt\">";
		switch ($content["TYPE"])
		{
		  case "beat": echo $content["BLURB"]; break;
		}
		echo "</div>\n";
		echo "              </div>\n";
		echo "            </a>\n";
	  }
	  echo "          </div>\n";
	  echo "        </div>\n";
	  echo "        <div style=\"clear:both;\"></div>\n";
	  echo "      </div>\n";
	  if (ADMIN_VIEW)
	  {
	    echo "<div class=\"adminViewBox\">\n";
	    echo "<b>Special Identity:</b> " . $this->_identity . "\n";
	    echo "</div>\n";
	  }
	}
}
?>