<?php
class GoAvesLeafListingOld {
	private $_itemsPerPage = 12;
	private $_currentPage = 0;
	private $_numberPages = 0;
	private $_issues = array();
	function load($path, $database)
	{
	  $numberItems = $database->querySingle("SELECT count(*) FROM leaf_issuu");
	  
	  $this->_numberPages = floor($numberItems % $this->_itemsPerPage == 0 ? $numberItems / $this->_itemsPerPage :
		$numberItems / $this->_itemsPerPage + 1);
	  if ($this->_numberPages < 1)
	    $this->_numberPages = 1;
	  $this->_currentPage = parseCurrentPage((isset($path[1]) ? $path[1] : ""), $this->_numberPages);
	  
	  $get_issues = $database->query("SELECT post_date, issue_name, handle, bw_cover, color_cover FROM leaf_issuu ORDER BY post_date DESC LIMIT " . $this->_itemsPerPage . " OFFSET " .
		($this->_itemsPerPage * ($this->_currentPage - 1)));
	  while ($issue = $get_issues->fetchArray())
	    $this->_issues[] = array("TITLE" => $issue["issue_name"], "HANDLE" => $issue["handle"], "POST_DATE" => strtotime($issue["post_date"]),
			"BW_COVER" => $issue["bw_cover"], "COLOR_COVER" => $issue["color_cover"]);
	}
	function checkOrRedirect($path, $database)
	{
	  $this->load($path, $database);
	  return true;
	}
	function getMainTab() { return "leaf"; }
	function getPageHandle() { return "leaf"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "The Leaf Archives"; }
	function getBreadTrail() { return null; }
	function getPageContents()
	{
	  generatePageNavigation("leaf", $this->_currentPage, $this->_numberPages);
	  echo "      <center>\n";
	  foreach ($this->_issues as $issue)
	  {
		echo "        <div onMouseOver=\"this.className='listItemHover';\" onMouseOut=\"this.className='listItem';\" " .
			"onClick=\"window.location='" . WEB_PATH . "/leaf-issue/" . $issue["HANDLE"] . "/';\" class=\"listItem\">\n";
		echo "          <div>" . $issue["TITLE"] . "</div>\n";
		echo "          <img src=\"" . WEB_PATH . "/images/leaf_covers/" . $issue["BW_COVER"] . "\" onMouseOver=\"this.src='" . WEB_PATH .
			"/images/leaf_covers/" . $issue["COLOR_COVER"] . "';\" onMouseOut=\"this.src='" . WEB_PATH . "/images/leaf_covers/" .
			$issue["BW_COVER"] . "';\" />\n";
		echo "        </div>\n";
	  }
	  echo "      </center>\n";
	  generatePageNavigation("leaf", $this->_currentPage, $this->_numberPages);
	}
}
?>