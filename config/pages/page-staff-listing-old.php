<?php
class GoAvesStaffOld  {
	private $_numberActiveStaff;
	private $_currentPage;
	private $_numberPages;
	private $_staff = array();
	function loadStaffListing($path, $database)
	{
	  $this->_numberActiveStaff = $database->querySingle("SELECT count(*) FROM staff WHERE active='TRUE'");
	  
	  $this->_numberPages = floor($this->_numberActiveStaff % STAFF_PER_LISTING_PAGE == 0 ? $this->_numberActiveStaff / STAFF_PER_LISTING_PAGE :
		$this->_numberActiveStaff / STAFF_PER_LISTING_PAGE + 1);
	  if ($this->_numberPages < 1)
	    $this->_numberPages = 1;
	  $this->_currentPage = parseCurrentPage($path[1], $this->_numberPages);
	  
	  $get_staff = $database->query("SELECT first_name, last_name, image_file FROM staff WHERE active='TRUE' AND " .
		"display_on_staff_listing='TRUE' ORDER BY last_name, first_name ASC LIMIT " .
		STAFF_PER_LISTING_PAGE . " OFFSET " . (($this->_currentPage - 1) * STAFF_PER_LISTING_PAGE));
	  while ($staff = $get_staff->fetchArray())
	    $this->_staff[] = array("FIRST_NAME" => $staff["first_name"], "LAST_NAME" => $staff["last_name"], "IMAGE" => $staff["image_file"]);
	}
	function checkOrRedirect($path, $database)
	{
	  $this->loadStaffListing($path, $database);
	  return true;
	}
	function getMainTab() { return "staff"; }
	function getPageHandle() { return "staff"; }
	function getPageSubhandle() { return null; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => $this->getPageTitle()); }
	function getPageTitle() { return "Staff Listing"; }
	function getPageContents()
	{
	  generatePageNavigation("staff-old", $this->_currentPage, $this->_numberPages);
	  echo "        <center>\n";
	  foreach ($this->_staff as $staff)
	  {
		echo "          <div class=\"staffListingContainer\" onMouseOver=\"this.className='staffListingContainerHover';\" onMouseOut=\"" .
			"this.className='staffListingContainer';\" onClick=\"window.location='" . WEB_PATH . "/staff-portfolio/" . mb_strtolower($staff["FIRST_NAME"] .
			"-" . $staff["LAST_NAME"]) . "/';\">\n";
		echo "            <div>" . $staff["FIRST_NAME"] . " " . $staff["LAST_NAME"] . "</div>\n";
		echo "            <img src=\"" . WEB_PATH . "/images/staff/" . $staff["IMAGE"] . "\" />\n";
		echo "          </div>\n";
	  }
	  echo "        </center>\n";
	  
	  generatePageNavigation("staff-old", $this->_currentPage, $this->_numberPages);
	}
}
?>