<?php
class GoAvesSports {
	private $_season;
	private $_seasonTypeIdentity;
	private $_seasonTitle;
	private $_teams = array();
	function determineSeason($path)
	{
	  if (isset($path[1]) && $path[1] != "" && $path[1] != null)
	  {
	    $param = mb_strtolower($path[1]);
		if ($param == "fall")
		{
		  $this->_season = "fall";
		  $this->_seasonTypeIdentity = 2;
		  return;
		}
		if ($param == "winter")
		{
		  $this->_season = "winter";
		  $this->_seasonTypeIdentity = 3;
		  return;
		}
		if ($param == "spring")
		{
		  $this->_season = "spring";
		  $this->_seasonTypeIdentity = 4;
		  return;
		}
	  }
	  $month = date("n");
	  if ($month >= 7 && $month < 11)
	  {
		$this->_season = "fall";
		$this->_seasonTypeIdentity = 2;
	  } else if ($month >= 11 || $month < 3)
	  {
		$this->_season = "winter";
		$this->_seasonTypeIdentity = 3;
	  } else if ($month >= 3 && $month <= 6)
	  {
		$this->_season = "spring";
		$this->_seasonTypeIdentity = 4;
	  }
	}
	function checkOrRedirect($path, $database)
	{
	  $this->determineSeason($path);
	  $this->_seasonTitle = ucfirst($this->_season);
	  
	  $get_sports_teams = $database->query("SELECT groups.handle, groups.title, groups.icon_file FROM groups WHERE " .
		"type='" . $this->_seasonTypeIdentity . "' ORDER BY groups.title ASC");
	  while ($team = $get_sports_teams->fetchArray())
	    $this->_teams[] = array("TITLE" => $team["title"], "HANDLE" => $team["handle"], "ICON" => $team["icon_file"]);
	  return true;
	}
	function getMainTab() { return "sports"; }
	function getPageHandle() { return "sports"; }
	function getPageSubhandle() { return $this->_season; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => $this->_seasonTitle . " Sports"); }
	function getPageTitle() { return $this->_seasonTitle . " Sports"; }
	function getPageContents()
	{
	  echo "      <div class=\"sportsListContainer\">\n";
	  echo "        <div class=\"header\">" . $this->_seasonTitle . " Sports</div>\n";
	  echo "        <div class=\"navigation\">" . ($this->_season == "fall" ? "Fall Sports" :
		"<a href=\"" . WEB_PATH . "/sports/fall/\">Fall Sports</a>") . " &bull; " . ($this->_season == "winter" ? "Winter Sports" :
		"<a href=\"" . WEB_PATH . "/sports/winter/\">Winter Sports</a>") . " &bull; " . ($this->_season == "spring" ? "Spring Sports" :
		"<a href=\"" . WEB_PATH . "/sports/spring/\">Spring Sports</a>") . "</div>\n";
	  if (sizeof($this->_teams) == 0)
	    echo "        There are no sports teams registered for the " . $this->_seasonTitle . " season yet. Check back soon.\n";
	  else
	  {
	    foreach ($this->_teams as $team)
		{
		  echo "        <div class=\"listItem\" onMouseOver=\"this.className='listItemHover';\" " .
			"onMouseOut=\"this.className='listItem';\" onClick=\"window.location='" . WEB_PATH . "/group/" . $team["HANDLE"] . "/';\">\n";
		  echo "          <div>" . $team["TITLE"] . "</div>\n";
		  echo "          <img src=\"" . WEB_PATH . "/images/groups/" . $team["ICON"] . "\" />\n";
		  echo "        </div>\n";
		}
	  }
	  echo "      </div>\n";
	}
}
?>