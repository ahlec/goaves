<?php
class GoAvesCustomPage {
	private $_identity;
	private $_published;
	private $_staffIdentity;
	private $_isFeature;
	private $_authorTitle = "Author";
	private $_handle;
	private $_postTime;
	private $_contents;
	private $_isPHP;
	
	private $_relatedGroup;
	private $_topic = null;
	
	private $_hasTitle;
	private $_title;
	private $_displaySubtitle;
	private $_contentType;
	private $_displaySidebar;
	
	private $_hasReturnLink;
	private $_returnLinkHREF;
	private $_returnLinkDestination;
	
	function loadPage($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT page_identity, handle, staff_identity, published, date_created, date_last_updated, content, is_php, " .
		"related_group, topic, has_title, title, display_subtitle, content_type, display_sidebar, has_return_link, return_link_href, return_link_destination " .
		"FROM pages WHERE handle LIKE '" . $database->escapeString($path[1]) .
		"'" . (!ADMIN_VIEW ? " AND published='TRUE'" : "") . " LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_identity = $info["page_identity"];
	  $this->_published = ($info["published"] == "TRUE");
	  $this->_isFeature = ($database->querySingle("SELECT count(*) FROM features WHERE feature_type='page' AND item_identity='" . $this->_identity . "'") > 0);
	  $this->_staffIdentity = $info["staff_identity"];
	  $this->_handle = $info["handle"];
	  $this->_isPHP = ($info["is_php"] == "TRUE");
	  
	  $this->_dateCreated = strtotime($info["date_created"]);
	  $this->_dateLastUpdated = strtotime($info["date_last_updated"]);
	  
	  $this->_contents = format_content($info["content"]);
	  $this->_relatedGroup = ($info["related_group"] != "" ? $info["related_group"] : null);
	  $this->_topic = ($info["topic"] != "" ? $info["topic"] : null);

	  $this->_hasTitle = ($info["has_title"] == "TRUE");
	  $this->_title = ($info["title"] != "" ? format_content($info["title"]) : null);
	  $this->_displaySubtitle = ($info["display_subtitle"] == "TRUE");
	  $this->_contentType = ($info["content_type"] != "" ? format_content($info["content_type"]) : null);
	  $this->_displaySidebar = ($info["display_sidebar"] == "TRUE");
	  
	  $this->_hasReturnLink = ($info["has_return_link"] == "TRUE");
	  $this->_returnLinkHREF = ($info["return_link_href"] != "" ? $info["return_link_href"] : null);
	  $this->_returnLinkDestination = ($info["return_link_destination"] != "" ? format_content($info["return_link_destination"]) : null);
	  
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadPage($path, $database))
	    return new GoAvesNews(); // ?
	  return true;
	}
	function getMainTab() { return "news"; }
	function getPageHandle() { return "page"; }
	function getPageSubhandle() { return $this->_handle; }
	function getBreadTrail() { return array("home" => "GoAves.com", "news" => "Latest News", "[this]" => $this->_title); }
	function getPageTitle() { return $this->_title; }

	function getPageContents()
	{
	  if ($this->_hasReturnLink)
	    echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/" . $this->_returnLinkHREF . "/\">&laquo; Return" .
			($this->_returnLinkDestination != null ? " to " . $this->_returnLinkDestination : "") . "</a></div>\n";
	  
	  if ($this->_displaySidebar)
	  {
	    echo "      <div class=\"sidebarSpecContent\">\n";
	    require_once (DOCUMENT_ROOT . "/components/page_components/sidebar_author_info.php");
	    if ($this->_relatedGroup != null)
	    {
	      echo "        <div class=\"separator\"></div>\n";
	      require_once (DOCUMENT_ROOT . "/components/page_components/sidebar_related_group.php");
	    }
	    if ($this->_topic != null)
	    {
	      echo "        <div class=\"sectionHeader\">More content on <a href=\"" . WEB_PATH . "/topic/" . $this->_topic["HANDLE"] . 
			"/\">" . $this->_topic["TITLE"] . "</a>:</div>\n";
	      if (sizeof($this->_topic["CONTENTS"]) == 0)
             echo "        <div class=\"noResults\">No other content on this topic</div>\n";
	      else
	      {
		  echo "        <ul>\n";
		  foreach ($this->_topic["CONTENTS"] as $topic_content)
		    echo "          <li><a href=\"" . WEB_PATH . "/article/" . $topic_content["HANDLE"] . "/\">" . $topic_content["TITLE"] .
			"</a></li>\n";
		  echo "        </ul>\n";
	      }
	    }
	    echo "      </div>\n";
	  }
	  
	  echo "      <div class=\"specContentContainer" . (!$this->_displaySidebar ? " noSidebar" : "") . "\">\n";
	  if ($this->_hasTitle)
	    echo "        <div class=\"title\">" . (!$this->_published ? "<span class=\"unpublishedMaterialNote\">[Unpublished]</span> " : "") . $this->_title . "</div>\n";
	  if ($this->_displaySubtitle)
	    echo "        <div class=\"subtitle\">" . ($this->_contentType != null ? "<span class=\"content-type\">" . $this->_contentType . "</span> &ndash; " : "") .
			(date("Y-m-d", $this->_dateLastUpdated) != date("Y-m-d", $this->_dateCreated) ?
			"<b>Last updated on</b> " . date(LONG_DATE_FORMAT, $this->_dateLastUpdated) . " &ndash; " : "") . "<b>Created on</b> " .
			date(LONG_DATE_FORMAT, $this->_dateCreated) . "</div>\n";
	  echo "        <div class=\"article\">\n";
	  if ($this->_isPHP)
	    eval ($this->_contents);
	  else
	    echo format_content($this->_contents);
	  echo "          <div style=\"clear:both;\"></div>\n";
	  echo "        </div>\n";
	  echo "        <div style=\"clear:both;\"></div>\n";
	  if (ADMIN_VIEW)
	  {
	    echo "<div class=\"adminViewBox\">\n";
	    echo "<b>Page Identity:</b> " . $this->_identity . "<br />\n";
	    echo "<b>Staff Identity:</b> " . $this->_staffIdentity . "<br />\n";
	    echo "<b>Is a feature:</b> " . ($this->_isFeature ? "Yes" : "No") . "<br />\n";
	    echo "<a href=\"" . WEB_PATH . "/manage/admin-article.php?article_id=" . $this->_identity . "\" target=\"_blank\">Edit</a><br />\n";
	    echo "<a href=\"" . WEB_PATH . "/manage/admin-article-delete.php?article_id=" . $this->_identity . "\" target=\"_blank\">Delete</a><br />\n";
	    echo "<a href=\"" . WEB_PATH . "/manage/admin-major-story.php?article_id=" . $this->_identity . "\" target=\"_blank\">" . ($this->_isFeature ? "Remove Major Story Label" : "Make Major Story") . "</a>\n";
	    echo "</div>\n";
	  }
	}
}
?>