<?php
class GoAves404 {
	function checkOrRedirect($path, $database) { return true; }
	function getMainTab() { return "goaves"; }
	function getPageHandle() { return "page-not-found"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Page Not Found"; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "Pages That Don't Exist"); }
	function getPageContents()
	{
	  echo "      <div class=\"errorContainer\">\n";
	  echo "        <div class=\"header\">Page Not Found</div>\n";
	  echo "        <div class=\"subheader\">Error 404</div>\n";
	  echo "        <div class=\"description\">";
	  echo "The page or file you were trying to load does not exist. This might be due to an error in our programming, or the file you were trying to access " .
		"having been taken down. If you think you have reached this page in error, please <b><a href=\"" . 
		WEB_PATH . "/contact/\">contact us</a></b> and voice your concern.<br /><br /><b><a href=\"" . WEB_PATH . "/\">&laquo; " .
		"Return to the homepage &raquo;</a></b>";
	  echo "</div>\n";
	  echo "      </div>\n";
	}
}
?>