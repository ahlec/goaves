<?php
class GoAvesPoll {
	private $_identity;
	private $_handle;
	private $_question;
	private $_dateStart;
	private $_dateEnd;
	private $_isActive;
	private $_hasVoted;
	private $_responses = array();
	private $_totalResponses;
	function loadPoll($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT identity, poll_handle, date_start, date_end, poll_question FROM polls WHERE poll_handle LIKE '" . 
		$database->escapeString($path[1]) . "' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  require_once (DOCUMENT_ROOT . "/config/polls.inc.php");
	  $this->_identity = $info["identity"];
	  $this->_handle = $info["poll_handle"];
	  $this->_question = $info["poll_question"];
	  $this->_dateStart = strtotime($info["date_start"]);
	  $this->_dateEnd = strtotime($info["date_end"]);
	  $this->_isActive = ($info["date_start"] <= date("Y-m-d", strtotime("+1 hours")) && $info["date_end"] >= date("Y-m-d", strtotime("+1 hours")));
	  $this->_hasVoted = isset($_SESSION[POLL_VOTING_SESSION]);
	  $get_responses = $database->query("SELECT response_identity, response FROM poll_responses WHERE poll_identity='" . $info["identity"] . "'");
	  while ($response = $get_responses->fetchArray())
	    $this->_responses[] = array("IDENTITY" => $response["response_identity"], "TEXT" => $response["response"]);
	  if ($this->_hasVoted)
	  {
	    $this->_totalResponses = $database->querySingle("SELECT count(*) FROM poll_votes WHERE poll_identity='" . $info["identity"] . "'");
	    for ($response_index = 0; $response_index < sizeof($this->_responses); $response_index++)
	      $this->_responses[$response_index]["VOTES"] = $database->querySingle("SELECT count(*) FROM poll_votes WHERE poll_identity='" . $info["identity"] . "' AND response_identity='" .
			$this->_responses[$response_index]["IDENTITY"] . "'");
	  }
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadPoll($path, $database))
		return new GoAvesInteractions();
	  return true;
	}
	function getMainTab() { return "interactions"; }
	function getPageHandle() { return "poll"; }
	function getPageSubhandle() { return $this->_handle; }
	function getPageTitle() { return "[" . ($this->_isActive ? "Active " : "") . "Poll] " . $this->_question; }
	function getBreadTrail() { return array("home" => "GoAves.com", "interactions" => "Student Interactions", "[this]" => "[" . ($this->_isActive ? "Active " : "") . "Poll] " . $this->_question); }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/interactions/\">&laquo; Return to Student Interactions</a></div>\n";
	  echo "      <div class=\"interactionsContainer\">\n";
	  echo "        <div class=\"title\">" . $this->_question . "</div>\n";
	  echo "        <div class=\"subtitle\">" . date(DATE_FORMAT, $this->_dateStart) . ($this->_dateStart != $this->_dateEnd ? " &ndash; " . date(DATE_FORMAT, $this->_dateEnd) : "") .
		($this->_isActive ? " (Active)" : "") . "</div>\n";

	  if ($this->_isActive && !$this->_hasVoted)
	  {
	    echo "        <div class=\"poll_question\">Question: " . $this->_question . "</div>\n";
	    echo "        <form name=\"latest_poll\" onSubmit=\"return false;\">\n";
	    echo "          <input type=\"hidden\" name=\"latest-poll-identity\" id=\"latest-poll-identity\" value=\"" . $this->_identity . "\" />\n";
	    echo "          <input type=\"hidden\" name=\"latest-poll-handle\" id=\"latest-poll-handle\" value=\"" . $this->_handle . "\" />\n";
	    foreach ($this->_responses as $response)
	      echo "          <div class=\"entry\"><input type=\"radio\" name=\"response\" id=\"poll-response-" . $response["IDENTITY"] . "\" value=\"" .
			$response["IDENTITY"] . "\"> <label for=\"poll-response-" . $response["IDENTITY"] . "\">" . $response["TEXT"] . "</label></div>\n";
	    echo "        </form>\n";
	    echo "        <div class=\"submit\"><button onClick=\"processPollVoting()\">Vote</button></div>\n";
	  }
	  else
	  {
	    echo "<div class=\"responseCount\">" . $this->_totalResponses . " vote" . ($this->_totalResponses == 1 ? "" : "s") . "</div>\n";
	    foreach ($this->_responses as $response)
	      outputProgressBar($response["TEXT"], $response["VOTES"], "vote", ($this->_totalResponses == 0 ? 0 : floor(($response["VOTES"] / $this->_totalResponses) * 100)), 300, false);
	    echo "<br />\n";
	  }
	  echo "      </div>\n";
	}
}
?>