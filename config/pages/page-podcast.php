<?php
class GoAvesPodcast {
	private $_identity;
	private $_handle;
	private $_title;
	private $_datePublished;
	private $_soundFile;
	private $_description;
	private $_audioTranscript;
	private $_artist;
	private $_groupInformation;
	private $_published;
	function loadAudio($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT sound_identity, sound_clips.handle, sound_clips.title, sound_clips.date_published, sound_clips.sound_file, " .
		"sound_clips.description, sound_clips.audio_transcript, sound_clips.artist, sound_clips.related_group, sound_clips.published FROM sound_clips " .
		"WHERE sound_clips.handle LIKE '" . $database->escapeString($path[1]) . "'" . (ADMIN_VIEW ? "" : " AND published='TRUE'") . " LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_identity = $info["sound_identity"];
	  $this->_handle = $info["handle"];
	  $this->_title = $info["title"];
	  $this->_datePublished = strtotime($info["date_published"]);
	  $this->_soundFile = $info["sound_file"];
	  $this->_description = $info["description"];
	  $this->_audioTranscript = $info["audio_transcript"];
	  $this->_artist = $info["artist"];
	  $this->_published = ($info["published"] == "TRUE");
	  if ($info["related_group"] != "" && ctype_digit($info["related_group"]))
	    $this->_groupInformation = getGroupInformation($database, $info["related_group"], $info["sound_identity"], "sound_clip");
	  else
	    $this->_groupInformation = null;
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadAudio($path, $database))
	    return new GoAvesMultimedia();
	  return true;
	}
	
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "audio"; }
	function getPageSubhandle() { return $this->_handle; }
	function getPageTitle() { return $this->_title; }
	function getBreadTrail() { return array("home" => "GoAves.com", "multimedia" => "All Multimedia", "[Audio] " . $this->_title); }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/multimedia/\">&laquo; Return to multimedia</a></div>\n";
	  
	  echo "      <div class=\"sidebarSpecContent\">\n";
	  if ($this->_groupInformation != null)
	    outputGroupInformation($this->_groupInformation);
	  echo "      </div>\n";
	  
	  echo "      <div class=\"articleContainer\">\n";
	  echo "        <div class=\"title\">" . (!$this->_published ? "<span class=\"unpublishedMaterialNote\">[Unpublished]</span> " : "") . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\">Posted " . date(LONG_DATE_FORMAT, $this->_datePublished) . "</div>\n";
	  echo "        <div class=\"audioContainer\">\n";
	  echo "          <div id=\"audio_player\" class=\"audio\">You must have flash installed in order to listen to this sound file.</div>\n";
	  echo "        </div>\n";
	  echo "        <script src=\"" . WEB_PATH . "/layout/audio-player/audio-player.js\"></script>\n"; 
      echo "        <script type=\"text/javascript\">\n";
	  echo "          AudioPlayer.setup(\"" . WEB_PATH . "/layout/audio-player/player.swf\", {\n";
      echo "            width: 600,\n";
	  echo "            animation: \"no\",\n";
      echo "            transparentpagebg: \"yes\"\n";
      echo "          });\n";
      echo "          AudioPlayer.embed(\"audio_player\", {\n";
	  echo "            soundFile: \"" . WEB_PATH . "/components/audio/" . $this->_soundFile . "\",\n";
	  echo "            titles: \"" . $this->_title . "\",\n";
	  echo "            artists: \"" . $this->_artist . "\",\n";
	  echo "            autostart: \"no\"";
	  echo "          });\n";
      echo "        </script>\n";
	  if ($this->_description != "")
	    echo "          <div class=\"caption\">" . $this->_description . "</div>\n";
	  if ($this->_audioTranscript != "")
	  {
	    echo "          <div class=\"audioTranscript\">\n";
		echo "            " . $this->_audioTranscript . "\n";
		echo "          </div>\n";
	  }
	  echo "      </div>\n";
	  echo "      <div style=\"clear:both;\"></div>\n";
	  if (ADMIN_VIEW)
	  {
	    echo "<div class=\"adminViewBox\">\n";
	    echo "Audio Identity: " . $this->_identity . "<br />\n";
	    echo "<span class=\"action\" onClick=\"executeAJAX('" . WEB_PATH . "/manage/admin-master-published-remote-toggle.php?type=sound_clip&identity=" . $this->_identity . "'," .
		"function evaluate(value) { if (value == 'published') alert('Audio published'); else if (value == 'unpublished') alert('Audio unpublished'); else alert(value); " .
		"window.location = '" . WEB_PATH . "/audio/" . $this->_handle . "/'; });\">" .
		($this->_published ? "Unpublish" : "Publish") . "</span>\n";
	    echo "</div>\n";
	  }
	}
}
?>