<?php
class GoAvesGroup {
	private $_identity;
	private $_handle;
	private $_name;
	private $_active;
	private $_yearsActive;
	private $_type;
	private $_imageFile;
	private $_iconFile;
	private $_rosterTitle;
	private $_startTab;
	function loadGroup($path, $database)
	{
	
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT group_identity, groups.handle, title, image_file, icon_file, description, active, years_active, groups.type " .
		"FROM groups WHERE groups.handle LIKE '" . $database->escapeString($path[1]) . "' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_identity = $info["group_identity"];
	  $this->_handle = $info["handle"];
	  $this->_name = $info["title"];
	  $this->_active = ($info["active"] == "TRUE");
	  $this->_yearsActive = $info["years_active"];
	  
	  /* Group Types */
	  $get_type = $database->querySingle("SELECT singular_name, plural_name, handle, roster_title FROM group_types WHERE type_identity='" . $info["type"] . "' LIMIT 1", true);
	  $this->_type = array("IDENTITY" => $info["type"], "SINGULAR_NAME" => $get_type["singular_name"], "PLURAL_NAME" => $get_type["plural_name"], "HANDLE" => $get_type["handle"]);
	  $this->_rosterTitle = $get_type["roster_title"];
	  
	  /* End Group Types */
	  
	  $this->_imageFile = ($info["image_file"] !== "" ? $info["image_file"] : null);
	  $this->_iconFile = $info["icon_file"];
	  
	  if (isset($path[2]))
	  {
	    switch ($path[2])
		{
		  case "content":
		  case "info":
		  case "roster":
		  case "schedule": $this->_startTab = $path[2]; break;
		  default: $this->_startTab = "info"; break;
		}
	  }
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadGroup($path, $database))
	    return new GoAvesGroupListing();
	  return true;
	}
	function getMainTab() { return "group-listing"; }
	function getPageHandle() { return "group"; }
	function getPageSubhandle() { return $this->_handle; }
	function getPageTitle() { return strip_tags($this->_name); }
	function getBreadTrail() { return array("home" => "GoAves.com", "group-listing" => "Group Listing", "[this]" => $this->_groupName); }
	function getBodyOnload() { return "groupPageLoad()"; }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/group-listing/\">&laquo; Return to group listings</a></div>\n";

	  echo "      <div class=\"groupPageContainer\">\n";
	  echo "        <img src=\"" . WEB_PATH . "/images/groups/" . ($this->_iconFile != null ? $this->_iconFile : "no-group-icon.jpg") . "\" class=\"groupIcon\" id=\"group-header-image\" />\n";
	  echo "        <div class=\"name\" id=\"group-header-name\">" . $this->_name . "</div>\n";
	  echo "        <div class=\"subtitle\" id=\"group-header-subtitle\"><a href=\"" . WEB_PATH . "/group-listing/" . $this->_type["HANDLE"] . "/\">" .
		$this->_type["SINGULAR_NAME"] . "</a>" . ($this->_active ? "" : " &ndash; Inactive") . "</div>\n";
	  
	  echo "        <div class=\"tabs\" id=\"group-page-tabs\">\n";
	  echo "          <div class=\"tab" . ($this->_startTab == "content" ? " selected" : "") . "\" id=\"group-page-tab-content\" onClick=\"groupPageChangeTab('" .
		$this->_identity . "','content');\">Content</div>\n";
	  echo "          <div class=\"tab" . ($this->_startTab == "info" ? " selected" : "") . "\" id=\"group-page-tab-info\" onClick=\"groupPageChangeTab('" .
		$this->_identity . "','info');\">Info</div>\n";
	  echo "          <div class=\"tab" . ($this->_startTab == "roster" ? " selected" : "") . "\" id=\"group-page-tab-roster\" onClick=\"groupPageChangeTab('" .
		$this->_identity . "','roster');\">" . $this->_rosterTitle . "</div>\n";
	  echo "          <div class=\"tab" . ($this->_startTab == "schedule" ? " selected" : "") . "\" id=\"group-page-tab-schedule\" onClick=\"groupPageChangeTab('" .
		$this->_identity . "','schedule');\">Schedule</div>\n";
	  echo "        </div>\n";
	  
	  echo "        <div class=\"tabPage\" id=\"group-tab-page\">\n";
 	  require_once (DOCUMENT_ROOT . "/components/page_components/group-page-tab-pages.php");
	  echo "        </div>\n";
	  echo "      </div>\n";

	  if (ADMIN_VIEW)
	  {
	    echo "<div class=\"adminViewBox\">\n";
	    echo "<b>Group Identity:</b> " . $this->_identity . "\n";
	    echo "</div>\n";
	  }
	}
}
?>