<?php
class GoAvesComic {
	private $_authorTitle = "Illustrator";
	private $_identity;
	private $_handle;
	private $_title;
	private $_imageFile;
	private $_datePosted;
	private $_relatedGroup;
	private $_newerComic = array();
	private $_olderComic = array();
	private $_newestComic = array();
	private $_published;
	function loadComic($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT cartoons.identity, cartoons.image_file, cartoons.handle, cartoons.title, cartoons.published, " .
		"cartoons.date_posted, cartoons.artist, related_group, staff.icon_file AS \"artist_icon\", staff.positions " .
		"FROM cartoons JOIN staff ON cartoons.artist = " .
		"staff.identity WHERE cartoons.handle LIKE '" . $database->escapeString($path[1]) . "'" . (ADMIN_VIEW ? "" : " AND published='TRUE'") . " LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_identity = $info["identity"];
	  $this->_published = ($info["published"] == "TRUE");
	  $this->_handle = $info["handle"];
	  $this->_title = $info["title"];
	  $this->_imageFile = $info["image_file"];
	  $this->_datePosted = strtotime($info["date_posted"]);
	  $this->_staffIdentity = $info["artist"];
	  $this->_relatedGroup = ($info["related_group"] != "0" ? $info["related_group"] : null);
	  
	  // show (up to) 10 comics newer, then 20 - number of newer comics number of comics that are older than this
	  
	  $this->_newestComic = $database->querySingle("SELECT image_file, handle, title FROM cartoons WHERE identity<>'" .
		$info["identity"] . "' AND published='TRUE' ORDER BY date_posted DESC LIMIT 1", true);

	  $this->_newerComic = $database->querySingle("SELECT image_file, handle, title, published FROM cartoons WHERE identity<>'" .
		$info["identity"] . "' AND date_posted > '" . $info["date_posted"] . "'" . (!ADMIN_VIEW ? " AND published='TRUE'" : "") .
		" ORDER BY date_posted ASC LIMIT 1", true);
	  $this->_olderComic = $database->querySingle("SELECT image_file, handle, title, published FROM cartoons WHERE identity<>'" .
		$info["identity"] . "' AND date_posted < '" . $info["date_posted"] . "'" . (!ADMIN_VIEW ? " AND published='TRUE'" : "") .
		" ORDER BY date_posted DESC LIMIT 1", true);
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadComic($path, $database))
	    return new GoAvesMultimedia();
	  return true;
	}
	
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "comic"; }
	function getPageSubhandle() { return $this->_handle; }
	function getBreadTrail() { return array("home" => "GoAves.com", "multimedia" => "Multimedia", "[this]" => "[Comic] " . $this->_title); }
	function getPageTitle() { return $this->_title; }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/multimedia/comics/\">&laquo; Return to comics listing</a></div>\n";
	  echo "      <div class=\"sidebarSpecContent\">\n";
	  require_once (DOCUMENT_ROOT . "/components/page_components/sidebar_author_info.php");
	  if ($this->_relatedGroup != null)
	  {
	    echo "        <div class=\"separator\"></div>\n";
	    require_once (DOCUMENT_ROOT . "/components/page_components/sidebar_related_group.php");
	  }
	  
	  echo "        <div class=\"separator\"></div>\n";
	  echo "        <div class=\"sidePanelSectionContainer\">\n";
	  echo "          <div class=\"header\">Other Comics</div>\n";
	  echo "        </div>\n";
	  echo "        <div style=\"clear:left;\"></div>\n";
	  echo "        <div class=\"comicNavHeader\">Newer Comic:</div>\n";
	  echo "        <div class=\"comicNavigation\">\n";
	  if (!is_bool($this->_newerComic))
	  {
		echo "        <a href=\"" . WEB_PATH . "/comic/" . $this->_newerComic["handle"] . "/\">\n";
		echo "          <img src=\"" . WEB_PATH . "/images/cartoons/" . $this->_newerComic["image_file"] .
			"\" class=\"comicIcon" . ($this->_newerComic["published"] == "TRUE" ? "" : " unpublishedMaterialNote") .
			"\" /><br />\n";
		echo "          " . $this->_newerComic["title"] . "\n";
		echo "        </a>\n";
		echo "        <br />\n";
	  } else
	    echo "        No newer comics\n";
	  echo "        </div>\n";
	  echo "        <div class=\"comicNavHeader\">Older Comic:</div>\n";
	  echo "        <div class=\"comicNavigation\">\n";
	  if (!is_bool($this->_olderComic))
	  {
		echo "        <a href=\"" . WEB_PATH . "/comic/" . $this->_olderComic["handle"] . "/\">\n";
		echo "          <img src=\"" . WEB_PATH . "/images/cartoons/" . $this->_olderComic["image_file"] . "\" class=\"comicIcon\" /><br />\n";
		echo "          " . $this->_olderComic["title"] . "\n";
		echo "        </a>\n";
	  } else
	    echo "        No older comics\n";
	  echo "        </div>\n";
	  echo "      </div>\n";
	  
	  echo "      <div class=\"specContentContainer\">\n";
	  echo "        <div class=\"title\">" . (!$this->_published ? "<span class=\"unpublishedMaterialNote\">[Unpublished]</span> " : "") . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\"><span class=\"content-type\">Comic</span> &ndash; <b>Posted on</b> " . date(LONG_DATE_FORMAT, $this->_datePosted) .
		"</div>\n";
	  echo "        <center><img src=\"" . WEB_PATH . "/images/cartoons/" . $this->_imageFile . "\" class=\"comic\" /></center>\n";
	  echo "      </div>\n";
	  if (ADMIN_VIEW)
	  {
	    echo "<div class=\"adminViewBox\">\n";
	    echo "Cartoon Identity: " . $this->_identity . "<br />\n";
	    echo "<span class=\"action\" onClick=\"executeAJAX('" . WEB_PATH . "/manage/admin-master-published-remote-toggle.php?type=cartoon&identity=" . $this->_identity . "'," .
		"function evaluate(value) { if (value == 'published') alert('Comic published'); else if (value == 'unpublished') alert('Comic unpublished'); else alert(value); " .
		"window.location = '" . WEB_PATH . "/comic/" . $this->_handle . "/'; });\">" .
		($this->_published ? "Unpublish" : "Publish") . "</span>\n";
	    echo "</div>\n";
	  }
	}
}
?>