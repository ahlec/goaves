<?php
class GoAvesGroupOld {
	private $_identity;
	private $_handle;
	private $_groupName;
	private $_imageFile;
	private $_iconFile;
	private $_description;
	private $_groupContent = array();
	private $_groupType;
	private $_isSport;
	private $_sportSeason;
	private $_events = array();
	private $_advisors = array();
	private $_groupListingPage;
	function loadGroup($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT group_identity, handle, title, image_file, icon_file, description, type " .
		"FROM groups WHERE handle LIKE '" . $database->escapeString($path[1]) . "' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_identity = $info["group_identity"];
	  $this->_handle = $info["handle"];
	  $this->_groupName = $info["title"];
	  $this->_imageFile = ($info["image_file"] !== "" ? $info["image_file"] : null);
	  $this->_iconFile = $info["icon_file"];
	  $this->_description = $info["description"];
	  $this->_isSport = (strpos($info["type"], "sport") !== false);
	  if ($info["type"] == "fall_sport")
	    $this->_sportSeason = "Fall";
	  else if ($info["type"] == "winter_sport")
	    $this->_sportSeason = "Winter";
	  else if ($info["type"] == "spring_sport")
	    $this->_sportSeason = "Spring";

	  if ($this->_isSport)
	    $this->_groupType = $this->_sportSeason . " Sport";
	  else if ($info["type"] == "class")
	    $this->_groupType = "Student Class"; // ?
	  else if ($info["type"] == "department")
	    $this->_groupType = "School Department";
	  else if ($info["type"] == "club")
	    $this->_groupType = "School Club";

	  $get_advisors = $database->query("SELECT first_name, last_name, gender FROM group_advisors WHERE group_identity='" . $this->_identity . "' ORDER BY last_name, first_name ASC");
	  while ($advisor = $get_advisors->fetchArray())
	    $this->_advisors[] = array("FIRST_NAME" => $advisor["first_name"], "LAST_NAME" => $advisor["last_name"], "TITLE" => ($advisor["gender"] == "MALE" ? "Mr." : "Ms."));

         $groups_before_this = $database->querySingle("SELECT count(*) FROM groups WHERE image_file <> '' AND image_file IS NOT null AND " .
		"icon_file <> '' AND icon_file IS NOT null AND title < '" . $database->escapeString($info["title"]) . "'");
         $this->_groupListingPage = ($groups_before_this == 0 ? 1 : floor($groups_before_this / GROUPS_PER_LISTING_PAGE) + 1);

	  $get_content = $database->query("SELECT title, handle, type FROM group_content WHERE related_group='" . $info["group_identity"] .
		"' ORDER BY datestamp DESC");
	  while ($content = $get_content->fetchArray())
	  {
	    $this->_groupContent[] = array("HANDLE" => $content["handle"], "TITLE" => utf8_encode($content["title"]));
		switch ($content["type"])
		{
		  case "article":
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_HANDLE"] = "article";
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_TITLE"] = "Article";
			break;
		  case "photo_gallery":
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_HANDLE"] = "gallery";
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_TITLE"] = "Photo Gallery";
			break;
		  case "sound_clip":
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_HANDLE"] = "audio";
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_TITLE"] = "Audio";
			break;
		  case "video":
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_HANDLE"] = "video";
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_TITLE"] = "Video";
			break;
		  case "cartoon":
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_HANDLE"] = "comic";
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_TITLE"] = "Comic";
			break;
		  case "sound_slide":
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_HANDLE"] = "sound-slides";
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_TITLE"] = "Sound Slides";
			break;
		}
	  }
	  
	  $get_events = $database->query("SELECT date, title FROM calendar WHERE related_group='" . $info["group_identity"] . "'");
	  while ($event = $get_events->fetchArray())
	    $this->_events[] = array_merge(array("TITLE" => $event["title"]), parse_time_pieces($event["date"]));
	  usort($this->_events, "sort_by_time_pieces");
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadGroup($path, $database))
	    return new GoAvesGroupListing();
	  return true;
	}
	function getMainTab() { return "group-listing"; }
	function getPageHandle() { return "group"; }
	function getPageSubhandle() { return $this->_handle; }
	function getPageTitle() { return $this->_groupName; }
	function getBreadTrail() { return array("home" => "GoAves.com", "group-listing" => "Group Listing", "[this]" => $this->_groupName); }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/group-listing/page-" . $this->_groupListingPage . "/\">&laquo; Return to group listings</a></div>\n";
	 
 	  echo "      <div class=\"articleSide groupSide\">\n";
	  echo "        <div class=\"eventHeader\">Advisor" . (sizeof($this->_advisors) != 1 ? "s" : "") . "</div>\n";
	  foreach ($this->_advisors as $key => $advisor)
	    echo ($key > 0 ? ($key + 1 == sizeof($this->_advisors) ? " and " : ", ") : "") . $advisor["TITLE"] . " " . $advisor["LAST_NAME"];
	  if (sizeof($this->_advisors) == 0)
	    echo "<center>No advisors have been reported.</center>\n";
	  echo "        <div class=\"eventHeader\">Calendar</div>\n";
	  $previous_event_date = null;
	  for ($output_events = 0; $output_events < sizeof($this->_events); $output_events++)
	  {
	    if (isset($this->_events[$output_events]["EVERY"]))
		  echo "        <div class=\"eventSpecial\">Every " . ucfirst($this->_events[$output_events]["EVERY"]) . ": <span>" .
			$this->_events[$output_events]["TITLE"] . "</span></div>\n";
		else
		{
	      $current_event_date = (isset($this->_events[$output_events]["DATE"]) ? $this->_events[$output_events]["DATE"] :
			$this->_events[$output_events]["DATE_START"]);
	      if ($previous_event_date == null || $previous_event_date != $current_event_date)
		    echo "        <div class=\"eventTitle\">" . date(DATE_FORMAT, $current_event_date) . "</div>\n";
		  $previous_event_date = $current_event_date;
		  echo "        <div class=\"eventEntry\">- " . $this->_events[$output_events]["TITLE"] . "</div>\n";
		}
	  }
	  if (sizeof($this->_events) == 0)
	    echo "        <center>No calendar has been reported</center>\n";

	  echo "        <div class=\"eventHeader\">Related Content</div>\n";
	  foreach ($this->_groupContent as $content)
	  {
	    echo "        <a href=\"" . WEB_PATH . "/" . $content["PAGE_HANDLE"] . "/" . $content["HANDLE"] . "/\" class=\"groupRelated\">" .
			"[" . $content["PAGE_TITLE"] . "] " . $content["TITLE"] . "</a><br />\n";
	  }
	  if (sizeof($this->_groupContent) == 0)
	    echo "        <center>No related content has been posted yet.</center>\n";

	  echo "      </div>\n";
	  
	  echo "      <div class=\"articleContainer groupContainer\">\n";
	  echo "        <div class=\"title\">" . $this->_groupName . "</div>\n";
	  echo "        <div class=\"subtitle\">" . $this->_groupType . "</div>\n";
	  if ($this->_imageFile !== null)
	    echo "        <center><img src=\"" . WEB_PATH . "/images/groups/" . $this->_imageFile . "\" class=\"groupImage\" /></center>\n";
	  echo "        <div class=\"description\">" . $this->_description . "</div>\n";
	  echo "      </div>\n";

	  if (ADMIN_VIEW)
	  {
	    echo "<div class=\"adminViewBox\">\n";
	    echo "<b>Group Identity:</b> " . $this->_identity . "\n";
	    echo "</div>\n";
	  }
	}
}
?>