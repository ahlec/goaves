<?php
class GoAvesNews {
	private $_criticalPageNumbers = 3;
	private $_storiesPerPage = 20;
	private $_numberStories;
	private $_currentPage;
	private $_numberPages;
	private $_stories = array();
	private $_selectedCategory = null;
	private $_criticalNumbers = array();
	private $_categories = array();
	function loadStories($path, $database)
	{
	  if (isset($path[1]) && $database->querySingle("SELECT count(*) FROM beat_types WHERE handle LIKE '" .
		$database->escapeString($path[1]) . "'") > 0)
	  {
	    $requested_info = $database->querySingle("SELECT type_identity, handle, title FROM beat_types WHERE handle LIKE '". $database->escapeString($path[1]) . "' LIMIT 1", true);
	    $this->_selectedCategory = array("IDENTITY" => $requested_info["type_identity"], "HANDLE" => $requested_info["handle"], "TITLE" => $requested_info["title"]);
	  }

	  $this->_numberStories = $database->querySingle("SELECT count(*) FROM beats" . (!ADMIN_VIEW || $this->_selectedCategory !== null ? " WHERE" : "") . (!ADMIN_VIEW ? " published='TRUE'" : "") .
		(!ADMIN_VIEW && $this->_selectedCategory !== null ? " AND" : "") . ($this->_selectedCategory !== null ?	" type='" . $this->_selectedCategory["IDENTITY"] . "'" : ""));
	  
	  $this->_numberPages = floor($this->_numberStories % $this->_storiesPerPage == 0 ? $this->_numberStories / $this->_storiesPerPage :
		$this->_numberStories / $this->_storiesPerPage + 1);
	  if ($this->_numberPages < 1)
	    $this->_numberPages = 1;
	  $this->_currentPage = parseCurrentPage(($this->_selectedCategory == null ? (isset($path[1]) ? $path[1] : "") : $path[2]), $this->_numberPages);
	  
	  $get_latest_beats = $database->query("SELECT beats.handle, beats.title, beats.last_updated, beats.contents, beats.image_file, beats.icon_file, " .
		"beats.image_alignment, beats.published, " .
		"beats.image_caption, staff.first_name, staff.last_name, beat_types.short_title AS \"beat_title_short\", beat_types.handle AS \"beat_type_handle\" FROM beats JOIN staff ON beats.staff_identity = " .
		"staff.identity JOIN beat_types ON beats.type = beat_types.type_identity" . (!ADMIN_VIEW || $this->_selectedCategory !== null ? " WHERE" : "") . (!ADMIN_VIEW ? " published='TRUE'" : "") .
		(!ADMIN_VIEW && $this->_selectedCategory !== null ? " AND" : "") . ($this->_selectedCategory !== null ?	" beats.type='" . $this->_selectedCategory["IDENTITY"] . "'" : "") .
		" ORDER BY beats.last_updated DESC LIMIT " . $this->_storiesPerPage . " OFFSET " . (($this->_currentPage - 1) * $this->_storiesPerPage));
	  while ($story = $get_latest_beats->fetchArray())
	    $this->_stories[] = array("HANDLE" => $story["handle"], "TITLE" => format_content($story["title"]), "LAST_UPDATED" => strtotime($story["last_updated"]),
			"IMAGE" => ($story["image_file"] != "" ? $story["image_file"] : "layout/no-image-article.jpg"),
			"ICON" => ($story["icon_file"] != "" ? $story["icon_file"] : null),
			"AUTHOR_FIRST_NAME" => $story["first_name"], "AUTHOR_LAST_NAME" => $story["last_name"], "BEAT_TITLE_SHORT" => $story["beat_title_short"],
			"BEAT_TYPE_HANDLE" => $story["beat_type_handle"], "SNIPPET" => get_smart_blurb($story["contents"], 40), "PUBLISHED" => ($story["published"] == "TRUE"));
	
	  // Generate Critical Page Numbers
	  $this->_criticalNumbers[] = $this->_currentPage;
	  for ($pre = 1; $pre <= $this->_criticalPageNumbers; $pre++)
	    if ($this->_currentPage - $pre >= 1)
	      $this->_criticalNumbers[] = ($this->_currentPage - $pre);
	  for ($post = 1; $post <= $this->_criticalPageNumbers; $post++)
	    if ($this->_currentPage + $post <= $this->_numberPages)
	      $this->_criticalNumbers[] = ($this->_currentPage + $post);
	  if ($this->_currentPage - $this->_criticalPageNumbers >= 1)
	    $this->_criticalNumbers[] = 1;
	  if ($this->_numberPages - $this->_currentPage - $this->_criticalPageNumbers >= 0)
	    $this->_criticalNumbers[] = $this->_numberPages;
	  $this->_criticalNumbers = array_unique($this->_criticalNumbers);
	  sort($this->_criticalNumbers);
	
	  $get_categories = $database->query("SELECT handle, short_title FROM beat_types ORDER BY sort_order ASC");
	  while ($category = $get_categories->fetchArray())
	    $this->_categories[] = array("HANDLE" => $category["handle"], "TITLE" => $category["short_title"]);
	  
	}
	function checkOrRedirect($path,$database)
	{
	  $this->loadStories($path, $database);
	  return true;
	}
	function getMainTab() { return "news"; }
	function getPageHandle() { return "news"; }
	function getPageSubhandle() { return $this->_selectedCategory["HANDLE"]; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "Latest News"); }
	function getPageTitle() { return "Latest News"; }

	function getPageContents()
	{
	  echo "      <div class=\"newsListing\">\n";
	  echo "        <div class=\"listingTitle\">\n";
	  echo "          <div class=\"super\">News Listing</div>\n";
	  echo "          <div class=\"main\">" . ($this->_selectedCategory !== null ? $this->_selectedCategory["TITLE"] . " Articles" : "All Articles") . "</div>\n";
	  echo "          <div class=\"sub\">";
	  echo ($this->_selectedCategory == null ? "<b>" : "<a href=\"" . WEB_PATH . "/news/\">") . "All" .
	  	($this->_selectedCategory == null ? "</b>" : "</a>") . (sizeof($this->_categories) > 0 ?
	  	" &bull; " : "");
	  for ($category = 0; $category < sizeof($this->_categories); $category++)
	    echo ($this->_selectedCategory != null && $this->_selectedCategory["HANDLE"] == $this->_categories[$category]["HANDLE"] ?
	    	"<b>" : "<a href=\"" . WEB_PATH . "/news/" . $this->_categories[$category]["HANDLE"] . "/\">") .
	        $this->_categories[$category]["TITLE"] . ($this->_selectedCategory != null && $this->_selectedCategory["HANDLE"] ==
	        $this->_categories[$category]["HANDLE"] ? "</b>" : "</a>") . ($category < sizeof($this->_categories) - 1 ?
	        " &bull; " : "");
	  echo "</div>\n";
	  echo "        </div>\n";
	  echo "        <div class=\"newNavigation\"><b>Go to page:</b> \n";
	  foreach ($this->_criticalNumbers as $crit_position => $critical)
	  {
	    echo "          ";
	    if ($critical == $this->_currentPage)
	      echo "<b>";
	    else
	      echo "<a href=\"" . WEB_PATH . "/news" . ($this->_selectedCategory !== null ? "/" . $this->_selectedCategory["HANDLE"] : "") . "/page-" . $critical . "/\">";
	    echo $critical;
	    echo ($critical == $this->_currentPage ? "</b>" : "</a>");
	    if (isset($this->_criticalNumbers[$crit_position + 1]))
	    {
	      if ($this->_criticalNumbers[$crit_position + 1] - $critical > 1)
		 echo " ... ";
	      else if ($this->_criticalNumbers[$crit_position + 1] - $critical == 1)
	        echo " &middot; ";
	    }
	    echo "\n";
	  }
	  echo "        </div>\n";
	  if (sizeof($this->_stories) > 0)
	  {
	    echo "        <div class=\"header\">\n";
	    echo "          <div class=\"category\">category</div>\n";
	    echo "          <div class=\"image\"></div>\n";
	    echo "          <div class=\"title\"></div>\n";
	    echo "          <div class=\"time\">last updated</div>\n";
	    echo "        </div>\n";
	    $even_number = false;
	    foreach ($this->_stories as $story)
	    {
	      echo "        <div class=\"article " . ($even_number ? "even" : "odd") . ($story["PUBLISHED"] ? "" : " unpublishedMaterialNote") .
			"\" onMouseOver=\"this.className='article hover " . ($even_number ? "even" : "odd") . ($story["PUBLISHED"] ? "" : " unpublishedMaterialNote") . "';\" " .
			"onMouseOut=\"this.className='article " . ($even_number ? "even" : "odd") . ($story["PUBLISHED"] ? "" : " unpublishedMaterialNote") . "';\">\n";
	      $even_number = !$even_number;
	      echo "          <div class=\"category\">" . ($story["BEAT_TITLE_SHORT"] != "Beat" ? "[<a href=\"" . WEB_PATH . "/news/" . $story["BEAT_TYPE_HANDLE"] . "/\">" . $story["BEAT_TITLE_SHORT"] . "</a>]" : "") . "</div>\n";
	      echo "          <div class=\"image\"><a href=\"" . WEB_PATH . "/article/" . $story["HANDLE"] . "/\">" . 
			"<img src=\"" . WEB_PATH . "/images/articles/" . ($story["ICON"] != null ? $story["ICON"] : $story["IMAGE"]) .
			"\"" . ($story["PUBLISHED"] ? "" : " class=\"unpublishedMaterialNote\"") . " border=\"0\" /></a></div>\n";
	      echo "          <div class=\"title\">" . ($story["PUBLISHED"] ? "" : "<span class=\"unpublishedMaterialNote\"><b>[Unpublished]</b></span> ") .
			"<a href=\"" . WEB_PATH . "/article/" . $story["HANDLE"] . "/\" class=\"title\">" . $story["TITLE"] . "</a><br />" . $story["SNIPPET"] . "... <a href=\"" .
			WEB_PATH . "/article/" . $story["HANDLE"] . "/\">Full Story &raquo;</a></div>\n";
	      echo "          <div class=\"time\">[" . date(DATE_FORMAT, $story["LAST_UPDATED"]) . "]</div>\n";
	      echo "        </div>\n";
	    }
	  } else
	    echo "There are no articles posted.\n";
	  echo "        <div class=\"header\">\n";
	  echo "          <div class=\"category\">category</div>\n";
	  echo "          <div class=\"image\"></div>\n";
	  echo "          <div class=\"title\"></div>\n";
	  echo "          <div class=\"time\">last updated</div>\n";
	  echo "        </div>\n";
	  echo "        <div class=\"newNavigation\"><b>Go to page:</b> \n";
	  foreach ($this->_criticalNumbers as $crit_position => $critical)
	  {
	    echo "          ";
	    if ($critical == $this->_currentPage)
	      echo "<b>";
	    else
	      echo "<a href=\"" . WEB_PATH . "/news" . ($this->_selectedCategory !== null ? "/" . $this->_selectedCategory["HANDLE"] : "") . "/page-" . $critical . "/\">";
	    echo $critical;
	    echo ($critical == $this->_currentPage ? "</b>" : "</a>");
	    if (isset($this->_criticalNumbers[$crit_position + 1]))
	    {
	      if ($this->_criticalNumbers[$crit_position + 1] - $critical > 1)
		 echo " ... ";
	      else if ($this->_criticalNumbers[$crit_position + 1] - $critical == 1)
	        echo " &middot; ";
	    }
	    echo "\n";
	  }
	  echo "        </div>\n";
	  echo "      </div>\n";
	}
}
?>