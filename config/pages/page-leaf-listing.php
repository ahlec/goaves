<?php
class GoAvesLeafListing {
	private $_itemsPerPage = 12;
	private $_currentPage = 0;
	private $_numberPages = 0;
	private $_issues = array();
	private $_schoolYear;
	private $_schoolMonths = array("August", "September", "October", "November", "December", "January", "February", "March", "April", "May");
	private $_schoolYearsWithIssuesOnline = array();
	private $_latestIssue;
	private $_isCurrentSchoolYear;
	function load($path, $database)
	{
	  if (isset($path[1]) && mb_strlen($path[1]) == 9)
	  {
	    $year_pieces = explode("-", $path[1]);
		if (sizeof($year_pieces) == 2 && ctype_digit($year_pieces[0]) && ctype_digit($year_pieces[1]) && $year_pieces[1] - $year_pieces[0] == 1)
		  $this->_schoolYear = $database->escapeString($year_pieces[0]);
		else
		  $this->_schoolYear = (date("m") < 8 ? date("Y") - 1 : date("Y"));
	  } else
	    $this->_schoolYear = (date("m") < 8 ? date("Y") - 1 : date("Y"));
	  $this->_isCurrentSchoolYear = ($this->_schoolYear == (date("m") < 8 ? date("Y") - 1 : date("Y")));
	  $this->_latestIssue = $database->querySingle("SELECT issuu_identity FROM leaf_issuu ORDER BY post_date DESC LIMIT 1");
	  $get_issues = $database->query("SELECT issuu_identity, post_date, issue_name, handle, bw_cover, color_cover, number_pages FROM leaf_issuu WHERE post_date > '" .
		$this->_schoolYear . "-07' AND post_date < '" . ($this->_schoolYear + 1) . "-08' ORDER BY post_date DESC");
	  $this->_issues = array_pad($this->_issues, 10, array("PUBLISHED" => false));
	  while ($issue = $get_issues->fetchArray())
	  {
	    $get_items = $database->query("SELECT page_number, item_type, item_title, subtitle FROM leaf_items WHERE leaf_identity='" . $issue["issuu_identity"] .
			"' AND noteworthy='TRUE' ORDER BY page_number, item_type, item_title ASC");
		$items = array();
		while ($item = $get_items->fetchArray())
		  $items[] = array("PAGE" => $item["page_number"], "TYPE" => $item["item_type"], "TITLE" => format_content($item["item_title"]),
					"SUBTITLE" => ($item["subtitle"] != "" ? format_content($item["subtitle"]) : null));
	    $issue_index = 9 - array_search(date("F", strtotime($issue["post_date"])), $this->_schoolMonths);
		
	    $this->_issues[$issue_index] = array("TITLE" => $issue["issue_name"], "HANDLE" => $issue["handle"], "POST_DATE" => strtotime($issue["post_date"]),
			"BW_COVER" => $issue["bw_cover"], "COLOR_COVER" => $issue["color_cover"], "NUMBER_PAGES" => $issue["number_pages"], "ITEMS" => $items,
			"IDENTITY" => $issue["issuu_identity"]);
	  }
	  $this->_issues = array_pad($this->_issues, -10, array("PUBLISHED" => false));
	  
	  $earliest_issue = strtotime($database->querySingle("SELECT post_date FROM leaf_issuu ORDER BY post_date ASC LIMIT 1"));
	  $latest_issue = strtotime($database->querySingle("SELECT post_date FROM leaf_issuu ORDER BY post_date DESC LIMIT 1"));
	  $cur_school_year = strtotime((date("m", $earliest_issue) < 8 ? date("Y", $earliest_issue) - 1 : date("Y", $earliest_issue)) . "-01-01");
	  while (date("m", $latest_issue) > 8 ? (date("Y", $cur_school_year) < date("Y", $latest_issue) - 1) : (date("Y", $cur_school_year) < date("Y", $latest_issue)))
	  {
	    $this->_schoolYearsWithIssuesOnline[] = date("Y", $cur_school_year);
		$cur_school_year = strtotime("+1 year", $cur_school_year);
	  }
	  $this->_schoolYearsWithIssuesOnline = array_reverse($this->_schoolYearsWithIssuesOnline);
	  
	  /*$get_school_years = $database->query("SELECT DISTINCT SUBSTR(post_date, 0, 4) AS \"year\" FROM leaf_issuu ORDER BY year DESC");
	  $previous_school_year = null;
	  $unused_school_years = array();
	  while ($school_year = $get_school_years->fetchArray())
	  {
	    //if ($previous_school_year != null && $previous_school_year - $school_year["year"] == 1)
		    $this->_schoolYearsWithIssuesOnline[] = $school_year["year"];
		//else
		//  $unused_school_years[] = $school_year["year"];
		//$previous_school_year = $school_year["year"];
	  }*/
	}
	function checkOrRedirect($path, $database)
	{
	  $this->load($path, $database);
	  return true;
	}
	function getMainTab() { return "leaf"; }
	function getPageHandle() { return "leaf"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "The Leaf Archives"; }
	function getBreadTrail() { return null; }
	function getPageContents()
	{
	  echo "        <div class=\"leafListing\">\n";
	  echo "          <div class=\"title\"><i>The Leaf</i> Online</div>\n";
	  echo "          <div class=\"subtitle\">" . $this->_schoolYear . " &ndash; " . ($this->_schoolYear + 1) . " school year</div>\n";
	  echo "          <div class=\"sidebar\">\n";
	  echo "            <div class=\"header\">School Years</div>\n";
	  foreach ($this->_schoolYearsWithIssuesOnline as $school_year)
	    echo "            <a class=\"item" . ($this->_schoolYear == $school_year ? " selectedItem" : "") . "\" href=\"" . WEB_PATH . "/leaf/" .
			$school_year . "-" . ($school_year + 1) . "/\">&rsaquo; " . $school_year . " - " . ($school_year + 1) . "</a><br />\n";
	  echo "          </div>\n";
	  echo "          <div class=\"listContainer\">\n";
	  $month_index = 9;
	  foreach ($this->_issues as $issue)
	  {
	    $leaf_exists = (!isset($issue["PUBLISHED"]));
		echo "        <" . ($leaf_exists ? "a href=\"" . WEB_PATH . "/leaf-issue/" . $issue["HANDLE"] . "/\"" : "div") . " class=\"issue" . 
			($leaf_exists && $this->_latestIssue == $issue["IDENTITY"] ? " latest" : "") . "\">\n";
		echo "          <div class=\"month\">" . $this->_schoolMonths[$month_index] . "</div>\n";
		echo "          <div class=\"cover coverDefault\" style=\"background-image:url('" . WEB_PATH . "/images/leaf_covers/" . ($leaf_exists ? $issue["BW_COVER"] :
			"no-listing-cover.jpg") . "');\"></div>\n";
		if ($leaf_exists)
			echo "          <div class=\"cover coverAlternate\" style=\"background-image:url('" . WEB_PATH . "/images/leaf_covers/" . $issue["COLOR_COVER"] . "');\"></div>\n";
		echo "          <div class=\"issueTitle\">\n";
		echo "            " . ($leaf_exists ? $issue["TITLE"] : $this->_schoolMonths[$month_index] . " Issue") . "\n";
		echo "            <div class=\"issueSubtitle\">" . ($leaf_exists ? $issue["NUMBER_PAGES"] . " pages" . ($this->_latestIssue == $issue["IDENTITY"] ?
			" &ndash; <b>latest issue</b>" : "") : "Not online") . "</div>\n";
		echo "          </div>\n";
		echo "          <div class=\"issueInfo\">\n";
		if ($leaf_exists)
		{
		  echo "          <div class=\"label\">Inside:</div>\n";
		  echo "          <div class=\"items\">\n";
		  foreach ($issue["ITEMS"] as $item_index => $item)
		  {
		    echo "            <span class=\"item\"><img src=\"" . WEB_PATH . "/images/icons/";
			switch ($item["TYPE"])
			{
			  case "article": echo "icon-leaf-article.png"; break;
			}
			echo "\" border=\"0\" /><span class=\"itemTitle\">" . $item["TITLE"] . "";
			$last_title_char = substr($item["TITLE"], -1);
			if (!in_array(substr($item["TITLE"], -1), array(".", "?", "!", ";", ":")) && $item["SUBTITLE"] != null)
			  echo ":";
			echo "</span> " . $item["SUBTITLE"] . "</span>" . ($item_index < sizeof($issue["ITEMS"]) - 1 ? " | " : "") . "\n";
		  }
		  echo "          </div>\n";
		} else if ($this->_isCurrentSchoolYear)
			echo "          <span class=\"comingSoon\">Coming Soon</span>\n";
		else if (!$this->_isCurrentSchoolYear)
			echo "          <span class=\"comingSoon\">Issue is not available at this time</span>\n";
		echo "          </div>\n";
		echo "        </" . ($leaf_exists ? "a" : "div") . ">\n";
		$month_index--;
	  }
	  echo "          </div>\n";
	  echo "        </div>\n";
	}
}
?>