<?php
class GoAvesMultimediaOld {
	private $_itemsPerPage = 18;
	private $_numberItems;
	private $_currentPage;
	private $_numberPages;
	private $_items = array();
	function loadMultimedia($path, $database)
	{
	  $this->_numberItems = $database->querySingle("SELECT count(*) FROM all_published_multimedia");
	  
	  $this->_numberPages = floor($this->_numberItems % $this->_itemsPerPage == 0 ? $this->_numberItems / $this->_itemsPerPage :
		$this->_numberItems / $this->_itemsPerPage + 1);
	  if ($this->_numberPages < 1)
	    $this->_numberPages = 1;
	  $this->_currentPage = parseCurrentPage($path[1], $this->_numberPages);
	  
	  $get_items = $database->query("SELECT handle, title, type, image FROM ordered_multimedia LIMIT " . $this->_itemsPerPage .
		" OFFSET " . (($this->_currentPage - 1) * $this->_itemsPerPage));
	  while ($item = $get_items->fetchArray())
	  {
	    $this->_items[] = array("HANDLE" => $item["handle"], "TITLE" => $item["title"], "TYPE" => $item["type"], "IMAGE" => $item["image"]);
		switch ($item["type"])
		{
		  case "photo_gallery": $this->_items[sizeof($this->_items) - 1]["PAGE_HANDLE"] = "gallery"; break;
		  case "sound_clip": $this->_items[sizeof($this->_items) - 1]["PAGE_HANDLE"] = "audio"; break;
		  case "video": $this->_items[sizeof($this->_items) - 1]["PAGE_HANDLE"] = "video"; break;
		  case "cartoon": $this->_items[sizeof($this->_items) - 1]["PAGE_HANDLE"] = "comic"; break;
		  case "sound_slide": $this->_items[sizeof($this->_items) - 1]["PAGE_HANDLE"] = "sound-slides"; break;
		  default: $this->_items[sizeof($this->_items) - 1]["PAGE_HANDLE"] = "page-not-found"; break;
		}
	  }
	}
	function checkOrRedirect($path,$database)
	{
	  $this->loadMultimedia($path, $database);
	  return true;
	}
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "multimedia"; }
	function getPageSubhandle() { return null; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "All Multimedia"); }
	function getPageTitle() { return "All Multimedia"; }
	function getPageContents()
	{ 
	  generatePageNavigation("multimedia", $this->_currentPage, $this->_numberPages);
	  echo "      <center>\n";
	  foreach ($this->_items as $item)
	  {
		echo "        <div onMouseOver=\"this.className='listItemHover';\" onMouseOut=\"this.className='listItem';\" " .
			"onClick=\"window.location='" . WEB_PATH . "/" . $item["PAGE_HANDLE"] . "/" . $item["HANDLE"] . "/';\" class=\"listItem\">\n";
		echo "          <div>" . $item["TITLE"] . "</div>\n";
		echo "          <img src=\"" . WEB_PATH . "/";
		switch ($item["TYPE"])
		{
		  case "photo_gallery": echo ($item["IMAGE"] != "" ? "images/photos/" . $item["IMAGE"] : "layout/large-icon-galleries.png"); break;
		  case "sound_clip": echo "layout/large-icon-audio.png"; break;
		  case "video": echo "layout/large-icon-video.png"; break;
		  case "cartoon": echo "images/cartoons/" . $item["IMAGE"]; break;
		  default: echo "page-not-found"; break;
		}
		echo "\" />\n";
		echo "        </div>\n";
	  }
	  echo "      </center>\n";
	  
	  generatePageNavigation("multimedia", $this->_currentPage, $this->_numberPages);
	}
}
?>