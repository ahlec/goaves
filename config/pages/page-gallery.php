<?php
class GoAvesGallery {
	private $_identity;
	private $_handle;
	private $_staffIdentity;
	private $_authorTitle = ""; // ???
	private $_title;
	private $_dateLastUpdated;
	private $_numberPhotos;
	private $_selectedPhoto = null;
	private $_photos = array();
	private $_published;
	private $_relatedGroup;
	function loadGallery($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT gallery_identity, staff_identity, handle, title, date_last_updated, related_group, published, topic FROM " .
		"photo_galleries WHERE handle LIKE '" . $database->escapeString($path[1]) . "'" . (ADMIN_VIEW ? "" : " AND published='TRUE'") . " LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_identity = $info["gallery_identity"];
	  $this->_staffIdentity = ($info["staff_identity"] != "" ? $info["staff_identity"] : null);
	  $this->_published = ($info["published"] == "TRUE");
	  $this->_handle = $info["handle"];
	  $this->_title = $info["title"];
	  $this->_dateLastUpdated = strtotime($info["date_last_updated"]);
	  $this->_relatedGroup = ($info["related_group"] != "" ? $info["related_group"] : null);
	  $this->_topic = ($info["topic"] != "" ? $info["topic"] : null);
	  $this->_numberPhotos = $database->querySingle("SELECT count(*) FROM photos WHERE gallery_identity='" . $info["gallery_identity"] . "'");
	  
	  if (isset($path[2]) && $database->querySingle("SELECT count(*) FROM photos WHERE gallery_identity='" .
		$this->_identity . "' AND handle LIKE '" . $database->escapeString($path[2]) . "'") > 0)
	  {
	    $selected_photo = $database->querySingle("SELECT title, post_date, caption_text, image_file, " .
			"picture_identity FROM photos WHERE gallery_identity='" . $this->_identity . "' AND handle LIKE '" .
			$database->escapeString($path[2]) . "' LIMIT 1", true);
	    $this->_selectedPhoto = array("IMAGE" => $selected_photo["image_file"], 
			"TITLE" => $selected_photo["title"], "DATE_POSTED" => strtotime($selected_photo["post_date"]),
			"CAPTION" => $selected_photo["caption_text"], "IDENTITY" => $selected_photo["picture_identity"]);
	  }
	  
	  $get_photos = $database->query("SELECT handle, image_file, icon_file FROM photos WHERE gallery_identity='" . $info["gallery_identity"] .
		"' ORDER BY post_date DESC");
	  while ($photo = $get_photos->fetchArray())
	    $this->_photos[] = array("HANDLE" => $photo["handle"], "ICON" => ($photo["icon_file"] != "" ? $photo["icon_file"] : $photo["image_file"]));
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadGallery($path, $database))
	    return new GoAvesMultimedia();
	  return true;
	}
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "photo-gallery"; }
	function getPageSubhandle() { return $this->_handle; }
	function getPageTitle() { return $this->_title; }
	function getBreadTrail() { return array("home" => "GoAves.com", "multimedia" => "Multimedia", "[this]" => "[Photo Gallery] " . $this->_title); }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/multimedia/photo-galleries/\">&laquo; Return to photo galleries listing</a></div>\n";
	  echo "      <div class=\"sidebarSpecContent\">\n";
	  if ($this->_staffIdentity != null)
	    require_once (DOCUMENT_ROOT . "/components/page_components/sidebar_author_info.php");
	  if ($this->_staffIdentity != null && $this->_relatedGroup != null)
		echo "        <div class=\"separator\"></div>\n";
	  if ($this->_relatedGroup != null)
	    require_once (DOCUMENT_ROOT . "/components/page_components/sidebar_related_group.php");
	  echo "      </div>\n";
	  
	  echo "      <div class=\"specContentContainer\">\n";
	  echo "        <div class=\"title\">" . (!$this->_published ? "<span class=\"unpublishedMaterialNote\">[Unpublished]</span> " : "") . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\"><span class=\"content-type\">Photo Gallery</span> &ndash; <b>" .
		$this->_numberPhotos . "</b> photo" . ($this->_numberPhotos == 1 ? "" : "s") . " &ndash; " .
		"<b>Last updated on</b> " . date(LONG_DATE_FORMAT, $this->_dateLastUpdated) . "</div>\n";
	
	  if ($this->_selectedPhoto != null)
	  {
	    echo "        <center>\n";
		echo "          <div class=\"galleryPhotoContainer\">\n";
		echo "            <div class=\"photo\">\n";
		echo "              <img src=\"" . WEB_PATH . "/images/photos/" . $this->_selectedPhoto["IMAGE"] . "\" />\n";
		echo "            </div>\n";
		if ($this->_selectedPhoto["TITLE"] != "")
		  echo "            <div class=\"photoTitle\">" . $this->_selectedPhoto["TITLE"] . "</div>\n";
		if ($this->_selectedPhoto["CAPTION"] != "")
		  echo "            <div class=\"photoCaption\">" . $this->_selectedPhoto["CAPTION"] . "</div>\n";
		echo "            <div class=\"photoDate\">Posted on " . date(DATE_FORMAT, $this->_selectedPhoto["DATE_POSTED"]) . "</div>\n";
	    echo "          </div>\n";
		echo "        </center>\n";
	  }
	  
	  echo "        <div class=\"subheader\">Images</div>\n";
	  
	  if ($this->_numberPhotos == 0)
	    echo "        No photos in the gallery\n";
	  else
	  {
	    echo "        <center>\n";
	    foreach ($this->_photos as $photo)
		  echo "          <a href=\"" . WEB_PATH . "/photo-gallery/" . $this->_handle . "/" . 
			$photo["HANDLE"] . "/\" class=\"no_underline\"><img class=\"galleryPhotoIcon\" src=\"" .
			WEB_PATH . "/images/photos/" . $photo["ICON"] . "\" border=\"0\" /></a>\n";
		echo "        </center>\n";
	  }
	  echo "      </div>\n";
	  
	  echo "      <div style=\"clear:both;\"></div>\n";
	  
	  if (ADMIN_VIEW)
	  {
	    echo "<div class=\"adminViewBox\">\n";
	    echo "<b>Gallery Identity:</b> " . $this->_identity . "<br />\n";
		echo "<b>Staff Identity:</b> " . ($this->_staffIdentity != null ? $this->_staffIdentity : "(collaborative)") . "<br />\n";
		if ($this->_selectedPhoto != null)
		  echo "<b>Photo Identity:</b> " . $this->_selectedPhoto["IDENTITY"] . "<br />\n";
	    echo "<span class=\"action\" onClick=\"executeAJAX('" . WEB_PATH . "/manage/admin-master-published-remote-toggle.php?type=photo_gallery&identity=" . $this->_identity . "'," .
		"function evaluate(value) { if (value == 'published') alert('Gallery published'); else if (value == 'unpublished') alert('Gallery unpublished'); else alert(value); " .
		"window.location = '" . WEB_PATH . "/gallery/" . $this->_handle . "/'; });\">" .
		($this->_published ? "Unpublish" : "Publish") . "</span>\n";
	    echo "</div>\n";
	  }
	}
}
?>