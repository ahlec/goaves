<?php
class GoAvesSpecials {
	private $_activeSpecials = array();
	private $_endedSpecials = array();
	function loadTopics($path, $database)
	{
	  $get_specials = $database->query("SELECT handle, title, short_description, active, ended, listing_image, listing_style FROM specials ORDER BY active DESC, listing_style, title ASC");
	  while ($special = $get_specials->fetchArray())
	  {
	    $special_array = array("HANDLE" => $special["handle"], "TITLE" => format_content($special["title"]), "ACTIVE" => ($special["active"] == "TRUE"),
			"ENDED" => ($special["active"] == "TRUE" ? null : strtotime($special["ended"])), "DESCRIPTION" => format_content($special["short_description"]),
			"IMAGE" => $special["listing_image"], "STYLE" => strtolower($special["listing_style"]));
		if ($special_array["ACTIVE"])
		  $this->_activeSpecials[] = $special_array;
		else
		  $this->_endedSpecials[] = $special_array;
	  }
	}
	function checkOrRedirect($path,$database)
	{
	  $this->loadTopics($path, $database);
	  return true;
	}
	function getMainTab() { return "specials"; }
	function getPageHandle() { return "specials"; }
	function getPageSubhandle() { return null; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "Specials"); }
	function getPageTitle() { return "Specials"; }
	function getPageContents()
	{ 
	  echo "      <div class=\"specialsContainer\">\n";
	  echo "        <div class=\"endedsContainer\">\n";
	  echo "          <div class=\"header\">Past Specials</div>\n";
	  foreach ($this->_endedSpecials as $index => $ended)
	  {
	    echo "          <a href=\"" . WEB_PATH . "/special/" . $ended["HANDLE"] . "/\" class=\"special" . ($index < sizeof($this->_endedSpecials) - 1 ? "" : " last") .
			"\">" . $ended["TITLE"] . "</a>\n";
	  }
	  echo "        </div>\n";
	  echo "        <div class=\"activesContainer\">\n";
	  foreach ($this->_activeSpecials as $special)
	  {
	    echo "          <a href=\"" . WEB_PATH . "/special/" . $special["HANDLE"] . "/\" class=\"special " . $special["STYLE"] . "\">\n";
		echo "            <div class=\"title\"><span>" . $special["TITLE"] . "</span></div>\n";
		echo "            <img src=\"" . WEB_PATH . "/images/specials/" . $special["IMAGE"] . "\" class=\"banner\" border=\"0\" />\n";
		echo "            <div class=\"description\">" . $special["DESCRIPTION"] . "</div>\n";
		echo "          </a>\n";
	  }
	  echo "        </div>\n";
	  echo "        <div style=\"clear:both;\"></div>\n";
	  echo "      </div>\n";
	}
}
?>