<?php
class GoAvesYearbook {
	private $_pageToLoad;
	private $_title;
	private $_faq = array();
	private $_confirmation = array();
	function loadYearbook($path, $database)
	{
	  if (isset($path[1]) && $path[1] == "confirmation")
	  {
	    $this->_pageToLoad = "confirmation";
	    $confirmations = $database->query("SELECT customer_name, quantity, date_purchased, grade FROM yearbook_confirmations ORDER BY " .
			"customer_name ASC");
		while ($confirmation = $confirmations->fetchArray())
		  $this->_confirmation[] = array("NAME" => $confirmation["customer_name"], "QUANTITY" => $confirmation["quantity"],
			"DATE_PURCHASED" => $confirmation["date_purchased"], "GRADE" => $confirmation["grade"]);
	  } else
	  {
	    $this->_pageToLoad = "faq";
		$questions = $database->query("SELECT question, answer, requires_webroot FROM yearbook_faq_questions ORDER BY question_identity");
		while ($question = $questions->fetchArray())
		  $this->_faq[] = array("QUESTION" => $question["question"], "ANSWER" => ($question["requires_webroot"] ? 
			preg_replace("[\{WEB_PATH\}]", WEB_PATH, $question["answer"]) : $question["answer"]));
	  }
	  $this->_title = ($this->_pageToLoad == "faq" ? "Yearbook FAQ" : "Confirmation List");
	}
	function checkOrRedirect($path, $database)
	{
	  $this->loadYearbook($path, $database);
	  return true;
	}
	function getMainTab() { return "home"; }
	function getPageHandle() { return "yearbook"; }
	function getPageSubhandle() { return null; }
	function getBreadTrail() { return ($this->_pageToLoad === "confirmation" ? array("home" => "GoAves.com", "yearbook" => "Yearbook FAQ",
		"[this]" => "Confirmation List") : array("home" => "GoAves.com", "[this]" => "Yearbook FAQ")); }
	function getPageTitle() { return $this->_title; }
	function getPageContents()
	{
	  if ($this->_pageToLoad == "faq")
	  {
	    echo "      <div class=\"yearbookHeader\">\n";
	    echo "        <div class=\"header\">Yearbook</div>\n";
	    echo "        <div class=\"subheader\">Questions with Mrs. Cheralyn Jardine, Yearbook Advisor</div>\n";
		echo "        <div class=\"navigation\">.: <b>Question and Answer</b> &ndash; <a href=\"" . WEB_PATH . "/yearbook/confirmation/\">" .
			"Confirmation Listings</a> &ndash; <a href=\"" . WEB_PATH . "/documents/yearbook-order-form.htm\" target=\"_blank\">Order Form</a> :.</div>\n";
		echo "      </div>\n";
		echo "      <div class=\"yearbookContainer\">\n";
		
		foreach ($this->_faq as $question)
		{
		  echo "        <div class=\"entry\">\n";
		  echo "          <div class=\"question\"><b>Q:</b> " . $question["QUESTION"] . "</div>\n";
		  echo "          <div class=\"answer\"><b>A:</b> " . $question["ANSWER"] . "</div>\n";
		  echo "        </div>\n";
		}
		echo "      </div>\n";
	  } else
	  {
	    echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/yearbook/\">&laquo; Return to Yearbook Home</a></div>\n";
	    echo "      <div class=\"yearbookHeader\">\n";
		echo "        <div class=\"header\">Yearbook</div>\n";
		echo "        <div class=\"subheader\">Confirmation Listing</div>\n";
		echo "        <div class=\"navigation\">.: <a href=\"" . WEB_PATH . "/yearbook/\">Question and Answer</a> &ndash; <b>" .
			"Confirmation Listings</b> &ndash; <a href=\"" . WEB_PATH . "/documents/yearbook-order-form.htm\" target=\"_blank\">Order Form</a> :.</div>\n";
		echo "      </div>\n";
		
		if (sizeof($this->_confirmation) > 0)
		{
		  echo "      <table class=\"yearbookConfirmations\">\n";
		  echo "        <tr class=\"header\"><td>Name</td><td>Quantity</td><td>Date Ordered</td></tr>\n";
		  foreach ($this->_confirmation as $order)
		  {
		    echo "      <tr class=\"row\" onMouseOver=\"this.className='rowHover';\" onMouseOut=\"this.className='row';\"><td>" .
			  $order["NAME"] . "</td><td>" .
			  $order["QUANTITY"] . "</td><td class=\"last\">" . ($order["DATE_PURCHASED"] != "" ? date(DATE_FORMAT, strtotime($order["DATE_PURCHASED"])) :
			  "Unlisted") . "</td></tr>\n";
		  }
		  echo "      </table>\n";
		} else
		  echo "      <center>No records have been posted yet. Check back soon!</center>\n";
	  }
	  
	}
}
?>