<?php
class GoAvesGroupListingOld {
	private $_numberItems;
	private $_currentPage;
	private $_numberPages;
	private $_specificSearch = null;
	private $_items = array();
	function loadGroups($path, $database)
	{
	  $page_param = 1;
	  if (isset($path[1]) && ($path[1] == "clubs" || $path[1] == "sports"))
	  {
	    $this->_specificSearch = $path[1];
		$page_param = 2;
	  }
	  $number_query = "SELECT count(*) FROM groups WHERE image_file <> '' AND image_file IS NOT null AND " .
		"icon_file <> '' AND icon_file IS NOT null";
	  if ($this->_specificSearch == "clubs")
	    $number_query .= " AND type='club'";
	  else if ($this->_specificSearch == "sports")
	    $number_query .= " AND type LIKE '%_sports'";
	  $this->_numberItems = $database->querySingle($number_query);
	  
	  $this->_numberPages = floor($this->_numberItems % GROUPS_PER_LISTING_PAGE == 0 ? $this->_numberItems / GROUPS_PER_LISTING_PAGE :
		$this->_numberItems / GROUPS_PER_LISTING_PAGE + 1);
	  if ($this->_numberPages < 1)
	    $this->_numberPages = 1;
	  $this->_currentPage = parseCurrentPage((isset($path[$page_param]) ? $path[$page_param] : ""), $this->_numberPages);
	  
	  $get_query = "SELECT handle, title, icon_file FROM groups WHERE image_file <> '' AND image_file IS NOT null " .
		"AND icon_file <> '' AND icon_file IS NOT null";
	  if ($this->_specificSearch == "clubs")
	    $get_query .= " AND type='club'";
	  else if ($this->_specificSearch == "sports")
	    $get_query .= " AND type LIKE '%_sport'";
	  $get_items = $database->query($get_query . " ORDER BY title ASC LIMIT " . GROUPS_PER_LISTING_PAGE .
		" OFFSET " . (($this->_currentPage - 1) * GROUPS_PER_LISTING_PAGE));
	  while ($item = $get_items->fetchArray())
	    $this->_items[] = array("HANDLE" => $item["handle"], "TITLE" => $item["title"], "ICON" => $item["icon_file"]);
	}
	function checkOrRedirect($path,$database)
	{
	  $this->loadGroups($path, $database);
	  return true;
	}
	function getMainTab() { return "group-listing"; }
	function getPageHandle() { return "group-listing"; }
	function getPageSubhandle() { return $this->_specificSearch; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "Group Listing"); }
	function getPageTitle() { return "All Groups"; }
	function getPageContents()
	{ 
	  generatePageNavigation("group-listing-old" . ($this->_specificSearch != null ? "/" . $this->_specificSearch : ""), $this->_currentPage, $this->_numberPages);
	  echo "      <center>\n";
	  foreach ($this->_items as $item)
	  {
		echo "        <div onMouseOver=\"this.className='listItemHover';\" onMouseOut=\"this.className='listItem';\" " .
			"onClick=\"window.location='" . WEB_PATH . "/group/" . $item["HANDLE"] . "/';\" class=\"listItem\">\n";
		echo "          <div>" . $item["TITLE"] . "</div>\n";
		echo "          <img src=\"" . WEB_PATH . "/images/groups/" . $item["ICON"] . "\" />\n";
		echo "        </div>\n";
	  }
	  echo "      </center>\n";
	  
	  generatePageNavigation("group-listing-old" . ($this->_specificSearch != null ? "/" . $this->_specificSearch : ""), $this->_currentPage, $this->_numberPages);
	}
}
?>