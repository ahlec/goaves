<?php
class GoAvesNewsOld {
	private $_storiesPerPage = 10;
	private $_numberStories;
	private $_currentPage;
	private $_numberPages;
	private $_stories = array();
	private $_requestedType = null;
	private $_requestedHandle = null;
	function loadStories($path, $database)
	{
	  if (isset($path[1]) && $database->querySingle("SELECT count(*) FROM beat_types WHERE handle LIKE '" .
		$database->escapeString($path[1]) . "'") > 0)
	  {
	    $this->_requestedType = $database->querySingle("SELECT type_identity FROM beat_types WHERE handle LIKE '" .
		$database->escapeString($path[1]) . "' LIMIT 1");
	    $this->_requestedHandle = $database->escapeString($path[1]);
	  }

	  $this->_numberStories = $database->querySingle("SELECT count(*) FROM beats WHERE published='TRUE'" . ($this->_requestedType !== null ?
		" AND type='" . $this->_requestedType . "'" : ""));
	  
	  $this->_numberPages = floor($this->_numberStories % $this->_storiesPerPage == 0 ? $this->_numberStories / $this->_storiesPerPage :
		$this->_numberStories / $this->_storiesPerPage + 1);
	  if ($this->_numberPages < 1)
	    $this->_numberPages = 1;
	  $this->_currentPage = parseCurrentPage(($this->_requestedType == null ? $path[1] : $path[2]), $this->_numberPages);
	  
	  $get_latest_beats = $database->query("SELECT beats.handle, beats.title, beats.post_time, beats.contents, beats.image_file, beats.image_alignment, " .
		"beats.image_caption, staff.first_name, staff.last_name, beat_types.title AS \"beat_type\" FROM beats JOIN staff ON beats.staff_identity = " .
		"staff.identity JOIN beat_types ON beats.type = beat_types.type_identity WHERE published='TRUE' " .
		($this->_requestedType === null ? "" : "AND type='" . $this->_requestedType . "' ") .
		"ORDER BY beats.post_time DESC LIMIT " . $this->_storiesPerPage . " OFFSET " . (($this->_currentPage - 1) * $this->_storiesPerPage));
	  while ($story = $get_latest_beats->fetchArray())
	    $this->_stories[] = array("HANDLE" => $story["handle"], "TITLE" => format_content($story["title"]), "POST_TIME" => strtotime($story["post_time"]),
			"IMAGE" => ($story["image_file"] != "" ? "images/articles/" . $story["image_file"] : "layout/no-image-article.jpg"),
			"AUTHOR_FIRST_NAME" => $story["first_name"], "AUTHOR_LAST_NAME" => $story["last_name"], "BEAT_TYPE" => $story["beat_type"]);
	}
	function checkOrRedirect($path,$database)
	{
	  $this->loadStories($path, $database);
	  return true;
	}
	function getMainTab() { return "news"; }
	function getPageHandle() { return "news"; }
	function getPageSubhandle() { return $this->_requestedHandle; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "Latest News"); }
	function getPageTitle() { return "Latest News"; }

	function getPageContents()
	{
	  generatePageNavigation("news-old" . ($this->_requestedType === null ? "" : "/" . $this->_requestedHandle), $this->_currentPage,
		$this->_numberPages);
	  foreach ($this->_stories as $story)
	  {
		echo "      <div class=\"articleListingContainer\" onMouseOver=\"this.className='articleListingContainerHover';\" " .
			"onMouseOut=\"this.className='articleListingContainer';\" onClick=\"window.location='" . WEB_PATH . "/article/" . $story["HANDLE"] . "/';\">\n";
		echo "        <div class=\"articleImage\"><img src=\"" . WEB_PATH . "/" . $story["IMAGE"] . "\" /></div>\n";
		echo "        <div class=\"title\">" . $story["TITLE"] . "</div>\n";
		echo "        <div class=\"subtitle\">" . date(LONG_DATETIME_FORMAT, $story["POST_TIME"]) . " &ndash; <b>" . $story["AUTHOR_FIRST_NAME"] . " " .
			$story["AUTHOR_LAST_NAME"] . "</b>" . ($this->_requestedType === null ? 
			($story["BEAT_TYPE"] != "Beat" ? " &ndash; " . $story["BEAT_TYPE"] : "") : "") . "</div>\n";
		echo "        <div style=\"clear:left;\"></div>\n";
		echo "      </div>\n";
	  }
	  generatePageNavigation("news-old" . ($this->_requestedType === null ? "" : "/" . $this->_requestedHandle), $this->_currentPage,
		$this->_numberPages);
	}
}
?>