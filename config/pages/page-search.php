<?php
class GoAvesSearch {
	private $_inputSearchQuery;
	private $_scope;
	private $_results = array();
	private $_stopWords = array("and", "the", "a", "an", "of", "or");
	function checkOrRedirect($path,$database)
	{
	  return true;
	}
	function loadSearch($searchQuery, $scope, $database)
	{
	  $this->_inputSearchQuery = $searchQuery;
	  $pieces = preg_split("/ /", $searchQuery);
	  $search_pieces = array();
	  foreach ($pieces as $piece)
	    if (!in_array($piece, $this->_stopWords))
		{
		  $piece_string = trim(preg_replace("/[%]/", "'%", $piece), " ");
		  if (preg_match("/[^a-zA-Z0-9\\\\%]/", $piece_string) !== 0)
		  {
		    $piece_string = $database->escapeString(preg_replace("/[^a-zA-Z0-9\\\\%]/", "%", $piece_string));
			if (preg_replace("%", "", $piece_string) !== "")
			  $search_pieces[] = $piece_string;
		    continue;
		  }
		  $search_pieces[] = $database->escapeString($piece_string);
		}
	  $search = $database->escapeString($searchQuery);
	  
	  $search_scope = ($scope != null ? mb_strtolower($scope) : "all");
	  $this->_scope = $search_scope;

	  if (sizeof($search_pieces) == 0)
	    return;
	    
	  if ($search_scope == "all" || $search_scope == "staff")
	  {
	    $staff_results = $database->query($this->createQuery("SELECT identity, first_name, last_name, positions, active, icon_file, year_graduating, display_on_staff_listing FROM staff",
			array("first_name", "last_name"), $search_pieces) . (!ADMIN_VIEW ? " AND display_on_staff_listing='TRUE'" : ""));
		while ($staff_result = $staff_results->fetchArray())
		{
		  $this->_results[] = array("IDENTITY" => $staff_result["identity"], "CONTENT_TYPE" => "staff", "PUBLISHED" => ($staff_result["display_on_staff_listing"] == "TRUE"),
			"PRIMARY_TITLE" => format_content($staff_result["first_name"] . " " . $staff_result["last_name"]), "SECONDARY_TITLE" => format_content($staff_result["positions"]),
			"ICON" => $staff_result["icon_file"], "HANDLE" => mb_strtolower($staff_result["first_name"] . "-" . $staff_result["last_name"]),
			"HITS" => $this->getNumberHits($staff_result, array("first_name", "last_name"), $search_pieces) * 900);
		}
	  }
	  $real_search_pieces = $search_pieces;
	  $search_pieces = implode("%", $search_pieces);
	  if ($search_scope == "all" || $search_scope == "beats")
	  {
	    $beat_results = $database->query($this->createQuery("SELECT beat_identity, staff.first_name, staff.last_name, beats.title, beats.handle, beats.post_time, " .
			"beats.contents, beats.image_file, beats.icon_file, beats.last_updated, beat_types.title AS \"type_title\", published FROM beats JOIN staff ON beats.staff_identity = " .
			"staff.identity JOIN beat_types ON beats.type = beat_types.type_identity", array("beats.title", "contents"), $search_pieces) . (!ADMIN_VIEW ? " AND published='TRUE'" : ""));
		while ($beat_result = $beat_results->fetchArray())
		{
		  $hits = $this->getNumberHits($beat_result, array("title", "contents"), $real_search_pieces) + $this->getNumberHits($beat_result, array("title", "contents"), $search) * 2;
		  if ($hits > 0)
		    $this->_results[] = array("IDENTITY" => $beat_result["beat_identity"], "CONTENT_TYPE" => "beat", "PUBLISHED" => ($beat_result["published"] == "TRUE"),
				"PRIMARY_TITLE" => format_content($beat_result["title"]), "SECONDARY_TITLE" => "<i>" . $beat_result["type_title"] . "</i> &ndash; Posted " .
				date(DATE_FORMAT, strtotime($beat_result["post_time"])), "ICON" => ($beat_result["icon_file"] != "" ? $beat_result["icon_file"] :
				$beat_result["image_file"]), "HANDLE" => $beat_result["handle"], "HITS" => $hits);
		}
	  }
	  if ($search_scope == "all" || $search_scope == "leaf")
	  {
	    $issue_results = $database->query($this->createQuery("SELECT issuu_identity, post_date, issue_name, handle, color_cover, number_pages FROM leaf_issuu",
			array("issue_name"), $search_pieces));
		while ($issue_result = $issue_results->fetchArray())
		  $this->_results[] = array("IDENTITY" => $issue_result["issuu_identity"], "CONTENT_TYPE" => "leaf_issue", "PUBLISHED" => true,
			"PRIMARY_TITLE" => format_content($issue_result["issue_name"]), "SECONDARY_TITLE" => date("F Y", strtotime($issue_result["post_date"]) . " issue"),
			"ICON" => $issue_result["color_cover"], "HANDLE" => $issue_result["handle"]);
		
		$leaf_item_results = $database->query($this->createQuery("SELECT item_identity, leaf_items.leaf_identity, page_number, item_title, subtitle, item_type, noteworthy, leaf_issuu.handle, " .
			"leaf_issuu.issue_name FROM leaf_items JOIN leaf_issuu ON leaf_items.leaf_identity = leaf_issuu.issuu_identity", array("item_title", "subtitle"), $search_pieces));
		while ($item_result = $leaf_item_results->fetchArray())
		{
		  $hits = $this->getNumberHits($item_result, array("item_title", "subtitle"), $real_search_pieces) * 8 +
			$this->getNumberHits($item_result, array("item_title", "subtitle"), $search) * 17;
		  if ($hits > 0)
		    $this->_results[] = array("IDENTITY" => $item_result["item_identity"], "CONTENT_TYPE" => "leaf_item", "PUBLISHED" => true,
				"PRIMARY_TITLE" => format_content($item_result["item_title"]), "SECONDARY_TITLE" => "On page <b>" . $item_result["page_number"] . "</b> of the <b>" .
				$item_result["issue_name"] . "</b>", "ICON" => null, "HANDLE" => $item_result["handle"] . "/" . $item_result["page_number"], "HITS" => $hits);
		}
	  }
	  if ($search_scope == "all" || $search_scope == "multimedia" || $search_scope == "galleries")
	  {
	    $gallery_results = $database->query($this->createQuery("SELECT gallery_identity, handle, title, gallery_image, number_photos, published FROM photo_galleries", array("title"),
			$search_pieces) . (!ADMIN_VIEW ? " AND (published == 'TRUE')" : ""));
		while ($gallery_result = $gallery_results->fetchArray())
		{
		  $hits = $this->getNumberHits($gallery_result, array("title"), $real_search_pieces) * 5 +
			$this->getNumberHits($gallery_result, array("title"), $search) * 10;
		  if ($hits > 0)
		    $this->_results[] = array("IDENTITY" => $gallery_result["gallery_identity"], "CONTENT_TYPE" => "photo_gallery", "PUBLISHED" => ($gallery_result["published"] == "TRUE"),
				"PRIMARY_TITLE" => format_content($gallery_result["title"]), "SECONDARY_TITLE" => "<b>" . $gallery_result["number_photos"] . "</b> photo" .
				($gallery_result["number_photos"] != 1 ? "s" : ""), "ICON" => $gallery_result["gallery_image"], "HANDLE" => $gallery_result["handle"], "HITS" => $hits);
		}
			
		$photo_results = $database->query($this->createQuery("SELECT picture_identity, photos.handle, photos.title, photos.icon_file, photo_galleries.published, caption_text, " .
			"sort_order, photo_galleries.title AS \"gallery_title\", photo_galleries.handle AS \"gallery_handle\" FROM photos JOIN photo_galleries ON (photos.title IS NOT null" .
			(!ADMIN_VIEW ? " AND photo_galleries.published='TRUE'" : "") . 
			" AND photos.gallery_identity = photo_galleries.gallery_identity)", array("photos.title", "caption_text"), $search_pieces));
		while ($photo_result = $photo_results->fetchArray())
		  $this->_results[] = array("IDENTITY" => $photo_result["picture_identity"], "CONTENT_TYPE" => "gallery_photo", "PUBLISHED" => ($photo_result["published"] == "TRUE"),
			"PRIMARY_TITLE" => format_content($photo_result["title"]), "SECONDARY_TITLE" => "From the gallery <b>" . format_content($photo_result["gallery_title"]) . "</b>",
			"ICON" => $photo_result["icon_file"], "HANDLE" => $photo_result["gallery_handle"] . "/" . $photo_result["handle"]);
	  }
	  if ($search_scope == "all" || $search_scope == "multimedia" || $search_scope == "videos")
	  {
	    $video_results = $database->query($this->createQuery("SELECT video_identity, handle, title, date_published, published, captions, image_thumbnail FROM videos",
			array("title", "captions"), $search_pieces) . (!ADMIN_VIEW ? " AND published='TRUE'" : ""));
		while ($video_result = $video_results->fetchArray())
		  $this->_results[] = array("IDENTITY" => $video_result["video_identity"], "CONTENT_TYPE" => "video", "PUBLISHED" => ($video_result["published"] == "TRUE"),
			"PRIMARY_TITLE" => format_content($video_result["title"]), "SECONDARY_TITLE" => null, "ICON" => $video_result["thumbnail_image"],
			"HANDLE" => $video_result["handle"]);
	  }
	  if ($search_scope == "all" || $search_scope == "multimedia" || $search_scope == "comics")
	  {
	    $comic_results = $database->query($this->createQuery("SELECT identity, image_file, icon_file, handle, title, date_posted, published FROM cartoons",
			array("title"), $search_pieces) . (!ADMIN_VIEW ? " AND published='TRUE'" : ""));
		while ($comic_result = $comic_results->fetchArray())
		{
		  $hits = $this->getNumberHits($comic_result, array("title"), $real_search_pieces) + $this->getNumberHits($comic_result, array("title"), $search) * 2;
		  if ($hits > 0)
		    $this->_results[] = array("IDENTITY" => $comic_result["identity"], "CONTENT_TYPE" => "comic", "PUBLISHED" => ($comic_result["published"] == "TRUE"),
				"PRIMARY_TITLE" => format_content($comic_result["title"]), "SECONDARY_TITLE" => null, "ICON" => ($comic_result["icon_file"] != "" ? $comic_result["icon_file"] :
				$comic_result["image_file"]), "HANDLE" => $comic_result["handle"], "HITS" => $hits);
		}
	  }
	  if ($search_scope == "all" || $search_scope == "multimedia" || $search_scope == "soundslides")
	  {
	    $soundslide_results = $database->query($this->createQuery("SELECT slideshow_identity, handle, title, published, post_date, thumbnail_image, date_last_updated FROM sound_slides",
			array("title"), $search_pieces) . (!ADMIN_VIEW ? " AND published='TRUE'" : ""));
		while ($soundslide_result = $soundslide_results->fetchArray())
		{
		  $hits = $this->getNumberHits($soundslide_result, array("title"), $real_search_pieces) * 4 + $this->getNumberHits($soundslide_result, array("title"), $search) * 5;
		  if ($hits > 0)
		    $this->_results[] = array("IDENTITY" => $soundslide_result["slideshow_identity"], "CONTENT_TYPE" => "soundslide", "PUBLISHED" => ($soundslide_result["published"] == "TRUE"),
				"PRIMARY_TITLE" => format_content($soundslide_result["title"]), "SECONDARY_TITLE" => null, "ICON" => $soundslide_result["thumbnail_image"],
				"HANDLE" => $soundslide_result["handle"], "HITS" => $hits);
		}
	  }
	  if ($search_scope == "all" || $search_scope == "multimedia" || $search_scope == "podcasts")
	  {
	    $podcast_results = $database->query($this->createQuery("SELECT podcast_identity, handle, title, date_posted, published, icon_file, description, transcript, date_last_updated " .
			"FROM podcasts", array("title", "description", "transcript"), $search_pieces) . (!ADMIN_VIEW ? " AND published='TRUE'" : ""));
		while ($podcast_result = $podcast_results->fetchArray())
		  $this->_results[] = array("IDENTITY" => $podcast_result["podcast_identity"], "CONTENT_TYPE" => "podcast", "PUBLISHED" => ($podcast_result["published"] == "TRUE"),
			"PRIMARY_TITLE" => format_content($podcast_result["title"]), "SECONDARY_TITLE" => null, "ICON" => $podcast_result["icon_file"], "HANDLE" => $podcast_result["handle"]);
	  }
	  if ($search_scope == "all" || $search_scope == "groups" || $search_scope == "sports")
	  {
	    $group_results = $database->query($this->createQuery("SELECT group_identity, groups.handle, title, icon_file, group_types.singular_name, group_types.handle AS \"type_handle\", " .
			"active FROM groups JOIN group_types ON (groups.type = group_types.type_identity" . ($search_scope != "all" ? " AND group_types.handle " .
			($search_scope == "groups" ? "NOT " : "") . "LIKE \"%_sport\"" : "") . ")", array("title"), $search_pieces));
		while ($group_result = $group_results->fetchArray())
		{
		  $hits = $this->getNumberHits($group_result, array("title"), $real_search_pieces) * 10 + $this->getNumberHits($group_result, array("title"), $search) * 20;
		  $this->_results[] = array("IDENTITY" => $group_result["group_identity"], "CONTENT_TYPE" => (strpos($group_result["type_handle"], "_sport") === FALSE ? "group" : "sport"), "PUBLISHED" => true, "PRIMARY_TITLE" => format_content($group_result["title"]),
			"SECONDARY_TITLE" => format_content($group_result["singular_name"]), "ICON" => $group_result["icon_file"], "HANDLE" => $group_result["handle"], "HITS" => $hits);
		}

		/*$subgroup_results = $database->query($this->createQuery("SELECT subgroup_identity, groups.handle, group_subgroups.name, groups.icon_file, group_types.singular_name, " .
			"groups.active FROM group_subgroups JOIN groups ON group_subgroups.group_identity = groups.group_identity JOIN group_types ON " .
			"(groups.type = group_types.type_identity AND group_types.handle NOT LIKE \"%_sport\")", array("group_subgroups.name"), $search_pieces));
		while ($subgroup_result = $subgroup_results->fetchArray())
		  $this->_results[] = array("IDENTITY" => $subgroup_result["subgroup_identity"], "CONTENT_TYPE" => "subgroup", "PUBLISHED" => true,
			"PRIMARY_TITLE" => format_content($subgroup_result["name"]), "SECONDARY_TITLE" => format_content($subgroup_result["singular_name"]), "ICON" => $subgroup_result["icon_file"],
			"HANDLE" => $subgroup_result["handle"]);*/
	  }
	  usort($this->_results, array("GoAvesSearch", "sortSearchResults"));
	}
	static function sortSearchResults($resultA, $resultB)
	{
	  if ($resultA["HITS"] === $resultB["HITS"])
	    return 0;
	  return ($resultA["HITS"] < $resultB["HITS"] ? 1 : -1);
	}
	function createQuery($base_query, $search_columns, $search_pieces)
	{
	  $query = $base_query . " WHERE";
	  
	  if (!is_array($search_pieces))
	  {
		foreach ($search_columns as $column_name)
		{
			$exact_match = (substr($column_name, 0, 1) == "=");
			if ($exact_match)
				$column_name = substr($column_name, 1);
			$query .= " " . $column_name . ($exact_match ? "='" . $search_pieces . "'" : " LIKE '%" . $search_pieces . "%'") . " OR";
		}
		return rtrim($query, " OR") . "";
	  }
	  
	  foreach ($search_pieces as $column_name => $piece)
	  {
	    if (is_int($column_name))
		{
	      foreach ($search_columns as $column)
		  {
		    $exact_match = (substr($column, 0, 1) == "=");
			if ($exact_match)
			  $column = substr($column, 1);
		    $query .= " " . $column . ($exact_match ? "='" . $piece . "'" : " LIKE '%" . $piece . "%'") . " OR";
		  }
		} else
		{
		  $exact_match = (substr($column_name, 0, 1) == "=");
		  if ($exact_match)
		    $column_name = substr($column_name, 1);
		  if (is_array($piece))
		  {
		    foreach ($piece as $piece_piece)
			  $query .= " " . $column_name . ($exact_match ? "='" . $piece_piece . "'" : " LIKE '%" . $piece_piece . "%'") .
				" OR";
		  } else
		    $query .= " " . $column_name . ($exact_match ? "='" . $piece . "'" : " LIKE '%" . $piece . "%'") . " OR";
		}
      }
	  $query = rtrim($query, " OR");
	  return $query;
    }
	function getNumberHits($sql_results, $columns, $search_pieces)
	{
	  $hits = 0;
	  if (!is_array($search_pieces))
	    $search_pieces = array($search_pieces);
	  foreach ($search_pieces as $search_piece)
	    foreach ($columns as $column)
		{
		  $out = array();
		  //echo $search_piece . ", " . format_content($sql_results[$column]) . "<br /><br />\n";
		  //echo preg_replace("/<a(.*)>(.*)<\\/a>/ie", "\\2", format_content($sql_results[$column])) . "<br />\n";
		  $hits += preg_match_all("/" . $search_piece . "/i", strip_tags(preg_replace("/<a(.*)>(.*)<\\/a>/", "", format_content($sql_results[$column]))), &$out);
		}
	  return $hits;
	}
	function getMainTab() { return "goaves"; }
	function getPageHandle() { return "search"; }
	function getPageSubhandle() { return ($this->_specificMediaType != null ? $this->_specificMediaType["PAGE_HANDLE"] : null); }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "All Multimedia"); }
	function getPageTitle() { return "Search for '" . $this->_inputSearchQuery . "'"; }
	function getPageContents()
	{
	  echo "<div class=\"searchResultsContainer\">\n";
	  echo "  <div class=\"headerLine\"><b>" . sizeof($this->_results) . "</b> result" . (sizeof($this->_results) != 1 ? "s were" : " was") . " found for the phrase '<b>" .
	  	$this->_inputSearchQuery . "</b>'" . ($this->_scope != "all" ? " in <b>" . $this->_scope . "</b>" : " on GoAves.com") . ".</div>\n";
	  if (sizeof($this->_results) > 0)
	  {
	    foreach ($this->_results as $result)
	    {
	      if (!ADMIN_VIEW && !$result["PUBLISHED"])
	        continue;
	      if ($result["HITS"] === 0)
		    continue;
		  echo "  <a href=\"" . WEB_PATH . "/";
	      switch ($result["CONTENT_TYPE"])
	      {
	        case "staff": echo "staff-portfolio"; break;
	        case "beat": echo "article"; break;
	        case "leaf_issue": echo "leaf"; break;
	        case "leaf_item": echo "leaf-issue"; break;
	        case "gallery_photo":
	        case "photo_gallery": echo "photo-gallery"; break;
	        case "video": echo "video"; break;
	        case "comic": echo "comic"; break;
	        case "soundslide": echo "sound-slides"; break;
	        case "podcast": echo "podcast"; break;
	        case "group":
	        case "sport": echo "group"; break;
	      }
	      echo "/" . $result["HANDLE"] . "/\" target=\"_blank\" class=\"resultContainer" . (!$result["PUBLISHED"] ? " unpublishedResult" : "") . "\">\n";
	      echo "    <div class=\"type\">\n";
	      echo "      <img src=\"" . WEB_PATH . "/images/icons/result-type-" . $result["CONTENT_TYPE"] . ".png\" border=\"0\" /><br />\n";
	      switch ($result["CONTENT_TYPE"])
	      {
	        case "staff": echo "Staff"; break;
	        case "beat": echo "Article"; break;
	        case "leaf_issue": echo "<i>Leaf</i> issue"; break;
	        case "leaf_item": echo "<i>Leaf</i> article"; break;
	        case "gallery_photo": echo "Photo"; break;
	        case "photo_gallery": echo "Photo Gallery"; break;
	        case "video": echo "Video"; break;
	        case "comic": echo "Comic"; break;
	        case "soundslide": echo "Soundslide"; break;
	        case "podcast": echo "Podcast"; break;
	        case "group": echo "Group"; break;
	        case "sport": echo "Sport"; break;
	      }
	      echo "    </div>\n";
	      echo "    <img src=\"" . WEB_PATH . "/images/";
	      switch ($result["CONTENT_TYPE"])
	      {
	        case "staff": echo "staff"; break;
	        case "beat": echo "articles"; break;
	        case "leaf_issue":
	        case "leaf_item": echo "leaf_covers"; break;
	        case "gallery_photo":
	        case "photo_gallery": echo "photos"; break;
	        case "video": echo "video_thumbnails"; break;
	        case "comic": echo "cartoons"; break;
	        case "soundslide": echo "sound_slides"; break;
	        case "podcast": echo "podcasts"; break;
	        case "group":
	        case "sport": echo "groups"; break;
	      }
	      echo "/" . $result["ICON"] . "\" class=\"icon\" border=\"0\" />\n";
	      echo "    <div class=\"primaryTitle\">" . $result["PRIMARY_TITLE"] . (!$result["PUBLISHED"] ? " <span>(Unpublished)</span>" : "") . 
			(ADMIN_VIEW ? " (HITS: " . $result["HITS"] . ")" : "") . "</div>\n";
	      echo "    <div class=\"secondaryTitle\">" . $result["SECONDARY_TITLE"] . "</div>\n";
	      echo "    <div style=\"clear:both;\"></div>\n";
	      echo "  </a>\n";
	    }
	  } else
	  {
	    echo "no results";
	  }
	}
}
?>