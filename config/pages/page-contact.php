<?php
class GoAvesContact {
	private $_contacts = array();
	function loadContacts($database)
	{
	  $get_contacts = $database->query("SELECT contact_information.name, contact_information.reason, contact_information.email, groups.title, " .
		"groups.icon_file FROM contact_information JOIN groups ON contact_information.group_identity = groups.group_identity ORDER BY " .
		"groups.group_identity ASC, name ASC");
	  while ($contact = $get_contacts->fetchArray())
	    $this->_contacts[] = array("NAME" => $contact["name"], "REASON" => $contact["reason"], "EMAIL" => $contact["email"], 
			"GROUP_TITLE" => $contact["title"], "ICON_FILE" => $contact["icon_file"]);
	}
	function checkOrRedirect($path, $database)
	{
	  $this->loadContacts($database);
	  return true;
	}
	function getMainTab() { return "home"; }
	function getPageHandle() { return "contact"; }
	function getPageSubhandle() { return null; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "Contact Information"); }
	function getPageTitle() { return "Contact Information"; }
	function getPageContents()
	{
	  $previous_group = null;
	  echo "      <div class=\"contactContainer\">\n";
	  foreach ($this->_contacts as $contact)
	  {
	    if ($previous_group == null || $contact["GROUP_TITLE"] != $previous_group)
		{
		  if ($previous_group != null)
		  {
		    echo "          <div style=\"clear:both;\"></div>\n";
			echo "        </div>\n";
		  }
		  $previous_group = $contact["GROUP_TITLE"];
		  echo "        <div class=\"group\">\n";
		  echo "          <img src=\"" . WEB_PATH . "/images/groups/" . $contact["ICON_FILE"] . "\" class=\"icon\" />\n";
		  echo "          <div class=\"title\">" . $contact["GROUP_TITLE"] . "</div>\n";
		}
		echo "          <div class=\"entry\">\n";
		echo "            <div class=\"name\">" . $contact["NAME"] . "</div>\n";
		echo "            <div class=\"reason\">" . $contact["REASON"] . "</div>\n";
		echo "            <a href=\"mailto:" . $contact["EMAIL"] . "\">" . $contact["EMAIL"] . "</a>\n";
		echo "          </div>\n";
	  }
		    echo "          <div style=\"clear:both;\"></div>\n";
			echo "        </div>\n";
	  echo "      </div>\n";
	}
}
?>