<?php
$manage_web_path = WEB_PATH . "/new-manage";
if (strpos($_SERVER["SERVER_NAME"], "www.") === false) // Otherwise, AJAX gives us authorization errors
  $manage_web_path = str_replace("www.", "", $manage_web_path);
define("MANAGE_WEB_PATH", $manage_web_path);
define("MANAGE_DOCUMENT_ROOT", DOCUMENT_ROOT . "/new-manage");
define("MANAGE_THEME", "horizons-management");
define("MANAGE_TRANSFER_DATA", "goavesManagePageTransferDataSession");
define("SESSION_PREFIX", "goavesManage");
define("MESSAGE_MCRYPT_KEY", "goavesHORIzONSK3Y");
define("STORAGE_DOCUMENT_ROOT", "F:/goaves_stored_files");

define("PERM_BEATS", 0);
define("PERM_GALLERIES", 1);
define("PERM_PODCASTS", 2);
define("PERM_VIDEOS", 3);
define("PERM_COMICS", 4);
define("PERM_SOUNDSLIDES", 5);
define("PERM_SPORTS", 6);
define("PERM_GROUPS", 7);
define("PERM_INTERACTIONS", 8);
define("PERM_ADMIN", 9);
define("PERM_SPECIALS", 10);

define("PERM_INFOGRAPHICS", 11);
define("PERM_LEAF", 12);
define("PERM_YEARBOOK", 13);

define("AUTH_NONE", 0);
define("AUTH_CONTRIB", 1);
define("AUTH_MODERATOR", 2);

$leaf_item_types = array("article" => "Article", "calendar" => "Calendar", "snapshots" => "Snapshots", "column" => "Column", "comic" => "Comic",
	"enterprise" => "Enterprise", "q_a" => "Q&A");

function UpdateContentStatistic()
{
  $db = new DeitloffDatabase(DATABASE_PATH);
  $db->exec("UPDATE statistics SET value='" . date("Y-m-d H:i:s") . "' WHERE stat_handle='content_last_updated'");
}
function isModeratorOver($permission_index)
{
  return (substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], $permission_index - 1, 1) === "2");
}
function encodeMailMessage($message)
{
	$td = mcrypt_module_open('tripledes', '', 'ecb', '');
	$iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	mcrypt_generic_init($td, MESSAGE_MCRYPT_KEY, $iv);
	$encrypted_data = mcrypt_generic($td, $message);
	mcrypt_generic_deinit($td);
	mcrypt_module_close($td); 
	return $encrypted_data;
}
function decodeMailMessage($message)
{
  if ($message == null || $message == "")
    return "";
	$td = mcrypt_module_open('tripledes', '', 'ecb', '');
	$iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	mcrypt_generic_init($td, MESSAGE_MCRYPT_KEY, $iv);
	$encrypted_data = mdecrypt_generic($td, $message);
	mcrypt_generic_deinit($td);
	mcrypt_module_close($td); 
	return $encrypted_data;
}
?>