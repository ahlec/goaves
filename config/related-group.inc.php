<?php
class GroupInformation
{
  private $_groupHandle;
  private $_groupTitle;
  private $_groupIdentity;
  private $_content = array();
  function __construct($handle, $title, $identity, $icon)
  {
    $this->_groupHandle = $handle;
	$this->_groupTitle = $title;
	$this->_groupIdentity = $identity;
	$this->_groupIcon = $icon;
  }
  function addContent($handle, $title, $type)
  {
    $this->_content[] = array("HANDLE" => $handle, "TITLE" => $title, "TYPE" => $type);
  }
  function getTitle() { return $this->_groupTitle; }
  function getHandle() { return $this->_groupHandle; }
  function getContent() { return $this->_content; }
  function getIcon() { return $this->_groupIcon; }
}

function getGroupInformation($database, $group_identity, $post_identity, $post_type)
{
  if ($group_identity == "" || $group_identity == null)
    return null;
  $get_group_information = $database->querySingle("SELECT group_identity, handle, title, icon_file FROM groups WHERE group_identity='" .
	$database->escapeString($group_identity) . "' LIMIT 1", true);
  $group_information = new GroupInformation($get_group_information["handle"], $get_group_information["title"], $group_identity, $get_group_information["icon_file"]);
  
  $related_content = $database->query("SELECT title, handle, type FROM group_content WHERE ((type='" . $post_type . "' AND identity<>'" .
	$post_identity . "') OR (type<>'" . $post_type . "')) AND related_group='" . $group_identity . "' ORDER BY datestamp DESC LIMIT " .
	RELATED_CONTENT_TO_DISPLAY);
  while ($related = $related_content->fetchArray())
	$group_information->addContent($related["handle"], format_content($related["title"]), $related["type"]);
  return $group_information;
}

function outputGroupInformation($group_information)
{
    echo "        <div class=\"sidePanelSectionContainer\">\n";
	echo "          <a href=\"" . WEB_PATH . "/group/" . $group_information->getHandle() . "/\"><img src=\"" . WEB_PATH . "/images/groups/" . $group_information->getIcon() . 
		"\" class=\"icon\" border=\"0\" /></a>\n";
	echo "          <div class=\"super\">More content on the</div>\n";
	echo "          <div class=\"header\"><a href=\"" . WEB_PATH . "/group/" . $group_information->getHandle() . "/\">" . $group_information->getTitle() . "</a></div>\n";
	echo "          <div style=\"clear:left;\"></div>\n";
	if (sizeof($group_information->getContent()) > 0)
	{
	  foreach ($group_information->getContent() as $content)
	  {
	    echo "          <a href=\"" . WEB_PATH . "/";
		switch ($content["TYPE"])
		{
		  case "cartoon": echo "comic"; break;
		  case "video": echo "video"; break;
		  case "photo_gallery": echo "gallery"; break;
		  case "sound_clip": echo "audio"; break;
		  case "sound_slides": echo "sound-slides"; break;
		  case "article": echo "article"; break;
		}
		echo "/" . $content["HANDLE"] ."/\"><div class=\"item\"><img src=\"" . WEB_PATH . "/images/icons/";
		switch ($content["TYPE"])
	    {
	      case "cartoon": echo "icon-comic.png"; break;
	      case "video": echo "icon-video.png"; break;
	      case "photo_gallery": echo "icon-photo-gallery.png"; break;
	      case "sound_clip": echo "icon-audio.png"; break;
	      case "sound_slides": echo "icon-sound-slides.png"; break;
		  case "article": echo "icon-article.png"; break;
	    }
		$clipped_title = get_smart_blurb($content["TITLE"], 5);
		echo "\" border=\"0\" /><span>" . $clipped_title . ($clipped_title != $content["TITLE"] ? "..." : "") . "</span></div></a>\n";
	  }
	} else
	  echo "          <div class=\"text\">There is no more posted content on this group.</div>\n";
	echo "        </div>\n";
}
?>