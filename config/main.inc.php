<?php

error_reporting(E_ALL);
ini_set("display_errors", "1");

ini_set('default_charset', 'UTF-8');
define("WEB_PATH", "http://www.deitloff.com/goaves");
define("DOCUMENT_ROOT", "/home/deitloff/public_html/goaves");
require_once (DOCUMENT_ROOT . "/config/database.inc.php");
define("DATABASE_PATH", DOCUMENT_ROOT . "/config/goaves.db");
define("DATE_FORMAT", "j F Y");
define("LONG_DATE_FORMAT", "l, j F Y");
define("LONG_DATETIME_FORMAT", "l, j F Y \a\\t g:i A");
define("DATETIME_FORMAT", "j F Y \a\\t g:i A");
define("SHORT_DATE_FORMAT", "j M");
define("TIME_FORMAT", "g:i A");
define("DEBUG", false);
define("CURRENT_THEME", "professional");
define("DEVELOPMENTAL_THEME", "");//seasonal-2010");
define("PAGE_NAVIGATION_SESSION", "goavesNavigationSession");
define("VISITOR_SESSION", "goavesVisitorSession");

define("POLL_VOTING_SESSION", "goavesPollVoting");
define("DATES_ON_FRONT_PAGE", 10);

define("GROUPS_PER_LISTING_PAGE", 18);
define("STAFF_PER_LISTING_PAGE", 18);
define("INTERACTIONS_PER_LISTING_PAGE", 18);

define("FEED_ITEMS_DISPLAY", 20);

define("MANAGE_SESSION", "goavesManageSession");
define("MANAGE_PERMISSIONS_SESSION", "goavesManagePermissions");

define("RELATED_CONTENT_TO_DISPLAY", 10);
define("MAJOR_STORIES_TO_DISPLAY", 5);
define("FEATURED_ARTICLE_TYPES_ARTICLES_TO_DISPLAY", 7);

define("DEFAULT_PASSWORD", "c21f969b5f03d33d43e04f8f136e7682");
require_once (DOCUMENT_ROOT . "/config/related-group.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions.inc.php");
require_once (DOCUMENT_ROOT . "/config/dates-times.inc.php");
require_once (DOCUMENT_ROOT . "/config/errors.inc.php");
ini_set("session.cookie_domain", WEB_PATH);
?>
