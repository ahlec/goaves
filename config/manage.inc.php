<?php
require_once("main.inc.php");
define("UPLOAD_PATH", "C:/xampplite/tmp");
define("LATEST_CHANGE_THRESHOLD", "-3 days");
$_permission_definition = array(1 => "Articles", 2 => "Photos", 3 => "Audio", 4 => "Videos", 5 => "Comics", 6 => "Sports", 7 => "Polls", 8 => "Groups", 9 => "Admin",
	10 => "Sound Slides");
ini_set('default_charset', 'UTF-8');

function processManageSession()
{
  session_start();
  if (!isset($_SESSION[MANAGE_SESSION]))
    header("Location: " . WEB_PATH . "/manage/login.php");
  $database = new DeitloffDatabase(DATABASE_PATH);
  if ($database->querySingle("SELECT password FROM staff WHERE identity='" . $_SESSION[MANAGE_SESSION] . "' LIMIT 1") == DEFAULT_PASSWORD)
    if (substr($_SERVER["SCRIPT_NAME"], strrpos($_SERVER["SCRIPT_NAME"],"/") + 1) != "change-password.php")
      header("Location: " . WEB_PATH . "/manage/change-password.php?must_change=true");
  $permissions = $database->querySingle("SELECT permissions FROM staff WHERE identity='" . $_SESSION[MANAGE_SESSION] . "' LIMIT 1");
  if ($permissions !== false)
	$_SESSION[MANAGE_PERMISSIONS_SESSION] = preg_split('//', $permissions);
  
}
function outputManageHeader($page_title, $suppress_navigation = false, $onload = "")
{
  echo "<html>\n";
  echo "  <head>\n";
  echo "    <title>" . $page_title . "</title>\n";
  echo "    <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/stylesheet-manage.css\" />\n";
  echo "    <script src=\"" . WEB_PATH . "/layout/javascript-manage.js\"></script>\n";
  echo "    <script> var web_path = '" . WEB_PATH . "'; </script>\n";
  echo "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n";

  echo "  </head>\n";
  $database = new DeitloffDatabase(DATABASE_PATH);
  echo "  <body" . ($onload != "" ? " onLoad=\"" . $onload . "\"" : "") . ">\n";
  if (!isset($_SESSION))
	session_start();
  if (isset($_SESSION[MANAGE_SESSION]))
  {
    $number_messages = $database->querySingle("SELECT count(*) FROM staff_messages WHERE recipient_identity='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]) . "' AND read_yet='FALSE'");
    if ($number_messages > 0)
    echo "<a href=\"" . WEB_PATH . "/manage/inbox.php\" class=\"messagesNotification\">" .
		"<div class=\"messagesNotification\"><b>You have " .
		$number_messages . " unread message" .
		($number_messages == 1 ? "" : "s") . ".</b> Click here to open the inbox.</div></a>\n";
  }
  if (!$suppress_navigation && isset($_SESSION[MANAGE_SESSION]))
  {
    echo "  <div class=\"navigation\">\n";
	echo "    <center>\n";
	$staff_name = $database->querySingle("SELECT first_name, last_name FROM staff WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) .
		"' AND active='TRUE'", true);
	echo "      <div style=\"font-weight:bold;\">" . $staff_name["first_name"] . " " . $staff_name["last_name"] . "</div>\n";
	echo "      <a href=\"" . WEB_PATH . "/manage/index.php\"><img src=\"" . WEB_PATH . "/layout/manage-icon-home.png\" border=\"0\" /></a>\n";
	echo "    </center>\n";
	echo "    <div class=\"navigationHeader\" style=\"margin-top:0px;\">Mail</div>\n";
	echo "    <div class=\"navigationItem\">&raquo; <a href=\"" . WEB_PATH . "/manage/inbox.php\">Inbox" .
		($number_messages > 0 ? " [" . $number_messages . "]" : "") . "</a></div>\n";
	echo "    <div class=\"navigationItem\">&raquo; <a href=\"" . WEB_PATH . "/manage/send-mail.php\">Compose</a></div>\n";
	echo "    <div class=\"navigationHeader\">Content</div>\n";
	if ($_SESSION[MANAGE_PERMISSIONS_SESSION][1] == "1")
	{
	  echo "    <div class=\"navigationItem\">&raquo; Articles</div>\n";
	  $number_drafts = $database->querySingle("SELECT count(*) FROM beats WHERE staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) .
		"' AND published='FALSE'");
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/post-article.php\">New Article</a></div>\n";
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/manage-articles.php\">Manage [" . $number_drafts . " Draft" .
		($number_drafts != 1 ? "s" : "") . "]</a></div>\n";
         echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/major-stories.php\">Your Major Stories</a></div>\n";
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/migrate-article.php\">Migrate Article</a></div>\n";
    }
	if ($_SESSION[MANAGE_PERMISSIONS_SESSION][2] == "1")
	{
	  echo "    <div class=\"navigationItem\">&raquo; Photos</div>\n";
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/do-gallery.php\">New Photo Gallery</a></div>\n";
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/manage-galleries.php\">Manage Galleries</a></div>\n";
	}
	if ($_SESSION[MANAGE_PERMISSIONS_SESSION][3] == "1")
	{
	  echo "    <div class=\"navigationItem\">&raquo; Audio</div>\n";
	}
	if ($_SESSION[MANAGE_PERMISSIONS_SESSION][4] == "1")
	{
	  echo "    <div class=\"navigationItem\">&raquo; Video</div>\n";
	}
	if ($_SESSION[MANAGE_PERMISSIONS_SESSION][5] == "1")
	{
	  echo "    <div class=\"navigationItem\">&raquo; Comics</div>\n";
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/upload-comic.php\">Upload Comic</a></div>\n";
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/manage-comics.php\">Manage</a></div>\n";
	}
	if ($_SESSION[MANAGE_PERMISSIONS_SESSION][6] == "1")
	{
	  echo "    <div class=\"navigationItem\">&raquo; Sports</div>\n";
	}
	if ($_SESSION[MANAGE_PERMISSIONS_SESSION][7] == "1")
	{
	  echo "    <div class=\"navigationItem\">&raquo; Polls</div>\n";
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/new-poll.php\">New Poll</a></div>\n";
	  $number_active_polls = $database->querySingle("SELECT count(*) FROM polls WHERE date('now') BETWEEN date_start AND date_end");
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/manage-polls.php\">Manage [" . $number_active_polls . 
		" active]</a></div>\n";
	}
	if ($_SESSION[MANAGE_PERMISSIONS_SESSION][8] == "1")
	{
	  echo "    <div class=\"navigationItem\">&raquo; Groups</div>\n";
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/do-group.php\">Create Group</a></div>\n";
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/manage-groups.php\">Manage Groups</a></div>\n";
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/change-group-picture.php\">Change Group Picture</a></div>\n";
	}
       if ($_SESSION[MANAGE_PERMISSIONS_SESSION][9] == "1")
       {
         echo "    <div class=\"navigationItem\">&raquo; Administration</div>\n";
         echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/manage-staff.php\">Manage Staff</a></div>\n";
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/manage-statistics.php\">Manage Statistics</a></div>\n";
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/approve-staff-positions.php\"><small>Approve Staff Positions</small></a></div>\n";
		 echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/admin-parse-excel.php\">Parse XML</a></div>\n";
       }
	if ($_SESSION[MANAGE_PERMISSIONS_SESSION][10] == "1")
	{
	  echo "    <div class=\"navigationItem\">&raquo; Sound Slides</div>\n";
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/do-soundslide.php\">Create Sound Slideshow</a></div>\n";
	  echo "    <div class=\"navigationSubItem\">&ndash; <a href=\"" . WEB_PATH . "/manage/manage-soundslides.php\">Manage Soundslides</a></div>\n";
	}
	echo "    <div class=\"navigationHeader\">Account</div>\n";
	echo "    <div class=\"navigationItem\">&raquo; <a href=\"" . WEB_PATH . "/manage/change-portfolio-bio.php\">Change Portfolio Bio</a></div>\n";
	echo "    <div class=\"navigationItem\">&raquo; <a href=\"" . WEB_PATH . "/manage/change-staff-positions.php\">Change Staff Positions</a></div>\n";
	echo "    <div class=\"navigationItem\">&raquo; <a href=\"" . WEB_PATH . "/manage/change-staff-links.php\">Manage Staff Links</a></div>\n";
	echo "    <div class=\"navigationItem\">&raquo; <a href=\"" . WEB_PATH . "/manage/change-staff-picture.php\">Change Picture</a></div>\n";
	echo "    <div class=\"navigationItem\">&raquo; <a href=\"" . WEB_PATH . "/manage/change-password.php\">Change Password</a></div>\n";

    echo "    <div class=\"navigationItem\">&raquo; <a href=\"" . WEB_PATH . "/manage/logout.php\">Logout</a></div>\n";
    echo "  </div>\n";
  }
  echo "  <div class=\"mainColumn\">\n";
  echo "    <div class=\"mainColumnHeader\">" . $page_title . "</div>\n";
}
function outputManageFooter()
{
  echo "  </div>\n";
  echo "  </body>\n";
  echo "</html>\n";
}
?>
