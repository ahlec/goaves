<?php
require_once (DOCUMENT_ROOT . "/config/related-group.inc.php");
/* **************************************
 *  Valid Pages Include                 *
 * **************************************/
 
$_PAGES = array("index" => "GoAvesIndex", "home" => "GoAvesIndex", "news" => "GoAvesNews", "page-not-found" => "GoAves404",
	"article" => "GoAvesArticle", "video" => "GoAvesVideo", "gallery" => "GoAvesGallery", "staff" => "GoAvesStaff",
	"staff-portfolio" => "GoAvesStaffPortfolio", "comic" => "GoAvesComic", "multimedia" => "GoAvesMultimedia",
	"photo" => "GoAvesPhoto", "audio" => "GoAvesAudio", "contact" => "GoAvesContact", "yearbook" => "GoAvesYearbook",
	"leaf" => "GoAvesLeaf", "polls" => "GoAvesPoll", "sports" => "GoAvesSports", "leaf-issue" => "GoAvesLeafIssue",
	"sound-slides" => "GoAvesSoundSlides", "group" => "GoAvesGroup", "group-listing" => "GoAvesGroupListing");
$_ERROR_PAGES = array(404 => "page-not-found");

function parsePath($path, $page_definitions, $error_pages)
{
  $path_pieces = explode("/", $path);
  if (sizeof($path_pieces) == 0 || $path_pieces[0] == "")
    $path_pieces[0] = "index";
  else
    $path_pieces[0] = mb_strtolower($path_pieces[0]);
  if (!in_array($path_pieces[0], array_keys($page_definitions)))
    $path_pieces = array($error_pages[404], $path_pieces[0]);
  return $path_pieces;
}
function selectPage($path_pieces, $page_definitions, $database)
{
  // Find Proper Page
  $selected_page = new $page_definitions[$path_pieces[0]]();
  $selected_page_final = $selected_page->checkOrRedirect($path_pieces, $database);
  while($selected_page_final !== true)
  {
    $selected_page = $selected_page_final;
	$selected_page_final = $selected_page->checkOrRedirect($path_pieces, $database);
  }
  return $selected_page;
}

function generateBreadcrumbs($bread_slice)
{
  $bread_trail = "";
  foreach (array_keys($bread_slice) as $bread_token)
  {
    if ($bread_token == "[this]")
	  $bread_trail .= "<b>" . $bread_slice["[this]"] . "</b>";
	else
	  $bread_trail .= "<a href=\"" . WEB_PATH . "/" . $bread_token . "/\">" . $bread_slice[$bread_token] . "</a>";
	$bread_trail .= " &raquo; ";
  }
  return rtrim($bread_trail, " &raquo; ");
}
function generatePageNavigation($page_handle, $current_page, $total_pages)
{
  $has_previous_page = ($current_page > 1);
  $has_next_page = ($current_page < $total_pages);
  generateArbitraryPageNavigation(($has_previous_page ? $page_handle . "/page-" . ($current_page - 1) : null), "Previous Page",
	"Page " . $current_page . " of " . $total_pages, ($has_next_page ? $page_handle . "/page-" . ($current_page + 1) : null),
	"Next Page");
}
function generateArbitraryPageNavigation($previous_page_handle, $previous_page_title, $center, $next_page_handle, $next_page_title)
{
  echo "      <table class=\"pageNavigation\"><tr>\n";
  echo "        <td class=\"left " . ($previous_page_handle != null ? "active" : "disabled") . "\" " .
	($previous_page_handle != null ? " onMouseOver=\"this.className='left activeHover';\" onMouseOut=\"this.className='left active';\" " .
	"onClick=\"window.location='" . WEB_PATH . "/" . $previous_page_handle . "/';\"" : "") . ">" . $previous_page_title . "</td>\n";
  echo "        <td class=\"center\">" . $center . "</td>\n";
  echo "        <td class=\"right " . ($next_page_handle != null ? "active" : "disabled") . "\" " .
	($next_page_handle != null ? " onMouseOver=\"this.className='right activeHover';\" onMouseOut=\"this.className='right active';\" " .
	"onClick=\"window.location='" . WEB_PATH . "/" . $next_page_handle . "/';\"" : "") . ">" . $next_page_title . "</td>\n";
  echo "      </tr></table>\n";
}

function parseCurrentPage($path_page, $number_pages)
{
  if ($path_page == null || $path_page == "" || strpos($path_page, "page-") === false || !ctype_digit(str_replace("page-","",$path_page)))
    return 1;
  else
  {
    $cur_page = str_replace("page-", "", $path_page);
	if ($cur_page >= $number_pages)
	  return $number_pages;
	if ($cur_page < 1)
	  return 1;
    return $cur_page;
  }
  return 1;
}

/* ****************************************
 *  Page Definitions                      *
 * ****************************************/
class GoAvesIndex {
	private $_recentMajorStories = array();
	private $_homepageBeatTypes = array();
	function loadIndex($path, $database)
	{
	  $get_major_stories = $database->query("SELECT header_image, beats.title, beats.handle, beats.post_time, beats.contents FROM " .
		"major_stories JOIN beats ON major_stories.article_identity = beats.beat_identity WHERE beats.published='TRUE' ORDER BY " .
		"beats.post_time DESC LIMIT " . MAJOR_STORIES_TO_DISPLAY);
	  while ($major_story = $get_major_stories->fetchArray())
	  {
	    $smart_blurb = get_smart_blurb($major_story["contents"], 50);
		$final_blurb_character = substr($smart_blurb, strlen($smart_blurb)*-1);
	    $this->_recentMajorStories[] = array("HEADER_IMAGE" => $major_story["header_image"],
			"TITLE" => utf8_encode($major_story["title"]), "HANDLE" => $major_story["handle"], "POST_TIME" => strtotime($major_story["post_time"]),
			"CONTENTS" => utf8_encode($smart_blurb . "... <a href='" . WEB_PATH . "/article/" . $major_story["handle"] . "/'>Full story &raquo;</a>"));
	  }
	  
	  $get_frontpage_beat_types = $database->query("SELECT type_identity, handle, title, icon_file FROM beat_types WHERE " .
		"feature_on_front_page='TRUE' ORDER BY sort_order ASC, title ASC");
	  while ($fbt = $get_frontpage_beat_types->fetchArray())
	  {
	    $this->_homepageBeatTypes[] = array("HANDLE" => $fbt["handle"], "TITLE" => $fbt["title"], "ICON" => $fbt["icon_file"]);
		$articles = array();
		$get_frontpage_articles = $database->query("SELECT handle, title, contents, image_file FROM beats WHERE published='TRUE' AND type='" .
			$fbt["type_identity"] . "' ORDER BY post_time DESC LIMIT " . FEATURED_ARTICLE_TYPES_ARTICLES_TO_DISPLAY);
		while ($article = $get_frontpage_articles->fetchArray())
		  $articles[] = array("HANDLE" => $article["handle"], "TITLE" => utf8_encode($article["title"]), "CONTENTS" => get_smart_blurb(
			utf8_encode($article["contents"]), 20) . "...", "IMAGE_FILE" => $article["image_file"]);
		$this->_homepageBeatTypes[sizeof($this->_homepageBeatTypes) - 1]["ARTICLES"] = $articles;
	  }
	}
	function checkOrRedirect($path, $database) { $this->loadIndex($path, $database); return true; }
	function getMainTab() { return "home"; }
	function getPageHandle() { return "home"; }
	function getPageSubhandle() { return null; }
	function getBreadTrail() { return null; }
	function getPageTitle() { return null; }
	function getBodyOnload() { return "startMajorStoryInterval()"; }
	function getPageContents()
	{
	  echo "      <div class=\"indexContainer\">\n";
	  
	  // Tier One: Upcoming Events (Left), Major Stories (Right)
	  echo "        <script>\n";
	  echo "          var major_story_definitions = new Array();\n";
	  for ($populate_array = 0; $populate_array < sizeof($this->_recentMajorStories); $populate_array++)
	    echo "          major_story_definitions[" . $populate_array . "] = new Array('" . ($populate_array + 1) . "','" .
			addslashes($this->_recentMajorStories[$populate_array]["TITLE"]) . "','" .
			addslashes($this->_recentMajorStories[$populate_array]["CONTENTS"]) . "','" . WEB_PATH . "/images/major_stories/" .
			$this->_recentMajorStories[$populate_array]["HEADER_IMAGE"] . "');\n";
	  echo "        </script>\n";
	  echo "        <div class=\"majorStoryContainer\" style=\"background-image:url('" . WEB_PATH . "/images/major_stories/" .
		$this->_recentMajorStories[0]["HEADER_IMAGE"] . "');\" id=\"major-story-container\">\n";
	  echo "          <div class=\"storyContent\" onMouseOver=\"stopMajorStoryInterval();\" onMouseOut=\"startMajorStoryInterval();\">";
	  echo "            <div class=\"title\" id=\"major-story-title\">" . $this->_recentMajorStories[0]["TITLE"] . "</div>\n";
	  echo "            <div id=\"major-story-contents\">" . $this->_recentMajorStories[0]["CONTENTS"] . "</div>\n";
	  echo "          </div>\n";
	  echo "        </div>\n";
	  echo "        <div class=\"majorStoryNavigation\" id=\"major-story-navigation\">\n";
	  $number_words = array("One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten");
	  for ($navigation_out = 0; $navigation_out < sizeof($this->_recentMajorStories); $navigation_out++)
	    echo "          <div class=\"item" . ($navigation_out == 0 ? " active" : "") . "\" onClick=\"toggleMajorStory('" .
			($navigation_out + 1) . "');\" id=\"major-story-" . ($navigation_out + 1) . 
			"\">" .
			$number_words[$navigation_out] . "</div>" . ($navigation_out < sizeof($this->_recentMajorStories) - 1 ?
			" | " : "") . "\n";
	  echo "        </div>\n";
	  
	  echo "        <div class=\"upcomingEventsContainer\">\n";
	  echo "          [Upcoming Events coming soon]<br />\n";
	  echo "          <small>(No, the irony does not escape me)</small>\n";
	  //echo "<br /><br /><b>So, I herd you liek Mudkips???</b>\n";
	  //echo "<img src=\"" . WEB_PATH . "/mudkip.png\" />\n";
	  echo "        </div>\n";

	  echo "        <div style=\"clear:both;\"></div>\n";
	  
	  echo "      <div class=\"pollContainer\">\n";
	  echo "        <div class=\"header\">Poll</div>\n";
	  echo "        <div class=\"question\">What is your favorite school subject?</div>\n";
	  echo "        <div class=\"entry\"><input type=\"radio\"> Science</div>\n";
	  echo "        <div class=\"entry\"><input type=\"radio\"> History</div>\n";
	  echo "        <div class=\"entry\"><input type=\"radio\"> Maths</div>\n";
	  echo "        <div class=\"entry\"><input type=\"radio\"> English</div>\n";
	  echo "        <div class=\"entry\"><input type=\"radio\"> Other</div>\n";
	  echo "        <div class=\"submit\"><button>Vote</button></div>\n";
	  echo "      </div>\n";
	  /* Featured Article Types */
	  echo "      <div class=\"featuredArticleTypesContainer\">\n";
	  foreach ($this->_homepageBeatTypes as $featured_type)
	  {
	    echo "        <div class=\"container\">\n";
		echo "          <div class=\"title\"><span>" . ($featured_type["HANDLE"] == "beat" ? "Main" : $featured_type["TITLE"]) . "</span></div>\n";
		if (sizeof($featured_type["ARTICLES"]) > 0)
		{
		  echo "          <a href=\"" . WEB_PATH . "/article/" . $featured_type["ARTICLES"][0]["HANDLE"] . "/\"><img src=\"" .
			WEB_PATH . "/images/articles/" . $featured_type["ARTICLES"][0]["IMAGE_FILE"] . "\" border=\"0\"></a>\n";
		  echo "          <a href=\"" . WEB_PATH . "/article/" . $featured_type["ARTICLES"][0]["HANDLE"] . "/\" class=\"first\">" .
			$featured_type["ARTICLES"][0]["TITLE"] . "</a><br />\n";
		  echo "          <div class=\"firstBlurb\">" . $featured_type["ARTICLES"][0]["CONTENTS"] . "</div>\n";
		  if (sizeof($featured_type["ARTICLES"]) > 1)
		    echo "          <div class=\"divider\"></div>\n";
		  for ($article = 1; $article < sizeof($featured_type["ARTICLES"]); $article++)
		    echo "          <a href=\"" . WEB_PATH . "/article/" . $featured_type["ARTICLES"][$article]["HANDLE"] .
				"/\" class=\"other\">" . $featured_type["ARTICLES"][$article]["TITLE"] . "</a><br />\n";
		} else
		  echo "          <center>No articles found.</center>\n";
		echo "        </div>\n";

	  }
	  echo "      </div>\n";

	  echo "      <div style=\"clear:both;\"></div>\n";
	  
	  /* Latest Multimedia AND Polls */
	  
	  echo "      </div>\n";
	  echo "      <div style=\"clear:both;\"></div>\n";
	}
}
class GoAvesNews {
	private $_storiesPerPage = 10;
	private $_numberStories;
	private $_currentPage;
	private $_numberPages;
	private $_stories = array();
	private $_requestedType = null;
	private $_requestedHandle = null;
	function loadStories($path, $database)
	{
	  if (isset($path[1]) && $database->querySingle("SELECT count(*) FROM beat_types WHERE handle LIKE '" .
		$database->escapeString($path[1]) . "'") > 0)
	  {
	    $this->_requestedType = $database->querySingle("SELECT type_identity FROM beat_types WHERE handle LIKE '" .
		$database->escapeString($path[1]) . "' LIMIT 1");
	    $this->_requestedHandle = $database->escapeString($path[1]);
	  }

	  $this->_numberStories = $database->querySingle("SELECT count(*) FROM beats WHERE published='TRUE'" . ($this->_requestedType !== null ?
		" AND type='" . $this->_requestedType . "'" : ""));
	  
	  $this->_numberPages = floor($this->_numberStories % $this->_storiesPerPage == 0 ? $this->_numberStories / $this->_storiesPerPage :
		$this->_numberStories / $this->_storiesPerPage + 1);
	  if ($this->_numberPages < 1)
	    $this->_numberPages = 1;
	  $this->_currentPage = parseCurrentPage(($this->_requestedType == null ? $path[1] : $path[2]), $this->_numberPages);
	  
	  $get_latest_beats = $database->query("SELECT beats.handle, beats.title, beats.post_time, beats.contents, beats.image_file, beats.image_alignment, " .
		"beats.image_caption, staff.first_name, staff.last_name, beat_types.title AS \"beat_type\" FROM beats JOIN staff ON beats.staff_identity = " .
		"staff.identity JOIN beat_types ON beats.type = beat_types.type_identity WHERE published='TRUE' " .
		($this->_requestedType === null ? "" : "AND type='" . $this->_requestedType . "' ") .
		"ORDER BY beats.post_time DESC LIMIT " . $this->_storiesPerPage . " OFFSET " . (($this->_currentPage - 1) * $this->_storiesPerPage));
	  while ($story = $get_latest_beats->fetchArray())
	    $this->_stories[] = array("HANDLE" => $story["handle"], "TITLE" => format_content($story["title"]), "POST_TIME" => strtotime($story["post_time"]),
			"IMAGE" => ($story["image_file"] != "" ? "images/articles/" . $story["image_file"] : "layout/no-image-article.jpg"),
			"AUTHOR_FIRST_NAME" => $story["first_name"], "AUTHOR_LAST_NAME" => $story["last_name"], "BEAT_TYPE" => $story["beat_type"]);
	}
	function checkOrRedirect($path,$database)
	{
	  $this->loadStories($path, $database);
	  return true;
	}
	function getMainTab() { return "news"; }
	function getPageHandle() { return "news"; }
	function getPageSubhandle() { return $this->_requestedHandle; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "Latest News"); }
	function getPageTitle() { return "Latest News"; }

	function getPageContents()
	{
	  generatePageNavigation("news" . ($this->_requestedType === null ? "" : "/" . $this->_requestedHandle), $this->_currentPage,
		$this->_numberPages);
	  foreach ($this->_stories as $story)
	  {
		echo "      <div class=\"articleListingContainer\" onMouseOver=\"this.className='articleListingContainerHover';\" " .
			"onMouseOut=\"this.className='articleListingContainer';\" onClick=\"window.location='" . WEB_PATH . "/article/" . $story["HANDLE"] . "/';\">\n";
		echo "        <div class=\"articleImage\"><img src=\"" . WEB_PATH . "/" . $story["IMAGE"] . "\" /></div>\n";
		echo "        <div class=\"title\">" . $story["TITLE"] . "</div>\n";
		echo "        <div class=\"subtitle\">" . date(LONG_DATETIME_FORMAT, $story["POST_TIME"]) . " &ndash; <b>" . $story["AUTHOR_FIRST_NAME"] . " " .
			$story["AUTHOR_LAST_NAME"] . "</b>" . ($this->_requestedType === null ? 
			($story["BEAT_TYPE"] != "Beat" ? " &ndash; " . $story["BEAT_TYPE"] : "") : "") . "</div>\n";
		echo "        <div style=\"clear:left;\"></div>\n";
		echo "      </div>\n";
	  }
	  generatePageNavigation("news" . ($this->_requestedType === null ? "" : "/" . $this->_requestedHandle), $this->_currentPage,
		$this->_numberPages);
	}
}
class GoAvesArticle {
	private $_maxArticlesDisplayOnSide = 7;
	private $_handle;
	private $_title;
	private $_postTime;
	private $_contents;
	private $_imageFile;
	private $_imageAlignment;
	private $_imageCaption;
	private $_authorFirstName;
	private $_authorLastName;
	private $_authorImage;
	private $_authorPosition;
	private $_typeIdentity;
	private $_typeTitle;
	private $_typeHandle;
	//private $_authorsOtherArticles = array();
	private $_otherTypeArticles = array();
	private $_groupInformation;
	private $_photoCredit;
	function loadArticle($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT beats.beat_identity, beats.handle, beats.title, beats.post_time, beats.contents, " .
		"beats.image_file, beats.image_alignment, beats.image_caption, beats.type, beat_types.title AS \"type_title\", " .
		"beat_types.handle AS \"type_handle\", staff.first_name, staff.last_name, " .
		"staff.icon_file AS \"staff_picture\", staff.positions, beats.staff_identity, related_group, photo_credit " .
		"FROM beats JOIN staff ON beats.staff_identity = staff.identity JOIN beat_types ON " .
		"beat_types.type_identity = beats.type WHERE beats.handle LIKE '" . 
		$database->escapeString($path[1]) . "' AND published='TRUE' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_handle = $info["handle"];
	  $this->_title = format_content($info["title"]);
	  $this->_postTime = strtotime($info["post_time"]);
	  $this->_contents = format_content($info["contents"]);
	  $this->_imageFile = ($info["image_file"] != "" ? $info["image_file"] : null);
	  $this->_imageAlignment = ($info["image_file"] != "" ? strtolower($info["image_alignment"]) : null);
	  $this->_imageCaption = ($info["image_file"] != "" ? format_content($info["image_caption"]) : null);
	  $this->_authorFirstName = utf8_encode($info["first_name"]);
	  $this->_authorLastName = utf8_encode($info["last_name"]);
	  $this->_authorImage = ($info["staff_picture"] != "" ? $info["staff_picture"] : "no-staff-icon.jpg");
	  $this->_authorPosition = $info["positions"];
	  $this->_photoCredit = ($info["photo_credit"] != "" ? format_content($info["photo_credit"]) : null);
	  if ($info["related_group"] != "" && ctype_digit($info["related_group"]))
	    $this->_groupInformation = getGroupInformation($database, $info["related_group"], $info["beat_identity"], "article");
	  else
	    $this->_groupInformation = null;
	  /*$other_articles = $database->query("SELECT handle, title FROM beats WHERE staff_identity='" . $info["staff_identity"] . "' AND beat_identity<>'" .
		$info["beat_identity"] . "' AND published='TRUE' ORDER BY post_time DESC LIMIT " .
		$this->_maxArticlesDisplayOnSide);*/
	  $other_type_articles = $database->query("SELECT handle, title FROM beats WHERE beat_identity<>'" . $info["beat_identity"] . "' AND published='TRUE' " .
		"AND type='" . $info["type"] . "' ORDER BY post_time DESC LIMIT " . $this->_maxArticlesDisplayOnSide);
	  $this->_typeIdentity = $info["type"];
	  $this->_typeTitle = $info["type_title"];
	  $this->_typeHandle = $info["type_handle"];
	  //while ($oa = $other_articles->fetchArray())
	  //  $this->_authorsOtherArticles[] = array("HANDLE" => $oa["handle"], "TITLE" => $oa["title"]);
	  while ($ota = $other_type_articles->fetchArray())
	    $this->_otherTypeArticles[] = array("HANDLE" => $ota["handle"], "TITLE" => utf8_encode($ota["title"]));
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadArticle($path, $database))
	    return new GoAvesNews();
	  return true;
	}
	function getMainTab() { return "news"; }
	function getPageHandle() { return "article"; }
	function getPageSubhandle() { return $this->_handle; }
	function getBreadTrail() { return array("home" => "GoAves.com", "news" => "Latest News", "[this]" => $this->_title); }
	function getPageTitle() { return $this->_title; }

	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/news/\">&laquo; Return to news listing</a></div>\n";
	  echo "      <div class=\"articleSide\">\n";
	  echo "        <img src=\"" . WEB_PATH . "/images/staff/" . $this->_authorImage . "\" class=\"staffPicture\" />\n";
	  echo "        <div class=\"authorInfo\">\n";
	  echo "          <b><a href=\"" . WEB_PATH . "/staff-portfolio/" . mb_strtolower($this->_authorFirstName . "-" . $this->_authorLastName) . "/\">" .
		$this->_authorFirstName . " " . $this->_authorLastName . "</a></b><br />\n";
	  echo "          " . $this->_authorPosition . "\n";
	  echo "        </div>\n";
	  echo "        <div style=\"clear:left;\"></div>\n";
	  /*echo "        <div class=\"otherHeader\">Other articles by " . $this->_authorFirstName . " " . $this->_authorLastName . ":</div>\n";
	  if (sizeof($this->_authorsOtherArticles) == 0)
	    echo "        <center>" . $this->_authorFirstName . " has no other articles</center>\n";
	  else
	  {
	    echo "        <ul>\n";
	    foreach ($this->_authorsOtherArticles as $other_article)
		  echo "          <li><a href=\"" . WEB_PATH . "/article/" . $other_article["HANDLE"] . "/\">" . $other_article["TITLE"] . "</a></li>\n";
		echo "        </ul>\n";
	  }*/
	  if ($this->_groupInformation != null)
	    outputGroupInformation($this->_groupInformation);
	  if ($this->_typeIdentity > 1)
	  {
	    echo "        <div class=\"sectionHeader\">More articles in <a href=\"" . WEB_PATH . "/news/" . $this->_typeHandle . 
			"/\">" . $this->_typeTitle . "</a>:</div>\n";
		if (sizeof($this->_otherTypeArticles) == 0)
		  echo "        <center>No other articles</center>\n";
		else
		{
		  echo "        <ul>\n";
		  foreach ($this->_otherTypeArticles as $other_type_articles)
		    echo "          <li><a href=\"" . WEB_PATH . "/article/" . $other_type_articles["HANDLE"] . "/\">" . $other_type_articles["TITLE"] .
			"</a></li>\n";
		  echo "        </ul>\n";
		}
	  }
	  echo "      </div>\n";
	  echo "      <div class=\"articleContainer\">\n";
	  echo "        <div class=\"title\">" . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\">Posted " . date(LONG_DATETIME_FORMAT, $this->_postTime) . "</div>\n";
	  echo "        <div class=\"article\">\n";
	  if ($this->_imageFile != null)
	  {
	    echo "          <div class=\"image " . $this->_imageAlignment . "\">\n";
	    echo "            <center><img src=\"" . WEB_PATH . "/images/articles/" . $this->_imageFile . "\" /></center>\n";
	    echo "            " . $this->_imageCaption . ($this->_photoCredit != null ? " <span class=\"credit\">Photo courtesy of " . $this->_photoCredit . ".</span>" : "") . "\n";
	    echo "          </div>\n";
	  }
	  echo $this->_contents;
	  echo "          <div style=\"clear:both;\"></div>\n";
	  echo "        </div>\n";
	  echo "        <div style=\"clear:both;\"></div>\n";
	}
}
class GoAvesVideo {
	private $_handle;
	private $_title;
	private $_datePublished;
	private $_videoFile;
	private $_caption;
	private $_groupInformation;
	function loadVideo($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT videos.video_identity, videos.title, videos.handle, videos.date_published, videos.video_file, " .
		"videos.captions, videos.related_group FROM videos WHERE videos.handle LIKE '" . 
		$database->escapeString($path[1]) . "' AND videos.published='TRUE' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_handle = $info["handle"];
	  $this->_title = $info["title"];
	  $this->_datePublished = strtotime($info["date_published"]);
	  $this->_videoFile = $info["video_file"];
	  $this->_caption = $info["captions"];
	  if ($info["related_group"] != "" && ctype_digit($info["related_group"]))
	    $this->_groupInformation = getGroupInformation($database, $info["related_group"], $info["video_identity"], "video");
	  else
	    $this->_groupInformation = null;
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadVideo($path, $database))
	    return new GoAvesMultimedia();
	  return true;
	}
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "video"; }
	function getPageSubhandle() { return $this->_handle; }
	function getPageTitle() { return $this->_title; }
	function getBreadTrail() { return array("home" => "GoAves.com", "multimedia" => "Multimedia", "[this]" => "[Video] " . $this->_title); }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/multimedia/\">&laquo; Return to multimedia</a></div>\n";
	  
	  echo "      <div class=\"articleSide\">\n";
	  if ($this->_groupInformation != null)
	    outputGroupInformation($this->_groupInformation);
	  echo "      </div>\n";
	  
	  echo "      <div class=\"articleContainer\">\n";
	  echo "        <div class=\"title\">" . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\">Posted " . date(LONG_DATE_FORMAT, $this->_datePublished) . "</div>\n";
	  echo "        <center>\n";
	  echo "          <a href=\"" . WEB_PATH . "/components/videos/" . $this->_videoFile . "\" class=\"video\" id=\"player\"></a>\n";
	  echo "          <script src=\"" . WEB_PATH . "/layout/flowplayer/flowplayer-3.2.2.min.js\"></script>\n";
	  echo "          <script language=\"JavaScript\">\n";
	  echo "            flowplayer(\"player\", \"" . WEB_PATH . "/layout/flowplayer/flowplayer-3.2.2.swf\", { clip : { url : '" . WEB_PATH .
		"/components/videos/" . $this->_videoFile . "', autoPlay : false} });\n";
	  echo "          </script>\n";
	  echo "        </center>\n";
	  if ($this->_caption != "")
	    echo "        <div class=\"caption\">" . $this->_caption . "</div>\n";
	  echo "      </div>\n";
	}
}
class GoAves404 {
	function checkOrRedirect($path, $database) { return true; }
	function getMainTab() { return "goaves"; }
	function getPageHandle() { return "page-not-found"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "Page Not Found"; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "Pages That Don't Exist"); }
	function getPageContents()
	{
	  echo "      <div class=\"errorContainer\">\n";
	  echo "        <div class=\"header\">Page Not Found</div>\n";
	  echo "        <div class=\"subheader\">Error 404</div>\n";
	  echo "        <div class=\"description\">";
	  echo "The page or file you were trying to load does not exist. This might be due to an error in our programming, or the file you were trying to access " .
		"having been taken down. If you think you have reached this page in error, please <b><a href=\"" . 
		WEB_PATH . "/contact/\">contact us</a></b> and voice your concern.<br /><br /><b><a href=\"" . WEB_PATH . "/\">&laquo; " .
		"Return to the homepage &raquo;</a></b>";
	  echo "</div>\n";
	  echo "      </div>\n";
	}
}
class GoAvesGallery {
    private $_handle;
	private $_title;
	private $_dateLastUpdated;
	private $_photosPerPage = 8;
	private $_numberPhotos;
	private $_currentPage;
	private $_numberPages;
	private $_photos = array();
	private $_groupInformation;
	function loadGallery($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT gallery_identity, handle, title, date_last_updated, related_group FROM " .
		"photo_galleries WHERE handle LIKE '" . $database->escapeString($path[1]) . "' AND published='TRUE' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_handle = $info["handle"];
	  $this->_title = $info["title"];
	  $this->_dateLastUpdated = strtotime($info["date_last_updated"]);
	  $this->_numberPhotos = $database->querySingle("SELECT count(*) FROM photos WHERE gallery_identity='" . $info["gallery_identity"] . "'");
	  
	  $this->_numberPages = floor($this->_numberPhotos % $this->_photosPerPage == 0 ? $this->_numberPhotos / $this->_photosPerPage :
		$this->_numberPhotos / $this->_photosPerPage + 1);
	  if ($this->_numberPages < 1)
	    $this->_numberPages = 1;
	  $this->_currentPage = parseCurrentPage($path[2], $this->_numberPages);
	  
	  $get_photos = $database->query("SELECT handle, image_file, icon_file FROM photos WHERE gallery_identity='" . $info["gallery_identity"] .
		"' ORDER BY picture_identity ASC LIMIT " . $this->_photosPerPage . " OFFSET " . (($this->_currentPage - 1) * $this->_photosPerPage));
	  while ($photo = $get_photos->fetchArray())
	    $this->_photos[] = array("HANDLE" => $photo["handle"], "ICON" => ($photo["icon_file"] != "" ? $photo["icon_file"] : $photo["image_file"]));
	  if ($info["related_group"] != "" && ctype_digit($info["related_group"]))
	    $this->_groupInformation = getGroupInformation($database, $info["related_group"], $info["gallery_identity"], "photo_gallery");
	  else
	    $this->_groupInformation = null;
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadGallery($path, $database))
	    return new GoAvesMultimedia();
	  return true;
	}
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "gallery"; }
	function getPageSubhandle() { return $this->_handle; }
	function getPageTitle() { return $this->_title; }
	function getBreadTrail() { return array("home" => "GoAves.com", "multimedia" => "Multimedia", "[this]" => "[Photo Gallery] " . $this->_title); }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/multimedia/\">&laquo; Return to multimedia</a></div>\n";
	  echo "      <div class=\"articleSide\">\n";
	  
	  if ($this->_groupInformation != null)
	    outputGroupInformation($this->_groupInformation);
	  echo "      </div>\n";
	  echo "      <div class=\"articleContainer galleryContainer\">\n";
	  echo "        <div class=\"title\">" . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\">" . $this->_numberPhotos . " photo" . ($this->_numberPhotos == 1 ? "" : "s") . "</div>\n";
	  
	  generatePageNavigation("gallery/" . $this->_handle, $this->_currentPage, $this->_numberPages);
	  if ($this->_numberPhotos == 0)
	    echo "        No photos in the gallery\n";
	  else
	    foreach ($this->_photos as $photo)
		  echo "        <a href=\"" . WEB_PATH . "/photo/" . $photo["HANDLE"] . "/\"><img class=\"icon\" src=\"" .
			WEB_PATH . "/images/photos/" . $photo["ICON"] . "\" border=\"0\" /></a>\n";
	  generatePageNavigation("gallery/" . $this->_handle, $this->_currentPage, $this->_numberPages);
	  echo "      </div>\n";
	}
}
class GoAvesStaff  {
	private $_numberActiveStaff;
	private $_currentPage;
	private $_numberPages;
	private $_staff = array();
	function loadStaffListing($path, $database)
	{
	  $this->_numberActiveStaff = $database->querySingle("SELECT count(*) FROM staff WHERE active='TRUE'");
	  
	  $this->_numberPages = floor($this->_numberActiveStaff % STAFF_PER_LISTING_PAGE == 0 ? $this->_numberActiveStaff / STAFF_PER_LISTING_PAGE :
		$this->_numberActiveStaff / STAFF_PER_LISTING_PAGE + 1);
	  if ($this->_numberPages < 1)
	    $this->_numberPages = 1;
	  $this->_currentPage = parseCurrentPage($path[1], $this->_numberPages);
	  
	  $get_staff = $database->query("SELECT first_name, last_name, image_file FROM staff WHERE active='TRUE' AND " .
		"display_on_staff_listing='TRUE' ORDER BY last_name, first_name ASC LIMIT " .
		STAFF_PER_LISTING_PAGE . " OFFSET " . (($this->_currentPage - 1) * STAFF_PER_LISTING_PAGE));
	  while ($staff = $get_staff->fetchArray())
	    $this->_staff[] = array("FIRST_NAME" => $staff["first_name"], "LAST_NAME" => $staff["last_name"], "IMAGE" => $staff["image_file"]);
	}
	function checkOrRedirect($path, $database)
	{
	  $this->loadStaffListing($path, $database);
	  return true;
	}
	function getMainTab() { return "home"; }
	function getPageHandle() { return "staff"; }
	function getPageSubhandle() { return null; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => $this->getPageTitle()); }
	function getPageTitle() { return "Staff Listing"; }
	function getPageContents()
	{
	  generatePageNavigation("staff", $this->_currentPage, $this->_numberPages);
	  echo "        <center>\n";
	  foreach ($this->_staff as $staff)
	  {
		echo "          <div class=\"staffListingContainer\" onMouseOver=\"this.className='staffListingContainerHover';\" onMouseOut=\"" .
			"this.className='staffListingContainer';\" onClick=\"window.location='" . WEB_PATH . "/staff-portfolio/" . mb_strtolower($staff["FIRST_NAME"] .
			"-" . $staff["LAST_NAME"]) . "/';\">\n";
		echo "            <div>" . $staff["FIRST_NAME"] . " " . $staff["LAST_NAME"] . "</div>\n";
		echo "            <img src=\"" . WEB_PATH . "/images/staff/" . $staff["IMAGE"] . "\" />\n";
		echo "          </div>\n";
	  }
	  echo "        </center>\n";
	  
	  generatePageNavigation("staff", $this->_currentPage, $this->_numberPages);
	}
}
class GoAvesStaffPortfolio {
	private $_firstName;
	private $_lastName;
	private $_shortBio;
	private $_positions;
	private $_imageFile;
	private $_active;
	private $_works = array();
	private $_staffListingPage;
	function loadPortfolio($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "" || strpos($path[1], "-") === false)
	    return false;
	  $name = explode("-", $path[1], 2);
	  $info = $database->querySingle("SELECT identity, first_name, last_name, short_bio, positions, image_file, active FROM staff WHERE first_name LIKE '" .
		$database->escapeString($name[0]) . "' AND last_name LIKE '" . $database->escapeString($name[1]) . "' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_firstName = $info["first_name"];
	  $this->_lastName = $info["last_name"];
	  $this->_shortBio = format_content($info["short_bio"]);
	  $this->_positions = $info["positions"];
	  $this->_imageFile = $info["image_file"];
	  $this->_active = ($info["active"] == "TRUE");

         $staff_before = $database->querySingle("SELECT count(*) FROM staff WHERE ((last_name < '" . $info["last_name"] . "') OR (last_name = '" . $info["last_name"] . "' AND " .
		"first_name < '" . $info["first_name"] . "')) AND active='TRUE' AND display_on_staff_listing='TRUE'");
         $this->_staffListingPage = ($staff_before == 0 ? 1 : floor($staff_before / STAFF_PER_LISTING_PAGE) + 1);
	  
	  $get_works = $database->query("SELECT handle, title, post_time AS \"datestamp\", \"article\" AS \"type\", null AS \"image\" FROM beats WHERE staff_identity='" .
		$info["identity"] . "' AND published='TRUE' UNION SELECT handle, title, date_posted AS \"datestamp\", \"comic\" AS \"type\", image_file AS \"image\"" .
		"FROM cartoons WHERE artist='" . $info["identity"] . "' UNION SELECT handle, title, post_date AS \"datestamp\", \"photo\" AS \"type\", " .
		"image_file AS \"image\" FROM photos WHERE photographer_identity='" . $info["identity"] . "' ORDER BY type ASC, datestamp DESC");
	  while ($work = $get_works->fetchArray())
	    $this->_works[] = array("TYPE" => $work["type"], "HANDLE" => $work["handle"], "TITLE" => format_content($work["title"]),
			"IMAGE" => ($work["image"] != "null" ? $work["image"] : null));
	  return true;
	}
	function checkOrRedirect($path, $database)
	{	
	  if (!$this->loadPortfolio($path, $database))
	    return new GoAvesStaff();
	  return true;
	}
	function getMainTab() { return "home"; }
	function getPageHandle() { return "staff-portfolio"; }
	function getPageSubhandle() { return mb_strtolower($this->_firstName . "-" . $this->_lastName); }
	function getBreadTrail() { return array("home" => "GoAves.com", "staff" => "Staff Listing", "[this]" => "[Portfolio] " . $this->_firstName .
		" " . $this->_lastName); }
	function getPageTitle() { return $this->_firstName . " " . $this->_lastName; }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/staff/page-" . $this->_staffListingPage . "/\">&laquo; Return to staff listing</a></div>\n";
	  echo "      <img src=\"" . WEB_PATH . "/images/staff/" . $this->_imageFile . "\" class=\"staffPortfolioImage\" />\n";
	  echo "      <div class=\"portfolioContainer\">\n";
	  echo "        <div class=\"name\">" . $this->_firstName . " " . $this->_lastName . "</div>\n";
	  echo "        <div class=\"positions\">" . $this->_positions . "</div>\n";
	  echo "        <div class=\"blurb\">" . $this->_shortBio . "</div>\n";
	  
	  if (sizeof($this->_works) > 0)
	  {
	    echo "      <div class=\"works\">\n";
		$work_type = null;
		foreach ($this->_works as $work)
		{
		  if ($work_type != $work["TYPE"])
		  {
		    $work_type = $work["TYPE"];
			echo "        <div class=\"header\"" . ($work_type != "article" ? " style=\"margin-bottom:5px;\"" : "") . ">";
			switch ($work_type)
			{
			   case "article": echo "Articles"; break;
			   case "comic": echo "Comics"; break;
			   case "photo": echo "Photos"; break;
			}
			echo "</div>\n";
		  }
		  if ($work["TYPE"] == "article")
		    echo "        <a href=\"" . WEB_PATH . "/article/" . $work["HANDLE"] . "/\" class=\"entry\">" . $work["TITLE"] . "</a><br/>\n";
		  else if ($work["TYPE"] == "comic" || $work["type"] == "photo")
		    echo "        <a href=\"" . WEB_PATH . "/" . $work["TYPE"] . "/" . $work["HANDLE"] . "/\"><img src=\"" . WEB_PATH . "/images/" .
				($work["TYPE"] == "comic" ? "cartoons" : "photos") . "/" . $work["IMAGE"] . "\" class=\"entry\" border=\"0\"></a>\n";
		}
	    echo "      </div>\n";
	  }
	  echo "      <div style=\"clear:right;\"></div>\n";
	}
}
class GoAvesComic {
	private $_handle;
	private $_title;
	private $_imageFile;
	private $_datePosted;
	private $_artistFirstName;
	private $_artistLastName;
	private $_artistIcon;
	private $_artistPositions;
	private $_newerComic = array();
	private $_olderComic = array();
	private $_newestComic = array();
	function loadComic($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT cartoons.identity, cartoons.image_file, cartoons.handle, cartoons.title, " .
		"cartoons.date_posted, staff.first_name, staff.last_name, staff.icon_file, related_group, staff.icon_file AS \"artist_icon\", staff.positions " .
		"FROM cartoons JOIN staff ON cartoons.artist = " .
		"staff.identity WHERE cartoons.handle LIKE '" . $database->escapeString($path[1]) . "' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_handle = $info["handle"];
	  $this->_title = $info["title"];
	  $this->_imageFile = $info["image_file"];
	  $this->_datePosted = strtotime($info["date_posted"]);
	  $this->_artistFirstName = $info["first_name"];
	  $this->_artistLastName = $info["last_name"];
	  $this->_artistPositions = $info["positions"];
	  $this->_artistIcon = ($info["artist_icon"] != "" ? $info["artist_icon"] : "no-staff-icon.jpg");
	  $this->_newestComic = $database->querySingle("SELECT image_file, handle, title FROM cartoons WHERE identity<>'" .
		$info["identity"] . "' AND published='TRUE' ORDER BY date_posted DESC LIMIT 1", true);
	  $this->_newerComic = $database->querySingle("SELECT image_file, handle, title FROM cartoons WHERE identity<>'" .
		$info["identity"] . "' AND date_posted > '" . $info["date_posted"] . "' AND published='TRUE' ORDER BY date_posted ASC LIMIT 1", true);
	  $this->_olderComic = $database->querySingle("SELECT image_file, handle, title FROM cartoons WHERE identity<>'" .
		$info["identity"] . "' AND date_posted < '" . $info["date_posted"] . "' AND published='TRUE' ORDER BY date_posted DESC LIMIT 1", true);
	  if ($info["related_group"] != "" && ctype_digit($info["related_group"]))
	    $this->_groupInformation = getGroupInformation($database, $info["related_group"], $info["identity"], "comic");
	  else
	    $this->_groupInformation = null;
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadComic($path, $database))
	    return new GoAvesMultimedia();
	  return true;
	}
	
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "comic"; }
	function getPageSubhandle() { return $this->_handle; }
	function getBreadTrail() { return array("home" => "GoAves.com", "multimedia" => "Multimedia", "[this]" => "[Comic] " . $this->_title); }
	function getPageTitle() { return $this->_title; }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/multimedia/\">&laquo; Return to multimedia</a></div>\n";
	  echo "      <div class=\"articleSide\">\n";
	  echo "        <img src=\"" . WEB_PATH . "/images/staff/" . $this->_artistIcon . "\" class=\"staffPicture\" />\n";
	  echo "        <div class=\"authorInfo\">\n";
	  echo "          <b><a href=\"" . WEB_PATH . "/staff-portfolio/" . mb_strtolower($this->_artistFirstName . "-" . $this->_artistLastName) . "/\">" .
		$this->_artistFirstName . " " . $this->_artistLastName . "</a></b><br />\n";
	  echo "          " . $this->_artistPositions . "\n";
	  echo "        </div>\n";
	  echo "        <div style=\"clear:left;\"></div>\n";
	  echo "        <div class=\"comicNavHeader\">Newer Comic:</div>\n";
	  echo "        <div class=\"comicNavigation\">\n";
	  if (!is_bool($this->_newerComic))
	  {
		echo "        <a href=\"" . WEB_PATH . "/comic/" . $this->_newerComic["handle"] . "/\">\n";
		echo "          <img src=\"" . WEB_PATH . "/images/cartoons/" . $this->_newerComic["image_file"] . "\" class=\"comicIcon\" /><br />\n";
		echo "          " . $this->_newerComic["title"] . "\n";
		echo "        </a>\n";
		echo "        <br />\n";
	  } else
	    echo "        No newer comics\n";
	  echo "        </div>\n";
	  echo "        <div class=\"comicNavHeader\">Older Comic:</div>\n";
	  echo "        <div class=\"comicNavigation\">\n";
	  if (!is_bool($this->_olderComic))
	  {
		echo "        <a href=\"" . WEB_PATH . "/comic/" . $this->_olderComic["handle"] . "/\">\n";
		echo "          <img src=\"" . WEB_PATH . "/images/cartoons/" . $this->_olderComic["image_file"] . "\" class=\"comicIcon\" /><br />\n";
		echo "          " . $this->_olderComic["title"] . "\n";
		echo "        </a>\n";
	  } else
	    echo "        No older comics\n";
	  echo "        </div>\n";
	  if ($this->_groupInformation != null)
	    outputGroupInformation($this->_groupInformation);
	  echo "      </div>\n";
	  echo "      <div class=\"articleContainer\">\n";
	  echo "        <div class=\"title\">" . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\">" . date(LONG_DATE_FORMAT, $this->_datePosted) . "</div>\n";
	  echo "        <center><img src=\"" . WEB_PATH . "/images/cartoons/" . $this->_imageFile . "\" class=\"comic\" /></center>\n";
	  echo "      </div>\n";
	}
}
class GoAvesMultimedia {
	private $_itemsPerPage = 18;
	private $_numberItems;
	private $_currentPage;
	private $_numberPages;
	private $_items = array();
	function loadMultimedia($path, $database)
	{
	  $this->_numberItems = $database->querySingle("SELECT count(*) FROM all_published_multimedia");
	  
	  $this->_numberPages = floor($this->_numberItems % $this->_itemsPerPage == 0 ? $this->_numberItems / $this->_itemsPerPage :
		$this->_numberItems / $this->_itemsPerPage + 1);
	  if ($this->_numberPages < 1)
	    $this->_numberPages = 1;
	  $this->_currentPage = parseCurrentPage($path[1], $this->_numberPages);
	  
	  $get_items = $database->query("SELECT handle, title, type, image FROM ordered_multimedia LIMIT " . $this->_itemsPerPage .
		" OFFSET " . (($this->_currentPage - 1) * $this->_itemsPerPage));
	  while ($item = $get_items->fetchArray())
	  {
	    $this->_items[] = array("HANDLE" => $item["handle"], "TITLE" => $item["title"], "TYPE" => $item["type"], "IMAGE" => $item["image"]);
		switch ($item["type"])
		{
		  case "photo_gallery": $this->_items[sizeof($this->_items) - 1]["PAGE_HANDLE"] = "gallery"; break;
		  case "sound_clip": $this->_items[sizeof($this->_items) - 1]["PAGE_HANDLE"] = "audio"; break;
		  case "video": $this->_items[sizeof($this->_items) - 1]["PAGE_HANDLE"] = "video"; break;
		  case "cartoon": $this->_items[sizeof($this->_items) - 1]["PAGE_HANDLE"] = "comic"; break;
		  case "sound_slide": $this->_items[sizeof($this->_items) - 1]["PAGE_HANDLE"] = "sound-slides"; break;
		  default: $this->_items[sizeof($this->_items) - 1]["PAGE_HANDLE"] = "page-not-found"; break;
		}
	  }
	}
	function checkOrRedirect($path,$database)
	{
	  $this->loadMultimedia($path, $database);
	  return true;
	}
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "multimedia"; }
	function getPageSubhandle() { return null; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "All Multimedia"); }
	function getPageTitle() { return "All Multimedia"; }
	function getPageContents()
	{ 
	  generatePageNavigation("multimedia", $this->_currentPage, $this->_numberPages);
	  echo "      <center>\n";
	  foreach ($this->_items as $item)
	  {
		echo "        <div onMouseOver=\"this.className='listItemHover';\" onMouseOut=\"this.className='listItem';\" " .
			"onClick=\"window.location='" . WEB_PATH . "/" . $item["PAGE_HANDLE"] . "/" . $item["HANDLE"] . "/';\" class=\"listItem\">\n";
		echo "          <div>" . $item["TITLE"] . "</div>\n";
		echo "          <img src=\"" . WEB_PATH . "/";
		switch ($item["TYPE"])
		{
		  case "photo_gallery": echo ($item["IMAGE"] != "" ? "images/photos/" . $item["IMAGE"] : "layout/large-icon-galleries.png"); break;
		  case "sound_clip": echo "layout/large-icon-audio.png"; break;
		  case "video": echo "layout/large-icon-video.png"; break;
		  case "cartoon": echo "images/cartoons/" . $item["IMAGE"]; break;
		  default: echo "page-not-found"; break;
		}
		echo "\" />\n";
		echo "        </div>\n";
	  }
	  echo "      </center>\n";
	  
	  generatePageNavigation("multimedia", $this->_currentPage, $this->_numberPages);
	}
}
class GoAvesPhoto {
	private $_galleryHandle;
	private $_galleryTitle;
	private $_pictureIdentity;
	private $_title;
	private $_postDate;
	private $_pictureTakenDate;
	private $_captionText;
	private $_imageFile;
	private $_photographer = array();
	private $_photographerFirstName;
	private $_photographerLastName;
	private $_previousImage;
	private $_nextImage;
	private $_numPicturesInGallery;
	private $_pictureNumber;
	function loadPhoto($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT photo_galleries.handle AS \"gallery_handle\", photo_galleries.title AS \"gallery_title\", " .
		"photos.title, photos.post_date, photos.picture_taken_date, photos.caption_text, photos.image_file, staff.first_name AS \"photographer_first_name\", " .
		"staff.last_name AS \"photographer_last_name\", staff.icon_file AS \"photographer_icon\", photos.photographer_identity, photos.picture_identity, " .
		"photo_galleries.gallery_identity, related_group FROM photos JOIN " .
		"photo_galleries ON photos.gallery_identity = photo_galleries.gallery_identity LEFT JOIN staff ON photos.photographer_identity = " .
		"staff.identity WHERE photos.handle LIKE '" . 
		$database->escapeString($path[1]) . "' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_galleryHandle = $info["gallery_handle"];
	  $this->_galleryTitle = $info["gallery_title"];
	  $this->_pictureIdentity = $info["picture_identity"];
	  $this->_title = $info["title"];
	  $this->_postDate = strtotime($info["post_date"]);
	  $this->_pictureTakenDate = strtotime($info["picture_taken_date"]);
	  $this->_captionText = $info["caption_text"];
	  $this->_numPicturesInGallery = $database->querySingle("SELECT count(*) FROM photos WHERE gallery_identity='" . $info["gallery_identity"] . "'");
	  $this->_pictureNumber = $database->querySingle("SELECT count(*) FROM photos WHERE gallery_identity='" . $info["gallery_identity"] . "' AND " .
		"picture_identity <='" . $info["picture_identity"] . "'");
	  $this->_imageFile = $info["image_file"];
	  if ($info["photographer_identity"] == "FALSE")
	    $this->_photographer = false;
	  else
	    $this->_photographer = array("FIRST_NAME" => $info["photographer_first_name"], "LAST_NAME" => $info["photographer_last_name"],
			"ICON" => $info["photographer_icon"]);
	  $this->_photographerFirstName = $info["photographer_first_name"];
	  $this->_photographerLastName = $info["photographer_last_name"];
	  
	  $prev_photo = $database->querySingle("SELECT handle, title, icon_file FROM photos WHERE " .
		"picture_identity < '" . $info["picture_identity"] . "' AND gallery_identity='" . $info["gallery_identity"] . "' ORDER BY " .
		"picture_identity DESC LIMIT 1", true);
	  if ($prev_photo === false)
	    $this->_previousImage = null;
	  else
	    $this->_previousImage = array("HANDLE" => $prev_photo["handle"], "TITLE" => $prev_photo["title"], "ICON" => $prev_photo["icon_file"]);
	  $next_photo = $database->querySingle("SELECT handle, title, icon_file FROM photos WHERE " .
		"picture_identity > '" . $info["picture_identity"] . "' AND gallery_identity='" . $info["gallery_identity"] . "' ORDER BY " .
		"picture_identity ASC LIMIT 1", true);
	  if ($next_photo === false)
	    $this->_nextImage = null;
	  else
	    $this->_nextImage = array("HANDLE" => $next_photo["handle"], "TITLE" => $next_photo["title"], "ICON" => $next_photo["icon_file"]);
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadPhoto($path, $database))
	    return new GoAvesMultimedia();
	  return true;
	}
	
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "photo"; }
	function getPageSubhandle() { return $this->_handle; }
	function getBreadTrail() { return array("home" => "GoAves.com", "multimedia" => "All Multimedia", "gallery/" . $this->_galleryHandle =>
		"[Photo Gallery] " . $this->_galleryTitle, "[this]" => "[Photo] " . $this->_handle); }
	function getPageTitle() { return $this->_title; }

	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/gallery/" . $this->_galleryHandle . "/\">&laquo; Return to gallery</a></div>\n";
		
	  echo "      <div class=\"articleSide\">\n";
	  if ($this->_photographer !== false)
	  {
	    echo "        <img src=\"" . WEB_PATH . "/images/staff/" . $this->_photographer["ICON"] . "\" class=\"staffPicture\" />\n";
	    echo "        <div class=\"authorInfo\">\n";
	    echo "          <b><a href=\"" . WEB_PATH . "/staff-portfolio/" . mb_strtolower($this->_photographer["FIRST_NAME"] . "-" . 
			$this->_photographer["LAST_NAME"]) . "/\">" . $this->_photographer["FIRST_NAME"] . " " . $this->_photographer["LAST_NAME"] . 
			"</a></b><br />\n";
	    //echo "          " . $this->_artistPositions . "\n";
	    echo "        </div>\n";
	    echo "        <div style=\"clear:left;\"></div>\n";
	  }
	  
	  echo "        <div class=\"galleryHeader\">Photo " . $this->_pictureNumber . " of " . $this->_numPicturesInGallery . 
		" in<br /><a href=\"" . WEB_PATH . "/gallery/" .
		$this->_galleryHandle . "/\">" . $this->_galleryTitle . "</a></div>\n";
	  echo "        <div class=\"photoNavHeader\">Previous:</div>\n";
	  echo "        <div class=\"photoNavigation\">\n";
	  if ($this->_previousImage != null)
	  {
		echo "        <a href=\"" . WEB_PATH . "/photo/" . $this->_previousImage["HANDLE"] . "/\">\n";
		echo "          <img src=\"" . WEB_PATH . "/images/photos/" . $this->_previousImage["ICON"] . "\" class=\"photo\" /><br />\n";
		echo "          " . $this->_previousImage["TITLE"] . "\n";
		echo "        </a>\n";
		echo "        <br />\n";
	  }
	  echo "        </div>\n";
	  echo "        <div class=\"photoNavHeader\">Next:</div>\n";
	  echo "        <div class=\"photoNavigation\">\n";
	  if ($this->_nextImage != null)
	  {
		echo "        <a href=\"" . WEB_PATH . "/photo/" . $this->_nextImage["HANDLE"] . "/\">\n";
		echo "          <img src=\"" . WEB_PATH . "/images/photos/" . $this->_nextImage["ICON"] . "\" class=\"photo\" /><br />\n";
		echo "          " . $this->_nextImage["TITLE"] . "\n";
		echo "        </a>\n";
	  }
	  echo "        </div>\n";
	  echo "      </div>\n";
	
	  echo "      <div class=\"articleContainer\">\n";
	  echo "        <div class=\"title\">" . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\">Posted " . date(LONG_DATE_FORMAT, $this->_postDate) . "</div>\n";
	  
	  echo "        <img src=\"" . WEB_PATH . "/images/photos/" . $this->_imageFile . "\" class=\"photo\" border=\"0\"/>";
	  echo "\n";
	  if ($this->_captionText != "")
	    echo "        <div class=\"caption\">" . $this->_captionText . "</div>\n";
		
	  /*generateArbitraryPageNavigation(($this->_previousImage != null ? "photo/" . $this->_previousImage["HANDLE"] : null),
		($this->_previousImage != null ? $this->_previousImage["TITLE"] : ""), $this->_title, ($this->_nextImage != null ? "photo/" .
		$this->_nextImage["HANDLE"] : null), ($this->_nextImage != null ? $this->_nextImage["TITLE"] : ""));*/
	  echo "      </div>\n";
	}
}
class GoAvesAudio {
	private $_handle;
	private $_title;
	private $_datePublished;
	private $_soundFile;
	private $_description;
	private $_audioTranscript;
	private $_artist;
	private $_groupInformation;
	function loadAudio($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT sound_identity, sound_clips.handle, sound_clips.title, sound_clips.date_published, sound_clips.sound_file, " .
		"sound_clips.description, sound_clips.audio_transcript, sound_clips.artist, sound_clips.related_group FROM sound_clips " .
		"WHERE sound_clips.handle LIKE '" . $database->escapeString($path[1]) . "' AND published='TRUE' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_handle = $info["handle"];
	  $this->_title = $info["title"];
	  $this->_datePublished = strtotime($info["date_published"]);
	  $this->_soundFile = $info["sound_file"];
	  $this->_description = $info["description"];
	  $this->_audioTranscript = $info["audio_transcript"];
	  $this->_artist = $info["artist"];
	  if ($info["related_group"] != "" && ctype_digit($info["related_group"]))
	    $this->_groupInformation = getGroupInformation($database, $info["related_group"], $info["video_identity"], "sound_clip");
	  else
	    $this->_groupInformation = null;
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadAudio($path, $database))
	    return new GoAvesMultimedia();
	  return true;
	}
	
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "audio"; }
	function getPageSubhandle() { return $this->_handle; }
	function getPageTitle() { return $this->_title; }
	function getBreadTrail() { return array("home" => "GoAves.com", "multimedia" => "All Multimedia", "[Audio] " . $this->_title); }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/multimedia/\">&laquo; Return to multimedia</a></div>\n";
	  
	  echo "      <div class=\"articleSide\">\n";
	  if ($this->_groupInformation != null)
	    outputGroupInformation($this->_groupInformation);
	  echo "      </div>\n";
	  
	  echo "      <div class=\"articleContainer\">\n";
	  echo "        <div class=\"title\">" . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\">Posted " . date(LONG_DATE_FORMAT, $this->_datePublished) . "</div>\n";
	  echo "        <div class=\"audioContainer\">\n";
	  echo "          <div id=\"audio_player\" class=\"audio\">You must have flash installed in order to listen to this sound file.</div>\n";
	  echo "        </div>\n";
	  echo "        <script src=\"" . WEB_PATH . "/layout/audio-player/audio-player.js\"></script>\n"; 
      echo "        <script type=\"text/javascript\">\n";
	  echo "          AudioPlayer.setup(\"" . WEB_PATH . "/layout/audio-player/player.swf\", {\n";
      echo "            width: 600,\n";
	  echo "            animation: \"no\",\n";
      echo "            transparentpagebg: \"yes\"\n";
      echo "          });\n";
      echo "          AudioPlayer.embed(\"audio_player\", {\n";
	  echo "            soundFile: \"" . WEB_PATH . "/components/audio/" . $this->_soundFile . "\",\n";
	  echo "            titles: \"" . $this->_title . "\",\n";
	  echo "            artists: \"" . $this->_artist . "\",\n";
	  echo "            autostart: \"no\"";
	  echo "          });\n";
      echo "        </script>\n";
	  if ($this->_description != "")
	    echo "          <div class=\"caption\">" . $this->_description . "</div>\n";
	  if ($this->_audioTranscript != "")
	  {
	    echo "          <div class=\"audioTranscript\">\n";
		echo "            " . $this->_audioTranscript . "\n";
		echo "          </div>\n";
	  }
	  echo "      </div>\n";
	}
}
class GoAvesContact {
	private $_contacts = array();
	function loadContacts($database)
	{
	  $get_contacts = $database->query("SELECT contact_information.name, contact_information.reason, contact_information.email, groups.title, " .
		"groups.icon_file FROM contact_information JOIN groups ON contact_information.group_identity = groups.group_identity ORDER BY " .
		"groups.group_identity ASC, name ASC");
	  while ($contact = $get_contacts->fetchArray())
	    $this->_contacts[] = array("NAME" => $contact["name"], "REASON" => $contact["reason"], "EMAIL" => $contact["email"], 
			"GROUP_TITLE" => $contact["title"], "ICON_FILE" => $contact["icon_file"]);
	}
	function checkOrRedirect($path, $database)
	{
	  $this->loadContacts($database);
	  return true;
	}
	function getMainTab() { return "home"; }
	function getPageHandle() { return "contact"; }
	function getPageSubhandle() { return null; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "Contact Information"); }
	function getPageTitle() { return "Contact Information"; }
	function getPageContents()
	{
	  $previous_group = null;
	  echo "      <div class=\"contactContainer\">\n";
	  foreach ($this->_contacts as $contact)
	  {
	    if ($previous_group == null || $contact["GROUP_TITLE"] != $previous_group)
		{
		  if ($previous_group != null)
		  {
		    echo "          <div style=\"clear:both;\"></div>\n";
			echo "        </div>\n";
		  }
		  $previous_group = $contact["GROUP_TITLE"];
		  echo "        <div class=\"group\">\n";
		  echo "          <img src=\"" . WEB_PATH . "/images/groups/" . $contact["ICON_FILE"] . "\" class=\"icon\" />\n";
		  echo "          <div class=\"title\">" . $contact["GROUP_TITLE"] . "</div>\n";
		}
		echo "          <div class=\"entry\">\n";
		echo "            <div class=\"name\">" . $contact["NAME"] . "</div>\n";
		echo "            <div class=\"reason\">" . $contact["REASON"] . "</div>\n";
		echo "            <a href=\"mailto:" . $contact["EMAIL"] . "\">" . $contact["EMAIL"] . "</a>\n";
		echo "          </div>\n";
	  }
		    echo "          <div style=\"clear:both;\"></div>\n";
			echo "        </div>\n";
	  echo "      </div>\n";
	}
}
class GoAvesYearbook {
	private $_pageToLoad;
	private $_title;
	private $_faq = array();
	private $_confirmation = array();
	function loadYearbook($path, $database)
	{
	  if (isset($path[1]) && $path[1] == "confirmation")
	  {
	    $this->_pageToLoad = "confirmation";
	    $confirmations = $database->query("SELECT first_name, last_name, quantity, date_purchased, grade FROM yearbook_confirmations ORDER BY " .
			"last_name, first_name ASC");
		while ($confirmation = $confirmations->fetchArray())
		  $this->_confirmation[] = array("NAME" => $confirmation["first_name"] . " " . $confirmation["last_name"], "QUANTITY" => $confirmation["quantity"],
			"DATE_PURCHASED" => $confirmation["date_purchased"], "GRADE" => $confirmation["grade"]);
	  } else
	  {
	    $this->_pageToLoad = "faq";
		$questions = $database->query("SELECT question, answer, requires_webroot FROM yearbook_faq_questions ORDER BY question_identity");
		while ($question = $questions->fetchArray())
		  $this->_faq[] = array("QUESTION" => $question["question"], "ANSWER" => ($question["requires_webroot"] ? 
			preg_replace("[\{WEB_PATH\}]", WEB_PATH, $question["answer"]) : $question["answer"]));
	  }
	  $this->_title = ($this->_pageToLoad == "faq" ? "Yearbook FAQ" : "Confirmation List");
	}
	function checkOrRedirect($path, $database)
	{
	  $this->loadYearbook($path, $database);
	  return true;
	}
	function getMainTab() { return "home"; }
	function getPageHandle() { return "yearbook"; }
	function getPageSubhandle() { return null; }
	function getBreadTrail() { return ($this->_pageToLoad === "confirmation" ? array("home" => "GoAves.com", "yearbook" => "Yearbook FAQ",
		"[this]" => "Confirmation List") : array("home" => "GoAves.com", "[this]" => "Yearbook FAQ")); }
	function getPageTitle() { return $this->_title; }
	function getPageContents()
	{
	  if ($this->_pageToLoad == "faq")
	  {
	    echo "      <div class=\"yearbookHeader\">\n";
	    echo "        <div class=\"header\">Yearbook</div>\n";
	    echo "        <div class=\"subheader\">Questions with Mrs. Cheralyn Jardine, Yearbook Advisor</div>\n";
		echo "        <div class=\"navigation\">.: <b>Question and Answer</b> &ndash; <a href=\"" . WEB_PATH . "/yearbook/confirmation/\">" .
			"Confirmation Listings</a> &ndash; <a href=\"" . WEB_PATH . "/documents/yearbook-order-form.htm\" target=\"_blank\">Order Form</a> :.</div>\n";
		echo "      </div>\n";
		echo "      <div class=\"yearbookContainer\">\n";
		
		foreach ($this->_faq as $question)
		{
		  echo "        <div class=\"entry\">\n";
		  echo "          <div class=\"question\"><b>Q:</b> " . $question["QUESTION"] . "</div>\n";
		  echo "          <div class=\"answer\"><b>A:</b> " . $question["ANSWER"] . "</div>\n";
		  echo "        </div>\n";
		}
		echo "      </div>\n";
	  } else
	  {
	    echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/yearbook/\">&laquo; Return to Yearbook Home</a></div>\n";
	    echo "      <div class=\"yearbookHeader\">\n";
		echo "        <div class=\"header\">Yearbook</div>\n";
		echo "        <div class=\"subheader\">Confirmation Listing</div>\n";
		echo "        <div class=\"navigation\">.: <a href=\"" . WEB_PATH . "/yearbook/\">Question and Answer</a> &ndash; <b>" .
			"Confirmation Listings</b> &ndash; <a href=\"" . WEB_PATH . "/documents/yearbook-order-form.htm\" target=\"_blank\">Order Form</a> :.</div>\n";
		echo "      </div>\n";
		
		if (sizeof($this->_confirmation) > 0)
		{
		  echo "      <table class=\"yearbookConfirmations\">\n";
		  echo "        <tr class=\"header\"><td>Name</td><td>Grade</td><td>Quantity</td><td>Date Ordered</td></tr>\n";
		  foreach ($this->_confirmation as $order)
		  {
		    echo "      <tr class=\"row\" onMouseOver=\"this.className='rowHover';\" onMouseOut=\"this.className='row';\"><td>" .
			  $order["NAME"] . "</td><td>" . $order["GRADE"] . "</td><td>" .
			  $order["QUANTITY"] . "</td><td class=\"last\">" . ($order["DATE_PURCHASED"] != "" ? date(DATE_FORMAT, strtotime($order["DATE_PURCHASED"])) :
			  "Unlisted") . "</td></tr>\n";
		  }
		  echo "      </table>\n";
		} else
		  echo "      <center>No records have been posted yet. Check back soon!</center>\n";
	  }
	  
	}
}
class GoAvesLeaf {
	private $_itemsPerPage = 10;
	private $_currentPage = 0;
	private $_numberPages = 0;
	private $_issues = array();
	function load($path, $database)
	{
	  $numberItems = $database->querySingle("SELECT count(*) FROM leaf_issuu");
	  
	  $this->_numberPages = floor($numberItems % $this->_itemsPerPage == 0 ? $numberItems / $this->_itemsPerPage :
		$numberItems / $this->_itemsPerPage + 1);
	  if ($this->_numberPages < 1)
	    $this->_numberPages = 1;
	  $this->_currentPage = parseCurrentPage($path[1], $this->_numberPages);
	  
	  $get_issues = $database->query("SELECT post_date, issue_name, handle, bw_cover, color_cover FROM leaf_issuu ORDER BY post_date DESC LIMIT 10");
	  while ($issue = $get_issues->fetchArray())
	    $this->_issues[] = array("TITLE" => $issue["issue_name"], "HANDLE" => $issue["handle"], "POST_DATE" => strtotime($issue["post_date"]),
			"BW_COVER" => $issue["bw_cover"], "COLOR_COVER" => $issue["color_cover"]);
	}
	function checkOrRedirect($path, $database)
	{
	  $this->load($path, $database);
	  return true;
	}
	function getMainTab() { return "leaf"; }
	function getPageHandle() { return "leaf"; }
	function getPageSubhandle() { return null; }
	function getPageTitle() { return "The Leaf Archives"; }
	function getBreadTrail() { return null; }
	function getPageContents()
	{
	  generatePageNavigation("leaf", $this->_currentPage, $this->_numberPages);
	  echo "      <center>\n";
	  foreach ($this->_issues as $issue)
	  {
		echo "        <div onMouseOver=\"this.className='listItemHover';\" onMouseOut=\"this.className='listItem';\" " .
			"onClick=\"window.location='" . WEB_PATH . "/leaf-issue/" . $issue["HANDLE"] . "/';\" class=\"listItem\">\n";
		echo "          <div>" . $issue["TITLE"] . "</div>\n";
		echo "          <img src=\"" . WEB_PATH . "/images/leaf_covers/" . $issue["BW_COVER"] . "\" onMouseOver=\"this.src='" . WEB_PATH .
			"/images/leaf_covers/" . $issue["COLOR_COVER"] . "';\" onMouseOut=\"this.src='" . WEB_PATH . "/images/leaf_covers/" .
			$issue["BW_COVER"] . "';\" />\n";
		echo "        </div>\n";
	  }
	  echo "      </center>\n";
	  generatePageNavigation("leaf", $this->_currentPage, $this->_numberPages);
	}
}
class GoAvesPoll {
	private $_justVoted = false;
	private $_responseText = null;
	function vote($database)
	{
	  if (!isset($_SERVER['REMOTE_USER']) || $_SERVER['REMOTE_USER'] == null || $_SERVER['REMOTE_USER'] == "")
	  {
	    $this->_justVoted = false;
		$this->_responseText = "Unable to record user information for poll response.";
		return;
	  }
	  $user_information = $database->escapeString($_SERVER['REMOTE_USER']);
		
	  $poll = $database->escapeString($_POST["poll"]);
	  if ($database->querySingle("SELECT count(*) FROM polls WHERE identity='" . $poll . "'") != 1)
	  {
	    $this->_justVoted = false;
		$this->_responseText = "The poll you voted for does not exist.";
		return;
	  }
		  
	  $vote = $database->escapeString($_POST["poll-" . $poll]);
	  if ($database->querySingle("SELECT count(*) FROM poll_responses WHERE response_identity='" . $vote . "' AND poll_identity='" . $poll . "'") != 1)
	  {
	    $this->_justVoted = false;
		$this->_responseText = "Selected response is not valid.";
		return;
	  }
		
	  if ($database->querySingle("SELECT count(*) FROM poll_votes WHERE poll_identity='" . $poll . "' AND user='" . $user_information . "'") == 1)
	  {
	    $this->_justVoted = false;
		$this->_responseText = "You have already voted in this poll.";
		return;
	  }
		
	  $vote_counted = $database->query("INSERT INTO poll_votes(poll_identity, response_identity, user, ip_address) VALUES('" . $poll . "','" . $vote .
		"','" . $user_information . "','" . $_SERVER['REMOTE_ADDR'] . "')");
	  if (!$vote_counted)
	  {
	    $this->_justVoted = false;
		$this->_responseText = "Unable to count your vote (error in the program code)";
		return;
	  }
	  $this->_justVoted = true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (isset ($_POST["poll"]))
	    $this->vote($database);
	  return true;
	}
	function getMainTab() { return "goaves"; }
	function getPageHandle() { return "poll"; }
	function getPageSubhandle() { return null; }
	function getPageTitle($path, $database)
	{
	  return "Polls";
	}
	function getPageContents($path, $database)
	{
	  echo "      <div class=\"mainColumnBreadcrumbs\">" . generateBreadcrumbs(array("home" => "GoAves.com", "[this]" => "Polls")) . "</div>\n";
	  
	  if ($this->_justVoted)
	    echo "      <div class=\"success\"><b>Success.</b> Your vote has been counted.</div>\n";
	  else if ($this->_responseText !== null)
	    echo "      <div class=\"error\"><b>Error.</b> " . $this->_responseText . "</div>\n";
		
	  if (!isset($_SERVER['REMOTE_USER']) || $_SERVER['REMOTE_USER'] == null || $_SERVER['REMOTE_USER'] == "")
	    echo "      <div class=\"notice\"><b>Notice.</b> You cannot vote, as we are unable to get your user information.</div>\n";
	  $current_polls = $database->query("SELECT identity, poll_handle, poll_question FROM polls WHERE date('now') BETWEEN date_start AND date_end");
	  if ($current_polls->numberRows() == 0)
	    echo "There are no currently active polls";
	  else
	  {
	    while ($poll = $current_polls->fetchArray())
		{
		  echo "      <div class=\"pollContainer\" id=\"poll-container-" . $poll["identity"] . "\">\n";
		  if (!isset($_SERVER['REMOTE_USER']) || $_SERVER['REMOTE_USER'] == null || $_SERVER['REMOTE_USER'] == "")
		    $display_results = true;
		  else
		    $display_results = ($database->querySingle("SELECT count(*) FROM poll_votes WHERE poll_identity='" . $database->escapeString($poll["identity"]) .
			  "' AND user='" . $database->escapeString($_SERVER['REMOTE_USER']) . "'") > 0);
		  $poll_responses = $database->query("SELECT response_identity, response FROM poll_responses WHERE poll_identity='" . $poll["identity"] .
			"' ORDER BY response_identity ASC");
		  echo "      <div class=\"noticePanelHeader\">Current Poll</div>\n";
		  echo "      <div class=\"pollHeader\">" . $poll["poll_question"] . "</div>\n";
		  if (!$display_results)
		  {
			echo "      <form method=\"post\" action=\"" . WEB_PATH . "/polls/\" style=\"margin:0px;\">\n";
			echo "        <input type=\"hidden\" name=\"poll\" value=\"" . $poll["identity"] . "\" />\n";
			while ($response = $poll_responses->fetchArray())
			  echo "      <input type=\"radio\" name=\"poll-" . $poll["identity"] . "\" id=\"poll-" . $poll["identity"] . "-" . $response["response_identity"] .
				"\" value=\"" . $response["response_identity"] . "\" /><label for=\"poll-" . $poll["identity"] . "-" . $response["response_identity"] .
				"\">" . $response["response"] . "</label><br />\n";
			echo "        <center><input type=\"submit\" class=\"pollButton\" name=\"poll-vote\" value=\"Vote\" /></center>\n";
			echo "      </form>\n";
		  } else
		  {
		    $all_votes = $database->querySingle("SELECT count(*) FROM poll_votes WHERE poll_identity='" . $poll["identity"] . "'");
			while ($response = $poll_responses->fetchArray())
			{
			  $num_votes = $database->querySingle("SELECT count(*) FROM poll_votes WHERE poll_identity='" . $poll["identity"] . "' AND response_identity='" .
				$response["response_identity"] . "'");
			  echo "      <div style=\"margin-left:5px;\">\n";
			  echo "        <b>" . $response["response"] . "</b> &ndash; " . $num_votes . " vote" . ($num_votes != 1 ? "s" : "");
			  echo "        <div class=\"pollResultsContainer\">\n";
			  if ($num_votes > 0 && $all_votes > 0)
			    echo "          <div class=\"pollResultsBar\" style=\"width:" . floor(($num_votes / $all_votes) * 100) . "%;\"></div>\n";
			  echo "        </div>\n";
			  echo "      </div>\n";
			}
		  }
		  echo "      </div>\n";
	    }
	  }
	}
}
class GoAvesSports {
	private $_season;
	private $_seasonTitle;
	private $_teams = array();
	function determineSeason($path)
	{
	  if (isset($path[1]) && $path[1] != "" && $path[1] != null)
	  {
	    $param = mb_strtolower($path[1]);
		if ($param == "fall" || $param == "winter" || $param == "spring")
		{
		  $this->_season = $param;
		  return;
		}
	  }
	  $month = date("n");
	  if ($month >= 7 && $month < 11)
		$this->_season = "fall";
	  else if ($month >= 11 || $month < 3)
		$this->_season = "winter";
	  else if ($month >= 3 && $month <= 6)
		$this->_season = "spring";
	}
	function checkOrRedirect($path, $database)
	{
	  $this->determineSeason($path);
	  $this->_seasonTitle = ucfirst($this->_season);
	  
	  $get_sports_teams = $database->query("SELECT groups.handle, groups.title, groups.icon_file FROM groups WHERE " .
		"type='" . $database->escapeString($this->_season) . "_sport' ORDER BY groups.title ASC");
	  while ($team = $get_sports_teams->fetchArray())
	    $this->_teams[] = array("TITLE" => $team["title"], "HANDLE" => $team["handle"], "ICON" => $team["icon_file"]);
	  return true;
	}
	function getMainTab() { return "sports"; }
	function getPageHandle() { return "sports"; }
	function getPageSubhandle() { return $this->_season; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => $this->_seasonTitle . " Sports"); }
	function getPageTitle() { return $this->_seasonTitle . " Sports"; }
	function getPageContents()
	{
	  echo "      <div class=\"sportsListContainer\">\n";
	  echo "        <div class=\"header\">" . $this->_seasonTitle . " Sports</div>\n";
	  echo "        <div class=\"navigation\">" . ($this->_season == "fall" ? "Fall Sports" :
		"<a href=\"" . WEB_PATH . "/sports/fall/\">Fall Sports</a>") . " &bull; " . ($this->_season == "winter" ? "Winter Sports" :
		"<a href=\"" . WEB_PATH . "/sports/winter/\">Winter Sports</a>") . " &bull; " . ($this->_season == "spring" ? "Spring Sports" :
		"<a href=\"" . WEB_PATH . "/sports/spring/\">Spring Sports</a>") . "</div>\n";
	  if (sizeof($this->_teams) == 0)
	    echo "        There are no sports teams registered for the " . $this->_seasonTitle . " season yet. Check back soon.\n";
	  else
	  {
	    foreach ($this->_teams as $team)
		{
		  echo "        <div class=\"listItem\" onMouseOver=\"this.className='listItemHover';\" " .
			"onMouseOut=\"this.className='listItem';\" onClick=\"window.location='" . WEB_PATH . "/group/" . $team["HANDLE"] . "/';\">\n";
		  echo "          <div>" . $team["TITLE"] . "</div>\n";
		  echo "          <img src=\"" . WEB_PATH . "/images/groups/" . $team["ICON"] . "\" />\n";
		  echo "        </div>\n";
		}
	  }
	  echo "      </div>\n";
	}
}
class GoAvesLeafIssue {
	private $_title;
	private $_handle;
	private $_documentID;
	private $_documentName;
	private $_loadingText;
	private $_postDate;
	function loadIssue($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT document_id, document_name, loading_text, post_date, issue_name, handle FROM leaf_issuu WHERE handle LIKE '" .
		$database->escapeString($path[1]) . "' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_documentID = $info["document_id"];
	  $this->_docmentName = $info["document_name"];
	  $this->_loadingText = $info["loading_text"];
	  $this->_postDate = strtotime($info["post_date"]);
	  $this->_title = $info["issue_name"];
	  $this->_handle = $info["handle"];
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadIssue($path, $database))
	    return new GoAvesLeaf();
	  return true;
	}
	function getMainTab() { return "leaf"; }
	function getPageHandle() { return "leaf-issue"; }
	function getPageSubhandle() { return $this->_handle; }
	function getBreadTrail() { return array("leaf" => "<i>The Leaf</i> Archives", "[this]" => $this->_title); }
	function getPageTitle() { return "The Leaf"; }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/leaf/\">&laquo; Return to <i>The Leaf</i> archives</a></div>\n";
	  echo "      <div class=\"articleContainer\">\n";
	  echo "        <div class=\"title\">" . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\">Distributed in " . date("F Y", $this->_postDate) . "</div>\n";
	  echo "        <div class=\"leafIssue\">\n";
	  echo "          <object style=\"width:600px;height:464px\">\n";
	  echo "            <param name=\"movie\" value=\"http://static.issuu.com/webembed/viewers/style1/v1/IssuuViewer.swf?mode=embed" .
		"&amp;layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&amp;showFlipBtn=true&amp;documentId=" .
		$this->_documentID . "&amp;docName=" . $this->_documentName . "&amp;username=TheLeaf&amp;loadingInfoText=" .
		$this->_loadingText . "&amp;et=1280256249565&amp;er=66\" />\n";
      echo "            <param name=\"allowfullscreen\" value=\"true\"/>\n";
	  echo "            <param name=\"menu\" value=\"false\"/>\n";
	  echo "            <embed src=\"http://static.issuu.com/webembed/viewers/style1/v1/IssuuViewer.swf\" type=\"application/x-shockwave-flash\" " .
		"allowfullscreen=\"true\" menu=\"false\" style=\"width:600px;height:464px\" flashvars=\"mode=embed&amp;layout=" .
		"http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&amp;showFlipBtn=true&amp;documentId=" . $this->_documentID . 
		"&amp;docName=" . $this->_documentName . "&amp;username=TheLeaf&amp;loadingInfoText=" . $this->_loadingText .
		"&amp;et=1280256249565&amp;er=66\" />\n";
	  echo "          </object><br />\n";
	  echo "          <a href=\"http://issuu.com/TheLeaf/docs/" . $this->_documentName .
		"?mode=embed&amp;layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&amp;showFlipBtn=true\" target=\"_blank\">Open publication</a>" .
		" - Free <a href=\"http://issuu.com\" target=\"_blank\">publishing</a> - <a href=\"http://issuu.com/search?q=sycamore\" target=\"_blank\">" .
		"More sycamore</a>\n";
	  echo "        </div>\n";
	  echo "      </div>\n";
	}
}
class GoAvesSoundSlides {
	private $_handle;
	private $_title;
	private $_datePublished;
	private $_soundFile;
	private $_slides = array();
	private $_groupInformation;
	function loadSoundSlides($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT slideshow_identity, handle, title, post_date, related_group, sound_file FROM sound_slides " .
		"WHERE handle LIKE '" . $database->escapeString($path[1]) . "' AND published='TRUE' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_handle = $info["handle"];
	  $this->_title = $info["title"];
	  $this->_datePublished = strtotime($info["post_date"]);
	  $this->_soundFile = $info["sound_file"];
	  $get_slides = $database->query("SELECT image_file, start_time, caption FROM sound_slides_slides WHERE slideshow_identity='" .
		$info["slideshow_identity"] . "' ORDER BY start_time ASC");
	  while ($slide = $get_slides->fetchArray())
	    $this->_slides[] = array("IMAGE_FILE" => $slide["image_file"], "START" => $slide["start_time"], "CAPTION" => $slide["caption"]);
	  if ($info["related_group"] != "" && ctype_digit($info["related_group"]))
	    $this->_groupInformation = getGroupInformation($database, $info["related_group"], $info["slideshow_identity"], "sound_slide");
	  else
	    $this->_groupInformation = null;
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadSoundSlides($path, $database))
	    return new GoAvesMultimedia();
	  return true;
	}
	
	function getMainTab() { return "multimedia"; }
	function getPageHandle() { return "sound-slides"; }
	function getPageSubhandle() { return $this->_handle; }
	function getPageTitle() { return $this->_title; }
	function getBreadTrail() { return array("home" => "GoAves.com", "multimedia" => "All Multimedia", "[this]" => "[Sound Slides] " . $this->_title); }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/multimedia/\">&laquo; Return to multimedia</a></div>\n";
	  
	  echo "      <div class=\"articleSide\">\n";
	  if ($this->_groupInformation != null)
	    outputGroupInformation($this->_groupInformation);
	  echo "      </div>\n";
	  
	  echo "      <div class=\"articleContainer\">\n";
	  echo "        <div class=\"title\">" . $this->_title . "</div>\n";
	  echo "        <div class=\"subtitle\">Posted " . date(LONG_DATE_FORMAT, $this->_datePublished) . "</div>\n";
	  echo "        <div class=\"audioContainer\">\n";
	  echo "          <div style=\"background-image:url('" . WEB_PATH . "/images/sound_slides/" . $this->_slides[0]["IMAGE_FILE"] . "');\" " .
		"class=\"soundSlideImage\" id=\"sound-slide-image\">\n";
	  echo "            <div class=\"caption\" id=\"sound-slide-caption\">" . $this->_slides[0]["CAPTION"] . "</div>\n";
	  echo "          </div>\n";
	  echo "          <div id=\"audio_player\" class=\"audio\">You must have flash installed in order to listen to this sound file.</div>\n";
	  echo "        </div>\n";
	  echo "        <script src=\"" . WEB_PATH . "/layout/audio-player/audio-player.js\"></script>\n"; 
      echo "        <script type=\"text/javascript\">\n";
	  echo "          AudioPlayer.setup(\"" . WEB_PATH . "/layout/audio-player/player.swf\", {\n";
      echo "            width: 600,\n";
	  echo "            animation: \"no\",\n";
      echo "            transparentpagebg: \"yes\"\n";
      echo "          });\n";
      echo "          AudioPlayer.embed(\"audio_player\", {\n";
	  echo "            soundFile: \"" . WEB_PATH . "/components/audio/" . $this->_soundFile . "\",\n";
	  echo "            titles: \"" . $this->_title . "\",\n";
	  echo "            artists: \"" . $this->_artist . "\",\n";
	  echo "            autostart: \"no\"";
	  echo "          });\n";
	  echo "alert('hi');\n";
	  echo "          var player = AudioPlayer.getPlayer(0);\n";
	  echo "alert(player);\n";
	  echo "          alert(player.getVolume());\n";
	  echo "          AudioPlayer.addListener(\"audio_player\",\"change\", function() { alert('CLICK'); });\n";
	  echo "          alert('hi');\n";
	  echo "          var sound_slides = new Array();\n";
	  for ($output_slide = 0; $output_slide < sizeof($this->_slides); $output_slide++)
	    echo "          sound_slides[" . $output_slide . "] = new Array('" . WEB_PATH . "/images/sound_slides/" . 
			$this->_slides[$output_slide]["IMAGE_FILE"] . "','" . $this->_slides[$output_slide]["START"] . "','" .
			addslashes($this->_slides[$output_slide]["CAPTION"]) . "');\n";
      echo "        </script>\n";
	  echo "      </div>\n";
	}
}
class GoAvesGroup {
	private $_handle;
	private $_groupName;
	private $_imageFile;
	private $_iconFile;
	private $_description;
	private $_groupContent = array();
	private $_isSport;
	private $_sportSeason;
	private $_events = array();
	private $_groupListingPage;
	function loadGroup($path, $database)
	{
	  if (!isset($path[1]) || $path[1] == "")
	    return false;
	  $info = $database->querySingle("SELECT group_identity, handle, title, image_file, icon_file, description, type " .
		"FROM groups WHERE handle LIKE '" . $database->escapeString($path[1]) . "' LIMIT 1", true);
	  if ($info === false)
	    return false;
	  $this->_handle = $info["handle"];
	  $this->_groupName = $info["title"];
	  $this->_imageFile = $info["image_file"];
	  $this->_iconFile = $info["icon_file"];
	  $this->_description = $info["description"];
	  $this->_isSport = !($info["type"] == "club");
	  if ($info["type"] == "fall_sport")
	    $this->_sportSeason = "Fall";
	  else if ($info["type"] == "winter_sport")
	    $this->_sportSeason = "Winter";
	  else if ($info["type"] == "spring_sport")
	    $this->_sportSeason = "Spring";

         $groups_before_this = $database->querySingle("SELECT count(*) FROM groups WHERE image_file <> '' AND image_file IS NOT null AND " .
		"icon_file <> '' AND icon_file IS NOT null AND title < '" . $database->escapeString($info["title"]) . "'");
         $this->_groupListingPage = ($groups_before_this == 0 ? 1 : floor($groups_before_this / GROUPS_PER_LISTING_PAGE) + 1);

	  $get_content = $database->query("SELECT title, handle, type FROM group_content WHERE related_group='" . $info["group_identity"] .
		"' ORDER BY datestamp DESC");
	  while ($content = $get_content->fetchArray())
	  {
	    $this->_groupContent[] = array("HANDLE" => $content["handle"], "TITLE" => utf8_encode($content["title"]));
		switch ($content["type"])
		{
		  case "article":
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_HANDLE"] = "article";
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_TITLE"] = "Article";
			break;
		  case "photo_gallery":
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_HANDLE"] = "gallery";
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_TITLE"] = "Photo Gallery";
			break;
		  case "sound_clip":
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_HANDLE"] = "audio";
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_TITLE"] = "Audio";
			break;
		  case "video":
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_HANDLE"] = "video";
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_TITLE"] = "Video";
			break;
		  case "cartoon":
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_HANDLE"] = "comic";
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_TITLE"] = "Comic";
			break;
		  case "sound_slide":
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_HANDLE"] = "sound-slides";
			$this->_groupContent[sizeof($this->_groupContent) - 1]["PAGE_TITLE"] = "Sound Slides";
			break;
		}
	  }
	  
	  $get_events = $database->query("SELECT date, title FROM calendar WHERE related_group='" . $info["group_identity"] . "'");
	  while ($event = $get_events->fetchArray())
	    $this->_events[] = array_merge(array("TITLE" => $event["title"]), parse_time_pieces($event["date"]));
	  usort($this->_events, "sort_by_time_pieces");
	  return true;
	}
	function checkOrRedirect($path, $database)
	{
	  if (!$this->loadGroup($path, $database))
	    return new GoAvesGroupListing();
	  return true;
	}
	function getMainTab() { return "group-listing"; }
	function getPageHandle() { return "group"; }
	function getPageSubhandle() { return $this->_handle; }
	function getPageTitle() { return $this->_groupName; }
	function getBreadTrail() { return array("home" => "GoAves.com", "group-listing" => "Group Listing", "[this]" => $this->_groupName); }
	function getPageContents()
	{
	  echo "      <div class=\"topContentStrip\"><a href=\"" . WEB_PATH . "/group-listing/page-" . $this->_groupListingPage . "/\">&laquo; Return to group listings</a></div>\n";
	  
	  echo "      <div class=\"articleSide groupSide\">\n";
	  echo "        <div class=\"eventHeader\">Calendar</div>\n";
	  $previous_event_date = null;
	  for ($output_events = 0; $output_events < sizeof($this->_events); $output_events++)
	  {
	    if (isset($this->_events[$output_events]["EVERY"]))
		  echo "        <div class=\"eventSpecial\">Every " . ucfirst($this->_events[$output_events]["EVERY"]) . ": <span>" .
			$this->_events[$output_events]["TITLE"] . "</span></div>\n";
		else
		{
	      $current_event_date = (isset($this->_events[$output_events]["DATE"]) ? $this->_events[$output_events]["DATE"] :
			$this->_events[$output_events]["DATE_START"]);
	      if ($previous_event_date == null || $previous_event_date != $current_event_date)
		    echo "        <div class=\"eventTitle\">" . date(DATE_FORMAT, $current_event_date) . "</div>\n";
		  $previous_event_date = $current_event_date;
		  echo "        <div class=\"eventEntry\">- " . $this->_events[$output_events]["TITLE"] . "</div>\n";
		}
	  }
	  if (sizeof($this->_events) == 0)
	    echo "        <center>No calendar has been reported</center>\n";
	  echo "      </div>\n";
	  
	  echo "      <div class=\"articleContainer groupContainer\">\n";
	  echo "        <div class=\"title\">" . $this->_groupName . "</div>\n";
	  echo "        <div class=\"subtitle\">" . ($this->_isSport ? $this->_sportSeason . " Sports Team" : "School Club/Organization") . "</div>\n";
	  echo "        <center><img src=\"" . WEB_PATH . "/images/groups/" . $this->_imageFile . "\" class=\"groupImage\" /></center>\n";
	  echo "        <div class=\"description\">" . $this->_description . "</div>\n";
	  echo "        <div class=\"contentHeader\">Related Content</div>\n";
	  foreach ($this->_groupContent as $content)
	  {
	    echo "        <a href=\"" . WEB_PATH . "/" . $content["PAGE_HANDLE"] . "/" . $content["HANDLE"] . "/\" class=\"groupRelated\">" .
			"[" . $content["PAGE_TITLE"] . "] " . $content["TITLE"] . "</a><br />\n";
	  }
	  if (sizeof($this->_groupContent) == 0)
	    echo "        <center>No related content has been posted yet.</center>\n";
	  echo "      </div>\n";
	}
}
class GoAvesGroupListing {
	private $_numberItems;
	private $_currentPage;
	private $_numberPages;
	private $_specificSearch = null;
	private $_items = array();
	function loadGroups($path, $database)
	{
	  $page_param = 1;
	  if ($path[1] == "clubs" || $path[1] == "sports")
	  {
	    $this->_specificSearch = $path[1];
		$page_param = 2;
	  }
	  $number_query = "SELECT count(*) FROM groups WHERE image_file <> '' AND image_file IS NOT null AND " .
		"icon_file <> '' AND icon_file IS NOT null";
	  if ($this->_specificSearch == "clubs")
	    $number_query .= " AND type='club'";
	  else if ($this->_specificSearch == "sports")
	    $number_query .= " AND type LIKE '%_sports'";
	  $this->_numberItems = $database->querySingle($number_query);
	  
	  $this->_numberPages = floor($this->_numberItems % GROUPS_PER_LISTING_PAGE == 0 ? $this->_numberItems / GROUPS_PER_LISTING_PAGE :
		$this->_numberItems / GROUPS_PER_LISTING_PAGE + 1);
	  if ($this->_numberPages < 1)
	    $this->_numberPages = 1;
	  $this->_currentPage = parseCurrentPage($path[$page_param], $this->_numberPages);
	  
	  $get_query = "SELECT handle, title, icon_file FROM groups WHERE image_file <> '' AND image_file IS NOT null " .
		"AND icon_file <> '' AND icon_file IS NOT null";
	  if ($this->_specificSearch == "clubs")
	    $get_query .= " AND type='club'";
	  else if ($this->_specificSearch == "sports")
	    $get_query .= " AND type LIKE '%_sport'";
	  $get_items = $database->query($get_query . " ORDER BY title ASC LIMIT " . GROUPS_PER_LISTING_PAGE .
		" OFFSET " . (($this->_currentPage - 1) * GROUPS_PER_LISTING_PAGE));
	  while ($item = $get_items->fetchArray())
	    $this->_items[] = array("HANDLE" => $item["handle"], "TITLE" => $item["title"], "ICON" => $item["icon_file"]);
	}
	function checkOrRedirect($path,$database)
	{
	  $this->loadGroups($path, $database);
	  return true;
	}
	function getMainTab() { return "group-listing"; }
	function getPageHandle() { return "group-listing"; }
	function getPageSubhandle() { return $this->_specificSearch; }
	function getBreadTrail() { return array("home" => "GoAves.com", "[this]" => "Group Listing"); }
	function getPageTitle() { return "All Groups"; }
	function getPageContents()
	{ 
	  generatePageNavigation("group-listing" . ($this->_specificSearch != null ? "/" . $this->_specificSearch : ""), $this->_currentPage, $this->_numberPages);
	  echo "      <center>\n";
	  foreach ($this->_items as $item)
	  {
		echo "        <div onMouseOver=\"this.className='listItemHover';\" onMouseOut=\"this.className='listItem';\" " .
			"onClick=\"window.location='" . WEB_PATH . "/group/" . $item["HANDLE"] . "/';\" class=\"listItem\">\n";
		echo "          <div>" . $item["TITLE"] . "</div>\n";
		echo "          <img src=\"" . WEB_PATH . "/images/groups/" . $item["ICON"] . "\" />\n";
		echo "        </div>\n";
	  }
	  echo "      </center>\n";
	  
	  generatePageNavigation("group-listing" . ($this->_specificSearch != null ? "/" . $this->_specificSearch : ""), $this->_currentPage, $this->_numberPages);
	}
}
?>