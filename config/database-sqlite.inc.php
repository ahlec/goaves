<?php
class DeitloffDatabase
{
  private $_database;
  function __construct($path)
  {
    $this->_database = sqlite_open($path);
	$this->exec("PRAGMA short_column_names = ON");
  }
  function query($query)
  {
    $results = sqlite_query($this->_database, $query);
    return ($results == FALSE ? FALSE : new DeitloffDatabaseResult($results));
  }
  function querySingle($query, $return_full_row = false)
  {
    $results = sqlite_fetch_array(sqlite_query($this->_database, $query, true), SQLITE_ASSOC);
	if (sizeof($results) == 0 || is_bool($results) || $return_full_row)
	  return $results;
	$result_column_names = array_keys($results);
	return $results[$result_column_names[0]];
  }
  function exec($query)
  {
    return sqlite_exec($this->_database, $query);
  }
  function escapeString($string)
  {
    return sqlite_escape_string($string);
  }
  function close()
  {
    sqlite_close($this->_database);
  }
  function lastErrorCode()
  {
    return sqlite_last_error($this->_database);
  }
  function lastErrorMsg()
  {
    return sqlite_error_string(lastErrorCode());
  }
}
class DeitloffDatabaseResult
{
  private $_results;
  function __construct($results)
  {
	$this->_results = $results;
  }
  function fetchArray()
  {
    return sqlite_fetch_array($this->_results, SQLITE_ASSOC);
  }
  function reset()
  {
    return sqlite_rewind($this->_results);
  }
  function numberRows()
  {
    return sqlite_num_rows($this->_results);
  }
}
?>