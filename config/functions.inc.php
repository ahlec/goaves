<?php

function get_smart_blurb($string, $number_of_words)
{
  $string = format_content($string);
  $string = preg_replace("/(\n|\r)/", " ", strip_tags($string));
  $string = preg_replace("/( |&nbsp;)+/", " ", $string);
  if (str_word_count($string) <= $number_of_words)
    return $string;
  $words = preg_split("/ /", $string);
  $words = explode(" ", preg_replace("/<br((.?){2})>/", " ", $string));
  $smart_blurb = $words[0];
  for ($output = 1; $output < $number_of_words; $output++)
    $smart_blurb .= " " . $words[$output];
  $smart_blurb = trim($smart_blurb);
  $final_blurb_character = mb_substr($smart_blurb, mb_strlen($smart_blurb) - 1, 1);
  $removed_punctuation_marks = array(".","?","!",",",";");
  if (in_array($final_blurb_character, $removed_punctuation_marks))
    $smart_blurb = mb_substr($smart_blurb, 0, mb_strlen($smart_blurb) - 1);
  return $smart_blurb;
}

function old_get_smart_blurb($string, $number_of_words)
{
  $string = format_content($string);
  $string = str_replace("\n", "", str_replace("\r", "", strip_tags($string)));
  $string = preg_replace("/( |&nbsp;)+/", " ", $string);
  if (str_word_count($string) <= $number_of_words)
    return $string;
  $words = preg_split("/ /", $string);
  $words = explode(" ", str_replace("<br>","", str_replace("<br />", "", $string)));
  $smart_blurb = $words[0];
  for ($output = 1; $output < $number_of_words; $output++)
    $smart_blurb .= " " . $words[$output];
  $smart_blurb = trim($smart_blurb);
  $final_blurb_character = mb_substr($smart_blurb, mb_strlen($smart_blurb) - 1, 1);
  $removed_punctuation_marks = array(".","?","!",",",";");
  if (in_array($final_blurb_character, $removed_punctuation_marks))
    $smart_blurb = mb_substr($smart_blurb, 0, mb_strlen($smart_blurb) - 1);
  return $smart_blurb;
}

function format_content($string)
{
  $string = stripslashes(utf8_decode($string));
  return $string;
}

function prepare_content_for_insert($string, $remove_elements = false)
{
  $string = utf8_encode($string);
  if ($remove_elements)
  {
    $string = preg_replace("/<\/p>/", "", preg_replace("/<p(|[^\>]*)>/", "", $string));
    $string = preg_replace("/<\/span>/", "", preg_replace("/<span(|[^\>]*)>/", "", $string));
  }
  $string = str_replace("\\\"", "\"", $string);
  $string = trim($string);
  return $string;
}

function reformat_url_string($string)
{
  return unicode_decode($string);
}
function unicode_decode($str){
    return preg_replace("/\\\u([0-9A-F]{4})/ie", "iconv('utf-16', 'utf-8', hex2str(\"$1\"))", $str);    
}
function hex2str($hex) {
    $r = '';
    for ($i = 0; $i < strlen($hex) - 1; $i += 2)
    $r .= chr(hexdec($hex[$i] . $hex[$i + 1]));
    return $r;
}

function getAdminView()
{
  return (isset($_SESSION[MANAGE_SESSION]) && substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 10, 1) == "1");
}

?>