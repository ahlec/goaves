<?php
function goaves_error_handling($error_number, $error_string, $error_file, $error_line)
{
  if ($error_number == E_NOTICE)
    return;
  echo "<b>" . $error_string . "</b><br />\n";
  if (defined("OUTPUT_HAS_BEGUN"))
  {
    echo "Output has already started";
  } else
  {
    echo "Output has not yet begun";
  }
  // Should exit, but for development purposes, we're continuing.
}


?>