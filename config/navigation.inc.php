<?php
class MainTab
{
  private $_title;
  private $_linkSrc;
  private $_subItems = array();
  function __construct($title, $link_src)
  {
    $this->_title = $title;
	$this->_linkSrc = $link_src;
  }
  function getTitle() { return $this->_title; }
  function getLinkSrc() { return $this->_linkSrc; }
  function addSubItem($item)
  {
    $this->_subItems[] = $item;
	if (isset($this->_subItems[sizeof($this->_subItems) - 2]))
	  $this->_subItems[sizeof($this->_subItems) - 2]->setLastItemSetting(false);
	$this->_subItems[sizeof($this->_subItems) - 1]->setLastItemSetting(true);
  }
  function getSubItems() { return $this->_subItems; }
}
class LocalNavigationItem
{
  private $_handle;
  private $_text;
  private $_isLastItem = false;
  private $_isExternalLink;
  private $_isDisabled;
  function __construct($handle, $text, $isExternal = false, $disabled = false)
  {
    $this->_handle = $handle;
	$this->_text = $text;
	$this->_isExternalLink = $isExternal;
	$this->_isDisabled = $disabled;
  }
  function setLastItemSetting($value) { $this->_isLastItem = $value; }
  function getOutput($current_page, $current_subhandle)
  {
    $isCurrentPage = ($current_page . ($current_subhandle != null ? "/" . $current_subhandle : "") == $this->_handle);
     return ($isCurrentPage ? "<b class=\"localLink\">" : "<a href=\"" . WEB_PATH . "/" . (strlen($this->_handle) > 0 ? $this->_handle . ($this->_isExternalLink ? "" : "/") : "") .
	"\" class=\"localLink\"" . ($this->_isExternalLink ? " target=\"_blank\"" : "") . ">") .
	$this->_text . ($isCurrentPage ? "</b>" : "</a>") .	(!$this->_isLastItem ? " <span class=\"localLink\">&bull;</span> " : "");
  }
}

function getMainTabs($database)
{
  $tab_goaves = new MainTab("GoAves", "home");
  $tab_goaves->addSubItem(new LocalNavigationItem("home", "Homepage"));
  $tab_goaves->addSubItem(new LocalNavigationItem("contact", "Contact Information"));
  
  $tab_news = new MainTab("News", "news");
  $get_beat_types = $database->query("SELECT handle, title FROM beat_types ORDER BY title ASC");
  while ($beat_type = $get_beat_types->fetchArray())
    $tab_news->addSubItem(new LocalNavigationItem("news" . ($beat_type["handle"] != "general" ? "/" . $beat_type["handle"] : ""), format_content($beat_type["title"])));
  
  $tab_staff = new MainTab("Staff", "staff");
  $tab_staff->addSubItem(new LocalNavigationItem("staff", "Staff Listing"));
  $tab_staff->addSubItem(new LocalNavigationItem("joining-journalism", "Joining the staff"));
  
  $tab_leaf = new MainTab("<i class=\"localLink\">The Leaf</i>", "leaf");
  
  $tab_log = new MainTab("<i class=\"localLink\">The Log</i>", "log");
  $tab_log->addSubItem(new LocalNavigationItem("log", "<i class=\"localLink\">The Log</i> FAQ"));
  $tab_log->addSubItem(new LocalNavigationItem("log/confirmations", "Order Confirmations"));
  $tab_log->addSubItem(new LocalNavigationItem("/documents/order-form-for-2011-Log.docx", "Order Form", true));
  
  $tab_multimedia = new MainTab("Multimedia", "multimedia");
  $tab_multimedia->addSubItem(new LocalNavigationItem("multimedia/photo-galleries", "Photo Galleries"));
  $tab_multimedia->addSubItem(new LocalNavigationItem("multimedia/videos", "Videos"));
  $tab_multimedia->addSubItem(new LocalNavigationItem("multimedia/comics", "Comics"));
  $tab_multimedia->addSubItem(new LocalNavigationItem("multimedia/sound-slides", "Sound Slides"));
  $tab_multimedia->addSubItem(new LocalNavigationItem("multimedia/podcasts", "Podcasts"));
  
  $tab_sports = new MainTab("Sports", "sports");
  $tab_sports->addSubItem(new LocalNavigationItem("sports-information", "Information"));
  $tab_sports->addSubItem(new LocalNavigationItem("sports/fall", "Fall Sports"));
  $tab_sports->addSubItem(new LocalNavigationItem("sports/winter", "Winter Sports"));
  $tab_sports->addSubItem(new LocalNavigationItem("sports/spring", "Spring Sports"));
  
  $tab_groups = new MainTab("Activities", "group-listing");
  $get_group_types = $database->query("SELECT singular_name, handle FROM group_types");
  while ($group_type = $get_group_types->fetchArray())
    $tab_groups->addSubItem(new LocalNavigationItem("group-listing/" . $group_type["handle"], format_content($group_type["singular_name"])));
//  $tab_groups->addSubItem(new LocalNavigationItem("group-listing/clubs", "All Clubs"));
//  $tab_groups->addSubItem(new LocalNavigationItem("group-listing/sports", "All Sports"));
  
  $tab_interactions = new MainTab("Interactions", "interactions");
  $tab_interactions->addSubItem(new LocalNavigationItem("interactions/polls", "Polls"));
  $tab_interactions->addSubItem(new LocalNavigationItem("interactions/caption-contests", "Caption Contests"));
  
  $tab_specials = new MainTab("Specials", "specials");
  $get_active_specials = $database->query("SELECT handle, title FROM specials WHERE active='TRUE' ORDER BY title ASC");
  while ($active_special = $get_active_specials->fetchArray())
    $tab_specials->addSubItem(new LocalNavigationItem("special/" . $active_special["handle"], format_content($active_special["title"])));
  
  return array($tab_goaves, $tab_news, $tab_specials, $tab_multimedia, $tab_leaf, $tab_staff, $tab_log,
	$tab_groups);//$tab_sports, $tab_groups);
}

function outputSiteNavigation($page, $database)
{

  $main_tabs = getMainTabs($database);
  $loaded_tab = $page->getMainTab();
  foreach ($main_tabs as $tab)
    echo "        <a href=\"" . WEB_PATH . "/" . $tab->getLinkSrc() . "/\" class=\"item" . (strtolower($tab->getLinkSrc()) == $loaded_tab ? "LoadedHover" : "") .
    	"\" id=\"tab-" . strtolower($tab->getLinkSrc()) . "\" onMouseOver=\"navigationTab('" . strtolower($tab->getLinkSrc()) . 
		"');\" onMouseOut=\"clearNavigation();\">" . $tab->getTitle() . "</a>\n";
  echo "      </div>\n";
  
  echo "      <div class=\"local\" id=\"localSiteNavigation\" onMouseOut=\"clearNavigation();\">\n";
  foreach ($main_tabs as $tab)
  {
    echo "        <div id=\"local-navigation-" . strtolower($tab->getLinkSrc()) . "\" class=\"localContainer" . (strtolower($tab->getLinkSrc()) == $loaded_tab ? " loadedLocal" : "") .
		"\">\n";
    foreach ($tab->getSubItems() as $subItem)
	  echo "          " . $subItem->getOutput($page->getPageHandle(), $page->getPageSubhandle()) . "\n";
	echo "        </div>\n";
  }
  //require_once ("components/local-navigation.php");
  //outputLocalNavigation($loaded_tab, $page->getPageHandle(), $page->getPageSubhandle());
  echo "      </div>\n";
}

?>