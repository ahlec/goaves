<?php
require_once (DOCUMENT_ROOT . "/config/related-group.inc.php");
/* **************************************
 *  Valid Pages Include                 *
 * **************************************/
 
$_PAGES = array("index" => "GoAvesIndex", "home" => "GoAvesIndex", "news" => "GoAvesNews", "page-not-found" => "GoAves404",
	"article" => "GoAvesArticle", "video" => "GoAvesVideo", "photo-gallery" => "GoAvesGallery", "staff" => "GoAvesStaff",
	"staff-portfolio" => "GoAvesStaffPortfolio", "comic" => "GoAvesComic", "multimedia" => "GoAvesMultimedia",
	"photo" => "GoAvesPhoto", "podcast" => "GoAvesPodcast", "contact" => "GoAvesContact",
	"leaf" => "GoAvesLeafListing", "poll" => "GoAvesPoll", "sports" => "GoAvesSports", "leaf-issue" => "GoAvesLeafIssue",
	"sound-slides" => "GoAvesSoundSlides", "group" => "GoAvesGroup", "group-listing" => "GoAvesGroupListing",
	"interactions" => "GoAvesInteractions", "topic-hub" => "GoAvesTopicHub", "topic" => "GoAvesTopicPage", "multiold" => "GoAvesMultimediaOld",
	"news-old" => "GoAvesNewsOld", "staff-portfolio-old" => "GoAvesStaffPortfolioOld", "staff-old" => "GoAvesStaffOld", "group-listing-old" => "GoAvesGroupListingOld",
	"group-old" => "GoAvesGroupOld", "all-sound-slides" => "GoAvesSoundSlidesListing","leaf-old" => "GoAvesLeafListingOld",
	"log" => "GoAvesLog", "page" => "GoAvesCustomPage", "search" => "GoAvesSearch", "specials" => "GoAvesSpecials", "special" => "GoAvesSpecial");
$_ERROR_PAGES = array(404 => "page-not-found");

function parsePath($path, $page_definitions, $error_pages, $database, $is_admin)
{
  $path_pieces = explode("/", $path);
  if (sizeof($path_pieces) == 0 || $path_pieces[0] == "")
    $path_pieces[0] = "index";
  else
    $path_pieces[0] = mb_strtolower($path_pieces[0]);
  $path_pieces = processAliases($path_pieces, $database, $is_admin);
  if (!in_array($path_pieces[0], array_keys($page_definitions)))
    $path_pieces = array($error_pages[404], $path_pieces[0]);
  return $path_pieces;
}

function processAliases($path_pieces, $database, $is_admin)
{
  $path = $path_pieces;
  $constructed_paths = array();
  if ($path[sizeof($path) - 1] == "" || $path[sizeof($path) - 1] == null)
    unset ($path[sizeof($path) - 1]);
  $final_piece = array_pop($path);
  $following_pieces = array();
  while ($final_piece != null)
  {
	$translation = $database->querySingle("SELECT translation FROM aliases WHERE alias LIKE '" .
		$database->escapeString(ltrim(implode("/", $path) . "/" . $final_piece, "/")) . "%'" . (!$is_admin ? " AND published='TRUE'" : "") . " LIMIT 1");
    if ($translation != false)
	{
	  $translated_pieces = explode("/", ltrim($translation, "/"));
	  foreach ($following_pieces as $piece_followed)
	    $translated_pieces[] = $piece_followed;
	  return $translated_pieces;
	}
	$following_pieces[] = $final_piece;
	$final_piece = array_pop($path);
  }
  return $path_pieces;
}

function selectPage($path_pieces, $page_definitions, $database)
{
  // Find Proper Page
  $selected_page = new $page_definitions[$path_pieces[0]]();
  $selected_page_final = $selected_page->checkOrRedirect($path_pieces, $database);
  while($selected_page_final !== true)
  {
    $selected_page = $selected_page_final;
	$selected_page_final = $selected_page->checkOrRedirect($path_pieces, $database);
  }
  return $selected_page;
}

function generateBreadcrumbs($bread_slice)
{
  $bread_trail = "";
  foreach (array_keys($bread_slice) as $bread_token)
  {
    if ($bread_token == "[this]")
	  $bread_trail .= "<b>" . $bread_slice["[this]"] . "</b>";
	else
	  $bread_trail .= "<a href=\"" . WEB_PATH . "/" . $bread_token . "/\">" . $bread_slice[$bread_token] . "</a>";
	$bread_trail .= " &raquo; ";
  }
  return rtrim($bread_trail, " &raquo; ");
}
function generatePageNavigation($page_handle, $current_page, $total_pages)
{
  $has_previous_page = ($current_page > 1);
  $has_next_page = ($current_page < $total_pages);
  generateArbitraryPageNavigation(($has_previous_page ? $page_handle . "/page-" . ($current_page - 1) : null), "Previous Page",
	"Page " . $current_page . " of " . $total_pages, ($has_next_page ? $page_handle . "/page-" . ($current_page + 1) : null),
	"Next Page");
}
function generateNewPageNavigation($page_handle, $current_page, $total_pages)
{
  $critical_page_number_padding = 3;

  // Determine Critical Page Numbers
  $critical_page_numbers = array();
  if ($current_page - $critical_page_number_padding > 0)
    $critical_page_numbers[] = 0;
  for ($pre_current_page = 1; $pre_current_page <= $critical_page_number_padding; $pre_curent_page++)
    if ($current_page - $pre_current_page >= 1)
      $critical_page_numbers[] = $current_page - $pre_current_page;
  $critical_page_numbers[] = $current_page;
  for ($post_current_page = 1; $post_current_page <= $critical_page_number_padding; $post_current_page++)
    if ($current_page + $post_current_page <= $total_pages)
      $critical_page_numbers[] = $current_page + $post_current_page;
  if ($total_pages - $critical_page_number_padding > $current_page)
    $critical_page_numbers[] = $total_pages;
  echo "<pre>";
  print_r($critical_page_numbers);
  echo "</pre>";

  //echo "      <div class=\"pageNavigation\">\n";
  //echo "      </div>\n";

}
function generateArbitraryPageNavigation($previous_page_handle, $previous_page_title, $center, $next_page_handle, $next_page_title)
{
  echo "      <table class=\"pageNavigation\"><tr>\n";
  echo "        <td class=\"left " . ($previous_page_handle != null ? "active" : "disabled") . "\" " .
	($previous_page_handle != null ? " onMouseOver=\"this.className='left activeHover';\" onMouseOut=\"this.className='left active';\" " .
	"onClick=\"window.location='" . WEB_PATH . "/" . $previous_page_handle . "/';\"" : "") . ">" . $previous_page_title . "</td>\n";
  echo "        <td class=\"center\">" . $center . "</td>\n";
  echo "        <td class=\"right " . ($next_page_handle != null ? "active" : "disabled") . "\" " .
	($next_page_handle != null ? " onMouseOver=\"this.className='right activeHover';\" onMouseOut=\"this.className='right active';\" " .
	"onClick=\"window.location='" . WEB_PATH . "/" . $next_page_handle . "/';\"" : "") . ">" . $next_page_title . "</td>\n";
  echo "      </tr></table>\n";
}

function parseCurrentPage($path_page, $number_pages)
{
  if ($path_page == null || $path_page == "" || strpos($path_page, "page-") === false || !ctype_digit(str_replace("page-","",$path_page)))
    return 1;
  else
  {
    $cur_page = str_replace("page-", "", $path_page);
	if ($cur_page >= $number_pages)
	  return $number_pages;
	if ($cur_page < 1)
	  return 1;
    return $cur_page;
  }
  return 1;
}
?>