<?php
function preoutputErrorHandling($errorNumber, $errorMessage, $errorFile, $errorLine)
{
  $error_log = @fopen(DOCUMENT_ROOT . "/config/error_logs/prerender_log.txt", "a");
  if ($error_log !== false)
  {
    @fwrite($error_log, "\n\r[" . date("Y-m-d G:i:s") . "] '" . $errorMessage . "' (Error Level: '" . $errorNumber . "', File: '" . 
		$errorFile . "', Line: '" . $errorLine . "')\n\r");
	@fclose($error_log);
  } else
    echo "no";
  
  if ($errorNumber == E_ERROR || $errorNumber == E_CORE_ERROR || $errorNumber == E_COMPILE_ERROR || $errorNumber == E_USER_ERROR)
  {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
    echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\n";
    echo "<head>\n";
  
    echo "  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=8\" />\n";
    echo "  <meta http-equiv=\"Pragma\" content=\"no-cache\">\n";
    echo "  <meta http-equiv=\"cache-control\" content=\"no-cache\">\n";
    echo "  <meta http-equiv=\"expires\" content=\"0\">\n";
    echo "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n";
    echo "  <meta http-equiv=\"Content-Language\" content=\"en-US\" />\n";
    echo "  <meta name=\"description\" content=\"The Online Voice of Sycamore\" />\n";
    echo "  <meta name=\"copyright\" content=\"(c)2010 Sycamore Community High School\" />\n";
    echo "  <meta name=\"author\" content=\"Sycamore High School Journalism Staff\" />\n";
	echo "  <title>Error - GoAves.com</title>\n";
    echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/universal/stylesheet-prerender-fatality.css\" />\n";
	echo "</head>\n";
	echo "<body>\n";
	echo "  <div class=\"siteLogo\">GoAves.com</div>\n";
	echo "  <div class=\"mainContainer\">\n";
	echo "    <div class=\"header\">A major error has occurred.</div>\n";
	echo "    <div class=\"explication\">A major error occurred when trying to load the website, and the page which you were attempting to access was unable to " .
		"load.</div>\n";
	echo "    <div class=\"header\">What should I do now?</div>\n";
	echo "    <div class=\"explication\">Well, first try <b>refreshing your page</b>. This error could easily have been a fluke; the problem might have been fixed already. " .
		"If you've already tried refreshing, however, the problem is still there. Another simple solution would be to try <b>going to another page</b>, such as the " .
		"<a href=\"" . WEB_PATH . "/\">homepage</a> or the <a href=\"" . WEB_PATH. "/news/\">news listing</a>. If you are still seeing this message, however, that means " .
		"that a rather serious problem has occurred. Luckily, all the information that is needed has already been recorded, and this problem will be resolved very soon.</div>\n";
	echo "    <div class=\"finalNote\">The most important thing to remember is that <span>this error is temporary</span>. Check back soon; by then, the problem will be " .
		"fixed.</div>\n";
	echo "    <div class=\"closing\">Thank you for your understanding, <b>GoAves.com</b></div>\n";
	echo "  </div>\n";
	echo "</body>\n";
	echo "</html>\n";
    exit();
  }
}
function goavesErrorHandling($errorNumber, $errorMessage, $errorFile, $errorLine)
{
}
?>