<?php
session_start();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>GoAves Survey Center</title>
<link rel="stylesheet" type="text/css" href="main.css" />
        <script type="text/javascript">

        function formSubmit()
        {
             document.forms["next"].submit();
        }

        </script>

    </head>
    <body>

        <?php

	require_once("F:\\survey_config\\connection.php");
        require_once("F:\\survey_config\\functions.php");

            $survey_id = $_SESSION['survey_id'];

            //verify user


            if(isset($_POST['username']) || isset($_POST['email']) || $_SESSION['username'] == null || $_SESSION['email'] == null){

                if(!isset($_POST['username']) || !isset($_POST['email']) || $_POST['username']== null || $_POST['email']== null){
                    $_SESSION['username'] = null;
                    $_SESSION['email'] = null;
                    $_SESSION['error'] = "You must provide both your student ID and a valid email address before you can begin. You cannot leave any feilds blank.";
                    redirect_to("start.php");
                    exit;
                }

                if(!(preg_match('%^[1]+[0-9]{4,5}$%', stripslashes(trim($_POST['username']))) && preg_match('%^[A-Za-z0-9._-]+@[A-Za-z0-9.-]+\.[A-Za-z0-9]{2,4}$%', stripslashes(trim($_POST['email'])))))
                {
                    $_SESSION['username'] = null;
                    $_SESSION['email'] = null;
                    $_SESSION['error'] = "You must provide a valid user ID and email address before you can begin.";
                    redirect_to("start.php");
                    exit;
                }

                $_SESSION['username'] = mysql_prep($_POST['username']);
                $_SESSION['email'] = mysql_prep($_POST['email']);

                $username = $_SESSION['username'];
                $email = $_SESSION['email'];


                $query = "SELECT username FROM users WHERE username = '$username' OR email = '$email' AND survey_id = '$survey_id'";
                $result = mysql_query($query);

                if (mysql_num_rows($result) != 0)
                {

                    $_SESSION['username'] = null;
                    $_SESSION['email'] = null;
                    $_SESSION['error'] = "Our records show that you have already taken this survey. You cannot take this survey more than once.";
                    redirect_to("start.php");
                    exit;

                }
            
            }

            //load survey

            if (!isset ($_SESSION['questions'])){

            //load survey questions

            //echo "SURVEY QUESTIONS";

            $query = "SELECT question_id, question, page_number, `order` FROM questions WHERE enabled =1 AND survey_id =$survey_id ORDER BY page_number, `order`";
            $result = mysql_query($query);
            while($row = mysql_fetch_array($result)){

                $_SESSION['questions'][] = $row;
                
            }

            //load survey answers

            //echo "SURVEY ANSWERS";

            $query = "SELECT answers.answer_id, answers.answer, answers.question_id, answers.type FROM answers JOIN questions ON answers.question_id=questions.question_id WHERE questions.survey_id=$survey_id ORDER BY answers.order";
            $result = mysql_query($query);
            while($row = mysql_fetch_array($result)){

                $_SESSION['answers'][] = Array("answer_id" => $row['answer_id'], "question_id" => $row['question_id'], "answer" => $row['answer'], "type" => $row['type']);

            }


             //get survey page length

                $_SESSION['page_length'] = $_SESSION['questions'][(sizeof($_SESSION['questions'])-1)]["page_number"];

                $_SESSION['current_page'] = 1;

            }

            if (isset($_POST['next_page']))
            {

                $_SESSION['current_page'] = $_POST['next_page'];

            }


            if (isset($_SESSION['answer_id']))
            {
                foreach($_SESSION['answer_id'] as $post_value)
                {
                    if (isset($_POST[$post_value]))
                    {
                        $_SESSION['response'][$post_value] = mysql_prep($_POST[$post_value]);
                    }
                }

            }

            if ($_SESSION['current_page'] > $_SESSION['page_length']){
                redirect_to("submit.php");
                exit;
            }

            //ECHO PAGE TITLE AND INSTRUCTIONS
		$current_survey_page = $_SESSION['current_page'];


$query = "SELECT page_title FROM instructions WHERE survey_id = '$survey_id' AND page_id = '$current_survey_page' LIMIT 1";
                $result = mysql_query($query);
                $row = mysql_fetch_row($result);

                if ($row[0]!= null){
                	$page_title = $row[0];
                }else{
			$page_title = "Page: " . $current_survey_page;
		}

		echo "<div id=\" header_container\">";
            echo "<p id=\"page_title\">" . $page_title . "</p>";
            echo "<p id=\"exit\"><a href=\"index.php\">[Exit Survey]</a></p>";
	    echo "<div style=\"clear: both;\"></div>";
		echo "</div>";

echo "<br />";

            //load special page instructions here
                $query = "SELECT instructions FROM instructions WHERE survey_id = '$survey_id' AND page_id = '$current_survey_page' LIMIT 1";
                $result = mysql_query($query);
                $row = mysql_fetch_row($result);

                if ($row[0]!= null){
                echo $row[0];
                echo "<br /><br />";
                }


            echo "<form id=\"next\" action=\"";
            
            //if ($_SESSION['current_page'] >= $_SESSION['page_length']){
                //echo "submit.php";
            //}else{
                echo "survey.php";
            //}

            echo "\" method=\"POST\">\n";
            $next_page = $_SESSION['current_page'] + 1;
            echo "<input type=hidden name=next_page value=$next_page>\n";

            //DISPLAY QUESTIONS HERE

            foreach ($_SESSION['questions'] as $question) {
                if ($question['page_number'] == $_SESSION['current_page']){
                    echo $question['order'] . ". ";
		    echo $question['question'];
                    echo "<br />";

                    foreach ($_SESSION['answers'] as $answer){
                        if ($answer['question_id'] == $question['question_id']){
                            
                            $type = $answer['type'];
                            $name = $answer['answer_id'];

                            if ($type == "text")
                            {
                                echo $answer['answer'];
                                echo "<input type=\"$type\" name=\"$name\" />";
                            }
                            elseif($type == "checkbox")
                            {
				echo $answer['answer'];
                                echo "<input type=\"$type\" name=\"$name\" />";
                                //echo $answer['answer'];
                            }
                            elseif($type == "textarea")
                            {
                                echo "<textarea name=\"$name\"></textarea>";
                            }

                            $_SESSION['answer_id'][$name] =  $name;

                            echo "<br />";
                        }
                    }

                    echo "<br />";

                }
            }


            echo "</form>\n";

            echo "Page " . $_SESSION['current_page'] . " of " . $_SESSION['page_length'];
            echo "<br />\n";

            if ($_SESSION['current_page'] == $_SESSION['page_length']){

                echo "<a href=\"javascript:formSubmit();\" onClick=\"return confirm('Are you sure? You won\'t be able to retake or change any responses to this survey once you submit.');\">Submit Survey</a>\n";
                echo "<br /><b>Once you submit the survey your survey responses are final and cannot be changed.</b>";

            }else{

                echo "<a href=\"javascript:formSubmit();\" onClick=\"return confirm('Are you sure? Unless you restart the survey before the final submit, your responses on this page are final.');\">Next Page -></a>\n";

            }

        ?>

    </body>
</html>
