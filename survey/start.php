<?php
session_start();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>GoAves Survey Center</title>
        <link rel="stylesheet" type="text/css" href="main.css" />
        <script type="text/javascript">

        function formSubmit()
        {
             document.forms["begin"].submit();
        }

        </script>

    </head>
    <body>
<?php

	require_once("F:\\survey_config\\connection.php");
        require_once("F:\\survey_config\\functions.php");

unset($_SESSION['response'], $_SESSION['answer_id']);
$_SESSION['current_page'] = 1;

$_SESSION['username'] = null;
$_SESSION['email'] = null;

if (isset($_POST['survey_id']) && is_numeric($_POST['survey_id'])) {
    $_SESSION['survey_id'] = $_POST['survey_id'];
}else{
    if (!isset($_SESSION['survey_id']))
    {
    redirect_to("index.php");
    exit;
    }
}

//echo "survey-id: " . $_SESSION['survey_id'];

echo "<center><div id=\"index_title\">GoAves Survey Center</div></center>\n";

if(isset($_SESSION['error']) && $_SESSION['error']!=null){
    echo "<br /><font color=\"red\">";
    echo $_SESSION['error'];
    echo "</font><br />";

    $_SESSION['error'] = null;
}

echo "<br />To begin, enter in valid information below (all fields are required).\n";
echo "<br /> Please note that the survey must be completed all at once. <br />You may exit the survey at any time before submiting and come back to the survey later but your progress does not get saved.<br /><br />";
echo "<form id=\"begin\" action=\"survey.php\" method=\"POST\">\n";
echo "Student ID: <input type=\"text\" name=\"username\" /><br />\n";
echo "Email Address: <input type=\"text\" name=\"email\" /><br />\n";
echo "<a href=\"javascript:formSubmit();\">Begin Survey</a>\n";
echo "</form>";
echo "<br />\n";
echo "<a href=\"index.php\">Go Back</a>";

?>

    </body>
</html>