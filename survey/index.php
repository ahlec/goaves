<?php
session_start();
session_unset();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>GoAves Survey Center</title>
        <link rel="stylesheet" type="text/css" href="main.css" />
        <script type="text/javascript">

        function formSubmit(survey_id)
        {
             document.forms["survey_id"].survey_id.value = survey_id;
             document.forms["survey_id"].submit();
        }

        </script>
    </head>
    <body>
        <center>
            <div id="index_title">GoAves Survey Center</div>
            <div class="container">
            <div id="select">Select a survey from below:</div>
        <form id ="survey_id" action="start.php" method="POST">
        <input type=hidden name=survey_id value=-1>

        <?php

        require_once("F:\\survey_config\\connection.php");
        require_once("F:\\survey_config\\functions.php");

        echo "<div id=\"box\">";
        echo "<div id=\"selection\">";

            $query = "SELECT title, survey_id, description FROM survey WHERE enabled = 1";
            $result = mysql_query($query);
            while($row = mysql_fetch_array($result)){

                $survey_id = $row['survey_id'];
                $title = $row['title'];
                echo "<a href=\"javascript:formSubmit($survey_id);\">- $title</a>\n";
                echo "<br /> \n";
            
            }

            echo "</div>";
echo "</div>";
        ?>

        </form>
            </div>
        </center>

		<br /><a href="http://www.goaves.com">Go back to GoAves.com</a>
			
    </body>
</html>
