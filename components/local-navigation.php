<?php
/* AJAX Entry */
if (isset($_GET["tab"]) && $_GET["tab"] != null && $_GET["tab"] != "")
{
  $tab = str_replace("tab-", "", $_GET["tab"]);
  require_once ("../config/main.inc.php");
  outputLocalNavigation($tab, $_GET["current_page"], (isset($_GET["current_subhandle"]) && $_GET["current_subhandle"] != "" ?
	$_GET["current_subhandle"] : null));
}

/* PHP Function */
function outputLocalNavigation($tab, $current_page, $current_subhandle)
{
  $database = new DeitloffDatabase(DATABASE_PATH);
  switch ($tab)
  {
    case "home":
    {
	  echo parseLocalNavigationItem("home", "Home", $current_page, $current_subhandle);
	  echo parseLocalNavigationItem("contact", "Contact Information", $current_page, $current_subhandle, true);
      break;
    }
    case "news":
    {
	  echo parseLocalNavigationItem("news", "All Articles", $current_page, $current_subhandle);
	  echo parseLocalNavigationItem("topic-hub", "Topics", $current_page, $current_subhandle);
	  /*$latest_stories = $database->query("SELECT title, handle FROM beats WHERE published='TRUE' ORDER BY post_time DESC LIMIT 3");
	  $stories = array();
	  while ($story = $latest_stories->fetchArray())
	    $stories[] = array("HANDLE" => $story["handle"], "TITLE" => utf8_encode($story["title"]));
	  for ($out = 0; $out < sizeof($stories); $out++)
	    echo parseLocalNavigationItem("article/" . $stories[$out]["HANDLE"], "[Article] " . $stories[$out]["TITLE"], $current_page, $current_subhandle,
			($out + 1 == sizeof($stories)));*/
	  $story_types = $database->query("SELECT handle, title FROM beat_types WHERE type_identity<>'1' AND post_on_local_navigation='TRUE' ORDER BY sort_order ASC");
	  $types = array();
	  while ($type = $story_types->fetchArray())
	    $types[] = array("HANDLE" => $type["handle"], "TITLE" => utf8_decode($type["title"]));
	  for ($out = 0; $out < sizeof($types); $out++)
	    echo parseLocalNavigationItem("news/" . $types[$out]["HANDLE"], $types[$out]["TITLE"], $current_page,
		$current_subhandle, ($out + 1 == sizeof($types)));
      break;
    }
	case "leaf":
	{
	  echo parseLocalNavigationItem("leaf", "<i>The Leaf</i> Archives", $current_page, $current_subhandle);
	  $latest_issues = $database->query("SELECT issue_name, handle FROM leaf_issuu ORDER BY post_date DESC LIMIT 3");
	  $issues = array();
	  while ($issue = $latest_issues->fetchArray())
	    $issues[] = array("TITLE" => $issue["issue_name"], "HANDLE" => $issue["handle"]);
	  for ($output = 0; $output < sizeof($issues); $output++)
	    echo parseLocalNavigationItem("leaf-issue/" . $issues[$output]["HANDLE"], $issues[$output]["TITLE"], $current_page, $current_subhandle,
			($output + 1 == sizeof($issues)));
	  break;
	}
    case "log":
    {
      echo parseLocalNavigationItem("log", "<i>The Log</i> FAQ", $current_page, $current_subhandle);
      echo parseLocalNavigationItem("log/confirmations", "Order Confirmations", $current_page, $current_subhandle);
      echo parseLocalNavigationItem("documents/yearbook-order-form.htm", "Order Form", $current_page,
      	$current_subhandle, true, true);
      break;
    }
    case "staff":
    {
      echo parseLocalNavigationItem("staff", "Staff Listing", $current_page, $current_subhandle, true);
      break;
    }
    case "multimedia":
    {
	  echo parseLocalNavigationItem("multimedia", "Multimedia Listing", $current_page, $current_subhandle);
	  echo parseLocalNavigationItem("multimedia/photo-galleries", "Photo Galleries", $current_page, $current_subhandle);
	  echo parseLocalNavigationItem("multimedia/videos", "Videos", $current_page, $current_subhandle);
	  echo parseLocalNavigationItem("multimedia/comics", "Comics", $current_page, $current_subhandle);
	  echo parseLocalNavigationItem("multimedia/sound-slides", "Soundslides", $current_page, $current_subhandle);
	  echo parseLocalNavigationItem("multimedia/audio", "Audio", $current_page, $current_subhandle, true);
      break;
    }
    case "sports":
    {
	  echo parseLocalNavigationItem("sports-information", "Information", $current_page, $current_subhandle);
	  echo parseLocalNavigationItem("sports/fall", "Fall Sports", $current_page, $current_subhandle);
	  echo parseLocalNavigationItem("sports/winter", "Winter Sports", $current_page, $current_subhandle);
	  echo parseLocalNavigationItem("sports/spring", "Spring Sports", $current_page, $current_subhandle, true);
      break;
    }
    case "group-listing":
    {
	  echo parseLocalNavigationItem("group-listing", "Group Listings", $current_page, $current_subhandle);
	  echo parseLocalNavigationItem("group-listing/clubs", "All Clubs", $current_page, $current_subhandle);
	  echo parseLocalNavigationItem("group-listing/sports", "All Sports Teams", $current_page, $current_subhandle, true);
	  break;
    }
    case "interactions":
    {
	  echo parseLocalNavigationItem("interactions", "All Student Interactions", $current_page, $current_subhandle, true);
    }
  }
}
function parseLocalNavigationItem($handle, $text, $current_page, $current_subhandle, $last_item = false, $is_external_link = false)
{
  return ($current_page . ($current_subhandle != null ? "/" . $current_subhandle : "") == $handle ? 
	"<b class=\"localLink\">" : "<a href=\"" . WEB_PATH . "/" . (strlen($handle) > 0 ? $handle . ($is_external_link ? "" : "/") : "") .
	"\" class=\"localLink\"" . ($is_external_link ? " target=\"_blank\"" : "") . ">") .
	$text . ($current_page . ($current_subhandle != null ? "/" . $current_subhandle : "") == $handle ? "</b>" : "</a>") .
	(!$last_item ? " <span class=\"localLink\">&bull;</span> " : "");
}
?>