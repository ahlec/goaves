<?php
if (!defined("DOCUMENT_ROOT"))
  exit("This page may only be called from within the website.");
if (!isset($this) || !isset($this->_leafIdentity) || !isset($this->_items) || !isset($this->_colorCover))
  exit("Enough information was not provided.");
	
  echo "        <div class=\"sidePanelSectionContainer\">\n";
  echo "          <img src=\"" . WEB_PATH . "/images/leaf_covers/" . $this->_colorCover . "\" class=\"icon\" border=\"0\" />\n";
  echo "          <div class=\"super\">" . $this->_schoolYearPublished . " - " . ($this->_schoolYearPublished + 1) . "</div>\n";
  echo "          <div class=\"header\">Inside " . $this->_title . "</div>\n";
  echo "          <div style=\"clear:left;\"></div>\n";
  echo "          <div class=\"sideLeafContainer\">\n";
  $previous_page = null;
  foreach ($this->_items as $item)
  {
    if ($previous_page != $item["PAGE_NUMBER"])
	{
	  echo ($previous_page != null ? "          <div class=\"leafItemSeparator\"></div>\n" : "");
	  echo "          <div class=\"leafItemHeader\" onClick=\"goLeafPage('" . $item["PAGE_NUMBER"] . "');\">Page " . $item["PAGE_NUMBER"] . "</div>\n";
	  $previous_page = $item["PAGE_NUMBER"];
	}
    echo "          <div class=\"item leafItemItem\" onClick=\"goLeafPage('" . $item["PAGE_NUMBER"] . "');\"><img src=\"" . WEB_PATH . "/images/icons/";
	switch ($item["TYPE"])
	{
		case "article": echo "icon-leaf-article.png"; break;
		case "calendar": echo "icon-leaf-calendar.png"; break;
		case "snapshots": echo "icon-leaf-snapshots.png"; break;
		case "comic": echo "icon-leaf-comic.png"; break;
		case "column": echo "icon-leaf-column.png"; break;
		case "q_a": echo "icon-leaf-q_a.png"; break;
		case "enterprise": echo "icon-leaf-enterprise.png"; break;
		default: echo "icon-leaf-unknown.png"; break;
	}
	echo "\" />" . $item["TITLE"] . "</div>\n";
  }
  echo "          </div>\n";
  echo "        </div>\n";
?>