<?php
if (!defined("DOCUMENT_ROOT"))
  exit("This page may only be called from within the website.");
if (!isset($this) || !isset($this->_leafIdentity) || !isset($this->_contributors))
  exit("Enough information was not provided.");
	
  echo "        <div class=\"sidePanelSectionContainer\">\n";
  echo "          <img src=\"" . WEB_PATH . "/images/icons/leaf_contributors.jpg\" class=\"icon\" border=\"0\" />\n";
//  echo "          <div class=\"super\">" . $this->_schoolYearPublished . " - " . ($this->_schoolYearPublished + 1) . "</div>\n";
  echo "          <div class=\"header\">Contributors</div>\n";
  echo "          <div style=\"clear:left;\"></div>\n";
  echo "          <div class=\"sideLeafContainer\">\n";
  foreach ($this->_contributors as $contributor)
  {
    echo "          <div class=\"item contributorItem\"><a href=\"" . WEB_PATH . "/staff-portfolio/" . mb_strtolower($contributor["FIRST_NAME"] . "-" .
		$contributor["LAST_NAME"]) . "/\" class=\"portfolio-link\" target=\"_blank\"><img src=\"" . WEB_PATH . "/images/staff/" . 
		$contributor["ICON"] . "\" border=\"0\" />" . $contributor["FIRST_NAME"] . " " . $contributor["LAST_NAME"] . "</a><br /> " .
		$contributor["POSITION"] . "<div style=\"clear:both;\"></div></div>\n";
  }
  echo "          </div>\n";
  echo "        </div>\n";
?>