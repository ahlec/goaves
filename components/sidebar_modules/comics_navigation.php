<?php
if (!defined("DOCUMENT_ROOT"))
  exit("This page may only be called from within the website.");
if (!isset($this) || !isset($this->_identity) || !isset($this->_datePosted))
  exit("Comic identity or date posted was not provided.");

if (!isset($database))
  $database = new DeitloffDatabase(DATABASE_PATH);

  echo "        <div class=\"sidePanelSectionContainer\">\n";
  echo "          <a href=\"" . WEB_PATH . "/staff-portfolio/" . mb_strtolower($staff_info["first_name"] . "-" . $staff_info["last_name"]) .
	"/\"><img src=\"" . WEB_PATH . "/images/staff/\" class=\"icon\" border=\"0\" /></a>\n";
  echo "          <div class=\"super\"></div>\n";
  echo "          <div class=\"header\"><a href=\"" . WEB_PATH . "/multimedia/comics/\">Other Comics</a></div>\n";
  echo "          <div style=\"clear:both;\"></div>\n";
  echo "          <center>\n";
  
  $newer_comics = $database->query("SELECT handle, title, published, image_file FROM cartoons WHERE date_posted>='" . date("Y-m-d", $this->_datePosted) .
	"' AND identity<>'" . $this->_identity . "'" . (!ADMIN_VIEW ? " AND published='TRUE'" : "") . " ORDER BY date_posted ASC LIMIT 10");
  $number_older_comics_to_allow = 20 - $newer_comics->numberRows();
  $older_comics = $database->query("SELECT handle, title, published, image_file FROM cartoons WHERE date_posted<='" . date("Y-m-d", $this->_datePosted) .
	"' AND identity<>'" . $this->_identity . "'" . (!ADMIN_VIEW ? " AND published='TRUE'" : "") . " ORDER BY date_posted DESC LIMIT " . $number_older_comics_to_allow);
  while ($older = $older_comics->fetchArray())
    echo "            <a href=\"" . WEB_PATH . "/comic/" . $older["handle"] . "/\" class=\"no_underline\"><img src=\"" . WEB_PATH . "/images/cartoons/" .
		$older["image_file"] . "\" class=\"comic_navigation_icon\" border=\"0\" /></a>\n";
  echo "          </center>\n";
  echo "          <div style=\"clear:left;\"></div>\n";
  echo "        </div>\n";
  
?>