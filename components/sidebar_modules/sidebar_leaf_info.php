<?php
if (!defined("DOCUMENT_ROOT"))
  exit("This page may only be called from within the website.");
if (!isset($this) || !isset($this->_leafIdentity) || !isset($this->_schoolYearPublished))
  exit("Enough information was not provided.");

if (!isset($database))
  $database = new DeitloffDatabase(DATABASE_PATH);
$leaf_info = $database->querySingle("SELECT issuu_identity, post_date, issue_name, color_cover, number_pages " .
	"FROM leaf_issuu WHERE issuu_identity='" . $database->escapeString($this->_leafIdentity) . "' LIMIT 1", true);
	
  echo "        <div class=\"sidePanelSectionContainer\">\n";
  echo "          <img src=\"" . WEB_PATH . "/images/leaf_covers/" . $leaf_info["color_cover"] . "\" class=\"icon\" border=\"0\" />\n";
  echo "          <div class=\"super\">" . $this->_schoolYearPublished . " - " . ($this->_schoolYearPublished + 1) . "</div>\n";
  echo "          <div class=\"header\">" . format_content($leaf_info["issue_name"]) . "</div>\n";
  echo "          <div style=\"clear:left;\"></div>\n";
  echo "          <div class=\"item\"><img src=\"" . WEB_PATH . "/images/icons/icon-years-on-staff.png\" /><span><b>Page count:</b> " .
    	$leaf_info["number_pages"] . "</span></div>\n";
  echo "        </div>\n";
?>