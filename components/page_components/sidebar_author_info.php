<?php
if (!defined("DOCUMENT_ROOT"))
  exit("This page may only be called from within the website.");
if (!isset($this) || !isset($this->_staffIdentity))
  exit("Staff identity was not provided.");
if (!isset($this->_authorTitle))
  exit("No author title has been supplied.");

if (!isset($database))
  $database = new DeitloffDatabase(DATABASE_PATH);
$staff_info = $database->querySingle("SELECT first_name, last_name, positions, identity, icon_file, active, year_graduating " .
	"FROM staff WHERE identity='" . $database->escapeString($this->_staffIdentity) . "' LIMIT 1", true);

  echo "        <div class=\"sidePanelSectionContainer\">\n";
  echo "          <a href=\"" . WEB_PATH . "/staff-portfolio/" . mb_strtolower($staff_info["first_name"] . "-" . $staff_info["last_name"]) .
	"/\"><img src=\"" . WEB_PATH . "/images/staff/" . $staff_info["icon_file"] . "\" class=\"icon\" border=\"0\" /></a>\n";
  echo "          <div class=\"super\">" . $this->_authorTitle . "</div>\n";
  echo "          <div class=\"header\"><a href=\"" . WEB_PATH . "/staff-portfolio/" . mb_strtolower($staff_info["first_name"] . "-" .
	$staff_info["last_name"]) . "/\">" . $staff_info["first_name"] . " " . $staff_info["last_name"] . "</a></div>\n";
  echo "          <div style=\"clear:left;\"></div>\n";
  $current_grade = (date("m") < 6 ? $staff_info["year_graduating"] - date("Y") : $staff_info["year_graduating"] - date("Y") - 1);
  
  if ($staff_info["active"] == "TRUE")
  {
    $grades_by_year = array("senior", "junior", "sophomore", "freshman");
    echo "          <div class=\"item\"><img src=\"" . WEB_PATH . "/images/icons/icon-staff-on-staff.png\" /><span>Currently in <b>" .
    	$grades_by_year[$current_grade] . "</b> year</span></div>\n";
  } else if ($current_grade < 0)
    echo "          <div class=\"item\"><img src=\"" . WEB_PATH . "/images/icons/icon-staff-not-on-staff.png\" /><span><b>Graduated</b> " .
    	$staff_info["year_graduating"] . "</span></div>\n";
  else
    echo "          <div class=\"item\"><img src=\"" . WEB_PATH . "/images/icons/icon-staff-not-on-staff.png\" /><span><b>No longer</b> " .
    	"on staff</span></div>\n";
    	
  if ($staff_info["active"] == "TRUE")
    echo "          <div class=\"item\"><img src=\"" . WEB_PATH . "/images/icons/icon-staff-position.png\" /><span>Current " . 
    	"Position: <b>" . $staff_info["positions"] . "</b></span></div>\n";
  echo "        </div>\n";
  
/*  echo "          <div class=\"item\"><img src=\"" . WEB_PATH . "/images/icons/" . ($this->_staffInfo["ON_STAFF"] ? "icon-staff-on-staff.png" : "icon-staff-not-on-staff.png") .
		"\"><span>" . $this->_staffInfo["STATUS"] . "</span></div>\n";

  echo "          <div class=\"item\"><img src=\"" . WEB_PATH . "/images/icons/icon-staff-position.png\" /><span><b>Current Position:</b> " . $this->_staffInfo["POSITION"] .
		"</span></div>\n";
	  $numbers_to_text = array("One", "Two", "Three", "Four");
	  echo "          <div class=\"item\"><img src=\"" . WEB_PATH . "/images/icons/icon-years-on-staff.png\" /><span><b>Years on Staff:</b> " . $numbers_to_text[$this->_staffInfo["YEARS_ON_STAFF"] - 1] . "</span></div>\n";

		
	  $this->_staffInfo = array("FIRST_NAME" => $info["first_name"], "LAST_NAME" => $info["last_name"], "POSITION" => $info["positions"], "IDENTITY"=> $info["staff_identity"], 
		"ICON" => $info["staff_icon"], "ON_STAFF" => $on_staff, "STATUS" => $staff_status, "YEARS_ON_STAFF" =>
		($on_staff ? 1 : 0) + $database->querySingle("SELECT count(*) FROM previous_staff_positions WHERE staff_identity='" .
		$info["staff_identity"] . "'"));*/
?>