<?php
if (!defined("DOCUMENT_ROOT"))
  exit("This page may only be called from within the website.");
if (!isset($this) || !isset($this->_relatedGroup))
  exit("Group identity was not provided.");

if (!isset($database))
  $database = new DeitloffDatabase(DATABASE_PATH);
  
  $group_info = $database->querySingle("SELECT group_identity, title, handle, icon_file FROM groups WHERE group_identity='" .
	$database->escapeString($this->_relatedGroup) . "' LIMIT 1", true);
	
  echo "        <div class=\"sidePanelSectionContainer\">\n";
  echo "          <a href=\"" . WEB_PATH . "/group/" . $group_info["handle"] . "/\"><img src=\"" . WEB_PATH . "/images/groups/" .
  	$group_info["icon_file"] . "\" class=\"icon\" border=\"0\" /></a>\n";
  echo "          <div class=\"super\">Latest content on the</div>\n";
  echo "          <div class=\"header\"><a href=\"" . WEB_PATH . "/group/" . $group_info["handle"] . "/\">" . $group_info["title"] .
  	"</a></div>\n";
  echo "          <div style=\"clear:left;\"></div>\n";
  
  $group_content = $database->query("SELECT identity, title, handle, type FROM group_content WHERE related_group='" .
  	$group_info["group_identity"] . "' ORDER BY datestamp DESC LIMIT " . RELATED_CONTENT_TO_DISPLAY);

  if ($group_content->numberRows() > 0)
  {
    while ($content = $group_content->fetchArray())
    {
      switch ($content["type"])
      {
        case "cartoon": $content_page_handle = "comic"; $content_page_icon = "icon-comic.png"; break;
        case "video": $content_page_handle = "video"; $content_page_icon = "icon-video.png"; break;
        case "photo_gallery": $content_page_handle = "photo-gallery"; $content_page_icon = "icon-photo-gallery.png"; break;
        case "sound_clip": $content_page_handle = "audio"; $content_page_icon = "icon-audio.png"; break;
        case "sound_slides": $content_page_handle = "sound-slides"; $content_page_icon = "icon-sound-slides.png"; break;
        case "article": $content_page_handle = "article"; $content_page_icon = "icon-article.png"; break;
      }
      $clipped_title = get_smart_blurb($content["title"], 5);
      if ($content_page_handle == $this->getPageHandle() && $content["identity"] == $this->_identity)
        echo "          <b><div class=\"item\">";
      else
        echo "          <a class=\"item\" href=\"" . WEB_PATH . "/" . $content_page_handle . "/" . $content["handle"] . "/\">";
      echo "<img src=\"" . WEB_PATH . "/images/icons/" . $content_page_icon . "\" border=\"0\" /><span>" .
      	$clipped_title . ($clipped_title != format_content($content["title"]) ? "..." : "") . "</span>";
      if ($content_page_handle == $this->getPageHandle() && $content["identity"] == $this->_identity)
        echo "</div></b>";
      else
        echo "</a>";
      echo "\n";
    }
    if ($group_content->numberRows() < $database->querySingle("SELECT count(*) FROM group_content WHERE related_group='" .
    	$group_info["group_identity"] . "'"))
    	echo "          <a href=\"" . WEB_PATH . "/group/" . $group_info["handle"] . "/content/\" class=\"text\">&raquo; More content &laquo;</a>\n";
  } else
    echo "          <div class=\"text\">There is no more posted content on this group.</div>\n";
  echo "        </div>\n";
?>