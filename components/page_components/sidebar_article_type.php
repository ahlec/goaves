<?php
if (!defined("DOCUMENT_ROOT"))
  exit("This page may only be called from within the website.");
if (!isset($this) || !isset($this->_typeIdentity))
  exit("Type identity was not provided.");

if (!isset($database))
  $database = new DeitloffDatabase(DATABASE_PATH);
  
  $type_info = $database->querySingle("SELECT type_identity, title, handle, icon_file FROM beat_types WHERE type_identity='" .
	$database->escapeString($this->_typeIdentity) . "' LIMIT 1", true);
	
  echo "        <div class=\"sidePanelSectionContainer\">\n";
  echo "          <a href=\"" . WEB_PATH . "/news/" . $type_info["handle"] . "/\"><img src=\"" . WEB_PATH . "/images/article_types/" .
  	$type_info["icon_file"] . "\" class=\"icon\" border=\"0\" /></a>\n";
  echo "          <div class=\"super\">Other articles concerning</div>\n";
  echo "          <div class=\"header\"><a href=\"" . WEB_PATH . "/news/" . $type_info["handle"] . "/\">" . $type_info["title"] .
  	"</a></div>\n";
  echo "          <div style=\"clear:left;\"></div>\n";
  
  $other_articles = $database->query("SELECT beat_identity, title, handle, published FROM beats WHERE type='" .
  	$type_info["type_identity"] . "'" . (!ADMIN_VIEW ? " AND published='TRUE'" : "") . " ORDER BY last_updated DESC LIMIT " . RELATED_CONTENT_TO_DISPLAY);

  if ($other_articles->numberRows() > 0)
  {
    while ($article = $other_articles->fetchArray())
    {
      $clipped_title = get_smart_blurb($article["title"], 5);
      if ($article["beat_identity"] == $this->_identity)
        echo "          <b><div class=\"item\">";
      else
        echo "          <a class=\"item\" href=\"" . WEB_PATH . "/article/" . $article["handle"] . "/\">";
	  if ($article["published"] == "FALSE")
	    echo "<span class=\"unpublishedMaterialNote\">[Unpublished]</span> ";
      echo "<img src=\"" . WEB_PATH . "/images/icons/icon-article.png\" border=\"0\" /><span>" .
      	$clipped_title . ($clipped_title != format_content($article["title"]) ? "..." : "") . "</span>";
      if ($article["beat_identity"] == $this->_identity)
        echo "</div></b>";
      else
        echo "</a>";
      echo "\n";
    }
    if ($other_articles->numberRows() < $database->querySingle("SELECT count(*) FROM beats WHERE type='" .
    	$type_info["type_identity"] . "'" . (!ADMIN_VIEW ? " AND published='TRUE'" : "")))
    	echo "          <a href=\"" . WEB_PATH . "/news/" . $type_info["handle"] . "/\" class=\"text\">&raquo; More articles &laquo;</a>\n";
  } else
    echo "          <div class=\"text\">There are no other articles in this category.</div>\n";
  echo "        </div>\n";
?>