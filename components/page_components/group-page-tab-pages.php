<?php
if (!defined("DOCUMENT_ROOT"))
  require_once ("../../config/main.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION))
  session_start();
if (!defined("ADMIN_VIEW"))
  define("ADMIN_VIEW", getAdminView());

/* Load Group */
if (isset($_GET["group_identity"]))
  $target_group = $database->escapeString($_GET["group_identity"]);
if (isset($_GET["path"]) && isset($this->_identity))
  $target_group = $database->escapeString($this->_identity);
if ($database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $target_group . "'") == 0)
  exit("Selected group does not exist (Group identity: '" . $target_group . "')");
$group_identity = $target_group;

/* Select Tab */
if (isset($_GET["path"]) && isset($this->_identity))
  $target_tab = $this->_startTab;
else
{
  switch ($_GET["tab"])
  {
    case "content": $target_tab = "content"; break;
	case "info": $target_tab = "info"; break;
	case "roster": $target_tab = "roster"; break;
	case "schedule": $target_tab = "schedule"; break;
	default: exit("No tab has been supplied");
  }
}
if (!isset($target_tab))
  exit("Could not determine which tab to select");

/* Output Tab */
switch ($target_tab)
{
  case "content":
  {
    echo "<div class=\"relatedContent\">\n";
	$media_content = $database->query("SELECT title, handle, type, datestamp, image FROM group_content WHERE related_group='" . $group_identity . "' AND type<>'article' ORDER BY datestamp DESC");
	
	echo "  <div class=\"relMediaContainer\">\n";
	echo "    <div class=\"relHeader\">Multimedia</div>\n";
	echo "    <div id=\"related-multimedia\">\n";
	$multimedia_display_identity = 0;
	while ($media = $media_content->fetchArray())
	{
	  switch ($media["type"])
	  {
	    case "cartoon": $media_type_page_handle = "comic"; $media_type_image_dir = "cartoons"; $media_type_icon = "icon-comic.png"; break;
		case "video": $media_type_page_handle = "video"; $media_type_image_dir = "video_thumbnails"; $media_type_icon = "icon-video.png"; break;
		case "photo_gallery": $media_type_page_handle = "photo-gallery"; $media_type_image_dir = "photos"; $media_type_icon = "icon-photo-gallery.png"; break;
		case "sound_slides": $media_type_page_handle = "sound-slides"; $media_type_image_dir = "sound_slides"; $media_type_icon = "icon-sound-slides.png"; break;
		case "sound_clip": $media_type_page_handle = "audio"; $media_type_image_dir = ""; $media_type_icon = "icon-audio.png"; break;
	  }
	  echo "      <a href=\"" . WEB_PATH . "/" . $media_type_page_handle . "/" . $media["handle"] .
		"/\" class=\"mediaIconLink\"><div class=\"mediaIcon\" " .
		"id=\"related-multimedia-" . $multimedia_display_identity . "\" onMouseOver=\"groupPageRelatedMultimediaHover('" . 
		$multimedia_display_identity . "');\" onMouseOut=\"groupPageRelatedMultimediaOut('" . $multimedia_display_identity .
		"');\">\n";
	  echo "        <img src=\"" . WEB_PATH . "/images/" . $media_type_image_dir . "/" . $media["image"] . "\" class=\"mediaImage\" />\n";
	  echo "        <div class=\"mediaImageOverlay\" id=\"related-multimedia-overlay-" . $multimedia_display_identity . "\"></div>\n";
	  echo "        <img src=\"" . WEB_PATH . "/images/icons/" . $media_type_icon . "\" class=\"mediaType\" />\n";
	  echo "        <div class=\"mediaTitle\" style=\"display:none;\" id=\"related-multimedia-title-" . $multimedia_display_identity . "\">" .
		$media["title"] . "</div>\n";
	  echo "      </div></a>\n";
	  $multimedia_display_identity += 1;
	}
	if ($media_content->numberRows() == 0)
	  echo "    <div class=\"relContentNoResults\">No multimedia</div>\n";
	echo "    </div>\n";
	echo "  </div>\n";
	
	$article_content = $database->query("SELECT title, handle, image_file, published, last_updated, contents FROM beats WHERE related_group='" . $group_identity .
		"'" . (ADMIN_VIEW ? "" : " AND published='TRUE'") . "	ORDER BY last_updated DESC");
    echo "  <div class=\"relArticleContainer\">\n";
	echo "    <div class=\"relHeader\">Articles</div>\n";
	$article_out_even = true;
	while ($article = $article_content->fetchArray())
	{
	  echo "      <div class=\"article " . ($article_out_even ? "even" : "odd") . ($article["published"] == "FALSE" ? " unpublishedMaterialNote" : "") . "\">\n";
	  echo "        <div class=\"image\"><a href=\"" . WEB_PATH . "/article/" . $article["handle"] . "/\"><img src=\"" .
		WEB_PATH . "/images/articles/" . $article["image_file"] . "\"></a></div>\n";
	  $title_shortened = utf8_encode(get_smart_blurb(format_content($article["title"]), 7));
	  echo "        <div class=\"articleBody\"><div class=\"title\">" . ($article["published"] == "FALSE" ? "<span class=\"unpublishedMaterialNote\">" : "") .
		"<a href=\"" . WEB_PATH . "/article/" . $article["handle"] . "/\" class=\"title\">" .
		$title_shortened . ($title_shortened != format_content($article["title"]) ? "..." : "") . "</a>" . ($article["published"] == "FALSE" ? "</span>" : "") .
		"</div>" . get_smart_blurb($article["contents"], 30) .
		"... <a href=\"" . WEB_PATH . "/article/" . $article["handle"] .
		"/\">Full Story &raquo;</a></div>\n";
	  echo "        <div class=\"time\">[" . date(DATE_FORMAT, strtotime($article["last_updated"])) . "]</div>\n";
	  echo "      </div>\n";
	  $article_out_even = !$article_out_even;
	}
	echo "  </div>\n";
	if ($article_content->numberRows() == 0)
	  echo "    <div class=\"relContentNoResults\">No articles</div>\n";
	
	echo "  <div style=\"clear:both;\"></div>\n";
	echo "</div>\n";
    break;
  }
  case "info":
  {
    $group_information = $database->querySingle("SELECT image_file, description, mission_statement, group_types.singular_name AS \"type_name_singular\", " .
		"group_types.handle AS \"type_handle\", active, years_active, group_last_updated FROM groups JOIN group_types ON groups.type = group_types.type_identity WHERE group_identity='" .
		$group_identity . "' LIMIT 1", true);
	$get_advisors = $database->query("SELECT first_name, last_name, gender FROM group_advisors WHERE group_identity='" . $group_identity . "'");
	echo "<div class=\"groupInfoItem\"><b>Type:</b> <a href=\"" . WEB_PATH . "/group-listing/" . $group_information["type_handle"] . "/\">" . $group_information["type_name_singular"] .
		"</a></div>\n";
	if ($group_information["years_active"] == "" && $group_information["active"] == "FALSE")
	  echo "<div class=\"groupInfoItem\"><b>Status:</b> No longer active.</div>\n";
	else if ($group_information["years_active"] != "")
	  echo "<div class=\"groupInfoItem\"><b>Years Active:</b> " . $group_information["years_active"] . ($group_information["active"] == "FALSE" ?
		" (No longer active)" : "") . "</div>\n";
	if ($group_information["mission_statement"] != "")
	  echo "<div class=\"groupInfoItem\"><b>Mission Statement:</b> " . $group_information["mission_statement"] . "</div>\n";
	echo "<center>\n";
	if ($group_information["description"] != "")
	  echo "  <div class=\"groupDescription\">" . $group_information["description"] . "</div>\n";
	if ($group_information["image_file"] != "")
	  echo "  <img src=\"" . WEB_PATH . "/images/groups/" . $group_information["image_file"] . "\" class=\"groupImage\" />\n";
	echo "<div class=\"groupInfoLastUpdated\">Last updated " . date(DATE_FORMAT, strtotime($group_information["group_last_updated"])) . "</div>\n";
	echo "</center>\n";
    break;
  }
  case "roster":
  {
	echo "<center>\n";
	$get_advisors = $database->query("SELECT first_name, last_name, gender, position FROM group_advisors WHERE group_identity='" . $group_identity . "'");
	if ($get_advisors->numberRows() > 0)
	  echo "<div class=\"rosterHeader\"><u>" . $database->querySingle("SELECT roster_administrator_title_" . ($get_advisors->numberRows() == 1 ? "singular" : "plural") . 
		" FROM group_types JOIN groups ON groups.type = group_types.type_identity WHERE groups.group_identity='" . $group_identity . "' LIMIT 1") . "</u></div>\n";
	while ($advisor_entry = $get_advisors->fetchArray())
	  echo "<div class=\"rosterEntry\"><b>" . $advisor_entry["first_name"] . " " . $advisor_entry["last_name"] . "</b>" . ($advisor_entry["position"] != "" ? 
		", " . $advisor_entry["position"] : "") . "</div>\n";
	if ($get_advisors->numberRows() > 0)
	  echo "<div class=\"rosterSubgroupDivider\"></div>\n";
    $get_roster = $database->query("SELECT group_roster.subgroup_identity, group_subgroups.name AS \"subgroup_name\", first_name, last_name, position, member_number, grade, " .
		"group_subgroups.image_file AS \"subgroup_image\" FROM group_roster LEFT JOIN group_subgroups ON group_subgroups.subgroup_identity = group_roster.subgroup_identity " .
		"WHERE group_roster.group_identity='" .	$group_identity . "' ORDER BY group_roster.subgroup_identity, sort_order, first_name, last_name ASC");
		
	if ($get_roster->numberRows() > 0)
	  echo "<div class=\"rosterHeader\"><u>" . $database->querySingle("SELECT roster_title FROM group_types JOIN groups ON groups.type = group_types.type_identity " .
		"WHERE groups.group_identity='" . $group_identity . "' LIMIT 1") . "</u></div>\n";
	$previous_subgroup_identity = null;
	while ($roster_entry = $get_roster->fetchArray())
	{
	  if ($previous_subgroup_identity === null || $previous_subgroup_identity != $roster_entry["subgroup_identity"])
	  {
	    if ($previous_subgroup_identity != null && $roster_entry["subgroup_identity"] != "")
	      echo "<div class=\"rosterSubgroupDivider\"></div>\n";
		if ($roster_entry["subgroup_name"] != "")
	      echo "<div class=\"rosterSubgroupHeader\">" . $roster_entry["subgroup_name"] . "</div>\n";
		echo "<img src=\"" . WEB_PATH . "/images/groups/" . ($roster_entry["subgroup_image"] != "" ? $roster_entry["subgroup_image"] :
			"no-subgroup-image.jpg") . "\" class=\"rosterSubgroupImage\" />\n";
	    $previous_subgroup_identity = ($roster_entry["subgroup_identity"] != "" ? $roster_entry["subgroup_identity"] : "");
	  }
	  if ($roster_entry["first_name"] == "Dummy"){
		continue;
	  }
	  echo "<div class=\"rosterEntry\"><b>" . $roster_entry["first_name"] . " " . $roster_entry["last_name"] . "</b>" .
		($roster_entry["grade"] != "" ? " (" . $roster_entry["grade"] . ")" : "") .
		($roster_entry["position"] != "" ? ", " . $roster_entry["position"] : "") .
		($roster_entry["member_number"] != "" ? " (#" .
		$roster_entry["member_number"] . ")" : "") . "</div>\n";
	}
	print_r($get_roster->fetchArray());
	if ($get_advisors->numberRows() == 0 && $get_roster->numberRows() == 0)
	  echo "<div class=\"rosterNoResults\">A roster has not yet been provided.</div>\n";
	else
	  echo "<div style=\"clear:both;\"></div>\n";
	echo "</center>";
    break;
  }
  case "schedule":
  {
    $get_events = $database->query("SELECT datetime, title, location_name, location_address, has_location FROM calendar WHERE related_group='" . $group_identity . "' ORDER BY datetime ASC");
	if ($get_events->numberRows() == 0)
	  echo "<div class=\"scheduleNoResults\">A schedule has not yet been provided.</div>\n";
	else
	{
	  echo "<div class=\"groupScheduleContainer\">\n";
	  echo "  <div class=\"header\">\n";
	  echo "    <div class=\"date\">Date</div>\n";
	  echo "    <div class=\"time\">Time</div>\n";
	  echo "    <div class=\"title\"></div>\n";
	  echo "    <div class=\"location\">Location</div>\n";
	  echo "  </div>\n";
	  $previous_day = null;
	  $previous_day_odd = true;
	  while ($event = $get_events->fetchArray())
	  {
	    $changed_days = false;
	    if ($previous_day != date("Y-m-d", strtotime($event["datetime"])))
		{
		  $previous_day_odd = !$previous_day_odd;
		  $previous_day = date("Y-m-d", strtotime($event["datetime"]));
		  $changed_days = true;
		}
		
	    echo "  <div class=\"entry " . ($previous_day_odd ? "odd" : "even") . "\">\n";
		echo "    <div class=\"date\">" . ($changed_days ? date(DATE_FORMAT, strtotime($event["datetime"])) : "") . "</div>\n";
		echo "    <div class=\"time\">" . date(TIME_FORMAT, strtotime($event["datetime"])) . "</div>\n";
		echo "    <div class=\"title\"><b>" . format_content($event["title"]) . "</b></div>\n";
		echo "    <div class=\"location\">";
	    if ($event["has_location"] == "TRUE")
		{
		  echo "<b>" . $event["location_name"] . "</b><br />\n";
		  echo $event["location_address"];
		}
		echo "</div>\n";
		echo "    <div style=\"clear:both;\"></div>\n";
		echo "  </div>\n";
	  }
	  echo "</div>\n";
	echo "<pre>";
	while ($event = $get_events->fetchArray())
	  print_r($event);
	echo "</pre>";
	}
    break;
  }
}
?>