<?php
if (!defined("DOCUMENT_ROOT"))
  require_once ("../../config/main.inc.php");
if (!isset($database))
  $database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION))
  session_start();
if (!defined("ADMIN_VIEW"))
  define("ADMIN_VIEW", (isset($_SESSION[MANAGE_SESSION]) && $_SESSION[MANAGE_PERMISSIONS_SESSION][9] == "1"));
  
$load_query = "SELECT group_identity, groups.handle, title, icon_file, groups.type, group_types.plural_name, groups.active FROM groups";
$join_statements = array("JOIN group_types ON group_types.type_identity = groups.type");
$where_clauses = array();

if (isset($this))
{
  if ($this->_currentGroupTypeIdentity != null)
  {
    $where_clauses[] = "type='" . $this->_currentGroupTypeIdentity . "'";
    $filter = $database->querySingle("SELECT plural_name FROM group_types WHERE type_identity='" . $this->_currentGroupTypeIdentity . "' LIMIT 1");
    $type_filter = true;
  } else
  {
    $where_clauses[] = "active='TRUE'";
	$filter = "Active";
	$type_filter = false;
  }
  $filtering = true;
}

if (!ADMIN_VIEW)
{
  //$where_clauses[] = "icon_file<>''";
  //$where_clauses[] = "icon_file IS NOT null";
}
if (isset($_GET["search"]) && $_GET["search"] == "true")
{
  $filter_query = $database->escapeString($_GET["query"]);
  $where_clauses[] = "(title LIKE '%" . $filter_query . "%')";
  $searching = true;
} else
  $searching = false;
  
  /////
if (isset($_GET["filter"]))
{
  $type_filter = false;
  if ($_GET["filter"] == "active")
  {
    $where_clauses[] = "active='TRUE'";
    $filter = "Active";
  } else if ($_GET["filter"] == "inactive")
  {
    $where_clauses[] = "active='FALSE'";
    $filter = "Inactive";
  } else if (strpos($_GET["filter"], "type-") !== false)
  {
    $group_type = $database->escapeString(str_replace("type-", "", $_GET["filter"]));
    $where_clauses[] = "groups.type='" . $group_type . "'";
    $filter = $database->querySingle("SELECT plural_name FROM group_types WHERE type_identity='" . $group_type . "' LIMIT 1");
    $type_filter = true;
  }
  $filtering = true;
} else if (!isset($filtering))
  $filtering = false;
  
  ////
foreach ($join_statements as $join_statement)
  $load_query .= " " . $join_statement;
if (sizeof($where_clauses) > 0)
  $load_query .= " WHERE";
foreach ($where_clauses as $identity => $clause)
  $load_query .= ($identity > 0 ? " AND" : "") . " " . $clause;
$load_query .= " ORDER BY type, title ASC";
$get_groups = $database->query($load_query);

if ($filtering || $searching)
  echo "<div class=\"bar\">" . ($searching ? "" : "All ") . ($filtering ? "<b>" . ($searching ? $filter : strtolower($filter)) . "</b>" . ($type_filter ? "" : " groups") : "Groups") .
	($searching ? " whose name contains '<b>" . stripslashes($filter_query) . "</b>'" : "") . ".</div>\n";

echo "<div id=\"group-listing-results\">\n";
$previous_type = null;
while ($group = $get_groups->fetchArray())
{
  if ($previous_type == null || $group["type"] !== $previous_type)
  {
    echo "            <div class=\"group-type-header\">" . $group["plural_name"] . "</div>\n";
    $previous_type = $group["type"];
  }
  echo "            <a href=\"" . WEB_PATH . "/group/" . $group["handle"] . "/\" onMouseOver=\"groupListingHoverIcon('" . $group["group_identity"] . "');\" " .
	"onMouseOut=\"groupListingLeaveIcon('" . $group["group_identity"] . "');\"><div class=\"group\">\n";
  echo "              <img src=\"" . WEB_PATH . "/images/groups/" . ($group["icon_file"] != "" ? $group["icon_file"] : "no-group-icon.jpg") .
	"\" class=\"groupIcon" . ($group["active"] == "FALSE" ? " inactiveGroup" : "") .
	"\" id=\"group-icon-" . $group["group_identity"] . "\" " .
	//"onMouseOver=\"blurAllOtherIconsStaffListing('" . $staff["identity"] . "'); createStaffListingHoverBox('" . $staff["first_name"] . "','" .
	//$staff["last_name"] . "');\" onMouseOut=\"unblurAllStaffListingIcons(); deleteStaffListingHoverBox();\" " .
	"title=\"" . $group["title"] . "\" />\n";
  echo "              <div class=\"groupTitle\" id=\"group-list-name-" . $group["group_identity"] . "\" style=\"display:none;\">" .$group["title"] . "</div>\n";
  //echo "              <div class=\"groupListIconOverlay\" id=\"group-list-overlay-" . $group["group_identity"] . "\"></div>\n";
  echo "            </div></a>\n";
}
echo "</div>\n";
if ($get_groups->numberRows() == 0)
{
  echo "            <div class=\"no-results\">There were no groups found that matched your search.</div>\n";
}
?>