<?php
if (!defined("DOCUMENT_ROOT") || !isset($this))
  exit("This page may only be called from within the website.");
if (!isset($database))
  $database = new DeitloffDatabase(DATABASE_PATH);
  
echo "      <div class=\"topContentStrip\">";
if (isset($_SESSION[PAGE_NAVIGATION_SESSION]["PREV_PAGE_HANDLE"]))
  echo "<a href=\"" . WEB_PATH . "/" . $_SESSION[PAGE_NAVIGATION_SESSION]["PREV_PAGE_HANDLE"] . "/" . (isset($_SESSION[PAGE_NAVIGATION_SESSION]["PREV_PAGE_SUBHANDLE"]) ?
  	$_SESSION[PAGE_NAVIGATION_SESSION]["PREV_PAGE_SUBHANDLE"] . "/" : "") . "\">&laquo; Return to previous page</a>";
echo "      </div>\n";
//<a href=\"" . WEB_PATH . "/multimedia/\">&laquo; Return to multimedia</a></div>\n";
?>