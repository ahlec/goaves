<?php
if (!defined("DOCUMENT_ROOT"))
  require_once ("../../config/main.inc.php");
else
  $_GET["filter"] = "active";
if (!isset($database))
  $database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_SESSION))
  session_start();
if (!defined("ADMIN_VIEW"))
  define("ADMIN_VIEW", (isset($_SESSION[MANAGE_SESSION]) && $_SESSION[MANAGE_PERMISSIONS_SESSION][9] == "1"));
$load_query = "SELECT identity, first_name, last_name, icon_file, active, display_on_staff_listing FROM staff";
$join_statements = array();
$where_clauses = array();
if (!ADMIN_VIEW)
  $where_clauses[] = "display_on_staff_listing='TRUE'";
if (isset($_GET["search"]) && $_GET["search"] == "true")
{
  $filter_query = $database->escapeString($_GET["query"]);
  $query_pieces = explode(" ", $filter_query);
  foreach ($query_pieces as $piece)
    $where_clauses[] = "(first_name LIKE '%" . $piece . "%' OR last_name LIKE '%" . $piece . "%')";
//  $where_clauses[] = "(first_name LIKE '%" . $filter_query . "%' OR last_name LIKE '%" . $filter_query . "%')";
  $searching = true;
} else
  $searching = false;
if (isset($_GET["filter"]))
{
  if ($_GET["filter"] == "active")
  {
    $where_clauses[] = "active='TRUE'";
    $filter = "Active";
  } else if (strpos($_GET["filter"], "year-start-") !== false)
  {
    $start_year = $database->escapeString(str_replace("year-start-", "", $_GET["filter"]));
    $join_statements[] = "JOIN previous_staff_positions ON previous_staff_positions.staff_identity = staff.identity";
    $where_clauses[] = "previous_staff_positions.start_year='" . $start_year . "'";
    $where_clauses[] = "previous_staff_positions.approval_pending='FALSE'";
    $filter = $start_year . " &ndash; " . ($start_year + 1);
  }
  $filtering = true;
} else
  $filtering = false;
foreach ($join_statements as $join_statement)
  $load_query .= " " . $join_statement;
if (sizeof($where_clauses) > 0)
  $load_query .= " WHERE";
foreach ($where_clauses as $identity => $clause)
  $load_query .= ($identity > 0 ? " AND" : "") . " " . $clause;
$load_query .= " ORDER BY last_name, first_name ASC";
$get_staff = $database->query($load_query);

if ($filtering || $searching)
  echo "<div class=\"bar\">" . ($searching ? "" : "All ") . ($filtering ? "<b>" . ($searching ? $filter : strtolower($filter)) . "</b> staff" : "Staff") . " members" .
	($searching ? " whose name contains '<b>" . stripslashes($filter_query) . "</b>'" : "") . ".</div>\n";

while ($staff = $get_staff->fetchArray())
{
  echo "            <a href=\"" . WEB_PATH . "/staff-portfolio/" . mb_strtolower($staff["first_name"] . "-" . $staff["last_name"]) . "/\">" .
	"<img src=\"" . WEB_PATH . "/images/staff/" . $staff["icon_file"] . "\" class=\"staffIcon\" id=\"staff-icon-" . $staff["identity"] . "\" " .
	"onMouseOver=\"blurAllOtherIconsStaffListing('" . $staff["identity"] . "'); createStaffListingHoverBox('" . $staff["first_name"] . "','" .
	$staff["last_name"] . "');\" onMouseOut=\"unblurAllStaffListingIcons(); deleteStaffListingHoverBox();\" " .
	"title=\"" . $staff["first_name"] . " " . $staff["last_name"] . "\" /></a>\n";
  //echo $staff["first_name"] . " " . $staff["last_name"] . "<br />\n";
}
?>