<?php
if (!isset($_GET["poll"]))
  exit ("Must select a poll to vote in.");
if (!isset($_GET["response"]))
  exit ("Must select a response for this poll.");
session_start();
require_once ("../config/main.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
$poll_identity = $database->escapeString($_GET["poll"]);
if ($database->querySingle("SELECT count(*) FROM polls WHERE identity='" . $poll_identity .
	"' AND date_start <= '" . date("Y-m-d", strtotime("+1 hours")) . "' AND date_start <= '" .
	date("Y-m-d", strtotime("+1 hours")) . "'") == 0)
  exit ("Poll does not exist or is not active.");
$response_identity = $database->escapeString(str_replace("poll-response-", "", $_GET["response"]));
if ($database->querySingle("SELECT count(*) FROM poll_responses WHERE poll_identity='" .
	$poll_identity . "' AND response_identity='" . $response_identity . "'") == 0)
  exit ("The response you provided does not exist for this poll.");
if (isset($_SESSION[POLL_VOTING_SESSION][$poll_identity]))
  exit ("You have already voted in this poll.");
$success = $database->exec("INSERT INTO poll_votes(poll_identity, response_identity, user, ip_address) " .
	"VALUES('" . $poll_identity . "','" . $response_identity . "','','" .$_SERVER['REMOTE_ADDR'] . "')");
if ($success)
{
  if (!isset($_SESSION[POLL_VOTING_SESSION]))
    $_SESSION[POLL_VOTING_SESSION] = array();
  $_SESSION[POLL_VOTING_SESSION][$poll_identity] = true;
  exit ("success");
} else
  exit ("Error posting vote to database.");
?>