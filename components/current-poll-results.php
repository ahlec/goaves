<?php
if (!isset($_GET["poll"]))
  exit ("Must select a poll to vote in.");
session_start();
require_once ("../config/main.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
$poll_identity = $database->escapeString($_GET["poll"]);
if ($database->querySingle("SELECT count(*) FROM polls WHERE identity='" . $poll_identity .
	"' AND date_start <= '" . date("Y-m-d", strtotime("+1 hours")) . "' AND date_start <= '" .
	date("Y-m-d", strtotime("+1 hours")) . "'") == 0)
  exit ("Poll does not exist or is not active.");
  
require_once (DOCUMENT_ROOT . "/config/polls.inc.php");
$latestPoll = loadLatestPoll($database, true);

echo "<div class=\"voteCounted\">Your vote has been counted.</div>\n";
foreach ($latestPoll["RESPONSES"] as $response)
	outputProgressBar($response["TEXT"], null, null, ($latestPoll["TOTAL_VOTES"] == 0 ? 0 : floor(($response["VOTES"] / $latestPoll["TOTAL_VOTES"]) * 100)), 215);
?>