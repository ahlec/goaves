<?php
  $start_time = microtime();
  ini_set("session.cookie_domain", "goaves.com");
  @session_start();
  require_once ("config/main.inc.php");
  require_once (DOCUMENT_ROOT . "/config/manage.conf.php");
  require_once (DOCUMENT_ROOT . "/config/error.php");
  set_error_handler("preoutputErrorHandling");
  require_once (DOCUMENT_ROOT . "/config/navigation.inc.php");
  require_once (DOCUMENT_ROOT . "/config/pages.inc.php");
  require_once (DOCUMENT_ROOT . "/config/pages/page_definitions.php");
  $database = new DeitloffDatabase(DATABASE_PATH);
  if ($database === false)
    trigger_error("Unable to connect to database (" . @$database->lastErrorMsg() . ")", E_USER_ERROR);
	
/* Load Staff Member */
  define("ADMIN_VIEW", (isset($_SESSION[MANAGE_SESSION]) && substr($_SESSION[MANAGE_SESSION]["PERMISSIONS"], 10, 1) != "0"));
  
/* Visitor Session */
  /*if (!isset($_SESSION[VISITOR_SESSION]))
  {
    $access_time = date("Y-m-d H:i:s");
    $database->exec("INSERT INTO visitors(ip_address, date_time, is_staff) VALUES('" . $database->escapeString($_SERVER["REMOTE_ADDR"]) .
    	"','" . $access_time . "','" . (isset($_SESSION[MANAGE_SESSION]) ? "TRUE" : "FALSE") .
    	"')");
    $visitor_session_identity = $database->querySingle("SELECT session_identity FROM visitors WHERE ip_address='" .
    	$database->escapeString($_SERVER["REMOTE_ADDR"]) . "' AND date_time='" . $access_time . "' AND is_staff='" .
    	(isset($_SESSION[MANAGE_SESSION]) ? "TRUE" : "FALSE") . "' LIMIT 1");
    $_SESSION[VISITOR_SESSION] = array("SESSION_IDENTITY" => $visitor_session_identity, "IS_STAFF" => isset($_SESSION[MANAGE_SESSION]));
  } else if (!$_SESSION[VISITOR_SESSION]["IS_STAFF"] && isset($_SESSION[MANAGE_SESSION]))
  {
    $database->exec("UPDATE visitors SET is_staff='TRUE' WHERE session_identity='" .
    	$database->escapeString($_SESSION[VISITOR_SESSION]["SESSION_IDENTITY"]) . "'");
    $_SESSION[VISITOR_SESSION]["IS_STAFF"] = true;
  }*/
    
/* Load Page */
  $called_path = (isset($_GET["path"]) ? $_GET["path"] : "");
  $path_pieces = parsePath($called_path, $_PAGES, $_ERROR_PAGES, $database, ADMIN_VIEW);
  $page = selectPage($path_pieces, $_PAGES, $database);
  if (method_exists($page, "loadSearch"))
	$page->loadSearch(isset($_GET["search"]) ? $_GET["search"] : null, isset($_GET["scope"]) ? $_GET["scope"] : null, $database);
  
/* Manage navigation session */
  if (!isset($_SESSION[PAGE_NAVIGATION_SESSION]))
    $_SESSION[PAGE_NAVIGATION_SESSION] = array("CUR_PAGE_HANDLE" => $page->getPageHandle(), "CUR_PAGE_SUBHANDLE" => $page->getPageSubhandle());
  else if ($_SESSION[PAGE_NAVIGATION_SESSION]["CUR_PAGE_HANDLE"] != $page->getPageHandle() || ($_SESSION[PAGE_NAVIGATION_SESSION] == $page->getPageHandle() &&
  	$_SESSION[PAGE_NAVIGATION_SESSION]["CUR_PAGE_SUBHANDLE"] != $page->getPageSubhandle()))
    $_SESSION[PAGE_NAVIGATION_SESSION] = array("PREV_PAGE_HANDLE" => $_SESSION[PAGE_NAVIGATION_SESSION]["CUR_PAGE_HANDLE"], "PREV_PAGE_SUBHANDLE" => $_SESSION[PAGE_NAVIGATION_SESSION]["CUR_PAGE_SUBHANDLE"],
    	"CUR_PAGE_HANDLE" => $page->getPageHandle(), "CUR_PAGE_SUBHANDLE" => $page->getPageSubhandle());

  set_error_handler("goavesErrorHandling");
/* HTML Header */
  header("Cache-Control: no-cache, must-revalidate"); 
  echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
  echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\n";
  echo "<head>\n";
  
  echo "  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=8\" />\n";
  echo "  <meta http-equiv=\"Pragma\" content=\"no-cache\">\n";
  echo "  <meta http-equiv=\"cache-control\" content=\"no-cache\">\n";
  echo "  <meta http-equiv=\"expires\" content=\"0\">\n";
  echo "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n";
  echo "  <meta http-equiv=\"Content-Language\" content=\"en-US\" />\n";
  echo "  <meta name=\"description\" content=\"The Online Voice of Sycamore\" />\n";
  echo "  <meta name=\"copyright\" content=\"(c)2010 Sycamore Community High School\" />\n";
  echo "  <meta name=\"author\" content=\"Sycamore High School Journalism Staff\" />\n";
  
  $page_title = $page->getPageTitle();
  echo "  <title>" . ($page_title !== null ? $page_title . " &ndash; " : "") . "GoAves.com</title>\n";
  
  echo "  <link rel=\"shortcut icon\" href=\"" . WEB_PATH . "/layout/shortcut-icon.ico\" />\n";
  $theme_to_use = (ADMIN_VIEW && DEVELOPMENTAL_THEME != "" ? DEVELOPMENTAL_THEME : CURRENT_THEME);
  /*if (ADMIN_VIEW)
  {
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/skins/core/core-main.css.php\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/skins/core/core-goaves.css.php\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/skins/core/stylesheet-tab-news.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/skins/core/stylesheet-tab-leaf.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/skins/core/stylesheet-tab-multimedia.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/skins/core/stylesheet-tab-sports.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/skins/core/stylesheet-tab-groups.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/skins/core/stylesheet-tab-interactions.css\" />\n";
  } else
  {*/
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . $theme_to_use . "/stylesheet-main.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . $theme_to_use . "/stylesheet-tab-goaves.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . $theme_to_use . "/stylesheet-tab-news.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . $theme_to_use . "/stylesheet-tab-specials.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . $theme_to_use . "/stylesheet-tab-leaf.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . $theme_to_use . "/stylesheet-tab-multimedia.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . $theme_to_use . "/stylesheet-tab-sports.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . $theme_to_use . "/stylesheet-tab-groups.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . $theme_to_use . "/stylesheet-tab-interactions.css\" />\n";
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/Jcrop/css/jquery.Jcrop.css\" media=\"screen\" />\n";
  //}
  echo "  <link rel=\"stylesheet\" href=\"" . WEB_PATH . "/layout/" . $theme_to_use . "/stylesheet-admin.css\" />\n";
  
  echo "  <script>\n";
  echo "    var web_path = '" . WEB_PATH . "';\n";
  echo "    var manage_web_path = '" . MANAGE_WEB_PATH . "';\n";
  echo "    var current_page_handle = '" . $page->getPageHandle() . "';\n";
  echo "    var current_subhandle = '" . $page->getPageSubhandle() . "';\n";
  if (method_exists($page, "getPageJavascript"))
    echo $page->getPageJavascript();
  echo "  </script>\n";
  echo "  <script src=\"" . WEB_PATH . "/layout/professional/javascript-main.js\"></script>\n";
  echo "  <script src=\"" . WEB_PATH . "/layout/professional/javascript-manage.js\"></script>\n";
  echo "  <script src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.min.js\"></script>\n";
  echo "  <script src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.Jcrop.js\"></script>\n";
  
  echo "</head>\n";
  echo "<body" . (method_exists($page, "getBodyOnload") ? " onLoad=\"" . $page->getBodyOnload() . ";\"" : "") . ">\n";

/* Site Header */
  echo "    <center>\n";
  if (ADMIN_VIEW)
  {
    echo "    <div class=\"adminViewNotice\"><b>Admin View Mode.</b> You are an administrator for the GoAves staff, and so your " .
	"view of the website (when you are logged in to the management panel) is different.</div>\n";
  }
  echo "    <div class=\"siteHeader\" id=\"pageHeader\">\n";
  echo "      <div class=\"logo\"><a href=\"" . WEB_PATH . "/\"><img src=\"" . WEB_PATH . "/images/new-header.png\" border=\"0\" /></a></div>\n";// onClick=\"window.location='" . WEB_PATH . "/';\"></div>\n";
  
  echo "      <form method=\"GET\" class=\"searchPanel\" action=\"" . WEB_PATH . "/search/\" class=\"searchPanel\" id=\"pageSearch\" onClick=\"toggleSearch();\">\n";
	echo "        Site Search\n";
	echo "        <select class=\"scope\" name=\"scope\" id=\"pageSearch-Scope\">\n";
	echo "          <option value=\"all\">GoAves.com</option>\n";
	echo "          <option value=\"beats\">Articles</option>\n";
	echo "          <option value=\"staff\">Staff</option>\n";
	echo "          <option value=\"leaf\">The Leaf</option>\n";
	echo "          <option value=\"multimedia\">Multimedia</option>\n";
	echo "          <option value=\"galleries\">- Photo Galleries</option>\n";
	echo "          <option value=\"videos\">- Videos</option>\n";
	echo "          <option value=\"comics\">- Comics</option>\n";
	echo "          <option value=\"soundslides\">- Soundslides</option>\n";
	echo "          <option value=\"podcasts\">- Podcasts</option>\n";
	echo "          <option value=\"groups\">Groups</option>\n";
	echo "          <option value=\"sports\">Sports</option>\n";
	echo "        </select>\n";
	echo "        <input type=\"text\" name=\"search\" class=\"searchBox\" id=\"pageSearch-Search\" />\n";
	echo "      </form>\n";
  
  echo "    <div class=\"siteNavigation\">\n";
  
  echo "      <div class=\"main\" id=\"mainSiteNavigation\">\n";
  $main_tabs = getMainTabs($database);
  $loaded_tab = $page->getMainTab();
  foreach ($main_tabs as $tab)
    echo "        <a href=\"" . WEB_PATH . "/" . $tab->getLinkSrc() . "/\" class=\"item" . (strtolower($tab->getLinkSrc()) == $loaded_tab ? "LoadedHover" : "") .
    	"\" id=\"tab-" . strtolower($tab->getLinkSrc()) . "\" onMouseOver=\"navigationTab('" . strtolower($tab->getLinkSrc()) . 
		"');\" onMouseOut=\"clearNavigation();\">" . $tab->getTitle() . "<div class=\"obscurant\"></div></a>\n";
	
  echo "        <div style=\"clear:both;\"></div>\n";
  echo "      </div>\n";
  
  echo "      <div class=\"local\" id=\"localSiteNavigation\" onMouseOut=\"clearNavigation();\">\n";
  foreach ($main_tabs as $tab)
  {
    echo "        <div id=\"local-navigation-" . strtolower($tab->getLinkSrc()) . "\" class=\"localContainer" . (strtolower($tab->getLinkSrc()) == $loaded_tab ? " loadedLocal" : "") .
		"\">\n";
    foreach ($tab->getSubItems() as $subItem)
	  echo "          " . $subItem->getOutput($page->getPageHandle(), $page->getPageSubhandle()) . "\n";
	echo "        </div>\n";
  }
  echo "      </div>\n";
  echo "    </div>\n";
  
    echo "    </div>\n";
	
/* Main Column */
  echo "    <div class=\"contentBody\">\n";
  echo "      <div class=\"content\">\n";
  $page->getPageContents();
  echo "        <div style=\"clear:both;\"></div>\n";
  echo "      </div>\n";
  echo "    </div>\n";
  
/* Footer */
  echo "    <div class=\"siteFooter\">\n";
  echo "      <a href=\"http://www.twitter.com/TheLeafSHS\" target=\"_blank\"><img src=\"" . WEB_PATH . "/layout/twitter-logo.png\" border=\"0\" " .
	"class=\"twitter\" /></a>\n";
  echo "      GoAves.com is created and run by the staff of the Sycamore Leaf, a student " .
	"organization at Sycamore High School, Cincinnati, Ohio. The opinions expressed on GoAves.com are solely of the staff " .
	"of GoAves and The Sycamore Leaf, not of those in charge of said staff.<br />" .
	"<a href=\"" . WEB_PATH . "/contact/\">&laquo; Contact Information &raquo;</a><br />\n";
  $code_updated = $database->querySingle("SELECT value FROM statistics WHERE stat_handle='code_last_updated' LIMIT 1");
  $content_updated = $database->querySingle("SELECT value FROM statistics WHERE stat_handle='content_last_updated' LIMIT 1");
  echo "      <b>Content Last Updated:</b> " . date(DATE_FORMAT, strtotime($content_updated)) . " | <b>Code Last Updated:</b> " . 
	date(DATE_FORMAT, strtotime($code_updated)) . "\n";
  echo "    </div>\n";
  echo "  </center>\n";

/* Debug Output */
  if (DEBUG)
    echo "<br /><b>EXECUTION TIME:</b> " . (microtime() - $start_time) . " seconds";
 
  echo "</body>\n";
  echo "</html>\n";
?>