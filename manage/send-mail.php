<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
if (isset($_POST["send-mail"]))
{
  $recipient_identity = $database->escapeString($_POST["recipient-identity"]);
  $message = $database->escapeString($_POST["message"]);
  $send_time = date("Y-m-d H:i:s");
  if ($database->querySingle("SELECT count(*) FROM staff WHERE identity='" . $database->escapeString($recipient_identity) . "' AND active='TRUE'") > 0)
  {
    $success_send = $database->exec("INSERT INTO staff_messages(sender_identity, recipient_identity, time_sent, message, read_yet) VALUES('" .
    	$database->escapeString($_SESSION[MANAGE_SESSION]) . "','" . $recipient_identity . "','" . $send_time . "','" . $message . "','FALSE')");
    if ($success_send)
    {
      header("Location: " . WEB_PATH . "/manage/inbox.php?sent-message");
      exit();
    } else
      echo "<div class=\"error\"><b>Error.</b> Could not send message (programming error).</div>\n";
  } else
    echo "<div class=\"error\"><b>Error.</b> Recipient is either inactive or does not exist.</div>";
}
if (isset($_GET["reply"]))
{
  $reply_info = $database->querySingle("SELECT sender_identity FROM staff_messages WHERE message_identity='" . $database->escapeString($_GET["reply"]) . "' LIMIT 1", true);
}
outputManageHeader(isset($reply_info) ? "Reply to Message" : "Compose Message");
echo "<form method=\"post\" action=\"" . WEB_PATH . "/manage/send-mail.php\">\n";
echo "<b>To:</b> <select name=\"recipient-identity\">";
$all_staff = $database->query("SELECT identity, first_name, last_name FROM staff WHERE active='TRUE' ORDER BY first_name, last_name");
while ($staff = $all_staff->fetchArray())
  echo "<option value=\"" . $staff["identity"] . "\"" . (isset($reply_info) && $reply_info["sender_identity"] == $staff["identity"] ? " selected=\"selected\"" : "") .
  ">" . $staff["first_name"] . " " . $staff["last_name"] . "</option>\n";
echo "</select><br />\n";
echo "<textarea name=\"message\" style=\"width:100%;\" rows=\"10\"></textarea><br />\n";
echo "<input type=\"submit\" value=\"Send\" name=\"send-mail\" />\n";
echo "</form>\n";
outputManageFooter();
?>