<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("Manage Soundslides");
if (isset($_SESSION["toggle-publish-results"]))
{
  if (isset($_SESSION["toggle-publish-results"]["CODE"]))
  {
    switch ($_SESSION["toggle-publish-results"]["CODE"])
    {
      case "03": echo "<div class=\"error\"><b>Error.</b> Slideshow does not exist or you are not the author to the slideshow.</div>\n"; break;
	  case "04": echo "<div class=\"error\"><b>Error.</b> Could not update the slideshow (programming error).</div>\n"; break;
	  case "05": echo "<div class=\"error\"><b>Error.</b> Could not publish the slideshow -- there are no slides in the slideshow yet.</div>\n";
    }
  } else if (isset($_SESSION["toggle-publish-results"]["SUCCESS"]))
    echo "<div class=\"success\"><b>Success.</b> The slideshow has been " . $_SESSION["toggle-publish-results"]["NEW_TOGGLED_VALUE"] . ".</div>\n";
  unset($_SESSION["toggle-publish-results"]);
}
$soundslides = $database->query("SELECT slideshow_identity, title, published, post_date, thumbnail_image FROM sound_slides WHERE staff_identity='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]) . "' ORDER BY post_date DESC");
if ($soundslides->numberRows() == 0)
  echo "You have no soundslides.<br /><br />\n";
else
{
  while ($slideshow = $soundslides->fetchArray())
  {
    echo "<div class=\"articleContainer" . ($slideshow["published"] == "TRUE" ? " containerPublished" : " containerDraft") .  "\">" . 
	  "<b>" . format_content($slideshow["title"]) . "</b> <span style=\"font-size:80%;\">&ndash; (Posted: " . date(DATE_FORMAT, strtotime($slideshow["post_date"])) . ")</span><br />\n";
	echo "<a href=\"" . WEB_PATH . "/manage/soundslides-slides.php?slideshow_identity=" . $slideshow["slideshow_identity"] . "\"><img src=\"" . WEB_PATH .
		"/images/icons/icon-sound-slides.png\" border=\"0\" class=\"draftIcon\" /> Manage Slides</a> ";
	/*echo "<a href=\"" . WEB_PATH . "/manage/toggle-publish.php?type=cartoon&identity=" . $comic["identity"] . "\"><img src=\"" .
		WEB_PATH . "/layout/manage-icon-toggle-" . ($comic["published"] == "TRUE" ? "unpublish" : "publish") . ".png\" border=\"0\" class=\"" .
		"draftIcon\" title=\"" . ($comic["published"] == "TRUE" ? "Unpublish" : "Publish") . "\" /> " . ($comic["published"] == "TRUE" ?
		"Unpublish" : "Publish") . "</a> ";*/
	$can_publish = false;
	if ($slideshow["thumbnail_image"] != "" && $database->querySingle("SELECT count(*) FROM sound_slides_slides WHERE slideshow_identity='" . $slideshow["slideshow_identity"] .
		"'") > 0)
		$can_publish = true;
	if ($can_publish)
	  echo "<a href=\"" . WEB_PATH . "/manage/toggle-publish.php?type=sound_slide&identity=" . $slideshow["slideshow_identity"] . "\"><img src=\"" . 
		WEB_PATH . "/layout/manage-icon-toggle-" . ($slideshow["published"] == "TRUE" ? "unpublish" : "publish") . ".png\" border=\"0\" class=\"" .
		"draftIcon\" title=\"" . ($slideshow["published"] == "TRUE" ? "Unpublish" : "Publish") . "\" /> " . ($slideshow["published"] == "TRUE" ?
		"Unpublish" : "Publish") . "</a> ";
	else
	  echo "<span><img src=\"" . WEB_PATH . "/layout/manage-icon-toggle-unpublish.png\" class=\"draftIcon\" />Unable to publish (no slides)</span> ";
	echo "</div>\n";
  }
  echo "<br>\n";
}
outputManageFooter();
?>