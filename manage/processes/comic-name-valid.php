<?php
require_once ("../../config/manage.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
$title = $database->escapeString(preg_replace("/(<\?php|<\?)([^\?>]+)(\?>)/","", html_entity_decode($_GET["name"])));
if (mb_strlen($title) < 2)
  exit ("0");
if ($database->querySingle("SELECT count(*) FROM cartoons WHERE title LIKE '" . $title . "'") != 0)
  exit ("0");
$handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $title))));
if (mb_strlen($handle) < 2)
  exit ("0");
if ($database->querySingle("SELECT count(*) FROM cartoons WHERE handle='" . $handle . "'") != 0)
  exit ("0");
exit ("1");
?>