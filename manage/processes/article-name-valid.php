<?php
require_once ("../../config/manage.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
$article_title = $database->escapeString(preg_replace("/(<\?php|<\?)([^\?>]+)(\?>)/","", html_entity_decode($_GET["name"])));
$article_identity = (isset($_GET["article"]) ? $database->escapeString($_GET["article"]) : null);
if (mb_strlen($article_title) < 8)
  exit ("0");
if ($database->querySingle("SELECT count(*) FROM beats WHERE title LIKE '" . $article_title . "'" . ($article_identity !== null ?
	" AND beat_identity <> '" . $article_identity . "'" : "")) != 0)
  exit ("0");
$article_handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $article_title))));
if (mb_strlen($article_handle) < 8)
  exit ("0");
if ($database->querySingle("SELECT count(*) FROM beats WHERE handle='" . $article_handle . "'" . ($article_identity !== null ?
	" AND beat_identity <> '" . $article_identity . "'" : "")) != 0)
  exit ("0");
exit ("1");
?>