<?php
function prepareString($string, $remove_elements = false)
{
  $value = utf8_encode($string);//html_entity_decode($string));
  $value = str_replace("�", "'", $value);
  $value = str_replace("�", "'", $value);
  $value = str_replace("�", '"', $value);
  $value = str_replace("�", '"', $value);
  $value = str_replace("�", "-", $value);
  $value = str_replace("�", "...", $value);
  $value = preg_replace("/(&nbsp;| )+/", " ", $value);
  if ($remove_elements)
  {
    $value = preg_replace("/<\/p>/", "", preg_replace("/<p(|[^\>]*)>/", "", $value));
    $value = preg_replace("/<\/span>/", "", preg_replace("/<span(|[^\>]*)>/", "", $value));
  }
  $value = str_replace("\\\"", "\"", $value);
  $value = trim($value);
  return $value;
}
?>