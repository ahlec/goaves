<?php
if (!defined("DOCUMENT_ROOT"))
{
  define("DYNAMIC_ACCESS", true);
  require_once ("../../config/manage.inc.php");
  processManageSession();
  $database = new DeitloffDatabase(DATABASE_PATH);
  if (!isset($_GET["desired_title"]))
    exit("Not enough parameters");
  exit(generate_handle($_GET["desired_title"]));
}
function generate_handle($from_title)
{
  $database = new DeitloffDatabase(DATABASE_PATH);
  $desired_title = trim($database->escapeString(preg_replace("/(<\?php|<\?)([^\?>]+)(\?>)/","", html_entity_decode($from_title))));
  $generated_handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $desired_title))));
  return $generated_handle;
}
?>