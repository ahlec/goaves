<?php
if (!defined("DOCUMENT_ROOT"))
{
  define("DYNAMIC_ACCESS", true);
  require_once ("../../config/manage.inc.php");
  processManageSession();
  $database = new DeitloffDatabase(DATABASE_PATH);
  if (!isset($_GET["file"]))
    exit("Not enough parameters");
  exit(getFileExtension($_GET["file"]));
}
function getFileExtension($filename)
{
  $pieces = explode(".", $filename);
  if (sizeof($pieces) == 0)
    return "No file specified";
  return mb_strtolower($pieces[sizeof($pieces) - 1]);
}
?>