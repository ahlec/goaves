<?php
require_once ("../../config/manage.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
$article_title = trim($database->escapeString(preg_replace("/(<\?php|<\?)([^\?>]+)(\?>)/","", html_entity_decode($_GET["name"]))));
$article_handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $article_title))));
exit ($article_handle);
?>