<?php
require_once ("../../config/manage.inc.php");
processManageSession();
$tmp_name = preg_split("(\\\\|/)", $_GET["tmp_name"]);
$tmp_name = $tmp_name[sizeof($tmp_name) - 1];

if (!isset($_SESSION[MANAGE_SESSION]))
  exit("00");
if (mb_strlen($tmp_name) == 0 || $tmp_name == ".." || $tmp_name == ".")
  exit("01");
if (!file_exists(DOCUMENT_ROOT . "/manage/temp/" . $tmp_name))
  exit("02");
if (!getimagesize(DOCUMENT_ROOT . "/manage/temp/" . $tmp_name))
  exit("03");
if (!unlink(DOCUMENT_ROOT . "/manage/temp/" . $tmp_name))
  exit("05");
exit ("04");
?>