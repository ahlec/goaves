<?php
require_once ("../../config/manage.inc.php");
processManageSession();
if ($_SESSION[MANAGE_PERMISSIONS_SESSION][9] !== "1")
  header("Location: " . WEB_PATH . "/manage/?permission=denied");
$database = new DeitloffDatabase(DATABASE_PATH);
$db_table = $database->escapeString($_GET["database"]);

$test_row = $database->query("SELECT * FROM " . $db_table . " LIMIT 1");
$columns = array();
$cur_column_identity = 0;
while ($test_row->fieldName($cur_column_identity) != false)
{
  $columns[$cur_column_identity] = $test_row->fieldName($cur_column_identity);
  $cur_column_identity++;
}

$xml_columns = array();
$xml_file = new DOMDocument();
$xml_file->loadXML($_SESSION["goavesRUSTYXMLPARSINGSESSION"]);
$xml_rows = $xml_file->getElementsByTagName("Row");
$header_row = $xml_rows->item(0);
$raw_xml_columns = $header_row->getElementsByTagName("Cell");
for ($parse_xml_column = 0; $parse_xml_column < $raw_xml_columns->length; $parse_xml_column++)
  $xml_columns[$parse_xml_column] = $raw_xml_columns->item($parse_xml_column)->nodeValue;

echo "<table style=\"width:100%;\">\n";
echo "<tr>\n";
echo "  <td style=\"border-bottom:1px solid black;text-align:center;border-right:1px dotted black;width:300px;\"><b>Column Name</b></td>\n";
echo "  <td style=\"border-bottom:1px solid black;\"><b>Binding</b></td>\n";
echo "</tr>\n";
foreach ($columns as $column_identity => $column)
{
  echo "<tr>\n";
  echo "  <td style=\"text-align:center;border-right:1px dotted black;width:300px;font-size:80%;border-bottom:1px dotted black;\">" . $column . "<input type=\"" .
	"hidden\" name=\"xml-parse-column-" . $column_identity . "-binding-database-column\" value=\"" . $column . "\" /></td>\n";
  echo "  <td style=\"text-align:left;font-size:80%;border-bottom:1px dotted black;\">";
  if ($column_identity > 0)
  {
    echo "  <b>Binding Type:</b> <input type=\"radio\" name=\"xml-parse-column-" . $column_identity . "-binding\" value=\"xml\" onChange=\"" .
		"document.getElementById('xml-parse-column-" . $column_identity . "-binding-value-label').innerHTML = 'XML Column:';document.getElementById('" .
			"xml-parse-column-" . $column_identity . "-binding-value-xml').style.display = 'block';document.getElementById('xml-parse-column-" .
			$column_identity . "-binding-value-text').style.display = 'none';\" /> XML " .
		"<input type=\"radio\" name=\"xml-parse-column-" . $column_identity . "-binding\" value=\"constant\" onChange=\"document.getElementById('" .
			"xml-parse-column-" . $column_identity . "-binding-value-label').innerHTML = 'Value:';document.getElementById('xml-parse-column-" .
			$column_identity . "-binding-value-xml').style.display = 'none';document.getElementById('xml-parse-column-" .
			$column_identity . "-binding-value-text').style.display = 'block';\" checked=\"checked\" /> Constant<br />\n";
	echo "  <b id=\"xml-parse-column-" . $column_identity . "-binding-value-label\">Value:</b>";
	echo "<select name=\"xml-parse-column-" . $column_identity . "-binding-value-xml\" id=\"xml-parse-column-" . $column_identity . "-binding-value-xml\" " .
		"style=\"display:none;\">\n";
	foreach ($xml_columns as $xml_column)
	  echo "<option>" . $xml_column . "</option>\n";
	echo "</select>\n";
	echo "<input type=\"text\" name=\"xml-parse-column-" . $column_identity .
		"-binding-value-text\" id=\"xml-parse-column-" . $column_identity . "-binding-value-text\" />\n";
  } else
    echo "<center>(Primary key may not be bound)</center>";
  echo "</td>\n";
  echo "</tr>\n";
}
echo "</table>\n";
echo "<center><input type=\"submit\" value=\"Parse and Bind\" name=\"bind-xml\" /></center>\n";
?>