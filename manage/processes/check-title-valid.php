<?php

if (!defined("DOCUMENT_ROOT"))
{
  define("DYNAMIC_ACCESS", true);
  require_once ("../../config/manage.inc.php");
  processManageSession();
  $database = new DeitloffDatabase(DATABASE_PATH);
  if (!isset($_GET["desired_title"]) || !isset($_GET["type"]) || !isset($_GET["min_length"]))
    exit("Not enough parameters");
  exit(check_title_valid($_GET["type"], $_GET["desired_title"], $_GET["min_length"], (isset($_GET["open_material_identity"]) ? $_GET["open_material_identity"] : null)));
//  exit(generate_handle($_GET["desired_title"]));
}

function check_title_valid($type, $desired_title, $min_length, $open_material_identity = null)
{
$database = new DeitloffDatabase(DATABASE_PATH);

$type = strtolower($type);
if ($type !== "article" && $type !== "sound_clip" && $type !== "article" && $type !== "photo_gallery" && $type !== "video" && $type !== "cartoon" && $type !== "sound_slide")
  return "Type is invalid";

$type_database = null;
$type_database_identity_column = null;
$type_name = null;
switch ($type)
{
  case "sound_clip": $type_database = "sound_clips"; $type_database_identity_column = "sound_identity"; $type_name = "audio clip"; break;
  case "article": $type_database = "beats"; $type_database_identity_column = "beat_identity"; $type_name = "article"; break;
  case "photo_gallery": $type_database = "photo_galleries"; $type_database_identity_column = "gallery_identity"; $type_name = "photo gallery"; break;
  case "video": $type_database = "videos"; $type_database_identity_column = "video_identity"; $type_name = "video"; break;
  case "cartoon": $type_database = "cartoons"; $type_database_identity_column = "identity"; $type_name = "comic"; break;
  case "sound_slide": $type_database = "sound_slides"; $type_database_identity_column = "slideshow_identity"; $type_name = "sound slideshow"; break;
  default: return "Invalid type";
}

$minimum_length = $min_length;
if (!is_int($minimum_length) && !ctype_digit($minimum_length))
  return "Minimum length must be an integer";

$desired_title = $database->escapeString(preg_replace("/(<\?php|<\?)([^\?>]+)(\?>)/","", html_entity_decode($desired_title)));
if (mb_strlen($desired_title) < $minimum_length)
  return "Title is not long enough (must be at least " . $minimum_length . " character" . ($minimum_length !== 1 ? "s" : "") . " in length)";

if ($open_material_identity != null)
{
  $identity = $database->escapeString($open_material_identity);
  if ($database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" . $identity . "'") == 0)
    return "Specified open item does not exist within type confines";
}

if ($database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE title LIKE '" . $desired_title . "'" . (isset($identity) ? " AND " . $type_database_identity_column .
	"<>'" . $identity . "'" : "")) > 0)
  return "Another " . $type_name . " already exists with this title.";

return "valid";
}
?>