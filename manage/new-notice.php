<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset($_POST["notice-title"]))
{
  $post_notice = true;
  $cancel_notice_reason = null;
  $title = $database->escapeString(preg_replace("/(<\?php|<\?)([^\?>]+)(\?>)/","", html_entity_decode($_POST["notice-title"])));
  $final_title_character = $title[mb_strlen($title) - 1];
  if ($final_title_character != "." && $final_title_character != "!" && $final_title_character != "?")
    $title .= ".";
  $explanation = $database->escapeString(preg_replace("/(<\?php|<\?)([^\?>]+)(\?>)/","", html_entity_decode($_POST["notice-explanation"])));
  if (mb_strlen($explanation) < 10)
  {
    $cancel_notice_reason = "Notice explanation must be a minimum of 10 characters.";
    $post_notice = false;
  }
  
  $post_code = "04";
  $query = "INSERT INTO notices(title, explanation) VALUES('" . $title . "','" . $explanation . "')";
  if (isset($_POST["notice-identity"]))
  {
    if ($database->querySingle("SELECT count(*) FROM notices WHERE notice_identity='" . $database->escapeString($_POST["notice-identity"]) . "'") == 0)
	  header("Location: " . WEB_PATH . "/manage/manage-notices.php?redirect=true&from=new-notice&code=01");
	$query = "UPDATE notices SET title='" . $title . "', explanation='" . $explanation . "' WHERE notice_identity='" .
		$database->escapeString($_POST["notice-identity"]) . "'";
	$post_code = "03";
  }
  if($post_notice && $database->exec($query))
    header("Location: " . WEB_PATH . "/manage/manage-notices.php?redirect=true&from=new-notice&code=" . $post_code);
}

if (isset($_GET["notice_id"]))
{
  if ($database->querySingle("SELECT count(*) FROM notices WHERE notice_identity='" . $database->escapeString($_GET["notice_id"]) . "'") == 0)
    header("Location: " . WEB_PATH . "/manage/manage-notices.php?redirect=true&from=new-notice&code=01");
  $notice_info = $database->querySingle("SELECT title, explanation FROM notices WHERE notice_identity='" . $database->escapeString($_GET["notice_id"]) .
	"' LIMIT 1", true);
}

outputManageHeader("New Notice");
if (isset($_POST["notice-title"]))
  echo "<div class=\"error\"><b>Error.</b> " . ($cancel_notice_reason == null ? "Unable to post the new notice." : $cancel_notice_reason) . "</div>\n";
  
  echo "  <form method=\"post\" enctype=\"multipart/form-data\">\n";
  if (isset($notice_info))
    echo "    <input type=\"hidden\" name=\"notice-identity\" value=\"" . $database->escapeString($_GET["notice_id"]) . "\" />\n";
  echo "    <span class=\"manageLabel alternate\" title=\"The title for the new notice.\">Brief title:</span><br />\n";
  echo "    <input type=\"text\" name=\"notice-title\" class=\"largeTextbox\" " . (isset($notice_info) ? "value=\"" . $notice_info["title"] . "\" " : "") .
	"autocomplete=\"off\" />\n";
  
  echo "    <span class=\"manageLabel alternate\" title=\"The explanation behind the brief title.\">Explanation:</span>";
  echo "    <div class=\"manageLabelPadding\"></div>\n";
  echo "    <textarea name=\"notice-explanation\" style=\"width:100%;\" class=\"simple-textarea\">" . (isset($notice_info) ? $notice_info["explanation"] : "") .
	"</textarea><br />\n";
  echo "      <div class=\"manageGuidelines\"><b>Guidelines:</b><br />\n";
  echo "        <ul>\n";
  echo "          <li>Must contain (at minimum) 10 characters.</li>\n";
  echo "        </ul>\n";
  echo "      </div>\n";
  
  echo "  <center>\n";
  echo "    <div class=\"divider\"></div>\n";
  echo "    <input type=\"submit\" class=\"largeButton green\" name=\"save-submit\" value=\"Save\" /><br />\n";
  echo "  </center>\n";
echo "  </form>\n";
outputManageFooter();
?>