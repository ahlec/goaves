<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("Manage Staff Positions");
echo "<div class=\"newChange\"><b>New Change.</b> Development<br />It's still a bit hazy. But it works (except for changing staff positions; that's what I'm doing now).<br />-Jacob</div>\n";

if (isset($_GET["change"]) && $_GET["change"] == "true")
{
  echo "<div class=\"notice\"><b>Notice.</b> This is not yet finished. This is the next aspect of the process that will be coded.</div>\n";
  if (isset ($_GET["type"]))
  {
    echo "Current Position";
  } else
  {
    echo "Past Position";
  }
  echo "[Here]";
  outputManageFooter();
  exit();
}

if (isset($_GET["delete"]) && $_GET["delete"] == "true")
{
  $position_identity = $database->escapeString($_GET["identity"]);
  if ($database->querySingle("SELECT count(*) FROM previous_staff_positions WHERE staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "' AND position_archive_identity='" . $position_identity .
	"'") > 0)
  {
    $position_information = $database->querySingle("SELECT start_year, end_year, position FROM previous_staff_positions WHERE position_archive_identity='" . $position_identity . "' LIMIT 1", true);
    echo "<center>Are you sure you wish to delete staff position<br /><b>" . $position_information["position"] . "</b><br />for the year<br /><b>" . $position_information["start_year"] . " &ndash; " .
	$position_information["end_year"] . "</b>?<br /><br />\n";
    echo "<form method=\"post\" action=\"" . WEB_PATH . "/manage/change-staff-positions.php?delete=final&identity=" . $position_identity . "\">\n";
    echo "  <input type=\"submit\" name=\"delete-yes\" class=\"largeButton red\" value=\"Yes\" />\n";
    echo "  <input type=\"submit\" name=\"delete-no\" class=\"largeButton green\" value=\"No\" />\n";
    echo "</form>\n";
    outputManageFooter();
    exit();
  } else
    echo "<div class=\"error\"><b>Error.</b> The staff position either does not exist in the database, or you are not the staff member it is registered to.</div>\n";
} else if (isset($_GET["delete"]) && $_GET["delete"] == "final" && isset($_POST["delete-yes"]))
{
  $position_identity = $database->escapeString($_GET["identity"]);
  if ($database->querySingle("SELECT count(*) FROM previous_staff_positions WHERE staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "' AND position_archive_identity='" . $position_identity .
	"'") > 0)
  {
    if ($database->exec("DELETE FROM previous_staff_positions WHERE position_archive_identity='" . $position_identity . "'"))
      echo "<div class=\"success\"><b>Success.</b> The staff position has been deleted.</div>\n";
    else
      echo "<div class=\"error\"><b>Error.</b> There was an error in the programming. Could not delete staff position.</div>\n";
  } else
    echo "<div class=\"error\"><b>Error.</b> The staff position either does not exist in the database, or you are not the staff member it is registered to.</div>\n";
}

if (isset($_POST["create-position-position"]))
{
  $new_position = $database->escapeString($_POST["create-position-position"]);
  if (mb_strlen($new_position) > 0)
  {
    if (ctype_digit($_POST["create-position-start-year"]) && ctype_digit($_POST["create-position-end-year"]))
    {
      if (strlen($_POST["create-position-start-year"]) == 4 && strlen($_POST["create-position-end-year"]) == 4)
      {
        if ($_POST["create-position-start-year"] < $_POST["create-position-end-year"])
        {
          if ($_POST["create-position-end-year"] - $_POST["create-position-start-year"] == 1)
          {
            $start_year = $database->escapeString($_POST["create-position-start-year"]);
            $end_year = $database->escapeString($_POST["create-position-end-year"]);
	     if ($database->querySingle("SELECT count(*) FROM previous_staff_positions WHERE staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "' AND start_year='" . $start_year . "'") == 0)
	     {
              $position = $database->escapeString($_POST["create-position-position"]);
	       $next_sort_order_position = $database->querySingle("SELECT sort_order FROM previous_staff_positions WHERE staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "' ORDER BY sort_order DESC LIMIT 1");
              if (!ctype_digit($next_sort_order_position))
                $next_sort_order_position = 0;
              $next_sort_order_position += 1;
              if ($database->exec("INSERT INTO previous_staff_positions(staff_identity, start_year, end_year, position, sort_order, approval_pending) VALUES('" . $database->escapeString($_SESSION[MANAGE_SESSION]) .
			"','" . $start_year . "','" . $end_year . "','" . $position ."','" . $next_sort_order_position . "','TRUE')"))
                echo "<div class=\"success\"><b>Success.</b> Your position has been submitted and will be confirmed by an administrator before it goes live.</div>\n";
              else
                echo "<div class=\"error\"><b>Error.</b> Programming error.</div>\n";
	     } else
	       echo "<div class=\"error\"><b>Error.</b> A position for you on staff has already been entered for that year. Please make all changes to positions below.</div>\n";
          } else
            echo "<div class=\"error\"><b>Error.</b> The range for start and end year must be only one year (2009-2010 is okay, 2009-2011 is not).</div>\n";
        } else
          echo "<div class=\"error\"><b>Error.</b> Start year must be less than the end year.</div>\n";
      } else
        echo "<div class=\"error\"><b>Error.</b> Years must be four characters long (YYYY).</div>\n";
    } else
      echo "<div class=\"error\"><b>Error.</b> Beginning and ending year must be numbers.</div>\n";
  } else
    echo "<div class=\"error\"><b>Error.</b> Position may not be blank.</div>\n";
}

echo "<div class=\"manageSubheader\">Create Staff Position</div>\n";

echo "<form method=\"POST\" action=\"" . WEB_PATH . "/manage/change-staff-positions.php\" style=\"margin:0px;\" onSubmit=\"return confirm('This position will be sent to be confirmed by a journalism " .
	"administrator before it is posted on your portfolio. Have you checked over your position before submitting it?');\">\n";
echo "<b><font color=\"red\">For past years only (Change your <u>current</u> staff position below).</font></b><br />\n";
echo "<b>School year:</b> <input type=\"text\" size=\"4\" maxlength=\"4\" name=\"create-position-start-year\" /> &mdash; <input type=\"text\" size=\"4\" maxlength=\"4\" name=\"create-position-end-year\" />" .
	" <small>(Must do one year increments)</small><br />\n";
echo "<b>Position:</b> <input type=\"text\" name=\"create-position-position\" size=\"100\" /><br />\n";
echo "<input type=\"submit\" value=\"Add\">\n";
echo "</form>\n";

echo "<br />\n";
echo "<div class=\"manageSubheader\">Current Staff Position</div>\n";
$current_staff_position = $database->querySingle("SELECT positions FROM staff WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "' LIMIT 1");
echo "<div class=\"manageContainer\">\n";
echo "  <div class=\"title\">" . $current_staff_position . "</div>\n";
echo "  <div class=\"actions\"><a href=\"" . WEB_PATH . "/manage/change-staff-positions.php?change=true&type=current\">Change</a></div>\n";
echo "</div>\n";

echo "<br />\n";
echo "<div class=\"manageSubheader\">Past Staff Positions</div>\n";
$past_staff_positions = $database->query("SELECT position_archive_identity, start_year, end_year, position, sort_order, approval_pending FROM previous_staff_positions WHERE staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) .
	"' ORDER BY sort_order ASC");
if ($past_staff_positions->numberRows() == 0)
  echo "No previous staff positions are listed.";
else
{
  while ($position = $past_staff_positions->fetchArray())
  {
    echo "<div class=\"manageContainer\">\n";
    echo "  <div class=\"title\">" . $position["position"] . ($position["approval_pending"] == "TRUE" ? " <i>(Awaiting Approval)</i>" : "") . "</div>\n";
    echo "  <div class=\"subtitle\">" . $position["start_year"] . " &ndash; " . $position["end_year"] . "</div>\n";
    echo "  <div class=\"actions\">\n";
    echo "    <a href=\"" . WEB_PATH . "/manage/change-staff-positions.php?change=true&identity=" . $position["position_archive_identity"] . "\">Change</a> &bull;\n";
    echo "    <a href=\"" . WEB_PATH . "/manage/change-staff-positions.php?delete=true&identity=" . $position["position_archive_identity"] . "\">Delete</a>\n";
    echo "  </div>\n";
    echo "</div>\n";
  }
}

echo "<br />\n";
outputManageFooter();
?>