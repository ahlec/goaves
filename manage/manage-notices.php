<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("Manage Notices");

if (isset($_GET["redirect"]))
{
  $from = $_GET["from"];
  $code = $_GET["code"];
  if ($from == "new-notice")
  {
    switch ($code)
	{
	  case "01" : echo "<div class=\"error\"><b>Error.</b> The notice you attempted to edit does not exist.</div>\n"; break;
	  case "03" : echo "<div class=\"success\"><b>Success.</b> The notice has been changed.</div>\n"; break;
	  case "04" : echo "<div class=\"success\"><b>Success.</b> Your notice has been posted.</div>\n"; break;
	}
  } else if ($from == "delete-notice")
  {
    switch ($code)
	{
	  case "01" : echo "<div class=\"error\"><b>Error.</b> The notice you tried to delete does not exist.</div>\n"; break;
	  case "04" : echo "<div class=\"success\"><b>Success.</b> The notice has been deleted.</div>\n"; break;
	}
  }
}

$notices = $database->query("SELECT notice_identity, title, explanation FROM notices ORDER BY notice_identity DESC");
if ($notices->numberRows() == 0)
  echo "There are no current notices.<br /><br />\n";
else
{
  while ($notice = $notices->fetchArray())
  {
    echo "<div class=\"articleContainer\"><b>" . $notice["title"] . "</b> <span style=\"font-size:80%;\">" . $notice["explanation"] . 
		"</span><br />\n";
	echo "<a href=\"" . WEB_PATH . "/manage/new-notice.php?notice_id=" . $notice["notice_identity"] . "\"><img src=\"" . 
		WEB_PATH . "/layout/manage-icon-edit.png\" border=\"0\" class=\"draftIcon\" title=\"Edit\" /> Edit</a> ";
	echo "<a href=\"" . WEB_PATH . "/manage/delete-notice.php?notice_id=" . $notice["notice_identity"] . "\"><img src=\"" . 
		WEB_PATH . "/layout/manage-icon-trash.png\" border=\"0\" class=\"draftIcon\" title=\"Delete\" /> Delete</a>";
	echo "</div>\n";
  }
  echo "<br>\n";
}
outputManageFooter();
?>