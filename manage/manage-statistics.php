<?php
error_reporting(E_ALL);
require_once ("../config/manage.inc.php");
processManageSession();
if ($_SESSION[MANAGE_PERMISSIONS_SESSION][9] !== "1")
  header("Location: " . WEB_PATH . "/manage/?permission=denied");
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("[Administration] Manage Statistics");  

if (isset($_POST["save-statistics"]))
{
  $code_last_updated = $_POST["code-last-updated"];
  if (!$database->exec("UPDATE statistics SET value='" . $database->escapeString($code_last_updated) . "' WHERE stat_handle='code_last_updated'"))
	exit("<div class=\"error\"><b>Error.</b> Unable to update code last updated</div>");
  else
       echo "<div class=\"success\"><b>Success.</b> 'Code last updated' has been changed.</div>\n";
}

echo "<form method=\"post\">\n";
echo "<b>Code last updated:</b> <input type=\"text\" name=\"code-last-updated\" value=\"" .
	$database->querySingle("SELECT value FROM statistics WHERE stat_handle='code_last_updated' LIMIT 1") . "\" /><br />\n";
echo "<input type=\"submit\" value=\"Save\" name=\"save-statistics\" />\n";
echo "</form>\n";

outputManageFooter();
?>