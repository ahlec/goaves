<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("Manage Articles");
if (isset($_GET["redirect"]))
{
  $from = $_GET["from"];
  $code = $_GET["code"];
  if ($from == "delete-article" && $code == "04")
    echo "<div class=\"success\"><b>Success.</b> Your article has been deleted.</div>\n";
  if ($from == "toggle-article")
  {
    switch ($code)
	{
	  case "01" : echo "<div class=\"error\"><b>Error.</b> The article you attempted to change does not exist.</div>\n"; break;
	  case "02" : echo "<div class=\"error\"><b>Error.</b> You are not the author of the article you attempted to toggle.</div>\n"; break;
	  case "03" : echo "<div class=\"error\"><b>Error.</b> Unable to toggle the article.</div>\n"; break;
	  case "04" : echo "<div class=\"success\"><b>Success.</b> Your article has been toggled.</div>\n"; break;
	}
  }
  if ($from == "post-article")
  {
    switch ($code)
	{
	  case "04" : echo "<div class=\"success\"><b>Success.</b> Your article has been posted. Publish it below if it is ready to go live.</div>\n"; break;
	}
  }
}
$articles = $database->query("SELECT beat_identity, title, post_time, published, last_updated FROM beats WHERE staff_identity='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]) . "' ORDER BY post_time DESC");
if ($articles->numberRows() == 0)
  echo "You have no articles.<br /><br />\n";
else
{
  while ($article = $articles->fetchArray())
  {
    echo "<div class=\"articleContainer" . ($article["published"] == "TRUE" ? " containerPublished" : " containerDraft") .  "\">" . 
	  "<b>" . format_content($article["title"]) . "</b> <span style=\"font-size:80%;\">&ndash; (Created: " . date(DATE_FORMAT, strtotime($article["post_time"])) . ", " .
	  "Last saved: " . date(DATE_FORMAT, strtotime($article["last_updated"])) . ")</span><br />\n";
	echo "<a href=\"" . WEB_PATH . "/manage/toggle-article.php?article_id=" . $article["beat_identity"] . "\"><img src=\"" .
		WEB_PATH . "/layout/manage-icon-toggle-" . ($article["published"] == "TRUE" ? "unpublish" : "publish") . ".png\" border=\"0\" class=\"" .
		"draftIcon\" title=\"" . ($article["published"] == "TRUE" ? "Unpublish" : "Publish") . "\" /> " . ($article["published"] == "TRUE" ?
		"Unpublish" : "Publish") . "</a> ";
	echo "<a href=\"" . WEB_PATH . "/manage/post-article.php?article_id=" . $article["beat_identity"] . "\"><img src=\"" . 
		WEB_PATH . "/layout/manage-icon-edit.png\" border=\"0\" class=\"draftIcon\" title=\"Edit\" /> Edit</a> ";
	echo "<a href=\"" . WEB_PATH . "/manage/delete-article.php?article_id=" . $article["beat_identity"] . "\"><img src=\"" . 
		WEB_PATH . "/layout/manage-icon-trash.png\" border=\"0\" class=\"draftIcon\" title=\"Delete\" /> Delete</a>";
	echo "</div>\n";
  }
  echo "<br>\n";
}
outputManageFooter();
?>