<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("Change Portfolio Bio");
if (isset($_POST["save"]))
{
  require_once(DOCUMENT_ROOT . "/manage/processes/string-management.inc.php");
  $bio = $database->escapeString(prepareString($_POST["staff-bio"]));
/*  $bio = $database->escapeString(preg_replace("/(<\?php|<\?)([^\?>]+)(\?>)/","", html_entity_decode($_POST["staff-bio"])));*/
  if ($database->querySingle("SELECT count(*) FROM staff WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "'") == 0)
    echo "<div class=\"error\"><b>Error.</b> Your staff identity is not in the database.</div>\n";
  else
  {
    $could_change = $database->exec("UPDATE staff SET short_bio='" . $bio . "' WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "'");
    if ($could_change)
      echo "<div class=\"success\"><b>Success.</b> Your portfolio has been updated.</div>\n";
    else
      echo "<div class=\"error\"><b>Error.</b> Unable to update your staff portfolio.</div>\n";	
  }
} else
  echo "<div class=\"newChange\"><b>New Change.</b> It's fixed!<br />In working on the new staff portfolios, I finally got around to fixing this. " .
	"Changing your staff portfolio <i>should</i> give you no errors or weird formatting errors. If it does, come and let me know.<br />-Jacob" .
	"</div>\n";


echo "  <form method=\"post\" onSubmit=\"return confirm('Save changes? (Suggestion: Have you run spell check on your article? It\'s the button on the third toolbar with a checkmark and \'ABC\' on it)');\">\n";

  echo "    <script type=\"text/javascript\" src=\"" . WEB_PATH . "/layout/tiny_mce/tiny_mce.js\"></script>\n";

  echo "    <script type=\"text/javascript\">\n";
  echo "      tinyMCE.init({\n";
  echo "        mode : \"exact\",\n"; //textareas\",\n";
  echo "        elements : \"staff-bio\",\n";
  echo "        entity_encoding : \"named\",\n";
  echo "        theme : \"advanced\",\n";
  echo "        skin : \"o2k7\",\n";
//  echo "        skin_variant : \"silver\",\n";
  echo "        editor_selector : \"advanced-textarea\",\n";
  echo "        plugins : \"safari,spellchecker,pagebreak,style,layer,save,advhr,advimage,advlink," .
        "iespell,inlinepopups,preview,media,searchreplace,print,contextmenu,paste,directionality," .
        "fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template\",\n";
  echo "        theme_advanced_buttons1 : \"cut,copy,paste,pastetext,|,search,replace,|,undo,redo,|,cleanup,code,|," .
        "preview,|,fullscreen,removeformat,visualaid\",\n";
  echo "        theme_advanced_buttons2 : \"bold,italic,underline,strikethrough,|,forecolor,link,unlink,|,sub,sup,|,charmap," .
        "iespell,|,bullist,numlist,|,outdent,indent\",\n";
  echo "        theme_advanced_buttons3 : \"styleprops," .
        "spellchecker,|,cite,abbr,acronym,del,ins,attribs,\",\n";
  echo "        theme_advanced_toolbar_location : \"top\",\n";
  echo "        theme_advanced_toolbar_align : \"left\",\n";
  echo "        theme_advanced_statusbar_location : \"bottom\",\n";
  echo "        theme_advanced_resizing : false,\n";
  //echo "        content_css : \"" . WEB_PATH . "/layout/stylesheet-main.css\",\n";
  echo "        template_external_list_url : \"js/template_list.js\",\n";
  echo "        external_link_list_url : \"js/link_list.js\",\n";
  //echo "        force_br_newlines : true,\n";
  //echo "        force_p_newlines : false,\n";
  echo "        remove_trailing_nbsp : true,\n";
  echo "        height : '360',\n";
  //echo "        forced_root_block : \"\",\n";
  //echo "        paste_retain_style_properties : \"\",\n";
  echo "        spellchecker_rpc_url : \"" . WEB_PATH . "/layout/tiny_mce/plugins/spellchecker/rpc.php\"\n";
  echo "      });\n";
  echo "      var web_path = '" . WEB_PATH . "';\n";
  echo "    </script>\n";

echo "    <textarea name=\"staff-bio\" id=\"staff-bio\" class=\"advanced-textarea normal\">" . utf8_encode(stripslashes(isset ($bio) ? 
str_replace("''", "'", $bio) : 
	$database->querySingle("SELECT short_bio FROM staff WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) .
	"' LIMIT 1"))) . "</textarea><br />\n";
echo "    <center><input type=\"submit\" class=\"largeButton green\" name=\"save\" value=\"Save\" /></center>\n";
echo "  </form>\n";
outputManageFooter();
?>
