<?php
require_once ("../config/manage.inc.php");
processManageSession();
if ($_SESSION[MANAGE_PERMISSIONS_SESSION][9] !== "1")
  exit("No permissions to be here");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_GET["type"]) || !isset($_GET["identity"]))
  exit("Not enough parameters");

$type = strtolower($_GET["type"]);
if ($type !== "sound_clip" && $type !== "article" && $type !== "photo_gallery" && $type !== "video" && $type !== "cartoon" && $type !== "sound_slide")
  exit ("Type is invalid");

$type_database = null;
$type_database_identity_column = null;
switch ($type)
{
  case "sound_clip": $type_database = "sound_clips"; $type_database_identity_column = "sound_identity"; break;
  case "article": $type_database = "beats"; $type_database_identity_column = "beat_identity"; break;
  case "photo_gallery": $type_database = "photo_galleries"; $type_database_identity_column = "gallery_identity"; break;
  case "video": $type_database = "videos"; $type_database_identity_column = "video_identity"; break;
  case "cartoon": $type_database = "cartoons"; $type_database_identity_column = "identity"; break;
  case "sound_slide": $type_database = "sound_slides"; $type_database_identity_column = "slideshow_identity"; break;
  default: exit("Invalid type");
}

$identity = $database->escapeString($_GET["identity"]);
if ($database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" . $identity . "'") == 0)
  exit("Specified item does not exist within type confines");

$get_current_value = ($database->querySingle("SELECT published FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" . $identity . "' LIMIT 1") == "TRUE");
$could_update = $database->exec("UPDATE " . $type_database . " SET published='" . ($get_current_value ? "FALSE" : "TRUE") . "' WHERE " . $type_database_identity_column . "='" . $identity . "'");
if (!$could_update)
  exit("Error - could not update");
exit ($get_current_value ? "unpublished" : "published");