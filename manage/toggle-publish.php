<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_GET["type"]) || !isset($_GET["identity"]))
{
  header("Location: " . WEB_PATH . "/manage/index.php?redirect=true&from=toggle-publish&code=01");
  exit();
}

$type = strtolower($_GET["type"]);
if ($type !== "sound_clip" && $type !== "article" && $type !== "photo_gallery" && $type !== "video" && $type !== "cartoon" && $type !== "sound_slide")
{
  header("Location: " . WEB_PATH . "/manage/index.php?redirect=true&from=toggle-publish&code=02");
  exit();
}

$type_database = null;
$type_database_identity_column = null;
switch ($type)
{
  case "sound_clip": $type_database = "sound_clips"; $staff_identity_column = ""; $type_database_identity_column = "sound_identity"; $return_page = "manage-audio.php"; break;
  case "article": $type_database = "beats"; $staff_identity_column = "staff_identity"; $type_database_identity_column = "beat_identity"; $return_page = "manage-articles.php"; break;
  case "photo_gallery": $type_database = "photo_galleries"; $is_complete_column = "is_complete"; $type_database_identity_column = "gallery_identity"; $return_page = "manage-galleries.php"; break;
  case "video": $type_database = "videos"; $staff_identity_column = ""; $type_database_identity_column = "video_identity"; $return_page = "manage-videos.php"; break;
  case "cartoon": $type_database = "cartoons"; $staff_identity_column = "artist"; $type_database_identity_column = "identity"; $return_page = "manage-comics.php"; break;
  case "sound_slide": $type_database = "sound_slides"; $staff_identity_column = "staff_identity"; $type_database_identity_column = "slideshow_identity"; $return_page = "manage-soundslides.php"; break;
  default: header("Location: " . WEB_PATH . "/manage/index.php?redirect=true&from=toggle-publish&code=02"); exit();
}

$identity = $database->escapeString($_GET["identity"]);
if ($database->querySingle("SELECT count(*) FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" . $identity . "'" . (isset($is_complete_column) || isset($staff_identity_column) ?
	" AND " : "") . (isset($staff_identity_column) ?  $staff_identity_column . "='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "'" : "") . (isset($is_complete_column) && isset($staff_identity_column) ?
	" AND " : "") . (isset($is_complete_column) ? $is_complete_column . "='TRUE'" : "")) == 0)
{
  $_SESSION["toggle-publish-results"] = array("FROM" => "toggle-publish", "CODE" => "03");
  header("Location: " . WEB_PATH . "/manage/" . $return_page);
  exit();
}

if ($type_database == "sound_slides")
{
  if ($database->querySingle("SELECT thumbnail_image FROM sound_slides WHERE slideshow_identity='" . $identity . "'") == "" ||
	  $database->querySingle("SELECT count(*) FROM sound_slides_slides WHERE slideshow_identity='" . $identity . "'") == 0)
	  {
	    $_SESSION["toggle-publish-results"] = array("FROM" => "toggle-publish", "CODE" => "05");
		header("Location: " . WEB_PATH . "/manage/" . $return_page);
		exit();
	  }
}

$get_current_value = ($database->querySingle("SELECT published FROM " . $type_database . " WHERE " . $type_database_identity_column . "='" . $identity . "' LIMIT 1") == "TRUE");
$could_update = $database->exec("UPDATE " . $type_database . " SET published='" . ($get_current_value ? "FALSE" : "TRUE") . "' WHERE " . $type_database_identity_column . "='" . $identity . "'");
if (!$could_update)
{
  header("Location: " . WEB_PATH . "/manage/" . $return_page . "?redirect=true&from=toggle-publish&code=04");
  exit();
}
$_SESSION["toggle-publish-results"] = array("SUCCESS" => true, "IDENTITY" => $identity, "NEW_TOGGLED_VALUE" => ($get_current_value ? "unpublished" : "published"));
header("Location: " . WEB_PATH . "/manage/" . $return_page);
exit();
?>