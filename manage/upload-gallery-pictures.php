<?php
error_reporting(E_ALL);
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_GET["identity"]) || $database->querySingle("SELECT count(*) FROM photo_galleries WHERE gallery_identity='" . $database->escapeString($_GET["identity"]) . "'") == 0)
{
  $_SESSION["upload-gallery-pictures-redirect"] = array("CODE" => "01");
  header("Location: " . WEB_PATH . "/manage/manage-galleries.php");
}
$gallery_identity = $database->escapeString($_GET["identity"]);
if (isset($_POST["upload-photos"]))
{
  $uploaded_photos = array();
  foreach ($_FILES as $file)
  {
    if ($file["error"] == 0)
      $uploaded_photos[] = array("NAME" => $file["name"], "TYPE" => $file["type"], "TMP_NAME" => $file["tmp_name"]
    else
      $upload_errors[] = "Error uploading '<b>" . $file["name"] . "</b>'";
  }
  echo "<pre>";
  print_r($_POST);
  print_r($_FILES);
  echo "</pre>";
  exit();
}
outputManageHeader("Upload Photos");
echo "<form method=\"post\" action=\"" . WEB_PATH . "/manage/upload-gallery-pictures.php?identity=" . $gallery_identity . "\" enctype=\"multipart/form-data\">\n";
echo "<div id=\"upload-gallery-photos\">\n";
echo "  <input type=\"file\" name=\"gallery-picture-upload-1\"><br />\n";
echo "  <script> var gallery_picture_upload_next = 2; </script>\n";
echo "</div>\n";
echo "  <span onClick=\"var new_file_upload = document.createElement('input');\n new_file_upload.setAttribute('type','file');\n new_file_upload.setAttribute('name','gallery-picture-upload-' + gallery_picture_upload_next.toString());\n " .
	"document.getElementById('upload-gallery-photos').appendChild(new_file_upload);document.getElementById('upload-gallery-photos').appendChild(document.createElement('br'));gallery_picture_upload_next += 1;\">+ Upload More</span>\n";
echo "<input type=\"submit\" value=\"Upload\" name=\"upload-photos\" />\n";
echo "</form>\n";
outputManageFooter();
exit();
?>