<?php
error_reporting(E_ALL);
require_once ("../config/manage.inc.php");
processManageSession();
if ($_SESSION[MANAGE_PERMISSIONS_SESSION][2] !== "1")
  header("Location: " . WEB_PATH . "/manage/?permission=denied");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset($_GET["gallery_id"]))
{
  $gallery_identity = $database->escapeString($_GET["gallery_id"]);
  if ($database->querySingle("SELECT count(*) FROM photo_galleries WHERE gallery_identity='" . $gallery_identity . "'") > 0)
    $gallery_information = $database->querySingle("SELECT gallery_identity, handle, title, related_group FROM photo_galleries WHERE gallery_identity='" . $gallery_identity . "' LIMIT 1", true);
}

if (isset($_POST["submit-gallery"]))
{
  require_once (DOCUMENT_ROOT . "/manage/processes/string-management.inc.php");
  require_once (DOCUMENT_ROOT . "/manage/processes/check-title-valid.php");
  require_once (DOCUMENT_ROOT . "/manage/processes/generate-handle.php");
  $title = prepareString($_POST["gallery-title"]);
  $handle = $database->escapeString(generate_handle($title));
  $title = $database->escapeString($title);
  $related_group = $_POST["related-group"];
  if (check_title_valid("photo_gallery", $title, 6) == "valid")
  {
    if ($database->querySingle("SELECT count(*) FROM photo_galleries WHERE handle LIKE '" . $handle . "'") == 0)
    {
      $could_create_gallery = $database->exec("INSERT INTO photo_galleries(handle, title, date_last_updated, published, related_group) VALUES('" . $handle . "','" . $title . "','" .
		date("Y-m-d H:i:s", strtotime("+1 hours")) . "','FALSE','" . $related_group . "')");
      if ($could_create_gallery)
      {
        $created_gallery_identity = $database->querySingle("SELECT gallery_identity FROM photo_galleries WHERE handle LIKE '" . $handle . "' LIMIT 1");
        $database->exec("INSERT INTO multimedia_feed(media_type, media_identity, post_time, post_type, posting_staff_member) VALUES('photo_gallery','" . $created_gallery_identity .
		"','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "','created','" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "')");
        header("Location: " . WEB_PATH . "/manage/manage-galleries.php?redirect=true&from=do-gallery&success=created");
        exit();
      } else
        $error = "Could not create the photo gallery in the database (Program error)";
    } else
      $error = "A photo gallery with this handle already exists";
  } else
    $error = check_title_valid("photo_gallery", $title, 6);
}
outputManageHeader((isset($gallery_information) ? "Edit Photo Gallery" : "Create Photo Gallery"), false, "enableDoGallery()");
if (isset($error))
  echo "<div class=\"error\"><b>Error.</b> " . $error . "</div>\n";
  echo "  <script>\n";
  echo "    var web_path = '" . WEB_PATH . "';\n";
  echo "  </script>\n";
  echo "  <form method=\"post\" action=\"" . WEB_PATH . "/manage/do-gallery.php" . (isset($gallery_information) ? "?gallery_id=" . $gallery_information["gallery_identity"] : "") . "\" >\n";
  echo "      <input type=\"text\" name=\"gallery-title\" class=\"largeTextbox " . (!isset($gallery_information) ? "in" : "") . "valid\" id=\"gallery-title\"" . (isset($gallery_information) ?
	" value=\"" . $gallery_information["title"] . "\"" : "") . " onKeyUp=\"checkTitleValid('gallery-title', 'gallery-title-validation-feedback','photo_gallery', this.value, 6" . (isset($gallery_information) ? 
	"," . $gallery_information["gallery_identity"] : "") . "); generateHandle('gallery-handle', this.value);\" autocomplete=\"off\" disabled=\"disabled\" />\n";
  echo "      <div class=\"validationErrorBox\" id=\"gallery-title-validation-feedback\"></div>\n";
  echo "      <div class=\"manageGuidelines\"><b>Guidelines:</b><br />\n";
  echo "        <ul>\n";
  echo "          <li>Must contain (at minimum) six characters.</li>\n";
  echo "          <li>Must be unique; no other photo gallery may have the same title.</li>\n";
  echo "        </ul>\n";
  echo "      </div>\n";
  echo "    <b>Handle:</b> '<span id=\"gallery-handle\">" . (isset($gallery_information) ? $gallery_information["handle"] : "") . "</span>' <small>(Automatically generates as you type)</small><br /><br />\n";
  echo "    <span class=\"manageLabel alternate\" title=\"A club or sports group that this photo gallery is related to.\">Related Group:</span> \n";
  $get_groups = $database->query("SELECT group_identity, title, type FROM groups ORDER BY title ASC");
  echo "    <select name=\"related-group\" id=\"gallery-related-group\" disabled=\"disabled\">\n";
  echo "      <option value=\"0\">(NONE)</option>\n";
  while ($group = $get_groups->fetchArray())
    echo "      <option value=\"" . $group["group_identity"] . "\"" . (isset($gallery_information) && $gallery_information["related_group"] == $group["group_identity"] ? " selected=\"selected\"" : "") .
	">" . $group["title"] . " (" . $group["type"] . ")</option>\n";
  echo "    </select><br /><br />\n";
  echo "    <center>\n";
  echo "      <input type=\"submit\" class=\"largeButton green\" value=\"" . (isset($gallery_information) ? "Edit" : "Create") . "\" name=\"submit-gallery\" id=\"submit-gallery\" disabled=\"disabled\" />\n";
  echo "    </center>\n";
  echo "  </form>\n";
  
outputManageFooter();
?>