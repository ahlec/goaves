<?php
error_reporting(E_ALL);
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("Change Staff Picture");
if (isset($_SESSION["upload-staff-picture"]) && isset($_GET["step"]) && $_GET["step"] == 2)
{
    echo "    <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"" . WEB_PATH . "/layout/Jcrop/css/jquery.Jcrop.css\"/>\n";
    echo "    <script type=\"text/javascript\" src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.min.js\"> </script>\n";
    echo "    <script type=\"text/javascript\" src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.Jcrop.js\"> </script>\n";
    echo "    <script type=\"text/javascript\">\n";
    echo "      jQuery(function(){\n";
    echo "        jQuery('#staff-image').Jcrop({\n";
    echo "          aspectRatio: 1.0,\n";
    echo "          bgOpacity: 0.4,\n";
    echo "          onSelect: updateCoords,\n";
    echo "          onChange: updateCoords\n";
    echo "        });\n";
    echo "      });\n";
    echo "      var cropbox_x = null;\n";
    echo "      var cropbox_y = null;\n";
    echo "      var cropbox_width = null;\n";
    echo "      var cropbox_height = null;\n";
    $image_size = getimagesize(DOCUMENT_ROOT . "/manage/temp/" . $_SESSION["upload-staff-picture"]);
    echo "      function updateCoords(cropbox)\n";
    echo "      {\n";
    echo "        var image_ratio = " . ($image_size[0] / ($image_size[0] > 800 ? 800 : $image_size[0])) . ";\n";
    echo "        cropbox_x = cropbox.x * image_ratio;\n";
    echo "        cropbox_y = cropbox.y * image_ratio;\n";
    echo "        cropbox_width = cropbox.w * image_ratio;\n";
    echo "        cropbox_height = cropbox.h * image_ratio;\n";
    echo "      }\n";
    echo "      function saveImage()\n";
    echo "      {\n";
    echo "        if (cropbox_x == null || cropbox_y == null || cropbox_width == null || cropbox_height == null)\n";
    echo "        {\n";
    echo "          alert(\"You must select a part of the image to use as your staff picture before moving to the next step.\");\n";
    echo "          return;\n";
    echo "        }\n";
    echo "        if ((cropbox_width < 60 || cropbox_height < 60) && !confirm(\"The selection you have chosen is less than 60x60pixels, and will have to scale up, potentially " .
	" resulting in pixelation. Are you SURE this is what you wish to do?\"))\n";
    echo "          return;\n";
    echo "        if (!confirm(\"Are you satisfied with the image selection?\"))\n";
    echo "          return;\n";
    echo "        executeAJAX('" . WEB_PATH . "/manage/scripts/crop-staff-icon.php?x=' + cropbox_x + '&y=' + cropbox_y + '&width=' + cropbox_width + '&height=' + " .
	"cropbox_height, function evaluate_results(value) {\n";
    echo "          if (value == 'success')\n";
    echo "            window.location = '" . WEB_PATH . "/manage/change-staff-picture.php';\n";
    echo "          else\n";
    echo "            alert (value);\n";
    echo "        });\n";
    echo "      }\n";
    echo "    </script>\n";
    echo "<b>Changing your staff picture</b>, step by step:<br />\n";
	echo "<ol>\n";
	echo "  <li><strike>Upload your picture</strike> (done)</li>\n";
	echo "  <li><strike>Crop your portfolio image</strike> (done)</li>\n";
	echo "  <li>Crop your headshot</li>\n";
	echo "</ol><br />\n";
    echo "  Crop your headshot the same way you cropped your picture in the previous step. For this, <font color=\"red\">you must focus on <b>only</b> your head</font>. " .
	"Your portfolio picture may be of anything you wish, but as your headshot represents you visually on every page, crop the picture so your face (and only your face) " .
	"is showing. Obviously, don't get super close; feel free to show a slight bit of background, so your face isn't boxed in. But don't show us Mickey Mouse smiling in the " .
	"background of your Disney World pictures. When you've finished, click the <b><font color=\"green\">Save</font></b> " .
	"button below.<br /><br />\n";
    echo "  <center><div style=\"width:" . ($image_size[0] > 800 ? 800 : $image_size[0]) . "px; border:1px solid black;\"><img src=\"" .
	WEB_PATH . "/manage/temp/" . $_SESSION["upload-staff-picture"] . "\" style=\"width:" . ($image_size[0] > 800 ? 800 : $image_size[0]) .
	"px;\" id=\"staff-image\" /></div></center><br />\n";
	echo "  <center><input type=\"button\" class=\"largeButton green\" value=\"Save\" onClick=\"saveImage();\" /> ";
	echo "<input type=\"button\" class=\"largeButton red\" value=\"Cancel\" onClick=\"cancelStaffPictureChange();\" />" .
		"</center><br />\n";
	exit();
}
else if (isset($_SESSION["upload-staff-picture"]) && isset($_GET["step"]) && $_GET["step"] == 1)
{
    echo "    <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"" . WEB_PATH . "/layout/Jcrop/css/jquery.Jcrop.css\"/>\n";
    echo "    <script type=\"text/javascript\" src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.min.js\"> </script>\n";
    echo "    <script type=\"text/javascript\" src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.Jcrop.js\"> </script>\n";
    echo "    <script type=\"text/javascript\">\n";
    echo "      jQuery(function(){\n";
    echo "        jQuery('#staff-image').Jcrop({\n";
    echo "          aspectRatio: .75,\n";
//    echo "          minSize: [300, 400],\n";
    echo "          bgOpacity: 0.4,\n";
    echo "          onSelect: updateCoords,\n";
    echo "          onChange: updateCoords\n";
    echo "        });\n";
    echo "      });\n";
    echo "      var cropbox_x = null;\n";
    echo "      var cropbox_y = null;\n";
    echo "      var cropbox_width = null;\n";
    echo "      var cropbox_height = null;\n";
    $image_size = getimagesize(DOCUMENT_ROOT . "/manage/temp/" . $_SESSION["upload-staff-picture"]);
    echo "      function updateCoords(cropbox)\n";
    echo "      {\n";
    echo "        var image_ratio = " . ($image_size[0] / ($image_size[0] > 800 ? 800 : $image_size[0])) . ";\n";
    echo "        cropbox_x = cropbox.x * image_ratio;\n";
    echo "        cropbox_y = cropbox.y * image_ratio;\n";
    echo "        cropbox_width = cropbox.w * image_ratio;\n";
    echo "        cropbox_height = cropbox.h * image_ratio;\n";
    echo "      }\n";
    echo "      function saveImage()\n";
    echo "      {\n";
    echo "        if (cropbox_x == null || cropbox_y == null || cropbox_width == null || cropbox_height == null)\n";
    echo "        {\n";
    echo "          alert(\"You must select a part of the image to use as your staff picture before moving to the next step.\");\n";
    echo "          return;\n";
    echo "        }\n";
    echo "        if ((cropbox_width < 300 || cropbox_height < 400) && !confirm(\"The selection you have chosen is less than 300x400pixels, and will have to scale up, potentially " .
	" resulting in pixelation. Are you SURE this is what you wish to do?\"))\n";
    echo "          return;\n";
    echo "        if (!confirm(\"Are you satisfied with the image selection?\"))\n";
    echo "          return;\n";
    echo "        executeAJAX('" . WEB_PATH . "/manage/scripts/crop-staff-picture.php?x=' + cropbox_x + '&y=' + cropbox_y + '&width=' + cropbox_width + '&height=' + " .
	"cropbox_height, function evaluate_results(value) {\n";
    echo "          if (value == 'success')\n";
    echo "            window.location = '" . WEB_PATH . "/manage/change-staff-picture.php?step=2';\n";
    echo "          else\n";
    echo "            alert (value);\n";
    echo "        });\n";
    echo "      }\n";
    echo "    </script>\n";
    echo "<b>Changing your staff picture</b>, step by step:<br />\n";
	echo "<ol>\n";
	echo "  <li><strike>Upload your picture</strike> (done)</li>\n";
	echo "  <li>Crop your portfolio image</li>\n";
	echo "  <li>Crop your headshot</li>\n";
	echo "</ol><br />\n";
    echo "  To begin cropping, click anywhere on your picture and drag. A box will appear, and using the handles (small gray boxes) on the corners and sides of the box, resize " .
	"the image and move it (click and drag) until what you have selected what you want your staff portfolio picture to be. Then click the <b><font color=\"green\">Save</font></b> " .
	"button below.<br /><br />\n";
    echo "  <center><div style=\"width:" . ($image_size[0] > 800 ? 800 : $image_size[0]) . "px; border:1px solid black;\"><img src=\"" .
	WEB_PATH . "/manage/temp/" . $_SESSION["upload-staff-picture"] . "\" style=\"width:" . ($image_size[0] > 800 ? 800 : $image_size[0]) .
	"px;\" id=\"staff-image\" /></div></center><br />\n";
	echo "  <center><input type=\"button\" class=\"largeButton green\" value=\"Save\" onClick=\"saveImage();\" /> ";
	echo "<input type=\"button\" class=\"largeButton red\" value=\"Cancel\" onClick=\"cancelStaffPictureChange();\" />" .
		"</center><br />\n";
	exit();
}
else if (isset($_POST["upload"]) || isset($_FILES["image-file"]))
{
  if (!isset($_SESSION["upload-staff-picture"]))
    $attempt_upload = uploadImage();
  if (isset($_SESSION["upload-staff-picture"]) || $attempt_upload["STATUS"])
  {
    if (!isset($_SESSION["upload-staff-picture"]))
      $_SESSION["upload-staff-picture"] = $attempt_upload["IMAGE_FILE"];
    $get_staff = $database->querySingle("SELECT image_file, icon_file FROM staff WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "'", true);
    $database->exec("UPDATE staff SET image_file='no-staff-picture.jpg', icon_file='no-staff-icon.jpg' WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "'");
    if ($get_staff["image_file"] !== "no-staff-picture.jpg")
      unlink(DOCUMENT_ROOT . "/images/staff/" . $get_staff["image_file"]);
    if ($get_staff["icon_file"] !== "no-staff-icon.jpg")
      unlink(DOCUMENT_ROOT . "/images/staff/" . $get_staff["icon_file"]);
    echo "<div style=\"font-size:200%;font-weight:bold;\">Let's Begin.</div>\n";
    echo "In order to change your picture, you <b>must crop both the picture and the headshot</b>. This is all a simple process, so if you follow along with the instructions we have " .
	"on each page, you'll be able to change your staff image successfully.<br /><br />\n";
    echo "<center><b><a href=\"" . WEB_PATH . "/manage/change-staff-picture.php?step=1\">Forward!</a></b></center><br/>\n";
    exit();
  } else
    echo "<div class=\"error\"><b>Error.</b> " . $attempt_upload["MESSAGE"] . "</div>\n";
}
if (isset($_GET["cancel"]))
{
  switch ($_GET["cancel"])
  {
    case "00": echo "<div class=\"error\"><b>Error.</b> You must be authentication in order to cancel picture upload. How'd you get this far?</div>\n"; break;
	case "01": echo "<div class=\"error\"><b>Error.</b> The picture you were trying to cancel with had an invalid filename or the filename was empty.</div>\n"; break;
	case "02": echo "<div class=\"error\"><b>Error.</b> The picture that you were trying to cancel with does not exist on the server.</div>\n"; break;
	case "03": echo "<div class=\"error\"><b>Error.</b> The picture that you were trying to cancel with is not an image file. Script will not delete.</div>\n"; break;
	case "05": echo "<div class=\"error\"><b>Error.</b> Unable to remove the uploaded picture from the temporary directory.</div>\n"; break;
  }
}
if (isset($_SESSION["upload-staff-picture"]))
  unset ($_SESSION["upload-staff-picture"]);
$has_staff_picture = ($database->querySingle("SELECT count(*) FROM staff WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) .
  "' AND image_file<>'no-staff-picture.jpg'") == 1);
if (isset($_POST["delete"]))
{
  if ($has_staff_picture)
  {
    $get_picture = $database->querySingle("SELECT image_file, icon_file FROM staff WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "' LIMIT 1", true);
	if ($get_picture["image_file"] !== "no-staff-picture.jpg")
		if (!unlink (DOCUMENT_ROOT . "/images/staff/" . $get_picture["image_file"]))
			echo "<div class=\"error\"><b>Error.</b> Unable to delete your picture.</div>\n";
	if ($get_picture["icon_file"] !== "no-staff-icon.jpg")
		if (!unlink (DOCUMENT_ROOT . "/images/staff/" . $get_picture["icon_file"]))
			echo "<div class=\"error\"><b>Error.</b> Unable to delete your headshot.</div>\n";
	else if (!$database->exec("UPDATE staff SET image_file='no-staff-picture.jpg', icon_file='no-staff-icon.jpg' WHERE identity='" .
		$database->escapeString($_SESSION[MANAGE_SESSION]) . "'"))
	  echo "<div class=\"error\"><b>Error.</b> Error updating the database when removing your picture.</div>\n";
	else
	{
	  echo "<div class=\"success\"><b>Success.</b> Your staff picture has been removed.</div>\n";
	  $has_staff_picture = false;
	} 
  } else
    echo "<div class=\"error\"><b>Error.</b> You do not have a picture set.</div>\n";
}
$current_staff_picture = $database->querySingle("SELECT image_file, icon_file FROM staff WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . 
  "' LIMIT 1", true);

echo "  <div class=\"success\"><b>Updated!</b> The code on this page has been updated, so the process is now easier for you, and the results have less errors. " .
	"If you're going to change your picture, make sure you read the instructions, as they <b>have</b> changed.</div>\n";

if (!isset($attempt_upload) || !$attempt_upload["STATUS"])
{
  echo "  <br />\n";
  echo "  <div class=\"manageSubheader\">Current Picture</div>\n";
  echo "  <center>\n";
  echo "    <img src=\"" . WEB_PATH . "/images/staff/" . $current_staff_picture["icon_file"] . "\" class=\"picture\" />\n";
  echo "    <img src=\"" . WEB_PATH . "/images/staff/" . $current_staff_picture["image_file"] . "\" class=\"picture\" /><br />\n";
  echo "    <small>(If you just changed your picture, but the images above are your old images, refresh the page, and they should update)</small>\n";
  echo "  </center><br /><br />\n";
  echo "  <div class=\"manageSubheader\">Upload</div>\n";
  echo "  <form method=\"post\" enctype=\"multipart/form-data\" action=\"" . WEB_PATH . "/manage/change-staff-picture.php\" >\n";
  //echo "    <input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"512000\" />\n";
  echo "    <b>File:</b> <input type=\"file\" name=\"image-file\" /><br />\n";
  echo "    <div class=\"manageGuidelines\">\n";
  echo "    <b>Guidelines:</b> <br />\n";
  echo "    <ul>\n";
  echo "      <li><b>Use a picture of you.</b> Not someone else, and not a picture of your dog, or favorite actor, or whatnot.</li>\n";
  echo "      <li><b>School appropriate.</b> Duh.</li>\n";
  echo "      <li><b>Proper file type.</b> The program will only accept JPEG, PNG, or GIF files currently.</li>\n";
  echo "      <li><b>Must be larger than 300x400 pixels.</b> Anything wider than that is fine, but the image needs to at least be large enough to reach " .
	"the minimum.</li>\n";
  echo "    </ul>\n";
  /*if ($has_staff_picture)
    echo "    <span style=\"font-size:80%;color:red;\">* If you want to use the picture you currently have, leave the file empty and click " .
	  "'Use Current Picture' below</span><br />\n";*/
  echo "    </div>\n";
  echo "    <center>\n";
  echo "      <input type=\"submit\" class=\"largeButton green\" value=\"Upload\" name=\"upload\" />\n";
  //if ($has_staff_picture)
  //  echo "      <input type=\"submit\" class=\"largeButton blue\" value=\"Use Current Picture\" name=\"reuse\" />\n";
  echo "    </center>\n";
  echo "  </form>\n";
  if ($has_staff_picture)
  {
    echo "  <form method=\"post\">\n";
    echo "    <div class=\"manageSubheader\">Delete?</div>\n";
	echo "    To remove your current staff picture and set it to the silhouette image, click the button below.<br />\n";
	echo "    <span style=\"font-size:80%;color:red;\">* This cannot be undone.</span><br />\n";
	echo "    <center>\n";
	echo "      <input type=\"submit\" class=\"largeButton red\" value=\"Delete\" name=\"delete\" />\n";
	echo "    </center>\n";
    echo "  </form>\n";
  }
}
  
outputManageFooter();

function uploadImage()
{
  if ($_FILES["image-file"]["error"] != 0)
  {
    switch ($_FILES["image-file"]["error"])
	{
	  case UPLOAD_ERR_INI_SIZE:
	  case UPLOAD_ERR_FORM_SIZE: return array("STATUS" => false, "MESSAGE" => "The file that you attempted to upload had a filesize that was too large.", 
		"IMAGE_FILE" => "");
	  case UPLOAD_ERR_PARTIAL: return array("STATUS" => false, "MESSAGE" => "The file upload process was interrupted.", "IMAGE_FILE" => "");
	  case UPLOAD_ERR_NO_FILE: return array("STATUS" => false, "MESSAGE" => "No file uploaded.", "IMAGE_FILE" => "");
	  default: return array("STATUS" => false, "MESSAGE" => "Encountered an unhandled exception when attempting to upload (Upload error #" .
		$_FILES["image-file"]["error"] . ").", "IMAGE_FILE" => "");
	}
  }
  if (!is_uploaded_file($_FILES["image-file"]["tmp_name"]))
    return array("STATUS" => false, "MESSAGE" => "Selected file was not an uploaded file.", "IMAGE_FILE" => "");
   
  $image_size = getimagesize($_FILES["image-file"]["tmp_name"]);
  if ($image_size === false)
    return array("STATUS" => false, "MESSAGE" => "Uploaded file is not an image in the proper format.", "IMAGE_FILE" => "");
	
  //if ($image_size[0] < 300 || $image_size[1] < 400)
  //  return array("STATUS" => false, "MESSAGE" => "Uploaded image must be 300x400 pixels or larger.", "IMAGE_FILE" => "");
  
  $image_type = exif_imagetype($_FILES["image-file"]["tmp_name"]);
  if ($image_type != IMAGETYPE_GIF && $image_type != IMAGETYPE_JPEG && $image_type != IMAGETYPE_PNG)
    return array("STATUS" => false, "MESSAGE" => "Uploaded image file is not a file of an allowed type (JPEG, PNG, or GIF only).", "IMAGE_FILE" => "");

  switch ($image_type)
  {
    case IMAGETYPE_GIF: $imagetype = "gif"; break;
	case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
	case IMAGETYPE_PNG: $imagetype = "png"; break;
  }
  $tmp_name = preg_split("(\\\\|/)", $_FILES["image-file"]["tmp_name"]);
  $tmp_name = $tmp_name[sizeof($tmp_name) - 1] . "." . $imagetype;
  
  if (!move_uploaded_file($_FILES["image-file"]["tmp_name"], DOCUMENT_ROOT . "/manage/temp/" . $tmp_name))
    return array("STATUS" => false, "MESSAGE" => "Could not move uploaded image to the temporary directory.", "IMAGE_FILE" => "");
  return array("STATUS" => true, "MESSAGE" => "", "IMAGE_FILE" => $tmp_name);
}
?>