<?php
require_once ("../config/manage.inc.php");
processManageSession();
if ($_SESSION[MANAGE_PERMISSIONS_SESSION][9] !== "1")
  header("Location: " . WEB_PATH . "/manage/?permission=denied");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_GET["staff_id"]) || !ctype_digit($_GET["staff_id"]))
  header("Location: " . WEB_PATH . "/manage/admin_landing.php?from=admin-staff-info&error=no-passed-staff-id");
if ($database->querySingle("SELECT count(*) FROM staff WHERE identity='" . $database->escapeString($_GET["staff_id"]) . "'") == 0)
  header("Location: " . WEB_PATH . "/manage/admin_landing.php?from=admin-staff-info&error=invalid-staff-id");
$staff_identity = $database->escapeString($_GET["staff_id"]);

if (isset($_POST["update-submit"]))
{
  $first_name = $database->escapeString($_POST["first-name"]);
  $last_name = $database->escapeString($_POST["last-name"]);
  $positions = $database->escapeString($_POST["staff-positions"]);
  $is_active = (isset($_POST["is-active"]) && $_POST["is-active"] == "on");
  $display_on_staff_listing = (isset($_POST["display-on-staff-listing"]) && $_POST["display-on-staff-listing"] == "on");
  $update_success = $database->exec("UPDATE staff SET first_name='" . $first_name . "', last_name='" . $last_name . "',positions='" .
	$positions . "',active='" . ($is_active ? "TRUE" : "FALSE") . "', display_on_staff_listing='" . ($display_on_staff_listing ?
	"TRUE" : "FALSE") . "' WHERE identity='" . $staff_identity . "'");
}
$staff_info = $database->querySingle("SELECT first_name, last_name, positions, password, active, display_on_staff_listing FROM " .
	"staff WHERE identity='" . $staff_identity . "' LIMIT 1", true);

outputManageHeader("[Admin] Edit Staff Information");
if (isset($update_success))
{
  if ($update_success)
    echo "<div class=\"success\"><b>Success.</b> Staff information updated</div>\n";
  else
    echo "<div class=\"error\"><b>Error.</b> Staff information could not be updated</div>\n";
}

  echo "  <form method=\"post\" action=\"" . WEB_PATH . "/manage/admin-staff-info.php?staff_id=" . $staff_identity . "\">\n";
  echo "      <input type=\"hidden\" name=\"staff-identity\" id=\"staff-identity\" value=\"" . $staff_identity . "\" />\n";
  echo "      <span class=\"manageLabel\">Staff Identity:</span> " . $staff_identity . "<br />\n";
  echo "      <span class=\"manageLabel\">First Name:</span> <input type=\"text\" name=\"first-name\" value=\"" . $staff_info["first_name"] . "\" /><br />\n";
  echo "      <span class=\"manageLabel\">Last Name:</span> <input type=\"text\" name=\"last-name\" value=\"" . $staff_info["last_name"] . "\" /><br /><br />\n";
  echo "      <span class=\"manageLabel\">Current Staff Position(s):</span><br />\n";
  echo "      <textarea name=\"staff-positions\" style=\"width:100%;\">" . $staff_info["positions"] . "</textarea><br /><br />\n";
  echo "      <span class=\"manageLabel\">Can log in:</span> <input type=\"checkbox\" name=\"is-active\"" . ($staff_info["active"] == "TRUE" ? " checked=\"checked\"" : "") .
	" /> <small><font color=\"red\">(* Determines if active. If unchecked, will display all articles by this staff member, but staff member will not be able to " .
	"log in, and will not display on staff listing, regardless of the value below)</font></small><br />\n";
  echo "      <span class=\"manageLabel\">Display on staff listing:</span> <input type=\"checkbox\" name=\"display-on-staff-listing\"" . ($staff_info["display_on_staff_listing"] == "TRUE" ?
	"checked=\"checked\"" : "") . " /> <small><font color=\"red\">(* If staff member is not active, will not display on staff listing regardless of this value)</font></small><br />\n";

  echo "  <center>\n";
  echo "    <div class=\"divider\"></div>\n";
  echo "    <input type=\"submit\" class=\"largeButton green\" name=\"update-submit\" value=\"Update\" /><br />\n";
  echo "  </center>\n";
echo "  </form>\n";
outputManageFooter();
?>