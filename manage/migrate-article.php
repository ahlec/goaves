<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
if (isset($_POST["migrate-submit"]))
{
/*echo "<pre>";
print_r($_POST);
echo "</pre>";
exit();*/
  require_once(DOCUMENT_ROOT . "/manage/scripts/migrate-article.php");
  //if (isset($article))
  //  $article_image_file = $article["image_file"];
  $article = getPostMigration($database);
  //$article["image_file"] = $article_image_file;
  //unset ($article_image_file);
  $post_results = migrateArticle($database, $article);
  if (is_array($post_results))
    $article["image_file"] = $post_results["IMAGE_FILE"];
  if (is_array($post_results) && !isset($article["identity"]))
    header("Location: " . WEB_PATH . "/manage/manage-articles.php?redirect=true&from=migrate-article&code=04");
  echo "<pre>";
  print_r($post_results);
  echo "</pre>";
  exit();
}
if (isset($_POST["beat-source"]))
{
  if (mb_strlen($_POST["beat-source"]) > 0)
    define("PARSE_MIGRATION", true);
  else
    define("PARSE_MIGRATION", false);
}
if (defined("PARSE_MIGRATION") && PARSE_MIGRATION)
{
  require_once(DOCUMENT_ROOT . "/manage/scripts/post-article.php");
  require_once(DOCUMENT_ROOT . "/manage/processes/generate-handle.php");
  require_once(DOCUMENT_ROOT . "/manage/processes/check-title-valid.php");
  if (!isset($database))
    $database = new DeitloffDatabase(DATABASE_PATH);
  
  $old_beat_source = stripslashes(html_entity_decode(utf8_encode(rawurldecode($_POST["beat-source"])))); //stripslashes(html_entity_decode(utf8_decode(rawurldecode($_POST["beat-source"]))));
  //$old_beat_source = utf8_decode($_POST["beat-source"]);  
//echo "<pre>";
//print_r(utf8_encode($_POST["beat-source"]));
//exit("</pre>");
  // Get (for the most part) the body
  $body = preg_replace("/(.*)<div id=\"container\">/sU","", $old_beat_source);

  // Get Information
  preg_match("/<span class=\"Headline\">(.*)<\\/span>/sU", $body, $title_pieces);
  $title = $title_pieces[1];

  preg_match("/<span class=\"ByLine\">(.*)<\\/span>/sU", $body, $by_line);
  $by_line = $by_line[1];
  preg_match("/Posted:(.*) by/sU", $body, $post_time);
  $post_time_str = $post_time[1];
  $post_time = strtotime($post_time_str);
  preg_match("/<LINK>(.*)<\\/a>/sU", preg_replace("/<a href=\"(.*)\">/sU", "<LINK>", $by_line), $author_pieces);
  $author = $author_pieces[1];

  preg_match("/<!--ImageBegin-->(.*)<!--ImageEnd-->/sU", $body, $image_section);
  $image_section = $image_section[1];
  preg_match("/<div class=\"Photo Float([^\">]*)\">/s", $image_section, $image_alignment);
  $image_alignment = $image_alignment[1];
  preg_match("/<img src=\"([^\"]*)\" alt=\"([^\"]*)\"/s", $image_section, $image_url_and_credit);
  $image_url = $image_url_and_credit[1];
  $image_url = str_replace("http://www.goaves.org/", "http://deitloff.com/goaves-archive/", $image_url);
  $photo_credit = $image_url_and_credit[2];
  preg_match("/<span class=\"Caption\">(.*)<\\/span>/sU", $image_section, $image_caption);
  $image_caption = $image_caption[1];
  preg_match("/<div class=\"Article\">(.*)<span class=\"ByLine\">/sU", $body, $article);
  $article = utf8_decode($article[1]);//prepareString($article[1]);

  $parsing_errors = array();
  $parsed = array();
  $parsed["title"] = $database->escapeString(prepareString($title));
  if (check_title_valid("article", $parsed["title"], 8) != "valid")
    $parsing_errors[] = check_title_valid("article", $parsed["title"], 8);
  $parsed["handle"] = $database->escapeString(generate_handle($parsed["title"]));
  $parsed["contents"] = $database->escapeString($article); //$database->escapeString(prepareString($article));
  $parsed["post_time"] = date("Y-m-d H:i:s", $post_time);
  $parsed["post_type"] = 1;
  $author_name_pieces = explode(" ", $author);
  if ($database->querySingle("SELECT count(*) FROM staff WHERE first_name LIKE '" . $database->escapeString($author_name_pieces[0]) . "' AND last_name LIKE '" .
	$database->escapeString($author_name_pieces[1]) . "'") > 0)
    $parsed["staff_identity"] = $database->querySingle("SELECT identity FROM staff WHERE first_name LIKE '" . $database->escapeString($author_name_pieces[0]) .
	"' AND last_name LIKE '" . $database->escapeString($author_name_pieces[1]) . "' LIMIT 1");
  else
    $parsing_errors[] = "Unable to locate staff in database [First name: '" . $author_name_pieces[0] . "', Last name: '" . $author_name_pieces[1] . "']";
  $parsed["publish"] = true;
  //$parsed["image_file"] = NULL;
  $parsed["image_alignment"] = (strtolower($image_alignment) == "left" ? "Left" : "Right");
  $parsed["image_caption"] = $database->escapeString(prepareString($image_caption, true));
  $parsed["is_updating"] = false;
  $parsed["photo_credit"] = $photo_credit;

  // Image
function genRandomString() {
    $length = 12;
    $characters = "0123456789abcdefghijklmnopqrstuvwxyz";
    $string = "";    

    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters))];
    }

    return $string;
}

  function save_image($inPath,$outPath) 
  { //Download images from remote server 
    $in=    fopen($inPath, "rb"); 
    $out=   fopen($outPath, "wb"); 
    while ($chunk = fread($in,8192)) 
    { 
        fwrite($out, $chunk, 8192); 
    } 
    fclose($in); 
    fclose($out); 
  } 
  /*$image_path = "temp/" . genRandomString() . ".jpg";
  save_image($image_url, $image_path);
  if (file_exists($image_path) && filesize($image_path) > 0)
  {
    require_once(DOCUMENT_ROOT . "/config/functions-images.inc.php");
    $cropped_image = new ProcessedImage();
    $cropped_image->loadOriginal($image_path);
    $cropped_image->scale(300, 900, false);
    $new_filename = $cropped_image->save(DOCUMENT_ROOT . "/manage/temp/", $parsed["handle"]);
    $cropped_image->close();
    $cropped_image->deleteOriginalImage();
    $parsed["image_file"] = $new_filename;
  } else
    $parsing_errors[] = "Unable to save article image. Manual upload may be necessary (when the option becomes available).";*/

  outputManageHeader("Migrate Article - Confirm Before Posting");
  foreach ($parsing_errors as $error)
    echo "<div class=\"error\"><b>Error.</b> " . $error . "</div>\n";

  echo "  <form method=\"post\" enctype=\"multipart/form-data\" onSubmit=\"return validateArticleMigration();\">\n";
  echo "      <script> var web_path = '" . WEB_PATH . "'; </script>\n";
  echo "      <span class=\"manageLabel alternate\" title=\"The article title is the unique name for this article.\">Title:</span><br />\n";
  echo "      <input type=\"text\" name=\"migrate-title\" class=\"largeTextbox " . (check_title_valid("article", $parsed["title"], 8) == "valid" ? "valid" : "invalid") .
	"\" " . "value=\"" . str_replace("\"", "&#34;", preg_replace("/\"+/","\"", preg_replace("/'+/","'",
		stripslashes(utf8_decode($parsed["title"]))))) .
	"\" id=\"migrate-title\" onKeyUp=\"checkTitleValid('migrate-title', 'article-title-validation-feedback','article', this.value, 8); " .
	"generateHandle('generated-article-handle', this.value);\" autocomplete=\"off\" />\n";
  echo "      <div class=\"validationErrorBox\" id=\"article-title-validation-feedback\"></div>\n";
  echo "      <div class=\"manageGuidelines\"><b>Guidelines:</b><br />\n";
  echo "        <ul>\n";
  echo "          <li>Must contain (at minimum) eight characters.</li>\n";
  echo "          <li>Must be unique; no other article may have the same title.</li>\n";
  echo "        </ul>\n";
  echo "      </div>\n";
  echo "      <span class=\"manageLabel alternate\" title=\"The article handle is the alphanumeric code that is unique to " .
	"each article. It is used by the program to determine which article should be loaded.\">Handle:</span> <span id=\"" .
	"generated-article-handle\" />" . $parsed["handle"] . "</span><br /><br />\n";

  echo "    <span class=\"manageLabel alternate\" title=\"The time and date when the article was posted.\">Post Date/Time:</span> <input type=\"text\" value=\"" . $parsed["post_time"] . "\" name=\"migrate-time\" /><br />\n";
  echo "    <span class=\"manageLabel alternate\" title=\"The author of this article.\">Author:</span> <select name=\"migrate-author\">\n";
  $get_all_staff = $database->query("SELECT identity, first_name, last_name FROM staff ORDER BY last_name, first_name ASC");
  while ($staff_member = $get_all_staff->fetchArray())
    echo "<option value=\"" . $staff_member["identity"] . "\"" . ($staff_member["identity"] == $parsed["staff_identity"] ? " selected=\"selected\"" : "") . ">" . $staff_member["first_name"] . " " . $staff_member["last_name"] .
	"</option>\n";
  echo "</select><br /><br />\n";

  echo "    <span class=\"manageLabel alternate\" title=\"The article itself.\">Article:</span><div class=\"manageLabelPadding\"></div>\n";
  echo "    <script type=\"text/javascript\" src=\"" . WEB_PATH . "/layout/tiny_mce/tiny_mce.js\"></script>\n";
 
  echo "    <script type=\"text/javascript\">\n";
  echo "      tinyMCE.init({\n";
  echo "        mode : \"exact\",\n"; //textareas\",\n";
  echo "        elements : \"article-contents\",\n";
  echo "        entity_encoding : \"named\",\n";
  echo "        theme : \"advanced\",\n";
  echo "        skin : \"o2k7\",\n";
//  echo "        skin_variant : \"silver\",\n";
  echo "        editor_selector : \"advanced-textarea\",\n";
  echo "        plugins : \"safari,spellchecker,pagebreak,style,layer,save,advhr,advimage,advlink," .
	"iespell,inlinepopups,preview,media,searchreplace,print,contextmenu,paste,directionality," .
	"fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template\",\n";
  echo "        theme_advanced_buttons1 : \"cut,copy,paste,pastetext,|,search,replace,|,undo,redo,|,cleanup,code,|," .
	"preview,|,fullscreen,removeformat,visualaid\",\n";
  echo "        theme_advanced_buttons2 : \"bold,italic,underline,strikethrough,|,forecolor,link,unlink,|,sub,sup,|,charmap," .
	"iespell,|,bullist,numlist,|,outdent,indent\",\n";
  echo "        theme_advanced_buttons3 : \"styleprops," .
	"spellchecker,|,cite,abbr,acronym,del,ins,attribs,\",\n";
  echo "        theme_advanced_toolbar_location : \"top\",\n";
  echo "        theme_advanced_toolbar_align : \"left\",\n";
  echo "        theme_advanced_statusbar_location : \"bottom\",\n";
  echo "        theme_advanced_resizing : false,\n";
  //echo "        content_css : \"" . WEB_PATH . "/layout/stylesheet-main.css\",\n";
  echo "        template_external_list_url : \"js/template_list.js\",\n";
  echo "        external_link_list_url : \"js/link_list.js\",\n";
  //echo "        force_br_newlines : true,\n";
  //echo "        force_p_newlines : false,\n";
  echo "        remove_trailing_nbsp : true,\n";
  echo "        height : '360',\n";
  //echo "        forced_root_block : \"\",\n";
  //echo "        paste_retain_style_properties : \"\",\n";
  echo "        spellchecker_rpc_url : \"" . WEB_PATH . "/layout/tiny_mce/plugins/spellchecker/rpc.php\"\n";
  echo "      });\n";
  echo "      var web_path = '" . WEB_PATH . "';\n";
  echo "      tinyMCE.init({ mode: \"exact\", elements : \"image-caption\", theme : \"simple\", skin : \"o2k7\", " .
	"remove_trailing_nbsp : true});\n";
  echo "    </script>\n";
  echo "    <textarea name=\"article-contents\" id=\"article-contents\" class=\"advanced-textarea normal\">" . 
	$parsed["contents"] . "</textarea><br />\n";

  echo "    <span class=\"manageLabel alternate\" title=\"A club or sports group that this article is about.\">Related Group:</span><div class=\"manageLabelPadding\"></div>\n";
  $get_groups = $database->query("SELECT group_identity, title, type FROM groups ORDER BY title ASC");
  echo "    <select name=\"related-group\">\n";
  echo "      <option value=\"0\">(NONE)</option>\n";
  while ($group = $get_groups->fetchArray())
    echo "      <option value=\"" . $group["group_identity"] . "\">" . $group["title"] . " (" . $group["type"] . ")</option>\n";
  echo "    </select><br /><br />\n";

  echo "    <div class=\"manageSubheader\">Image</div>\n";
  /*if ($parsed["image_file"] !== null)
  {
    echo "<center><img src=\"" . WEB_PATH . "/manage/temp/" . $parsed["image_file"] . "\" /></center>\n";
  } else
    echo "<center>No image could be migrated.</center>\n";*/
  echo "    <b>Image File:</b> <input type=\"file\" name=\"image-file\" id=\"image-file\" onChange=\"" .
	"executeAJAX('" . WEB_PATH . "/manage/processes/get-file-extension.php?file=' + this.value, function validate(value) { if (value != 'png' && value != 'jpg' && value != 'gif') { alert('" .
	"Invalid image file. Must be a PNG, GIF, or JPG file.'); " .
	"} });\" /><span class=\"redNotice\">" .
	"* If you do not want to upload an image with this article, simply leave this empty.</span><br />\n";
  echo "      <span class=\"manageLabel\">Image Alignment:</span> " .
	"<select name=\"migrate-image-alignment\" id=\"migrate-image-alignment\"><option value=\"right\"" . ($parsed["image_alignment"] == "Right" ? 
	" selected=\"selected\"" : "") . ">Right</option><option value=\"left\"" . ($parsed["image_alignment"] == "Left" ? " selected=\"selected\"" :
	"") . ">Left</option></select><br />\n";
  echo "      <b>Image Caption:</b>\n";
  echo "      <textarea name=\"migrate-image-caption\" id=\"image-caption\" class=\"simple-textarea normal\">" . 
	$parsed["image_caption"] . "</textarea>\n";
  echo "      <span class=\"manageLabel\">Photo Credit:</span> " .
	"<input type=\"text\" name=\"migrate-photo-credit\" value=\"" . $parsed["photo_credit"] .
	"\" /><small><font color=\"red\"> ( *Leave empty to not display a photo credit under the article)</font></small><br />\n";
    echo "  <center>\n";

  echo "    <div class=\"divider\"></div>\n";
  echo "    <input type=\"submit\" class=\"largeButton green\" name=\"migrate-submit\" value=\"Migrate\" /><br />\n";
  echo "  </center>\n";
echo "  </form>\n";

  outputManageFooter();
  exit();
}
outputManageHeader("Migrate Article");
if(defined("PARSE_MIGRATION") && PARSE_MIGRATION == false)
  echo "<div class=\"error\"><b>Error.</b> Article was not parsed (either submitted empty or of an invalid format)</div>";
echo "<div class=\"notice\"><b>Notice -- UPDATE.</b> Articles now migrate, but the archives are still being uploaded, and posting migrated articles is not yet finished.</div>\n";
echo "<div class=\"notice\"><b>Notice.</b> The source of the question marks has been identified. We're trying to remedy it. - Jacob</div>\n"; // They're in this file, in going UTF-8 from this form to the original processing
echo "<center><a href=\"http://deitloff.com/goaves-archive/9002/\" target=\"_blank\">Click here to go to archived site (find your article on THIS site only)</a></center><br />\n";
echo "<form method=\"POST\" action=\"" . WEB_PATH . "/manage/migrate-article.php\" enctype=\"multipart/form-data\" accept-charset=\"UTF-8\" " .
	"onSubmit=\"document.getElementById('beat-source').value = encodeURIComponent(document.getElementById('beat-source').value); return true;\">\n";
echo "<textarea id=\"beat-source\" name=\"beat-source\" rows=\"20\" style=\"width:100%;\"></textarea>\n";
echo "<input type=\"submit\" name=\"migrate\" value=\"Begin Migration\" />\n";
echo "</form>\n";
outputManageFooter();
?>