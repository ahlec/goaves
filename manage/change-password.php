<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("Change Password");
if (isset($_GET["must_change"]) && $_GET["must_change"] == "true")
  echo "<div class=\"newChange\"><b>New Change.</b> Passwords must be changed.<br />As we prepare to take the website live, " .
	"using the default passwords provides a serious security risk. To fix (or at least mitigate) this issue, until your " .
	"password has been changed from 'default', you will be unable to do any actions on the management panel.Thanks.<br />" .
	"- Jacob</div>\n";
if (isset($_POST["change"]))
{
  $old_password = md5($_POST["old-password"]);
  $new_password = md5($_POST["new-password"]);
  $confirm_new_password = md5($_POST["confirm-new-password"]);
  if (mb_strlen($_POST["new-password"]) < 5 || mb_strlen($_POST["confirm-new-password"]) < 5)
    echo "<div class=\"error\"><b>Error.</b> New password must be at minimum five characters long.</div>\n";
  else
  {
    if ($database->querySingle("SELECT count(*) FROM staff WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "' AND password='" .
	  $old_password . "' OR password=''") == 1)
    {
      if ($new_password == $confirm_new_password)
	  {
	    $could_change = $database->query("UPDATE staff SET password='" . $new_password . "' WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "'");
	    if (!$could_change)
	      echo "<div class=\"error\"><b>Error.</b> Unable to change passwords.</div>\n";
	    else
	      echo "<div class=\"success\"><b>Success.</b> Password was changed.</div>\n";
	  } else
	    echo "<div class=\"error\"><b>Error.</b> The new password you entered does not confirm (two different entries).</div>\n";
    } else
      echo "<div class=\"error\"><b>Error.</b> The password you have entered is not your current password.</div>\n";  
  }
}
echo "<form method=\"POST\" action=\"" . WEB_PATH . "/manage/change-password.php\">\n";
echo "  <b>Old Password:</b>\n";
echo "  <input type=\"password\" class=\"largeTextbox\" autocomplete=\"off\" name=\"old-password\" /><br /><br />\n";
echo "  <b>New Password:</b> <span style=\"font-size:80%;color:red;\">* Must be at minimum five (5) characters.</span>\n";
echo "  <input type=\"password\" class=\"largeTextbox\" autocomplete=\"off\" name=\"new-password\" /><br /><br />\n";
echo "  <b>Confirm New Password:</b>\n";
echo "  <input type=\"password\" class=\"largeTextbox\" autocomplete=\"off\" name=\"confirm-new-password\" /><br /><br />\n";
echo "  <center><input type=\"submit\" class=\"largeButton green\" name=\"change\" value=\"Change\" /></center>\n";
echo "</form>\n";
outputManageFooter();
?>
