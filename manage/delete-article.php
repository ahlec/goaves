<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_GET["article_id"]) || !ctype_digit($_GET["article_id"]))
  header("Location: " . WEB_PATH . "/manage/index.php?redirect=yes&from=delete-article&code=01");
$article = $database->escapeString($_GET["article_id"]);
if ($database->querySingle("SELECT count(*) FROM beats WHERE beat_identity='" . $article . "' AND staff_identity='" . $_SESSION[MANAGE_SESSION] . "'") == 0)
  header("Location: " . WEB_PATH . "/manage/index.php?redirect=yes&from=delete-article&code=02");
if (isset($_POST["delete-yes"]) || isset($_POST["delete-no"]))
{
  $article_was_posted = ($database->querySingle("SELECT count(*) FROM beats WHERE published='TRUE' AND beat_identity='" . $article . "'") != 0);
  $image = $database->querySingle("SELECT image_file FROM beats WHERE beat_identity='" . $article . "'");
  $delete_code = "03";
  if (isset($_POST["delete-yes"]))
  {
    $delete_code = ($database->exec("DELETE FROM beats WHERE beat_identity='" . $article . "' AND staff_identity='" . $_SESSION[MANAGE_SESSION] . "'") ? 
		"04" : "03");
	if (mb_strlen($image) > 0)
	  unlink (DOCUMENT_ROOT . "/images/articles/" . $image);
  }
  $database->exec("UPDATE statistics SET value=(SELECT post_time FROM beats WHERE published='TRUE' ORDER BY " .
	"post_time DESC LIMIT 1) WHERE stat_handle='content_last_updated'");
  header("Location: " . WEB_PATH . "/manage/manage-articles.php?code=" . $delete_code);
}
outputManageHeader("Delete Article");

$article_title = $database->querySingle("SELECT title FROM beats WHERE beat_identity='" . $article . "' LIMIT 1");

echo "<center>Delete<br /><b>" . $article_title . "</b><br /><br />\n";

echo "<form method=\"post\" action=\"" . WEB_PATH . "/manage/delete-article.php?article_id=" . $article . "\">\n";
echo "  <input type=\"submit\" name=\"delete-yes\" class=\"largeButton red\" value=\"Yes\" />\n";
echo "  <input type=\"submit\" name=\"delete-no\" class=\"largeButton green\" value=\"No\" />\n";
echo "</form>\n";
echo "<br /></center>\n";

outputManageFooter();
?>