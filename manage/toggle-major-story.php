<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_GET["article_id"]) || !ctype_digit($_GET["article_id"]))
  header("Location: " . WEB_PATH . "/manage/major-stories.php?redirect=yes&from=toggle-major-story&code=01");
$article = $database->escapeString($_GET["article_id"]);
if ($database->querySingle("SELECT count(*) FROM beats WHERE beat_identity='" . $article . "' AND staff_identity='" . $_SESSION[MANAGE_SESSION] . "'") == 0)
  header("Location: " . WEB_PATH . "/manage/major-stories.php?redirect=yes&from=toggle-major-story&code=02");

$is_major_story = ($database->querySingle("SELECT count(*) FROM features WHERE feature_type='article' AND item_identity='" . 
	$article . "'") == 1);
$article_title = $database->querySingle("SELECT title FROM beats WHERE beat_identity='" . $article . "' LIMIT 1");

if ($is_major_story)
{
  if (isset($_POST["demote-yes"]) || isset($_POST["demote-no"]))
  {
    if (isset($_POST["demote-yes"]))
    {
      $image_file = $database->querySingle("SELECT header_image FROM features WHERE feature_type='article' AND item_identity='" . $article . "'");
      if ($image_file !== "")
        if (!@unlink(DOCUMENT_ROOT . "/images/features/" . $image_file))
        {
          header("Location: " . WEB_PATH. "/manage/major-stories.php?redirect=yes&from=toggle-major-story&code=05");
          exit();
        }
      if ($database->exec("DELETE FROM features WHERE feature_type='article' AND item_identity='" . $article . "'"))
      {
        $database->exec("UPDATE statistics SET value=(SELECT last_updated FROM beats WHERE published='TRUE' ORDER BY " .
	   "last_updated DESC LIMIT 1) WHERE stat_handle='content_last_updated'");
        header("Location: " . WEB_PATH . "/manage/major-stories.php?redirect=yes&from=toggle-major-story&code=03&demoted=" . $article);
      } else
        header("Location: " . WEB_PATH . "/manage/major-stories.php?redirect=yes&from=toggle-major-story&code=04");
    } else
      header("Location: " . WEB_PATH . "/manage/major-stories.php");
    exit();
  }
  outputManageHeader("Demote Major Story?");
  echo "<center>Remove major story status from<br /><b>" . $article_title . "</b>?<br /><br />\n";
  echo "<form method=\"post\" action=\"" . WEB_PATH . "/manage/toggle-major-story.php?article_id=" . $article . "\">\n";
  echo "  <input type=\"submit\" name=\"demote-yes\" class=\"largeButton red\" value=\"Yes\" />\n";
  echo "  <input type=\"submit\" name=\"demote-no\" class=\"largeButton green\" value=\"No\" />\n";
  echo "</form>\n";
  echo "<br /></center>\n";
  exit();
} else
{
  if (isset ($_GET["step"]) && $_GET["step"] == 1)
  {
    if (isset($_POST["promote-no"]))
    {
      header("Location: " . WEB_PATH . "/manage/major-stories.php");
      exit();
    }
    if (!isset($_SESSION["make-major-story"]))
      $upload_status = uploadImage();
    if (!isset($_SESSION["make-major-story"]) && !$upload_status["STATUS"])
    {
      header("Location: " . WEB_PATH . "/manage/major-stories.php?redirect=yes&from=toggle-major-story&code=06&msg=" . $upload_status["MESSAGE"]);
      exit();
    }
    if (!isset($_SESSION["make-major-story"]))
      $_SESSION["make-major-story"] = array("IMAGE" => $upload_status["IMAGE_FILE"], "ARTICLE" => $article);
    
    outputManageHeader("Make Major Story?");
    echo "    <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"" . WEB_PATH . "/layout/Jcrop/css/jquery.Jcrop.css\"/>\n";
    echo "    <script type=\"text/javascript\" src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.min.js\"> </script>\n";
    echo "    <script type=\"text/javascript\" src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.Jcrop.js\"> </script>\n";
    echo "    <script type=\"text/javascript\">\n";
    echo "      document.onselectstart = function() { return false; }\n";
    echo "      document.onmousedown = function() { return false; }\n";
    echo "      jQuery(function(){\n";
    echo "        jQuery('#major-story-image').Jcrop({\n";
    echo "          aspectRatio: (7/3),\n";
    echo "          bgOpacity: 0.4,\n";
    echo "          onSelect: updateCoords,\n";
    echo "          onChange: updateCoords\n";
    echo "        });\n";
    echo "      });\n";
    echo "      var cropbox_x = null;\n";
    echo "      var cropbox_y = null;\n";
    echo "      var cropbox_width = null;\n";
    echo "      var cropbox_height = null;\n";
    $image_size = getimagesize(DOCUMENT_ROOT . "/manage/temp/" . $_SESSION["make-major-story"]["IMAGE"]);
    echo "      var image_ratio = " . ($image_size[0] / ($image_size[0] > 800 ? 800 : $image_size[0])) . ";\n";
    echo "      function updateCoords(cropbox)\n";
    echo "      {\n";
    echo "        cropbox_x = cropbox.x * image_ratio;\n";
    echo "        cropbox_y = cropbox.y * image_ratio;\n";
    echo "        cropbox_width = cropbox.w * image_ratio;\n";
    echo "        cropbox_height = cropbox.h * image_ratio;\n";
    echo "      }\n";
    echo "      function saveImage()\n";
    echo "      {\n";
    echo "        if (cropbox_x == null || cropbox_y == null || cropbox_width == null || cropbox_height == null)\n";
    echo "        {\n";
    echo "          alert(\"You must select a part of the image to use as the header before being able to promote the story.\");\n";
    echo "          return;\n";
    echo "        }\n";
    echo "        if ((cropbox_width * image_ratio < 700 || cropbox_height * image_ratio < 300) && " .
	"!confirm(\"The selection you have chosen is less than 700x300pixels, and will have to scale up, potentially " .
	" resulting in pixelation. Are you SURE this is what you wish to do?\"))\n";
    echo "          return;\n";
    echo "        if (!confirm(\"Are you satisfied with the image selection?\"))\n";
    echo "          return;\n";
    echo "        executeAJAX('" . WEB_PATH . "/manage/scripts/crop-major-story-header.php?x=' + cropbox_x + '&y=' + cropbox_y + '&width=' + cropbox_width + '&height=' + " .
	"cropbox_height, function evaluate_results(value) {\n";
    echo "          if (value == 'success')\n";
    echo "            window.location = '" . WEB_PATH . "/manage/major-stories.php?redirect=yes&from=toggle-major-story&code=07';\n";
    echo "          else\n";
    echo "            alert (value);\n";
    echo "        });\n";
    echo "      }\n";
    echo "      function cancelUpload()\n";
    echo "      {\n";
    echo "        executeAJAX('" . WEB_PATH . "/manage/scripts/cancel-major-story-upload.php', function evaluate_results(value) {\n";
    echo "          if (value == 'success')\n";
    echo "            window.location = '" . WEB_PATH . "/manage/major-stories.php?redirect=yes&from=toggle-major-story&code=08';\n";
    echo "          else\n";
    echo "            alert (value);\n";
    echo "        });\n";
    echo "      }\n";
    echo "    </script>\n";
    echo "  To begin cropping, click anywhere on your picture and drag. A box will appear, and using the handles (small gray boxes) on the corners and sides of the box, resize " .
	"the image and move it (click and drag) until what you have selected what you wish the major story header to be. When you've finished, click the <b>" .
	"<font color=\"green\">Save</font></b> button below.<br /><br />\n";
    echo "  <center><div style=\"width:" . ($image_size[0] > 800 ? 800 : $image_size[0]) . "px; border:1px solid black;\"><img src=\"" .
	WEB_PATH . "/manage/temp/" . $_SESSION["make-major-story"]["IMAGE"] . "\" style=\"width:" . ($image_size[0] > 800 ? 800 : $image_size[0]) .
	"px;\" id=\"major-story-image\" /></div></center><br />\n";
    echo "  <center><input type=\"button\" class=\"largeButton green\" value=\"Save\" onClick=\"saveImage();\" /> ";
    echo "<input type=\"button\" class=\"largeButton red\" value=\"Cancel\" onClick=\"cancelUpload();\" />" .
		"</center><br />\n";
    exit();
  } else
  {
    if (isset ($_SESSION["make-major-story"]))
      unset ($_SESSION["make-major-story"]);
    outputManageHeader("Make Major Story?");
    echo "<br />\n";
    echo "<form method=\"post\" enctype=\"multipart/form-data\" action=\"" . WEB_PATH . "/manage/toggle-major-story.php?article_id=" . $article . "&step=1\">\n";
    echo "<div class=\"manageSubheader\">Upload Header Image</div>\n";
    $image_file = $database->querySingle("SELECT image_file FROM beats WHERE beat_identity='" . $article . "' LIMIT 1");
    echo "    <center><img src=\"" . WEB_PATH . "/images/articles/" . $image_file . "\" style=\"border:1px solid black; padding:1px; max-width:300px; width:300px;\"><br />" . 
	"<small>(Article picture)</small></center><br />\n";
    echo "    <b>File:</b> <input type=\"file\" name=\"image-file\" /><br />\n";
    echo "    <div style=\"font-size:80%;margin-bottom:4px;border-bottom:1px solid #ccc;padding-bottom:4px;\">\n";
    echo "    <b>Guidelines:</b> <br />\n";
    echo "    <ul>\n";
    echo "      <li><b>Must use the same image as in the article.</b> You must use the same image here that you used for the article you're attempting to link to. If you don't, we'll " .
	"find you. So don't.</li>\n";
    echo "      <li><b>School appropriate.</b> Duh.</li>\n";
    echo "      <li><b>Proper file type.</b> The program will only accept JPEG, PNG, or GIF files currently.</li>\n";
    echo "      <li><b>Must be larger than 700x300 pixels.</b> Anything wider than that is fine, but the image needs to at least be large enough to reach " .
	"the minimum.</li>\n";
    echo "    </ul>\n";
    echo "  </div>\n"; 
    echo "<br />\n";
    echo "<div class=\"manageSubheader\">Confirm</div>\n";
    echo "<center>Are you sure you wish to make a major story out of<br /><b>" . $article_title . "</b>?<br /><br />\n";
  echo "  <input type=\"submit\" name=\"demote-yes\" class=\"largeButton green\" value=\"Yes\" />\n";
  echo "  <input type=\"submit\" name=\"demote-no\" class=\"largeButton red\" value=\"No\" />\n";
    echo "</form>\n";
    echo "<br /></center>\n";
    exit();
  }
}

function uploadImage()
{
  if ($_FILES["image-file"]["error"] != 0)
  {
    switch ($_FILES["image-file"]["error"])
	{
	  case UPLOAD_ERR_INI_SIZE:
	  case UPLOAD_ERR_FORM_SIZE: return array("STATUS" => false, "MESSAGE" => "The file that you attempted to upload had a filesize that was too large.", 
		"IMAGE_FILE" => "");
	  case UPLOAD_ERR_PARTIAL: return array("STATUS" => false, "MESSAGE" => "The file upload process was interrupted.", "IMAGE_FILE" => "");
	  case UPLOAD_ERR_NO_FILE: return array("STATUS" => false, "MESSAGE" => "No file uploaded.", "IMAGE_FILE" => "");
	  default: return array("STATUS" => false, "MESSAGE" => "Encountered an unhandled exception when attempting to upload (Upload error #" .
		$_FILES["image-file"]["error"] . ").", "IMAGE_FILE" => "");
	}
  }
  if (!is_uploaded_file($_FILES["image-file"]["tmp_name"]))
    return array("STATUS" => false, "MESSAGE" => "Selected file was not an uploaded file.", "IMAGE_FILE" => "");
   
  $image_size = getimagesize($_FILES["image-file"]["tmp_name"]);
  if ($image_size === false)
    return array("STATUS" => false, "MESSAGE" => "Uploaded file is not an image in the proper format.", "IMAGE_FILE" => "");
	
  //if ($image_size[0] < 700 || $image_size[1] < 300)
  //  return array("STATUS" => false, "MESSAGE" => "Uploaded image must be 700x300 pixels or larger.", "IMAGE_FILE" => "");
  
  $image_type = exif_imagetype($_FILES["image-file"]["tmp_name"]);
  if ($image_type != IMAGETYPE_GIF && $image_type != IMAGETYPE_JPEG && $image_type != IMAGETYPE_PNG)
    return array("STATUS" => false, "MESSAGE" => "Uploaded image file is not a file of an allowed type (JPEG, PNG, or GIF only).", "IMAGE_FILE" => "");

  switch ($image_type)
  {
    case IMAGETYPE_GIF: $imagetype = "gif"; break;
	case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
	case IMAGETYPE_PNG: $imagetype = "png"; break;
  }
  $tmp_name = preg_split("(\\\\|/)", $_FILES["image-file"]["tmp_name"]);
  $tmp_name = $tmp_name[sizeof($tmp_name) - 1] . "." . $imagetype;
  
  if (!move_uploaded_file($_FILES["image-file"]["tmp_name"], DOCUMENT_ROOT . "/manage/temp/" . $tmp_name))
    return array("STATUS" => false, "MESSAGE" => "Could not move uploaded image to the temporary directory.", "IMAGE_FILE" => "");
  return array("STATUS" => true, "MESSAGE" => "", "IMAGE_FILE" => $tmp_name);
}

?>