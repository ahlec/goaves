<?php
require_once ("../config/manage.inc.php");
processManageSession();
if ($_SESSION[MANAGE_PERMISSIONS_SESSION][9] !== "1")
  header("Location: " . WEB_PATH . "/manage/?permission=denied");
$database = new DeitloffDatabase(DATABASE_PATH);


if (isset($_GET["approve"]))
{
  $approval_identity = $database->escapeString($_GET["approve"]);
  if ($database->querySingle("SELECT count(*)FROM previous_staff_positions WHERE position_archive_identity='" . $approval_identity . "'") > 0)
  {
    if ($database->querySingle("SELECT count(*) FROM previous_staff_positions WHERE position_archive_identity='" . $approval_identity . "' AND approval_pending<>'FALSE'") > 0)
    {
      if ($database->exec("UPDATE previous_staff_positions SET approval_pending='FALSE' WHERE position_archive_identity='" . $approval_identity . "'"))
        echo "<div class=\"success\"><b>Success.</b> The staff position has been approved. Now visible on the website to all visitors.</div>\n";
      else
        echo "<div class=\"error\"><b>Error.</b> An error in the programming. Unable to approve staff position.</div>\n";
    }
  } else
    echo "<div class=\"error\"><b>Error.</b> The staff position that you selected does not exist.</div>\n";
}

outputManageHeader("[Admin] Approve Staff Positions");

$get_positions = $database->query("SELECT staff.first_name, staff.last_name, previous_staff_positions.staff_identity, start_year, end_year, previous_staff_positions.position, position_archive_identity FROM " .
	"previous_staff_positions JOIN staff ON staff.identity = previous_staff_positions.staff_identity WHERE approval_pending<>'FALSE'");
if ($get_positions->numberRows() > 0)
{
  echo "<table style=\"width:800px;border-collapse:collapse;margin:10px;\">\n";
  echo "  <tr>\n";
  echo "    <td style=\"border-bottom:1px solid black;\"><b>Staff Name</b></td>\n";
  echo "    <td style=\"border-bottom:1px solid black;\"><b>Start Year</b></td>\n";
  echo "    <td style=\"border-bottom:1px solid black;\"><b>End Year</b></td>\n";
  echo "    <td style=\"border-bottom:1px solid black;\"><b>Position</b></td>\n";
  echo "    <td style=\"border-bottom:1px solid black;\"></td>\n";
  echo "  </tr>\n";
  while($position = $get_positions->fetchArray())
  {
    echo "  <tr>\n";
    echo "    <td style=\"font-size:90%;\">" . $position["first_name"] . " " . $position["last_name"] . "</td>\n";
    echo "    <td style=\"font-size:90%;\">" . $position["start_year"] . "</td>\n";
    echo "    <td style=\"font-size:90%;\">" . $position["end_year"] . "</td>\n";
    echo "    <td style=\"font-size:90%;\">" . $position["position"] . "</td>\n";
    echo "    <td style=\"font-size:90%;\"><a href=\"" . WEB_PATH . "/manage/approve-staff-positions.php?approve=" . $position["position_archive_identity"] . "\">Approve</a></td>\n";
    echo "  </tr>\n";
  }
  echo "</table>\n";
  echo "<br />\n";
} else
  echo "<br /><center><b>There are no pending positions right now.</b></center><br />\n";
outputManageFooter();
?>