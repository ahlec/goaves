<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("Manage Photo Galleries");
if (isset($_SESSION["toggle-publish-results"]))
{
  if ($_SESSION["toggle-publish-results"]["CODE"] == "03")
    echo "<div class=\"error\"><b>Error.</b> The gallery you attempted to publish does not exist, or is not yet complete and cannot be published.</div>\n";
  else if (isset($_SESSION["toggle-publish-results"]["SUCCESS"]))
  {
    if ($_SESSION["toggle-publish-results"]["SUCCESS"])
      echo "<div class=\"success\"><b>Success.</b> Gallery '<b>" . $database->querySingle("SELECT title FROM photo_galleries WHERE gallery_identity='" . $database->escapeString($_SESSION["toggle-publish-results"]["IDENTITY"]) .
		"' LIMIT 1") . "</b>' has been " . $_SESSION["toggle-publish-results"]["NEW_TOGGLED_VALUE"] . "</div>\n";
  }
  unset ($_SESSION["toggle-publish-results"]); 
}
$photo_galleries = $database->query("SELECT gallery_identity, handle, title, date_last_updated, published, date_created, is_complete FROM photo_galleries ORDER BY date_last_updated DESC");
if ($photo_galleries->numberRows() == 0)
  echo "There are no photo galleries currently.<br /><br />\n";
else
{
  while ($gallery = $photo_galleries->fetchArray())
  {
    echo "<div class=\"articleContainer" . ($gallery["published"] == "TRUE" ? " containerPublished" : " containerDraft") .  "\">" . 
	  "<b>" . format_content($gallery["title"]) . "</b> <span style=\"font-size:80%;\">&ndash; (Created: " . date(DATE_FORMAT, strtotime($gallery["date_created"])) . ", Last Updated: " .
	  date(DATE_FORMAT, strtotime($gallery["date_last_updated"])) . ")</span><br />\n";
	if ($gallery["is_complete"] == "TRUE")
	{
	  echo "<a href=\"" . WEB_PATH . "/manage/toggle-publish.php?type=photo_gallery&identity=" . $gallery["gallery_identity"] . "\"><img src=\"" .
		WEB_PATH . "/layout/manage-icon-toggle-" . ($gallery["published"] == "TRUE" ? "unpublish" : "publish") . ".png\" border=\"0\" class=\"" .
		"draftIcon\" title=\"" . ($gallery["published"] == "TRUE" ? "Unpublish" : "Publish") . "\" /> " . ($gallery["published"] == "TRUE" ?
		"Unpublish" : "Publish") . "</a> ";
	} else
	  echo "<span class=\"manageNotComplete\"><img src=\"" . WEB_PATH . "/layout/manage-icon-toggle-publish-disabled.png\" class=\"draftIcon\" title=\"[Cannot Publish]\" /> [Cannot Publish - Gallery not complete]</span> ";
	echo "<a href=\"" . WEB_PATH . "/manage/upload-gallery-pictures.php?identity=" . $gallery["gallery_identity"] . "\"><img src=\"" . WEB_PATH . "/layout/manage-icon-upload-picture.png\" border=\"0\" class=\"" .
		"draftIcon\" title=\"Upload Pictures\" />Upload Pictures</a> ";
	echo "</div>\n";
  }
  echo "<br>\n";
}
outputManageFooter();
?>