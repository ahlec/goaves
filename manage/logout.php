<?php
require_once ("../config/manage.inc.php");
processManageSession("Logout");
if (isset($_SESSION[MANAGE_SESSION]))
{
  echo "<b>You are now logged out</b>\n";
  unset ($_SESSION[MANAGE_SESSION]);
  unset ($_SESSION[MANAGE_PERMISSIONS_SESSION]);
}
require_once ("login.php");
?>