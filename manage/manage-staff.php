<?php
error_reporting(E_ALL);
require_once ("../config/manage.inc.php");
processManageSession();
if ($_SESSION[MANAGE_PERMISSIONS_SESSION][9] !== "1")
  header("Location: " . WEB_PATH . "/manage/?permission=denied");
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("[Administration] Manage Staff");  

$staff_member = (isset($_GET["staff"]) && $database->querySingle("SELECT count(*) FROM staff WHERE identity='" .
	$database->escapeString($_GET["staff"]) . "'") > 0 ? $database->escapeString($_GET["staff"]) : null);

if ($staff_member !== null)
{
  if (isset ($_GET["page"]))
  {
    $page = $_GET["page"];
    if ($page == "name")
    {
    
    } else if ($page == "toggle_active")
    {

    } else if ($page == "positions")
    {

    } else if ($page == "reset_password")
    {

    } else if ($page == "reset_picture")
    {

    } else if ($page == "toggle_display")
    {

    }
  }
  echo "  <a href=\"" . WEB_PATH . "/manage/manage-staff.php\">[Return to staff list]</a><br />\n";
  $staff_information = $database->querySingle("SELECT first_name, last_name, active, positions, password, " .
	"image_file, icon_file, display_on_staff_listing FROM staff WHERE identity='" . $staff_member .
	"' LIMIT 1", true);
  echo "  <center><b>" . $staff_information["first_name"] . " " . $staff_information["last_name"] . 
	"</b></center><br />\n"; 
  echo "- <a href=\"" . WEB_PATH . "/manage/manage-staff.php?staff=" . $staff_member . "&page=name\">" .
	"Change first and/or last name</a><br />\n";
  echo "- <a href=\"" . WEB_PATH . "/manage/manage-staff.php?staff=" . $staff_member . "&page=permissions\">" .
	"Change staff permissions</a><br />\n";
  echo "- <a href=\"" . WEB_PATH . "/manage/manage-staff.php?staff=" . $staff_member . "&page=toggle_active\">" .
	"Make " . ($staff_information["active"] == "TRUE" ? "inactive" : "active") . "</a><br />\n";
  echo "- <a href=\"" . WEB_PATH . "/manage/manage-staff.php?staff=" . $staff_member . "&page=positions\">" .
	"Change current positions</a><br />\n";
  if ($staff_information["password"] !== md5("default"))
    echo "- <a href=\"" . WEB_PATH . "/manage/manage-staff.php?staff=" . $staff_member . "&page=reset_password\">" .
	"Reset password</a><br />\n";
  else
    echo "- (Password has not been set yet)<br />\n";
  echo "- <a href=\"" . WEB_PATH . "/manage/manage-staff.php?staff=" . $staff_member . "&page=reset_picture\">" .
	"Reset picture</a><br />\n";
  echo "- <a href=\"" . WEB_PATH . "/manage/manage-staff.php?staff=" . $staff_member . "&page=toggle_display\">" .
	($staff_information["display_on_staff_listing"] == "TRUE" ? "Hide from staff listing" :
	"Show on staff listing") . "</a><br />\n";
  exit();
} else
{
  echo "  <center>Select a staff member from the list below.</center>\n";
  $get_staff = $database->query("SELECT identity, first_name, last_name, active FROM staff ORDER BY active DESC, " .
	"last_name, first_name ASC");
  $previous_member_was_active = null;
  while ($staff = $get_staff->fetchArray())
  {
    if ($previous_member_was_active != $staff["active"])
    {
      $previous_member_was_active = $staff["active"];
      echo "  <div class=\"listHeader\">" . ($staff["active"] == "TRUE" ? "Active Staff" : "Inactive Staff") .
	"</div>\n";
    }
    echo "  <a href=\"" . WEB_PATH . "/manage/manage-staff.php?staff=" . $staff["identity"] . "\">" .
	"<div class=\"staffRow\">" . $staff["first_name"] . " " . $staff["last_name"] . "</div></a>\n";
  }
}

outputManageFooter();
?>