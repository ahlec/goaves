<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("Manage Comics");
$comics = $database->query("SELECT identity, image_file, title, date_posted, published FROM cartoons WHERE artist='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]) . "' ORDER BY date_posted DESC");
if ($comics->numberRows() == 0)
  echo "You have no comics.<br /><br />\n";
else
{
  while ($comic = $comics->fetchArray())
  {
    echo "<div class=\"articleContainer" . ($comic["published"] == "TRUE" ? " containerPublished" : " containerDraft") .  "\">" . 
	  "<b>" . format_content($comic["title"]) . "</b> <span style=\"font-size:80%;\">&ndash; (Posted: " . date(DATE_FORMAT, strtotime($comic["date_posted"])) . ")</span><br />\n";
	echo "<a href=\"" . WEB_PATH . "/manage/toggle-publish.php?type=cartoon&identity=" . $comic["identity"] . "\"><img src=\"" .
		WEB_PATH . "/layout/manage-icon-toggle-" . ($comic["published"] == "TRUE" ? "unpublish" : "publish") . ".png\" border=\"0\" class=\"" .
		"draftIcon\" title=\"" . ($comic["published"] == "TRUE" ? "Unpublish" : "Publish") . "\" /> " . ($comic["published"] == "TRUE" ?
		"Unpublish" : "Publish") . "</a> ";
	echo "</div>\n";
  }
  echo "<br>\n";
}
outputManageFooter();
?>