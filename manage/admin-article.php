<?php


// Remove table elements and other such elements from posting



require_once ("../config/manage.inc.php");
processManageSession();
if ($_SESSION[MANAGE_PERMISSIONS_SESSION][9] !== "1")
  header("Location: " . WEB_PATH . "/manage/?permission=denied");
$database = new DeitloffDatabase(DATABASE_PATH);
require_once (DOCUMENT_ROOT . "/manage/scripts/post-article.php");
if (isset($_GET["article_id"]) && ctype_digit($_GET["article_id"]))
  $article = getPostedArticle($database, $_GET["article_id"], true);
if (isset($article) && $article === false)
  header("Location: " . WEB_PATH . "/manage/index.php?redirect=true&from=post-article&code=01");
if (isset($_POST["article-title"]))
{
  if (isset($article))
    $article_image_file = $article["image_file"];
  $article = getPostArticle($database, true);
  $article["image_file"] = $article_image_file;
  unset ($article_image_file);
  $post_results = postArticle($database, $article, true);
  if (is_array($post_results))
    $article["image_file"] = $post_results["IMAGE_FILE"];
  if (!isset($article["identity"]))
    header("Location: " . WEB_PATH . "/manage/manage-articles.php?redirect=true&from=post-article&code=04");
}
outputManageHeader((isset($_GET["article_id"]) ? "[Admin] Edit Article" : "[Admin] Post Article"));
if (isset($_POST["article-title"]))
{
  if ($post_results["RESULT"] === true)    
  {
    if (sizeof($post_results["NOTICES"]) > 0)
	  foreach ($post_results["NOTICES"] as $notice)
	    echo "<div class=\"notice\"><b>Notice.</b> " . $notice . "</div>\n";
    echo "<div class=\"success\"><b>Article Saved.</b> You can review and publish it from <a href=\"" . WEB_PATH . "/manage/manage-articles.php\">" .
		"your articles listing</a>.</div>\n";
	if (!isset($_GET["article_id"]))
	  unset ($article);
  }
  else
    echo "<div class=\"error\"><b>Error.</b> " . $post_results . "</div>\n";
}
  echo "  <form method=\"post\" enctype=\"multipart/form-data\">\n";
  if (isset ($article))
    echo "      <input type=\"hidden\" name=\"article-identity\" id=\"article-identity\" value=\"" . $article["identity"] . "\" />\n";
  if (isset ($article["publish"]) && $article["publish"])
    echo "      <input type=\"hidden\" name=\"article-published\" id=\"article-published\" value=\"published\" />\n";
  echo "      <br /><div class=\"manageSubheader\">Article</div>\n";
  echo "      <span class=\"manageLabel alternate\" title=\"The article title is the unique name for this article.\">Title:</span><br />\n";
  echo "      <input type=\"text\" name=\"article-title\" class=\"largeTextbox " . (isset($article["title"]) && $article["is_updating"] ? 
	"validating" : "invalid") . "\" " . (isset ($article["title"]) ? "value=\"" . str_replace("\"", "&#34;", preg_replace("/\"+/","\"", preg_replace("/'+/","'",
		stripslashes(utf8_decode($article["title"]))))) .
	"\" " : "") . "id=\"article-title\" onKeyUp=\"checkArticleTitle(); generateArticleHandle();\" autocomplete=\"off\" />\n";
  echo "      <div class=\"manageGuidelines\"><b>Guidelines:</b><br />\n";
  echo "        <ul>\n";
  echo "          <li>Must contain (at minimum) eight characters.</li>\n";
  echo "          <li>Must be unique; no other article may have the same title.</li>\n";
  echo "        </ul>\n";
  echo "      </div>\n";
  echo "      <span class=\"manageLabel alternate\" title=\"The article handle is the alphanumeric code that is unique to " .
	"each article. It is used by the program to determine which article should be loaded.\">Handle:</span> <span id=\"" .
	"generated-article-handle\" />" . (isset($article["handle"]) ? $article["handle"] : "") . "</span><br /><br />\n";
  echo "      <span class=\"manageLabel\">Author:</span> <select name=\"change-author\">\n";
  $get_staff = $database->query("SELECT identity, first_name, last_name, active FROM staff ORDER BY last_name, first_name ASC");
  while ($staff = $get_staff->fetchArray())
    echo "        <option value=\"" . $staff["identity"] . "\"" . ($staff["identity"] == $article["staff_identity"] ? " selected=\"selected\"" : "") . ">" . $staff["first_name"] . " " . $staff["last_name"] . " (" . ($staff["active"] == "TRUE" ? "Active" : "Inactive") . ")</option>\n";
  echo "      </select><br /><br />\n";
  echo "      <span class=\"manageLabel\">Original Post Time/Date:</span> <input type=\"text\" name=\"change-post-time\" value=\"" . $article["exec_time"] . "\" /><br /><br />\n";
  echo "      <span class=\"manageLabel alternate\" title=\"The category that entails the article's contents\">Beat Category:</span> " .
	"<small>(Select one)</small><div class=\"manageLabelPadding\"></div>\n";
  $get_beat_types = $database->query("SELECT type_identity, title, icon_file FROM beat_types WHERE LIKE(\"%\" || type_identity || \"|%\"," .
	"(SELECT valid_article_types FROM staff WHERE identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "' LIMIT 1)) == 1 " .
	"ORDER BY type_identity ASC");
  $is_first_type = true;
  while ($beat_type = $get_beat_types->fetchArray())
  {
    echo "      <input type=\"radio\" class=\"boxRadioButtons\" name=\"article-type\" value=\"" . $beat_type["type_identity"] . "\"";
    if ((!isset($article) && $is_first_type) || (isset($article) && $article["post_type"] == $beat_type["type_identity"]))
      echo " checked=\"checked\"";
	echo " id=\"type-" . $beat_type["type_identity"] . "\">";
	echo "<label class=\"boxRadioButtons";
	if ((!isset($article) && $is_first_type) || (isset($article) && $article["post_type"] == $beat_type["type_identity"]))
      echo " boxRadioChecked";
	echo "\" id=\"label-type-" . $beat_type["type_identity"] . "\" for=\"type-" . $beat_type["type_identity"] . 
		"\" onClick=\"toggleBoxRadioButton('type-" . $beat_type["type_identity"] . "');\">";
	echo "<img src=\"" . WEB_PATH . "/layout/" . $beat_type["icon_file"] . "\" /> " . ($beat_type["type_identity"] == "1" ? "Main" : $beat_type["title"]) . "</label>\n";
    $is_first_type = false;
  }
 
  echo "    <br />\n";
  echo "    <span class=\"manageLabel alternate\" title=\"The article itself.\">Article:</span><div class=\"manageLabelPadding\"></div>\n";
  echo "    <script type=\"text/javascript\" src=\"" . WEB_PATH . "/layout/tiny_mce/tiny_mce.js\"></script>\n";
 
  echo "    <script type=\"text/javascript\">\n";
  echo "      tinyMCE.init({\n";
  echo "        mode : \"exact\",\n"; //textareas\",\n";
  echo "        elements : \"article-contents\",\n";
  echo "        entity_encoding : \"named\",\n";
  echo "        theme : \"advanced\",\n";
  echo "        skin : \"o2k7\",\n";
//  echo "        skin_variant : \"silver\",\n";
  echo "        editor_selector : \"advanced-textarea\",\n";
  echo "        plugins : \"safari,spellchecker,pagebreak,style,layer,save,advhr,advimage,advlink," .
	"iespell,inlinepopups,preview,media,searchreplace,print,contextmenu,paste,directionality," .
	"fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template\",\n";
  echo "        theme_advanced_buttons1 : \"cut,copy,paste,pastetext,|,search,replace,|,undo,redo,|,cleanup,code,|," .
	"preview,|,fullscreen,removeformat,visualaid\",\n";
  echo "        theme_advanced_buttons2 : \"bold,italic,underline,strikethrough,|,forecolor,link,unlink,|,sub,sup,|,charmap," .
	"iespell,|,bullist,numlist,|,outdent,indent\",\n";
  echo "        theme_advanced_buttons3 : \"styleprops," .
	"spellchecker,|,cite,abbr,acronym,del,ins,attribs,\",\n";
  echo "        theme_advanced_toolbar_location : \"top\",\n";
  echo "        theme_advanced_toolbar_align : \"left\",\n";
  echo "        theme_advanced_statusbar_location : \"bottom\",\n";
  echo "        theme_advanced_resizing : false,\n";
  //echo "        content_css : \"" . WEB_PATH . "/layout/stylesheet-main.css\",\n";
  echo "        template_external_list_url : \"js/template_list.js\",\n";
  echo "        external_link_list_url : \"js/link_list.js\",\n";
  //echo "        force_br_newlines : true,\n";
  //echo "        force_p_newlines : false,\n";
  echo "        remove_trailing_nbsp : true,\n";
  echo "        height : '360',\n";
  //echo "        forced_root_block : \"\",\n";
  //echo "        paste_retain_style_properties : \"\",\n";
  echo "        spellchecker_rpc_url : \"" . WEB_PATH . "/layout/tiny_mce/plugins/spellchecker/rpc.php\"\n";
  echo "      });\n";
  echo "      var web_path = '" . WEB_PATH . "';\n";
  echo "      tinyMCE.init({ mode: \"exact\", elements : \"image-caption\", theme : \"simple\", skin : \"o2k7\", " .
	"remove_trailing_nbsp : true});\n";
  echo "    </script>\n";
  echo "    <textarea name=\"article-contents\" id=\"article-contents\" class=\"advanced-textarea normal\">" . 
	(isset ($article) ? utf8_decode($article["contents"]) : "") . "</textarea><br />\n";

  echo "    <span class=\"manageLabel alternate\" title=\"A club or sports group that this article is about.\">Related Group:</span><div class=\"manageLabelPadding\"></div>\n";
  $get_groups = $database->query("SELECT group_identity, title, type FROM groups ORDER BY title ASC");
  echo "    <select name=\"related-group\">\n";
  echo "      <option value=\"0\">(NONE)</option>\n";
  while ($group = $get_groups->fetchArray())
    echo "      <option value=\"" . $group["group_identity"] . "\"" . (isset($article) && $article["related_group"] == $group["group_identity"] ? " selected=\"selected\"" : "") .
	">" . $group["title"] . " (" . $group["type"] . ")</option>\n";
  echo "    </select><br /><br />\n";

  echo "    <span class=\"manageLabel alternate\" title=\"A major event or topic that this article is related to.\">Topic:</span><div class=\"manageLabelPadding\"></div>\n";
  $get_topics = $database->query("SELECT topic_identity, title FROM topics WHERE (date_start <='" . date("Y-m-d", (isset($article) ? $article["original_post_time"] : strtotime("+1 hours"))) .
	"' OR date_start IS null OR date_start = '') AND (date_end >='" . date("Y-m-d", (isset($article) ? $article["original_post_time"] : strtotime("+1 hours"))) . "' OR date_end IS null " .
	"OR date_end = '') ORDER BY title ASC");
  echo "    <select name=\"topic\">\n";
  echo "      <option value=\"0\">(NONE)</option>\n";
  while ($topic = $get_topics->fetchArray())
    echo "      <option value=\"" . $topic["topic_identity"] . "\"" . (isset($article) && $article["topic_identity"] == $topic["topic_identity"] ? " selected=\"selected\"" : "") .
	">" . $topic["title"] . "</option>\n";
  echo "    </select><br /><br />\n";
  
  echo "    <div class=\"manageSubheader\">Image</div>\n";
  /*echo "<input type=\"hidden\" name=\"upload-image\" id=\"upload-image\" value=\"" . 
	(isset($article["image_file"]) && $article["image_file"] != "" ? "yes" : "no") . "\" />\n";
  echo "<center><span id=\"toggle-upload-value\" style=\"font-size:80%;\">Currently " .
	(isset($article["image_file"]) && $article["image_file"] != "" ? "" : "not ") . "uploading an image</span>\n";
  echo "<input type=\"button\" class=\"mediumButton green\" value=\"Upload\" onClick=\"toggleUploadImage();\" id=\"toggle-upload-image\" /></center>";
  echo "<div class=\"manageLabelPadding\"></div>\n";
  echo "    <div class=\"postArticleImageContainer\" id=\"postArticleImageContainer\"" .
	(isset($article["image_file"]) && $article["image_file"] != "" ? "" : "style=\"display:none;\"") . ">";*/
  if (isset($article["image_file"]) && $article["image_file"] != "")
  {
    echo "      <span class=\"manageLabel alternate\" title=\"The image that is currently uploaded for this article\">Current Image:</span>\n";
	echo "      <div class=\"manageLabelPadding\"></div>\n";
    echo "      <center>";
	echo "<img src=\"" . WEB_PATH . "/images/articles/" . $article["image_file"] . "\" class=\"picture\" style=\"margin-bottom:3px;\" /><br />";
	//echo "</center>\n";
	echo "      <input type=\"submit\" class=\"mediumButton red\" name=\"delete-image\" value=\"Delete Image\" /></center> <div style=\"text-align:" .
		"right;\"><span class=\"redNotice\">" .
		"* Deleting the article image will also save all changes you have made thus far.</span></div><br />\n";
  }
  echo "      <span class=\"manageLabel\">Image File:</span> <input type=\"file\" name=\"image-file\" id=\"image-file\" onChange=\"" .
	"executeAJAX('" . WEB_PATH . "/manage/processes/get-file-extension.php?file=' + this.value, function validate(value) { if (value != 'png' && value != 'jpg' && value != 'gif') { alert('" .
	"Invalid image file. Must be a PNG, GIF, or JPG file.'); " .
	"} });\" /><span class=\"redNotice\">" .
	"* If you do not want to upload an image with this article, simply leave this empty.</span><br />\n";
  echo "      <span class=\"manageLabel\">Image Alignment:</span> " .
	"<select name=\"image-alignment\" id=\"image-alignment\"><option value=\"right\"" . (isset($article) && $article["image_alignment"] == "Right" ? 
	" selected=\"selected\"" : "") . ">Right</option><option value=\"left\"" . (isset($article) && $article["image_alignment"] == "Left" ? " selected=\"selected\"" :
	"") . ">Left</option></select><br />\n";
  echo "      <b>Image Caption:</b>\n";
  echo "      <textarea name=\"image-caption\" id=\"image-caption\" class=\"simple-textarea normal\">" . 
	(isset ($article["image_caption"]) ? $article["image_caption"] : "") . "</textarea>\n";
  echo "      <span class=\"manageLabel\">Photo Credit:</span> " .
	"<input type=\"text\" name=\"photo-credit\" value=\"" . (isset ($article) ? $article["photo_credit"] :
	"") . "\" /><small><font color=\"red\"> ( *Leave empty to not display a photo credit under the article)</font></small><br />\n";
  //echo "    </div>\n";

  echo "  <center>\n";
  echo "    <div class=\"divider\"></div>\n";
  echo "    <input type=\"submit\" class=\"largeButton green\" name=\"save-submit\" value=\"Save\" /><br />\n";
  if (!isset($article))
    echo "    <span style=\"font-size:75%;text-align:center;color:red;\">(Does Not Publish)</span>\n";
  echo "  </center>\n";
echo "  </form>\n";
outputManageFooter();
?>