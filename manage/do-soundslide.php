<?php
error_reporting(E_ALL);
require_once ("../config/manage.inc.php");
processManageSession();
if ($_SESSION[MANAGE_PERMISSIONS_SESSION][10] !== "1")
  header("Location: " . WEB_PATH . "/manage/?permission=denied");
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset($_GET["gallery_id"]))
{
  $gallery_identity = $database->escapeString($_GET["gallery_id"]);
  if ($database->querySingle("SELECT count(*) FROM photo_galleries WHERE gallery_identity='" . $gallery_identity . "'") > 0)
    $gallery_information = $database->querySingle("SELECT gallery_identity, handle, title, related_group FROM photo_galleries WHERE gallery_identity='" . $gallery_identity . "' LIMIT 1", true);
}

if (isset($_POST["submit-slideshow"]))
{
  require_once (DOCUMENT_ROOT . "/manage/processes/string-management.inc.php");
  require_once (DOCUMENT_ROOT . "/manage/processes/check-title-valid.php");
  require_once (DOCUMENT_ROOT . "/manage/processes/generate-handle.php");
  require_once (DOCUMENT_ROOT . "/manage/processes/get-file-extension.php");
  $title = prepareString($_POST["slideshow-title"]);
  $handle = $database->escapeString(generate_handle($title));
  $title = $database->escapeString($title);
  $related_group = $_POST["related-group"];
  echo "<pre>";
  print_r($_FILES);
  echo "</pre>";
  if (check_title_valid("sound_slide", $title, 6) == "valid")
  {
    if ($database->querySingle("SELECT count(*) FROM sound_slides WHERE handle LIKE '" . $handle . "'") == 0)
    {
	  // Audio file
	  $audio_uploaded = false;
	  if (isset($_FILES["slideshow-audio"]) && $_FILES["slideshow-audio"]["error"] == "0")
	  {
	    if (getFileExtension($_FILES["slideshow-audio"]["name"]) == "mp3")
		{
		  if (move_uploaded_file($_FILES["slideshow-audio"]["tmp_name"], DOCUMENT_ROOT . "/components/soundslides_audio/" . $handle . ".mp3"))
		  {
		    $audio_uploaded = true;
			$audio_file = $handle . ".mp3";
		  } else
		    $audio_uploaded = "Uploaded file could not be moved.";
		} else
		  $audio_uploaded = "Uploaded file was not an MP3 file.";
	  } else
	  {
	    switch ($_FILES["slideshow-audio"]["error"])
		{
		  case "4": $audio_uploaded = "No audio file uploaded."; break;
		  default: $audio_uploaded = "Audio file is too large to process (" . $_FILES["slideshow-audio"]["error"] . ")."; break;
		}
	  }
	  
	  if ($audio_uploaded === true)
	  {
        $could_create_slideshow = $database->exec("INSERT INTO sound_slides(handle, title, published, post_date, related_group, sound_file, staff_identity, date_last_updated) VALUES('" .
		  $handle . "','" . $title . "','FALSE','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "','" . $related_group . "','" . $audio_file . "','" .
		  $database->escapeString($_SESSION[MANAGE_SESSION]) . "','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "')");
        if ($could_create_slideshow)
        {
          $created_slideshow_identity = $database->querySingle("SELECT slideshow_identity FROM sound_slides WHERE handle LIKE '" . $handle . "' LIMIT 1");
          $database->exec("INSERT INTO multimedia_feed(media_type, media_identity, post_time, post_type, posting_staff_member) VALUES('sound_slides','" . $created_slideshow_identity .
		    "','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "','created','" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "')");
          header("Location: " . WEB_PATH . "/manage/manage-soundslides.php?redirect=true&from=do-soundslides&success=created");
          exit();
        } else
          $error = "Could not create the sound slideshow in the database (Program error)";
	  } else
	    $error = $audio_uploaded;
    } else
      $error = "A sound slideshow with this handle already exists";
  } else
    $error = check_title_valid("sound_slide", $title, 6);
}
outputManageHeader((isset($slideshow_information) ? "Edit Sound Slideshow" : "Create Sound Slideshow"), false, "enableDoSoundslides()");
if (isset($error))
  echo "<div class=\"error\"><b>Error.</b> " . $error . "</div>\n";
  
  
  echo "  <script>\n";
  echo "    var web_path = '" . WEB_PATH . "';\n";
  echo "  </script>\n";
  echo "  <form method=\"post\" enctype=\"multipart/form-data\" action=\"" . WEB_PATH . "/manage/do-soundslide.php" . (isset($slideshow_information) ? "?slideshow_id=" .
	$slideshow_information["slideshow_identity"] : "") . "\" >\n";
  echo "      <input type=\"text\" name=\"slideshow-title\" class=\"largeTextbox " . (!isset($slideshow_information) ? "in" : "") . "valid\" id=\"slideshow-title\"" .
	(isset($slideshow_information) ?
	" value=\"" . $slideshow_information["title"] . "\"" : "") . " onKeyUp=\"checkTitleValid('slideshow-title', 'slideshow-title-validation-feedback','sound_slide', this.value, 6" .
	(isset($slideshow_information) ? 
	"," . $slideshow_information["slideshow_identity"] : "") . "); generateHandle('slideshow-handle', this.value);\" autocomplete=\"off\" />\n";
  echo "      <div class=\"validationErrorBox\" id=\"slideshow-title-validation-feedback\"></div>\n";
  echo "      <div class=\"manageGuidelines\"><b>Guidelines:</b><br />\n";
  echo "        <ul>\n";
  echo "          <li>Must contain (at minimum) six characters.</li>\n";
  echo "          <li>Must be unique; no other sound slideshow may have the same title.</li>\n";
  echo "        </ul>\n";
  echo "      </div>\n";
  echo "    <b>Handle:</b> '<span id=\"slideshow-handle\">" . (isset($slideshow_information) ? $slideshow_information["handle"] : "") .
	"</span>' <small>(Automatically generates as you type)</small><br /><br />\n";
  echo "    <span class=\"manageLabel alternate\" title=\"The MP3 file containing the audio for the sound slideshow.\">Sound file:</span> <input type=\"file\" name=\"slideshow-audio\" " .
	"onChange=\"executeAJAX('" . WEB_PATH . "/manage/processes/get-file-extension.php?file=' + this.value, function validate(value) { " .
	"if (value != 'mp3') { alert('Invalid audio file. Must be an MP3 file.'); document.getElementById('submit-slideshow').disabled = true; } else {
	document.getElementById('submit-slideshow').disabled = false; } });\" />\n<br /><br />\n";
  echo "    <span class=\"manageLabel alternate\" title=\"A club or sports group that this slideshow is related to.\">Related Group:</span> \n";
  $get_groups = $database->query("SELECT group_identity, title, type FROM groups ORDER BY title ASC");
  echo "    <select name=\"related-group\" id=\"slideshow-related-group\">\n";
  echo "      <option value=\"0\">(NONE)</option>\n";
  while ($group = $get_groups->fetchArray())
    echo "      <option value=\"" . $group["group_identity"] . "\"" . (isset($slideshow_information) && $slideshow_information["related_group"] == $group["group_identity"] ? " selected=\"selected\"" : "") .
	">" . $group["title"] . " (" . $group["type"] . ")</option>\n";
  echo "    </select><br /><br />\n";
  echo "    <center>\n";
  echo "      <input type=\"submit\" class=\"largeButton green\" value=\"" . (isset($slideshow_information) ? "Edit" : "Create") . "\" name=\"submit-slideshow\" " .
	"id=\"submit-slideshow\" disabled=\"disabled\" />\n";
  echo "    </center>\n";
  echo "  </form>\n";
  
outputManageFooter();
?>