<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("Manage Staff Links");

if (isset($_POST["link-type"]))
{
  switch ($_POST["link-type"])
  {
    case "facebook": $link_type = "FACEBOOK"; $url_prepend = "http://www.facebook.com/"; break;
    case "twitter": $link_type = "TWITTER"; $url_prepend = "http://www.twitter.com/"; break;
    case "youtube": $link_type = "YOUTUBE"; $url_prepend = "http://www.youtube.com/user/"; break;
    case "myspace": $link_type = "MYSPACE"; $url_prepend = "http://www.myspace.com/"; break;
    case "stumbleupon": $link_type = "STUMBLE_UPON"; $url_prepend = "http://www.stumbleupon.com/stumbler/"; break;
    case "deviantart": $link_type = "DEVIANT_ART"; $url_prepend = ""; break;
  }
  if (isset($link_type))
  {
    $link_url = $database->escapeString($_POST["link-url"]);
    if (mb_strlen($link_url) > 0)
    {
      if ($database->querySingle("SELECT count(*) FROM staff_links WHERE link_type='" . $link_type . "' AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "'") == 0)
      {
        $latest_sort_order_position = $database->querySingle("SELECT sort_order FROM staff_links WHERE staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "' ORDER BY sort_order DESC LIMIT 1");
        if (!ctype_digit($latest_sort_order_position))
          $latest_sort_order_position = 0;
        $could_add = $database->exec("INSERT INTO staff_links(staff_identity, url, link_type, sort_order) VALUES('" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "','" .
		$link_url . "','" . $link_type . "','" . ($latest_sort_order_position + 1) . "')");
        if (!$could_add)
          $post_error = "Could not add the link (Programming error).";
        else
          $post_success = true;
      } else
        $post_error = "Already have a link of this type (you cannot add two or more of the same type of link).";
   } else
     $post_error = "URL is not valid for this type of link.";
  } else
    $post_error = "Type of link is incorrect.";
  $link_url = $_POST["link-url"];
}
if (isset($_GET["delete"]) && $_GET["delete"] == "true")
{
  switch ($_GET["type"])
  {
    case "facebook": $link_type = "FACEBOOK"; break;
    case "twitter": $link_type = "TWITTER"; break;
    case "youtube": $link_type = "YOUTUBE"; break;
    case "myspace": $link_type = "MYSPACE"; break;
    case "stumbleupon": $link_type = "STUMBLE_UPON"; break;
    case "deviantart": $link_type = "DEVIANT_ART"; break;
  }
  if (isset($link_type))
  {
    if ($database->querySingle("SELECT count(*) FROM staff_links WHERE link_type='" . $link_type . "' AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "'") > 0)
    {
      if ($database->exec("DELETE FROM staff_links WHERE link_type='" . $link_type . "' AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "'"))
        echo "<div class=\"success\"><b>Success.</b> Your link has been removed.</div>\n";
      else
        echo "<div class=\"error\"><b>Error.</b> Your link could not be removed (programming error).</div>\n";
    } else
      echo "<div class=\"error\"><b>Error.</b> You do not have a link of this type to delete.</div>\n";
  } else
    echo "<div class=\"error\"><b>Error.</b> Link type was not valid, so could not delete link.</div>\n";
}

if (isset($post_success))
  echo "<div class=\"success\"><b>Success.</b> Your link has been added to your portfolio.</div>\n";
if (isset($post_error))
  echo "<div class=\"error\"><b>Error.</b> " . $post_error . "</div>\n";

echo "<div class=\"manageSubheader\">Create Link</div>\n";
echo "<form method=\"POST\" action=\"" . WEB_PATH . "/manage/change-staff-links.php\" style=\"margin:0px;\" onSubmit=\"return confirm('Are you sure you wish to add this link to your profile? The link will " .
	"be visible to anyone who visits your portfolio, and if the link you provide here has content on it that you wouldn\'t wish colleges, employers, or other such people to see, you might want to " .
	"reconsider adding your link.');\">\n";
echo "<b>Link Style:</b> <select name=\"link-type\"><option value=\"facebook\">Facebook</option><option value=\"twitter\">Twitter</option><option value=\"youtube\">YouTube account</option>" .
	"<option value=\"myspace\">MySpace</option><option value=\"stumbleupon\">Stumble Upon</option><option value=\"deviantart\">Deviant Art</option></select><br />\n";
echo "<b>URL:</b> <input type=\"text\" name=\"link-url\" size=\"50\" /><br />\n";
echo "<input type=\"submit\" value=\"Add\">\n";
echo "</form>\n";

echo "<br />\n";
echo "<div class=\"manageSubheader\">Existing Links</div>\n";
$existing_staff_links = $database->query("SELECT link_identity, url, link_type, sort_order FROM staff_links WHERE staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) .
	"' ORDER BY sort_order ASC");
if ($existing_staff_links->numberRows() == 0)
  echo "You have no staff links.";
else
{
  while ($link = $existing_staff_links->fetchArray())
  {
    echo "<div class=\"manageContainer\">\n";
    echo "  <img src=\"" . WEB_PATH . "/images/icons/";
    switch ($link["link_type"])
    {
       case "FACEBOOK": echo "icon-fcbk.png"; break;
	case "TWITTER": echo "icon-twitter.png"; break;
	case "YOUTUBE": echo "icon-youtube.png"; break;
	case "MYSPACE": echo "icon-myspace.png"; break;
	case "STUMBLE_UPON": echo "icon-stumbleupon.png"; break;
	case "DEVIANT_ART": echo "icon-deviantart.png"; break;
    }
    echo "\" class=\"icon\" />\n";
    echo "  <div class=\"title\">";
    switch ($link["link_type"])
    {
      case "FACEBOOK": echo "Facebook"; break;
      case "TWITTER": echo "Twitter"; break;
      case "MYSPACE": echo "Myspace"; break;
      case "DEVIANT_ART": echo "Deviant Art"; break;
      case "YOUTUBE": echo "YouTube"; break;
      case "STUMBLE_UPON": echo "Stumble Upon"; break;

    }
    echo "</div>\n";
    echo "  <div class=\"subtitle\"><a href=\"" . $link["url"] . "\">" . $link["url"] . "</a></div>\n";
    switch ($link["link_type"])
    {
      case "FACEBOOK": $url_handle = "facebook"; break;
      case "TWITTER": $url_handle = "twitter"; break;
      case "MYSPACE": $url_handle = "myspace"; break;
      case "YOUTUBE": $url_handle = "youtube"; break;
      case "STUMBLE_UPON": $url_handle = "stumbleupon"; break;
      case "DEVIANT_ART": $url_handle = "deviantart"; break;
    }
    echo "  <div class=\"actions\"><a href=\"" . WEB_PATH . "/manage/change-staff-links.php?delete=true&type=" . $url_handle . "\">Delete</a></div>\n";
    echo "</div>\n";
  }
}

echo "<br />\n";
outputManageFooter();
?>