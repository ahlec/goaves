<?php
error_reporting(E_ALL);
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("Change Group Picture");
if (isset($_POST["upload"]) || isset($_FILES["image-file"]))
{
  if (!isset($_SESSION["upload-group-picture"]))
    $attempt_upload = uploadImage();
  if (isset($_SESSION["upload-group-picture"]) || $attempt_upload["STATUS"])
  {
    $_SESSION["upload-group-picture"] = array("IMAGE" => $attempt_upload["IMAGE_FILE"],
	"IDENTITY" => $database->escapeString($_POST["group"]));
    $get_group = $database->querySingle("SELECT handle, image_file, icon_file FROM groups WHERE group_identity='" .
	$database->escapeString($_SESSION["upload-group-picture"]["IDENTITY"]) . "' LIMIT 1", true);
    $database->exec("UPDATE groups SET image_file='', icon_file='' WHERE group_identity='" . $database->escapeString($_SESSION["upload-group-picture"]["IDENTITY"]) . "'");
    if ($get_group["image_file"] !== "")
      unlink(DOCUMENT_ROOT . "/images/groups/" . $get_group["image_file"]);
    require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
    $group_image = new ProcessedImage();
    $group_image->loadOriginal(DOCUMENT_ROOT . "/manage/temp/" . $attempt_upload["IMAGE_FILE"]);
    $group_image->scale(650, 500, false);
    $image_filename = $group_image->save(DOCUMENT_ROOT . "/images/groups/", $get_group["handle"]);
    $group_image->close();
    $database->exec("UPDATE groups SET image_file='" . $database->escapeString($image_filename) .
	"' WHERE group_identity='" . $database->escapeString($_SESSION["upload-group-picture"]["IDENTITY"]) . "'");
    if ($get_group["icon_file"] !== "")
      unlink(DOCUMENT_ROOT . "/images/groups/" . $get_group["icon_file"]);

    echo "    <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"" . WEB_PATH . "/layout/Jcrop/css/jquery.Jcrop.css\"/>\n";
    echo "    <script type=\"text/javascript\" src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.min.js\"> </script>\n";
    echo "    <script type=\"text/javascript\" src=\"" . WEB_PATH . "/layout/Jcrop/js/jquery.Jcrop.js\"> </script>\n";
    echo "    <script type=\"text/javascript\">\n";
    echo "      document.onselectstart = function() { return false; }\n";
    echo "      document.onmousedown = function() { return false; }\n";
    echo "      jQuery(function(){\n";
    echo "        jQuery('#group-image').Jcrop({\n";
    echo "          aspectRatio: 1.0,\n";
    echo "          bgOpacity: 0.4,\n";
    echo "          onSelect: updateCoords,\n";
    echo "          onChange: updateCoords\n";
    echo "        });\n";
    echo "      });\n";
    echo "      var cropbox_x = null;\n";
    echo "      var cropbox_y = null;\n";
    echo "      var cropbox_width = null;\n";
    echo "      var cropbox_height = null;\n";
    $image_size = getimagesize(DOCUMENT_ROOT . "/manage/temp/" . $attempt_upload["IMAGE_FILE"]);
    echo "      var image_ratio = " . ($image_size[0] / ($image_size[0] > 800 ? 800 : $image_size[0])) . ";\n";
    echo "      function updateCoords(cropbox)\n";
    echo "      {\n";
    echo "        cropbox_x = cropbox.x * image_ratio;\n";
    echo "        cropbox_y = cropbox.y * image_ratio;\n";
    echo "        cropbox_width = cropbox.w * image_ratio;\n";
    echo "        cropbox_height = cropbox.h * image_ratio;\n";
    echo "      }\n";
    echo "      function saveImage()\n";
    echo "      {\n";
    echo "        if (cropbox_x == null || cropbox_y == null || cropbox_width == null || cropbox_height == null)\n";
    echo "        {\n";
    echo "          alert(\"You must select a part of the image to use as the group image before moving to the next step.\");\n";
    echo "          return;\n";
    echo "        }\n";
    echo "        if ((cropbox_width * image_ratio < 150 || cropbox_height * image_ratio < 150) && " .
	"!confirm(\"The selection you have chosen is less than 150x150pixels, and will have to scale up, potentially " .
	" resulting in pixelation. Are you SURE this is what you wish to do?\"))\n";
    echo "          return;\n";
    echo "        if (!confirm(\"Are you satisfied with the image selection?\"))\n";
    echo "          return;\n";
    echo "        executeAJAX('" . WEB_PATH . "/manage/scripts/crop-group-icon.php?x=' + cropbox_x + '&y=' + cropbox_y + '&width=' + cropbox_width + '&height=' + " .
	"cropbox_height, function evaluate_results(value) {\n";
    echo "          if (value == 'success')\n";
    echo "            window.location = '" . WEB_PATH . "/manage/change-group-picture.php?changed=true';\n";
    echo "          else\n";
    echo "            alert (value);\n";
    echo "        });\n";
    echo "      }\n";
    echo "      function cancelUpload()\n";
    echo "      {\n";
    echo "        executeAJAX('" . WEB_PATH . "/manage/scripts/cancel-group-picture-upload.php', function evaluate_results(value) {\n";
    echo "          if (value == 'success')\n";
    echo "            window.location = '" . WEB_PATH . "/manage/change-group-picture.php?changed=false';\n";
    echo "          else\n";
    echo "            alert (value);\n";
    echo "        });\n";
    echo "      }\n";
    echo "    </script>\n";
    echo "  To begin cropping, click anywhere on your picture and drag. A box will appear, and using the handles (small gray boxes) on the corners and sides of the box, resize " .
	"the image and move it (click and drag) until what you have selected what you believe the group/sport icon should be. When you've finished, click the <b>" .
	"<font color=\"green\">Save</font></b> button below.<br /><br />\n";
    echo "  <center><div style=\"width:" . ($image_size[0] > 800 ? 800 : $image_size[0]) . "px; border:1px solid black;\"><img src=\"" .
	WEB_PATH . "/manage/temp/" . $_SESSION["upload-group-picture"]["IMAGE"] . "\" style=\"width:" . ($image_size[0] > 800 ? 800 : $image_size[0]) .
	"px;\" id=\"group-image\" /></div></center><br />\n";
	echo "  <center><input type=\"button\" class=\"largeButton green\" value=\"Save\" onClick=\"saveImage();\" /> ";
	echo "<input type=\"button\" class=\"largeButton red\" value=\"Cancel\" onClick=\"cancelUpload();\" />" .
		"</center><br />\n";
	exit();

  } else
    echo "<div class=\"error\"><b>Error.</b> " . $attempt_upload["MESSAGE"] . "</div>\n";
}

if (isset($_SESSION["upload-group-picture"]))
{
  if (isset($_GET["changed"]) && $_GET["changed"] == "true")
  {
    $get_new_image_files = $database->querySingle("SELECT title, image_file, icon_file FROM groups WHERE group_identity='" . $database->escapeString($_SESSION["upload-group-picture"]["IDENTITY"]) . "' LIMIT 1", true);
    echo "  <div class=\"success\"><b>Success.</b> The group image and icon for <b>" . $get_new_image_files["title"] . "</b> have been updated.</div>\n";
    echo "  <div class=\"manageSubheader\">Success</div>\n";
    echo "  <center>\n";
    echo "    <img src=\"" . WEB_PATH . "/images/groups/" . $get_new_image_files["icon_file"] . "\" style=\"border:1px solid black;\">\n";
    echo "    <img src=\"" . WEB_PATH . "/images/groups/" . $get_new_image_files["image_file"] . "\" style=\"border:1px solid black;\">\n";
    echo "  </center>\n";
  } else if (isset($_GET["changed"]) && $_GET["changed"] == "false")
  {
    $get_cancelled_club = $database->querySingle("SELECT title FROM groups WHERE group_identity='" . $database->escapeString($_SESSION["upload-group-picture"]["IDENTITY"]) . "' LIMIT 1");
    echo "  <div class=\"error\"><b>Cancelled.</b> Cancelled the upload of a new image and icon. Reverted to default group images.</div>\n";
  }
  unset ($_SESSION["upload-group-picture"]);
}

if (isset($_POST["delete"]))
{
  $group_identity = $_POST["group"];
  $get_group = $database->querySingle("SELECT title, image_file, icon_file FROM groups WHERE group_identity='" . $database->escapeString($group_identity) . "' LIMIT 1", true);
  if ($get_group["image_file"] !== "")
    if (!@unlink(DOCUMENT_ROOT . "/images/groups/" . $get_group["image_file"]))
      echo "<b>Error deleting group image (filename: '" . $get_group["image_file"] . "')</b><br />\n";
  if ($get_group["icon_file"] !== "")
    if (!@unlink(DOCUMENT_ROOT . "/images/groups/" . $get_group["icon_file"]))
      echo "<b>Error deleting group icon (filename: '" . $get_group["icon_file"] . "')</b><br />\n";
  if ($get_group["image_file"] !== "" || $get_group["icon_file"] !== "")
    if ($database->exec("UPDATE groups SET image_file='', icon_file='' WHERE group_identity='" . $database->escapeString($group_identity) . "'"))
      echo "  <div class=\"success\"><b>Images deleted.</b> The group image and icon for <b>" . $get_group["title"] . "</b> have been deleted.</div>\n";
    else
      echo "  <div class=\"error\"><b>Error deleting images.</b> Unable to update the database when attempting to delete group images.</div>\n";
}

if (!isset($attempt_upload) || !$attempt_upload["STATUS"])
{
  echo "  <br />\n";
  echo "  <div class=\"manageSubheader\">Upload</div>\n";
  echo "  <form method=\"post\" enctype=\"multipart/form-data\" action=\"" . WEB_PATH . "/manage/change-group-picture.php\" >\n";
  $get_groups = $database->query("SELECT group_identity, title, (CASE WHEN image_file = '' THEN \"false\" WHEN image_file IS null THEN \"false\" ELSE \"true\" END) AS \"has_image\", " .
	"(CASE WHEN icon_file = '' THEN \"false\" WHEN icon_file IS null THEN \"false\" ELSE \"true\" END) AS \"has_icon\" FROM groups ORDER BY has_image, has_icon, title ASC");
  echo "    <b>Group:</b> <select name=\"group\">\n";
  while ($group = $get_groups->fetchArray())
    echo "      <option value=\"" . $group["group_identity"] . "\">" . ($group["has_image"] == "false" || $group["has_icon"] == "false" ? "[NO IMAGE] " : "") . $group["title"] . "</option>\n";
  echo "    </select><br />\n";
  echo "    <b>File:</b> <input type=\"file\" name=\"image-file\" /><br />\n";
  echo "    <div class=\"manageGuidelines\">\n";
  echo "    <b>Guidelines:</b> <br />\n";
  echo "    <ul>\n";
  echo "      <li><b>School appropriate.</b> Duh.</li>\n";
  echo "      <li><b>Proper file type.</b> The program will only accept JPEG, PNG, or GIF files currently.</li>\n";
  echo "      <li><b>Must be larger than 150x150 pixels.</b> Anything wider than that is fine, but the image needs to at least be large enough to reach " .
	"the minimum.</li>\n";
  echo "    </ul>\n";
  echo "    </div>\n";
  echo "    <center>\n";
  echo "      <input type=\"submit\" class=\"largeButton green\" value=\"Upload\" name=\"upload\" />\n";
  echo "    </center>\n";
  echo "  </form>\n";
  
  echo "  <br />\n";
  echo "  <div class=\"manageSubheader\">Delete</div>\n";
  echo "  <form method=\"post\" action=\"" . WEB_PATH . "/manage/change-group-picture.php\" onsubmit=\"return confirm('Are you sure you wish to delete this club\'s image and icon?');\">\n";
  echo "    <b>Group:</b> <select name=\"group\">\n";
  $get_groups->reset();
  while ($group = $get_groups->fetchArray())
    if ($group["has_image"] == "true" || $group["has_icon"] == "true")
      echo "      <option value=\"" . $group["group_identity"] . "\">" . $group["title"] . "</option>\n";
  echo "    </select><br />\n";
  echo "    <center>\n";
  echo "      <input type=\"submit\" class=\"largeButton red\" value=\"Delete\" name=\"delete\" />\n";
  echo "    </center>\n";
  echo "  </form>\n";
}
  
outputManageFooter();

function uploadImage()
{
  if ($_FILES["image-file"]["error"] != 0)
  {
    switch ($_FILES["image-file"]["error"])
	{
	  case UPLOAD_ERR_INI_SIZE:
	  case UPLOAD_ERR_FORM_SIZE: return array("STATUS" => false, "MESSAGE" => "The file that you attempted to upload had a filesize that was too large.", 
		"IMAGE_FILE" => "");
	  case UPLOAD_ERR_PARTIAL: return array("STATUS" => false, "MESSAGE" => "The file upload process was interrupted.", "IMAGE_FILE" => "");
	  case UPLOAD_ERR_NO_FILE: return array("STATUS" => false, "MESSAGE" => "No file uploaded.", "IMAGE_FILE" => "");
	  default: return array("STATUS" => false, "MESSAGE" => "Encountered an unhandled exception when attempting to upload (Upload error #" .
		$_FILES["image-file"]["error"] . ").", "IMAGE_FILE" => "");
	}
  }
  if (!is_uploaded_file($_FILES["image-file"]["tmp_name"]))
    return array("STATUS" => false, "MESSAGE" => "Selected file was not an uploaded file.", "IMAGE_FILE" => "");
   
  $image_size = getimagesize($_FILES["image-file"]["tmp_name"]);
  if ($image_size === false)
    return array("STATUS" => false, "MESSAGE" => "Uploaded file is not an image in the proper format.", "IMAGE_FILE" => "");
	
  if ($image_size[0] < 150 || $image_size[1] < 150)
    return array("STATUS" => false, "MESSAGE" => "Uploaded image must be 150x150 pixels or larger.", "IMAGE_FILE" => "");
  
  $image_type = exif_imagetype($_FILES["image-file"]["tmp_name"]);
  if ($image_type != IMAGETYPE_GIF && $image_type != IMAGETYPE_JPEG && $image_type != IMAGETYPE_PNG)
    return array("STATUS" => false, "MESSAGE" => "Uploaded image file is not a file of an allowed type (JPEG, PNG, or GIF only).", "IMAGE_FILE" => "");

  switch ($image_type)
  {
    case IMAGETYPE_GIF: $imagetype = "gif"; break;
	case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
	case IMAGETYPE_PNG: $imagetype = "png"; break;
  }
  $tmp_name = preg_split("(\\\\|/)", $_FILES["image-file"]["tmp_name"]);
  $tmp_name = $tmp_name[sizeof($tmp_name) - 1] . "." . $imagetype;
  
  if (!move_uploaded_file($_FILES["image-file"]["tmp_name"], DOCUMENT_ROOT . "/manage/temp/" . $tmp_name))
    return array("STATUS" => false, "MESSAGE" => "Could not move uploaded image to the temporary directory.", "IMAGE_FILE" => "");
  return array("STATUS" => true, "MESSAGE" => "", "IMAGE_FILE" => $tmp_name);
}
?>