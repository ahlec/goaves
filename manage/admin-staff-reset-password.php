<?php
require_once ("../config/manage.inc.php");
processManageSession();
if ($_SESSION[MANAGE_PERMISSIONS_SESSION][9] !== "1")
  header("Location: " . WEB_PATH . "/manage/?permission=denied");

$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_GET["staff_id"]) || !ctype_digit($_GET["staff_id"]))
  header("Location: " . WEB_PATH . "/manage/admin_landing.php?from=admin-staff-reset-password&error=no-passed-staff-id");
if ($database->querySingle("SELECT count(*) FROM staff WHERE identity='" . $database->escapeString($_GET["staff_id"]) . "'") == 0)
  header("Location: " . WEB_PATH . "/manage/admin_landing.php?from=admin-staff-reset-password&error=invalid-staff-id");
$staff_identity = $database->escapeString($_GET["staff_id"]);

if (isset($_POST["reset-yes"]))
{
  $update_success = $database->exec("UPDATE staff SET password='" . DEFAULT_PASSWORD . "' WHERE identity='" . $staff_identity . "'");
  header("Location: " . WEB_PATH . "/manage/admin_landing.php?from=admin-staff-reset-password&reset=" . ($update_success ? "yes" : "no"));
} else if (isset($_POST["reset-no"]))
  header("Location: " . WEB_PATH . "/manage/admin_landing.php?from=admin-staff-reset-password&reset=no");

outputManageHeader("[Admin] Reset Staff Password");

$staff_name = $database->querySingle("SELECT first_name, last_name FROM staff WHERE identity='" . $staff_identity . "' LIMIT 1", true);
echo "<center>Reset password for<br /><b>" . $staff_name["first_name"] . " " . $staff_name["last_name"] . "</b><br /><br />\n";

echo "<form method=\"post\" action=\"" . WEB_PATH . "/manage/admin-staff-reset-password.php?staff_id=" . $staff_identity . "\">\n";
echo "  <input type=\"submit\" name=\"reset-yes\" class=\"largeButton red\" value=\"Yes\" />\n";
echo "  <input type=\"submit\" name=\"reset-no\" class=\"largeButton green\" value=\"No\" />\n";
echo "</form>\n";
echo "<br /></center>\n";

outputManageFooter();
?>