<?php
session_start();
require_once ("../../config/manage.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
$temp_directory = DOCUMENT_ROOT . "/manage/temp/";
$destination = DOCUMENT_ROOT . "/images/groups/";

$image_file = $_SESSION["upload-group-picture"]["IMAGE"];
$x = $_GET["x"];
$y = $_GET["y"];
$width = $_GET["width"];
$height = $_GET["height"];
$group_handle = $database->querySingle("SELECT handle FROM groups WHERE group_identity='" .
	$database->escapeString($_SESSION["upload-group-picture"]["IDENTITY"]) . "' LIMIT 1");

$cropped_image = new ProcessedImage();
$cropped_image->loadOriginal($temp_directory . $image_file);
$cropped_image->crop($x, $y, $width, $height, 150, 150);//true, 300, 400);
$new_filename = $cropped_image->save($destination, $group_handle . "-icon");
$cropped_image->close();
$cropped_image->deleteOriginalImage();

$database->exec("UPDATE groups SET icon_file='" . $database->escapeString($new_filename) . "' WHERE " .
	"group_identity='" . $database->escapeString($_SESSION["upload-group-picture"]["IDENTITY"]) . "'");
exit ("success");
?>