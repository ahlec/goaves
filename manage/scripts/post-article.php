<?php
function prepareString($string, $remove_elements = false)
{
  $value = utf8_encode($string);//html_entity_decode($string));
  $value = str_replace("�", "'", $value);
  $value = str_replace("�", "'", $value);
  $value = str_replace("�", '"', $value);
  $value = str_replace("�", '"', $value);
  $value = str_replace("�", "-", $value);
  $value = str_replace("�", "...", $value);
  $value = preg_replace("/(&nbsp;| )+/", " ", $value);
  if ($remove_elements)
  {
    $value = preg_replace("/<\/p>/", "", preg_replace("/<p(|[^\>]*)>/", "", $value));
    $value = preg_replace("/<\/span>/", "", preg_replace("/<span(|[^\>]*)>/", "", $value));
  }
  $value = str_replace("\\\"", "\"", $value);
  $value = trim($value);
  return $value;
}
function getPostArticle($database, $as_admin = false)
{
  $settings = array();
  $settings["title"] = $database->escapeString(prepareString($_POST["article-title"]));

  $settings["handle"] = trim($database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $settings["title"])))));
  $settings["contents"] = $database->escapeString(prepareString($_POST["article-contents"]));
  $settings["exec_time"] = ($as_admin ? $database->escapeString($_POST["change-post-time"]) : date("Y-m-d H:i:s", strtotime("+1 hour")));
  $settings["post_type"] = $database->escapeString($_POST["article-type"]);
  if ($database->querySingle("SELECT count(*) FROM beat_types WHERE type_identity='" . $settings["post_type"] . "'") == 0)
    $settings["post_type"] = 1;
  $settings["staff_identity"] = ($as_admin ? $database->escapeString($_POST["change-author"]) : $database->escapeString($_SESSION[MANAGE_SESSION]));
  $settings["publish"] = isset($_POST["article-published"]);
  $settings["image_file"] = "";
  $settings["image_alignment"] = ($_POST["image-alignment"] == "left" ? "Left" : "Right");
  $settings["image_caption"] = $database->escapeString(prepareString($_POST["image-caption"], true));
  $settings["is_updating"] = isset($_POST["article-identity"]);
  if (isset($_POST["article-identity"]) && ctype_digit($_POST["article-identity"]))
    $settings["identity"] = $database->escapeString($_POST["article-identity"]);
  $settings["is_migrated_article"] = (isset($settings["identity"]) && $database->querySingle("SELECT count(*) FROM beats WHERE beat_identity='" . $settings["identity"] . "' AND parameters LIKE '%" .
	"-migrated%'") == 1);
  if ($_POST["related-group"] > 0 && $database->querySingle("SELECT count(*) FROM groups WHERE group_identity='" . $database->escapeString($_POST["related-group"]) . "'") > 0)
    $settings["related_group"] = $_POST["related-group"];
  $settings["photo_credit"] = $database->escapeString(prepareString($_POST["photo-credit"]));
  if (isset($settings["identity"]))
    $settings["original_post_time"] = strtotime($database->querySingle("SELECT post_time FROM beats WHERE beat_identity='" . $settings["identity"] . "' LIMIT 1"));
  if ($_POST["topic"] > 0 && $database->querySingle("SELECT count(*) FROM topics WHERE topic_identity='" . $database->escapeString($_POST["topic"]) . "' AND (date_start <= '" . date("Y-m-d",
	(isset($settings["original_post_time"]) ? $settings["original_post_time"] : $settings["exec_time"])) . "' OR date_start IS null OR date_start = '') AND (date_end >= '" . date("Y-m-d", (isset($settings["original_post_time"]) ?
	$settings["original_post_time"] : $settings["exec_time"])) . "' OR date_end IS null OR date_end = '')") > 0)
    $settings["topic_identity"] = $database->escapeString($_POST["topic"]);
  /*echo "<pre>";
  print_r($settings);
  echo "</pre>";
exit();*/
  return $settings;
}

function unparseArticleDatabase($string)
{
  $string = str_replace("''","'", $string);
  return $string;
}
function getPostedArticle($database, $article_id, $override_need_ownership = false)
{
  $article = $database->querySingle("SELECT beat_identity, staff_identity, post_time, handle, title, contents, image_alignment, " .
	"image_caption, type, published, image_file, related_group, photo_credit, topic FROM beats " .
	"WHERE beat_identity='" . $database->escapeString($article_id) . "'" . (!$override_need_ownership ? " AND staff_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "'" : "") . " LIMIT 1", true);
  if ($article === false)
    return false;
  return array("title" => $article["title"], "handle" => $article["handle"], "contents" => unparseArticleDatabase($article["contents"]), "exec_time" => date("Y-m-d G:i:s"),
	"post_type" => $article["type"], "staff_identity" => $article["staff_identity"], "publish" => $article["published"],
	"image_alignment" => $article["image_alignment"], "related_group" => $article["related_group"],
	"image_caption" => unparseArticleDatabase($article["image_caption"]), "identity" => $article["beat_identity"], "is_updating" => true, "image_file" => $article["image_file"],
	"photo_credit" => $article["photo_credit"], "original_post_time" => strtotime($article["post_time"]), "topic_identity" => $article["topic"]);
}

function uploadPostImage($article)
{
  if ($_FILES["image-file"]["error"] == 4)
    return array("STATUS" => false, "MESSAGE" => "No image uploaded", "IMAGE_FILE" => "");
  
  if ($_FILES["image-file"]["error"] != 0)
  {
    if ($_FILES["image-file"]["error"] != 4)
	  return array("STATUS" => false, "MESSAGE" => "Uploaded file is too large to process", "IMAGE_FILE" => "");
	else
	  return array("STATUS" => false, "MESSAGE" => "No file uploaded", "IMAGE_FILE" => "");
  }
  if (!is_uploaded_file($_FILES["image-file"]["tmp_name"]))
    return array("STATUS" => false, "MESSAGE" => "Selected file was not an uploaded file", "IMAGE_FILE" => "");
   
  $image_size = getimagesize($_FILES["image-file"]["tmp_name"]);
  if ($image_size === false)
    return array("STATUS" => false, "MESSAGE" => "Uploaded file is not an image in the proper format", "IMAGE_FILE" => "");
  
  $image_type = exif_imagetype($_FILES["image-file"]["tmp_name"]);
  if ($image_type != IMAGETYPE_GIF && $image_type != IMAGETYPE_JPEG && $image_type != IMAGETYPE_PNG)
    return array("STATUS" => false, "MESSAGE" => "Uploaded image file is not a file of an allowed type", "IMAGE_FILE" => "");

  if ($image_size[0] > 300)
  {
    switch ($image_type)
	{
	  case IMAGETYPE_GIF: $uploaded_image = imagecreatefromgif($_FILES["image-file"]["tmp_name"]); break;
	  case IMAGETYPE_JPEG: $uploaded_image = imagecreatefromjpeg($_FILES["image-file"]["tmp_name"]); break;
	  case IMAGETYPE_PNG: $uploaded_image = imagecreatefrompng($_FILES["image-file"]["tmp_name"]); break;
	}
	$new_width = floor(300);
	$new_height = floor(($new_width / $image_size[0]) * $image_size[1]);
	$new_image = imagecreatetruecolor($new_width, $new_height);
	if (!imagecopyresampled($new_image, $uploaded_image, 0, 0, 0, 0, $new_width, $new_height, $image_size[0], $image_size[1]))
	  return array("STATUS" => false, "MESSAGE" => "Unable to resize image", "IMAGE_FILE" => "");
    else
	{
	  switch ($image_type)
	  {
	    case IMAGETYPE_GIF: imagegif($new_image, $_FILES["image-file"]["tmp_name"]); break;
		case IMAGETYPE_JPEG: imagejpeg($new_image, $_FILES["image-file"]["tmp_name"]); break;
		case IMAGETYPE_PNG: imagepng($new_image, $_FILES["image-file"]["tmp_name"]); break;
	  }
	}
  }
  switch ($image_type)
  {
    case IMAGETYPE_GIF: $imagetype = "gif"; break;
	case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
	case IMAGETYPE_PNG: $imagetype = "png"; break;
  }
  $image_file = $article["handle"] . "." . $imagetype;
  return array("STATUS" => true, "MESSAGE" => "", "IMAGE_FILE" => $image_file);
}

function postArticle($database, $article, $as_admin = false)
{
  $notices = array();
  if (mb_strlen($article["title"]) < 8)
    return "Article title must have a minimum of 8 characters";
  if (mb_strlen($article["handle"]) < 8)
    return "Article handle must have a minimum of 8 characters";
  if (!$article["is_updating"] && $database->querySingle("SELECT count(*) FROM beats WHERE handle='" . $article["handle"] . "'") != 0)
    return "Title/handle are already in use";
  if (mb_strlen($article["contents"]) < 40 && $article["publish"])
    return "Articles should be (at minimum) of 40 characters or more.";
  if (!ctype_digit($article["staff_identity"]) || $database->querySingle("SELECT count(*) FROM staff WHERE identity='" . $article["staff_identity"] . "'") == 0)
    return "Invalid staff account";

  if (isset($_POST["delete-image"]) && $article["image_file"] != "")
  {
    if (unlink(DOCUMENT_ROOT . "/images/articles/" . $article["image_file"]))
	{
	  if (!$database->exec("UPDATE beats SET image_file='' WHERE beat_identity='" . $article["identity"] . "'"))
	    $notices[] = "Unable to update article to delete image";
	  else
	    $article["image_file"] = "";
	} else
	  $notices[] = "Unable to delete article image";
  }
  $image_results = uploadPostImage($article);
  if (!$image_results["STATUS"] && $image_results["MESSAGE"] != "" && $image_results["IMAGE_FILE"] == "")
    return $image_results["MESSAGE"];

  /* Attempt Posting */
  $article_query = null;
  if ($article["is_updating"])
  {
    $article_query = "UPDATE beats SET handle='" . $article["handle"] . "', title='" . $article["title"] . "', contents='" . $article["contents"] . "',";
	if ($image_results["STATUS"])
	  $article_query .= " image_file='" . $image_results["IMAGE_FILE"] . "',";
	if ($as_admin)
	  $article_query .= " staff_identity='" . $article["staff_identity"] . "', post_time='" . $article["exec_time"] . "',";
	if (!$article["is_migrated_article"])
	  $article_query .= " last_updated='" . $article["exec_time"] . "',";
	$article_query .= " image_alignment='" . $article["image_alignment"] . "', image_caption='" .
		$article["image_caption"] . "', type='" . $article["post_type"] . "', published='" . ($article["publish"] ? "TRUE" : "FALSE") .
		"', related_group='" . (isset($article["related_group"]) ? $article["related_group"] : "") . "', " .
		"photo_credit='" . $article["photo_credit"] . "', topic='" . (isset($article["topic_identity"]) ? $article["topic_identity"] : "") . "' WHERE beat_identity='" . $article["identity"] . "'";
  
  } else
    $article_query = "INSERT INTO beats(staff_identity, handle, title, post_time, contents, image_file, image_alignment, " .
		"image_caption, type, published, last_updated, related_group, photo_credit, topic) VALUES('" . $article["staff_identity"] . "','" . $article["handle"] . "','" . $article["title"] . "','" . 
		$article["exec_time"] . "','" . $article["contents"] . "','" . ($image_results["STATUS"] ? $image_results["IMAGE_FILE"] : "") . "','" . 
		($image_results["STATUS"] ? $article["image_alignment"] : "") . "','" . ($image_results["STATUS"] ? $article["image_caption"] : "") . 
		"','" . $article["post_type"] . "','" . ($article["publish"] ? "TRUE" : "FALSE") . "','" . $article["exec_time"] . "','" .
		(isset($article["related_group"]) ? $article["related_group"] : "") . "','" . $article["photo_credit"] . "','" . (isset($article["topic_identity"]) ? $article["topic_identity"] : "") . "')";
	
  $article_posted = $database->exec($article_query);
  if ($article_posted && $image_results["STATUS"])
  {
    if (!move_uploaded_file($_FILES["image-file"]["tmp_name"], DOCUMENT_ROOT . "/images/articles/" . $image_results["IMAGE_FILE"]))
      $notices[] = "Image could not be moved.";
    else
	$article["image_file"] = $image_results["IMAGE_FILE"];
  }
  if ($article_posted)
  {
    if ($article["publish"])
      $database->query("UPDATE statistics SET value='" . date("Y-m-d") . "' WHERE stat_handle='content_last_updated'");
	return array("RESULT" => true, "NOTICES" => $notices, "IMAGE_FILE" => $article["image_file"]);
  } else
    return "Article could not be " . ($article["is_updating"] ? "updated" : "posted");
	
}
?>