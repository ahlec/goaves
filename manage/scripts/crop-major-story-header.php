<?php
session_start();
require_once ("../../config/manage.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
$temp_directory = DOCUMENT_ROOT . "/manage/temp/";
$destination = DOCUMENT_ROOT . "/images/features/";
if (!isset($_SESSION["make-major-story"]))
  exit ("No valid upload session");
if ($database->querySingle("SELECT count(*) FROM beats WHERE beat_identity='" .
	$database->escapeString($_SESSION["make-major-story"]["ARTICLE"]) . "'") == 0)
  exit ("No valid article.");
$article = $database->escapeString($_SESSION["make-major-story"]["ARTICLE"]);
if ($database->querySingle("SELECT count(*) FROM beats WHERE beat_identity='" . $article .
	"' AND staff_identity='" . $_SESSION[MANAGE_SESSION] . "'") == 0)
  exit ("You are not the author of the article you attempted to manage.");
if ($database->querySingle("SELECT count(*) FROM features WHERE feature_type='article' AND item_identity='" .
	$article . "'") > 0)
  exit ("Intended article is already a major story.");

$image_file = $_SESSION["make-major-story"]["IMAGE"];
$x = $_GET["x"];
$y = $_GET["y"];
$width = $_GET["width"];
$height = $_GET["height"];
$article_handle = $database->querySingle("SELECT handle FROM beats WHERE beat_identity='" .
	$article . "' LIMIT 1");

$cropped_image = new ProcessedImage();
$cropped_image->loadOriginal($temp_directory . $image_file);
$cropped_image->crop($x, $y, $width, $height, 700, 300);//true, 300, 400);
$new_filename = $cropped_image->save($destination, $article_handle);
$cropped_image->close();
$cropped_image->deleteOriginalImage();

$database->exec("INSERT INTO features(feature_type, item_identity, header_image, date_featured) VALUES('article','" . $article . "','" . 
	$database->escapeString($new_filename) . "','" . date("Y-m-d", strtotime("+1 hours")) . "')");
unset ($_SESSION["make-major-story"]);
exit ("success");
?>