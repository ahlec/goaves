<?php
session_start();
require_once ("../../config/manage.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);

if (!isset($_SESSION["make-major-story"]))
  exit ("success");
if ($database->querySingle("SELECT count(*) FROM beats WHERE beat_identity='" .
	$database->escapeString($_SESSION["make-major-story"]["ARTICLE"]) . "'") == 0)
  exit ("success");
$article = $database->escapeString($_SESSION["make-major-story"]["ARTICLE"]);
if ($database->querySingle("SELECT count(*) FROM beats WHERE beat_identity='" . $article .
	"' AND staff_identity='" . $_SESSION[MANAGE_SESSION] . "'") == 0)
  exit ("success");
if ($database->querySingle("SELECT count(*) FROM major_stories WHERE article_identity='" .
	$article . "'") == 0)
  exit ("success");

$header_image = $database->querySingle("SELECT header_image FROM major_stories WHERE article_identity='" .
	$article . "' LIMIT 1");

if (!$database->exec("DELETE FROM major_stories WHERE article_identity='" . $article . "'"))
  exit ("Unable to update the database for the removal of major story status.");
if ($header_image !== "")
  if (!@unlink (DOCUMENT_ROOT . "/images/major_stories/" . $header_image))
    exit ("Unable to delete header_image");
if (!@unlink (DOCUMENT_ROOT . "/manage/temp/" . $_SESSION["make-major-story"]["IMAGE"]))
  exit ("Unable to delete temporary image file.");
unset ($_SESSION["make-major-story"]);
exit ("success");
?>