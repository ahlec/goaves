<?php
session_start();
require_once ("../../config/manage.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);

$group_pictures = $database->querySingle("SELECT image_file, icon_file FROM groups WHERE group_identity='" .
	$database->escapeString($_SESSION["upload-group-picture"]["IDENTITY"]) . "' LIMIT 1", true);

if (!$database->exec("UPDATE groups SET image_file='', icon_file='' WHERE group_identity='" . $database->escapeString($_SESSION["upload-group-picture"]["IDENTITY"]) . "'"))
  exit ("Unable to update the database for the removal of current group pictures.");
if ($group_pictures["image_file"] !== "")
  if (!@unlink (DOCUMENT_ROOT . "/images/groups/" . $group_pictures["image_file"]))
    exit ("Unable to delete current group image.");
if ($group_pictures["icon_file"] !== "")
  if (!@unlink (DOCUMENT_ROOT . "/images/groups/" . $group_pictures["icon_file"]))
    exit ("Unable to delete current group icon file.");
if (!@unlink (DOCUMENT_ROOT . "/manage/temp/" . $_SESSION["upload-group-picture"]["IMAGE"]))
  exit ("Unable to delete temporary image file.");
exit ("success");
?>