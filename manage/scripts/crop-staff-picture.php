<?php
session_start();
require_once ("../../config/manage.inc.php");
require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
$temp_directory = DOCUMENT_ROOT . "/manage/temp/";
$destination = DOCUMENT_ROOT . "/images/staff/";

$image_file = $_SESSION["upload-staff-picture"];
$x = $_GET["x"];
$y = $_GET["y"];
$width = $_GET["width"];
$height = $_GET["height"];
$staff_name = $database->querySingle("SELECT first_name, last_name FROM staff WHERE identity='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]) . "' LIMIT 1", true);

$cropped_image = new ProcessedImage();
$cropped_image->loadOriginal($temp_directory . $image_file);
$cropped_image->crop($x, $y, $width, $height, 300, 400, false);//true, 300, 400);
$new_filename = $cropped_image->save($destination, mb_strtolower($staff_name["first_name"] . "-" .
	$staff_name["last_name"]));
$cropped_image->close();

$database->exec("UPDATE staff SET image_file='" . $database->escapeString($new_filename) . "' WHERE " .
	"identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "'");
exit ("success");
?>