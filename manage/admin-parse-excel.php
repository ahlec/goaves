<?php

error_reporting(E_ALL);
require_once ("../config/manage.inc.php");
processManageSession();
if ($_SESSION[MANAGE_PERMISSIONS_SESSION][9] !== "1")
  header("Location: " . WEB_PATH . "/manage/?permission=denied");
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("[Administration] Parse XML File");  

if (isset($_POST["bind-xml"]))
{
  $bound_database = $_POST["xml-parse-database-table"];
  $bound_columns = array();
  $cur_column_identity = 1;
  while (isset($_POST["xml-parse-column-" . $cur_column_identity . "-binding"]))
  {
    $bound_columns[] = array("BINDING" => $_POST["xml-parse-column-" . $cur_column_identity . "-binding"], "VALUE" => $_POST["xml-parse-column-" .
		$cur_column_identity . "-binding-value-" . ($_POST["xml-parse-column-" . $cur_column_identity . "-binding"] == "xml" ? "xml" : "text")],
		"DATABASE_COLUMN" => $_POST["xml-parse-column-" . $cur_column_identity . "-binding-database-column"]);
    $cur_column_identity++;
  }
  foreach ($bound_columns as $col_position => $bound_column)
    if ($bound_column["VALUE"] == "")
	  unset ($bound_columns[$col_position]);
/*echo "<pre>\n";
print_r($_POST);
print_r($bound_columns);
exit("</pre>\n");*/
  $xml_file = new DOMDocument();
  $xml_file->loadXML($_SESSION["goavesRUSTYXMLPARSINGSESSION"]);
  $data_table = $_POST["xml-parse-database-table"];
  $number_columns = $_POST["xml-parse-number-columns"];
  
  
$xml_rows = $xml_file->getElementsByTagName("Row");
$header_row = $xml_rows->item(0);
$raw_xml_columns = $header_row->getElementsByTagName("Cell");
for ($parse_xml_column = 0; $parse_xml_column < $raw_xml_columns->length; $parse_xml_column++)
  $xml_columns[$raw_xml_columns->item($parse_xml_column)->nodeValue] = $parse_xml_column;//] = $raw_xml_columns->item($parse_xml_column)->nodeValue;
  
  $data_rows = $xml_file->getElementsByTagName("Row");
  $bound_queries = array();
  for ($row = 1; $row < $data_rows->length; $row++)
  {
    $query = "INSERT INTO " . $data_table . "(";
	foreach ($bound_columns as $position => $bound_column)
	  $query .= $bound_column["DATABASE_COLUMN"] . ($position <= sizeof($bound_columns) ? ", " : "");
	$query .= ") VALUES(";
	$cells = $data_rows->item($row)->getElementsByTagName("Cell");
	foreach ($bound_columns as $position => $bound_column)
	{
	  $query .= "'";
	  if ($bound_column["BINDING"] == "constant")
	    $query .= $database->escapeString($bound_column["VALUE"]);
	  else
	    $query .= $database->escapeString(@$cells->item($xml_columns[$bound_column["VALUE"]])->nodeValue);
	  $query .= "'" . ($position <= sizeof($bound_columns) ? ", " : "");
	}
	$query .= ");";
	$bound_queries[$row] = $query;
  }
  echo "<div class=\"notice\"><b>Notice.</b> SQL queries have been generated, but must be manually copied to the database (allows for review of entries, individual " .
	"exclusions)</div>\n";
  echo "<textarea style=\"width:100%;\" rows=\"20\">\n";
  foreach ($bound_queries as $bound_query)
    echo $bound_query . "\n";
  echo "</textarea>\n";
} else if (isset($_POST["parse"]))
{
  $_SESSION["goavesRUSTYXMLPARSINGSESSION"] = $_POST["xml-source"];
  $xml_file = new DOMDocument();
  $xml_file->loadXML($_POST["xml-source"]);
  $xml_rows = $xml_file->getElementsByTagName("Row");
  if ($xml_rows->length > 0)
  {
    $header_row = $xml_rows->item(0);
	$columns = $header_row->getElementsByTagName("Cell");
	$number_of_columns = $columns->length;
	echo "<form method=\"post\" id=\"xml-parse-form\" onSubmit=\"return false;\">\n";
//	echo "<b>Number of columns:</b> " . $number_of_columns;
	echo "<input type=\"hidden\" name=\"xml-parse-number-columns\" value=\"" . $number_of_columns . "\" />\n";
	echo "<br>";
	echo "<b>Bind to database table:</b> <select name=\"xml-parse-database-table\" onChange=\"loadXMLParsingBinding(this.value);\"><option></option>\n";
	$database_databases = $database->query("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name ASC");
	while ($database_name = $database_databases->fetchArray())
	  echo "  <option>" . $database_name["name"] . "</option>\n";
	echo "</select>\n";
	
	echo "<div id=\"xml-parse-bindings\" style=\"display:none;\">\n";
	echo "</div>\n";
	//echo "<br /><input type=\"submit\" value=\"Parse and Bind\" name=\"bind-xml\" />\n";
	echo "</form>\n";
  } else
    echo "<div class=\"error\"><b>Error.</b> There are no rows provided in the XML file.</div>\n";
} else
{
  echo "<form method=\"post\">\n";
  echo "<textarea name=\"xml-source\" style=\"width:100%;\" rows=\"20\"></textarea><br />\n";
  echo "<input type=\"submit\" value=\"Parse\" name=\"parse\" />\n";
  echo "</form>\n";
}
outputManageFooter();
?>