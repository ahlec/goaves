<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("Inbox");
if (isset($_GET["sent-message"]))
  echo "<div class=\"success\"><b>Success.</b> Message sent.</div>\n";
$messages = $database->query("SELECT message_identity, staff.first_name AS \"sender_first_name\", " .
	"staff.last_name AS \"sender_last_name\", message, time_sent, read_yet  FROM staff_messages JOIN staff ON staff.identity=" . 
	"staff_messages.sender_identity WHERE staff_messages.recipient_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) .
	"' ORDER BY time_sent DESC");
if ($messages->numberRows() == 0)
  echo "Your inbox is empty.<br /><br />\n";
else
{
  while ($message = $messages->fetchArray())
  {
    echo "<a href=\"" . WEB_PATH . "/manage/message.php?identity=" . $message["message_identity"] . "\">";
    echo "<div class=\"articleContainer" . ($message["read_yet"] == "FALSE" ? " containerPublished" : " containerDraft") .  "\">" . 
	  "<b>From:</b> " . $message["sender_first_name"] . " " . $message["sender_last_name"] . " <b>on</b> " .
	  date(DATE_FORMAT, strtotime($message["time_sent"])) . "<br />\n";
    $blurb = get_smart_blurb(format_content($message["message"]), 20);
    echo "<div style=\"font-size:80%;\">" . $blurb . ($blurb != format_content($message["message"]) ? "..." : "") . "</div>\n";
    echo "</div></a>\n";
  }
  echo "<br>\n";
}
outputManageFooter();
?>