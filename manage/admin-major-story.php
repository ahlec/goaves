<?php
require_once ("../config/manage.inc.php");
processManageSession();
if ($_SESSION[MANAGE_PERMISSIONS_SESSION][9] !== "1")
  header("Location: " . WEB_PATH . "/manage/?permission=denied");
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("Toggle Major Story");
echo "Under development";
outputManageFooter();
exit();
if (isset($_GET["redirect"]))
{
  $from = $_GET["from"];
  $code = $_GET["code"];
  if ($from == "toggle-major-story")
  {
    switch ($code)
	{
	  case "01" : echo "<div class=\"error\"><b>Error.</b> The article you attempted to change does not exist.</div>\n"; break;
	  case "02" : echo "<div class=\"error\"><b>Error.</b> You are not the author of the article you attempted to toggle.</div>\n"; break;
	  case "03" : echo "<div class=\"success\"><b>Success.</b> Removed major story status from the selected article.</div>\n"; break;
	  case "04" : echo "<div class=\"error\"><b>Error.</b> Unable to remove major story status from the selected article.</div>\n"; break;
         case "05" : echo "<div class=\"error\"><b>Error.</b> Unable to delete the header image for the selected major story. Demotion cancelled.</div>\n"; break;
	  case "06" : echo "<div class=\"error\"><b>Error.</b> " . $_GET["msg"] . "</div>\n"; break;
         case "07" : echo "<div class=\"success\"><b>Success.</b> Promoted the selected article to major story status. Now visible on the front page.</div>\n"; break;
         //case "08" : echo "<div class=\"notice\"><b>Notice.</b> Promotion cancelled.</div>\n"; break;
       }
  }

}
$articles = $database->query("SELECT beat_identity, title, post_time, (CASE WHEN header_image IS null THEN \"FALSE\" ELSE \"TRUE\" END)" .
	" AS \"is_major_story\" FROM beats LEFT JOIN major_stories ON beat_identity = article_identity WHERE staff_identity='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]) . "' ORDER BY is_major_story, post_time DESC");
if ($articles->numberRows() == 0)
  echo "You have no articles.<br /><br />\n";
else
{
  while ($article = $articles->fetchArray())
  {
    echo "<div class=\"articleContainer" . ($article["is_major_story"] == "TRUE" ? " containerPublished" : " containerDraft") .  "\">" . 
	  "<b>" . $article["title"] . "</b><br />\n";
	echo "<a href=\"" . WEB_PATH . "/manage/toggle-major-story.php?article_id=" . $article["beat_identity"] . "\"><img src=\"" .
		WEB_PATH . "/layout/manage-icon-toggle-" . ($article["is_major_story"] == "TRUE" ? "unpublish" : "publish") . ".png\" border=\"0\" class=\"" .
		"draftIcon\" title=\"" . ($article["is_major_story"] == "FALSE" ? "Make a major story" : "demote") . "\" /> " . ($article["is_major_story"] == "FALSE" ?
		"Make a major story" : "Demote") . "</a> ";
	echo "</div>\n";
  }
  echo "<br>\n";
}
outputManageFooter();
?>