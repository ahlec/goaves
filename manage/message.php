<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_GET["identity"]) || $database->querySingle("SELECT count(*) FROM staff_messages WHERE (recipient_identity='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]) . "' OR sender_identity='" . $database->escapeString($_SESSION[MANAGE_SESSION]) .
	"') AND message_identity='" . $database->escapeString($_GET["identity"]) . "'") == 0)
{
  header("Location: " . WEB_PATH . "/manage/inbox.php?read-failure");
  exit();
}
$message_info = $database->querySingle("SELECT message_identity, staff.first_name AS \"sender_first_name\", " .
	"staff.last_name AS \"sender_last_name\", message, time_sent, read_yet FROM staff_messages JOIN staff ON staff.identity=" . 
	"staff_messages.sender_identity WHERE message_identity='" . $database->escapeString($_GET["identity"]) .
	"' LIMIT 1", true);
if ($message_info["read_yet"] == "FALSE")
  $database->exec("UPDATE staff_messages SET read_yet='TRUE' WHERE message_identity='" . $message_info["message_identity"] . "'");
outputManageHeader("Message");
echo "<b>From:</b> " . $message_info["sender_first_name"] . " " . $message_info["sender_last_name"] . "<br />\n";
echo "<b>Sent:</b> " . date(LONG_DATETIME_FORMAT, strtotime($message_info["time_sent"])) . "<br /><br />\n";
echo "<div style=\"border-top:1px solid black;border-bottom:1px solid black;text-align:center;font-size:90%;\">" .
	format_content($message_info["message"]) . "</div>\n";
echo "<a href=\"" . WEB_PATH . "/manage/send-mail.php?reply=" . $message_info["message_identity"] . "\">Reply</a>\n";
outputManageFooter();
?>