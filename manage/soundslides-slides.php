<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_GET["slideshow_identity"]) || $database->querySingle("SELECT count(*) FROM sound_slides WHERE staff_identity='" .
	$database->escapeString($_SESSION[MANAGE_SESSION]) . "' AND slideshow_identity='" . $database->escapeString($_GET["slideshow_identity"]) . "'") == 0)
	header("Location: " . WEB_PATH . "/manage/manage-soundslides.php?redirect=true&from=soundslides-slides&code=01");
$slideshow_information = $database->querySingle("SELECT slideshow_identity, title, date_last_updated FROM sound_slides WHERE slideshow_identity='" .
	$database->escapeString($_GET["slideshow_identity"]) . "' LIMIT 1", true);
outputManageHeader("Manage Slideshow: '" . $slideshow_information["title"] . "'");

echo "<div class=\"newChange\"><b>Note:</b> Like everything else, you need to publish your soundslides from the <a href=\"" . WEB_PATH . "/manage/manage-soundslides.php\">manage soundslides</a> page.</div>\n";

if (isset($_GET["delete-slide"]))
{
  $target_slide = $database->escapeString($_GET["delete-slide"]);
  if ($database->querySingle("SELECT count(*) FROM sound_slides_slides WHERE slideshow_identity='" . $slideshow_information["slideshow_identity"] . "' AND slide_identity='" .
	$target_slide . "'") > 0)
	{
	  $target_slide_info = $database->querySingle("SELECT image_file, start_time FROM sound_slides_slides WHERE slide_identity='" . $target_slide . "' LIMIT 1", true);
	  if ($database->exec("DELETE FROM sound_slides_slides WHERE slide_identity='" . $target_slide . "'"))
	  {
	    if (!unlink(DOCUMENT_ROOT . "/images/sound_slides/" . $target_slide_info["image_file"]))
		  echo "<div class=\"error\"><b>Error.</b> Deleted the slide from the database, but could not remove the image from the server.</div>\n";
		if ($target_slide_info["start_time"] == "0" || $target_slide_info["start_time"] == "0000")
		{
		  $slideshow_thumbnail = $database->querySingle("SELECT thumbnail_image FROM sound_slides WHERE slideshow_identity='" . $slideshow_information["slideshow_identity"] .
			"' LIMIT 1");
		  $database->exec("UPDATE sound_slides SET thumbnail_image='' WHERE slideshow_identity='" . $slideshow_information["slideshow_identity"] . "'");
		  if (!unlink(DOCUMENT_ROOT . "/images/sound_slides/" . $slideshow_thumbnail))
		    echo "<div class=\"error\"><b>Error.</b> Unable to delete the thumbnail image for the slideshow.</div>\n";
		}
	  } else
	    echo "<div class=\"error\"><b>Error.</b> Unable to delete the slide from the slideshow (Programming error).</div>\n";
	} else
	  echo "<div class=\"error\"><b>Error.</b> The slide you attempted to delete does not exist within the current slideshow.</div>\n";
}

if (isset($_POST["submit-add-slide"]))
{
  require_once (DOCUMENT_ROOT . "/manage/processes/get-file-extension.php");
  require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
  $slide_caption = $database->escapeString($_POST["slide-caption"]);
  $slide_start_minutes = $_POST["slide-start-minutes"];
  $slide_start_seconds = $_POST["slide-start-seconds"];
  if (ctype_digit($slide_start_minutes) && ctype_digit($slide_start_seconds))
  {
    $start_time = (($slide_start_minutes * 60) + $slide_start_seconds) * 1000;
	if ($start_time >= 0) // OR LARGER THAN TIME OF AUDIO FILE
	{
	  if (isset($_FILES["slide-image"]) && $_FILES["slide-image"]["error"] == "0")
	  {
	    $uploaded_file_extension = getFileExtension($_FILES["slide-image"]["name"]);
		if ($uploaded_file_extension == "jpg" || $uploaded_file_extension == "jpeg" || $uploaded_file_extension == "png" || $uploaded_file_extension == "gif")
		{
		  $tmp_name = preg_split("(\\\\|/)", $_FILES["slide-image"]["tmp_name"]);
		  $tmp_name = $tmp_name[sizeof($tmp_name) - 1] . "." . $uploaded_file_extension;
		  move_uploaded_file($_FILES["slide-image"]["tmp_name"], DOCUMENT_ROOT . "/manage/temp/" . $tmp_name);
		  $uploaded_image = new ProcessedImage();
		  $uploaded_image->loadOriginal(DOCUMENT_ROOT . "/manage/temp/" . $tmp_name);
		  $uploaded_image->scale(500, 500, false);
		  $next_slide_in_set = $database->querySingle("SELECT count(*) FROM sound_slides_slides WHERE slideshow_identity='" . $slideshow_information["slideshow_identity"] . "'") + 1;
		  $new_filename = $uploaded_image->save(DOCUMENT_ROOT . "/images/sound_slides/", $slideshow_information["slideshow_identity"] . "-" . $next_slide_in_set);
		  $uploaded_image->close();
		  if ($start_time == 0)
		  {
		    $thumbnail_image = new ProcessedImage();
		    $thumbnail_image->loadOriginal(DOCUMENT_ROOT . "/manage/temp/" . $tmp_name);
		    $thumbnail_image->scale(150, 150, true);
		    $slide_thumbnail = $thumbnail_image->save(DOCUMENT_ROOT . "/images/sound_slides/", "thumbnail-" . $slideshow_information["slideshow_identity"]);
		    $thumbnail_image->close();
			$thumbnail_image->deleteOriginalImage();
		  }else
			$uploaded_image->deleteOriginalImage();
		  if ($database->exec("INSERT INTO sound_slides_slides(slideshow_identity, image_file, start_time, caption) VALUES('" . $slideshow_information["slideshow_identity"] .
			"','" . $new_filename . "','" . $database->escapeString($start_time) . "','" . $slide_caption . "')"))
			{
			  if (date("Y-m-d", strtotime($slideshow_information["date_last_updated"])) != date("Y-m-d", strtotime("+1 hours")))
			    $database->exec("INSERT INTO multimedia_feed(media_type, media_identity, post_time, post_type, posting_staff_member) VALUES('sound_slides','" .
					$slideshow_information["slideshow_identity"] .
					"','" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "','has new slides','" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "')");
			  $database->exec("UPDATE sound_slides SET date_last_updated='" . date("Y-m-d H:i:s", strtotime("+1 hours")) . "' WHERE slideshow_identity='" . $slideshow_information["slideshow_identity"] .
				"'");
			  echo "<div class=\"success\"><b>Posting success.</b> The slide has been added to the slideshow.</div>\n";
			  if ($start_time == 0)
			    $database->exec("UPDATE sound_slides SET thumbnail_image='" . $slide_thumbnail . "' WHERE slideshow_identity='" . $slideshow_information["slideshow_identity"] . "'");
			} else
			  echo "<div class=\"error\"><b>Posting error.</b> Unable to add slide to database (Programming error).</div>\n";
	    } else
		  echo "<div class=\"error\"><b>Posting error.</b> Uploaded file was not of the proper file type (must be JPEG, PNG, or GIF).</div>\n";
	  } else
	    echo "<div class=\"error\"><b>Posting error.</b> Image file could not be uploaded (no image supplied, or file was too large).</div>\n";
	} else
	  echo "<div class=\"error\"><b>Posting error.</b> Start time was negative (minimum start time is 00:00) or exceeded the length of the audio file.</div>\n";
  } else
    echo "<div class=\"error\"><b>Posting error.</b> Start time contained non-numeric characters.</div>\n";
}

if ($database->querySingle("SELECT count(*) FROM sound_slides_slides WHERE slideshow_identity='" . $slideshow_information["slideshow_identity"] .
	"' AND (start_time LIKE '0' OR start_time LIKE '0000')") == 0)
  echo "<div class=\"notice\"><b>Notice.</b> In order to publish a sound slide, there must be a slide that begins at '00:00'.</div>\n";

echo "<div class=\"manageSubheader\">Existing Slides</div>\n";
$slides = $database->query("SELECT slide_identity, image_file, start_time, caption FROM sound_slides_slides WHERE slideshow_identity='" .
	$slideshow_information["slideshow_identity"] . "' ORDER BY start_time ASC");
if ($slides->numberRows() == 0)
{
  echo "The slideshow has no slides";
} else
  while ($slide = $slides->fetchArray())
  {
    echo "<div class=\"slideshowIconContainer\">\n";
	echo "  <img src=\"" . WEB_PATH . "/images/sound_slides/" . $slide["image_file"] . "\" class=\"slideIcon\" />\n";
	$seconds_in = $slide["start_time"] / 1000;
	$minutes_in = floor($seconds_in / 60);
	$seconds_in -= ($minutes_in * 60);
	echo "  <div class=\"slideInfo\"><br /><b>Begins at</b><br />" . str_pad($minutes_in, 2, "0", STR_PAD_LEFT) . ":" . 
		str_pad($seconds_in, 2, "0", STR_PAD_LEFT) . "<br /><br /><a href=\"" . WEB_PATH . "/manage/soundslides-slides.php?slideshow_identity=" .
		$slideshow_information["slideshow_identity"] . "&delete-slide=" . $slide["slide_identity"] . "\"><small>Delete</small></a></div>\n";
	echo "</div>\n";
  }
echo "<br /><br /><div class=\"manageSubheader\">Create Slide</div>\n";
echo "<form method=\"post\" action=\"" . WEB_PATH . "/manage/soundslides-slides.php?slideshow_identity=" . $slideshow_information["slideshow_identity"] . "\" enctype=\"multipart/form-data\">\n";
echo "<b>Image file:</b> <input type=\"file\" name=\"slide-image\" /><br />\n";
echo "<b>Caption:</b> <textarea name=\"slide-caption\"></textarea><br />\n";
echo "<b>Caption start time:</b> <input type=\"text\" name=\"slide-start-minutes\" />:<input type=\"text\" name=\"slide-start-seconds\" /> (minutes:seconds)</br >\n";
echo "<input type=\"submit\" value=\"Add Slide\" name=\"submit-add-slide\" />\n";
echo "</form>\n";
outputManageFooter();
?>