<?php
function redirectError($message)
{
  echo "<div class=\"error\"><b>Error.</b> " . $message . "</div>\n";
}
function redirectSuccess($message)
{
  echo "<div class=\"success\"><b>Success.</b> " . $message . "</div>\n";
}
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("Management Panel");
if (isset($_GET["permission"]) && $_GET["permission"] == "denied")
  redirectError("You do not have the proper permissions to access the feature you attempted to load.");
if (isset($_GET["page"]))
  redirectError("The page you tried to load does not exist.");
if (isset($_GET["redirect"]))
{
  $from = $_GET["from"];
  $code = $_GET["code"];
  if ($from == "post-article" && $code == "01")
    redirectError("You are not the author of the article you tried to edit.");
  if ($from == "delete-article" && $code == "01")
    redirectError("The article you tried to delete does not exist.");
  if ($from == "delete-article" && $code == "02")
    redirectError("You are not the author of the article you tried to delete.");
  if ($from == "delete-article" && $code == "04")
    redirectSuccess("Your article has been deleted.");
  if ($from == "login" && $code == "04")
    redirectSuccess("You have been logged in."); 
}
echo "<center><b>Jacob's List</b><br /><small>Note: Not in order of priority</small></center><br />\n";
$get_to_do = $database->query("SELECT value, done, in_progress, last_updated FROM webmaster_to_do ORDER BY done ASC, in_progress DESC");
$current_values = null;
while ($task = $get_to_do->fetchArray())
{
  if ($current_values == null || $current_values["DONE"] != $task["done"] || $current_values["IN_PROGRESS"] != $task["in_progress"])
  {
    if ($current_values != null)
      echo "</ul>\n";
    $current_values = array("DONE" => $task["done"], "IN_PROGRESS" => $task["in_progress"]);
    echo "<div class=\"webmasterTaskHeader\"><u>" . ($task["done"] == "TRUE" ? "Finished" : ($task["in_progress"] == "TRUE" ? "In Progress" :
	"Upcoming")) . "</u></div>\n";
    echo "<ul class=\"webmasterTasks\">\n";
  }
  $is_recently_changed = date("Y-m-d", strtotime(LATEST_CHANGE_THRESHOLD)) <= date("Y-m-d", strtotime($task["last_updated"]));
  echo "<li>" . ($is_recently_changed ? "<b>" : "") . $task["value"] . ($is_recently_changed ? "</b> <img src=\"" . WEB_PATH . "/images/icons/icon-new.png\" />" : "") . "</li>\n";
}
echo "</ul>\n";
echo "<br />\n";
echo "<center>If you want something designed and you don't see it on this list, suggest it to me! If it's a good idea, it'll make this list!" .
	"</center>\n";
outputManageFooter();
?>
