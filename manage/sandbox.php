<?php
  $page = $_GET["page"];
  $page = "F:/goaves/" . $page;
  if (isset($_POST["file-save"]))
  {
    if (file_put_contents($page, $_POST["file-contents"]) === false)
      $save_error = true;
  }
  if (isset($save_error))
    echo "<div class=\"error\"><b>Error Saving.</b> Could not write contents to file.</div>\n";
  else if (isset($_POST["file-save"]))
    echo "<div class=\"success\"><b>Success.</b> File has been changed.</div>\n";
  echo "'" . $page . "'<br />";
  echo "<form method=\"post\">\n";
  echo "<textarea name=\"file-contents\" rows=\"40\" style=\"width:100%;\">" . htmlentities(file_get_contents($page)) . "</textarea>\n";
  echo "<input type=\"submit\" value=\"Save\" name=\"file-save\" />\n";
?>