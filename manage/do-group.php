<?php
error_reporting(E_ALL);
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset($_POST["upload"]))
{


  if ($attempt_upload["STATUS"] == true)
  {
    $title = $database->escapeString($_POST["comic-title"]);
    if (mb_strlen($title) < 2)
      $error = "Comic title must be at least two characters long. C'mon, you can do it!";
    else
    {
      $handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $title))));
      if (mb_strlen($handle) < 2 || $database->querySingle("SELECT count(*) FROM cartoons WHERE handle='" . $handle . "' OR title LIKE '" . $title . "'") > 0)
        $error = "You tried to upload a comic with the same title/handle as another comic. Be unique, be creative!";
      else
      {
        if ($could_post)
          header("Location: " . WEB_PATH . "/manage/manage-comics.php");
        else
          $error = "Error in the upload process. Feel free to stand up, walk across the hall, and kick our butts.";
      }
    }
  } else
    $error = $attempt_upload["MESSAGE"];
}
outputManageHeader(isset($_GET["group_id"]) ? "Edit Group" : "Create Group");
if (isset($error))
  echo "<div class=\"error\"><b>Error.</b> " . $error . "</div>\n";
  echo "  <script>\n";
  echo "    var web_path = '" . WEB_PATH . "';\n";
  echo "  </script>\n";
  echo "  <form method=\"post\" action=\"" . WEB_PATH . "/manage/do-group.php" . (isset($_GET["group_id"]) ? "?group_id=" . $_GET["group_id"] : "") . "\" >\n";
  echo "      <input type=\"text\" name=\"group-title\" class=\"largeTextbox invalid\" id=\"group-title\" onKeyUp=\"checkComicTitle(); generateComicHandle();\" autocomplete=\"off\" />\n";
  echo "      <div class=\"manageGuidelines\"><b>Guidelines:</b><br />\n";
  echo "        <ul>\n";
  echo "          <li>Must contain (at minimum) two characters.</li>\n";
  echo "          <li>Must be unique; no other article may have the same title.</li>\n";
  echo "        </ul>\n";
  echo "      </div>\n";
  echo "    <b>Handle:</b> '<span id=\"group-handle\"></span>' <small>(Automatically generates as you type)</small><br />\n";
  echo "    <b>Group Type:</b> <select name=\"group-type\">\n";
  echo "      <option value=\"club\">Club</option>\n";
  echo "      <option value=\"fall_sport\">Sport - Fall</option>\n";
  echo "      <option value=\"winter_sport\">Sport - Winter</option>\n";
  echo "      <option value=\"spring_sport\">Sport - Spring</option>\n";
  echo "      <option value=\"class\">Student Class</option>\n";
  echo "      <option value=\"department\">School Department</option>\n";
  echo "    </select><br />\n";
  echo "    <b>Description:</b><br />\n";
  echo "    <textarea></textarea>\n";
  echo "    <center>\n";
  echo "      <input type=\"submit\" class=\"largeButton green\" value=\"" . (isset($_GET["group_id"]) ? "Edit" : "Create") . "\" name=\"submit-group\" />\n";
  echo "    </center>\n";
  echo "  </form>\n";
  
outputManageFooter();
?>