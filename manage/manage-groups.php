<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
outputManageHeader("Manage Groups");
if (isset($_GET["redirect"]))
{
  $from = $_GET["from"];
  $code = $_GET["code"];
  if ($from == "delete-article" && $code == "04")
    echo "<div class=\"success\"><b>Success.</b> Your article has been deleted.</div>\n";
  if ($from == "toggle-article")
  {
    switch ($code)
	{
	  case "01" : echo "<div class=\"error\"><b>Error.</b> The article you attempted to change does not exist.</div>\n"; break;
	  case "02" : echo "<div class=\"error\"><b>Error.</b> You are not the author of the article you attempted to toggle.</div>\n"; break;
	  case "03" : echo "<div class=\"error\"><b>Error.</b> Unable to toggle the article.</div>\n"; break;
	  case "04" : echo "<div class=\"success\"><b>Success.</b> Your article has been toggled.</div>\n"; break;
	}
  }
  if ($from == "post-article")
  {
    switch ($code)
	{
	  case "04" : echo "<div class=\"success\"><b>Success.</b> Your article has been posted. Publish it below if it is ready to go live.</div>\n"; break;
	}
  }
}
$groups = $database->query("SELECT group_identity, handle, title, image_file, icon_file, type FROM groups ORDER BY title ASC");
if ($groups->numberRows() == 0)
  echo "There are no groups.<br /><br />\n";
else
{
  while ($group = $groups->fetchArray())
  {
    echo "<div class=\"articleContainer " . ($group["image_file"] != "" && $group["icon_file"] != "" ? "containerPublished" : "containerDraft") . "\">" . 
	  "<b>" . $group["title"] . "</b> <span style=\"font-size:80%;\">(" . ($group["type"] == "club" ? "Club" : "Sports Team") . ")</span><br />\n";
	echo "<a href=\"" . WEB_PATH . "/manage/do-group.php?group_id=" . $group["group_identity"] . "\"><img src=\"" . 
		WEB_PATH . "/layout/manage-icon-edit.png\" border=\"0\" class=\"draftIcon\" title=\"Edit\" /> Edit</a> ";
	echo "<a href=\"" . WEB_PATH . "/manage/delete-article.php?article_id=" . $article["beat_identity"] . "\"><img src=\"" . 
		WEB_PATH . "/layout/manage-icon-trash.png\" border=\"0\" class=\"draftIcon\" title=\"Delete\" /> Delete</a>";
	echo "</div>\n";
  }
  echo "<br>\n";
}
outputManageFooter();
?>