<?php
require_once ("../config/manage.inc.php");
$database = new DeitloffDatabase(DATABASE_PATH);
session_start();
if (isset($_SESSION[MANAGE_SESSION]))
{
  header("Location: " . WEB_PATH . "/manage/index.php");
  exit();
}
if (isset($_POST["name"]) && isset($_POST["password"]) && mb_strlen($_POST["name"]) > 0 && mb_strlen($_POST["password"]))
{
  $_do_not_auth = array();
  $name = explode(" ", str_replace("%","", mb_strtolower($_POST["name"])));
  if (sizeof($name) != 2)
    $_do_not_auth[] = "Must include only one's first and last name, separated by a single space";
  else
  {
    $first_name = $database->escapeString($name[0]);
	$last_name = $database->escapeString($name[1]);
  }
  if (!isset($_POST["password"]))
    $_do_not_auth[] = "No password provided";
  else
    $password = $database->escapeString(md5($_POST["password"]));
  if (sizeof($_do_not_auth) == 0)
  {
    $staff_identity = $database->querySingle("SELECT identity, permissions FROM staff WHERE first_name LIKE '" . $first_name . "' AND last_name LIKE '" .
		$last_name . "' AND password='" . $password . "' AND active='TRUE' LIMIT 1", true);
	if ($staff_identity !== false)
	{
	  $_SESSION[MANAGE_SESSION] = $staff_identity["identity"];
	  $_SESSION[MANAGE_PERMISSIONS_SESSION] = preg_split('//', $staff_identity["permissions"]);
	  header("Location: " . WEB_PATH . "/manage/index.php?redirect=true&from=login&code=04");
	}
  }
}
outputManageHeader("Login", true);
if (isset($_do_not_auth))
  echo "<font color=\"red\">" . (sizeof($_do_not_auth) > 0 ? $_do_not_auth[0] : "Invalid staff name and/or password.") . "</font><br /><br />\n";
echo "<div style=\"background-color:white;width:300px;font-size:150%;text-align:center;margin-left:auto;margin-right:auto;\">The New Management Panel is now ready to be used. It is still in development, but you can now begin using it by going to <a href=\"" . WEB_PATH . "/new-manage/\">" . WEB_PATH . "/new-manage/</a>.</div>\n";
echo "<form method=\"post\">\n";
echo "<b>Full Name:</b> <input class=\"largeTextbox\" type=\"text\" name=\"name\" autocomplete=\"off\" /><br />\n";
echo "<b>Password:</b> <input class=\"largeTextbox\" type=\"password\" name=\"password\" autocomplete=\"off\" /><br />\n";
echo "<input type=\"submit\" value=\"Login\" name=\"submit\" />\n";
echo "</form>\n";
?>