<?php
require_once ("../config/manage.inc.php");
processManageSession();
if ($_SESSION[MANAGE_PERMISSIONS_SESSION][9] !== "1")
  header("Location: " . WEB_PATH . "/manage/?permission=denied");
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_GET["staff_id"]) || !ctype_digit($_GET["staff_id"]))
  header("Location: " . WEB_PATH . "/manage/admin_landing.php?from=admin-staff-permissions&error=no-passed-staff-id");
if ($database->querySingle("SELECT count(*) FROM staff WHERE identity='" . $database->escapeString($_GET["staff_id"]) . "'") == 0)
  header("Location: " . WEB_PATH . "/manage/admin_landing.php?from=admin-staff-permissions&error=invalid-staff-id");
$staff_identity = $database->escapeString($_GET["staff_id"]);

if (isset($_POST["update-submit"]))
{
  $submit_valid_beat_types = array();
  $submit_permissions = array();
  foreach ($_POST as $element_name => $element_value)
  {
    if (strstr($element_name, "beat-type-") !== FALSE)
    {
      $beat_type_id = $database->escapeString(str_replace("beat-type-", "", $element_name));
      if ($database->querySingle("SELECT count(*) FROM beat_types WHERE type_identity='" . $beat_type_id . "'") > 0)
        $submit_valid_beat_types[] = $beat_type_id;
    } else if (strstr($element_name, "permission-") !== FALSE)
    {
      $permission_id = $database->escapeString(str_replace("permission-", "", $element_name));
      if (array_key_exists($permission_id, $_permission_definition))
        $submit_permissions[$permission_id] = "1";
    }
  }
  $reconst_valid_beat_types = "";
  sort($submit_valid_beat_types);
  foreach ($submit_valid_beat_types as $vbt)
    $reconst_valid_beat_types .= $vbt . "|";
  $reconst_perms = "";
  foreach ($_permission_definition as $identity => $permission)
    $reconst_perms .= (array_key_exists($identity, $submit_permissions) ? "1" : "0");
  $update_success = $database->exec("UPDATE staff SET valid_article_types='" . $reconst_valid_beat_types . "', permissions='".  $reconst_perms . "' WHERE identity='" . $staff_identity . "'");
}
$staff_info = $database->querySingle("SELECT first_name, last_name, valid_article_types, permissions FROM " .
	"staff WHERE identity='" . $staff_identity . "' LIMIT 1", true);
$valid_beat_types = explode("|", $staff_info["valid_article_types"]);
$permissions = preg_split('//', $staff_info["permissions"]);

outputManageHeader("[Admin] Edit Staff Permissions");
if (isset($update_success))
{
  if ($update_success)
    echo "<div class=\"success\"><b>Success.</b> Staff permissions updated</div>\n";
  else
    echo "<div class=\"error\"><b>Error.</b> Staff permissions could not be updated</div>\n";
}
  echo "  <form method=\"post\" action=\"" . WEB_PATH . "/manage/admin-staff-permissions.php?staff_id=" . $staff_identity . "\">\n";
  echo "    <div class=\"manageLabel\">Staff Identity:</span> " . $staff_identity . "<br />\n";
  echo "    <div class=\"manageLabel\">Name:</span> " . $staff_info["first_name"] . " " . $staff_info["last_name"] . "<br /><br />\n";
  echo "    <div class=\"manageSubheader\">Valid Article Types</div>\n";
  $beat_types = $database->query("SELECT type_identity, title, icon_file FROM beat_types ORDER BY sort_order");
  while ($type = $beat_types->fetchArray())
    echo "    <span class=\"manageLabel\">" . $type["title"] . ":</span> <input type=\"checkbox\" name=\"beat-type-" . $type["type_identity"] . "\"" . (in_array($type["type_identity"],
	$valid_beat_types) ? " checked=\"checked\"" : "") . " /><br />\n";
  echo "    <br /><div class=\"manageSubheader\">Staff Permissions</div>\n";
  foreach ($_permission_definition as $identity => $permission)
  {
    echo "    <span class=\"manageLabel\">" . $permission . ":</span> <input type=\"checkbox\" name=\"permission-" . $identity . "\"" . ($permissions[$identity] == "1" ?
	" checked=\"checked\"" : "") . " /><br />\n";
  }
  
  echo "  <center>\n";
  echo "    <div class=\"divider\"></div>\n";
  echo "    <input type=\"submit\" class=\"largeButton green\" name=\"update-submit\" value=\"Update\" /><br />\n";
  echo "  </center>\n";
echo "  </form>\n";
outputManageFooter();
?>