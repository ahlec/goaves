<?php
require_once ("../config/manage.inc.php");
processManageSession();

$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_GET["notice_id"]) || !ctype_digit($_GET["notice_id"]))
  header("Location: " . WEB_PATH . "/manage/manage-notices.php?redirect=yes&from=delete-notice&code=01");
$notice = $database->escapeString($_GET["notice_id"]);
if ($database->querySingle("SELECT count(*) FROM notices WHERE notice_identity='" . $notice . "'") == 0)
  header("Location: " . WEB_PATH . "/manage/manage-notices.php?redirect=yes&from=delete-notice&code=01");

if (isset($_POST["delete-yes"]) || isset($_POST["delete-no"]))
{
  $delete_code = "03";
  if (isset($_POST["delete-yes"]))
    $delete_code = ($database->exec("DELETE FROM notices WHERE notice_identity='" . $notice . "'") ? "04" : "03");
  header("Location: " . WEB_PATH . "/manage/manage-notices.php?code=" . $delete_code);
}
outputManageHeader("Delete Article");

$notice_title = $database->querySingle("SELECT title FROM notices WHERE notice_identity='" . $notice . "' LIMIT 1");

echo "<center>Delete<br /><b>" . $notice_title . "</b><br /><br />\n";

echo "<form method=\"post\" action=\"" . WEB_PATH . "/manage/delete-notice.php?notice_id=" . $notice . "\">\n";
echo "  <button name=\"delete-yes\" class=\"largeButton red\">Yes</button>\n";
echo "  <button name=\"delete-no\" class=\"largeButton green\">No</button>\n";
echo "</form>\n";
echo "<br /></center>\n";

outputManageFooter();
?>