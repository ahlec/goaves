<?php
error_reporting(E_ALL);
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);

if (isset($_POST["upload"]) || isset($_FILES["image-file"]))
{
  $attempt_upload = uploadImage();
  if ($attempt_upload["STATUS"] == true)
  {
    $title = $database->escapeString($_POST["comic-title"]);
    if (mb_strlen($title) < 2)
      $error = "Comic title must be at least two characters long. C'mon, you can do it!";
    else
    {
      $handle = $database->escapeString(mb_strtolower(preg_replace("[ ]", "-", preg_replace("/[^a-zA-Z0-9 ]/","", $title))));
      if (mb_strlen($handle) < 2 || $database->querySingle("SELECT count(*) FROM cartoons WHERE handle='" . $handle . "' OR title LIKE '" . $title . "'") > 0)
        $error = "You tried to upload a comic with the same title/handle as another comic. Be unique, be creative!";
      else
      {
        require_once (DOCUMENT_ROOT . "/config/functions-images.inc.php");
        $comic_image = new ProcessedImage();
        $comic_image->loadOriginal(DOCUMENT_ROOT . "/manage/temp/" . $attempt_upload["IMAGE_FILE"]);
        $comic_image->scale(650, 500, false);
        $new_filename = $comic_image->save(DOCUMENT_ROOT . "/images/cartoons/", $handle);
        $comic_image->close();
        $comic_image->deleteOriginalImage();
        $could_post = $database->exec("INSERT INTO cartoons(artist, image_file, handle, title, date_posted, published, related_group) VALUES('" . $database->escapeString($_SESSION[MANAGE_SESSION]) .
	    "','" . $database->escapeString($new_filename) . "','" . $handle . "','" . $title . "','" . date("Y-m-d",strtotime("+1 hours")) . "','TRUE','')");
        if ($could_post)
	 {
	   $comic_identity = $database->querySingle("SELECT identity FROM cartoons WHERE handle='" . $handle . "' LIMIT 1");
	   $database->exec("INSERT INTO multimedia_feed(media_type, media_identity, post_time, post_type, posting_staff_member) VALUES('cartoon','" . $comic_identity . "','" .
		date("Y-m-d", strtotime("+1 hours")) . "','posted','" . $database->escapeString($_SESSION[MANAGE_SESSION]) . "')");
          header("Location: " . WEB_PATH . "/manage/manage-comics.php");
        } else
          $error = "Error in the upload process. Feel free to stand up, walk across the hall, and kick our butts.";
      }
    }
  } else
    $error = $attempt_upload["MESSAGE"];
}
outputManageHeader("Upload Comic");
if (isset($error))
  echo "<div class=\"error\"><b>Error.</b> " . $error . "</div>\n";
  echo "  <br />\n";
  echo "  <script>\n";
  echo "    var web_path = '" . WEB_PATH . "';\n";
  echo "  </script>\n";
  echo "  <div class=\"manageSubheader\">Upload</div>\n";
  echo "  <form method=\"post\" enctype=\"multipart/form-data\" action=\"" . WEB_PATH . "/manage/upload-comic.php\" >\n";
  echo "      <input type=\"text\" name=\"comic-title\" class=\"largeTextbox invalid\" id=\"comic-title\" onKeyUp=\"checkComicTitle(); generateComicHandle();\" autocomplete=\"off\" />\n";
  echo "      <div class=\"manageGuidelines\"><b>Guidelines:</b><br />\n";
  echo "        <ul>\n";
  echo "          <li>Must contain (at minimum) two characters.</li>\n";
  echo "          <li>Must be unique; no other article may have the same title.</li>\n";
  echo "        </ul>\n";
  echo "      </div>\n";
  echo "    <b>Handle:</b> '<span id=\"comic-handle\"></span>' <small>(Automatically generates as you type)</small><br />\n";
  echo "    <b>File:</b> <input type=\"file\" name=\"image-file\" /><br />\n";
  echo "    <div class=\"manageGuidelines\">\n";
  echo "    <b>Guidelines:</b> <br />\n";
  echo "    <ul>\n";
  echo "      <li><b>School appropriate.</b> Duh.</li>\n";
  echo "      <li><b>Proper file type.</b> The program will only accept JPEG, PNG, or GIF files currently.</li>\n";
  echo "    </ul>\n";
  echo "    </div>\n";
  /*$get_groups = $database->query("SELECT group_identity, title, (CASE WHEN image_file = '' THEN \"false\" WHEN image_file IS null THEN \"false\" ELSE \"true\" END) AS \"has_image\", " .
	"(CASE WHEN icon_file = '' THEN \"false\" WHEN icon_file IS null THEN \"false\" ELSE \"true\" END) AS \"has_icon\" FROM groups ORDER BY has_image, has_icon, title ASC");
  echo "    <b>Group:</b> <select name=\"group\">\n";
  while ($group = $get_groups->fetchArray())
    echo "      <option value=\"" . $group["group_identity"] . "\">" . ($group["has_image"] == "false" || $group["has_icon"] == "false" ? "[NO IMAGE] " : "") . $group["title"] . "</option>\n";
  echo "    </select><br />\n";*/
  echo "    <center>\n";
  echo "      <input type=\"submit\" class=\"largeButton green\" value=\"Upload\" name=\"upload\" />\n";
  echo "    </center>\n";
  echo "  </form>\n";
  
outputManageFooter();

function uploadImage()
{
  if ($_FILES["image-file"]["error"] != 0)
  {
    switch ($_FILES["image-file"]["error"])
	{
	  case UPLOAD_ERR_INI_SIZE:
	  case UPLOAD_ERR_FORM_SIZE: return array("STATUS" => false, "MESSAGE" => "The file that you attempted to upload had a filesize that was too large.", 
		"IMAGE_FILE" => "");
	  case UPLOAD_ERR_PARTIAL: return array("STATUS" => false, "MESSAGE" => "The file upload process was interrupted.", "IMAGE_FILE" => "");
	  case UPLOAD_ERR_NO_FILE: return array("STATUS" => false, "MESSAGE" => "No file uploaded.", "IMAGE_FILE" => "");
	  default: return array("STATUS" => false, "MESSAGE" => "Encountered an unhandled exception when attempting to upload (Upload error #" .
		$_FILES["image-file"]["error"] . ").", "IMAGE_FILE" => "");
	}
  }
  if (!is_uploaded_file($_FILES["image-file"]["tmp_name"]))
    return array("STATUS" => false, "MESSAGE" => "Selected file was not an uploaded file.", "IMAGE_FILE" => "");
   
  $image_size = getimagesize($_FILES["image-file"]["tmp_name"]);
  if ($image_size === false)
    return array("STATUS" => false, "MESSAGE" => "Uploaded file is not an image in the proper format.", "IMAGE_FILE" => "");
	
  $image_type = exif_imagetype($_FILES["image-file"]["tmp_name"]);
  if ($image_type != IMAGETYPE_GIF && $image_type != IMAGETYPE_JPEG && $image_type != IMAGETYPE_PNG)
    return array("STATUS" => false, "MESSAGE" => "Uploaded image file is not a file of an allowed type (JPEG, PNG, or GIF only).", "IMAGE_FILE" => "");

  switch ($image_type)
  {
    case IMAGETYPE_GIF: $imagetype = "gif"; break;
	case IMAGETYPE_JPEG: $imagetype = "jpg"; break;
	case IMAGETYPE_PNG: $imagetype = "png"; break;
  }
  $tmp_name = preg_split("(\\\\|/)", $_FILES["image-file"]["tmp_name"]);
  $tmp_name = $tmp_name[sizeof($tmp_name) - 1];// . "." . $imagetype;
  
  if (!move_uploaded_file($_FILES["image-file"]["tmp_name"], DOCUMENT_ROOT . "/manage/temp/" . $tmp_name))
    return array("STATUS" => false, "MESSAGE" => "Could not move uploaded image to the temporary directory.", "IMAGE_FILE" => "");
  return array("STATUS" => true, "MESSAGE" => "", "IMAGE_FILE" => $tmp_name);
}
?>