<?php
require_once ("../config/manage.inc.php");
processManageSession();
$database = new DeitloffDatabase(DATABASE_PATH);
if (!isset($_GET["article_id"]) || !ctype_digit($_GET["article_id"]))
  header("Location: " . WEB_PATH . "/manage/manage-articles.php?redirect=yes&from=toggle-article&code=01");
$article = $database->escapeString($_GET["article_id"]);
if ($database->querySingle("SELECT count(*) FROM beats WHERE beat_identity='" . $article . "' AND staff_identity='" . $_SESSION[MANAGE_SESSION] . "'") == 0)
  header("Location: " . WEB_PATH . "/manage/manage-articles.php?redirect=yes&from=toggle-article&code=02");

$toggle_query = "UPDATE beats SET published='" . ($database->querySingle("SELECT published FROM beats WHERE beat_identity='" . $article . "' LIMIT 1") ==
	"TRUE" ? "FALSE" : "TRUE") . "' WHERE beat_identity='" . $article . "'";
if (!$database->exec($toggle_query))
  header("Location: " . WEB_PATH . "/manage/manage-articles.php?redirect=yes&from=toggle-article&code=03");
$database->exec("UPDATE statistics SET value=(SELECT last_updated FROM beats WHERE published='TRUE' ORDER BY " .
	"last_updated DESC LIMIT 1) WHERE stat_handle='content_last_updated'");
header("Location: " . WEB_PATH . "/manage/manage-articles.php?redirect=yes&from=toggle-article&code=04");
?>