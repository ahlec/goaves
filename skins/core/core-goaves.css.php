<?php
require_once("../../config/main.inc.php");
if (!isset($_SESSION))
  session_start();
define("ADMIN_VIEW", (isset($_SESSION[MANAGE_SESSION]) && $_SESSION[MANAGE_PERMISSIONS_SESSION][9] == "1"));
$theme_to_use = (ADMIN_VIEW && DEVELOPMENTAL_THEME != "" ? DEVELOPMENTAL_THEME : CURRENT_THEME);
require_once (DOCUMENT_ROOT . "/skins/" . $theme_to_use . "/color-scheme.php");

echo "/* Staff Listing */
div.staffListingContainer, div.staffListingContainerHover
{
  display:inline-block;
  border:1px solid black;
  background-color:white;
  margin-bottom:4px;
  cursor:pointer;
}
* html div.staffListingContainer, * + html div.staffListingContainer, * html div.staffListingContainerHover, * + html div.staffListingContainerHover
{
  display:inline;
  margin-right:4px;
}
div.staffListingContainer
{
  filter:alpha(opacity=60);
  opacity:0.6;
}
div.staffListingContainer div, div.staffListingContainerHover div
{
  text-align:center;
  font-size:90%;
  padding-bottom:1px;
  border-bottom:1px solid black;
  font-weight:bold;
  background-color:#71C671;
  max-width:150px;
  height:32px;
}
* html div.staffListingContainer div, * + html div.staffListingContainer div, * html div.staffListingContainerHover div, * + html div.staffListingContainerHover div
{
  width:150px;
}
div.staffListingContainer img, div.staffListingContainerHover img
{
  max-width:150px;
  max-height:200px;
  width:150px;
  height:200px;
}

/* Staff Listing New */
div.staffListContainer
{
  margin-bottom:15px;
}
div.staffListContainer div.pageHeader
{
  font-size:200%;
  font-weight:bold;
  text-align:center;
  margin:0px 20px 8px 20px;
  border-bottom:1px solid " . $color_scheme["PAGE_HEADER_BORDER"] . ";
}
div.staffListContainer div.sidebar
{
  text-align:left;
  float:left;
  width:200px;
  border:1px solid " . $color_scheme["SIDE_PANEL_BORDER"] . ";
  padding:4px;
  background-color:" . $color_scheme["SIDE_PANEL_BACKGROUND"] . ";
  font-size:80%;
}
div.staffListContainer div.sidebar div.header
{
  font-weight:bold;
  margin-left:10px;
  margin-right:10px;
  border-bottom:1px solid " . $color_scheme["SIDE_PANEL_BORDER"] . ";
  text-align:center;
}
div.staffListContainer div.sidebar input.search
{
  font-size:90%;
  margin:4px 5px 10px 5px;
  border:1px solid " . $color_scheme["INPUT_BORDER"] . ";
  padding:2px;
  width:184px;
}
div.staffListContainer div.sidebar input.empty
{
  color:" . $color_scheme["INPUT_DISABLED_COLOR"] . ";
}
div.staffListContainer div.sidebar div.item
{
  cursor:pointer;
}
div.staffListContainer div.sidebar div.selectedItem
{
  font-weight:bold;
  cursor:pointer;
}
div.staffListContainer div.viewport
{
  margin-left:210px;
  font-size:90%;
}
div.staffListContainer div.viewport div.bar
{
  text-align:center;
  margin:5px 10px 5px 10px;
  background-color:" . $color_scheme["NOTICE_BACKGROUND"] . ";
  border:1px solid " . $color_scheme["NOTICE_BORDER"] . ";
}
div.staffListContainer div.viewport img.staffIcon
{
  border:1px solid " . $color_scheme["STAFF_IMAGE_BORDER"] . ";
  max-width:60px;
  width:60px;
  max-height:60px;
  height:60px;
  background-image:url('loading-image.gif');
  background-position:14px 14px;
  background-repeat:no-repeat;
}
div.staffListContainer div.viewport img.iconNoFocus
{
  background-color:" . $color_scheme["DISABLED_IMAGE_BACKGROUND"] . ";
  opacity:0.6;
  filter:alpha(opacity=60);
}
div.staffListHoverBox
{
  width:200px;
  max-width:200px;
  height:63px;
  max-height:63px;
  overflow:hidden;
  font-size:70%;
  position:absolute;
  padding:5px;
  background-image:url('staff-listing-hover-box.png');
  background-repeat:no-repeat;
}

/* Staff Portfolio New */
div.portfolioContainer
{
  margin-top:7px;
}
div.portfolioContainer div.staffName
{
  border:1px solid " . $color_scheme["PAGE_HEADER_BORDER"] . ";
  border-bottom:0px;
  text-align:right;
  padding-right:10px;
  font-weight:bold;
  background-color:" . $color_scheme["PAGE_HEADER_BACKGROUND"] . ";
  font-size:125%;
}
div.portfolioContainer div.staffSubtitle
{
  text-align:right;
  font-size:90%;
  background-color:" . $color_scheme["PAGE_SUBHEADER_BACKGROUND"] . ";
  border:1px solid " . $color_scheme["PAGE_SUBHEADER_BORDER"] . ";
  padding-right:10px;
}
div.portfolioContainer img.staffPicture
{
  border:1px solid " . $color_scheme["STAFF_IMAGE_BORDER"] . ";
  float:left;
  max-width:300px;
  width:300px;
  max-height:400px;
  height:400px;
  margin:5px 10px 5px 10px;
}
div.portfolioContainer div.staffBiography
{
  max-height:323px;
  height:323px;
  overflow:auto;
  padding:4px;
  margin:5px 10px 5px 0px;
  font-size:90%;
  text-align:left;
}
div.portfolioContainer div.staffLinks
{
  border-bottom:1px solid " . $color_scheme["ALTERNATE_BORDER"] . ";
  overflow:hidden;
  padding:4px;
  margin:0px 10px 5px 0px;
  max-height:56px;
  height:56px;
  text-align:center;
}
div.portfolioContainer div.staffLinks img.staffLinkIcon
{
  max-width:48px;
  width:48px;
  max-height:48px;
  height:48px;
}
div.portfolioContainer div.staffHistory
{
  float:left;
  margin:5px 10px 5px 10px;
  border-right:1px solid " . $color_scheme["ALTERNATE_BORDER"] . ";
  height:300px;
  max-height:300px;
  width:292px;
  max-width:292px;
  padding:4px;
  font-size:90%;
  text-align:left;
}
div.portfolioContainer div.staffHistory div.historyHeader
{
  font-weight:bold;
  text-align:center;
  border-bottom:1px solid " . $color_scheme["SIDE_PANEL_BORDER"] . ";
  margin-left:30px;
  margin-right:30px;
}
div.portfolioContainer div.staffHistory ul
{
  margin:0px;
}
div.portfolioContainer div.staffHistory div.historyText
{
  text-align:center;
  margin:10px 0px 10px 0px;
  font-size:90%;
}
div.portfolioContainer div.staffWorksNavigation
{
  text-align:center;
  font-size:90%;
  margin:0px 10px 5px 323px;
  border-bottom:1px solid " . $color_scheme["ALTERNATE_BORDER"] . ";
  max-height:20px;
  height:20px;
}
div.portfolioContainer div.staffWorksNavigation span.enabled
{
  color:black;
  cursor:pointer;
}
div.portfolioContainer div.staffWorksNavigation span.disabled
{
  cursor:default;
  color:" . $color_scheme["DISABLED_TEXT"] . ";
}
div.portfolioContainer div.staffWorksNavigation span.selected
{
  color:" . $color_scheme["SELECTED_TEXT"] . ";
  cursor:default;
  font-weight:bold;
}
div.portfolioContainer div.staffWorks
{
  max-height:286px;
  height:290px;
  overflow:auto;
  text-align:left;
  font-size:90%;
}
div.portfolioContainer div.staffWorks img.worksImageIcon
{
  max-width:80px;
  width:80px;
  max-height:80px;
  height:80px;
  border:1px solid " . $color_scheme["IMAGE_BORDER"] . ";
  padding:1px;
}
div.portfolioContainer div.staffWorks div.worksArticleContainer
{
  font-size:80%;
  margin:0px 10px 5px 10px;
  border:1px solid " . $color_scheme["NEWS_LIST_ITEM_BORDER"] . ";
  background-color:" . $color_scheme["NEWS_LIST_ITEM_BACKGROUND"] . ";
}
div.portfolioContainer div.staffWorks div.worksArticleContainer div.title
{
  font-weight:bold;
  font-size:125%;
  border-bottom:1px solid " . $color_scheme["NEWS_LIST_ITEM_BORDER"] . ";
  background-color:" . $color_scheme["NEWS_LIST_ITEM_TITLE_BACKGROUND"] . ";
  padding-left:7px;
  padding-right:7px;
}
div.portfolioContainer div.staffWorks div.worksArticleContainer div.subtitle
{
  font-size:90%;
  border-bottom:1px solid " . $color_scheme["NEWS_LIST_ITEM_BORDER"] . ";
  background-color:" . $color_scheme["NEWS_LIST_ITEM_SUBTITLE_BACKGROUND"] . ";
  padding-left:7px;
  padding-right:7px;
}
div.portfolioContainer div.staffWorks div.worksArticleContainer div.snippet
{
  padding-left:4px;
  padding-right:4px;
}

/* Staff Portfolio */
div.portfolioContainerOld
{
  margin-right:300px;
}
div.portfolioContainerOld div.name
{
  text-align:left;
  font-weight:bold;
  font-size:150%;
  border-bottom:1px solid #7da573;
  background-color:#85B479;
  padding-left:5px;
}
div.portfolioContainerOld div.positions
{
  text-align:left;
  border-bottom:1px solid #7da573;
  background-color:#9BC191;
  font-size:80%;
  padding-left:5px;
}
img.staffPortfolioImageOld
{
  float:right;
  margin:5px;
  border:1px solid black;
  background-color:white;
  padding:1px;
  max-width:286px;
  width:286px;
}
div.portfolioContainerOld div.blurb
{
  border-bottom:1px solid #476A34;
  font-size:80%;
  margin:5px 10px 5px 10px;
  padding:0px 10px 5px 10px;
  text-align:left;
}
div.portfolioContainerOld div.works
{
  text-align:left;
  margin-left:10px;
  margin-right:10px;
  margin-bottom:10px;
}
div.portfolioContainerOld div.works div.header
{
  font-weight:bold;
  border-bottom:1px solid black;
  width:40%;
  margin-top:8px;
}
div.portfolioContainerOld div.works a.entry
{
  font-size:90%;
  padding-left:16px;
  background:transparent url('bullet-green.png') no-repeat top left;
}
div.portfolioContainerOld  div.works img.entry
{
  border:1px solid black;
  background-color:white;
  padding:1px;
  max-width:95px;
  max-height:95px;
}

/* Yearbook */
div.yearbookContainer
{
  margin-top:6px;
  border-top:1px solid " . $color_scheme["ALTERNATE_BORDER"] . ";
  margin-left:5px;
  margin-right:10px;
  padding-top:10px;
}
div.yearbookContainer div.entry
{
  border:1px solid " . $color_scheme["PAGE_HEADER_BORDER"] . ";
  margin-bottom:7px;
  background-color:" . $color_scheme["PAGE_SUBHEADER_BACKGROUND"] . ";
  text-align:left;
  font-size:90%;
  margin-left:10px;
  margin-right:10px;
}
div.yearbookContainer div.entry div.question
{
  border-bottom:1px solid " . $color_scheme["PAGE_HEADER_BORDER"] . ";
  padding-bottom:4px;
  background-color:" . $color_scheme["PAGE_HEADER_BACKGROUND"] . ";
  padding-top:4px;
  padding-left:2px;
}
div.yearbookContainer div.entry div.answer
{
  padding-left:2px;
  padding-bottom:1px;
}
table.yearbookConfirmations
{
  width:95%;
  margin-top:8px;
  border-collapse:collapse;
  margin-bottom:8px;
}
table.yearbookConfirmations tr.header td
{
  font-weight:bold;
  border:1px solid black;
  width:25%;
  background-color:#86C67C;
  text-align:center;
}
table.yearbookConfirmations tr.row td, table.yearbookConfirmations tr.rowHover td
{
  font-size:80%;
  text-align:center;
  border-right:1px dashed gray;
  border-bottom:1px dashed gray;
}
table.yearbookConfirmations tr.rowHover td
{
  background-color:#66FF66;
  cursor:default;
}
table.yearbookConfirmations tr.row td.last, table.yearbookConfirmations tr.rowHover td.last
{
  border-right:none;
}
div.yearbookHeader
{
}
div.yearbookHeader div.header
{
  font-weight:bold;
  font-size:200%;
  text-align:center;
}
div.yearbookHeader div.subheader
{
  font-size:90%;
  text-align:center;
}
div.yearbookHeader div.navigation
{
  margin-top:5px;
  font-size:90%;
}

/* Home */
div.indexContainer
{
  margin:5px;
  text-align:right;
}
div.indexContainer div.majorStoryContainer
{
  float:right;
  border:1px solid " . $color_scheme["ALTERNATE_BORDER"] . ";
  background:" . $color_scheme["ALTERNATE_BACKGROUND"] . " url('') no-repeat top left;
  height:300px;
  width:700px;
  position:relative;
  max-height:300px;
}
div.indexContainer div.majorStoryContainer div.storyContent
{
  background:" . $color_scheme["MAJOR_STORY_BACKGROND"] . ";
  text-align:left;
  color:" . $color_scheme["MAJOR_STORY_FOREGROUND"] . ";
  opacity:0.9;
  filter:alpha(opacity=90);
  padding-left:10px;
  padding-right:10px;
  border-top:1px solid " . $color_scheme["MAJOR_STORY_FOREGROUND"] . ";
  position:absolute;
  bottom:0px;
  right:0px;
  left:0px;
  font-size:90%;
}
div.indexContainer div.majorStoryContainer div.storyContent div.title
{
  font-weight:bold;
}
div.indexContainer div.majorStoryNavigation
{
  float:right;
  width:700px;
  border:1px solid " . $color_scheme["ALTERNATE_BORDER"] . ";
  border-top:0px;
  text-align:center;
  height:19px;
  padding-bottom:1px;
  background-color:#86C67C;
}
div.indexContainer div.majorStoryNavigation div.item, div.indexContainer div.majorStoryNavigation div.active
{
  display:inline-block;
  cursor:pointer;
  font-size:80%;
  background-color:#B7C8B6;
  border:1px solid #838B83;
  padding-left:3px;
  padding-right:3px;
}
* html div.indexContainer div.majorStoryNavigation div.item, * + html div.indexContainer div.majorStoryNavigation div.item, 
	* html div.indexContainer div.majorStoryNavigation div.active, * + html div.indexContainer div.majorStoryNavigation div.active
{
  display:inline;
}

div.indexContainer div.majorStoryNavigation div.active
{
  background-color:#CCFFCC;
  border:1px solid #3D9140;
}
div.indexContainer div.goavesSnippetContainer
{
  border:1px solid " . $color_scheme["ALTERNATE_BORDER"] . ";
  height:321px;
  width:270px;
  background-color:" . $color_scheme["ALTERNATE_BACKGROUND"] . ";
  text-align:center;
}
div.indexContainer div.featuredArticleTypesContainer
{
  margin-right:5px;
  text-align:left;
}
div.indexContainer div.featuredArticleTypesContainer div.container
{
  display:inline-block;
  vertical-align:top;
  width:350px;
  margin:5px 0px 5px 5px;
  min-height:200px;
  text-align:left;
}
* html div.indexContainer div.featuredArticleTypesContainer div.container, * + html div.indexContainer div.featuredArticleTypesContainer div.container
{
  display:inline;
}
div.indexContainer div.featuredArticleTypesContainer img
{
  float:left;
  border:1px solid " . $color_scheme["IMAGE_BORDER"] . ";
  background-color:" . $color_scheme["IMAGE_BACKGROUND"] . ";
  padding:1px;
  max-width:200px;
  max-height:200px;
  margin-right:5px;
  margin-bottom:5px;
}
div.indexContainer div.featuredArticleTypesContainer a.first
{
  font-weight:bold;
}
div.indexContainer div.featuredArticleTypesContainer div.firstBlurb
{
  font-size:80%;
}
div.indexContainer div.featuredArticleTypesContainer div.divider
{
  clear:both;
  border-top:1px solid " . $color_scheme["ALTERNATE_BORDER"] . ";
  margin:0px auto 5px auto;
  width:300px;
}
div.indexContainer div.featuredArticleTypesContainer div.container a.other
{
  font-size:75%;
  padding-left:16px;
  background:transparent url('bullet-green.png') no-repeat top left;
}
div.indexContainer div.featuredArticleTypesContainer div.container div.title
{
  font-weight:bold;
  border-bottom:2px solid " . $color_scheme["NEWS_CATEGORY_BORDER"] . ";
  padding-right:10px;
  text-align:right;
  margin-bottom:5px;
}
div.indexContainer div.featuredArticleTypesContainer div.container div.title span
{
  display:inline-block;
  background-color:" . $color_scheme["NEWS_CATEGORY_BORDER"] . ";
  color:" . $color_scheme["NEWS_CATEGORY_BACKGROUND"] . ";
  padding-left:5px;
  padding-right:5px;
}
div.indexContainer div.featuredArticleTypesContainer div.container div.title span a
{
  border-bottom:0px;
  color:" . $color_scheme["NEWS_CATEGORY_LINK"] . ";
}
div.indexContainer div.featuredArticleTypesContainer div.container div.title span a:hover
{
  color:" . $color_scheme["NEWS_CATEGORY_LINK_HOVER"] . ";
}

div.indexContainer div.pollContainer
{
  float:right;
  border:1px solid " . $color_scheme["ALTERNATE_BORDER"] . ";
  width:260px;
  margin-top:10px;
  text-align:left;
  padding-bottom:5px;
}
div.indexContainer div.pollContainer div.header
{
  font-weight:bold;
  text-align:center;
  border-bottom:1px solid " . $color_scheme["ALTERNATE_BORDER"] . ";
  background-color:" . $color_scheme["ALTERNATE_BACKGROUND"] . ";
}
div.indexContainer div.pollContainer div.no_poll
{
  margin-top:10px;
  margin-bottom:10px;
  padding-left:4px;
  padding-right:4px;
  font-size:80%;
  text-align:center;
}
div.indexContainer div.pollContainer div.question
{
  font-weight:bold;
  padding-left:3px;
}
div.indexContainer div.pollContainer div.entry
{
  padding-left:3px;
}
div.indexContainer div.pollContainer div.submit
{
  text-align:center;
}
div.indexContainer div.pollContainer div.responses
{
  margin-left:5px;
}

div.indexContainer div.header
{
  text-align:right;
  font-weight:bold;
  font-size:150%;
}

/* Contact Information */
div.contactContainer
{
  text-align:left;
  margin-top:10px;
}
div.contactContainer div.group
{
  border:1px solid " . $color_scheme["PAGE_HEADER_BORDER"] . ";
  margin-right:20px;
  margin-left:20px;
  margin-bottom:10px;
}
div.contactContainer div.group img.icon
{
  float:right;
  border-left:1px solid " . $color_scheme["IMAGE_BORDER"] . ";
  max-height:150px;
  height:150px;
  max-width:150px;
  width:150px;
  background-color:" . $color_scheme["IMAGE_BACKGROUND"] . ";
}
div.contactContainer div.group div.title
{
  font-weight:bold;
  border-bottom:1px solid " . $color_scheme["PAGE_HEADER_BORDER"] . ";
  margin-right:150px;
  background-color:" . $color_scheme["PAGE_HEADER_BACKGROUND"] . ";
  text-align:center;
}
div.contactContainer div.group div.entry
{
  margin-left:10px;
  margin-bottom:5px;
  padding-bottom:5px;
  border-bottom:1px solid " . $color_scheme["PAGE_SUBHEADER_BORDER"] . ";
  margin-right:160px;
}
div.contactContainer div.group div.entry div.name
{
  font-weight:bold;
}
div.contactContainer div.group div.entry div.reason
{
  margin-left:10px;
  margin-right:10px;
  text-align:center;
}

/* Error Pages */
div.errorContainer
{
  margin-bottom:10px; 
}
div.errorContainer div.header
{
  font-weight:bold;
  font-size:200%;
  text-align:center;
}
div.errorContainer div.subheader
{
  margin-left:15%;
  margin-right:15%;
  border-bottom:1px solid " . $color_scheme["PAGE_SUBHEADER_BORDER"] . ";
  text-align:center;
  font-style:italic;
  font-size:90%;
  margin-bottom:10px;
}
div.errorContainer div.description
{
  text-align:center;
  margin-left:10px;
  margin-right:10px;
  font-size:90%;
}";
?>